#include "numa/boot/index.hpp"

int
main()
{
  numa::index<3> const ind{1, 0, 3};
  return ind[1];
}
