[![pipeline status](https://gitlab.com/TobyBrull/numa/badges/master/pipeline.svg)](https://gitlab.com/TobyBrull/numa/-/commits/master)

# numa
A library for numerical mathematics

## Developing, Building, Testing, Packaging

First build the base development image.
```
docker build -t numa-base -f Dockerfile.numa-base .
```

For complete builds one can simply run "docker build".

```
# TODO: add spack once it's faster.
for package_manager in archlinux hunter fetch_content debian conan nix;
do
  docker build -t build -f Dockerfile.$package_manager . ;
done
```

For development one can do the following and then run CMake in that container
as indicated by the corresponding Dockerfile.

```
docker build -t numa-dev -f Dockerfile.$package_manager --target=dev .
docker run --rm -it -v $PWD:/app -w /app numa-dev bash
```

## Prebuilt packages

Conan:
```
conan remote add gitlab https://gitlab.com/api/v4/packages/conan
conan install numa/0.0.1@TobyBrull+numa/testing --remote=gitlab
```

## Docs

[Here](docs/numa.md)
