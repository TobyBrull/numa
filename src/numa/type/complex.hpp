#ifndef INCLUDE_GUARD_NUMA_TYPE_INTERVAL_H_
#define INCLUDE_GUARD_NUMA_TYPE_INTERVAL_H_

namespace numa {
  template<typename T>
  struct complex {
    T real;
    T imag;
  };
}

#endif
