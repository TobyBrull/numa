namespace numa {
  struct as_nan_t {
  };

  constexpr inline as_nan_t as_nan = {};

  template<typename T>
  class float_type {
   public:
    float_type(T value);
    float_type(as_nan_t, std::string_view const& value);
    float_type(as_nan_t, int value);

    constexpr bool is_nan() const;

    T operator() const;
    std::string get_string_from_nan() const;
    get_integer_from_nan() const;

   private:
    double value_;
  };
}
