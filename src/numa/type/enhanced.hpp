#ifndef INCLUDE_GUARD_NUMA_TYPE_ENHANCED_H_
#define INCLUDE_GUARD_NUMA_TYPE_ENHANCED_H_

namespace numa {
  template<typename T, int Factor>
  class enhanced {
   public:
   private:
    std::array<T, Factor> value_;
  };
}

#endif
