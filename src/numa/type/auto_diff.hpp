#ifndef INCLUDE_GUARD_NUMA_TYPE_AUTO_DIFF_H_
#define INCLUDE_GUARD_NUMA_TYPE_AUTO_DIFF_H_

namespace numa {
  template<typename T, int Derivatives>
  class auto_diff {
   public:
   private:
    T value_;
    std::array<T, Derivatives> derivatives_;
  };
}

#endif
