#ifndef INCLUDE_GUARD_NUMA_TYPE_SAFE_H_
#  define INCLUDE_GUARD_NUMA_TYPE_SAFE_H_

namespace numa {
  namespace detail {
    template<typename Tag1, typename Tag2>
    struct unit_multiplies {
    };
  }

  template<typename Tag>
  struct unit {
  };

  template<typename Tag1, typename Tag2>
  auto operator*(unit<Tag1>, unit<Tag2>)
  {
    return unit<detail::unit_multiplies<Tag1, Tag2>>{};
  }

  template<typename T, typename Unit = void>
  class safe {
   public:
    T value_;
  };

  template<typename T, typename Unit1, typename Unit2>
  auto operator*(safe<T, Unit1> const x, safe<T, Unit2> const y)
  {
    using RetvalUnit = decltype(Unit1() * Unit2());

    return safe<T, RetvalUnit>(x.value() * y.value());
  }

  template<typename T, typename Unit>
  safe<T, Unit> operator+(safe<T, Unit> const x, safe<T, Unit> const y)
  {
    return {x.value() + y.value()};
  }
}

#endif

int
main()
{
  struct meter_tag : numa::tag {
  };
  struct second_tag : numa::tag {
  };

  using u_m = numa::unit<meter_tag>;
  using u_s = numa::unit<second_tag>;
  ;

  numa::safe<double, u_m> length = 3.0;
  numa::safe<double, u_s> time   = 1.0;
}
