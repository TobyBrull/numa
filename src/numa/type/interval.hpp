#ifndef INCLUDE_GUARD_NUMA_TYPE_INTERVAL_H_
#define INCLUDE_GUARD_NUMA_TYPE_INTERVAL_H_

namespace numa {
  template<typename T>
  class interval {
   public:
   private:
    T a_; // Lower bound.
    T b_; // Upper bound.
  };
}

#endif
