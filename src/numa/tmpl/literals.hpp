#ifndef INCLUDE_GUARD_NUMA_TMPL_LITERALS_H_
#define INCLUDE_GUARD_NUMA_TMPL_LITERALS_H_

#include "numa/boot/error.hpp"
#include "numa/boot/fwd.hpp"

#include <optional>
#include <string>

namespace numa {
  struct literal_tag_t {
  };

  /*
   *
   */
  template<typename T, T v>
  struct c_integer_t {
   public:
    constexpr static literal_tag_t literal_tag = {};

    using type = c_integer_t;

    constexpr static T value = v;

    constexpr operator T() const
    {
      return value;
    }
  };

  template<index_t v>
  using c_index_t = c_integer_t<index_t, v>;

  template<bool v>
  using c_bool_t = c_integer_t<bool, v>;

  using c_true_t  = c_bool_t<true>;
  using c_false_t = c_bool_t<false>;

  /*
   *
   */
  template<char... Cs>
  struct c_string_t {
    constexpr static literal_tag_t literal_tag = {};

    using type = c_string_t;

    /*
     * This is commented out as strings can at the moment not
     * be constexpr. Makeing this non-constexpr gives rise to
     * the initialization order problem of static variables.
     */
    // constexpr static std::string value = {Cs...};

    operator std::string() const
    {
      return std::string{Cs...};
    }
  };

  /*
   *
   */
  template<typename T>
  struct c_type_t {
    constexpr static literal_tag_t literal_tag = {};

    using type = T;

    constexpr c_type_t(){}; // = default;

    constexpr c_type_t(T){};
  };

  template<>
  struct c_type_t<void> {
    constexpr static literal_tag_t literal_tag = {};

    using type = void;

    constexpr c_type_t(){}; // = default;
  };

  struct c_null_t {
    constexpr static literal_tag_t literal_tag = {};

    using type = c_null_t;
  };

  /*
   *
   */
  template<typename T>
  constexpr auto is_literal(c_type_t<T> = {});

  template<typename T>
  concept Literal = is_literal<T>().value;

  template<typename T>
  constexpr auto to_literal(c_type_t<T> = {});

  template<Literal L>
  constexpr auto from_literal(L = {});

  template<typename L>
  using from_literal_t = typename decltype(from_literal<L>())::type;

  template<typename T>
  constexpr auto is_c_null(c_type_t<T> = {});

  /*
   *
   */
  template<typename T, T v>
  constexpr inline auto c_integer = c_integer_t<T, v>{};

  template<index_t v>
  constexpr inline auto c_index = c_index_t<v>{};

  template<bool v>
  constexpr inline auto c_bool = c_bool_t<v>{};

  constexpr inline c_true_t c_true   = {};
  constexpr inline c_false_t c_false = {};

  template<char... Cs>
  constexpr inline auto c_string = c_string_t<Cs...>{};

  template<typename T>
  constexpr inline auto c_type = c_type_t<T>{};

  constexpr inline c_null_t c_null = {};

  /*
   * literals
   */
  template<char... Cs>
  constexpr auto operator"" _i();

  template<char... Cs>
  constexpr auto operator"" _b();

  template<typename T, T... Cs>
  constexpr auto operator"" _b();

  template<typename T, T... Cs>
  constexpr auto operator"" _s();

  /*
   *
   */
  template<typename T, T v1, T v2>
  constexpr c_integer_t<T, v1 + v2>
  operator+(c_integer_t<T, v1>, c_integer_t<T, v2>)
  {
    return {};
  }

  template<typename T, T v>
  constexpr c_integer_t<T, -v> operator-(c_integer_t<T, v>)
  {
    return {};
  }

  template<typename T, T v1, T v2>
  constexpr c_integer_t<T, v1 - v2>
  operator-(c_integer_t<T, v1>, c_integer_t<T, v2>)
  {
    return {};
  }

  template<typename T, T v1, T v2>
  constexpr c_integer_t<T, v1 * v2>
  operator*(c_integer_t<T, v1>, c_integer_t<T, v2>)
  {
    return {};
  }

  template<typename T, T v1, T v2>
  constexpr c_integer_t<T, v1 / v2>
  operator/(c_integer_t<T, v1>, c_integer_t<T, v2>)
  {
    return {};
  }

  template<typename T, T v1, T v2>
  constexpr c_integer_t<T, v1 % v2>
  operator%(c_integer_t<T, v1>, c_integer_t<T, v2>)
  {
    return {};
  }

  template<typename T, T v1, T v2>
  constexpr c_bool_t<(v1 == v2)>
  operator==(c_integer_t<T, v1>, c_integer_t<T, v2>)
  {
    return {};
  }

  template<typename T, T v1, T v2>
  constexpr c_bool_t<(v1 != v2)>
  operator!=(c_integer_t<T, v1>, c_integer_t<T, v2>)
  {
    return {};
  }

  template<typename T, T v1, T v2>
  constexpr c_bool_t<(v1 < v2)>
  operator<(c_integer_t<T, v1>, c_integer_t<T, v2>)
  {
    return {};
  }

  template<typename T, T v1, T v2>
  constexpr c_bool_t<(v1 <= v2)>
  operator<=(c_integer_t<T, v1>, c_integer_t<T, v2>)
  {
    return {};
  }

  template<typename T, T v1, T v2>
  constexpr c_bool_t<(v1 > v2)>
  operator>(c_integer_t<T, v1>, c_integer_t<T, v2>)
  {
    return {};
  }

  template<typename T, T v1, T v2>
  constexpr c_bool_t<(v1 >= v2)>
  operator>=(c_integer_t<T, v1>, c_integer_t<T, v2>)
  {
    return {};
  }

  /*
   * bool
   */
  template<bool v1, bool v2>
  constexpr c_bool_t<(v1 && v2)> operator&&(c_bool_t<v1>, c_bool_t<v2>)
  {
    return {};
  }

  template<bool v1, bool v2>
  constexpr c_bool_t<(v1 || v2)> operator||(c_bool_t<v1>, c_bool_t<v2>)
  {
    return {};
  }

  template<bool v>
  constexpr c_bool_t<(!v)> operator!(c_bool_t<v>)
  {
    return {};
  }

  /**
   * string
   */
  template<char... Cs, char... Ds>
  constexpr c_string_t<Cs..., Ds...>
  operator+(c_string_t<Cs...>, c_string_t<Ds...>)
  {
    return {};
  }

  template<char... Cs, char... Ds>
  constexpr auto operator==(c_string_t<Cs...>, c_string_t<Ds...>);

  template<char... Cs, char... Ds>
  constexpr auto operator!=(c_string_t<Cs...>, c_string_t<Ds...>);

  /*
   *
   */
  template<typename T, typename U>
  constexpr c_bool_t<std::is_same_v<T, U>> operator==(c_type_t<T>, c_type_t<U>)
  {
    return {};
  }

  template<typename T, typename U>
  constexpr c_bool_t<!std::is_same_v<T, U>> operator!=(c_type_t<T>, c_type_t<U>)
  {
    return {};
  }

  /*
   *
   */
  constexpr inline c_true_t operator==(c_null_t, c_null_t)
  {
    return {};
  }
}

#include "literals.inl"

#endif
