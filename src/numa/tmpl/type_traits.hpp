#ifndef INCLUDE_GUARD_NUMA_TMPL_TYPE_TRAITS_H_
#define INCLUDE_GUARD_NUMA_TMPL_TYPE_TRAITS_H_

#include "fwd.hpp"
#include "literals.hpp"

namespace numa {
  /*
   *
   */
  template<typename T>
  constexpr auto is_c_string_t(c_type_t<T> = {});

  /*
   * std wrappers
   */
  template<typename T>
  constexpr auto is_signed(c_type_t<T> = {});

  template<typename T>
  constexpr auto is_const(c_type_t<T> = {});

  template<typename T>
  constexpr auto is_volatile(c_type_t<T> = {});

  template<typename T>
  constexpr auto is_reference(c_type_t<T> = {});

  template<typename T>
  constexpr auto decay(c_type_t<T> = {});

  template<typename T>
  using decay_t = typename decltype(decay<T>())::type;

  /*
   * numa specials: type predicates
   */
  template<typename T>
  constexpr auto is_value_type(c_type_t<T> = {});

  template<typename T>
  constexpr auto is_decayed(c_type_t<T> = {});

  template<typename T>
  constexpr auto is_axes_info(c_type_t<T> = {});

  template<typename T>
  constexpr auto is_vary_axes_info(c_type_t<T> = {});

  template<typename T>
  constexpr auto difference_type(c_type_t<T> = {});

  template<typename T>
  using difference_type_t = typename decltype(difference_type<T>())::type;

  template<typename T>
  constexpr auto value_type(c_type_t<T> = {});

  template<typename T>
  using value_type_t = typename decltype(value_type<T>())::type;

  template<typename T>
  constexpr auto element_type(c_type_t<T> = {});

  template<typename T>
  using element_type_t = typename decltype(element_type<T>())::type;
}

#include "type_traits.inl"

#endif
