namespace numa {
  namespace detail {
    template<typename T>
    struct is_c_string_t_impl : public c_false_t {
    };

    template<char... Cs>
    struct is_c_string_t_impl<c_string_t<Cs...>> : public c_true_t {
    };
  }

  template<typename T>
  constexpr auto is_c_string_t(c_type_t<T>)
  {
    return c_bool<detail::is_c_string_t_impl<T>::value>;
  }

  /*
   *
   */
  template<typename T>
  constexpr auto is_signed(c_type_t<T>)
  {
    return c_bool<std::is_signed_v<T>>;
  }

  template<typename T>
  constexpr auto is_const(c_type_t<T>)
  {
    return c_bool<std::is_const_v<T>>;
  }

  template<typename T>
  constexpr auto is_volatile(c_type_t<T>)
  {
    return c_bool<std::is_volatile_v<T>>;
  }

  template<typename T>
  constexpr auto is_reference(c_type_t<T>)
  {
    return c_bool<std::is_reference_v<T>>;
  }

  template<typename T>
  constexpr auto decay(c_type_t<T>)
  {
    return c_type<std::decay_t<T>>;
  }

  template<typename T>
  constexpr auto is_value_type(c_type_t<T> t)
  {
    return !is_const(t) && !is_volatile(t) && !is_reference(t);
  }

  template<typename T>
  constexpr auto is_decayed(c_type_t<T>)
  {
    return std::is_same_v<decay_t<T>, T>;
  }

  namespace detail {
    template<typename T>
    struct is_axes_info_impl : public c_false_t {
    };

    template<typename IndexList>
    struct is_axes_info_impl<axes_info<IndexList>> : public c_true_t {
    };

    template<typename T>
    struct is_vary_axes_info_impl : public c_false_t {
    };

    template<typename ModeAndIndexList>
    struct is_vary_axes_info_impl<vary_axes_info<ModeAndIndexList>>
      : public c_true_t {
    };
  }

  template<typename T>
  constexpr auto is_axes_info(c_type_t<T>)
  {
    return c_bool<detail::is_axes_info_impl<T>::value>;
  }

  template<typename T>
  constexpr auto is_vary_axes_info(c_type_t<T>)
  {
    return c_bool<detail::is_vary_axes_info_impl<T>::value>;
  }

  template<typename T>
  constexpr auto difference_type(c_type_t<T>)
  {
    return c_type<decay_t<decltype(std::declval<T>() - std::declval<T>())>>;
  }

  template<typename T>
  constexpr auto value_type(c_type_t<T>)
  {
    return c_type<typename T::value_type>;
  }

  template<typename T>
  constexpr auto element_type(c_type_t<T>)
  {
    return c_type<typename T::element_type>;
  }
}
