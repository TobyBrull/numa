#ifndef INCLUDE_GUARD_NUMA_TMPL_INTEGER_LIST_H_
#define INCLUDE_GUARD_NUMA_TMPL_INTEGER_LIST_H_

#include "type_list.hpp"

#include "numa/boot/index.hpp"

namespace numa {
  /*
   * integer-list types
   */
  template<typename T, T... Integers>
  using integer_list_t = type_list_t<c_integer_t<T, Integers>...>;

  template<index_t... Indices>
  using index_list_t = integer_list_t<index_t, Indices...>;

  template<bool... Values>
  using bool_list_t = integer_list_t<bool, Values...>;

  /*
   * Type-Traits
   */
  template<typename List>
  constexpr auto is_integer_list(c_type_t<List> = {});

  template<typename List, typename T>
  constexpr auto is_integer_list_of(c_type_t<List> = {}, c_type_t<T> = {});

  template<typename List>
  constexpr auto is_index_list(c_type_t<List> = {});

  template<typename List>
  constexpr auto is_bool_list(c_type_t<List> = {});

  /*
   * Concepts
   */
  template<typename List, typename T>
  concept IntegerList = is_integer_list<List>().value;

  template<typename List, typename T>
  concept IntegerListOf = is_integer_list_of<List, T>().value;

  template<typename List>
  concept IndexList = is_index_list<List>().value;

  template<typename List>
  concept BoolList = is_index_list<List>().value;

  /*
   *
   */
  template<typename T, T... Integers>
  constexpr inline auto integer_list = integer_list_t<T, Integers...>{};

  template<index_t... Indices>
  constexpr inline auto index_list = index_list_t<Indices...>{};

  template<bool... Values>
  constexpr inline auto bool_list = bool_list_t<Values...>{};

  /*
   * integer-list generating functions
   */
  template<typename T, index_t Size, T Value>
  constexpr auto
      integer_list_uniform(c_index_t<Size> = {}, c_integer_t<T, Value> = {});

  template<typename T, index_t Size, T Value>
  using integer_list_uniform_t =
      decltype(integer_list_uniform<T, Size, Value>());

  template<typename T, index_t Size>
  constexpr auto integer_list_iota(c_index_t<Size> = {}, c_type_t<T> = {});

  template<typename T, index_t Size>
  using integer_list_iota_t = decltype(integer_list_iota<T, Size>());

  template<typename T, index_t Size>
  constexpr auto integer_list_riota(c_index_t<Size> = {}, c_type_t<T> = {});

  template<typename T, index_t Size>
  using integer_list_riota_t = decltype(integer_list_riota<T, Size>());

  /*
   * index-list generating functions
   */
  template<index_t Size, index_t Value>
  using index_list_uniform_t = integer_list_uniform_t<index_t, Size, Value>;

  template<index_t Size, index_t Value>
  constexpr index_list_uniform_t<Size, Value>
      index_list_uniform(c_index_t<Size> = {}, c_index_t<Value> = {})
  {
    return {};
  }

  template<index_t Size>
  using index_list_iota_t = integer_list_iota_t<index_t, Size>;

  template<index_t Size>
  constexpr index_list_iota_t<Size> index_list_iota(c_index_t<Size> = {})
  {
    return {};
  }

  template<index_t Size>
  using index_list_riota_t = integer_list_riota_t<index_t, Size>;

  template<index_t Size>
  constexpr index_list_riota_t<Size> index_list_riota(c_index_t<Size> = {})
  {
    return {};
  }

  template<index_t Size>
  constexpr auto index_list_mirror(c_index_t<Size> = {});

  template<index_t Size>
  using index_list_mirror_t = decltype(index_list_mirror<Size>());

  /*
   * bool-list generating functions
   */
  template<index_t Size, bool Value>
  using bool_list_uniform_t = integer_list_uniform_t<bool, Size, Value>;

  template<index_t Size, bool Value>
  constexpr bool_list_uniform_t<Size, Value>
      bool_list_uniform(c_index_t<Size> = {}, c_bool_t<Value> = {})
  {
    return {};
  }

  /*
   * integer-list utility functions
   */
  template<typename T, T... Integers, typename F>
  constexpr auto apply(integer_list_t<T, Integers...>, F&& f);

  template<typename T, index_t N, T... Integers>
  constexpr auto plumb(c_index_t<N>, integer_list_t<T, Integers...>);

  template<typename T, T... Integers>
  constexpr auto is_sorted(integer_list_t<T, Integers...>);

  template<typename T, T... Integers>
  constexpr auto is_sorted_strictly(integer_list_t<T, Integers...>);

  template<typename T, T... Integers>
  constexpr auto is_permutation(integer_list_t<T, Integers...> = {});

  constexpr inline c_true_t is_permutation(type_list_t<> = {})
  {
    return {};
  }

  template<index_t N, typename T, T... Integers>
  constexpr auto
      is_variation(integer_list_t<T, Integers...> = {}, c_index_t<N> = {});

  template<index_t N>
  constexpr c_true_t is_variation(type_list_t<> = {}, c_index_t<N> = {})
  {
    return {};
  }

  /*
   * index-list utility functions
   */
  template<index_t... Indices>
  constexpr index<sizeof...(Indices)> as_index(index_list_t<Indices...>);

  /*
   * bool-list utility functions
   */
  template<bool... Values>
  constexpr auto any_of(bool_list_t<Values...>);

  template<bool... Values>
  constexpr auto all_of(bool_list_t<Values...>);

  template<bool... Values>
  constexpr auto none_of(bool_list_t<Values...>);

  template<bool... Values>
  constexpr auto logical_not(bool_list_t<Values...>);

  template<bool... ValuesLhs, bool... ValuesRhs>
  constexpr auto
      logical_and(bool_list_t<ValuesLhs...>, bool_list_t<ValuesRhs...>);

  template<bool... ValuesLhs, bool... ValuesRhs>
  constexpr auto
      logical_or(bool_list_t<ValuesLhs...>, bool_list_t<ValuesRhs...>);

  template<index_t N, bool... Selected>
  requires(N == sizeof...(Selected)) constexpr auto select_if(
      index<N> const& ind, bool_list_t<Selected...>);

  template<typename... Ts, bool... Selected>
  requires(sizeof...(Ts) == sizeof...(Selected)) constexpr auto select_if(
      type_list_t<Ts...>, bool_list_t<Selected...>);

  template<typename... Ts, bool... Selected>
  requires(sizeof...(Ts) == sizeof...(Selected)) constexpr auto select_if(
      std::tuple<Ts...> const& tt, bool_list_t<Selected...>);

  template<bool... Selected>
  constexpr auto count_if(bool_list_t<Selected...>);

  /*
   * auxilliary functions
   */
  template<index_t N, index_t... Axes>
  constexpr index<sizeof...(Axes)>
  select_at(index<N> const& ind, index_list_t<Axes...>);

  template<typename... Ts, index_t... Axes>
  constexpr auto select_at(type_list_t<Ts...>, index_list_t<Axes...>);

  template<typename... Ts, index_t... Axes>
  constexpr auto select_at(std::tuple<Ts...> const& tt, index_list_t<Axes...>);

  template<index_t N, index_t... Axes>
  constexpr void overwrite(
      index<N>& ind,
      index_list_t<Axes...>,
      index<sizeof...(Axes)> const& new_ind);

  template<index_t N, index_t... Axes>
  constexpr index<N> join(
      index<sizeof...(Axes)> const& ind,
      index_list_t<Axes...>,
      index<N - sizeof...(Axes)> const& jnd);

  template<index_t Na>
  constexpr auto index_list_transposition(c_index_t<Na> = {});
}

#include "integer_list.inl"

#endif
