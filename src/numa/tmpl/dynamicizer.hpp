#ifndef INCLUDE_GUARD_NUMA_TMPL_DYNAMICIZER_H_
#define INCLUDE_GUARD_NUMA_TMPL_DYNAMICIZER_H_

#include "integer_list.hpp"

#include <array>

namespace numa {
  template<index_t N, typename F, typename... Args>
  auto dynamicizer(F&& f, index_t k, Args&&... args);

  template<index_t N, typename F, std::size_t K, typename... Args>
  void dynamicizer_list(
      F&& f, std::array<index_t, K> const& index_list_t, Args&&... args);

  template<typename F, typename... Ts>
  void dynamic_tuple_at(F&& f, std::size_t ind, std::tuple<Ts...>& t);
}

#include "dynamicizer.inl"

#endif
