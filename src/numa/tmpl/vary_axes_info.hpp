#ifndef INCLUDE_GUARD_NUMA_TMPL_VARY_AXES_INFO_H_
#define INCLUDE_GUARD_NUMA_TMPL_VARY_AXES_INFO_H_

#include "axes_info.hpp"

namespace numa {
  enum class vary_axis_mode { dynamic = 0, fixed_capacity = 1, fixed_size = 2 };

  template<vary_axis_mode Modes, index_t Values>
  using vary_axis_tuple =
      std::tuple<c_integer_t<vary_axis_mode, Modes>, c_index_t<Values>>;

  template<vary_axis_mode... FieldModes, index_t... FieldValues>
  constexpr auto
      combine(type_list_t<vary_axis_tuple<FieldModes, FieldValues>...>);

  template<vary_axis_mode... Modes, index_t... Values>
  struct vary_axes_info<type_list_t<vary_axis_tuple<Modes, Values>...>> {
    using tuple_list_t = type_list_t<vary_axis_tuple<Modes, Values>...>;
    using mode_list_t  = integer_list_t<vary_axis_mode, Modes...>;
    using value_list_t = index_list_t<Values...>;

    constexpr static index_t num_axes = sizeof...(Modes);

    constexpr static tuple_list_t tuple_list = {};
    constexpr static mode_list_t mode_list   = {};
    constexpr static value_list_t value_list = {};

    constexpr static auto is_dynamic();
    constexpr static auto is_fixed_capacity();
    constexpr static auto is_fixed_size();
  };

  template<index_t Na>
  constexpr auto vary_axes_info_dynamic(c_index_t<Na> = {});

  template<index_t... Values>
  constexpr auto vary_axes_info_fixed_capacity(index_list_t<Values...> = {});

  template<index_t... Values>
  constexpr auto vary_axes_info_fixed_size(index_list_t<Values...> = {});
}

#include "vary_axes_info.inl"

#endif
