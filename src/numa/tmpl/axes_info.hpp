#ifndef INCLUDE_GUARD_NUMA_TMPL_AXES_INFO_H_
#define INCLUDE_GUARD_NUMA_TMPL_AXES_INFO_H_

#include "fwd.hpp"
#include "integer_list.hpp"

#include <algorithm>

namespace numa {
  template<index_t... Axes>
  constexpr auto is_valid_axes_info(index_list_t<Axes...> = {});

  template<index_t... Axes>
  struct axes_info<index_list_t<Axes...>> {
   private:
    constexpr static index_t num_axes_impl();

   public:
    using list_t = index_list_t<Axes...>;

    constexpr static list_t list = {};

    static_assert(
        is_valid_axes_info(list),
        "axes_info: trying to instantiate with invalid axes");

    constexpr static index_t num_axes = num_axes_impl();
    constexpr static index_t num_dims = sizeof...(Axes);

    constexpr static auto size_to_form(index<num_axes> const& size);

    template<index_t... SelectedAxes>
    constexpr static auto size_to_form(
        index_list_t<SelectedAxes...> sel_axes,
        index<sizeof...(SelectedAxes)> const& size);

    constexpr static auto form_to_size(index<num_dims> const& form);

    template<index_t... SelectedAxes>
    constexpr static auto
    select_dims(index_list_t<SelectedAxes...> sel_axes = {});

    template<index_t Axis>
    constexpr static auto dims_for(c_index_t<Axis> = {});

    template<index_t... SelectedAxes, typename TupleLike, typename Fill>
    constexpr static auto expand_tuple(
        index_list_t<SelectedAxes...>,
        TupleLike const& tuple_like,
        Fill const&
            fill) requires(sizeof...(SelectedAxes) == std::tuple_size_v<TupleLike>);
  };

  template<index_t Na>
  constexpr auto axes_info_tensor(c_index_t<Na> = {});

  template<index_t Na>
  constexpr auto axes_info_mirror_tensor(c_index_t<Na> = {});
}

#include "axes_info.inl"

#endif
