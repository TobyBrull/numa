namespace numa {
  namespace detail {
    /*
     * Implementation of FNV-1a hash for 32-bit unsigned integer.
     */
    template<std::size_t Len>
    constexpr uint32_t hash_32_fnv1a(std::array<char, Len> const& arr)
    {
      uint32_t retval = 0x811c9dc5;

      for (auto const c: arr) {
        retval ^= static_cast<uint32_t>(c);
        retval *= static_cast<uint32_t>(16777619UL);
      }

      return retval;
    }

    /*
     * Implementation of FNV-1a hash for 64-bit unsigned integer.
     */
    template<std::size_t Len>
    constexpr uint64_t hash_64_fnv1a(std::array<char, Len> const& arr)
    {
      uint64_t retval = 0xcbf29ce484222325;

      for (auto const c: arr) {
        retval ^= static_cast<uint64_t>(c);
        retval *= static_cast<uint64_t>(1099511628211ULL);
      }

      return retval;
    }
  }

  template<char... Cs>
  constexpr hash_value_t hash(c_string_t<Cs...>)
  {
    if constexpr (sizeof(hash_value_t) == 4) {
      return detail::hash_32_fnv1a<sizeof...(Cs)>({Cs...});
    }
    else if constexpr (sizeof(hash_value_t) == 8) {
      return detail::hash_64_fnv1a<sizeof...(Cs)>({Cs...});
    }
  }

  template<index_t I>
  constexpr hash_value_t clear_bit(hash_value_t value, c_index_t<I>)
  {
    return (value & ~(1ULL << I));
  }

  template<index_t I>
  constexpr hash_value_t set_bit(hash_value_t value, c_index_t<I>)
  {
    return (value | (1ULL << I));
  }

  template<index_t I>
  constexpr hash_value_t is_bit_set(hash_value_t value, c_index_t<I>)
  {
    return ((value >> I) & 1ULL);
  }
}
