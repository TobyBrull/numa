#ifndef INCLUDE_GUARD_NUMA_TMPL_HASH_H_
#define INCLUDE_GUARD_NUMA_TMPL_HASH_H_

#include "literals.hpp"

#include "numa/boot/fwd.hpp"

#include <array>

namespace numa {
  template<hash_value_t v>
  using c_hash_value_t = c_integer_t<hash_value_t, v>;

  template<hash_value_t v>
  constexpr inline auto c_hash_value = c_hash_value_t<v>{};

  /*
   *
   */
  template<char... Cs>
  constexpr hash_value_t hash(c_string_t<Cs...>);

  template<index_t I>
  constexpr hash_value_t clear_bit(hash_value_t value, c_index_t<I> = {});

  template<index_t I>
  constexpr hash_value_t set_bit(hash_value_t value, c_index_t<I> = {});

  template<index_t I>
  constexpr hash_value_t is_bit_set(hash_value_t value, c_index_t<I> = {});
}

#include "hash.inl"

#endif
