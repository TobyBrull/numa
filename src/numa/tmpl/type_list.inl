namespace numa {
  namespace detail {
    template<typename T>
    struct is_type_list_impl : public c_false_t {
    };

    template<typename... Ts>
    struct is_type_list_impl<type_list_t<Ts...>> : public c_true_t {
    };
  }

  template<typename List>
  constexpr auto is_type_list(c_type_t<List>)
  {
    return c_bool<detail::is_type_list_impl<List>::value>;
  }

  template<typename... Ts, typename... Us>
  constexpr auto operator==(type_list_t<Ts...>, type_list_t<Us...>)
  {
    if constexpr (sizeof...(Ts) == sizeof...(Us)) {
      return ((c_type<Ts> == c_type<Us>)&&...);
    }
    else {
      return c_false;
    }
  }

  template<typename... Ts, typename... Us>
  constexpr auto operator!=(type_list_t<Ts...> lhs, type_list_t<Us...> rhs)
  {
    return !(lhs == rhs);
  }

  template<typename... Ts, Literal L>
  constexpr auto append(type_list_t<Ts...>, L)
  {
    return type_list_t<Ts..., from_literal_t<L>>{};
  }

  namespace detail {
    template<typename T>
    constexpr auto concat_list_of_lists_impl(type_list_t<T> tl)
    {
      constexpr T retval{};

      return retval;
    }

    template<typename T, typename U, typename... Ts>
    constexpr auto concat_list_of_lists_impl(type_list_t<T, U, Ts...> tl)
    {
      using TU = decltype(concat(T{}, U{}));

      return concat_list_of_lists_impl(type_list_t<TU, Ts...>{});
    }
  }

  template<TypeList... Ts>
  constexpr auto concat_list_of_lists(type_list_t<Ts...> tl)
  {
    return detail::concat_list_of_lists_impl(tl);
  }

  namespace detail {
    template<typename TLRetval, typename U>
    constexpr auto erase_impl(TLRetval, c_type_t<U>, type_list_t<>)
    {
      return TLRetval{};
    }

    template<typename... Retvals, typename U, typename T, typename... Ts>
    constexpr auto
    erase_impl(type_list_t<Retvals...>, c_type_t<U> u, type_list_t<T, Ts...>)
    {
      if constexpr (std::is_same_v<T, U>) {
        return erase_impl(type_list_t<Retvals...>{}, u, type_list_t<Ts...>{});
      }
      else {
        return erase_impl(
            type_list_t<Retvals..., T>{}, u, type_list_t<Ts...>{});
      }
    }
  }

  template<typename... Ts, Literal L>
  constexpr auto erase(type_list_t<Ts...> tl, L)
  {
    return detail::erase_impl(type_list_t<>{}, c_type<from_literal_t<L>>, tl);
  }

  namespace detail {
    template<typename TLRetval>
    constexpr auto unique_impl(TLRetval, type_list_t<>)
    {
      return TLRetval{};
    }

    template<typename... Retvals, typename T, typename... Ts>
    constexpr auto unique_impl(type_list_t<Retvals...>, type_list_t<T, Ts...>)
    {
      return unique_impl(
          type_list_t<Retvals..., T>{}, erase(type_list_t<Ts...>{}, c_type<T>));
    }
  }

  template<typename... Ts>
  constexpr auto unique(type_list_t<Ts...> tl)
  {
    return detail::unique_impl(type_list_t<>{}, tl);
  }

  template<typename... Ts>
  constexpr auto is_unique(type_list_t<Ts...> tl)
  {
    return (size(unique(tl)) == size(tl));
  }

  namespace detail {
    template<index_t I, typename T, typename... Ts>
    constexpr auto get_impl(type_list_t<T, Ts...>)
    {
      if constexpr (I == 0) {
        return to_literal<T>();
      }
      else {
        static_assert(I > 0);

        return get_impl<I - 1>(type_list_t<Ts...>{});
      }
    }
  }

  template<index_t I, typename... Ts>
  constexpr auto get(type_list_t<Ts...> tl, c_index_t<I>)
  {
    return detail::get_impl<I>(tl);
  }

  template<typename... Ts>
  constexpr auto front(type_list_t<Ts...> tl)
  {
    static_assert(sizeof...(Ts) > 0, "front needs non-empty type-list");

    return detail::get_impl<0>(tl);
  }

  template<typename... Ts>
  constexpr auto back(type_list_t<Ts...> tl)
  {
    static_assert(sizeof...(Ts) > 0, "back needs non-empty type-list");

    return detail::get_impl<(sizeof...(Ts) - 1)>(tl);
  }

  template<typename... Ts, SubMode SM, index_t From, index_t SizeOrTill>
  constexpr auto
  sub(type_list_t<Ts...> tl, SM, c_index_t<From>, c_index_t<SizeOrTill>)
  {
    return for_each_indexed(tl, [](auto const ext, auto const t) {
      constexpr index_t Till =
          sub_mode_convert(SM{}, From, SizeOrTill, as_till);

      if constexpr ((From <= ext.value) && (ext.value < Till)) {
        return t;
      }
    });
  }

  template<index_t From, index_t Stride, typename... Ts>
  constexpr auto
  stride(type_list_t<Ts...> tl, c_index_t<From>, c_index_t<Stride>)
  {
    return for_each_indexed(tl, [](auto const ext, auto const t) {
      if constexpr ((From <= ext.value) && ((ext.value - From) % Stride == 0)) {
        return t;
      }
    });
  }

  namespace detail {
    template<typename U>
    constexpr auto find_impl(type_list_t<>, c_type_t<U>)
    {
      return c_null;
    }

    template<typename T, typename... Ts, typename U>
    constexpr auto find_impl(type_list_t<T, Ts...>, c_type_t<U> needle)
    {
      if constexpr (std::is_same_v<T, U>) {
        return 0_i;
      }
      else {
        constexpr auto result = find_impl(type_list_t<Ts...>{}, needle);

        if constexpr (is_c_null(decay(c_type_t{result}))) {
          return c_null;
        }
        else {
          return (result + 1_i);
        }
      }
    }
  }

  template<typename... Ts, Literal L>
  constexpr auto find(type_list_t<Ts...> const tl, L)
  {
    return detail::find_impl(tl, c_type<from_literal_t<L>>);
  }

  template<typename... Ts, typename Predicate>
  constexpr auto filter_for(type_list_t<Ts...> tl, Predicate p)
  {
    return for_each(tl, [p](auto const t) {
      auto const result = p(t);

      if constexpr (result.value) {
        return t;
      }
    });
  }

  template<typename... Ts, typename Predicate>
  constexpr auto filter_out(type_list_t<Ts...> tl, Predicate p)
  {
    return filter_for(tl, [p](auto const t) {
      auto const result = p(t);

      return c_bool<!result.value>;
    });
  }

  template<typename... Ts, typename Predicate>
  constexpr auto count_if(type_list_t<Ts...> tl, Predicate p)
  {
    return c_index<size(filter_for(tl, p))>;
  }

  template<typename... Ts, typename Predicate>
  constexpr auto any_of(type_list_t<Ts...> tl, Predicate p)
  {
    return (count_if(tl, p) > 0_i);
  }

  template<typename... Ts, typename Predicate>
  constexpr auto all_of(type_list_t<Ts...> tl, Predicate p)
  {
    return count_if(tl, p) == size(tl);
  }

  template<typename... Ts, typename Predicate>
  constexpr auto none_of(type_list_t<Ts...> tl, Predicate p)
  {
    return (count_if(tl, p) == 0_i);
  }

  template<typename... Ts, typename... Us>
  constexpr auto
  contains(type_list_t<Ts...> haystack, type_list_t<Us...> needle)
  {
    constexpr auto tl = for_each(haystack, [&](auto elem) {
      if constexpr (((elem == to_literal<Us>()) || ...)) {
        return c_true;
      }
    });

    return !is_empty(tl);
  }

  template<typename... Ts, Literal L>
  constexpr auto contains(type_list_t<Ts...> tl, L)
  {
    return contains(tl, type_list<from_literal_t<L>>);
  }

  namespace detail {
    template<index_t I, typename F, typename T, typename... Ts, typename... Us>
    constexpr auto for_each_indexed_impl(
        c_index_t<I> ext, type_list_t<T, Ts...>, F&& f, type_list_t<Us...>)
    {
      f(ext, to_literal<T>());

      using L = decltype(f(ext, to_literal<T>()));

      static_assert(
          (c_type<L> == c_type<void>) || Literal<L>,
          "functions passed to for-each(-indexed) must return "
          "void or a literal");

      constexpr auto retval = [] {
        if constexpr (c_type<L> == c_type<void>) {
          return c_type<type_list_t<Us...>>;
        }
        else {
          return c_type<type_list_t<Us..., from_literal_t<L>>>;
        }
      }();

      using Retval = typename decltype(retval)::type;

      if constexpr (sizeof...(Ts) > 0) {
        return for_each_indexed_impl(
            (ext + 1_i), type_list_t<Ts...>{}, std::forward<F>(f), Retval{});
      }
      else {
        return Retval{};
      }
    }
  }

  template<typename F, typename... Ts>
  constexpr auto for_each(type_list_t<Ts...> tl, F&& f)
  {
    return for_each_indexed(tl, [&f](auto const, auto t) { return f(t); });
  }

  template<typename F, typename... Ts>
  constexpr auto for_each_indexed(type_list_t<Ts...> tl, F&& f)
  {
    if constexpr (sizeof...(Ts) > 0) {
      return detail::for_each_indexed_impl(
          0_i, tl, std::forward<F>(f), type_list_t<>{});
    }
    else {
      return type_list_t<>{};
    }
  }

  namespace detail {
    template<std::size_t... Is, typename F, typename TupleLike>
    void tuple_for_each_indexed_impl(
        std::index_sequence<Is...>, F&& f, TupleLike& tuple_like)
    {
      (f(c_integer<std::size_t, Is>, std::get<Is>(tuple_like)), ...);
    }
  }

  template<typename F, typename TupleLike>
  constexpr void tuple_for_each(F&& f, TupleLike& tuple_like)
  {
    tuple_for_each_indexed(
        [&](auto const i, auto&& elem) {
          f(std::forward<decltype(elem)>(elem));
        },
        tuple_like);
  }

  template<typename F, typename TupleLike>
  constexpr void tuple_for_each_indexed(F&& f, TupleLike& tuple_like)
  {
    detail::tuple_for_each_indexed_impl(
        std::make_index_sequence<std::tuple_size_v<TupleLike>>(),
        std::forward<F>(f),
        tuple_like);
  }
}
