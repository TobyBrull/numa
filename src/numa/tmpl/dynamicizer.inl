namespace numa {
  namespace detail {
    template<index_t N>
    struct dynamicizer_impl {
      template<typename F, typename... Args>
      static auto apply(F&& f, index_t const k, Args&&... args)
      {
        if constexpr (N > 1) {
          if (k == (N - 1)) {
            return std::forward<F>(f).template operator()<N - 1>(
                std::forward<Args>(args)...);
          }
          else {
            return dynamicizer_impl<N - 1>::apply(
                std::forward<F>(f), k, std::forward<Args>(args)...);
          }
        }
        else {
          static_assert(N == 1);

          return std::forward<F>(f).template operator()<0>(
              std::forward<Args>(args)...);
        }
      }
    };
  }

  template<index_t N, typename F, typename... Args>
  auto dynamicizer(F&& f, index_t const k, Args&&... args)
  {
    ASSERT((0 <= k) && (k < N));

    return detail::dynamicizer_impl<N>::apply(
        std::forward<F>(f), k, std::forward<Args>(args)...);
  }

  namespace detail {
    template<index_t N, index_t L, typename IndexList>
    struct dynamicizer_list_impl;

    template<index_t N, index_t L, index_t... Indices>
    struct dynamicizer_list_impl<N, L, index_list_t<Indices...>> {
      struct functor {
        template<index_t K, typename... Args>
        void operator()(Args&&... args)
        {
          using NewIndexList =
              decltype(concat(index_list<Indices...>, index_list<K>));

          dynamicizer_list_impl<N, L + 1, NewIndexList>::apply(
              std::forward<Args>(args)...);
        }
      };

      template<typename F, std::size_t K, typename... Args>
      static void
      apply(F&& f, std::array<index_t, K> const& list, Args&&... args) requires(
          L <= K)
      {
        if constexpr (L == K) {
          std::forward<F>(f).template operator()<index_list_t<Indices...>>(
              std::forward<Args>(args)...);
        }
        else {
          dynamicizer<N>(
              functor{},
              list[L],
              std::forward<F>(f),
              list,
              std::forward<Args>(args)...);
        }
      }
    };
  }

  template<index_t N, typename F, std::size_t K, typename... Args>
  void
  dynamicizer_list(F&& f, std::array<index_t, K> const& list, Args&&... args)
  {
    if constexpr (K > 0) {
      for (index_t const i: list) {
        (void)i;

        ASSERT((0 <= i) && (i < N));
      }

      detail::dynamicizer_list_impl<N, 0, index_list_t<>>::apply(
          std::forward<F>(f), list, std::forward<Args>(args)...);
    }
  }

  namespace detail {
    template<std::size_t I, typename F, typename T>
    void conditional_call(F&& f, std::size_t const ind, T& val)
    {
      if (I == ind) {
        std::forward<F>(f)(val);
      }
    }

    template<typename F, std::size_t... Is, typename... Ts>
    void dynamic_tuple_at_impl(
        F&& f,
        std::size_t const ind,
        std::index_sequence<Is...>,
        std::tuple<Ts...>& t)
    {
      ((conditional_call<Is>(std::forward<F>(f), ind, std::get<Is>(t))), ...);
    }
  }

  template<typename F, typename... Ts>
  void dynamic_tuple_at(F&& f, std::size_t ind, std::tuple<Ts...>& t)
  {
    detail::dynamic_tuple_at_impl(
        std::forward<F>(f), ind, std::make_index_sequence<sizeof...(Ts)>(), t);
  }
}
