#ifndef INCLUDE_GUARD_NUMA_CORE_TYPE_LIST_H_
#define INCLUDE_GUARD_NUMA_CORE_TYPE_LIST_H_

#include "literals.hpp"

#include "numa/boot/modes.hpp"

#include <tuple>
#include <type_traits>

namespace numa {
  template<typename... Ts>
  struct type_list_t {
    constexpr type_list_t(){}; // = default;

    constexpr type_list_t(Ts...) requires(sizeof...(Ts) > 0) {}
  };

  type_list_t()->type_list_t<>;

  template<typename... Ts>
  constexpr inline auto type_list = type_list_t<Ts...>{};

  template<typename List>
  constexpr auto is_type_list(c_type_t<List> = {});

  template<typename List>
  concept TypeList = is_type_list<List>().value;

  template<typename... Ts, typename... Us>
  constexpr auto operator==(type_list_t<Ts...>, type_list_t<Us...>);

  template<typename... Ts, typename... Us>
  constexpr auto operator!=(type_list_t<Ts...>, type_list_t<Us...>);

  template<typename... Ts>
  constexpr c_index_t<sizeof...(Ts)> size(type_list_t<Ts...> = {})
  {
    return {};
  }

  template<typename... Ts>
  constexpr c_bool_t<sizeof...(Ts) == 0> is_empty(type_list_t<Ts...> = {})
  {
    return {};
  }

  template<typename... Ts, Literal L>
  constexpr auto append(type_list_t<Ts...>, L);

  template<typename... Ts, typename... Us>
  constexpr type_list_t<Ts..., Us...>
      concat(type_list_t<Ts...> = {}, type_list_t<Us...> = {})
  {
    return {};
  }

  template<TypeList... Ts>
  constexpr auto concat_list_of_lists(type_list_t<Ts...>);

  template<typename... Ts, Literal L>
  constexpr auto erase(type_list_t<Ts...>, L);

  template<typename... Ts>
  constexpr auto unique(type_list_t<Ts...> = {});

  template<typename... Ts>
  constexpr auto is_unique(type_list_t<Ts...> = {});

  template<index_t I, typename... Ts>
  constexpr auto get(type_list_t<Ts...>, c_index_t<I> = {});

  template<typename... Ts>
  constexpr auto front(type_list_t<Ts...>);

  template<typename... Ts>
  constexpr auto back(type_list_t<Ts...>);

  template<typename... Ts, SubMode SM, index_t From, index_t SizeOrTill>
  constexpr auto
      sub(type_list_t<Ts...>, SM, c_index_t<From>, c_index_t<SizeOrTill>);

  template<index_t From, index_t Stride, typename... Ts>
  constexpr auto
      stride(type_list_t<Ts...>, c_index_t<From> = {}, c_index_t<Stride> = {});

  template<typename... Ts, Literal L>
  constexpr auto find(type_list_t<Ts...>, L);

  template<typename... Ts, typename Predicate>
  constexpr auto filter_for(type_list_t<Ts...>, Predicate p);

  template<typename... Ts, typename Predicate>
  constexpr auto filter_out(type_list_t<Ts...>, Predicate p);

  template<typename... Ts, typename Predicate>
  constexpr auto count_if(type_list_t<Ts...>, Predicate p);

  template<typename... Ts, typename Predicate>
  constexpr auto any_of(type_list_t<Ts...>, Predicate p);

  template<typename... Ts, typename Predicate>
  constexpr auto all_of(type_list_t<Ts...>, Predicate p);

  template<typename... Ts, typename Predicate>
  constexpr auto none_of(type_list_t<Ts...>, Predicate p);

  template<typename... Ts, typename... Us>
  constexpr auto contains(type_list_t<Ts...>, type_list_t<Us...>);

  template<typename... Ts, Literal L>
  constexpr auto contains(type_list_t<Ts...>, L);

  template<typename F, typename... Ts>
  constexpr auto for_each(type_list_t<Ts...>, F&& f);

  template<typename F, typename... Ts>
  constexpr auto for_each_indexed(type_list_t<Ts...>, F&& f);
}

#include "type_list.inl"

#endif
