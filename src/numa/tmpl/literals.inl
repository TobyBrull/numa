namespace numa {
  namespace detail {
    template<typename T>
    concept HasLiteralTag = requires()
    {
      {
        T::literal_tag
      }
      ->std::convertible_to<literal_tag_t>;

      typename T::type;
    };

    template<typename T>
    struct is_literal_impl : public c_bool_t<HasLiteralTag<T>> {
    };

    template<typename T, typename U>
    struct is_literal_impl<std::pair<T, U>>
      : public c_bool_t<(is_literal<T>() && is_literal<U>()).value> {
    };

    template<typename... Ts>
    struct is_literal_impl<std::tuple<Ts...>>
      : public c_bool_t<((is_literal<Ts>()) && ...).value> {
    };

    template<typename L>
    struct from_literal_impl {
      static_assert(HasLiteralTag<L>);

      constexpr static c_type_t<typename L::type> result = {};
    };

    template<typename T, typename U>
    struct from_literal_impl<std::pair<T, U>> {
      static_assert(is_literal<T>() && is_literal<U>());

      constexpr static c_type_t<std::pair<T, U>> result = {};
    };

    template<typename... Ts>
    struct from_literal_impl<std::tuple<Ts...>> {
      static_assert(((is_literal<Ts>()) && ...));

      constexpr static c_type_t<std::tuple<Ts...>> result = {};
    };
  }

  template<typename T>
  constexpr auto is_literal(c_type_t<T>)
  {
    return c_bool<detail::is_literal_impl<T>::value>;
  }

  template<typename T>
  constexpr auto to_literal(c_type_t<T> t)
  {
    if constexpr (is_literal(t)) {
      return T{};
    }
    else {
      return t;
    }
  }

  template<Literal L>
  constexpr auto from_literal(L)
  {
    return detail::from_literal_impl<L>::result;
  }

  template<typename T>
  constexpr auto is_c_null(c_type_t<T> t)
  {
    return (t == c_type<c_null_t>);
  }

  namespace detail {
    template<typename IntType>
    constexpr IntType parse_char(char const c)
    {
      if (c >= '0' && c <= '9') {
        return static_cast<IntType>(c - '0');
      }
      else {
        THROW("Invalid character ", c, " when parsing compile-time integer");
      }
    }

    template<typename IntType>
    constexpr IntType parse_integer(IntType p)
    {
      return p;
    }

    template<typename IntType, typename T, typename... Ts>
    constexpr IntType parse_integer(IntType val, T c0, Ts... cs)
    {
      return parse_integer(val * 10 + parse_char<IntType>(c0), cs...);
    }

    template<typename... Ts>
    constexpr std::optional<bool> parse_bool_impl(Ts... cs)
    {
      if constexpr (sizeof...(Ts) == 1) {
        if ((std::tuple(cs...) == std::tuple('0')) ||
            (std::tuple(cs...) == std::tuple('f'))) {
          return false;
        }
        else if (
            (std::tuple(cs...) == std::tuple('1')) ||
            (std::tuple(cs...) == std::tuple('t'))) {
          return true;
        }
      }
      else if constexpr (sizeof...(Ts) == 5) {
        if (std::tuple(cs...) == std::tuple('f', 'a', 'l', 's', 'e')) {
          return false;
        }
      }
      else if constexpr (sizeof...(Ts) == 4) {
        if (std::tuple(cs...) == std::tuple('t', 'r', 'u', 'e')) {
          return true;
        }
      }

      return std::nullopt;
    }

    template<typename... Ts>
    constexpr bool parse_bool(Ts... cs)
    {
      auto const retval = parse_bool_impl(cs...);

      if (retval.has_value()) {
        return retval.value();
      }
      else {
        THROW("Invalid character sequence when parsing compile-time bool");
      }
    }
  }

  template<char... Cs>
  constexpr auto operator"" _i()
  {
    return c_index<detail::parse_integer<index_t>(0, Cs...)>;
  }

  template<char... Cs>
  constexpr auto operator"" _b()
  {
    return c_bool<detail::parse_bool(Cs...)>;
  }

  template<typename T, T... Cs>
  constexpr auto operator"" _b()
  {
    static_assert(std::is_same_v<T, char>);

    return c_bool<detail::parse_bool(Cs...)>;
  }

  template<typename T, T... Cs>
  constexpr auto operator"" _s()
  {
    return c_string<Cs...>;
  }

  template<char... Cs, char... Ds>
  constexpr auto operator==(c_string_t<Cs...>, c_string_t<Ds...>)
  {
    if constexpr (sizeof...(Cs) == sizeof...(Ds)) {
      return c_bool<((Cs == Ds) && ...)>;
    }
    else {
      return c_bool<false>;
    }
  }

  template<char... Cs, char... Ds>
  constexpr auto operator!=(c_string_t<Cs...> s1, c_string_t<Ds...> s2)
  {
    return !(s1 == s2);
  }
}
