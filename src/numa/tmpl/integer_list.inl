namespace numa {
  namespace detail {
    template<typename List>
    struct is_integer_list_impl : public c_false_t {
    };

    template<typename T, T... Integers>
    struct is_integer_list_impl<integer_list_t<T, Integers...>>
      : public c_true_t {
    };
  }

  template<typename List>
  constexpr auto is_integer_list(c_type_t<List>)
  {
    return c_bool<detail::is_integer_list_impl<List>::value>;
  }

  namespace detail {
    template<typename T, typename List>
    struct is_integer_list_of_impl : public c_false_t {
    };

    template<typename T, T... Integers>
    struct is_integer_list_of_impl<T, integer_list_t<T, Integers...>>
      : public c_true_t {
    };
  }

  template<typename List, typename T>
  constexpr auto is_integer_list_of(c_type_t<List>, c_type_t<T>)
  {
    return c_bool<detail::is_integer_list_of_impl<T, List>::value>;
  }

  template<typename List>
  constexpr auto is_index_list(c_type_t<List> ll)
  {
    return is_integer_list_of(ll, c_type<index_t>);
  }

  template<typename List>
  constexpr auto is_bool_list(c_type_t<List> ll)
  {
    return is_integer_list_of(ll, c_type<bool>);
  }

  /*
   * integer-list generating functions
   */
  namespace detail {
    template<index_t... Is, typename T, T Integer>
    constexpr auto integer_list_uniform_impl(
        std::integer_sequence<index_t, Is...>, c_integer_t<T, Integer> value)
    {
      return type_list_t(((void)Is, value)...);
    }
  }

  template<typename T, index_t Size, T Integer>
  constexpr auto
  integer_list_uniform(c_index_t<Size> size, c_integer_t<T, Integer> value)
  {
    if constexpr (Size == 0) {
      return type_list_t<>{};
    }
    else {
      return detail::integer_list_uniform_impl(
          std::make_integer_sequence<index_t, Size>(), value);
    }
  }

  namespace detail {
    template<typename T, T... Is>
    constexpr auto integer_list_iota_impl(std::integer_sequence<T, Is...>)
    {
      return type_list_t<c_integer_t<T, Is>...>{};
    }
  }

  template<typename T, index_t Size>
  constexpr auto integer_list_iota(c_index_t<Size>, c_type_t<T>)
  {
    return detail::integer_list_iota_impl<T>(
        std::make_integer_sequence<T, Size>());
  }

  namespace detail {
    template<typename T, T... Is>
    constexpr auto integer_list_riota_impl(std::integer_sequence<T, Is...>)
    {
      return type_list_t<c_integer_t<T, sizeof...(Is) - 1 - Is>...>{};
    }
  }

  template<typename T, index_t Size>
  constexpr auto integer_list_riota(c_index_t<Size>, c_type_t<T>)
  {
    return detail::integer_list_riota_impl<T>(
        std::make_integer_sequence<T, Size>());
  }

  template<index_t Size>
  constexpr auto index_list_mirror(c_index_t<Size> size)
  {
    return concat(index_list_iota(size), index_list_iota(size));
  }

  /*
   * utility functions
   */
  template<typename T, T... Integers, typename F>
  constexpr auto apply(integer_list_t<T, Integers...>, F&& f)
  {
    return std::forward<F>(f)(Integers...);
  }

  template<typename T, index_t N, T... Integers>
  constexpr auto plumb(c_index_t<N>, integer_list_t<T, Integers...>)
  {
    return for_each(integer_list_iota<T, N>(), [](auto const ax) {
      constexpr integer_list_t<T, Integers...> selected;

      if constexpr (!contains(selected, type_list_t(ax))) {
        return ax;
      }
    });
  }

  namespace detail {
    template<typename T, T... Integers, typename BinaryPredicate>
    constexpr bool
    is_pairwise(integer_list_t<T, Integers...> list, BinaryPredicate)
    {
      constexpr auto tl =
          for_each_indexed(list, [](auto const ind, auto const val) {
            if constexpr (ind.value + 1 < sizeof...(Integers)) {
              constexpr integer_list_t<T, Integers...> list;

              constexpr auto val_rhs = get(list, ind + 1_i);

              if constexpr (!BinaryPredicate{}(val, val_rhs)) {
                return c_false;
              }
            }
          });

      return is_empty(tl);
    }

    struct FunctorLessEqual {
      template<typename T, typename U>
      constexpr auto operator()(T const& lhs, U const& rhs) const
      {
        return (lhs <= rhs);
      }
    };

    struct FunctorLess {
      template<typename T, typename U>
      constexpr auto operator()(T const& lhs, U const& rhs) const
      {
        return (lhs < rhs);
      }
    };
  }

  template<typename T, T... Integers>
  constexpr auto is_sorted(integer_list_t<T, Integers...> list)
  {
    return c_bool<detail::is_pairwise(list, detail::FunctorLessEqual{})>;
  }

  template<typename T, T... Integers>
  constexpr auto is_sorted_strictly(integer_list_t<T, Integers...> list)
  {
    return c_bool<detail::is_pairwise(list, detail::FunctorLess{})>;
  }

  template<typename T, T... Integers>
  constexpr auto is_permutation(integer_list_t<T, Integers...> il)
  {
    return is_variation<sizeof...(Integers), T, Integers...>();
  }

  namespace detail {
    template<index_t N, typename T, T... Integers>
    struct is_variation_impl;

    template<index_t N, typename T>
    struct is_variation_impl<N, T> {
      constexpr static bool apply()
      {
        return true;
      }
    };

    template<index_t N, typename T, T Integer, T... Integers>
    struct is_variation_impl<N, T, Integer, Integers...> {
      constexpr static bool apply()
      {
        if constexpr (Integer >= N) {
          return false;
        }
        else {
          constexpr bool c =
              contains(integer_list<T, Integers...>, integer_list<T, Integer>);

          if constexpr (c) {
            return false;
          }
          else {
            return is_variation_impl<N, T, Integers...>::apply();
          }
        }
      }
    };
  }

  template<index_t N, typename T, T... Integers>
  constexpr auto is_variation(integer_list_t<T, Integers...>, c_index_t<N>)
  {
    return c_bool<detail::is_variation_impl<N, T, Integers...>::apply()>;
  }

  /*
   * index-list utility functions
   */
  template<index_t... Indices>
  constexpr index<sizeof...(Indices)> as_index(index_list_t<Indices...>)
  {
    return index<sizeof...(Indices)>(Indices...);
  }

  /*
   * bool-list utility functions
   */
  template<bool... Values>
  constexpr auto any_of(bool_list_t<Values...>)
  {
    return c_bool<(Values || ...)>;
  }

  template<bool... Values>
  constexpr auto all_of(bool_list_t<Values...>)
  {
    return c_bool<(Values && ...)>;
  }

  template<bool... Values>
  constexpr auto none_of(bool_list_t<Values...>)
  {
    return c_bool<((!Values) && ...)>;
  }

  template<bool... Values>
  constexpr auto logical_not(bool_list_t<Values...>)
  {
    return bool_list<(!Values)...>;
  }

  template<bool... ValuesLhs, bool... ValuesRhs>
  constexpr auto
      logical_and(bool_list_t<ValuesLhs...>, bool_list_t<ValuesRhs...>)
  {
    return bool_list<(ValuesLhs && ValuesRhs)...>;
  }

  template<bool... ValuesLhs, bool... ValuesRhs>
  constexpr auto
      logical_or(bool_list_t<ValuesLhs...>, bool_list_t<ValuesRhs...>)
  {
    return bool_list<(ValuesLhs || ValuesRhs)...>;
  }

  template<index_t N, bool... Selected>
  requires(N == sizeof...(Selected)) constexpr auto select_if(
      index<N> const& ind, bool_list_t<Selected...> bl)
  {
    return select_at(ind, select_if(index_list_iota<N>(), bl));
  }

  template<typename... Ts, bool... Selected>
  requires(sizeof...(Ts) == sizeof...(Selected)) constexpr auto select_if(
      type_list_t<Ts...> tl, bool_list_t<Selected...> bl)
  {
    static_assert(
        sizeof...(Ts) == sizeof...(Selected),
        "select_if: type-list and bool-list must have same size");

    return for_each_indexed(tl, [](auto const ext, auto const t) {
      if constexpr (numa::get<ext.value>(bool_list<Selected...>).value) {
        return t;
      }
    });
  }

  template<typename... Ts, bool... Selected>
  requires(sizeof...(Ts) == sizeof...(Selected)) constexpr auto select_if(
      std::tuple<Ts...> const& tt, bool_list_t<Selected...> bl)
  {
    return select_at(tt, select_if(index_list_iota<sizeof...(Ts)>(), bl));
  }

  template<bool... Selected>
  constexpr auto count_if(bool_list_t<Selected...> bl)
  {
    return count_if(bl, [](auto x) { return x; });
  }

  template<index_t N, index_t... Axes>
  constexpr index<sizeof...(Axes)>
  select_at(index<N> const& ind, index_list_t<Axes...> axes)
  {
    index<sizeof...(Axes)> retval = zero;

    for_each_indexed(axes, [&](auto const num_ax, auto const ax) {
      static_assert(ax.value < N);

      retval[num_ax.value] = ind[ax.value];
    });

    return retval;
  }

  template<typename... Ts, index_t... Axes>
  constexpr auto select_at(type_list_t<Ts...> tl, index_list_t<Axes...> inds)
  {
    return for_each(
        inds, [&](auto const ind) { return numa::get<ind.value>(tl); });
  }

  template<typename... Ts, index_t... Axes>
  constexpr auto select_at(std::tuple<Ts...> const& tt, index_list_t<Axes...>)
  {
    if constexpr (sizeof...(Axes) == 0) {
      return std::tuple<>();
    }
    else {
      return std::tuple(std::get<Axes>(tt)...);
    }
  }

  template<index_t N, index_t... Axes>
  constexpr void overwrite(
      index<N>& ind,
      index_list_t<Axes...> axes,
      index<sizeof...(Axes)> const& new_ind)
  {
    for_each_indexed(axes, [&](auto const num_ax, auto const ax) {
      static_assert(ax.value < N);

      ind[ax.value] = new_ind[num_ax.value];
    });
  }

  template<index_t N, index_t... Axes>
  constexpr index<N> join(
      index<sizeof...(Axes)> const& ind,
      index_list_t<Axes...> axes,
      index<N - sizeof...(Axes)> const& jnd)
  {
    index<N> retval = zero;

    for_each_indexed(axes, [&](auto const num_ax, auto const ax) {
      retval[ax.value] = ind[num_ax.value];
    });

    for_each_indexed(
        plumb<index_t>(c_index<N>, axes),
        [&](auto const num_ax, auto const ax) {
          retval[ax.value] = jnd[num_ax.value];
        });

    return retval;
  }

  template<index_t Na>
  constexpr auto index_list_transposition(c_index_t<Na> ext)
  {
    constexpr auto eli = index_list_iota(ext);

    constexpr auto eli_plus_Na =
        for_each(eli, [](auto const ax) { return (ax + c_index<Na>); });

    return concat(eli_plus_Na, eli);
  }
}
