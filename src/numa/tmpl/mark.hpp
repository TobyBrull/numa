#ifndef INCLUDE_GUARD_NUMA_TMPL_MARK_H_
#define INCLUDE_GUARD_NUMA_TMPL_MARK_H_

#include "type_list.hpp"

#define DEFINE_MARK(mark_name)                                      \
  struct m_##mark_name##_t : public numa::mark_base {               \
    constexpr static literal_tag_t literal_tag = {};                \
    using type                                 = m_##mark_name##_t; \
    constexpr static auto name                 = #mark_name ""_s;   \
  };                                                                \
  constexpr inline m_##mark_name##_t m_##mark_name = {};            \
  static_assert(                                                    \
      !contains(numa::reserved_marks, c_type_t(#mark_name ""_s)),   \
      "This name is not allowed for a mark");

namespace numa {
  struct mark_base {
  };

  constexpr inline auto reserved_marks = type_list_t("size"_s);

  template<typename T>
  constexpr auto is_mark(c_type_t<T> = {});

  template<typename T, typename U>
  requires((is_mark<T>() && is_mark<U>()).value) constexpr auto
  operator==(T, U);

  template<typename T, typename U>
  requires((is_mark<T>() && is_mark<U>()).value) constexpr auto
  operator!=(T, U);

  DEFINE_MARK(invalid);

  DEFINE_MARK(main);
  DEFINE_MARK(data);

  DEFINE_MARK(rows);
  DEFINE_MARK(cols);
  DEFINE_MARK(lays);
  DEFINE_MARK(vals);

  DEFINE_MARK(a);
  DEFINE_MARK(b);
  DEFINE_MARK(c);
  DEFINE_MARK(d);
  DEFINE_MARK(e);
  DEFINE_MARK(f);
  DEFINE_MARK(g);
  DEFINE_MARK(h);
  DEFINE_MARK(i);
  DEFINE_MARK(j);
  DEFINE_MARK(k);
  DEFINE_MARK(l);
  DEFINE_MARK(m);
  DEFINE_MARK(n);
  DEFINE_MARK(o);
  DEFINE_MARK(p);
  DEFINE_MARK(q);
  DEFINE_MARK(r);
  DEFINE_MARK(s);
  DEFINE_MARK(t);
  DEFINE_MARK(u);
  DEFINE_MARK(v);
  DEFINE_MARK(w);
  DEFINE_MARK(x);
  DEFINE_MARK(y);
  DEFINE_MARK(z);

  DEFINE_MARK(A);
  DEFINE_MARK(B);
  DEFINE_MARK(C);
  DEFINE_MARK(D);
  DEFINE_MARK(E);
  DEFINE_MARK(F);
  DEFINE_MARK(G);
  DEFINE_MARK(H);
  DEFINE_MARK(I);
  DEFINE_MARK(J);
  DEFINE_MARK(K);
  DEFINE_MARK(L);
  DEFINE_MARK(M);
  DEFINE_MARK(N);
  DEFINE_MARK(O);
  DEFINE_MARK(P);
  DEFINE_MARK(Q);
  DEFINE_MARK(R);
  DEFINE_MARK(S);
  DEFINE_MARK(T);
  DEFINE_MARK(U);
  DEFINE_MARK(V);
  DEFINE_MARK(W);
  DEFINE_MARK(X);
  DEFINE_MARK(Y);
  DEFINE_MARK(Z);
}

#include "mark.inl"

#endif
