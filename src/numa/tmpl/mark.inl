namespace numa {
  template<typename T>
  constexpr auto is_mark(c_type_t<T>)
  {
    return c_bool<std::is_base_of<mark_base, T>::value>;
  }

  template<typename T, typename U>
  requires((is_mark<T>() && is_mark<U>()).value) constexpr auto operator==(T, U)
  {
    return c_bool<std::is_same_v<T, U>>;
  }

  template<typename T, typename U>
  requires((is_mark<T>() && is_mark<U>()).value) constexpr auto operator!=(T, U)
  {
    return !(T{} == U{});
  }
}
