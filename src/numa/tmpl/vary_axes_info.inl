namespace numa {
  namespace detail {
    template<
        vary_axis_mode FieldMode,
        vary_axis_mode... FieldModes,
        index_t FieldValue,
        index_t... FieldValues>
    constexpr index_t
        find_first_fixed_size(type_list_t<
                              vary_axis_tuple<FieldMode, FieldValue>,
                              vary_axis_tuple<FieldModes, FieldValues>...>)
    {
      if constexpr (FieldMode == vary_axis_mode::fixed_size) {
        return FieldValue;
      }
      else {
        static_assert(sizeof...(FieldModes) > 0);

        return find_first_fixed_size(
            type_list_t<vary_axis_tuple<FieldModes, FieldValues>...>{});
      }
    }

    template<
        vary_axis_mode FieldMode,
        vary_axis_mode... FieldModes,
        index_t FieldValue,
        index_t... FieldValues,
        index_t Size>
    constexpr void check_fixed_size_valid(
        type_list_t<
            vary_axis_tuple<FieldMode, FieldValue>,
            vary_axis_tuple<FieldModes, FieldValues>...>,
        c_index_t<Size> size)
    {
      if constexpr (FieldMode == vary_axis_mode::fixed_size) {
        static_assert(
            Size == FieldValue,
            "combine: fixed-size axes with different sizes");
      }
      else if constexpr (FieldMode == vary_axis_mode::fixed_capacity) {
        static_assert(
            Size <= FieldValue,
            "combine: fixed-size axes with "
            "fixed-capacity axis of insufficient capacity");
      }

      if constexpr (sizeof...(FieldModes) > 0) {
        check_fixed_size_valid(
            type_list_t<vary_axis_tuple<FieldModes, FieldValues>...>{}, size);
      }
    }

    template<
        vary_axis_mode FieldMode,
        vary_axis_mode... FieldModes,
        index_t FieldValue,
        index_t... FieldValues>
    constexpr index_t find_min_fixed_capacity(
        type_list_t<
            vary_axis_tuple<FieldMode, FieldValue>,
            vary_axis_tuple<FieldModes, FieldValues>...>,
        index_t retval)
    {
      if constexpr (FieldMode == vary_axis_mode::fixed_capacity) {
        if (retval < 0) {
          retval = FieldValue;
        }
        else {
          retval = std::min(retval, FieldValue);
        }
      }

      if constexpr (sizeof...(FieldModes) > 0) {
        return find_min_fixed_capacity(
            type_list_t<vary_axis_tuple<FieldModes, FieldValues>...>{}, retval);
      }
      else {
        return retval;
      }
    }
  }

  template<vary_axis_mode... FieldModes, index_t... FieldValues>
  constexpr auto
  combine(type_list_t<vary_axis_tuple<FieldModes, FieldValues>...> list)
  {
    if constexpr (((FieldModes == vary_axis_mode::fixed_size) || ...)) {
      constexpr index_t retval = detail::find_first_fixed_size(list);

      detail::check_fixed_size_valid(list, c_index<retval>);

      return vary_axis_tuple<vary_axis_mode::fixed_size, retval>{};
    }
    else if constexpr (((FieldModes == vary_axis_mode::fixed_capacity) ||
                        ...)) {
      constexpr index_t retval = detail::find_min_fixed_capacity(list, -1);

      return vary_axis_tuple<vary_axis_mode::fixed_capacity, retval>{};
    }
    else {
      return vary_axis_tuple<vary_axis_mode::dynamic, 0>{};
    }
  }

  template<vary_axis_mode... Modes, index_t... Values>
  constexpr auto
  vary_axes_info<type_list_t<vary_axis_tuple<Modes, Values>...>>::is_dynamic()
  {
    return bool_list<(Modes == vary_axis_mode::dynamic)...>;
  }

  template<vary_axis_mode... Modes, index_t... Values>
  constexpr auto vary_axes_info<
      type_list_t<vary_axis_tuple<Modes, Values>...>>::is_fixed_capacity()
  {
    return bool_list<(Modes == vary_axis_mode::fixed_capacity)...>;
  }

  template<vary_axis_mode... Modes, index_t... Values>
  constexpr auto vary_axes_info<
      type_list_t<vary_axis_tuple<Modes, Values>...>>::is_fixed_size()
  {
    return bool_list<(Modes == vary_axis_mode::fixed_size)...>;
  }

  namespace detail {
    template<index_t... Zeros>
    constexpr auto vary_axes_info_dynamic_impl(index_list_t<Zeros...>)
    {
      return vary_axes_info<
          type_list_t<vary_axis_tuple<vary_axis_mode::dynamic, Zeros>...>>{};
    }
  }

  template<index_t Na>
  constexpr auto vary_axes_info_dynamic(c_index_t<Na> ext)
  {
    return detail::vary_axes_info_dynamic_impl(index_list_uniform(ext, 0_i));
  }

  template<index_t... Values>
  constexpr auto vary_axes_info_fixed_capacity(index_list_t<Values...>)
  {
    return vary_axes_info<type_list_t<
        vary_axis_tuple<vary_axis_mode::fixed_capacity, Values>...>>{};
  }

  template<index_t... Values>
  constexpr auto vary_axes_info_fixed_size(index_list_t<Values...>)
  {
    return vary_axes_info<
        type_list_t<vary_axis_tuple<vary_axis_mode::fixed_size, Values>...>>{};
  }
}
