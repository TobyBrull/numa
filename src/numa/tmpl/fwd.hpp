#ifndef INCLUDE_GUARD_NUMA_TMPL_FWD_H_
#define INCLUDE_GUARD_NUMA_TMPL_FWD_H_

namespace numa {
  template<typename IndexList>
  struct axes_info;

  template<typename ModeAndIndexList>
  struct vary_axes_info;
}

#endif
