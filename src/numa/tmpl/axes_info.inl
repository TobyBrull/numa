namespace numa {
  template<index_t... Axes>
  constexpr auto is_valid_axes_info(index_list_t<Axes...> list)
  {
    if constexpr (sizeof...(Axes) == 0) {
      return c_true;
    }
    else {
      constexpr auto temp_1 = erase(list, 0_i);

      if constexpr (size(temp_1) == size(list)) {
        return c_false;
      }
      else {
        constexpr auto temp_2 =
            for_each(temp_1, [](auto const ext) { return (ext - 1_i); });

        return is_valid_axes_info(temp_2);
      }
    }
  }

  template<index_t... Axes>
  constexpr index_t axes_info<index_list_t<Axes...>>::num_axes_impl()
  {
    if constexpr (sizeof...(Axes) > 0) {
      return apply(list, [](auto... vals) { return std::max({vals...}); }) + 1;
    }
    else {
      return 0;
    }
  }

  template<index_t... Axes>
  template<index_t Axis>
  constexpr auto axes_info<index_list_t<Axes...>>::dims_for(c_index_t<Axis> ax)
  {
    return for_each_indexed(list, [](auto const ind, auto ai) {
      if constexpr (ai == c_index<Axis>) {
        return ind;
      }
    });
  }

  template<index_t... Axes>
  requires(is_valid_axes_info(index_list_t<Axes...>{})) constexpr index_list_t<
      Axes...> make_axes_info(c_index_t<Axes>...)
  {
    return {};
  }

  template<index_t... Axes>
  constexpr auto
  axes_info<index_list_t<Axes...>>::form_to_size(index<num_dims> const& form)
  {
    index<num_axes> retval = zero;

    for_each_indexed(list, [&](auto const ind, auto const ax) {
      retval[ax.value] = form[ind.value];
    });

    for_each_indexed(list, [&](auto const ind, auto const ax) {
      THROW_IF_NOT(
          retval[ax.value] == form[ind.value],
          "form_to_size: converting size back to form "
          "would give different result");
    });

    return retval;
  }

  template<index_t... Axes>
  constexpr auto
  axes_info<index_list_t<Axes...>>::size_to_form(index<num_axes> const& size)
  {
    index<num_dims> retval = zero;

    for_each_indexed(list, [&](auto const dm, auto const ax) {
      retval[dm.value] = size[ax.value];
    });

    return retval;
  }

  namespace detail {
    template<index_t Axis, index_t... Axes>
    constexpr index_t lookup(
        c_index_t<Axis> ax,
        index_list_t<Axes...> axes,
        index<sizeof...(Axes)> const& ind)
    {
      constexpr auto find_ind = find(axes, c_type_t(ax));

      static_assert(!is_c_null(decay(c_type_t{find_ind})));

      return ind[find_ind.value];
    }
  }

  template<index_t... Axes>
  template<index_t... SelectedAxes>
  constexpr auto axes_info<index_list_t<Axes...>>::size_to_form(
      index_list_t<SelectedAxes...> sel_axes,
      index<sizeof...(SelectedAxes)> const& size)
  {
    constexpr auto sel_dims = select_dims(sel_axes);
    constexpr index_t Kd    = numa::size(sel_dims);

    index<Kd> retval = zero;

    constexpr auto dims_and_axes =
        for_each_indexed(list, [](auto const dm, auto const ax) {
          constexpr index_list_t<SelectedAxes...> sel_axes;

          if constexpr (contains(sel_axes, type_list_t(ax))) {
            return ax;
          }
        });

    for_each_indexed(dims_and_axes, [&](auto const ind, auto const ax) {
      constexpr index_list_t<SelectedAxes...> sel_axes;

      retval[ind.value] = detail::lookup(ax, sel_axes, size);
    });

    return retval;
  }

  template<index_t... Axes>
  template<index_t... SelAxes>
  constexpr auto
      axes_info<index_list_t<Axes...>>::select_dims(index_list_t<SelAxes...>)
  {
    return for_each_indexed(list, [](auto const dm, auto const ax) {
      if constexpr (contains(index_list<SelAxes...>, type_list_t(ax))) {
        return dm;
      }
    });
  }

  namespace detail {
    template<index_t... Is, typename... Ts, typename F>
    constexpr auto for_each_indexed_to_tuple_impl(
        std::integer_sequence<index_t, Is...>, type_list_t<Ts...>, F&& f)
    {
      return std::tuple(f(c_index<Is>, Ts{})...);
    }

    template<typename F, typename... Ts>
    constexpr auto for_each_indexed_to_tuple(type_list_t<Ts...> tl, F&& f)
    {
      if constexpr (sizeof...(Ts) > 0) {
        return for_each_indexed_to_tuple_impl(
            std::make_integer_sequence<index_t, sizeof...(Ts)>(),
            tl,
            std::forward<F>(f));
      }
      else {
        return std::tuple<>{};
      }
    }
  }

  template<index_t... Axes>
  template<index_t... SelectedAxes, typename TupleLike, typename Fill>
  constexpr auto axes_info<index_list_t<Axes...>>::expand_tuple(
      index_list_t<SelectedAxes...>,
      TupleLike const& tuple_like,
      Fill const&
          fill) requires(sizeof...(SelectedAxes) == std::tuple_size_v<TupleLike>)
  {
    return detail::for_each_indexed_to_tuple(
        list, [&](auto const dm, auto const ax) {
          constexpr index_list_t<SelectedAxes...> selected;

          constexpr auto found = find(selected, c_type_t{ax});

          if constexpr (!is_c_null(decay(c_type_t{found}))) {
            return get<found.value>(tuple_like);
          }
          else {
            return fill;
          }
        });
  }

  template<index_t Na>
  constexpr auto axes_info_tensor(c_index_t<Na>)
  {
    return axes_info<index_list_iota_t<Na>>{};
  }

  template<index_t Na>
  constexpr auto axes_info_mirror_tensor(c_index_t<Na>)
  {
    return axes_info<index_list_mirror_t<Na>>{};
  }
}
