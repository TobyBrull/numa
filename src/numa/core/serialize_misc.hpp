#ifndef INCLUDE_GUARD_NUMA_CORE_SERIALIZE_MISC_H_
#define INCLUDE_GUARD_NUMA_CORE_SERIALIZE_MISC_H_

#include "numa/boot/error.hpp"

#include <iomanip>
#include <iosfwd>

namespace numa {
  char peek_till_non_whitespace(std::istream& is);

  template<typename Predicate>
  std::string read_while(std::istream& is, Predicate&&);

  inline void read_till(std::istream& is, char const d);

  inline void forward_read_till(std::istream& is, std::string_view const sv);
}

#include "serialize_misc.inl"

#endif
