namespace numa {
  /*
   * a_binary
   */
  template<typename T, typename FBinary>
  a_binary<T, FBinary>::a_binary(T initial_state, FBinary&& f)
    : state_(std::move(initial_state)), f_(std::forward<FBinary>(f))
  {
  }

  template<typename T, typename FBinary>
  template<typename U>
  void a_binary<T, FBinary>::operator()(U const& value)
  {
    state_ = f_(state_, value);
  }

  template<typename T, typename FBinary>
  T const& a_binary<T, FBinary>::get() const
  {
    return state_;
  }

  /*
   * a_binary derived
   */
  template<typename T>
  a_plus<T>::a_plus(T initial_state)
    : a_binary<T, f_plus_t>(std::move(initial_state))
  {
  }

  template<typename T>
  a_times<T>::a_times(T initial_state)
    : a_binary<T, f_times_t>(std::move(initial_state))
  {
  }

  template<typename T>
  a_min<T>::a_min(T initial_state)
    : a_binary<T, f_min_t>(std::move(initial_state))
  {
    static_assert(std::numeric_limits<T>::has_infinity);
  }

  template<typename T>
  a_max<T>::a_max(T initial_state)
    : a_binary<T, f_max_t>(std::move(initial_state))
  {
    static_assert(std::numeric_limits<T>::has_infinity);
  }

  /*
   * a_abs_max
   */
  template<typename T>
  a_abs_max<T>::a_abs_max(T initial_state) : state_(std::move(initial_state))
  {
  }

  template<typename T>
  template<typename U>
  void a_abs_max<T>::operator()(U const& value)
  {
    using std::abs;
    using std::max;
    state_ = max(state_, abs(value));
  }

  template<typename T>
  template<typename U, typename V>
  void a_abs_max<T>::operator()(U const& u, V const& v)
  {
    (*this)(u - v);
  }

  template<typename T>
  T const& a_abs_max<T>::get() const
  {
    return state_;
  }

  /*
   * a_forbenius
   */
  template<typename T>
  a_frobenius<T>::a_frobenius(T initial_state)
    : state_(std::move(initial_state))
  {
  }

  template<typename T>
  template<typename U>
  void a_frobenius<T>::operator()(U const& value)
  {
    state_ += (value * value);
  }

  template<typename T>
  template<typename U, typename V>
  void a_frobenius<T>::operator()(U const& u, V const& v)
  {
    (*this)(u - v);
  }

  template<typename T>
  T a_frobenius<T>::get() const
  {
    using std::sqrt;
    return sqrt(state_);
  }
}
