namespace numa {
  template<
      typename FBinary,
      typename Retval,
      typename T_1,
      typename T_2,
      typename... Ts>
  constexpr auto pairwise_conjunction_impl_2(
      FBinary const& f,
      Retval const& retval,
      T_1 const& value_1,
      T_2 const& value_2,
      Ts const&... values)
  {
    if (!retval) {
      return retval;
    }
    else {
      auto new_retval = retval && f(value_1, value_2);

      if constexpr (sizeof...(Ts) == 0) {
        return new_retval;
      }
      else {
        return pairwise_conjunction_impl_2(f, new_retval, value_2, values...);
      }
    }
  }

  template<typename FBinary, typename T_1, typename T_2, typename... Ts>
  constexpr auto pairwise_conjunction_impl(
      FBinary const& f,
      T_1 const& value_1,
      T_2 const& value_2,
      Ts const&... values)
  {
    auto retval = f(value_1, value_2);

    if constexpr (sizeof...(Ts) == 0) {
      return retval;
    }
    else {
      return pairwise_conjunction_impl_2(f, retval, value_2, values...);
    }
  }

#define NUMA_DEF_BINARY_PRED_IMPL(name, functor)                        \
  template<typename... Ts>                                              \
  requires(sizeof...(Ts) >= 2) constexpr auto f_##name##_t::operator()( \
      Ts&&... values) const                                             \
  {                                                                     \
    return pairwise_conjunction_impl(                                   \
        functor<>{}, std::forward<Ts>(values)...);                      \
  }

  NUMA_DEF_BINARY_PRED_IMPL(is_less, std::less);
  NUMA_DEF_BINARY_PRED_IMPL(is_less_equal, std::less_equal);
  NUMA_DEF_BINARY_PRED_IMPL(is_greater, std::greater);
  NUMA_DEF_BINARY_PRED_IMPL(is_greater_equal, std::greater_equal);
  NUMA_DEF_BINARY_PRED_IMPL(is_equal, std::equal_to);

  template<typename T, typename U>
  constexpr auto f_is_not_equal_t::operator()(T&& lhs, U&& rhs) const
  {
    return (std::forward<T>(lhs) != std::forward<U>(rhs));
  }

#undef NUMA_DEF_BINARY_PRED_IMPL

#define NUMA_DEF_UNARY_PRED_IMPL(name, functor)               \
  template<typename U>                                        \
  template<typename T>                                        \
  constexpr auto f_##name##_t<U>::operator()(T&& value) const \
  {                                                           \
    return functor(std::forward<T>(value), state_);           \
  }

  NUMA_DEF_UNARY_PRED_IMPL(is_less_than, f_is_less);
  NUMA_DEF_UNARY_PRED_IMPL(is_less_equal_to, f_is_less_equal);
  NUMA_DEF_UNARY_PRED_IMPL(is_greater_than, f_is_greater);
  NUMA_DEF_UNARY_PRED_IMPL(is_greater_equal_to, f_is_greater_equal);
  NUMA_DEF_UNARY_PRED_IMPL(is_equal_to, f_is_equal);
  NUMA_DEF_UNARY_PRED_IMPL(is_not_equal_to, f_is_not_equal);

#undef NUMA_DEF_UNARY_PRED_IMPL

  template<typename T>
  constexpr auto f_id_t::operator()(T&& value) const
  {
    return std::forward<T>(value);
  }

  template<typename... Ts>
  requires(sizeof...(Ts) >= 1) constexpr auto f_plus_t::operator()(
      Ts&&... values) const
  {
    return (... + std::forward<Ts>(values));
  }

  template<typename... Ts>
  requires(sizeof...(Ts) >= 1) constexpr auto f_times_t::operator()(
      Ts&&... values) const
  {
    return (... * std::forward<Ts>(values));
  }

  template<typename T, typename U>
  constexpr auto f_minus_t::operator()(T&& lhs, U&& rhs) const
  {
    return (std::forward<T>(lhs) - std::forward<U>(rhs));
  }

  template<typename T, typename U>
  constexpr auto f_divided_t::operator()(T&& lhs, U&& rhs) const
  {
    return (std::forward<T>(lhs) / std::forward<U>(rhs));
  }

  template<typename T, typename U>
  constexpr auto f_modulus_t::operator()(T&& lhs, U&& rhs) const
  {
    return (std::forward<T>(lhs) % std::forward<U>(rhs));
  }

  template<typename T>
  constexpr auto f_negate_t::operator()(T&& lhs) const
  {
    return -std::forward<T>(lhs);
  }

  template<
      typename ResultType,
      typename Compare,
      typename T_1,
      typename T_2,
      typename... Ts>
  constexpr ResultType pairwise_select_impl(
      Compare const comp,
      T_1 const& value_1,
      T_2 const& value_2,
      Ts const&... values)
  {
    if constexpr (sizeof...(Ts) == 0) {
      if (comp(value_2, value_1)) {
        return value_2;
      }
      else {
        return value_1;
      }
    }
    else {
      if (comp(value_2, value_1)) {
        return pairwise_select_impl<ResultType>(comp, value_2, values...);
      }
      else {
        return pairwise_select_impl<ResultType>(comp, value_1, values...);
      }
    }
  }

  template<typename Compare, typename... Ts>
  constexpr auto pairwise_select(Compare const comp, Ts const&... values)
  {
    if constexpr (sizeof...(Ts) >= 2) {
      using ResultType = std::common_type_t<Ts...>;

      return pairwise_select_impl<ResultType>(comp, values...);
    }
    else {
      return [](auto const& val) {
        return val;
      }(values...);
    }
  }

  template<typename... Ts>
  requires(sizeof...(Ts) >= 1) constexpr auto f_min_t::operator()(
      Ts&&... values) const
  {
    return pairwise_select(std::less<>(), std::forward<Ts>(values)...);
  }

  template<typename... Ts>
  requires(sizeof...(Ts) >= 1) constexpr auto f_max_t::operator()(
      Ts&&... values) const
  {
    return pairwise_select(std::greater<>(), std::forward<Ts>(values)...);
  }

  template<typename T>
  constexpr auto f_div(x_t, T divisor)
  {
    return [divisor = std::move(divisor)](auto&& dividend) constexpr
    {
      return (std::forward<decltype(dividend)>(dividend) / divisor);
    };
  }

  template<typename T>
  constexpr auto f_div(T dividend, x_t)
  {
    return [dividend = std::move(dividend)](auto&& divisor) constexpr
    {
      return (dividend / std::forward<decltype(divisor)>(divisor));
    };
  }

  template<typename T>
  constexpr auto f_pow(x_t, T exponent)
  {
    return [exponent = std::move(exponent)](auto&& base) constexpr
    {
      return std::pow(std::forward<decltype(base)>(base), exponent);
    };
  }

  template<typename T>
  constexpr auto f_pow(T base, x_t)
  {
    return [base = std::move(base)](auto&& exponent) constexpr
    {
      return std::pow(base, std::forward<decltype(exponent)>(exponent));
    };
  }

#define NUMA_MAKE_MATH_FUNCTOR_IMPL(name)                  \
  template<typename T>                                     \
  constexpr auto f_##name##_t::operator()(T&& value) const \
  {                                                        \
    return std::name(std::forward<T>(value));              \
  }

  NUMA_MAKE_MATH_FUNCTOR_IMPL(sin);
  NUMA_MAKE_MATH_FUNCTOR_IMPL(cos);

  NUMA_MAKE_MATH_FUNCTOR_IMPL(abs);
  NUMA_MAKE_MATH_FUNCTOR_IMPL(arg);
  NUMA_MAKE_MATH_FUNCTOR_IMPL(norm);
  NUMA_MAKE_MATH_FUNCTOR_IMPL(polar);

  namespace detail {
    struct conj_impl {
      template<typename T>
      requires(!contains(type_list<double, float>, c_type<decay_t<T>>)
                    .value) constexpr static auto apply(T&& value)
      {
        return std::conj(std::forward<T>(value));
      }

      template<typename T>
      requires(contains(type_list<double, float>, c_type<decay_t<T>>)
                   .value) constexpr static auto apply(T&& value)
      {
        return value;
      }
    };
  }

  template<typename T>
  constexpr auto f_conj_t::operator()(T&& value) const
  {
    return detail::conj_impl::apply(std::forward<T>(value));
  }

  NUMA_MAKE_MATH_FUNCTOR_IMPL(exp);
  NUMA_MAKE_MATH_FUNCTOR_IMPL(exp2);
  NUMA_MAKE_MATH_FUNCTOR_IMPL(expm1);

  NUMA_MAKE_MATH_FUNCTOR_IMPL(log);
  NUMA_MAKE_MATH_FUNCTOR_IMPL(log2);
  NUMA_MAKE_MATH_FUNCTOR_IMPL(log1p);

  NUMA_MAKE_MATH_FUNCTOR_IMPL(sqrt);

#undef NUMA_MAKE_MATH_FUNCTOR_IMPL

  template<typename T>
  constexpr auto f_square_t::operator()(T&& value) const
  {
    return (value * value);
  }

  template<typename T, typename U>
  constexpr auto f_swap_t::operator()(T&& lhs, U&& rhs) const
  {
    std::swap(std::forward<T>(lhs), std::forward<U>(rhs));
  }

  template<typename... Ts>
  constexpr auto f_no_op_t::operator()(Ts&&... targets) const
  {
    no_op;
  }

  template<int N>
  template<typename... Ts>
  requires(sizeof...(Ts) > N) constexpr decltype(auto) f_get_t<N>::operator()(
      Ts&&... values) const
  {
    return std::get<N>(std::forward_as_tuple<Ts...>(values...));
  }

  template<int N>
  template<typename... Ts>
  constexpr decltype(auto)
  f_get_t<N>::operator()(std::tuple<Ts...> const& tt) const
  {
    return std::get<N>(tt);
  }

  template<int N>
  template<typename... Ts>
  constexpr decltype(auto) f_get_t<N>::operator()(std::tuple<Ts...>& tt) const
  {
    return std::get<N>(tt);
  }

  template<typename T>
  constexpr f_const_t<T>::f_const_t(T value) : value_(std::move(value))
  {
  }

  template<typename T>
  template<typename... Ts>
  constexpr T const& f_const_t<T>::operator()(Ts&&...) const
  {
    return value_;
  }

#define NUMA_IMPL_FUNCTOR_ASSIGN_WRAPPER(name, op)                             \
  template<typename F>                                                         \
  constexpr f_##name##_t<F>::f_##name##_t(F&& f) : f_(std::forward<F>(f))      \
  {                                                                            \
  }                                                                            \
                                                                               \
  template<typename F>                                                         \
  template<typename T, typename... Ts>                                         \
  constexpr void f_##name##_t<F>::operator()(T&& target, Ts&&... params) const \
  {                                                                            \
    std::forward<T>(target) op f_(std::forward<Ts>(params)...);                \
  }                                                                            \
                                                                               \
  template<typename F>                                                         \
  template<typename T, typename... Ts>                                         \
  constexpr void f_##name##_t<F>::operator()(T&& target, Ts&&... params)       \
  {                                                                            \
    std::forward<T>(target) op f_(std::forward<Ts>(params)...);                \
  }

  NUMA_IMPL_FUNCTOR_ASSIGN_WRAPPER(assign, =);
  NUMA_IMPL_FUNCTOR_ASSIGN_WRAPPER(assign_plus, +=);
  NUMA_IMPL_FUNCTOR_ASSIGN_WRAPPER(assign_times, *=);
  NUMA_IMPL_FUNCTOR_ASSIGN_WRAPPER(assign_minus, -=);
  NUMA_IMPL_FUNCTOR_ASSIGN_WRAPPER(assign_divided, /=);
  NUMA_IMPL_FUNCTOR_ASSIGN_WRAPPER(assign_modulus, %=);

#undef NUMA_IMPL_FUNCTOR_ASSIGN_WRAPPER
}
