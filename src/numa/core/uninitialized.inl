namespace numa {
  template<typename T>
  struct is_initializer : public std::false_type {
  };

  template<typename TargetT, typename... Args>
  struct is_initializer<initializer<TargetT, Args...>> : public std::true_type {
  };

  template<typename T>
  inline constexpr bool is_initializer_v = is_initializer<T>::value;

  template<typename TargetT, typename... Args>
  auto init(Args... args)
  {
    return initializer<TargetT, Args...>{
        std::tuple<Args...>(std::move(args)...)};
  }

  template<typename... Args>
  auto init_all(Args... args)
  {
    return initializer<void, Args...>{std::tuple<Args...>(std::move(args)...)};
  }

  template<typename Mark, typename... Args>
  requires(is_mark(c_type<Mark>).value) auto init(Mark, Args... args)
  {
    return initializer<Mark, Args...>{std::tuple<Args...>(std::move(args)...)};
  }

  template<typename T>
  concept Initializer = is_initializer_v<T>;

  namespace detail {
    template<
        typename MarkType,
        typename ValueType,
        Initializer Initer,
        Initializer... Initers>
    auto initializer_for_impl(Initer const& initer, Initers const&... initers)
    {
      using target_type = typename Initer::target_type;

      if constexpr (
          std::is_same_v<MarkType, target_type> ||
          std::is_same_v<ValueType, target_type> ||
          std::is_same_v<void, target_type>) {
        return initer.args_tuple;
      }
      else if constexpr (sizeof...(Initers) > 0) {
        return initializer_for_impl<MarkType, ValueType>(initers...);
      }
      else {
        return std::tuple<>();
      }
    }
  }

  template<typename MarkType, typename ValueType, typename... Initers>
  auto initializer_for_tuple(std::tuple<Initers const&...> initers_tuple)
  {
    return std::apply(
        [](auto const&... initers) {
          return initializer_for<MarkType, ValueType>(initers...);
        },
        initers_tuple);
  }

  template<typename MarkType, typename ValueType, typename... Initers>
  auto initializer_for(Initers const&... initers)
  {
    if constexpr (sizeof...(Initers) == 0) {
      return std::tuple<>{};
    }
    else if constexpr ((is_initializer_v<Initers> && ...)) {
      return detail::initializer_for_impl<MarkType, ValueType>(initers...);
    }
    else {
      static_assert(
          (!is_initializer_v<Initers> && ...),
          "For variadic initializer pack, all initers must be of the "
          "form init<T> (...) or none (but not a mixture of both)");

      return initializer_for<MarkType, ValueType>(init_all(initers...));
    }
  }

  template<typename... Args>
  constexpr bool is_uninitialized()
  {
    return std::is_same_v<std::tuple<Args...>, std::tuple<uninitialized_t>>;
  }

  namespace detail {
    template<typename T>
    concept has_uninitialized_constructor = requires()
    {
      {T(uninitialized)};
    };
  }

  template<typename T, typename... Args>
  void fill_assign(T& target, Args const&... args)
  {
    if constexpr (!is_uninitialized<Args...>()) {
      target = T(args...);
    }
  }

  template<typename T, typename... Args>
  void fill_construct(T& target, Args const&... args)
  {
    if constexpr (is_uninitialized<Args...>()) {
      if constexpr (detail::has_uninitialized_constructor<T>) {
        new (std::addressof(target)) T(uninitialized);
      }
      else {
        new (std::addressof(target)) T;
      }
    }
    else {
      if constexpr (std::is_fundamental_v<T>) {
        new (std::addressof(target)) T{args...};
      }
      else {
        new (std::addressof(target)) T(args...);
      }
    }
  }

  template<typename T>
  void copy_assign(T& target, T const& source)
  {
    target = source;
  }

  template<typename T>
  void copy_construct(T& target, T const& source)
  {
    new (std::addressof(target)) T(source);
  }

  template<typename T>
  requires(!std::is_reference_v<T>) void move_assign(T& target, T& source)
  {
    target = std::move(source);
  }

  template<typename T>
  requires(!std::is_reference_v<T>) void move_construct(T& target, T& source)
  {
    new (std::addressof(target)) T(std::move(source));
  }

  template<typename T>
  requires(!std::is_reference_v<T>) void destruct(T& target)
  {
    target.~T();
  }

  /*
   * Implementation of functors
   */

  template<typename... Args>
  f_fill_assign_t<Args...>::f_fill_assign_t(Args const&... args)
    : args_tuple_(args...)
  {
  }

  template<typename... Args>
  template<typename T>
  requires(!std::is_reference_v<T>) void f_fill_assign_t<Args...>::operator()(
      T& target) const
  {
    std::apply(
        [&target](auto const&... args) { numa::fill_assign(target, args...); },
        args_tuple_);
  }

  template<typename... Args>
  f_fill_construct_t<Args...>::f_fill_construct_t(Args const&... args)
    : args_tuple_(args...)
  {
  }

  template<typename... Args>
  template<typename T>
  requires(!std::is_reference_v<T>) void f_fill_construct_t<
      Args...>::operator()(T& target) const
  {
    std::apply(
        [&target](auto const&... args) {
          numa::fill_construct(target, args...);
        },
        args_tuple_);
  }

  template<typename T>
  void f_copy_assign_t::operator()(T& target, T const& source) const
  {
    numa::copy_assign(target, source);
  }

  template<typename T>
  void f_copy_construct_t::operator()(T& target, T const& source) const
  {
    numa::copy_construct(target, source);
  }

  template<typename T>
  requires(!std::is_reference_v<T>) void f_move_assign_t::operator()(
      T& target, T& source) const
  {
    numa::move_assign(target, source);
  }

  template<typename T>
  requires(!std::is_reference_v<T>) void f_move_construct_t::operator()(
      T& target, T& source) const
  {
    numa::move_construct(target, source);
  }

  template<typename T, typename... Ts>
  requires(!std::is_reference_v<T>) void f_destruct_t::operator()(
      T& target, Ts const&...) const
  {
    numa::destruct(target);
  }
}
