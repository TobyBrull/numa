#ifndef INCLUDE_GUARD_NUMA_CORE_ACCUMULATORS_H_
#define INCLUDE_GUARD_NUMA_CORE_ACCUMULATORS_H_

#include "functors.hpp"

namespace numa {
  template<typename Acc, typename T>
  concept Accumulator = requires(Acc& acc, T val)
  {
    {acc(val)};
    {acc.get()};
  };

  template<typename T, typename FBinary>
  class a_binary {
   public:
    a_binary(T initial_state, FBinary&& f = FBinary{});

    template<typename U>
    void operator()(U const& value);

    T const& get() const;

   private:
    T state_;
    FBinary f_;
  };

  template<typename T>
  class a_plus : public a_binary<T, f_plus_t> {
   public:
    a_plus(T initial_state = T{0});
  };

  template<typename T>
  class a_times : public a_binary<T, f_times_t> {
   public:
    a_times(T initial_state = T{1});
  };

  template<typename T>
  class a_min : public a_binary<T, f_min_t> {
   public:
    a_min(T initial_state = std::numeric_limits<T>::infinity());
  };

  template<typename T>
  class a_max : public a_binary<T, f_max_t> {
   public:
    a_max(T initial_state = -std::numeric_limits<T>::infinity());
  };

  template<typename T>
  class a_abs_max {
   public:
    a_abs_max(T initial_state = T{0});

    template<typename U>
    void operator()(U const& value);

    template<typename U, typename V>
    void operator()(U const& u, V const& v);

    T const& get() const;

   private:
    T state_;
  };

  template<typename T>
  class a_frobenius {
   public:
    a_frobenius(T initial_state = T{0});

    template<typename U>
    void operator()(U const& value);

    template<typename U, typename V>
    void operator()(U const& u, V const& v);

    T get() const;

   private:
    T state_;
  };
}

#include "accumulators.inl"

#endif
