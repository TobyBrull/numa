#ifndef INCLUDE_GUARD_NUMA_CORE_RANGE_H_
#define INCLUDE_GUARD_NUMA_CORE_RANGE_H_

#include "numa/boot/fwd.hpp"

#include <functional>

namespace numa {
  /**
   * Returns a functor that checks if the I-th element of the
   * passed argument is equal to @p value.
   */
  template<int I, typename T>
  auto element_find(T const& value);

  /**
   * Returns a functor that compares the I-th elements of the
   * passed arguments using @p comp.
   */
  template<int I, typename Compare>
  auto element_compare(Compare comp);

  /**
   * A range represented by two iterators.
   */
  template<typename BeginIterator, typename EndIterator = BeginIterator>
  class range {
   public:
    range(BeginIterator begin, EndIterator end);

    BeginIterator begin() const;
    EndIterator end() const;

    index_t size() const;

    bool is_empty() const;

    std::pair<BeginIterator, EndIterator> as_tuple() const&;
    std::pair<BeginIterator, EndIterator> as_tuple() &&;

   private:
    BeginIterator begin_;
    EndIterator end_;
  };
}

#include "range.inl"

#endif
