namespace numa {
  template<typename ForwardRange, typename... ForwardRanges>
  bool equal(ForwardRange const& range, ForwardRanges const&... ranges)
  {
    using std::begin;
    using std::end;
    using std::size;

    if (((static_cast<index_t>(size(range)) !=
          static_cast<index_t>(size(ranges))) ||
         ...)) {
      return false;
    }

    std::tuple iters(begin(range), begin(ranges)...);

    auto end_it = end(range);

    while (std::get<0>(iters) != end_it) {
      bool const all_equal = std::apply(
          [](auto& it, auto&... its) {
            bool const retval = (((*it) == (*its)) && ...);

            ++it;
            (++its, ...);

            return retval;
          },
          iters);

      if (!all_equal) {
        return false;
      }
    }

    return true;
  }

  namespace detail {
    template<typename... ForwardRanges>
    struct ForEachTupleRetType {
      std::tuple<ForwardRanges...> ranges_;

      template<typename F>
      void operator()(F&& f) const
      {
        using std::begin;
        using std::end;

        auto iters = std::apply(
            [&](auto&... ranges) { return std::tuple{begin(ranges)...}; },
            ranges_);

        auto end_it = std::apply(
            [&](auto& range, auto&...) { return end(range); }, ranges_);

        while (std::get<0>(iters) != end_it) {
          std::apply(
              [&f](auto&... its) {
                f(*its...);

                (++its, ...);
              },
              iters);
        }
      }
    };
  }

  template<typename... ForwardRanges>
  auto for_each_tuple(ForwardRanges&&... ranges)
  {
    return detail::ForEachTupleRetType<ForwardRanges...>{
        {std::forward<ForwardRanges>(ranges)...}};
  }

  namespace detail {
    template<typename... ForwardRanges>
    struct ForEachCombinationRetType {
      std::tuple<ForwardRanges...> ranges_;

      template<index_t I, typename F, typename... Ts>
      void apply(F& f, Ts&... vals) const
      {
        if constexpr (I == sizeof...(ForwardRanges)) {
          f(vals...);
        }
        else {
          for (auto& x: std::get<I>(ranges_)) {
            apply<I + 1>(f, vals..., x);
          }
        }
      }

      template<typename F>
      void operator()(F&& f) const
      {
        apply<0>(f);
      }
    };
  }

  template<typename... ForwardRanges>
  auto for_each_combination(ForwardRanges&&... ranges)
  {
    return detail::ForEachCombinationRetType<ForwardRanges...>{
        {std::forward<ForwardRanges>(ranges)...}};
  }

  template<typename RandomAccessRange, typename T>
  auto find(RandomAccessRange& range, T const& value)
  {
    using std::begin;
    using std::end;

    return std::find(begin(range), end(range), value);
  }

  template<typename RandomAccessRange, typename UnaryPredicate>
  auto find_if(RandomAccessRange& range, UnaryPredicate const p)
  {
    using std::begin;
    using std::end;

    return std::find_if(begin(range), end(range), p);
  }

  template<typename RandomAccessRange, typename T>
  auto lower_bound(RandomAccessRange& range, T const& value)
  {
    using std::begin;
    using std::end;

    return std::lower_bound(begin(range), end(range), value);
  }

  template<typename RandomAccessRange>
  void sort(RandomAccessRange& range)
  {
    using std::begin;
    using std::end;

    std::sort(begin(range), end(range));
  }

  template<typename RandomAccessRange, typename Compare>
  void sort(RandomAccessRange& range, Compare const comp)
  {
    using std::begin;
    using std::end;

    std::sort(begin(range), end(range), comp);
  }

  template<typename ForwardRange, typename Compare>
  bool is_sorted_strictly(ForwardRange const& range, Compare const comp)
  {
    using std::begin;
    using std::end;

    auto it           = begin(range);
    auto const end_it = end(range);

    if (it == end_it) {
      return true;
    }

    auto prev = it;
    ++it;

    for (; it != end_it; ++prev, ++it) {
      if (!comp(*prev, *it)) {
        return false;
      }
    }

    return true;
  }

  template<
      typename ForwardRange1,
      typename ForwardRange2,
      typename Compare,
      typename FOutput,
      typename FOutput2>
  void combine_sorted_strictly(
      ForwardRange1 const& strictly_sorted_range_1,
      ForwardRange2 const& strictly_sorted_range_2,
      Compare const comp,
      FOutput&& f,
      FOutput2&& f2)
  {
    auto it_1 = strictly_sorted_range_1.begin();
    auto it_2 = strictly_sorted_range_2.begin();

    auto const end_1 = strictly_sorted_range_1.end();
    auto const end_2 = strictly_sorted_range_2.end();

    while ((it_1 != end_1) || (it_2 != end_2)) {
      if (it_1 == end_1) {
        f(*it_2);
        ++it_2;
      }
      else if (it_2 == end_2) {
        f(*it_1);
        ++it_1;
      }
      else {
        auto const& val_1 = (*it_1);
        auto const& val_2 = (*it_2);

        if (comp(val_1, val_2)) {
          f(val_1);
          ++it_1;
        }
        else if (comp(val_2, val_1)) {
          f(val_2);
          ++it_2;
        }
        else {
          f2(val_1, val_2);
          ++it_1;
          ++it_2;
        }
      }
    }
  }
}
