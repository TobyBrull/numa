namespace numa {
  template<typename T>
  auto g_iota(T init)
  {
    return [current = std::move(init)](auto&&...) mutable {
      auto retval = current;
      ++current;
      return retval;
    };
  }

  template<typename T>
  auto g_riota(T init)
  {
    return [current = std::move(init)](auto&&...) mutable {
      auto retval = current;
      --current;
      return retval;
    };
  }

  template<typename Engine, typename Distribution>
  auto g_random(Engine& engine, Distribution distribution)
  {
    return
        [&engine, distribution = std::move(distribution)](auto&&...) mutable {
          return distribution(engine);
        };
  }

  template<typename T, typename Engine>
  auto g_random_normal(Engine& engine, T mean, T stddev)
  {
    return g_random<Engine, std::normal_distribution<T>>(
        engine,
        std::normal_distribution<T>(std::move(mean), std::move(stddev)));
  }

  template<typename T, typename Engine>
  auto g_random_uniform_int(Engine& engine, T a, T b)
  {
    return g_random<Engine, std::uniform_int_distribution<T>>(
        engine, std::uniform_int_distribution<T>{std::move(a), std::move(b)});
  }

  template<typename T, typename Engine>
  auto g_random_uniform_real(Engine& engine, T a, T b)
  {
    return g_random<Engine, std::uniform_real_distribution<T>>(
        engine, std::uniform_real_distribution<T>{std::move(a), std::move(b)});
  }

  template<typename Engine>
  auto g_random_test(Engine& engine)
  {
    return g_random_uniform_int<int>(engine, -10, 10);
  }
}
