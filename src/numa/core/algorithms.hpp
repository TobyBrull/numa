#ifndef INCLUDE_GUARD_NUMA_CORE_ALGORITHMS_H_
#define INCLUDE_GUARD_NUMA_CORE_ALGORITHMS_H_

#include "numa/boot/error.hpp"
#include "numa/boot/fwd.hpp"

#include <algorithm>
#include <functional>

namespace numa {
  template<typename ForwardRange, typename... ForwardRanges>
  bool equal(ForwardRange const& range, ForwardRanges const&... ranges);

  template<typename... ForwardRanges>
  auto for_each_tuple(ForwardRanges&&... ranges);

  template<typename... ForwardRanges>
  auto for_each_combination(ForwardRanges&&... ranges);

  template<typename RandomAccessRange, typename T>
  auto find(RandomAccessRange& range, T const& value);

  template<typename RandomAccessRange, typename UnaryPredicate>
  auto find_if(RandomAccessRange& range, UnaryPredicate const p);

  template<typename RandomAccessRange, typename T>
  auto lower_bound(RandomAccessRange& range, T const& value);

  template<typename RandomAccessRange>
  void sort(RandomAccessRange& range);

  template<typename RandomAccessRange, typename Compare>
  void sort(RandomAccessRange& range, Compare const comp);

  template<typename ForwardRange, typename Compare = std::less<>>
  bool
  is_sorted_strictly(ForwardRange const& range, Compare const comp = Compare());

  template<
      typename ForwardRange1,
      typename ForwardRange2,
      typename Compare,
      typename FOutput,
      typename FOutput2>
  void combine_sorted_strictly(
      ForwardRange1 const& strictly_sorted_range_1,
      ForwardRange2 const& strictly_sorted_range_2,
      Compare const comp,
      FOutput&& f,
      FOutput2&& f2);
}

#include "algorithms.inl"

#endif
