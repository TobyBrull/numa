#ifndef INCLUDE_GUARD_NUMA_CORE_SLICE_H_
#define INCLUDE_GUARD_NUMA_CORE_SLICE_H_

#include "numa/boot/index.hpp"

namespace numa {
  struct slice {
    slice() = default;

    slice(index_t base, index_t size, index_t stride = 1);

    index_t base   = 0;
    index_t size   = 0;
    index_t stride = 1;
  };

  inline slice slice_size(index_t base, index_t size, index_t stride = 1);

  inline slice slice_till(index_t base, index_t till, index_t stride = 1);

  template<index_t N>
  std::array<slice, N> make_slice_size_array(
      index<N> const& base,
      index<N> const& size,
      index<N> const& stride = index<N>(one));

  template<index_t N>
  std::array<slice, N> make_slice_till_array(
      index<N> const& base,
      index<N> const& till,
      index<N> const& stride = index<N>(one));
}

#include "slice.inl"

#endif
