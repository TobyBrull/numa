#ifndef INCLUDE_GUARD_NUMA_CORE_FUNCTORS_H_
#define INCLUDE_GUARD_NUMA_CORE_FUNCTORS_H_

#include "numa/tmpl/type_list.hpp"
#include "numa/tmpl/type_traits.hpp"

#include "numa/boot/fwd.hpp"

#include <cmath>
#include <complex>
#include <functional>
#include <utility>

namespace numa {
#define NUMA_DECL_FUNCTOR_EXACTLY_1(name)       \
  struct f_##name##_t {                         \
    template<typename T>                        \
    constexpr auto operator()(T&& value) const; \
  };                                            \
  constexpr inline f_##name##_t f_##name = {};

#define NUMA_DECL_STATEFUL_FUNCTOR_EXACTLY_1(name)                \
  template<typename U>                                            \
  struct f_##name##_t {                                           \
    U state_;                                                     \
    constexpr f_##name##_t(U state) : state_{std::move(state)} {} \
    template<typename T>                                          \
    constexpr auto operator()(T&& value) const;                   \
  };

#define NUMA_DECL_FUNCTOR_EXACTLY_2(name)              \
  struct f_##name##_t {                                \
    template<typename T, typename U>                   \
    constexpr auto operator()(T&& lhs, U&& rhs) const; \
  };                                                   \
  constexpr inline f_##name##_t f_##name = {};

#define NUMA_DECL_FUNCTOR_FREE(name)                 \
  struct f_##name##_t {                              \
    template<typename... Ts>                         \
    constexpr auto operator()(Ts&&... values) const; \
  };                                                 \
  constexpr inline f_##name##_t f_##name = {};

#define NUMA_DECL_FUNCTOR_AT_LEAST_1(name)      \
  struct f_##name##_t {                         \
    template<typename... Ts>                    \
    requires(sizeof...(Ts) >= 1) constexpr auto \
    operator()(Ts&&... values) const;           \
  };                                            \
  constexpr inline f_##name##_t f_##name = {};

#define NUMA_DECL_FUNCTOR_AT_LEAST_2(name)      \
  struct f_##name##_t {                         \
    template<typename... Ts>                    \
    requires(sizeof...(Ts) >= 2) constexpr auto \
    operator()(Ts&&... values) const;           \
  };                                            \
  constexpr inline f_##name##_t f_##name = {};

  /*
   * Binary (and higher) predicates.
   */
  NUMA_DECL_FUNCTOR_AT_LEAST_2(is_less);
  NUMA_DECL_FUNCTOR_AT_LEAST_2(is_less_equal);
  NUMA_DECL_FUNCTOR_AT_LEAST_2(is_greater);
  NUMA_DECL_FUNCTOR_AT_LEAST_2(is_greater_equal);
  NUMA_DECL_FUNCTOR_AT_LEAST_2(is_equal);
  NUMA_DECL_FUNCTOR_EXACTLY_2(is_not_equal);

  /*
   * Stateful, unary predicates.
   */
  NUMA_DECL_STATEFUL_FUNCTOR_EXACTLY_1(is_less_than);
  NUMA_DECL_STATEFUL_FUNCTOR_EXACTLY_1(is_less_equal_to);
  NUMA_DECL_STATEFUL_FUNCTOR_EXACTLY_1(is_greater_than);
  NUMA_DECL_STATEFUL_FUNCTOR_EXACTLY_1(is_greater_equal_to);
  NUMA_DECL_STATEFUL_FUNCTOR_EXACTLY_1(is_equal_to);
  NUMA_DECL_STATEFUL_FUNCTOR_EXACTLY_1(is_not_equal_to);

  /*
   * Simple arithmetic functors.
   */
  NUMA_DECL_FUNCTOR_EXACTLY_1(id);

  /*
   * Name like in asm?
   *  add
   *  sub
   *  div
   *  mul
   *
   *  mod
   *  neg
   *  min
   *  max
   */

  NUMA_DECL_FUNCTOR_AT_LEAST_1(plus);
  NUMA_DECL_FUNCTOR_AT_LEAST_1(times);

  NUMA_DECL_FUNCTOR_EXACTLY_2(minus);
  NUMA_DECL_FUNCTOR_EXACTLY_2(divided);
  NUMA_DECL_FUNCTOR_EXACTLY_2(modulus);

  NUMA_DECL_FUNCTOR_EXACTLY_1(negate);

  NUMA_DECL_FUNCTOR_AT_LEAST_1(min);
  NUMA_DECL_FUNCTOR_AT_LEAST_1(max);

  template<typename T>
  constexpr auto f_div(x_t, T divisor);

  template<typename T>
  constexpr auto f_div(T dividend, x_t);

  template<typename T>
  constexpr auto f_pow(x_t, T exponent);

  template<typename T>
  constexpr auto f_pow(T base, x_t);

  /*
   * Mathematical functions.
   */
  NUMA_DECL_FUNCTOR_EXACTLY_1(sin);
  NUMA_DECL_FUNCTOR_EXACTLY_1(cos);

  NUMA_DECL_FUNCTOR_EXACTLY_1(abs);
  NUMA_DECL_FUNCTOR_EXACTLY_1(arg);
  NUMA_DECL_FUNCTOR_EXACTLY_1(norm);
  NUMA_DECL_FUNCTOR_EXACTLY_1(polar);
  NUMA_DECL_FUNCTOR_EXACTLY_1(conj);

  NUMA_DECL_FUNCTOR_EXACTLY_1(exp);
  NUMA_DECL_FUNCTOR_EXACTLY_1(exp2);
  NUMA_DECL_FUNCTOR_EXACTLY_1(expm1);

  NUMA_DECL_FUNCTOR_EXACTLY_1(log);
  NUMA_DECL_FUNCTOR_EXACTLY_1(log2);
  NUMA_DECL_FUNCTOR_EXACTLY_1(log1p);

  NUMA_DECL_FUNCTOR_EXACTLY_1(sqrt);
  NUMA_DECL_FUNCTOR_EXACTLY_1(square);

  /*
   * Utility functions
   */
  NUMA_DECL_FUNCTOR_FREE(no_op);

  NUMA_DECL_FUNCTOR_EXACTLY_2(swap);

  template<int N>
  struct f_get_t {
    template<typename... Ts>
    requires(sizeof...(Ts) > N) constexpr decltype(auto)
    operator()(Ts&&... values) const;

    template<typename... Ts>
    constexpr decltype(auto) operator()(std::tuple<Ts...> const& tt) const;

    template<typename... Ts>
    constexpr decltype(auto) operator()(std::tuple<Ts...>& tt) const;
  };

  template<int N>
  constexpr inline f_get_t<N> f_get = {};

  template<typename T>
  struct f_const_t {
    T value_ = {};

    constexpr f_const_t(T value = {});

    template<typename... Ts>
    constexpr T const& operator()(Ts&&...) const;
  };

  template<typename T>
  constexpr inline f_const_t<T> f_const = {};

  /*
   * Assignment wrappers
   */
#define NUMA_DECL_FUNCTOR_ASSIGN_WRAPPER(name)                   \
  template<typename F>                                           \
  struct f_##name##_t {                                          \
    F f_;                                                        \
                                                                 \
    constexpr f_##name##_t(F&& f);                               \
                                                                 \
    template<typename T, typename... Ts>                         \
    constexpr void operator()(T&& target, Ts&&... params) const; \
                                                                 \
    template<typename T, typename... Ts>                         \
    constexpr void operator()(T&& target, Ts&&... params);       \
  };

  NUMA_DECL_FUNCTOR_ASSIGN_WRAPPER(assign);
  NUMA_DECL_FUNCTOR_ASSIGN_WRAPPER(assign_plus);
  NUMA_DECL_FUNCTOR_ASSIGN_WRAPPER(assign_times);
  NUMA_DECL_FUNCTOR_ASSIGN_WRAPPER(assign_minus);
  NUMA_DECL_FUNCTOR_ASSIGN_WRAPPER(assign_divided);
  NUMA_DECL_FUNCTOR_ASSIGN_WRAPPER(assign_modulus);

#undef NUMA_DECL_FUNCTOR_ASSIGN_WRAPPER
}

#include "functors.inl"

#endif
