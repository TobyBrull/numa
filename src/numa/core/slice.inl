namespace numa {
  inline slice::slice(
      index_t const base, index_t const size, index_t const stride)
    : base(base), size(size), stride(stride)
  {
  }

  inline slice
  slice_size(index_t const base, index_t const size, index_t const stride)
  {
    return slice(base, size, stride);
  }

  inline slice
  slice_till(index_t const base, index_t const till, index_t const stride)
  {
    return slice(base, till - base, stride);
  }

  template<index_t N>
  std::array<slice, N> make_slice_size_array(
      index<N> const& base, index<N> const& size, index<N> const& stride)
  {
    std::array<slice, N> retval;

    for (size_t i = 0; i < N; ++i) {
      retval[i] = slice_size(base[i], size[i], stride[i]);
    }

    return retval;
  }

  template<index_t N>
  std::array<slice, N> make_slice_till_array(
      index<N> const& base, index<N> const& till, index<N> const& stride)
  {
    std::array<slice, N> retval;

    for (size_t i = 0; i < N; ++i) {
      retval[i] = slice_till(base[i], till[i], stride[i]);
    }

    return retval;
  }
}
