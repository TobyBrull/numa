namespace numa {
  inline char peek_till_non_whitespace(std::istream& is)
  {
    while (true) {
      auto const p = is.peek();

      if (p == std::istringstream::traits_type::eof()) {
        return '\0';
      }
      else if ((p == ' ') || (p == '\t') || (p == '\n')) {
        is.get();
      }
      else {
        return p;
      }
    }
  }

  template<typename Predicate>
  std::string read_while(std::istream& is, Predicate&& p)
  {
    std::string retval;

    while (true) {
      if (!is) {
        break;
      }

      auto const c = is.peek();

      if (p(c)) {
        retval.push_back(c);
        is.get();
      }
      else {
        break;
      }
    }

    return retval;
  }

  template<typename... Chars>
  char read_till_any(std::istream& is, Chars const... cs)
  {
    char c = '\0';

    do {
      is.get(c);
    } while (is && ((c != cs) && ...));

    THROW_IF_NOT(is, "serialize_json: end-of-stream while reading");

    return c;
  }

  inline void read_till(std::istream& is, char const d)
  {
    read_till_any(is, d);
  }

  inline void forward_read_till(std::istream& is, std::string_view const sv)
  {
    /*
     * TODO: The implementation is buggy, if @p sv is repetivtive.
     */
    ASSERT(!sv.empty());

    char c = '\0';

    size_t pos = 0;

    do {
      is.get(c);

      if (c == sv[pos]) {
        ++pos;
      }
      else {
        pos = 0;
      }
    } while (is && (pos < sv.size()));

    THROW_IF_NOT(is, "serialize_json: end-of-stream while reading");
  }
}
