namespace numa::detail {
  template<typename IndexSeq, typename... Iterators>
  class zip_iterator_impl;

  struct from_iterators_t {
  };

  constexpr from_iterators_t from_iterators;

  template<typename IndexSeq, typename... Iterators>
  class proxy_value;

  template<typename... Iterators, std::size_t... I>
  class proxy_value<std::index_sequence<I...>, Iterators...> {
   public:
    using iter_tuple = std::tuple<Iterators...>;

    using value_tuple =
        std::tuple<typename std::iterator_traits<Iterators>::value_type...>;

    using reference_tuple =
        std::tuple<typename std::iterator_traits<Iterators>::reference...>;

    using const_reference_tuple = std::tuple<
        typename std::iterator_traits<Iterators>::value_type const&...>;

    proxy_value(proxy_value const& other) : variant_(other.copy_to_value_type())
    {
    }

    proxy_value(proxy_value&& other) : variant_(other.move_to_value_type()) {}

    ~proxy_value() = default;

    proxy_value& operator=(proxy_value const& other)
    {
      auto const rt = other.get_ref();

      std::visit(
          overload{
              [&rt](iter_tuple& t) {
                (((*std::get<I>(t)) = std::get<I>(rt)), ...);
              },
              [&rt](value_tuple& t) {
                ((std::get<I>(t) = std::get<I>(rt)), ...);
              }},
          variant_);

      return (*this);
    }

    proxy_value& operator=(proxy_value&& other)
    {
      auto rt = other.get_ref();

      std::visit(
          overload{
              [&rt](iter_tuple& t) {
                (((*std::get<I>(t)) = std::move(std::get<I>(rt))), ...);
              },
              [&rt](value_tuple& t) {
                ((std::get<I>(t) = std::move(std::get<I>(rt))), ...);
              }},
          variant_);

      return (*this);
    }

    operator reference_tuple()
    {
      return get_ref();
    }

    operator const_reference_tuple() const
    {
      return get_ref();
    }

    reference_tuple get_ref()
    {
      return std::visit(
          overload{
              [](iter_tuple& t) {
                return reference_tuple((*std::get<I>(t))...);
              },
              [](value_tuple& t) {
                return reference_tuple(std::get<I>(t)...);
              }},
          variant_);
    }

    const_reference_tuple get_ref() const
    {
      return std::visit(
          overload{
              [](iter_tuple const& t) {
                return const_reference_tuple((*std::get<I>(t))...);
              },
              [](value_tuple const& t) {
                return const_reference_tuple(std::get<I>(t)...);
              }},
          variant_);
    }

    template<std::size_t J>
    std::tuple_element_t<J, reference_tuple> get()
    {
      return std::visit(
          overload{
              [](iter_tuple& t) -> std::tuple_element_t<J, reference_tuple> {
                return (*std::get<J>(t));
              },
              [](value_tuple& t) -> std::tuple_element_t<J, reference_tuple> {
                return std::get<J>(t);
              }},
          variant_);
    }

    template<std::size_t J>
    std::tuple_element_t<J, const_reference_tuple> get() const
    {
      return std::visit(
          overload{
              [](iter_tuple const& t)
                  -> std::tuple_element_t<J, const_reference_tuple> {
                return *std::get<J>(t);
              },
              [](value_tuple const& t)
                  -> std::tuple_element_t<J, const_reference_tuple> {
                return std::get<J>(t);
              }},
          variant_);
    }

   private:
    proxy_value(from_iterators_t, Iterators... its)
      : variant_(iter_tuple(std::move(its)...))
    {
    }

    proxy_value(from_iterators_t, iter_tuple its) : variant_(std::move(its)) {}

    template<typename OtherIndSeq, typename... OtherIterators>
    friend class zip_iterator_impl;

    iter_tuple& its()
    {
      return std::get<0>(variant_);
    }

    iter_tuple const& its() const
    {
      return std::get<0>(variant_);
    }

    void reset(iter_tuple it)
    {
      variant_ = std::move(it);
    }

    value_tuple copy_to_value_type() const
    {
      return std::visit(
          overload(
              [](iter_tuple const& t) {
                return value_tuple(*(std::get<I>(t))...);
              },
              [](value_tuple const& t) {
                return value_tuple(std::get<I>(t)...);
              }),
          variant_);
    }

    value_tuple move_to_value_type()
    {
      return std::visit(
          overload{
              [](iter_tuple& t) {
                return value_tuple(std::move(*(std::get<I>(t)))...);
              },
              [](value_tuple& t) {
                return value_tuple(std::move(std::get<I>(t))...);
              }},
          variant_);
    }

    std::variant<iter_tuple, value_tuple> variant_;
  };
}

namespace std {
  template<typename... Iterators, std::size_t... I>
  struct tuple_size<
      numa::detail::proxy_value<std::index_sequence<I...>, Iterators...>>
    : integral_constant<std::size_t, sizeof...(Iterators)> {
  };

  template<std::size_t J, typename... Iterators, std::size_t... I>
  struct tuple_element<
      J,
      numa::detail::proxy_value<std::index_sequence<I...>, Iterators...>> {
    using proxy_class =
        numa::detail::proxy_value<std::index_sequence<I...>, Iterators...>;

    using type = std::remove_reference_t<
        typename tuple_element<J, typename proxy_class::reference_tuple>::type>;
  };
}

namespace numa::detail {
  template<typename... Iterators, std::size_t... I>
  class zip_iterator_impl<std::index_sequence<I...>, Iterators...> {
   public:
    using value_type = proxy_value<std::index_sequence<I...>, Iterators...>;

    using difference_type = typename std::iterator_traits<
        std::tuple_element_t<0, std::tuple<Iterators...>>>::difference_type;

    zip_iterator_impl(Iterators... its)
      : data_ref(from_iterators, std::move(its)...)
    {
    }

    zip_iterator_impl(zip_iterator_impl const& other)
      : data_ref(from_iterators, other.data_ref.its())
    {
    }

    zip_iterator_impl(zip_iterator_impl&& other)
      : data_ref(from_iterators, other.data_ref.its())
    {
    }

    void operator=(zip_iterator_impl const& other)
    {
      data_ref.reset(other.data_ref.its());
    }

    void operator=(zip_iterator_impl&& other)
    {
      data_ref.reset(std::move(other.data_ref.its()));
    }

    void operator+=(difference_type const inc)
    {
      ((std::get<I>(data_ref.its()) += inc), ...);
    }

    void operator-=(difference_type const dec)
    {
      ((std::get<I>(data_ref.its()) -= dec), ...);
    }

    void operator++()
    {
      (++std::get<I>(data_ref.its()), ...);
    }

    void operator--()
    {
      (--std::get<I>(data_ref.its()), ...);
    }

    difference_type operator-(zip_iterator_impl const& other) const
    {
      return static_cast<difference_type>(
          std::get<0>(data_ref.its()) - std::get<0>(other.data_ref.its()));
    }

    bool operator!=(zip_iterator_impl const& other) const
    {
      return (std::get<0>(data_ref.its()) != std::get<0>(other.data_ref.its()));
    }

    bool operator==(zip_iterator_impl const& other) const
    {
      return (std::get<0>(data_ref.its()) == std::get<0>(other.data_ref.its()));
    }

    bool operator<(zip_iterator_impl const& other) const
    {
      return (std::get<0>(data_ref.its()) < std::get<0>(other.data_ref.its()));
    }

   protected:
    mutable value_type data_ref;
  };
}

namespace numa {
  template<typename... Iterators>
  class zip_iterator
    : public detail::zip_iterator_impl<
          std::make_index_sequence<sizeof...(Iterators)>,
          Iterators...> {
   public:
    using base_class = detail::zip_iterator_impl<
        std::make_index_sequence<sizeof...(Iterators)>,
        Iterators...>;

    using value_type = typename base_class::value_type;

    using reference = value_type&;
    using pointer   = value_type*;

    using difference_type = typename base_class::difference_type;

    using iterator_category = std::random_access_iterator_tag;

    zip_iterator(Iterators... its) : base_class(std::move(its)...) {}

    zip_iterator(zip_iterator const& other) : base_class(other) {}

    zip_iterator(zip_iterator&& other) : base_class(std::move(other)) {}

    zip_iterator& operator=(zip_iterator const& other)
    {
      base_this()->operator=(other);
      return (*this);
    }

    zip_iterator& operator=(zip_iterator&& other)
    {
      base_this()->operator=(std::move(other));
      return (*this);
    }

    zip_iterator& operator+=(difference_type const inc)
    {
      base_this()->operator+=(inc);
      return (*this);
    }

    zip_iterator& operator-=(difference_type const dec)
    {
      base_this()->operator-=(dec);
      return (*this);
    }

    zip_iterator& operator++()
    {
      base_this()->operator++();
      return (*this);
    }

    zip_iterator& operator--()
    {
      base_this()->operator--();
      return (*this);
    }

    zip_iterator operator++(int)
    {
      zip_iterator retval = (*this);
      ++retval;
      return retval;
    }

    zip_iterator operator--(int)
    {
      zip_iterator retval = (*this);
      --retval;
      return retval;
    }

    difference_type operator-(zip_iterator const& other) const
    {
      return base_this()->operator-(other);
    }

    zip_iterator operator+(difference_type const inc)
    {
      auto retval = (*this);
      retval += inc;
      return retval;
    }

    zip_iterator operator-(difference_type const dec)
    {
      auto retval = (*this);
      retval -= dec;
      return retval;
    }

    friend bool operator==(zip_iterator const& lhs, zip_iterator const& rhs)
    {
      return lhs.base_this()->operator==(rhs);
    }

    friend bool operator!=(zip_iterator const& lhs, zip_iterator const& rhs)
    {
      return lhs.base_this()->operator!=(rhs);
    }

    friend bool operator<(const zip_iterator& lhs, const zip_iterator& rhs)
    {
      return lhs.base_this()->operator<(rhs);
    }

    value_type& operator*() const
    {
      return base_class::data_ref;
    }

    value_type* operator->() const
    {
      return &(base_class::data_ref);
    }

   private:
    base_class* base_this()
    {
      return this;
    }

    base_class const* base_this() const
    {
      return this;
    }
  };

  template<typename... RandomAccessRanges>
  auto make_zip_range(RandomAccessRanges&... ranges)
  {
    using std::begin;
    using std::end;

    return range(zip_iterator(begin(ranges)...), zip_iterator(end(ranges)...));
  }

  namespace detail {
    template<std::size_t... I, typename Compare, typename... RandomAccessRanges>
    void zip_sort_unpack_impl(
        std::index_sequence<I...>,
        Compare const comp,
        RandomAccessRanges&&... ranges)
    {
      auto zr = make_zip_range(ranges...);

      std::sort(zr.begin(), zr.end(), [comp](auto const& lhs, auto const& rhs) {
        return comp(
            std::get<I>(lhs.get_ref())..., std::get<I>(rhs.get_ref())...);
      });
    }
  }

  template<typename Compare, typename... RandomAccessRanges>
  void zip_sort_unpack(Compare const comp, RandomAccessRanges&&... ranges)
  {
    detail::zip_sort_unpack_impl(
        std::make_index_sequence<sizeof...(RandomAccessRanges)>(),
        comp,
        std::forward<RandomAccessRanges>(ranges)...);
  }

  template<typename Compare, typename... RandomAccessRanges>
  void zip_sort_tuple(const Compare comp, RandomAccessRanges&&... ranges)
  {
    auto zr = make_zip_range(ranges...);

    std::sort(zr.begin(), zr.end(), [comp](auto const& lhs, auto const& rhs) {
      return comp(lhs.get_ref(), rhs.get_ref());
    });
  }

  template<typename... RandomAccessRanges>
  void zip_sort(RandomAccessRanges&&... ranges)
  {
    zip_sort_tuple(std::less<>(), std::forward<RandomAccessRanges>(ranges)...);
  }

  template<typename FBinary, typename... RandomAccessRanges>
  index_t zip_unique_tuple(FBinary&& f, RandomAccessRanges&&... ranges)
  {
    auto zr = make_zip_range(ranges...);

    auto prev_it      = zr.begin();
    auto const end_it = zr.end();

    index_t retval = 0;

    if (prev_it == end_it) {
      return retval;
    }

    auto it = prev_it;
    ++it;
    ++retval;

    bool adjacent = true;

    while (it != end_it) {
      bool const remove = f(prev_it->get_ref(), it->get_ref());

      if (remove) {
        ++it;
        adjacent = false;
      }
      else {
        if (adjacent) {
          ++it;
          ++prev_it;
        }
        else {
          ++prev_it;
          (*prev_it) = (*it);
          ++it;
        }

        ++retval;
      }
    }

    return retval;
  }

  namespace detail {
    template<std::size_t... I, typename F, typename... RandomAccessRanges>
    index_t zip_unique_unpack_impl(
        std::index_sequence<I...>, F&& f, RandomAccessRanges&&... ranges)
    {
      return zip_unique_tuple(
          [&f](auto const& lhs_ref, auto const& rhs_ref) {
            return f(std::get<I>(lhs_ref)..., std::get<I>(rhs_ref)...);
          },
          std::forward<RandomAccessRanges>(ranges)...);
    }
  }

  template<typename F, typename... RandomAccessRanges>
  index_t zip_unique_unpack(F&& f, RandomAccessRanges&&... ranges)
  {
    return detail::zip_unique_unpack_impl(
        std::make_index_sequence<sizeof...(RandomAccessRanges)>(),
        std::forward<F>(f),
        std::forward<RandomAccessRanges>(ranges)...);
  }
}
