namespace std::chrono {
  /*
   * chrono
   */
  template<typename T>
  void to_json(std::ostream& os, std::chrono::duration<T> const dur, int)
  {
    numa::to_json(os, dur.count());
  }
}

namespace numa {
  template<typename T>
  void to_json(std::ostream& os, T* const ptr, int const)
  {
    os << ptr;
  }

  namespace detail {
    template<index_t N, typename T, typename U>
    void to_json_index_like(std::ostream& os, U const& ind)
    {
      os << '[';

      for (index_t i = 0; i < N; ++i) {
        if (i > 0) {
          os << ", ";
        }

        os << static_cast<T>(ind[i]);
      }

      os << ']';
    }

    template<index_t N, typename T, typename U>
    void from_json_index_like(std::istream& is, U& ind)
    {
      read_till(is, '[');

      THROW_IF_NOT(is, "serialize_json: end-of-stream while reading");

      T temp = {};

      for (index_t i = 0; i < N; ++i) {
        if (i > 0) {
          read_till(is, ',');

          THROW_IF_NOT(is, "serialize_json: end-of-stream while reading");
        }

        is >> temp;
        ind[i] = temp;

        THROW_IF_NOT(is, "serialize_json: end-of-stream while reading");
      }

      read_till(is, ']');

      THROW_IF_NOT(is, "serialize_json: end-of-stream while reading");
    }

    inline void json_encode_char(std::ostream& os, char const c)
    {
      switch (c) {
        case '\\':
        case '"':
        case '/':
          os << '\\' << c;
          break;

        case '\0':
          os << "\\0";
          break;

        case '\b':
          os << "\\b";
          break;

        case '\t':
          os << "\\t";
          break;

        case '\n':
          os << "\\n";
          break;

        case '\f':
          os << "\\f";
          break;

        case '\r':
          os << "\\r";
          break;

        default:
          if (c < ' ') {
            NOT_IMPLEMENTED();
            //            t = "000" + Integer.toHexString(c);
            //            sb.append("\\u" + t.substring(t.length() - 4));
          }
          else {
            os << c;
          }
      }
    }
  }

  inline void to_json(std::ostream& os, float const value, int)
  {
    os << std::defaultfloat << std::setprecision(9) << value;
  }

  inline void to_json(std::ostream& os, double const value, int)
  {
    os << std::defaultfloat << std::setprecision(17) << value;
  }

  inline void from_json(std::istream& is, float& value)
  {
    is >> value;
  }

  inline void from_json(std::istream& is, double& value)
  {
    is >> value;
  }

  inline void to_json(std::ostream& os, std::string_view const sv, int const)
  {
    os << '"';
    for (char const c: sv) {
      detail::json_encode_char(os, c);
    }
    os << '"';
  }

  inline void to_json(std::ostream& os, std::string const& s, int)
  {
    to_json(os, std::string_view(s));
  }

  inline void to_json(std::ostream& os, char const* s, int)
  {
    to_json(os, std::string_view(s));
  }

  inline void to_json(std::ostream& os, char const c, int)
  {
    os << '"';
    detail::json_encode_char(os, c);
    os << '"';
  }

  inline void from_json(std::istream& is, std::string& s)
  {
    s.clear();

    read_till(is, '"');

    char c = '\0';

    while (true) {
      is.get(c);

      if (!is || (c == '"')) {
        break;
      }
      else if (c == '\\') {
        is.get(c);

        THROW_IF(!is, "serialize_json: end-of-stream while reading string");

        switch (c) {
          case '\\':
          case '"':
          case '/':
            break;

          case '0':
            c = '\0';
            break;
          case 'b':
            c = '\b';
            break;
          case 't':
            c = '\t';
            break;
          case 'n':
            c = '\n';
            break;
          case 'f':
            c = '\f';
            break;
          case 'r':
            c = '\r';
            break;

          default:
            NOT_IMPLEMENTED();
        }
      }

      s.push_back(c);
    }

    THROW_IF_NOT(is, "serialize_json: end-of-stream while reading string");
  }

  inline void from_json(std::istream& is, char& c)
  {
    std::string temp;
    from_json(is, temp);

    THROW_IF_NOT(
        temp.size() == 1,
        "serialize_json: string of length not equal to one encountered "
        "when trying to read character");

    c = temp.at(0);
  }

  /*
   * tuple & pair
   */

  namespace detail {
    template<index_t I, typename... Ts>
    void tuple_to_json_impl(std::ostream& os, std::tuple<Ts...> const& tt)
    {
      if constexpr (I < sizeof...(Ts)) {
        to_json(os, std::get<I>(tt));

        if constexpr (I + 1 < sizeof...(Ts)) {
          os << ", ";
        }

        tuple_to_json_impl<I + 1>(os, tt);
      }
    }
  }

  template<typename... Ts>
  void to_json(std::ostream& os, std::tuple<Ts...> const& tt, int)
  {
    os << '[';
    detail::tuple_to_json_impl<0>(os, tt);
    os << ']';
  }

  namespace detail {
    template<index_t I, typename... Ts>
    void tuple_from_json_impl(std::istream& is, std::tuple<Ts...>& tt)
    {
      if constexpr (I < sizeof...(Ts)) {
        from_json(is, std::get<I>(tt));

        if constexpr (I + 1 < sizeof...(Ts)) {
          read_till(is, ',');
        }

        tuple_from_json_impl<I + 1>(is, tt);
      }
    }
  }

  template<typename... Ts>
  void from_json(std::istream& is, std::tuple<Ts...>& tt)
  {
    read_till(is, '[');
    detail::tuple_from_json_impl<0>(is, tt);
    read_till(is, ']');
  }

  /*
   * variant
   */

  template<typename... Ts>
  void
  to_json(std::ostream& os, std::variant<Ts...> const& tt, int const indent)
  {
    std::string const ii(indent, ' ');

    os << "{\n"
       << ii << "  \"index\": " << tt.index() << ",\n"
       << ii << "  \"value\": ";

    std::visit(
        [&os, indent](auto const& x) { to_json(os, x, indent + 2); }, tt);

    os << '\n' << ii << '}';
  }

  template<typename... Ts>
  void from_json(std::istream& is, std::variant<Ts...>& tt)
  {
    read_till(is, '{');
    forward_read_till(is, "\"index\"");
    read_till(is, ':');

    index_t const index = make_from_json<index_t>(is);

    read_till(is, ',');
    forward_read_till(is, "\"value\"");
    read_till(is, ':');

    dynamicizer<sizeof...(Ts)>(
        [&tt, &is]<index_t I>() {
          using T = std::tuple_element_t<I, std::tuple<Ts...>>;
          T x{};

          from_json(is, x);

          tt = std::move(x);
        },
        index);

    read_till(is, '}');
  }

  /*
   * index
   */
  template<index_t N>
  void to_json(std::ostream& os, index<N> const& ind, int const)
  {
    numa::detail::to_json_index_like<N, index_t>(os, ind);
  }

  template<index_t N>
  void from_json(std::istream& is, index<N>& ind)
  {
    numa::detail::from_json_index_like<N, index_t>(is, ind);
  }

  /*
   * Array spans
   */
  template<typename T, std::ptrdiff_t Extent>
  void
  to_json(std::ostream& os, std::span<T, Extent> const& as, int const indent)
  {
    os << '[';

    bool first = true;
    for (auto const& val: as) {
      if (first) {
        first = false;
      }
      else {
        os << ", ";
      }

      to_json(os, val, indent);
    }

    os << ']';
  }

  template<typename T, std::ptrdiff_t Extent>
  void from_json(std::istream& is, std::span<T, Extent> const& as)
  {
    read_till(is, '[');

    bool first = true;
    for (auto& val: as) {
      if (first) {
        first = false;
      }
      else {
        read_till(is, ',');
      }

      from_json(is, val);
    }

    read_till(is, ']');
  }

  /*
   * vector
   */
  template<typename T>
  void to_json(std::ostream& os, std::vector<T> const& vec, int const indent)
  {
    to_json(os, std::span{vec}, indent);
  }

  template<typename T>
  void from_json(std::istream& is, std::vector<T>& vec)
  {
    vec.clear();

    read_till(is, '[');

    while (true) {
      char const c = peek_till_non_whitespace(is);

      if (c == ']') {
        break;
      }
      else if (c == ',') {
        read_till(is, ',');
      }
      else {
        vec.emplace_back();
        from_json(is, vec.back());
      }
    }

    read_till(is, ']');
  }

  template<typename T, std::size_t N>
  void to_json(std::ostream& os, std::array<T, N> const& arr, int const indent)
  {
    to_json(os, std::span{arr}, indent);
  }

  template<typename T, std::size_t N>
  void from_json(std::istream& is, std::array<T, N>& arr)
  {
    from_json(is, std::span{arr});
  }

  /*
   * Slices
   */
  inline void to_json(std::ostream& os, slice const& sl, int const indent)
  {
    os << '[';

    to_json(os, sl.base);

    os << ':';

    to_json(os, sl.size);

    os << ':';

    to_json(os, sl.stride);

    os << ']';
  }

  inline void from_json(std::istream& is, slice& sl)
  {
    read_till(is, '[');

    from_json(is, sl.base);

    read_till(is, ':');

    from_json(is, sl.size);

    read_till(is, ':');

    from_json(is, sl.stride);

    read_till(is, ']');
  }

  /*
   * String conversion
   */
  template<typename T>
  requires(!std::is_base_of_v<std::ios_base, T>) std::string
      to_json(T const& value, int const indent)
  {
    std::ostringstream oss;

    to_json(oss, value, indent);

    return oss.str();
  }

  template<typename T>
  void from_json(std::string_view const sv, T&& value)
  {
    std::istringstream iss(std::string{sv});
    from_json(iss, std::forward<T>(value));
  }

  /*
   * Constructing from-json functions
   */
  template<typename T>
  T make_from_json_impl(c_type_t<T>, std::istream& is)
  {
    T retval{};
    from_json(is, retval);
    return retval;
  }

  template<typename T>
  T make_from_json(std::istream& is)
  {
    return make_from_json_impl(c_type<T>, is);
  }

  template<typename T>
  T make_from_json(std::string_view const sv)
  {
    std::istringstream iss(std::string{sv});
    return make_from_json<T>(iss);
  }
}

namespace std {
  /*
   * To string conversion & stream operator.
   */
  template<numa::SerializableJson T>
  requires(!numa::SerializablePretty<T> && !numa::SerializableNative<T>)
      std::ostream&
      operator<<(std::ostream& os, T const& value)
  {
    using namespace numa;
    to_json(os, value);
    return os;
  }

  template<numa::SerializableJson T>
  requires(!numa::SerializablePretty<T> && !numa::SerializableNative<T>)
      std::istream&
      operator>>(std::istream& is, T& value)
  {
    using namespace numa;
    from_json(is, value);
    return is;
  }
}
