#ifndef INCLUDE_GUARD_NUMA_CORE_GENERATORS_H_
#define INCLUDE_GUARD_NUMA_CORE_GENERATORS_H_

#include "numa/tmpl/type_list.hpp"
#include "numa/tmpl/type_traits.hpp"

#include "numa/boot/fwd.hpp"

#include <cmath>
#include <complex>
#include <functional>
#include <random>
#include <utility>

namespace numa {
  template<typename T = index_t>
  auto g_iota(T init = T{});

  template<typename T = index_t>
  auto g_riota(T init = T{});

  template<typename Engine, typename Distribution>
  auto g_random(Engine&, Distribution);

  template<typename T = double, typename Engine>
  auto g_random_normal(Engine&, T mean = T{0.0}, T stddev = T{1.0});

  template<typename T = index_t, typename Engine>
  auto g_random_uniform_int(
      Engine&, T a = T{0}, T b = std::numeric_limits<T>::max());

  template<typename T = double, typename Engine>
  auto g_random_uniform_real(Engine&, T a = T{0.0}, T b = T{1.0});

  template<typename Engine>
  auto g_random_test(Engine&);
}

#include "generators.inl"

#endif
