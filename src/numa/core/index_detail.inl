namespace numa::detail {
  template<index_t N>
  constexpr index<N> capacity_to_stride(index<N> ind)
  {
    index_t cur = 1;

    for (index_t i = 0; i < N; ++i) {
      index_t temp = ind[i];
      ind[i]       = cur;
      cur *= temp;
    }

    return ind;
  }

  template<index_t N>
  constexpr index_t capacity_dot(index<N> const& ind, index<N> const& capacity)
  {
    static_assert(N >= 0);

    index_t retval = ind[N - 1];

    for (index_t i = (N - 1); i > 0; --i) {
      index_t const j = (i - 1);

      retval *= capacity[j];
      retval += ind[j];
    }

    return retval;
  }

  template<index_t N>
  constexpr index<N> end_index(index<N> const& base, index<N> const& till)
  {
    index<N> retval = base;

    if constexpr (N > 0) {
      retval[N - 1] = till[N - 1];
    }

    return retval;
  }

  template<index_t N>
  constexpr bool
  is_end_index(index<N> const& ind, index<N> const& base, index<N> const& till)
  {
    static_assert(N >= 1);

    return (ind[N - 1] == till[N - 1]);
  }

  template<index_t N>
  constexpr index_t tensor_increment(
      index<N>& pos,
      index<N> const& base,
      index<N> const& till,
      index<N> const& stride)
  {
    static_assert(N >= 1);

    index_t retval = 0;

    for (index_t i = 0; i < N; ++i) {
      ++pos[i];

      if (pos[i] < till[i]) {
        retval += stride[i];
        break;
      }
      else {
        if (i < (N - 1)) {
          retval -= (pos[i] - base[i] - 1) * stride[i];
          pos[i] = base[i];
        }
        else {
          retval = 1;
          pos[i] = till[i];
        }
      }
    }

    return retval;
  }

  template<index_t N>
  constexpr index_t tensor_decrement(
      index<N>& pos,
      index<N> const& base,
      index<N> const& till,
      index<N> const& stride)
  {
    static_assert(N >= 1);

    index_t retval = 0;

    for (index_t i = 0; i < N; ++i) {
      --pos[i];

      if ((i == (N - 1)) && (pos[i] == (till[i] - 1))) {
        retval = -1;
      }
      else if (pos[i] >= base[i]) {
        retval -= stride[i];
        break;
      }
      else {
        pos[i] = (till[i] - 1);

        retval += (pos[i] - base[i]) * stride[i];
      }
    }

    return retval;
  }

  template<index_t N>
  index<N> offset_to_multistep(index<N> const& size, index_t offset)
  {
    ASSERT(is_bounded(index<N>(zero), size));

    auto const counts = capacity_to_stride(size);

    index<N> retval(zero);

    {
      index_t i = (N - 1);
      while (true) {
        retval[i] = (offset / counts[i]);

        offset -= retval[i] * counts[i];

        if ((offset == 0) || (i == 0)) {
          break;
        }

        --i;
      }
    }

    ASSERT(offset == 0);

    return retval;
  }

  template<index_t N>
  constexpr index_t tensor_advance(
      index<N>& pos,
      index_t offset,
      index<N> const& base,
      index<N> const& till,
      index<N> const& stride)
  {
    static_assert(N >= 1);

    auto const size = (till - base);

    if (offset > 0) {
      auto increments = offset_to_multistep(size, offset);

      index_t ptr_offset = 0;

      for (index_t i = 0; i < N; ++i) {
        if (pos[i] + increments[i] >= till[i]) {
          if (i < (N - 1)) {
            increments[i] -= size[i];

            ++increments[i + 1];
          }
          else {
            ASSERT(pos[i] + increments[i] == till[i]);
            pos[i] = till[i];
            ASSERT(pos == splice(base, till, N - 1));
            ASSERT(increments[i] > 0);

            --increments[i];
            ptr_offset += increments[i] * stride[i];

            for (index_t j = 0; j < N - 1; ++j) {
              ptr_offset += (size[j] - 1) * stride[j];
            }

            ++ptr_offset;

            return ptr_offset;
          }
        }

        pos[i] += increments[i];
        ptr_offset += increments[i] * stride[i];
      }

      return ptr_offset;
    }
    else if (offset < 0) {
      index_t ptr_offset = 0;

      if (pos == splice(base, till, N - 1)) {
        pos = till - index<N>(one);
        ++offset;
        ptr_offset = -1;
      }

      auto decrements = offset_to_multistep(size, offset);

      for (index_t i = 0; i < N; ++i) {
        if (pos[i] + decrements[i] < base[i]) {
          if (i < (N - 1)) {
            decrements[i] += size[i];

            --decrements[i + 1];
          }
          else {
            ASSERT(false);
          }
        }

        pos[i] += decrements[i];
        ptr_offset += decrements[i] * stride[i];
      }

      return ptr_offset;
    }
    else {
      return 0;
    }
  }

  template<index_t N>
  constexpr index_t tensor_distance_to(
      index<N> const& lhs_pos,
      index<N> const& rhs_pos,
      index<N> const& base,
      index<N> const& till)
  {
    return dot(lhs_pos - rhs_pos, capacity_to_stride(till - base));
  }
}
