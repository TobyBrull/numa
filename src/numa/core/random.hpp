#ifndef INCLUDE_GUARD_NUMA_CORE_RANDOM_H_
#define INCLUDE_GUARD_NUMA_CORE_RANDOM_H_

#include "numa/boot/index.hpp"

#include <algorithm>
#include <numeric>
#include <random>
#include <vector>

namespace numa {
  /**
   * Scalar random functions.
   */
  template<typename T, typename URBG>
  requires std::is_integral_v<T> T random(T size, URBG&& g);

  template<typename T, typename URBG>
  requires std::is_integral_v<T> T random_size(T from, T size, URBG&& g);

  template<typename T, typename URBG>
  requires std::is_integral_v<T> T random_till(T from, T till, URBG&& g);

  /**
   * Index-based random functions.
   */
  template<index_t N, typename URBG>
  index<N> random(index<N> const& size, URBG&& g);

  template<index_t N, typename URBG>
  index<N> random_size(index<N> const& from, index<N> const& size, URBG&& g);

  template<index_t N, typename URBG>
  index<N> random_till(index<N> const& from, index<N> const& till, URBG&& g);

  /**
   * Returns a random N-variation with K elements.
   */
  template<index_t K, index_t N, typename URBG>
  std::array<index_t, K> random_variation(URBG&& g);
}

#include "random.inl"

#endif
