#ifndef INCLUDE_GUARD_NUMA_CORE_CONCEPTS_H_
#define INCLUDE_GUARD_NUMA_CORE_CONCEPTS_H_

#include "numa/tmpl/type_traits.hpp"

namespace numa {
  template<typename Alloc, std::align_val_t AL>
  concept AllocatorImpl = requires(Alloc alloc, index_t count, void* ptr)
  {
    {
      alloc.alignment(AL)
    }
    ->std::convertible_to<std::align_val_t>;
    {
      alloc.template allocate<AL>(count)
    }
    ->std::convertible_to<void*>;
    {alloc.template deallocate<AL>(ptr, count)};

    requires noexcept(alloc.template deallocate<AL>(ptr, count));

    requires noexcept(alloc.alignment(AL));

    requires std::is_nothrow_copy_constructible_v<Alloc>;
    requires std::is_nothrow_copy_assignable_v<Alloc>;

    requires std::is_nothrow_move_constructible_v<Alloc>;
    requires std::is_nothrow_move_assignable_v<Alloc>;
  };

  template<typename T>
  concept Allocator =
      AllocatorImpl<std::decay_t<T>, static_cast<std::align_val_t>(1)>;
}

#endif
