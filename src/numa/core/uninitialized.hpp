#ifndef INCLUDE_GUARD_NUMA_CORE_UNINITIALIZED_H_
#define INCLUDE_GUARD_NUMA_CORE_UNINITIALIZED_H_

#include "numa/boot/fwd.hpp"
#include "numa/tmpl/mark.hpp"

#include <tuple>

namespace numa {
  // template<typename T>
  // inline constexpr bool is_initializer_v;

  template<typename TargetT, typename... Args>
  struct initializer {
    std::tuple<Args...> args_tuple;

    using target_type = TargetT;
  };

  template<typename TargetT, typename... Args>
  auto init(Args... args);

  template<typename... Args>
  auto init_all(Args... args);

  template<typename Mark, typename... Args>
  requires(is_mark(c_type<Mark>).value) auto init(Mark, Args... args);

  template<typename MarkType, typename ValueType, typename... Initers>
  auto initializer_for_tuple(std::tuple<Initers const&...> initers_tuple);

  template<typename MarkType, typename ValueType, typename... Initers>
  auto initializer_for(Initers const&... initers);

  template<typename... Args>
  constexpr bool is_uninitialized();

  template<typename T, typename... Args>
  void fill_assign(T& target, Args const&... args);

  template<typename T, typename... Args>
  void fill_construct(T& target, Args const&... args);

  template<typename T>
  void copy_assign(T& target, T const& source);

  template<typename T>
  void copy_construct(T& target, T const& source);

  template<typename T>
  requires(!std::is_reference_v<T>) void move_assign(T& target, T& source);

  template<typename T>
  requires(!std::is_reference_v<T>) void move_construct(T& target, T& source);

  template<typename T>
  requires(!std::is_reference_v<T>) void destruct(T& target);

  template<typename... Args>
  struct f_fill_assign_t {
   private:
    std::tuple<Args const&...> args_tuple_;

   public:
    f_fill_assign_t(Args const&... args);

    template<typename T>
    requires(!std::is_reference_v<T>) void operator()(T& target) const;
  };

  template<typename... Args>
  struct f_fill_construct_t {
   private:
    std::tuple<Args const&...> args_tuple_;

   public:
    f_fill_construct_t(Args const&... args);

    template<typename T>
    requires(!std::is_reference_v<T>) void operator()(T& target) const;
  };

  struct f_copy_assign_t {
    template<typename T>
    void operator()(T& target, T const& source) const;
  };

  struct f_copy_construct_t {
    template<typename T>
    void operator()(T& target, T const& source) const;
  };

  struct f_move_assign_t {
    template<typename T>
    requires(!std::is_reference_v<T>) void
    operator()(T& target, T& source) const;
  };

  struct f_move_construct_t {
    template<typename T>
    requires(!std::is_reference_v<T>) void
    operator()(T& target, T& source) const;
  };

  class f_destruct_t {
   public:
    template<typename T, typename... Ts>
    requires(!std::is_reference_v<T>) void
    operator()(T& target, Ts const&...) const;
  };

  constexpr inline f_copy_assign_t f_copy_assign       = {};
  constexpr inline f_copy_construct_t f_copy_construct = {};
  constexpr inline f_move_assign_t f_move_assign       = {};
  constexpr inline f_move_construct_t f_move_construct = {};
  constexpr inline f_destruct_t f_destruct             = {};
}

#include "uninitialized.inl"

#endif
