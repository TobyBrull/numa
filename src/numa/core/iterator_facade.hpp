#ifndef INCLUDE_GUARD_NUMA_CORE_ITERATOR_FACADE_ITERATOR_H_
#define INCLUDE_GUARD_NUMA_CORE_ITERATOR_FACADE_ITERATOR_H_

#include "numa/boot/fwd.hpp"

#include <iterator>

namespace numa {
  template<typename Subclass, typename T>
  class iterator_facade {
   public:
    static_assert(std::is_signed_v<index_t>);
    using difference_type   = index_t;
    using value_type        = T;
    using pointer           = T*;
    using reference         = T&;
    using iterator_category = std::random_access_iterator_tag;

    void operator++();
    void operator--();

    void operator+=(index_t offset);
    void operator-=(index_t offset);

    index_t operator-(Subclass const& other) const;

    friend bool operator<(Subclass const& lhs, Subclass const& rhs)
    {
      return 0 < (rhs - lhs);
    }

    friend bool operator<=(Subclass const& lhs, Subclass const& rhs)
    {
      return 0 <= (rhs - lhs);
    }

    friend bool operator>(Subclass const& lhs, Subclass const& rhs)
    {
      return 0 > (rhs - lhs);
    }

    friend bool operator>=(Subclass const& lhs, Subclass const& rhs)
    {
      return 0 >= (rhs - lhs);
    }

    friend bool operator!=(Subclass const& lhs, Subclass const& rhs)
    {
      return !lhs.equal_to(rhs);
    }

    friend bool operator==(Subclass const& lhs, Subclass const& rhs)
    {
      return lhs.equal_to(rhs);
    }

    decltype(auto) operator*() const;

    friend Subclass operator+(Subclass lhs, index_t const offset)
    {
      lhs += offset;
      return lhs;
    }
  };
}

#include "iterator_facade.inl"

#endif
