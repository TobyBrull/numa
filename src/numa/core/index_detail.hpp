#ifndef INCLUDE_GUARD_NUMA_CORE_INDEX_DETAIL_H_
#define INCLUDE_GUARD_NUMA_CORE_INDEX_DETAIL_H_

#include "numa/boot/index.hpp"

namespace numa::detail {
  template<index_t N>
  constexpr index<N> capacity_to_stride(index<N> ind);

  template<index_t N>
  constexpr index_t capacity_dot(index<N> const& ind, index<N> const& capacity);

  template<index_t N>
  constexpr index<N> end_index(index<N> const& base, index<N> const& till);

  template<index_t N>
  constexpr bool
  is_end_index(index<N> const& ind, index<N> const& base, index<N> const& till);

  template<index_t N>
  constexpr index_t tensor_increment(
      index<N>& pos,
      index<N> const& base,
      index<N> const& till,
      index<N> const& stride);

  template<index_t N>
  constexpr index_t tensor_decrement(
      index<N>& pos,
      index<N> const& base,
      index<N> const& till,
      index<N> const& stride);

  template<index_t N>
  constexpr index_t tensor_advance(
      index<N>& pos,
      index_t offset,
      index<N> const& base,
      index<N> const& till,
      index<N> const& stride);

  template<index_t N>
  constexpr index_t tensor_distance_to(
      index<N> const& lhs_pos,
      index<N> const& rhs_pos,
      index<N> const& base,
      index<N> const& till);
}

#include "index_detail.inl"

#endif
