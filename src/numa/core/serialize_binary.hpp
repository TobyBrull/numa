#ifndef INCLUDE_GUARD_NUMA_CORE_SERIALIZE_BINARY_H_
#define INCLUDE_GUARD_NUMA_CORE_SERIALIZE_BINARY_H_

#include "numa/tmpl/type_traits.hpp"

#include "numa/boot/fwd.hpp"
#include "numa/boot/index.hpp"

#include <chrono>
#include <iosfwd>
#include <variant>

namespace std::chrono {
  /*
   * chrono
   */
  template<typename T>
  void to_binary(std::ostream& os, std::chrono::duration<T>);

  template<typename T>
  void from_binary(std::istream& is, std::chrono::duration<T>&);
}

namespace numa {
  /*
   * all arithmetic types
   */

  template<typename T>
  requires std::is_arithmetic_v<T> void to_binary(std::ostream&, T const&);

  template<typename T>
  requires std::is_arithmetic_v<T> void from_binary(std::istream&, T&);

  /*
   * variant
   */

  template<typename... Ts>
  void to_binary(std::ostream&, std::variant<Ts...> const&);

  template<typename... Ts>
  void from_binary(std::istream&, std::variant<Ts...>&);

  /*
   * index
   */

  template<index_t N>
  void to_binary(std::ostream&, index<N> const&);

  template<index_t N>
  void from_binary(std::istream&, index<N>&);

  /*
   * String conversion
   */
  template<typename T>
  requires(!std::is_base_of_v<std::ios_base, T>) std::string
      to_binary(T const&);

  template<typename T>
  void from_binary(std::string_view, T&& value);

  /*
   * Constructing from-binary functions
   */
  template<typename T>
  T make_from_binary_impl(c_type_t<T>, std::istream&);

  template<typename T>
  T make_from_binary(std::istream&);

  template<typename T>
  T make_from_binary(std::string_view);

  /*
   * concept
   */

  template<typename T>
  concept SerializableBinary =
      requires(decay_t<T> t, std::ostream& os, std::istream& is)
  {
    {to_binary(os, t)};
    {from_binary(is, t)};
  };
}

#include "serialize_binary.inl"

#endif
