namespace std::chrono {
  /*
   * chrono
   */
  template<typename T>
  void to_pretty(std::ostream& os, std::chrono::duration<T> const dur)
  {
    numa::to_pretty(os, dur.count());
  }

  template<typename T>
  void from_pretty(std::istream& is, std::chrono::duration<T>& dur)
  {
    T val{};
    numa::from_pretty(is, val);

    dur = std::chrono::duration<T>(val);
  }
}

namespace numa {
  inline void to_pretty(std::ostream& os, float const value)
  {
    os << std::defaultfloat << std::setprecision(9) << value;
  }

  inline void to_pretty(std::ostream& os, double const value)
  {
    os << std::defaultfloat << std::setprecision(17) << value;
  }

  inline void from_pretty(std::istream& is, float& value)
  {
    is >> value;
  }

  inline void from_pretty(std::istream& is, double& value)
  {
    is >> value;
  }

  inline void to_pretty(std::ostream& os, identifier const& id)
  {
    os << id.data();
  }

  inline void from_pretty(std::istream& is, identifier& id)
  {
    peek_till_non_whitespace(is);
    auto temp = read_while(
        is, [](char const c) { return std::isalnum(c) || (c == '_'); });

    id = identifier(std::move(temp));
  }

  template<typename T>
  void to_pretty(std::ostream& os, std::vector<T> const& vec)
  {
    os << '[';

    bool first = true;
    for (auto const& x: vec) {
      if (!first) {
        os << ", ";
      }
      else {
        first = false;
      }

      to_pretty(os, x);
    }

    os << ']';
  }

  template<typename T>
  void from_pretty(std::istream& is, std::vector<T>& vec)
  {
    vec.clear();

    read_till(is, '[');

    while (true) {
      char const c = peek_till_non_whitespace(is);

      if (c == ']') {
        break;
      }
      else if (c == ',') {
        read_till(is, ',');
      }
      else {
        vec.emplace_back();
        from_pretty(is, vec.back());
      }
    }

    read_till(is, ']');
  }

  /*
   * derived functions
   */

  template<typename T>
  requires(!std::is_base_of_v<std::ios_base, T>) std::string
      to_pretty(T const& x)
  {
    std::ostringstream oss;
    to_pretty(oss, x);
    return oss.str();
  }

  template<typename T>
  void from_pretty(std::string_view const sv, T&& value)
  {
    std::istringstream iss(std::string{sv});
    from_pretty(iss, value);
  }

  template<typename T>
  T make_from_pretty(std::istream& is)
  {
    T retval{};
    from_pretty(is, retval);
    return retval;
  }

  template<typename T>
  T make_from_pretty(std::string_view const sv)
  {
    std::istringstream iss(std::string{sv});
    return make_from_pretty<T>(iss);
  }
}

namespace std {
  template<numa::SerializablePretty T>
  requires(!numa::SerializableNative<T>) std::ostream&
  operator<<(std::ostream& os, T const& value)
  {
    using namespace numa;
    to_pretty(os, value);
    return os;
  }

  template<numa::SerializablePretty T>
  requires(!numa::SerializableNative<T>) std::istream&
  operator>>(std::istream& is, T& value)
  {
    using namespace numa;
    from_pretty(is, value);
    return is;
  }
}
