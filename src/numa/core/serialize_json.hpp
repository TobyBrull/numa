#ifndef INCLUDE_GUARD_NUMA_CORE_SERIALIZE_JSON_H_
#define INCLUDE_GUARD_NUMA_CORE_SERIALIZE_JSON_H_

#include "serialize_pretty.hpp"
#include "slice.hpp"

#include "numa/tmpl/dynamicizer.hpp"
#include "numa/tmpl/literals.hpp"

#include "numa/boot/bits.hpp"
#include "numa/boot/index.hpp"
#include "numa/boot/span.hpp"

#include <chrono>
#include <sstream>
#include <string>
#include <tuple>
#include <variant>
#include <vector>

namespace std::chrono {
  /*
   * chrono
   */
  template<typename T>
  void to_json(std::ostream& os, std::chrono::duration<T>, int = 0);
}

namespace numa {
  template<typename T>
  void to_json(std::ostream& os, T* ptr, int = 0);

  /*
   * base types
   */
#define BASIC_SERIALIZE_JSON(T)                           \
  inline void to_json(std::ostream& os, T value, int = 0) \
  {                                                       \
    os << value;                                          \
  }                                                       \
  inline void from_json(std::istream& is, T& value)       \
  {                                                       \
    is >> value;                                          \
  }

  BASIC_SERIALIZE_JSON(bool);
  BASIC_SERIALIZE_JSON(int);
  BASIC_SERIALIZE_JSON(std::size_t);
  BASIC_SERIALIZE_JSON(index_t);

#undef BASIC_SERIALIZE_JSON

  /*
   * Make sure we have enough precision for doubles and floats.
   */
  void to_json(std::ostream& os, float value, int = 0);
  void to_json(std::ostream& os, double value, int = 0);
  void from_json(std::istream& is, float& value);
  void from_json(std::istream& is, double& value);

  /*
   * Strings and chars have to be escaped.
   */
  void to_json(std::ostream& os, std::string_view sv, int = 0);
  void to_json(std::ostream& os, std::string const& s, int = 0);
  void to_json(std::ostream& os, char const* s, int = 0);
  void to_json(std::ostream& os, char c, int = 0);
  void from_json(std::istream& is, std::string& s);
  void from_json(std::istream& is, char& c);

  /*
   * tuple & pair
   */
  template<typename... Ts>
  void to_json(std::ostream& os, std::tuple<Ts...> const& tt, int = 0);

  template<typename... Ts>
  void from_json(std::istream& is, std::tuple<Ts...>& tt);

  /*
   * variant
   */
  template<typename... Ts>
  void to_json(std::ostream& os, std::variant<Ts...> const& tt, int = 0);

  template<typename... Ts>
  void from_json(std::istream& is, std::variant<Ts...>& tt);

  /*
   * index
   */
  template<index_t N>
  void to_json(std::ostream& os, index<N> const& ind, int = 0);

  template<index_t N>
  void from_json(std::istream& is, index<N>& ind);

  /*
   * Array spans
   */
  template<typename T, std::ptrdiff_t Extent>
  void to_json(std::ostream& os, std::span<T, Extent> const&, int indent = 0);

  template<typename T, std::ptrdiff_t Extent>
  void from_json(std::istream& is, std::span<T, Extent> const&);

  /*
   * vector
   */
  template<typename T>
  void to_json(std::ostream&, std::vector<T> const&, int indent = 0);

  template<typename T>
  void from_json(std::istream&, std::vector<T>&);

  /*
   * array
   */
  template<typename T, std::size_t N>
  void to_json(std::ostream&, std::array<T, N> const&, int indent = 0);

  template<typename T, std::size_t N>
  void from_json(std::istream&, std::array<T, N>&);

  /*
   * Slices
   */
  inline void to_json(std::ostream& os, slice const& sl, int indent = 0);

  inline void from_json(std::istream& is, slice& sl);

  /*
   * Generic functions
   */

  /*
   * String conversion
   */
  template<typename T>
  requires(!std::is_base_of_v<std::ios_base, T>) std::string
      to_json(T const& value, int indent = 0);

  template<typename T>
  void from_json(std::string_view sv, T&& value);

  /*
   * Constructing from-json functions
   */
  template<typename T>
  T make_from_json_impl(c_type_t<T>, std::istream& is);

  template<typename T>
  T make_from_json(std::istream& is);

  template<typename T>
  T make_from_json(std::string_view sv);

  /*
   * concept
   */

  template<typename T>
  concept SerializableJson =
      requires(decay_t<T> t, std::ostream& os, std::istream& is)
  {
    {to_json(os, t)};
    {from_json(is, t)};
  };
}

namespace std {
  /*
   *
   */

  template<numa::SerializableJson T>
  requires(!numa::SerializablePretty<T> && !numa::SerializableNative<T>)
      std::ostream&
      operator<<(std::ostream& os, T const& value);

  template<numa::SerializableJson T>
  requires(!numa::SerializablePretty<T> && !numa::SerializableNative<T>)
      std::istream&
      operator>>(std::istream& os, T& value);
}

#include "serialize_json.inl"

#endif
