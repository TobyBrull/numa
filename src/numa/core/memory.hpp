#ifndef INCLUDE_GUARD_NUMA_CORE_MEMORY_H_
#define INCLUDE_GUARD_NUMA_CORE_MEMORY_H_

#include "concepts.hpp"

#include "numa/boot/bits.hpp"
#include "numa/boot/index.hpp"
#include "numa/boot/modes.hpp"

#include <algorithm>
#include <memory>
#include <variant>

#include <experimental/memory_resource>

namespace numa {
  constexpr std::align_val_t default_alignment =
      static_cast<std::align_val_t>(__STDCPP_DEFAULT_NEW_ALIGNMENT__);

  template<typename... Ts>
  constexpr std::align_val_t
      alignment_of = static_cast<std::align_val_t>(numa::lcm({alignof(Ts)...}));

  template<Allocator Alloc, std::align_val_t Al1, std::align_val_t Al2>
  constexpr bool
      use_same_alignment = (Alloc::alignment(Al1) == Alloc::alignment(Al2));

  struct default_allocator {
    template<std::align_val_t Al>
    [[nodiscard]] static void* allocate(index_t count);

    template<std::align_val_t Al>
    static void deallocate(void* p, index_t count) noexcept;

    constexpr static std::align_val_t alignment(std::align_val_t al) noexcept;
  };

  struct null_allocator {
    template<std::align_val_t Al>
    [[nodiscard]] static void* allocate(index_t count);

    template<std::align_val_t Al>
    static void deallocate(void* p, index_t count) noexcept;

    constexpr static std::align_val_t alignment(std::align_val_t al) noexcept;
  };

  template<std::align_val_t OverAl>
  struct over_aligned_allocator {
    template<std::align_val_t Al>
    [[nodiscard]] static void* allocate(index_t count);

    template<std::align_val_t Al>
    static void deallocate(void* p, index_t count) noexcept;

    constexpr static std::align_val_t alignment(std::align_val_t al) noexcept;
  };

  /*
   * RAII type for allocated memory. Never allocates itself.
   */
  template<Allocator Alloc, std::align_val_t MinAl>
  class memory {
   public:
    constexpr memory(Alloc alloc) noexcept;

    constexpr memory(Alloc alloc, void* p, index_t count) noexcept;

    memory(memory const&) = delete;
    memory& operator=(memory const&) = delete;

    template<std::align_val_t OtherMinAl>
    memory(memory<Alloc, OtherMinAl>&&) noexcept
        requires use_same_alignment<Alloc, MinAl, OtherMinAl>;

    template<std::align_val_t OtherMinAl>
    memory& operator=(memory<Alloc, OtherMinAl>&&) noexcept
        requires use_same_alignment<Alloc, MinAl, OtherMinAl>;

    ~memory();

    void clear() noexcept;

    void* release() noexcept;

    void reset(void* p, index_t count) noexcept;

    Alloc get_allocator() const noexcept;

    void* ptr() noexcept;
    void const* ptr() const noexcept;
    void const* const_ptr() const noexcept;

    index_t count() const noexcept;

    constexpr static std::align_val_t alignment() noexcept;

    template<SubMode SM>
    memory<null_allocator, MinAl>
    sub(SM, index_t from, index_t size_or_till) noexcept;

    bool is_empty() const noexcept;

   private:
    template<Allocator OtherAlloc, std::align_val_t OtherMinAl>
    requires std::is_same_v<Alloc, OtherAlloc> friend class memory;

    Alloc alloc_;

    void* p_       = nullptr;
    index_t count_ = 0;
  };

  template<Allocator Alloc, typename T>
  using memory_for = memory<Alloc, alignment_of<T>>;

  /*
   * Native alignment-based
   */
  template<std::align_val_t MinAl, Allocator Alloc>
  [[nodiscard]] void* allocate(Alloc& alloc, index_t count);

  template<std::align_val_t MinAl, Allocator Alloc>
  void deallocate(Alloc& alloc, void* p, index_t count) noexcept;

  /*
   * Memory alignment-based
   */
  template<std::align_val_t MinAl, Allocator Alloc>
  memory<Alloc, MinAl> allocate_memory(Alloc alloc, index_t count);

  /*
   * Native type-based
   */
  template<typename T, Allocator Alloc>
  [[nodiscard]] T* allocate_for(Alloc& alloc, index_t n);

  template<typename T, Allocator Alloc>
  void deallocate_for(Alloc& alloc, T* p, index_t n) noexcept;

  /*
   * Memory type-based
   */
  template<typename T, Allocator Alloc>
  memory_for<Alloc, T> allocate_memory_for(Alloc alloc, index_t n);

  /*
   * reform-mode stuff
   */
  struct default_reform_mode_t {
  };
  struct force_reallocate_t {
  };
  struct no_allocate_t {
  };

  namespace {
    constexpr default_reform_mode_t default_reform_mode;
    constexpr force_reallocate_t force_reallocate;
    constexpr no_allocate_t no_allocate;
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  using reform_mode = std::variant<
      default_reform_mode_t,
      force_reallocate_t,
      no_allocate_t,
      memory<Alloc, MinAl>>;

  template<Allocator Alloc, typename T>
  using reform_mode_for = reform_mode<Alloc, alignment_of<T>>;

  template<typename T, Allocator Alloc>
  constexpr bool reform_mode_must_reallocate_for(
      reform_mode_for<Alloc, T> const& mode) noexcept;

  template<typename T, Allocator Alloc>
  [[nodiscard]] T* reform_mode_allocate_for(
      Alloc& alloc, index_t n, reform_mode_for<Alloc, T> mode);
}

#include "memory.inl"

#endif
