namespace numa {
  template<typename T, typename URBG>
  requires std::is_integral_v<T> T random(T const size, URBG&& g)
  {
    ASSERT(size > 0);

    std::uniform_int_distribution<T> dist(0, size - 1);

    return dist(g);
  }

  template<typename T, typename URBG>
  requires std::is_integral_v<T>
      T random_size(T const from, T const size, URBG&& g)
  {
    ASSERT(size > 0);

    std::uniform_int_distribution<T> dist(from, from + (size - 1));

    return dist(g);
  }

  template<typename T, typename URBG>
  requires std::is_integral_v<T>
      T random_till(T const from, T const till, URBG&& g)
  {
    ASSERT(from < till);

    std::uniform_int_distribution<T> dist(from, till - 1);

    return dist(g);
  }

  template<index_t N, typename URBG>
  index<N> random(index<N> const& ind, URBG&& g)
  {
    index<N> retval = zero;

    for (index_t i = 0; i < N; ++i) {
      retval[i] = random<index_t>(ind[i], g);
    }

    return retval;
  }

  template<index_t N, typename URBG>
  index<N> random_size(index<N> const& ind, index<N> const& size, URBG&& g)
  {
    index<N> retval = zero;

    for (index_t i = 0; i < N; ++i) {
      retval[i] = random_size<index_t>(ind[i], size[i], g);
    }

    return retval;
  }

  template<index_t N, typename URBG>
  index<N> random_till(index<N> const& ind, const index<N>& till, URBG&& g)
  {
    index<N> retval = zero;

    for (index_t i = 0; i < N; ++i) {
      retval[i] = random_till<index_t>(ind[i], till[i], g);
    }

    return retval;
  }

  /**
   * Returns a random N-variation with K elements.
   */
  template<index_t K, index_t N, typename URBG>
  std::array<index_t, K> random_variation(URBG&& g)
  {
    ASSERT(K <= N);

    std::array<index_t, N> temp;

    for (std::size_t i = 0; i < N; ++i) {
      temp[i] = i;
    }

    std::shuffle(temp.begin(), temp.end(), g);

    std::array<index_t, K> retval;

    for (index_t i = 0; i < K; ++i) {
      retval[i] = temp[i];
    }

    return retval;
  }
}
