namespace std::chrono {
  /*
   * chrono
   */
  template<typename T>
  void to_binary(std::ostream& os, std::chrono::duration<T> const dur)
  {
    numa::to_binary(os, dur.count());
  }

  template<typename T>
  void from_binary(std::istream& is, std::chrono::duration<T>& dur)
  {
    T val{};
    numa::from_binary(is, dur);

    dur = std::chrono::duration<T>(val);
  }
}

namespace numa {
  /*
   * all arithmetic types
   */

  template<typename T>
  requires std::is_arithmetic_v<T> void
  to_binary(std::ostream& os, T const& value)
  {
    os.write(reinterpret_cast<char const*>(&value), sizeof(T));
  }

  template<typename T>
  requires std::is_arithmetic_v<T> void from_binary(std::istream& is, T& value)
  {
    is.read(reinterpret_cast<char*>(&value), sizeof(T));
  }

  /*
   * variant
   */

  template<typename... Ts>
  void to_binary(std::ostream& os, std::variant<Ts...> const& var)
  {
  }

  template<typename... Ts>
  void from_binary(std::istream& is, std::variant<Ts...>& var)
  {
  }

  /*
   * index
   */

  template<index_t N>
  void to_binary(std::ostream& os, index<N> const& ind)
  {
    for (index_t i = 0; i < N; ++i) {
      to_binary(os, ind[i]);
    }
  }

  template<index_t N>
  void from_binary(std::istream& is, index<N>& ind)
  {
    for (index_t i = 0; i < N; ++i) {
      from_binary(is, ind[i]);
    }
  }

  /*
   *
   */

  template<typename T>
  requires(!std::is_base_of_v<std::ios_base, T>) std::string
      to_binary(T const& value)
  {
    std::ostringstream oss;
    to_binary(oss, value);
    return oss.str();
  }

  template<typename T>
  void from_binary(std::string_view const sv, T&& value)
  {
    std::istringstream iss(std::string{sv});
    from_binary(iss, value);
  }

  /*
   *
   */

  template<typename T>
  T make_from_binary_impl(c_type_t<T>, std::istream& is)
  {
    T retval{};
    from_binary(is, retval);
    return retval;
  }

  template<typename T>
  T make_from_binary(std::istream& is)
  {
    return make_from_binary_impl(c_type<T>, is);
  }

  template<typename T>
  T make_from_binary(std::string_view const sv)
  {
    std::istringstream iss(std::string{sv});
    return make_from_binary<T>(iss);
  }
}
