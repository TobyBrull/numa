#ifndef INCLUDE_GUARD_NUMA_CORE_SERIALIZE_STREAM_H_
#define INCLUDE_GUARD_NUMA_CORE_SERIALIZE_STREAM_H_

#include "serialize_misc.hpp"

#include "numa/tmpl/type_traits.hpp"

#include "numa/boot/fwd.hpp"
#include "numa/boot/identifier.hpp"

#include <chrono>
#include <iosfwd>

namespace std::chrono {
  /*
   * chrono
   */
  template<typename T>
  void to_pretty(std::ostream& os, std::chrono::duration<T>);

  template<typename T>
  void from_pretty(std::istream& is, std::chrono::duration<T>&);
}

namespace numa {
  /*
   * Output stream operator
   */
  template<typename T>
  concept SerializableNative =
      requires(decay_t<T> t, std::ostream& os, std::istream& is)
  {
    {std::operator<<(os, t)};
    {std::operator>>(is, t)};
  };

  template<typename T>
  concept SerializablePretty =
      requires(decay_t<T> t, std::ostream& os, std::istream& is)
  {
    {to_pretty(os, t)};
    {from_pretty(is, t)};
  };

    /*
     * base types
     */

#define BASIC_SERIALIZE_PRETTY(T)                     \
  inline void to_pretty(std::ostream& os, T value)    \
  {                                                   \
    os << value;                                      \
  }                                                   \
  inline void from_pretty(std::istream& is, T& value) \
  {                                                   \
    is >> value;                                      \
  }

  BASIC_SERIALIZE_PRETTY(bool);
  BASIC_SERIALIZE_PRETTY(int);
  BASIC_SERIALIZE_PRETTY(std::size_t);
  BASIC_SERIALIZE_PRETTY(index_t);
  BASIC_SERIALIZE_PRETTY(std::string);

#undef BASIC_SERIALIZE_PRETTY

  /*
   * Make sure we have enough precision for doubles and floats.
   */
  void to_pretty(std::ostream&, float value);
  void to_pretty(std::ostream&, double value);
  void from_pretty(std::istream&, float& value);
  void from_pretty(std::istream&, double& value);

  void to_pretty(std::ostream&, identifier const&);
  void from_pretty(std::istream&, identifier&);

  template<typename T>
  void to_pretty(std::ostream&, std::vector<T> const&);
  template<typename T>
  void from_pretty(std::istream&, std::vector<T>&);

  template<typename T>
  requires(!std::is_base_of_v<std::ios_base, T>) std::string
      to_pretty(T const&);

  template<typename T>
  void from_pretty(std::string_view sv, T&& value);

  template<typename T>
  T make_from_pretty(std::istream& is);

  template<typename T>
  T make_from_pretty(std::string_view);
}

namespace std {
  /*
   * insertion & extraction operator
   */

  template<numa::SerializablePretty T>
  requires(!numa::SerializableNative<T>) std::ostream&
  operator<<(std::ostream& os, T const& value);

  template<numa::SerializablePretty T>
  requires(!numa::SerializableNative<T>) std::istream&
  operator>>(std::istream& is, T& value);
}

#include "serialize_pretty.inl"

#endif
