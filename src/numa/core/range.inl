namespace numa {
  template<int I, typename T>
  auto element_find(T const& value)
  {
    return [&value](auto const& p) {
      using std::get;

      return (get<I>(p) == value);
    };
  }

  template<int I, typename Compare>
  auto element_compare(Compare comp)
  {
    return [comp = std::move(comp)](auto const& lhs, auto const& rhs) {
      using std::get;

      return comp(get<I>(lhs), get<I>(rhs));
    };
  }

  template<typename BeginIterator, typename EndIterator>
  range<BeginIterator, EndIterator>::range(BeginIterator begin, EndIterator end)
    : begin_(std::move(begin)), end_(std::move(end))
  {
  }

  template<typename BeginIterator, typename EndIterator>
  BeginIterator range<BeginIterator, EndIterator>::begin() const
  {
    return begin_;
  }

  template<typename BeginIterator, typename EndIterator>
  EndIterator range<BeginIterator, EndIterator>::end() const
  {
    return end_;
  }

  template<typename BeginIterator, typename EndIterator>
  index_t range<BeginIterator, EndIterator>::size() const
  {
    static_assert(
        std::is_same_v<
            typename std::iterator_traits<BeginIterator>::iterator_category,
            std::random_access_iterator_tag> &&
            std::is_same_v<
                typename std::iterator_traits<EndIterator>::iterator_category,
                std::random_access_iterator_tag>,
        "Cannot call size on range of non-random-acess iterators");

    return static_cast<index_t>(end_ - begin_);
  }

  template<typename BeginIterator, typename EndIterator>
  bool range<BeginIterator, EndIterator>::is_empty() const
  {
    return (begin_ == end_);
  }

  template<typename BeginIterator, typename EndIterator>
  std::pair<BeginIterator, EndIterator>
  range<BeginIterator, EndIterator>::as_tuple() const&
  {
    return std::pair(begin_, end_);
  }

  template<typename BeginIterator, typename EndIterator>
  std::pair<BeginIterator, EndIterator>
  range<BeginIterator, EndIterator>::as_tuple() &&
  {
    return std::pair(std::move(begin_), std::move(end_));
  }
}
