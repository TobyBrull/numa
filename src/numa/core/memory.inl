namespace numa {
  /*
   * default-allocator
   */
  template<std::align_val_t Al>
  [[nodiscard]] void* default_allocator::allocate(index_t const count)
  {
    return ::operator new(count, Al);
  }

  template<std::align_val_t Al>
  void
  default_allocator::deallocate(void* const p, index_t const count) noexcept
  {
    ::operator delete(p, count, Al);
  }

  constexpr std::align_val_t
  default_allocator::alignment(std::align_val_t const al) noexcept
  {
    return std::max(al, default_alignment);
  }

  /*
   * null-allocator
   */
  template<std::align_val_t Al>
  [[nodiscard]] void* null_allocator::allocate(index_t const)
  {
    THROW("null_allocator::allocate: must not be called");
  }

  template<std::align_val_t Al>
  void null_allocator::deallocate(void*, index_t) noexcept
  {
    no_op;
  }

  constexpr std::align_val_t
  null_allocator::alignment(std::align_val_t const al) noexcept
  {
    return al;
  }

  /*
   * over-aligned-allocator
   */
  template<std::align_val_t OverAl>
  template<std::align_val_t Al>
  [[nodiscard]] void*
  over_aligned_allocator<OverAl>::allocate(index_t const count)
  {
    return ::operator new(count, std::max(Al, OverAl));
  }

  template<std::align_val_t OverAl>
  template<std::align_val_t Al>
  void over_aligned_allocator<OverAl>::deallocate(
      void* const p, index_t const count) noexcept
  {
    ::operator delete(p, count, std::max(Al, OverAl));
  }

  template<std::align_val_t OverAl>
  constexpr std::align_val_t
  over_aligned_allocator<OverAl>::alignment(std::align_val_t const al) noexcept
  {
    return std::max({al, default_alignment, OverAl});
  }

  /*
   * memory
   */
  template<Allocator Alloc, std::align_val_t MinAl>
  constexpr memory<Alloc, MinAl>::memory(Alloc alloc) noexcept
    : alloc_(std::move(alloc)), p_(nullptr), count_(0)
  {
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  constexpr memory<Alloc, MinAl>::memory(
      Alloc alloc, void* const p, index_t const count) noexcept
    : alloc_(std::move(alloc)), p_(p), count_(count)
  {
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  template<std::align_val_t OtherMinAl>
  memory<Alloc, MinAl>::memory(memory<Alloc, OtherMinAl>&& other) noexcept
      requires use_same_alignment<Alloc, MinAl, OtherMinAl>
    : alloc_(std::move(other.alloc_))
    , p_(other.p_)
    , count_(other.count_)
  {
    other.p_     = nullptr;
    other.count_ = 0;
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  template<std::align_val_t OtherMinAl>
  memory<Alloc, MinAl>&
  memory<Alloc, MinAl>::operator=(memory<Alloc, OtherMinAl>&& other) noexcept
      requires use_same_alignment<Alloc, MinAl, OtherMinAl>
  {
    clear();

    alloc_ = std::move(other.alloc_);
    p_     = other.p_;
    count_ = other.count_;

    other.p_     = nullptr;
    other.count_ = 0;

    return (*this);
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  memory<Alloc, MinAl>::~memory()
  {
    clear();
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  void memory<Alloc, MinAl>::clear() noexcept
  {
    if (!is_empty()) {
      deallocate<MinAl>(alloc_, p_, count_);

      p_     = nullptr;
      count_ = 0;
    }
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  void* memory<Alloc, MinAl>::release() noexcept
  {
    void* retval = p_;

    p_     = nullptr;
    count_ = 0;

    return retval;
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  void memory<Alloc, MinAl>::reset(void* const p, index_t const count) noexcept
  {
    clear();

    p_     = p;
    count_ = count;
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  Alloc memory<Alloc, MinAl>::get_allocator() const noexcept
  {
    return alloc_;
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  void* memory<Alloc, MinAl>::ptr() noexcept
  {
    return p_;
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  void const* memory<Alloc, MinAl>::ptr() const noexcept
  {
    return p_;
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  void const* memory<Alloc, MinAl>::const_ptr() const noexcept
  {
    return p_;
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  index_t memory<Alloc, MinAl>::count() const noexcept
  {
    return count_;
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  constexpr std::align_val_t memory<Alloc, MinAl>::alignment() noexcept
  {
    return MinAl;
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  template<SubMode SM>
  memory<null_allocator, MinAl> memory<Alloc, MinAl>::sub(
      SM, index_t const from, index_t const size_or_till) noexcept
  {
    index_t const size = sub_mode(SM{}, from, size_or_till, as_size);

    ASSERT(from >= 0);
    ASSERT(size >= 0);
    ASSERT(from + size <= count_);

    return {p_ + from, size};
  }

  template<Allocator Alloc, std::align_val_t MinAl>
  bool memory<Alloc, MinAl>::is_empty() const noexcept
  {
    if (p_ == nullptr) {
      ASSERT(count_ == 0);

      return true;
    }
    else {
      return false;
    }
  }

  /*
   * Native alignment-based
   */
  template<std::align_val_t MinAl, Allocator Alloc>
  [[nodiscard]] void* allocate(Alloc& alloc, index_t const count)
  {
    constexpr std::align_val_t Al = Alloc::alignment(MinAl);

    return alloc.template allocate<Al>(count);
  }

  template<std::align_val_t MinAl, Allocator Alloc>
  void deallocate(Alloc& alloc, void* const p, index_t const count) noexcept
  {
    constexpr std::align_val_t Al = Alloc::alignment(MinAl);

    return alloc.template deallocate<Al>(p, count);
  }

  /*
   * Memory alignment-based
   */
  template<std::align_val_t MinAl, Allocator Alloc>
  memory<Alloc, MinAl> allocate_memory(Alloc alloc, index_t const count)
  {
    void* const p = allocate<MinAl>(alloc, count);

    return memory<Alloc, MinAl>(std::move(alloc), p, count);
  }

  /*
   * Native type-based
   */
  template<typename T, Allocator Alloc>
  [[nodiscard]] T* allocate_for(Alloc& alloc, index_t const n)
  {
    return static_cast<T*>(allocate<alignment_of<T>>(alloc, n * sizeof(T)));
  }

  template<typename T, Allocator Alloc>
  void deallocate_for(Alloc& alloc, T* const p, index_t const n) noexcept
  {
    deallocate<alignment_of<T>>(alloc, p, n * sizeof(T));
  }

  /*
   * Memory type-based
   */
  template<typename T, Allocator Alloc>
  memory_for<Alloc, T> allocate_memory_for(Alloc alloc, index_t const n)
  {
    return allocate_memory<alignment_of<T>>(std::move(alloc), n * sizeof(T));
  }

  /*
   * cell-axes support
   */
  template<typename T, Allocator Alloc>
  constexpr bool reform_mode_must_reallocate_for(
      reform_mode_for<Alloc, T> const& mode) noexcept
  {
    struct visitor {
      bool operator()(default_reform_mode_t) const
      {
        return false;
      }
      bool operator()(force_reallocate_t) const
      {
        return true;
      }
      bool operator()(no_allocate_t) const
      {
        return false;
      }
      bool operator()(memory_for<Alloc, T> const&) const
      {
        return true;
      }
    };

    return std::visit(visitor{}, mode);
  }

  template<typename T, Allocator Alloc>
  [[nodiscard]] T* reform_mode_allocate_for(
      Alloc& alloc, index_t const n, reform_mode_for<Alloc, T> mode)
  {
    struct visitor {
      Alloc& alloc;
      index_t n;

      T*
      operator()(std::variant<default_reform_mode_t, force_reallocate_t>) const
      {
        return allocate_for<T>(alloc, n);
      }

      T* operator()(no_allocate_t) const
      {
        ILLEGAL_ALLOCATION();
      }

      T* operator()(memory_for<Alloc, T>&& mem) const
      {
        ASSERT(static_cast<index_t>(n * sizeof(T)) <= mem.count());

        return static_cast<T*>(mem.release());
      }
    };

    return std::visit(visitor{alloc, n}, std::move(mode));
  }
}
