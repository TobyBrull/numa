#ifndef INCLUDE_GUARD_NUMA_CORE_ZIP_H_
#define INCLUDE_GUARD_NUMA_CORE_ZIP_H_

#include "range.hpp"

#include "numa/boot/bits.hpp"
#include "numa/boot/fwd.hpp"

#include <tuple>
#include <variant>

namespace numa {
  /**
   * The value-type of this iterator is a proxy-class. Objects of this
   * proxy-class can be in one of two states: (a) they can refer to
   * objects in the zipped ranges, or (b) they can contain the correspoding
   * values themselves. User of this iterator can only construct objects in
   * state (a) themselves. The iterator uses objects of type (b) internally.
   *
   * To work with these proxy-objects, users can call get_ref() on them to
   * obtain a tuple of references, regardless of whether the proxy-object is
   * in state (a) or (b).
   */
  template<typename... Iterators>
  class zip_iterator;

  template<typename... Iterators>
  zip_iterator(Iterators... its) -> zip_iterator<Iterators...>;

  /**
   * Precondition: all @p ranges have the same length.
   */
  template<typename... RandomAccessRanges>
  auto make_zip_range(RandomAccessRanges&... ranges);

  /**
   * Zip multiple ranges and sort them by @p comp. @p comp is expected
   * to take two times sizeof... (RandomAccessRanges) parameters.
   * @p comp has to compare the first half of the parameters to the
   * second half of the parameters.
   */
  template<typename Compare, typename... RandomAccessRanges>
  void zip_sort_unpack(Compare const comp, RandomAccessRanges&&... ranges);

  /**
   * Zip multiple ranges and sort them by @p comp. @p comp is expected
   * to take two parameters, which are std::tuples of references.
   */
  template<typename Compare, typename... RandomAccessRanges>
  void zip_sort_tuple(Compare const comp, RandomAccessRanges&&... ranges);

  /**
   * Zip multiple ranges and sort them lexicographically using the
   * less-than operator.
   */
  template<typename... RandomAccessRanges>
  void zip_sort(RandomAccessRanges&&... ranges);

  /**
   * Similar to std::unique, but allows more sophisticated accumulation.
   *
   * @see zip_unique_tuple()
   */
  template<typename F, typename... RandomAccessRanges>
  index_t zip_unique_unpack(F&& f, RandomAccessRanges&&... ranges);

  /**
   * Similar to std::unique, but allows more sophisticated accumulation.
   *
   * The binary-functor @p f has to take two parameters and return if
   * they are considered equal. If not, both elements are kept. If yes,
   * the right parameter may be overwritten, and it is allowed to make
   * changes to the first parameter.
   *
   * Returns the number of unique elements.
   */
  template<typename FBinary, typename... RandomAccessRanges>
  index_t zip_unique_tuple(FBinary&& f, RandomAccessRanges&&... ranges);
}

#include "zip.inl"

#endif
