namespace numa {
  template<typename Subclass, typename T>
  void iterator_facade<Subclass, T>::operator++()
  {
    static_cast<Subclass*>(this)->increment();
  }

  template<typename Subclass, typename T>
  void iterator_facade<Subclass, T>::operator--()
  {
    static_cast<Subclass*>(this)->decrement();
  }

  template<typename Subclass, typename T>
  void iterator_facade<Subclass, T>::operator+=(index_t offset)
  {
    static_cast<Subclass*>(this)->advance(offset);
  }

  template<typename Subclass, typename T>
  void iterator_facade<Subclass, T>::operator-=(index_t offset)
  {
    static_cast<Subclass*>(this)->advance(-offset);
  }

  template<typename Subclass, typename T>
  index_t iterator_facade<Subclass, T>::operator-(Subclass const& other) const
  {
    return static_cast<Subclass const*>(this)->distance_to(other);
  }

  template<typename Subclass, typename T>
  decltype(auto) iterator_facade<Subclass, T>::operator*() const
  {
    return static_cast<Subclass const*>(this)->dereference();
  }
}
