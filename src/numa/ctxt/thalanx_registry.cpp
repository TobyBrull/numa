#include "thalanx_registry.hpp"

namespace numa {
  auto thalanx_registry::handlers_scope() -> std::vector<scope_handler_t>&
  {
    static std::vector<scope_handler_t> instance = [] {
      std::vector<scope_handler_t> retval;
      retval.reserve(5);
      return retval;
    }();

    return instance;
  }

  thalanx_remote_stacks thalanx_registry::execute_handlers(
      std::span<std::shared_ptr<thalanx_thread>> ths)
  {
    thalanx_remote_stacks retval(ths.size() * handlers_scope().size());

    for (auto& th: ths) {
      ASSERT(th != nullptr);

      for (auto& scope_handler: handlers_scope()) {
        retval.push_back(scope_handler(*th));
      }
    }

    return retval;
  }

  auto thalanx_registry::handlers_fork_master()
      -> std::vector<handler_fork_master>&
  {
    static std::vector<handler_fork_master> instance = [] {
      std::vector<handler_fork_master> retval;
      retval.reserve(5);
      return retval;
    }();

    return instance;
  }

  auto thalanx_registry::handlers_fork_per_thread()
      -> std::vector<handler_fork_per_thread>&
  {
    static std::vector<handler_fork_per_thread> instance = [] {
      std::vector<handler_fork_per_thread> retval;
      retval.reserve(5);
      return retval;
    }();

    return instance;
  }

  void thalanx_registry::execute_fork_master(
      index_t const parent_tree_id,
      std::span<index_t const> child_tree_ids,
      fork_type const ft,
      index_t const num_jobs)
  {
    for (auto& hh: handlers_fork_master()) {
      hh(parent_tree_id, child_tree_ids, ft, num_jobs);
    }
  }

  void thalanx_registry::execute_fork_per_thread(
      index_t const parent_tree_id,
      index_t const child_tree_id,
      fork_type const ft,
      fork_rank const fr)
  {
    for (auto& hh: handlers_fork_per_thread()) {
      hh(parent_tree_id, child_tree_id, ft, fr);
    }
  }
}
