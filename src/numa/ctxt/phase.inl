namespace numa {
  /*
   * Phase base
   */
  template<phase_level_t Level, char... Name, char... Loc>
  inline phase<
      c_phase_level_t<Level>,
      c_string_t<Name...>,
      void,
      c_string_t<Loc...>>::
      phase(c_phase_level_t<Level>, c_string_t<Name...>, c_string_t<Loc...>)
  {
    phase_receptor::notify_push(level, phase_id);
  }

  template<phase_level_t Level, char... Name, char... Loc>
  inline phase<
      c_phase_level_t<Level>,
      c_string_t<Name...>,
      void,
      c_string_t<Loc...>>::~phase()
  {
    phase_receptor::notify_pop(level, c_hash_value<set_bit(phase_id, 0_i)>);
  }

  template<phase_level_t Level, char... Name, char... Loc>
  template<typename... Ts>
  inline void
  phase<c_phase_level_t<Level>, c_string_t<Name...>, void, c_string_t<Loc...>>::
      status(Ts&&... str_args) const
  {
    phase_receptor::notify_status(
        level, phase_id, std::forward<Ts>(str_args)...);
  }

  template<phase_level_t Level, char... Name, char... Loc>
  template<typename... Ts>
  inline void
  phase<c_phase_level_t<Level>, c_string_t<Name...>, void, c_string_t<Loc...>>::
      args(std::string_view names, Ts&&... str_args) const
  {
    phase_receptor::notify_args(
        level, phase_id, names, std::forward<Ts>(str_args)...);
  }

  template<phase_level_t Level, char... Name, char... Loc>
  inline bool ATTRIBUTE_USED phase<
      c_phase_level_t<Level>,
      c_string_t<Name...>,
      void,
      c_string_t<Loc...>>::registered_ = [] {
    using phase_t = phase<
        c_phase_level_t<Level>,
        c_string_t<Name...>,
        void,
        c_string_t<Loc...>>;

    return phase_registry::register_phase(
        phase_t::phase_id, phase_t::location, phase_t::name, phase_t::level);
  }();

  /*
   * Progress
   */
  template<phase_level_t Level, char... Name, typename PT, char... Loc>
  inline phase<
      c_phase_level_t<Level>,
      c_string_t<Name...>,
      PT,
      c_string_t<Loc...>>::
      phase(
          c_phase_level_t<Level> pl,
          c_string_t<Name...> name,
          PT total,
          c_string_t<Loc...> loc)
    : phase<
          c_phase_level_t<Level>,
          c_string_t<Name...>,
          void,
          c_string_t<Loc...>>(pl, name, loc)
    , total_(std::move(total))
  {
    phase_receptor::notify_progress_push(level, phase_id);
  }

  template<phase_level_t Level, char... Name, typename PT, char... Loc>
  inline phase<
      c_phase_level_t<Level>,
      c_string_t<Name...>,
      PT,
      c_string_t<Loc...>>::~phase()
  {
    phase_receptor::notify_progress_pop(
        level, c_hash_value<set_bit(phase_id, 0_i)>);
  }

  template<phase_level_t Level, char... Name, typename PT, char... Loc>
  inline void
  phase<c_phase_level_t<Level>, c_string_t<Name...>, PT, c_string_t<Loc...>>::
      start(PT upcoming, double const total_progress)
  {
    ASSERT(upcoming >= PT{});

    begin_ = end_;
    end_ += std::move(upcoming);

    ASSERT(end_ <= total_);

    notify_monitor(total_progress);
  }

  template<phase_level_t Level, char... Name, typename PT, char... Loc>
  inline void
  phase<c_phase_level_t<Level>, c_string_t<Name...>, PT, c_string_t<Loc...>>::
      set(PT begin, PT end, double const total_progress)
  {
    ASSERT((PT{} <= begin) && (begin <= end) && (end <= total_));

    begin_ = std::move(begin);
    end_   = std::move(end);

    notify_monitor(total_progress);
  }

  template<phase_level_t Level, char... Name, typename PT, char... Loc>
  inline void
  phase<c_phase_level_t<Level>, c_string_t<Name...>, PT, c_string_t<Loc...>>::
      notify_monitor(double const total_progress) const
  {
    auto const map_offset = [this](PT const& x) {
      if (x == PT{}) {
        return 0.0;
      }
      else if (x == total_) {
        return 1.0;
      }
      else {
        return static_cast<double>(x) / static_cast<double>(total_);
      }
    };

    phase_receptor::notify_progress(
        level, phase_id, map_offset(begin_), map_offset(end_), total_progress);
  }

  template<phase_level_t Level, typename... Ts>
  inline void phase_status(c_phase_level_t<Level> level, Ts&&... str_args)
  {
    phase_receptor::notify_status(
        level, c_hash_value<0>, std::forward<Ts>(str_args)...);
  }
}
