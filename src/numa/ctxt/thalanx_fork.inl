namespace numa {
  /*
   * thalanx_fork static
   */
  namespace detail {
    template<index_t... Is, typename F, typename... Fs>
    requires(std::Invocable<F> && (std::Invocable<Fs> && ...) && sizeof...(Is) == sizeof...(Fs) && sizeof...(Is) >= 1) auto thalanx_fork_static_impl(
        std::integer_sequence<index_t, Is...>,
        std::span<index_t const, sizeof...(Fs) + 1> cc,
        F&& f,
        Fs&&... fs)
    {
      thalanx_impl_scope th_scope(cc, sizeof...(Fs) + 1);

      std::tuple futures = {
          th_scope.launch_slave(Is + 1, std::forward<Fs>(fs))...};

      using R = std::invoke_result_t<F>;

      auto retval_1 = [&] {
        if constexpr (std::Same<R, void>) {
          th_scope.launch_master(std::forward<F>(f));
          return void_return_t{};
        }
        else {
          return th_scope.launch_master(std::forward<F>(f));
        }
      }();

      /*
       * Join slave threads and return.
       *
       * Note: It is essential that "get" is called on all futures
       * before the thalanx_impl_scope d-tor is run.
       */
      auto retval = std::tuple{
          std::move(retval_1),
          void_return_get_future(std::get<Is>(futures))...};

      return void_return_make_tuple(std::move(retval));
    }
  }

  template<Chunking C, typename... Fs>
  requires(std::Invocable<Fs>&&...) auto thalanx_fork(C const& c, Fs&&... fs)
  {
    static_assert(
        sizeof...(Fs) >= 2, "numa::thalanx_fork: needs at least two functors.");

    return detail::thalanx_fork_static_impl(
        std::make_integer_sequence<index_t, sizeof...(Fs) - 1>{},
        chunking_apply(c, thalanx::size(), sizeof...(Fs)),
        std::forward<Fs>(fs)...);
  }

  template<Chunking C, typename F, typename... Args>
  requires(std::Invocable<F, Args>&&...) auto thalanx_fork(
      C const& c, F&& f, Args&&... args)
  {
    static_assert(
        sizeof...(Args) >= 2,
        "numa::thalanx_fork: needs at least two arguments.");

    return thalanx_fork(
        c, detail::arg_functor<F, Args>(f, std::forward<Args>(args))...);
  }

  /*
   * thalanx_fork dynamic
   */
  template<
      Chunking C,
      SubMode S,
      std::Invocable<index_t, index_t> F,
      Chunking RangeC>
  auto thalanx_fork(
      C const& c,
      index_t const num_teams,
      S sub_mode,
      F&& f,
      RangeC const& range_c,
      index_t const size)
  {
    auto const cc       = chunking_apply(c, thalanx::size(), num_teams);
    auto const range_cc = chunking_apply(range_c, size, num_teams);

    ASSERT(static_cast<index_t>(cc.size()) == num_teams);
    ASSERT(cc.size() == range_cc.size());

    detail::thalanx_impl_scope th_scope(cc, num_teams);

    using R = std::invoke_result_t<F, index_t, index_t>;

    std::vector<std::future<R>> slave_futures;

    for (index_t i = 1; i < num_teams; ++i) {
      index_t const from = range_cc[i - 1];
      index_t const till = range_cc[i];

      slave_futures.push_back(th_scope.launch_slave(i, [&f, from, till] {
        return chunk_invoke(S{}, f, as_till, from, till);
      }));
    }

    index_t const master_from = 0;
    index_t const master_till = range_cc.front();

    detail::vector_wrap<R> retval;
    retval.reserve(num_teams);

    auto f_master = [&f, master_from, master_till] {
      return chunk_invoke(S{}, f, as_till, master_from, master_till);
    };

    if constexpr (std::Same<R, void>) {
      th_scope.launch_master(std::move(f_master));
    }
    else {
      retval.vector_.push_back(th_scope.launch_master(std::move(f_master)));
    }

    for (auto& f: slave_futures) {
      if constexpr (std::Same<R, void>) {
        f.get();
      }
      else {
        retval.vector_.push_back(f.get());
      }
    }

    if constexpr (!std::Same<R, void>) {
      return std::move(retval.vector_);
    }
  }

  template<Chunking C, typename T, std::Invocable<T> F>
  auto thalanx_fork(C const& c, F&& f, std::span<T> const args)
  {
    return thalanx_fork(
        c,
        args.size(),
        as_size,
        [&f, args](auto const from, auto const size) {
          ASSERT(size == 1);
          return f(args[from]);
        },
        chunking_equal{},
        args.size());
  }

  template<
      Chunking C,
      typename T,
      std::Invocable<std::span<T>> F,
      Chunking RangeC>
  auto thalanx_fork(
      C const& c,
      index_t const num_teams,
      F&& f,
      RangeC const& range_c,
      std::span<T> const args)
  {
    return thalanx_fork(
        c,
        num_teams,
        as_size,
        [&f, args](auto const from, auto const size) {
          return f(args.subspan(from, size));
        },
        range_c,
        args.size());
  }
}
