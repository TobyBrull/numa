namespace numa {
  template<typename Context>
  bool thalanx_registry::register_context()
  {
    handlers_scope().push_back([](thalanx_thread& th) {
      auto const* parent = Context::thread_local_ptr();
      ASSERT(parent != nullptr);

      using S = thalanx_strand_t<Context>;

      return th.push_stack<S>(thalanx_remote_t{}, std::cref(*parent));
    });

    if constexpr (context_handles_forks<Context>) {
      handlers_fork_master().push_back(
          [](index_t const parent_tree_id,
             std::span<index_t const> child_tree_ids,
             fork_type const ft,
             index_t const num_jobs) {
            auto* ptr = Context::thread_local_ptr();
            ASSERT(ptr != nullptr);

            ptr->handle_fork(parent_tree_id, child_tree_ids, ft, num_jobs);
          });

      handlers_fork_per_thread().push_back([](index_t const parent_tree_id,
                                              index_t const child_tree_id,
                                              fork_type const ft,
                                              fork_rank const fr) {
        auto const* ptr = Context::thread_local_ptr();
        ASSERT(ptr != nullptr);

        switch (ft) {
          case fork_type::START: {
            ptr->handle_fork_became_active(child_tree_id);
          } break;

          case fork_type::STOP: {
            switch (fr) {
              case fork_rank::MASTER: {
                ptr->handle_fork_became_active(parent_tree_id);
              } break;

              case fork_rank::SLAVE: {
                ptr->handle_fork_became_active(stack_tree_invalid_id);
              } break;
            }
          } break;
        }
      });
    }

    return !handlers_scope().empty();
  }
}
