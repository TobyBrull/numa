namespace numa {
  template<typename F>
  requires std::InvocableR<
      F,
      bool,
      std::unordered_map<std::string, std::string> const&> void
  env_var_scope::traverse(F&& f)
  {
    detail::env_var_core::traverse(std::forward<F>(f));
  }

  /*
   * Free standing functions.
   */

  template<typename T>
  T env_var(std::string_view const name, c_type_t<T>)
  {
    if constexpr (c_type<T> == c_type<std::string_view>) {
      return detail::env_var_core::get(name);
    }
    else {
      auto const value = detail::env_var_core::get(name);

      THROW_IF(value.empty(), "numa::env_var: could not find variable ", name);

      std::istringstream iss(std::string{value});
      T retval;
      iss >> retval;
      return retval;
    }
  }

  template<typename T>
  std::optional<T> env_var_if(std::string_view const name, c_type_t<T>)
  {
    auto value = env_var(name);

    std::optional<T> retval;

    if (!value.empty()) {
      std::istringstream iss(std::string{value});
      T value;
      iss >> value;
      retval = std::move(value);
    }

    return retval;
  }

  template<typename T>
  auto env_var_default(
      std::string_view const name, T const& default_value, c_type_t<T> t)
  {
    if constexpr (std::is_constructible_v<std::string, T const&>) {
      auto const sv = env_var(name);

      if (sv.empty()) {
        return std::string(default_value);
      }
      else {
        return std::string(sv);
      }
    }
    else {
      auto opt = env_var_if(name, t);

      if (opt.has_value()) {
        return std::move(opt.value());
      }
      else {
        return default_value;
      }
    }
  }
}
