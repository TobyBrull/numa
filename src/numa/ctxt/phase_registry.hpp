#ifndef INCLUDE_GUARD_NUMA_CTXT_PHASE_REGISTRY_H_
#define INCLUDE_GUARD_NUMA_CTXT_PHASE_REGISTRY_H_

#include "phase_level.hpp"

#include "numa/boot/error.hpp"

#include <unordered_map>

namespace numa {
  class phase_registry {
   public:
    struct entry {
      std::string location_;
      std::string name_;
      phase_level_t level_;
    };

    static entry const& lookup_phase(hash_value_t const phase_id);

    static bool register_phase(
        hash_value_t const phase_id,
        std::string location,
        std::string name,
        phase_level_t level);

   private:
    static std::unordered_map<hash_value_t, entry> map_phase_;
  };
}

#include "phase_registry.inl"

#endif
