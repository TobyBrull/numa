#include "phase_record_parser.hpp"

namespace numa {
  phase_record_parser::phase_record_parser(duration_type const offset)
    : offset_(offset), stack_frames_(0)
  {
    std::size_t const maxi = std::numeric_limits<std::size_t>::max();
    stack_.push_back({0, maxi});
  }

  namespace {
    void append_args(std::string& str, std::string const& append_str)
    {
      if (!append_str.empty()) {
        if (str.empty()) {
          str = append_str;
        }
        else {
          str += ", ";
          str += append_str;
        }
      }
    }
  }

  std::size_t phase_record_parser::process_args(
      std::size_t arg_i,
      std::vector<phase_record_strand::entry_args> const& args)
  {
    ASSERT(arg_i < args.size());

    thread_local std::ostringstream oss;
    oss.str({});

    {
      auto const& aa = args[arg_i];
      oss << '\"' << aa.name_ << "\": " << aa.json_value_;

      ++arg_i;
    }

    ASSERT(stack_.size() > 1);
    auto const& sf = stack_.back();

    auto& ete = entries_trace_events_.at(sf.entry_trace_events_ind_);
    append_args(ete.pre_json_args_, oss.str());

    return arg_i;
  }

  void phase_record_parser::process_phase_record_strand(
      index_t const thread_id, phase_record_strand const& pls)
  {
    auto const& phases = pls.data();
    auto const& args   = pls.data_args();

    std::size_t phase_i       = 0;
    std::size_t const phase_n = phases.size();

    std::size_t arg_i       = 0;
    std::size_t const arg_n = args.size();

    for (; phase_i < phase_n; ++phase_i) {
      auto const& le = phases[phase_i];

      process_phase(thread_id, le);

      while ((arg_i < arg_n) && (args[arg_i].data_index_ == phase_i)) {
        arg_i = process_args(arg_i, args);
      }
    }

    ASSERT(arg_i == arg_n);
  }

  void
  phase_record_parser::to_json(std::ostream& os, trace_event_format const tef)
  {
    os << "{\n  \"traceEvents\": [";

    switch (tef) {
      case trace_event_format::duration: {
        to_json_trace_events_duration(os);
      } break;

      case trace_event_format::complete: {
        to_json_trace_events_complete(os);
      } break;
    }

    os << "\n  ],\n  \"stackFrames\": {";

    to_json_stack_frames(os);

    os << "\n  },\n  \"displayTimeUnit\": \"ns\"\n}";
  }

  void to_json_preamble(std::ostream& os, bool const first)
  {
    if (first) {
      os << "\n    ";
    }
    else {
      os << ",\n    ";
    }
  }

  double make_time_us(phase_record_parser::time_point_type tp)
  {
    return std::chrono::duration<double, std::micro>(tp.time_since_epoch())
        .count();
  }

  double make_time_us(phase_record_parser::duration_type d)
  {
    return std::chrono::duration<double, std::micro>(d).count();
  }

  void
  phase_record_parser::to_json_trace_events_duration(std::ostream& os) const
  {
    bool first = true;
    for (auto const& ete: entries_trace_events_) {
      to_json_preamble(os, first);

      bool const is_open = !is_bit_set(ete.phase_id_, 0_i);

      auto const& re =
          phase_registry::lookup_phase(clear_bit(ete.phase_id_, 0_i));

      os << "{\"pid\": 1, \"name\": \"" << re.name_;
      os << "\", \"cat\": \"";
      to_pretty(os, re.level_);
      os << "\", \"ph\": \"" << (is_open ? 'B' : 'E');
      os << "\", \"sf\": \"" << ete.stack_frame_id_;
      os << "\", \"tid\": " << ete.thread_id_;
      os << ", \"ts\": " << make_time_us(ete.time_point_);

      if (!ete.pre_json_args_.empty()) {
        os << ", \"args\": { " << ete.pre_json_args_ << " }";
      }

      os << '}';

      first = false;
    }
  }

  void
  phase_record_parser::to_json_trace_events_complete(std::ostream& os) const
  {
    bool first = true;
    for (std::size_t i = 0; i < entries_trace_events_.size(); ++i) {
      auto const& ete = entries_trace_events_[i];

      bool const is_open = !is_bit_set(ete.phase_id_, 0_i);

      if (is_open) {
        to_json_preamble(os, first);

        auto const& re =
            phase_registry::lookup_phase(clear_bit(ete.phase_id_, 0_i));

        os << "{\"pid\": 1, \"name\": \"" << re.name_;
        os << "\", \"cat\": \"";
        to_pretty(os, re.level_);
        os << "\", \"ph\": \"X"
              "\", \"sf\": \""
           << ete.stack_frame_id_;
        os << "\", \"tid\": " << ete.thread_id_;
        os << ", \"ts\": " << make_time_us(ete.time_point_);

        auto const& partner_ete = entries_trace_events_[ete.partner_];
        ASSERT(
            (is_bit_set(partner_ete.phase_id_, 0_i)) &&
            (clear_bit(partner_ete.phase_id_, 0_i) == ete.phase_id_) &&
            (partner_ete.partner_ == i));

        os << ", \"dur\": "
           << make_time_us(partner_ete.time_point_ - ete.time_point_);

        std::string args = ete.pre_json_args_;
        append_args(args, partner_ete.pre_json_args_);

        if (!args.empty()) {
          os << ", \"args\": { " << args << " }";
        }

        os << '}';

        first = false;
      }
    }
  }

  void phase_record_parser::to_json_stack_frames(std::ostream& os) const
  {
    index_t id = stack_frames_.root_id();

    bool first = true;

    while (id != stack_tree_invalid_id) {
      if (first) {
        os << "\n    \"";
        first = false;
      }
      else {
        os << ",\n    \"";
      }

      os << id << "\": { \"";

      index_t const parent_id = stack_frames_.get_parent(id);
      if (parent_id != stack_tree_invalid_id) {
        os << "parent\": \"" << parent_id << "\", \"";
      }

      hash_value_t const phase_id = stack_frames_.get(id);

      auto const& pr_e = phase_registry::lookup_phase(clear_bit(phase_id, 0_i));

      os << "name\": \"" << pr_e.location_ << "\", \"category\": \"";
      to_pretty(os, pr_e.level_);
      os << "\" }";

      id = stack_frames_.get_successor(id);
    }
  }

  void phase_record_parser::process_phase(
      index_t const thread_id, phase_record_strand::entry const& record_entry)
  {
    if (!is_bit_set(record_entry.phase_id_, 0_i)) {
      push_frame(thread_id, record_entry);
    }
    else {
      pop_frame(thread_id, record_entry);
    }
  }

  void phase_record_parser::push_frame(
      index_t const thread_id, phase_record_strand::entry const& record_entry)
  {
    /*
     * Get frame-id of current frame, which will be
     * the parent of the newly pushed frame.
     */
    ASSERT(!stack_.empty());
    index_t const parent_frame_id = stack_.back().frame_id_;

    hash_value_t const phase_id = record_entry.phase_id_;

    index_t const frame_id = [&] {
      index_t retval = stack_frames_.find_child(parent_frame_id, phase_id);

      if (retval == stack_tree_invalid_id) {
        retval = stack_frames_.insert_first_child(parent_frame_id, phase_id);
      }

      return retval;
    }();

    stack_.push_back({frame_id, entries_trace_events_.size()});

    entries_trace_events_.push_back(
        {record_entry.phase_id_,
         0,
         frame_id,
         thread_id,
         record_entry.time_point_ - offset_,
         std::string{}});
  }

  void phase_record_parser::pop_frame(
      index_t const thread_id, phase_record_strand::entry const& record_entry)
  {
    ASSERT(stack_.size() > 1);
    auto const& sf = stack_.back();

    ASSERT(
        set_bit(stack_frames_.get(sf.frame_id_), 0_i) ==
        record_entry.phase_id_);

    std::size_t const partner_ind = stack_.back().entry_trace_events_ind_;

    entries_trace_events_.at(partner_ind).partner_ =
        entries_trace_events_.size();

    entries_trace_events_.push_back(
        {record_entry.phase_id_,
         partner_ind,
         sf.frame_id_,
         thread_id,
         record_entry.time_point_ - offset_,
         std::string{}});

    stack_.pop_back();
  }
}
