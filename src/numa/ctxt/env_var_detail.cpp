#include "env_var_detail.hpp"

namespace numa::detail {
  std::string_view env_var_core::get(std::string_view const name)
  {
    node const* p = base::get().node_.get();

    while (p != nullptr) {
      auto const& data = p->data_;

      auto it = data.find(std::string{name});

      if (it != data.end()) {
        return std::string_view{it->second};
      }

      p = p->previous_.get();
    }

    return std::string_view{};
  }

  env_var_core::env_var_core(base b, thalanx_remote_t, env_var_core const& s)
    : base(std::move(b)), node_(s.node_)
  {
  }

  env_var_core::env_var_core(base b, root_t) : base(std::move(b)) {}

  env_var_core::env_var_core(base b, map_type data)
    : base(std::move(b))
    , node_(std::make_shared<node>(std::move(data), previous()->node_))
  {
  }

  env_var_core::node::node(map_type data, std::shared_ptr<const node> previous)
    : data_(std::move(data)), previous_(std::move(previous))
  {
  }
}
