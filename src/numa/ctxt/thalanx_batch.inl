namespace numa {
  /*
   * thalanx_batch static
   */
  namespace detail {
    template<typename F, typename... Fs>
    struct dyn_fs {
      using tuple_type = std::tuple<F, Fs...>;

      tuple_type fs_;

      std::tuple<
          void_return_invoke_result_t<F>,
          void_return_invoke_result_t<Fs>...>
          retval_;

      dyn_fs(F&& f, Fs&&... fs)
        : fs_(std::forward<F>(f), std::forward<Fs>(fs)...)
      {
      }

      constexpr static index_t N = sizeof...(Fs) + 1;

      template<index_t I>
      void operator()()
      {
        using R = std::invoke_result_t<std::tuple_element_t<I, tuple_type>>;

        if constexpr (std::Same<R, void>) {
          std::get<I>(fs_)();
        }
        else {
          std::get<I>(retval_) = std::get<I>(fs_)();
        }
      }
    };

    template<typename F, typename... Fs>
        requires std::Invocable<F> &&
        (std::Invocable<Fs> && ...) auto thalanx_batch_static_impl(
            std::span<index_t const> cc, F&& f, Fs&&... fs)
    {
      index_t const num_teams = cc.size();

      constexpr index_t num_jobs = (sizeof...(Fs) + 1);

      detail::thalanx_impl_scope th_scope(cc, num_jobs);

      dyn_fs<F, Fs...> dyna{std::forward<F>(f), std::forward<Fs>(fs)...};

      std::atomic<index_t> current_job{0};

      auto const worker = [&] {
        for (;;) {
          index_t const my_job =
              current_job.fetch_add(1, std::memory_order_relaxed);

          if (my_job >= num_jobs) {
            return;
          }

          dynamicizer<num_jobs>(dyna, my_job);
        }
      };

      std::vector<std::future<void>> slave_futures;

      for (index_t i = 1; i < num_teams; ++i) {
        slave_futures.push_back(th_scope.launch_slave(i, worker));
      }

      th_scope.launch_master(worker);

      for (auto& f: slave_futures) {
        f.get();
      }

      return void_return_make_tuple(std::move(dyna.retval_));
    }
  }

  template<Chunking C, typename... Fs>
  requires(std::Invocable<Fs>&&...) auto thalanx_batch(
      C const& c, index_t const num_teams, Fs&&... fs)
  {
    static_assert(
        sizeof...(Fs) >= 2,
        "numa::thalanx_batch: needs at least two functors.");

    return detail::thalanx_batch_static_impl(
        chunking_apply(c, thalanx::size(), num_teams), std::forward<Fs>(fs)...);
  }

  template<Chunking C, typename F, typename... Args>
  requires(std::Invocable<F, Args>&&...) auto thalanx_batch(
      C const& c, index_t const num_teams, F&& f, Args&&... args)
  {
    static_assert(
        sizeof...(Args) >= 2,
        "numa::thalanx_batch: needs at least two arguments.");

    return thalanx_batch(
        c,
        num_teams,
        detail::arg_functor<F, Args>(f, std::forward<Args>(args))...);
  }

  /*
   * thalanx_batch dynamic
   */
  template<
      Chunking C,
      SubMode S,
      std::Invocable<index_t, index_t> F,
      Chunking RangeC>
  auto thalanx_batch(
      C const& c,
      index_t const num_teams,
      S,
      F&& f,
      RangeC const& range_c,
      index_t const num_jobs,
      index_t const size)
  {
    using R = std::invoke_result_t<F, index_t, index_t>;
    detail::vector_wrap<R> retval(num_jobs);

    if (size == 0) {
      if constexpr (!std::Same<R, void>) {
        return std::move(retval.vector_);
      }
      else {
        return;
      }
    }

    auto const cc       = chunking_apply(c, thalanx::size(), num_teams);
    auto const range_cc = chunking_apply(range_c, size, num_jobs);

    ASSERT(static_cast<index_t>(cc.size()) == num_teams);
    ASSERT(static_cast<index_t>(range_cc.size()) == num_jobs);

    detail::thalanx_impl_scope th_scope(cc, num_jobs);

    std::atomic<index_t> current_job{0};

    auto const worker = [&f, &current_job, &range_cc, num_jobs, &retval] {
      for (;;) {
        index_t const my_job =
            current_job.fetch_add(1, std::memory_order_relaxed);

        if (my_job >= num_jobs) {
          return;
        }

        index_t const from = (my_job == 0) ? 0 : range_cc[my_job - 1];
        index_t const till = range_cc[my_job];

        if constexpr (std::Same<R, void>) {
          chunk_invoke(S{}, f, as_till, from, till);
        }
        else {
          retval.vector_[my_job] = chunk_invoke(S{}, f, as_till, from, till);
        }
      }
    };

    std::vector<std::future<void>> slave_futures;

    for (index_t i = 1; i < num_teams; ++i) {
      slave_futures.push_back(th_scope.launch_slave(i, worker));
    }

    th_scope.launch_master(worker);

    for (auto& f: slave_futures) {
      f.get();
    }

    if constexpr (!std::Same<R, void>) {
      return std::move(retval.vector_);
    }
  }

  template<Chunking C, typename T, std::Invocable<T> F>
  auto thalanx_batch(
      C const& c, index_t const num_teams, F&& f, std::span<T> const args)
  {
    return thalanx_batch(
        c,
        num_teams,
        as_size,
        [&f, args](auto const from, auto const size) {
          ASSERT(size == 1);
          return f(args[from]);
        },
        chunking_equal{},
        args.size(),
        args.size());
  }

  template<
      Chunking C,
      typename T,
      std::Invocable<std::span<T>> F,
      Chunking RangeC>
  auto thalanx_batch(
      C const& c,
      index_t const num_teams,
      F&& f,
      RangeC const& range_c,
      index_t const num_jobs,
      std::span<T> const args)
  {
    return thalanx_batch(
        c,
        num_teams,
        as_size,
        [&f, args](auto const from, auto const size) {
          return f(args.subspan(from, size));
        },
        range_c,
        num_jobs,
        args.size());
  }
}
