#ifndef INCLUDE_GUARD_NUMA_CTXT_TERMINAL_H_
#define INCLUDE_GUARD_NUMA_CTXT_TERMINAL_H_

#include <ostream>

namespace numa {
  /*
   * https://shiroyasha.svbtle.com/escape-sequences-a-quick-guide-1
   */
  constexpr inline char const* terminal_show_cursor = "\e[?25h";
  constexpr inline char const* terminal_hide_cursor = "\e[?25l";

  constexpr inline char const* terminal_line_up             = "\e[1A";
  constexpr inline char const* terminal_clear_line_till_end = "\e[0K";

  inline void terminal_lines_up(std::ostream& os, int count);

  class disable_cursor {
   public:
    disable_cursor(std::ostream& os);
    ~disable_cursor();

   private:
    std::ostream& os_;
  };
}

#include "terminal.inl"

#endif
