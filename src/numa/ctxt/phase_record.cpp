#include "phase_record.hpp"

#include "thalanx_thread.hpp"

namespace numa {
  phase_record::phase_record(
      std::multimap<index_t, phase_record_strand> strands)
    : strands_(std::move(strands))
  {
  }

  index_t phase_record::size() const
  {
    index_t retval = 0;

    for (auto const& pls: strands_) {
      retval += pls.second.size();
    }

    return retval;
  }

  phase_record_strand* phase_record::draw_strand(index_t const thread_id)
  {
    auto it = strands_.emplace(thread_id, phase_record_strand{});

    return &(it->second);
  }

  void
  phase_record::to_json(std::ostream& os, trace_event_format const tef) const
  {
    auto const offset = [&] {
      auto retval = duration_type::max();

      for (auto const& [thread_id, strand]: strands_) {
        (void)thread_id;

        if (!strand.data().empty()) {
          retval = std::min(
              retval, strand.data().front().time_point_.time_since_epoch());
        }
      }

      if (retval == duration_type::max()) {
        return duration_type::zero();
      }
      else {
        return retval;
      }
    }();

    phase_record_parser plp(offset);

    for (auto const& [thread_id, strand]: strands_) {
      plp.process_phase_record_strand(thread_id, strand);
    }

    plp.to_json(os, tef);
  }

  void phase_record::remove_overhead()
  {
    for (auto& [thread_id, strand]: strands_) {
      (void)thread_id;

      strand.remove_overhead();
    }
  }

  phase_record phase_record::remove_overhead_copy() const
  {
    std::multimap<index_t, phase_record_strand> retval;

    for (auto const& [thread_id, strand]: strands_) {
      retval.emplace_hint(
          retval.end(), thread_id, strand.remove_overhead_copy());
    }

    return phase_record(std::move(retval));
  }

  void to_pretty(std::ostream& os, phase_record const& pl)
  {
    for (auto const& [thread_id, strand]: pl.strands()) {
      os << "/-----------------------------\n";
      os << "thread-id: " << thread_id << '\n';
      to_pretty(os, strand);
    }
  }
}
