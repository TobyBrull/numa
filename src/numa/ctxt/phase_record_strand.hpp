#ifndef INCLUDE_GUARD_NUMA_CTXT_PHASE_LOG_STRAND_H_
#define INCLUDE_GUARD_NUMA_CTXT_PHASE_LOG_STRAND_H_

#include "phase_registry.hpp"

#include "numa/tmpl/hash.hpp"

#include <chrono>
#include <regex>
#include <vector>

namespace numa {
  using phase_clock_type = std::chrono::high_resolution_clock;

  /*
   * Definition of phase_record_strand.
   */
  class phase_record_strand {
   public:
    using time_point_type = std::chrono::time_point<phase_clock_type>;
    using duration_type   = typename time_point_type::duration;

    struct entry {
      entry() = default;
      entry(hash_value_t phase_id, time_point_type time_point);

      hash_value_t phase_id_      = 0;
      time_point_type time_point_ = {};
    };

    struct entry_args {
      std::size_t data_index_;

      std::string name_;
      std::string json_value_;
    };

    phase_record_strand() = default;
    phase_record_strand(std::vector<entry> data);

    bool is_empty() const;

    index_t size() const;

    void reserve(index_t size);

    std::vector<entry> const& data() const;
    std::vector<entry_args> const& data_args() const;

    void notify_now(hash_value_t phase_id);

    template<typename... Ts>
    void notify_args(std::string_view arg_names, Ts&&... str_args);

    duration_type duration_total() const;
    duration_type duration_overhead() const;
    duration_type duration_total_with_overhead() const;

    bool is_consistent() const;

    void remove_overhead();

    [[nodiscard]] phase_record_strand remove_overhead_copy() const;

   private:
    std::vector<entry> data_;
    std::vector<entry_args> data_args_;
  };

  void to_pretty(std::ostream& os, phase_record_strand const& pl);
}

#include "phase_record_strand.inl"

#endif
