namespace numa {
  namespace detail {
    template<typename F>
    constexpr void chunking_apply(
        F&& chunk_function, index_t const size, std::span<index_t> retval)
    {
      size_t const num_chunks = retval.size();

      ASSERT(num_chunks >= 1);

      index_t i = 0;
      std::forward<F>(chunk_function)(
          size, num_chunks, [&](index_t const from, index_t const till) {
            retval[i] = till;
            ++i;
          });

      ASSERT(i == retval.size());
    }
  }

  constexpr void
  chunking_equal::apply(index_t const size, std::span<index_t> retval) const
  {
    detail::chunking_apply(
        [](index_t const size, index_t const num_chunks, auto&& f) {
          chunk_equal<index_t>(
              size, num_chunks, as_till, std::forward<decltype(f)>(f));
        },
        size,
        retval);
  }

  constexpr void
  chunking_nth::apply(index_t const size, std::span<index_t> retval) const
  {
    detail::chunking_apply(
        [n   = n_,
         min = min_](index_t const size, index_t const num_chunks, auto&& f) {
          chunk_nth<index_t>(
              size, num_chunks, n, min, as_till, std::forward<decltype(f)>(f));
        },
        size,
        retval);
  }

  template<std::Range R>
  chunking_approx<R>::chunking_approx(R&& ratios, index_t const min)
    : ratios_(std::forward<R>(ratios)), min_(min)
  {
  }

  template<std::Range R>
  constexpr void
  chunking_approx<R>::apply(index_t const size, std::span<index_t> retval) const
  {
    detail::chunking_apply(
        [&](index_t const size, index_t const num_chunks, auto&& f) {
          chunk_approx<index_t>(
              size, ratios_, min_, as_till, std::forward<decltype(f)>(f));
        },
        size,
        retval);
  }

  template<Chunking C>
  std::vector<index_t>
  chunking_apply(C const& c, index_t const size, index_t const num_chunks)
  {
    THROW_IF_NOT(
        num_chunks >= 1,
        "numa::chunking_apply: num_chunks must be at least one.");

    std::vector<index_t> retval(num_chunks, 0);

    if constexpr (ChunkingFunctor<C>) {
      c.apply(size, std::span{retval});
    }
    else {
      static_assert(ChunkingSpan<C>);

      ASSERT(c.size() == retval.size());

      std::copy(c.begin(), c.end(), retval.begin());
    }

    return retval;
  }
}
