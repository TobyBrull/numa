namespace numa {
  template<typename T>
  template<typename... Us>
  env_scope<T>::env_scope(c_type_t<T>, Us&&... args)
    : env_scope(std::forward<Us>(args)...)
  {
  }

  template<typename T>
  template<typename... Us>
  env_scope<T>::env_scope(Us&&... args) : scope_(std::forward<Us>(args)...)
  {
  }

  template<typename T>
  T const& env(c_type_t<T> t)
  {
    return detail::env_impl(t);
  }
}
