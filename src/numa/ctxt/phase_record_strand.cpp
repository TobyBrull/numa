#include "phase_record_strand.hpp"

namespace numa {
  /*
   * phase_record_strand
   */
  phase_record_strand::phase_record_strand(std::vector<entry> data)
    : data_(std::move(data))
  {
  }

  phase_record_strand::entry::entry(
      hash_value_t const phase_id, time_point_type const time_point)
    : phase_id_(phase_id), time_point_(time_point)
  {
  }

  bool phase_record_strand::is_empty() const
  {
    return data_.empty();
  }

  index_t phase_record_strand::size() const
  {
    return data_.size();
  }

  void phase_record_strand::reserve(index_t const size)
  {
    data_.reserve(size);
    data_args_.reserve(size);
  }

  std::vector<phase_record_strand::entry> const&
  phase_record_strand::data() const
  {
    return data_;
  }

  std::vector<phase_record_strand::entry_args> const&
  phase_record_strand::data_args() const
  {
    return data_args_;
  }

  auto phase_record_strand::duration_total() const -> duration_type
  {
    return (duration_total_with_overhead() - duration_overhead());
  }

  auto phase_record_strand::duration_overhead() const -> duration_type
  {
    duration_type retval = duration_type::zero();

    time_point_type last;

    for (auto const& e: data_) {
      if (e.phase_id_ == 0) {
        ASSERT(last == time_point_type{});
        last = e.time_point_;
      }
      else if (e.phase_id_ == 1) {
        auto const to_add = (e.time_point_ - last);
        retval += to_add;
        last = time_point_type{};
      }
    }

    return retval;
  }

  auto phase_record_strand::duration_total_with_overhead() const
      -> duration_type
  {
    if (is_empty()) {
      return duration_type::zero();
    }
    else {
      return (data_.back().time_point_ - data_.front().time_point_);
    }
  }

  bool phase_record_strand::is_consistent() const
  {
    std::vector<hash_value_t> stack;

    auto tp = time_point_type::min();

    for (auto const& le: data_) {
      if (le.time_point_ > tp) {
        return false;
      }

      if (!is_bit_set(le.phase_id_, 0_i)) {
        stack.push_back(le.phase_id_);
      }
      else {
        if (stack.empty()) {
          return false;
        }
        else {
          if (set_bit(stack.back(), 0_i) != le.phase_id_) {
            return false;
          }
          else {
            stack.pop_back();
          }
        }
      }
    }

    return stack.empty();
  }

  namespace detail {
    template<
        typename DataType,
        typename DataArgsType,
        typename FData,
        typename FDataArgs>
    std::size_t remove_overhead_impl(
        DataType& data, DataArgsType& data_args, FData fd, FDataArgs fda)
    {
      if (data.empty()) {
        return 0;
      }

      auto oh_dur = phase_record_strand::duration_type::zero();

      std::size_t from      = 0;
      std::size_t into      = 0;
      std::size_t from_args = 0;
      std::size_t into_args = 0;

      for (; from < data.size(); ++from) {
        if (data[from].phase_id_ == 0) {
          /*
           * Initial overhead-start.
           */
          auto const oh_start = data[from].time_point_;

          std::size_t oh_count = 0;

          for (; from < data.size(); ++from) {
            if (data[from].phase_id_ == 0) {
              ++oh_count;
            }
            else if (data[from].phase_id_ == 1) {
              --oh_count;
            }

            if (oh_count == 0) {
              /*
               * End of initial overhead-start.
               */
              auto const oh_end = data[from].time_point_;
              oh_dur += (oh_end - oh_start);
              break;
            }
          }

          if (from == data.size()) {
            break;
          }
        }
        else {
          auto le_copy = data[from];
          le_copy.time_point_ -= oh_dur;
          fd(into, le_copy);
          ++into;
        }

        while ((from_args < data_args.size()) &&
               (data_args[from_args].data_index_ <= from)) {
          if (data_args[from_args].data_index_ == from) {
            auto ae_copy = data_args[from_args];

            ASSERT(into > 0);
            ae_copy.data_index_ = (into - 1);

            fda(into_args, ae_copy);
            ++into_args;
          }
          ++from_args;
        }
      }

      return into;
    }
  }

  void phase_record_strand::remove_overhead()
  {
    auto const into = detail::remove_overhead_impl(
        data_,
        data_args_,
        [this](auto const into, phase_record_strand::entry const& le) {
          data_[into] = le;
        },
        [this](auto const into_args, auto const& ae) {
          data_args_[into_args] = ae;
        });

    data_.resize(into);
  }

  phase_record_strand phase_record_strand::remove_overhead_copy() const
  {
    phase_record_strand retval;

    retval.data_.reserve(data_.size());
    retval.data_args_.reserve(data_args_.size());

    detail::remove_overhead_impl(
        data_,
        data_args_,
        [&retval](auto const into, phase_record_strand::entry const& le) {
          ASSERT(into == retval.data_.size());
          retval.data_.push_back(le);
        },
        [&retval](auto const into_args, auto const& ae) {
          ASSERT(into_args == retval.data_args_.size());
          retval.data_args_.push_back(ae);
        });

    return retval;
  }

  void to_pretty(std::ostream& os, phase_record_strand const& pl)
  {
    os << "phases:\n";

    {
      auto const& data = pl.data();
      auto const n     = data.size();

      for (std::size_t i = 0; i < n; ++i) {
        auto const& le = data[i];
        auto const& re =
            phase_registry::lookup_phase(clear_bit(le.phase_id_, 0_i));

        bool const is_open = !is_bit_set(le.phase_id_, 0_i);

        os << '[' << i << "] = " << (is_open ? '+' : '-') << " | "
           << re.location_ << ":" << re.name_ << " @ "
           << le.time_point_.time_since_epoch().count() << '\n';
      }
    }

    os << "args:\n";

    {
      auto const& data_args = pl.data_args();
      auto const n          = data_args.size();

      for (std::size_t i = 0; i < n; ++i) {
        auto const& lea = data_args[i];

        os << '[' << i << "] = phase_" << lea.data_index_ << ": " << lea.name_
           << " = " << lea.json_value_ << '\n';
      }
    }
  }
}
