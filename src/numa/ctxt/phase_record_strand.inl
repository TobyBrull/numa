namespace numa {
  namespace detail {
    inline std::ostringstream& notify_args_to_string_oss()
    {
      thread_local std::ostringstream oss;
      return oss;
    }

    template<typename T>
    auto notify_arg_to_string(T&& arg)
    {
      auto& oss = notify_args_to_string_oss();
      oss.str({});
      oss << '\"' << std::forward<T>(arg) << '\"';
      return oss.str();
    }
  }

  inline void phase_record_strand::notify_now(hash_value_t const phase_id)
  {
    if (data_.capacity() == data_.size()) {
      data_.emplace_back(0, phase_clock_type::now());
      data_.emplace_back(1, phase_clock_type::now());
    }
    data_.emplace_back(phase_id, phase_clock_type::now());
  }

  template<typename... Ts>
  inline void phase_record_strand::notify_args(
      std::string_view const arg_names, Ts&&... str_args)
  {
    if constexpr (sizeof...(Ts) > 0) {
      data_.emplace_back(0, phase_clock_type::now());

      ASSERT(data_.size() >= 2);
      auto const data_index = data_.size() - 2;

      {
        static std::regex const re{"\\s*,\\s*"};
        std::cregex_token_iterator arg_names_iter(
            arg_names.begin(), arg_names.end(), re, -1);
        std::cregex_token_iterator const arg_names_end;

        std::array<std::string, sizeof...(Ts)> json_values = {
            detail::notify_arg_to_string(std::forward<Ts>(str_args))...};

#define ERR_MSG                                   \
  "phase::arg: the argument names \"", arg_names, \
      "\" were probably parsed incorrectly."

        std::size_t i = 0;

        for (; arg_names_iter != arg_names_end; ++arg_names_iter, ++i) {
          THROW_IF_NOT(i < sizeof...(Ts), ERR_MSG);

          data_args_.push_back(
              {data_index, *arg_names_iter, std::move(json_values[i])});
        }

        THROW_IF_NOT(i == sizeof...(Ts), ERR_MSG);

#undef ERR_MSG
      }

      data_.emplace_back(1, time_point_type{});
      data_.back().time_point_ = phase_clock_type::now();
    }
  }
}
