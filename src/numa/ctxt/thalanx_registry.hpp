#ifndef INCLUDE_GUARD_NUMA_CTXT_THALANX_REGISTRY_H_
#define INCLUDE_GUARD_NUMA_CTXT_THALANX_REGISTRY_H_

#include "thalanx_remote_stacks.hpp"

#include "numa/boot/span.hpp"
#include "numa/boot/stack_tree.hpp"

#include <functional>

namespace numa {
  struct thalanx_remote_t {
  };

  template<typename T>
  struct thalanx_strand {
    using type = T;
  };

  template<typename T>
  using thalanx_strand_t = typename thalanx_strand<T>::type;

  enum class fork_type { START, STOP };

  enum class fork_rank { MASTER, SLAVE };

  template<typename Context>
  concept context_handles_forks = requires(
      Context* ptr,
      index_t id,
      std::span<index_t const> ids,
      fork_type ft,
      index_t num_jobs)
  {
    {ptr->handle_fork(id, ids, ft, num_jobs)};
    {ptr->handle_fork_became_active(id)};
  };

  namespace detail {
    class thalanx_impl_scope;
  }

  class thalanx_registry {
   public:
    template<typename Context>
    static bool register_context();

   private:
    friend class thalanx;
    friend class thalanx_fork;
    friend class detail::thalanx_impl_scope;

    /*
     * Handling forks
     */
    using handler_fork_master = std::function<void(
        index_t, std::span<index_t const>, fork_type, index_t)>;

    using handler_fork_per_thread =
        std::function<void(index_t, index_t, fork_type, fork_rank)>;

    static std::vector<handler_fork_master>& handlers_fork_master();

    static std::vector<handler_fork_per_thread>& handlers_fork_per_thread();

    static void execute_fork_master(
        index_t parent_tree_id,
        std::span<index_t const> child_tree_ids,
        fork_type,
        index_t num_jobs);

    static void execute_fork_per_thread(
        index_t parent_tree_id, index_t child_tree_id, fork_type, fork_rank);

    friend void detail_thalanx_registry_execute_fork_per_thread_start_slave(
        index_t parent_tree_id, index_t child_tree_id);

    friend void detail_thalanx_registry_execute_fork_per_thread_stop_slave(
        index_t parent_tree_id, index_t child_tree_id);

    /*
     * Handling remote context scopes
     */
    using scope_handler_t =
        std::function<thalanx_thread::remote_stack(thalanx_thread&)>;

    static std::vector<scope_handler_t>& handlers_scope();

    static thalanx_remote_stacks
    execute_handlers(std::span<std::shared_ptr<thalanx_thread>> t);
  };
}

#include "thalanx_registry.inl"

#endif
