#ifndef INCLUDE_GUARD_NUMA_CTXT_THALANX_BATCH_H_
#define INCLUDE_GUARD_NUMA_CTXT_THALANX_BATCH_H_

#include "thalanx_detail.hpp"

#include "numa/tmpl/dynamicizer.hpp"

namespace numa {
  /*
   * static
   */
  template<Chunking C, typename... Fs>
  requires(std::Invocable<Fs>&&...) auto thalanx_batch(
      C const& c, index_t num_teams, Fs&&... fs);

  template<Chunking C, typename F, typename... Args>
  requires(std::Invocable<F, Args>&&...) auto thalanx_batch(
      C const& c, index_t num_teams, F&& f, Args&&... args);

  /*
   * dynamic
   */
  template<
      Chunking C,
      SubMode S,
      std::Invocable<index_t, index_t> F,
      Chunking RangeC>
  auto thalanx_batch(
      C const& c,
      index_t num_teams,
      S,
      F&& f,
      RangeC const& range_c,
      index_t num_jobs,
      index_t size);

  /*
   * dynamic derived
   */
  template<Chunking C, typename T, std::Invocable<T> F>
  auto thalanx_batch(C const& c, index_t num_teams, F&& f, std::span<T> args);

  template<
      Chunking C,
      typename T,
      std::Invocable<std::span<T>> F,
      Chunking RangeC>
  auto thalanx_batch(
      C const& c,
      index_t num_teams,
      F&& f,
      RangeC const& range_c,
      index_t num_jobs,
      std::span<T> args);
}

#include "thalanx_batch.inl"

#endif
