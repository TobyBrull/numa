#include "progress_stack_strand.hpp"

#include <iomanip>

namespace numa {
  progress_stack_strand::progress_stack_strand()
  {
    data_.emplace_back(0);
    data_.back().cur_end_ = 1.0;
  }

  void to_stream_percentage(std::ostream& os, double const status)
  {
    os << std::setw(5) << std::setprecision(1) << std::fixed << (status * 100.0)
       << '%';
  }

  void to_pretty(std::ostream& os, progress_stack_strand const& stack)
  {
    os << std::setprecision(4);

    for (auto const& e: stack.data()) {
      os << std::setw(6) << e.cur_begin_ << ", " << std::setw(6) << e.cur_end_
         << ", " << std::setw(6) << e.cur_progress_ << ", " << std::setw(6)
         << e.total_progress_ << "|\n";
    }

    os << "status = " << stack.status().load() << '\n'
       << "history = " << stack.history();
  }
}
