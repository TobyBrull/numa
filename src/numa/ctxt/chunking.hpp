#ifndef INCLUDE_GUARD_NUMA_CTXT_CHUNKING_H_
#define INCLUDE_GUARD_NUMA_CTXT_CHUNKING_H_

#include "numa/tmpl/literals.hpp"

#include "numa/boot/chunk.hpp"

namespace numa {
  bool chunking_is_valid(index_t size, std::span<index_t const> chunking);

  struct chunking_equal {
    constexpr void apply(index_t size, std::span<index_t> retval) const;
  };

  struct chunking_nth {
    index_t n_;
    index_t min_ = 1;

    constexpr void apply(index_t size, std::span<index_t> retval) const;
  };

  struct chunking_front : public chunking_nth {
    chunking_front() : chunking_nth{0} {}
  };

  struct chunking_back : public chunking_nth {
    chunking_back() : chunking_nth{-1} {}
  };

  template<std::Range R>
  struct chunking_approx {
    chunking_approx(R&& ratios, index_t min = 1);

    R ratios_;
    index_t min_;

    constexpr void apply(index_t size, std::span<index_t> retval) const;
  };

  template<typename T>
  chunking_approx(std::initializer_list<T>, index_t)
      -> chunking_approx<std::initializer_list<T>>;

  template<typename T>
  chunking_approx(std::initializer_list<T>)
      -> chunking_approx<std::initializer_list<T>>;

  template<typename F>
  concept ChunkingFunctor =
      requires(F const& f, index_t const size, std::span<index_t> retval)
  {
    {f.apply(size, retval)};
  };

  template<typename T>
  concept ChunkingSpan =
      std::is_convertible<T, std::span<index_t const>>::value;

  template<typename T>
  concept Chunking = ChunkingFunctor<T> || ChunkingSpan<T>;

  template<Chunking C>
  std::vector<index_t>
  chunking_apply(C const&, index_t size, index_t num_chunks);
}

#include "chunking.inl"

#endif
