#ifndef INCLUDE_GUARD_NUMA_CTXT_PHASE_LOG_H_
#define INCLUDE_GUARD_NUMA_CTXT_PHASE_LOG_H_

#include "phase_record_parser.hpp"

#include <map>

namespace numa {
  class phase_record {
   public:
    using duration_type = typename phase_record_strand::duration_type;

    phase_record() = default;

    std::multimap<index_t, phase_record_strand> const& strands() const;
    std::multimap<index_t, phase_record_strand>& strands();

    phase_record_strand const& strand() const;
    phase_record_strand& strand();

    index_t size() const;

    /*
     * TODO: make thread-safe. More precisely, think about the case where
     * two threads run in parallel on the same phase_record, and both
     * create a new thalanx-scope at the same time.
     */
    phase_record_strand* draw_strand(index_t thread_id);

    /*
     * To be viewed by typing "about:tracing" into Google Chrome.
     *
     * https://github.com/catapult-project/catapult/wiki/Trace-Event-Format
     */
    void to_json(
        std::ostream& os,
        trace_event_format tef = trace_event_format::complete) const;

    /*
     * Do these implementations make sense???
     */
    void remove_overhead();

    [[nodiscard]] phase_record remove_overhead_copy() const;

   private:
    phase_record(std::multimap<index_t, phase_record_strand> strands);

    std::multimap<index_t, phase_record_strand> strands_;
  };

  void to_pretty(std::ostream& os, phase_record const& pl);
}

#include "phase_record.inl"

#endif
