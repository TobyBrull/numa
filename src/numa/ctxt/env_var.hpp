#ifndef INCLUDE_GUARD_NUMA_CTXT_ENV_VAR_H_
#define INCLUDE_GUARD_NUMA_CTXT_ENV_VAR_H_

#include "env_var_detail.hpp"

#include "numa/tmpl/literals.hpp"
#include "numa/tmpl/type_traits.hpp"

#include <type_traits>

namespace numa {
  //  using env_var_value = std::variant<
  //      double,
  //      index_t,
  //      std::string,
  //      std::vector<env_var_value>,
  //      >

  class env_var_scope {
   public:
    env_var_scope(
        std::initializer_list<std::pair<std::string, std::string>> map);

    env_var_scope(std::unordered_map<std::string, std::string> map);

    /*
     * Return value determines if traversal is supposed to continue.
     */
    template<typename F>
    requires std::InvocableR<
        F,
        bool,
        std::unordered_map<std::string, std::string> const&> static void
    traverse(F&& f);

    static void to_pretty(std::ostream& os);

    static void to_stream_unique(std::ostream& os);

   private:
    context<detail::env_var_core> scope_;
  };

  std::unordered_map<std::string, std::string> env_var_map_system();

  std::unordered_map<std::string, std::string>
  env_var_map_command_line(int argc, char const** argv);

  std::unordered_map<std::string, std::string> env_var_map_test(
      int argc,
      char const** argv,
      std::initializer_list<std::pair<std::string, std::string>>);

  /*
   * env_var
   */
  template<typename T = std::string_view>
  T env_var(std::string_view name, c_type_t<T> = {});

  /*
   * env_var_if
   */
  template<typename T>
  std::optional<T> env_var_if(std::string_view name, c_type_t<T> = {});

  /*
   * env_var_default
   */
  template<typename T>
  auto env_var_default(
      std::string_view name, T const& default_value, c_type_t<T> = {});
}

#include "env_var.inl"

#endif
