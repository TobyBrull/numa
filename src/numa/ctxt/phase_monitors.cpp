#include "phase_monitors.hpp"

namespace numa {
  /*
   * phase_monitor_debug
   */

  phase_monitor_debug::phase_monitor_debug(
      std::ostream& os, phase_level_t const min_level)
    : os_(os), scope_(min_level, min_level, get_delegate())
  {
  }

  void phase_monitor_debug::handle_status(
      phase_level_t const phase, std::string_view const msg)
  {
    to_pretty(os_, phase);
    os_ << ": " << msg << '\n';
  }

  void phase_monitor_debug::handle_progress(progress_stack const& ps)
  {
    os_ << "[status]\n";
    to_pretty(os_, ps);
  }

  /*
   * phase_monitor_observe
   */

  phase_monitor_observe::phase_monitor_observe(
      std::ostream& os,
      phase_level_t const min_level,
      phase_level_t const interrupt_level)
    : os_(os)
    , interrupt_level_(interrupt_level)
    , scope_(min_level, min_level, get_delegate())
  {
  }

  void phase_monitor_observe::handle_status(
      phase_level_t const level, std::string_view const sv)
  {
    messages_[level] = sv;

    redraw(nullptr);
  }

  void phase_monitor_observe::handle_progress(progress_stack const& ps)
  {
    status_ = ps.status();

    pe_(status_);

    redraw(&ps);
  }

  void phase_monitor_observe::redraw(progress_stack const* ps)
  {
    terminal_lines_up(os_, cursor_);

    os_ << terminal_clear_line_till_end;

    {
      terminal_lines_up(os_, offset_);
      int offset = 0;

      os_ << terminal_clear_line_till_end;
      to_pretty(os_, progress_result{status_, pe_.seconds_left()});
      os_ << '\n';
      ++offset;

      if (ps != nullptr) {
        auto const& st = ps->data();

        st.for_each(
            [&](auto const& node, index_t const id, index_t const depth) {
              const auto& [thread_id, strand] = node;

              os_ << terminal_clear_line_till_end;
              os_ << std::string(2 * depth, ' ');
              os_ << '[' << thread_id << "] ";
              to_stream_percentage(os_, strand.status());
              os_ << " [" << std::setw(2) << strand.data().size() << "]\n";
              ++offset;
            });
      }

      while (offset < offset_) {
        os_ << terminal_clear_line_till_end << '\n';
        ++offset;
      }

      offset_ = offset;
    }

    os_ << '\n';

    for (auto const& [phase, msg]: messages_) {
      os_ << terminal_clear_line_till_end;
      to_pretty(os_, phase);
      os_ << ": " << msg << '\n';
    }
    os_ << '\n';

    cursor_ = (static_cast<int>(messages_.size()) + 2);
  }

  /*
   * phase_monitor_profile
   */
  phase_monitor_profile::phase_monitor_profile(phase_level_t const min_level)
    : scope_(min_level, c_off, get_delegate())
  {
  }

  numa::phase_record const& phase_monitor_profile::phase_record() const
  {
    return scope_.record();
  }

  [[noreturn]] void phase_monitor_profile::handle_status(
      phase_level_t const, std::string_view const)
  {
    UNREACHABLE();
  }

  [[noreturn]] void
  phase_monitor_profile::handle_progress(progress_stack const& ps)
  {
    UNREACHABLE();
  }

  /*
   * Free-Standing Functions
   */
  void to_pretty(std::ostream& os, progress_result const& rr)
  {
    to_stream_percentage(os, rr.status_);
    os << " [";
    to_stream_duration_remaining(os, rr.duration_left_);
    os << " left]";
  }
}
