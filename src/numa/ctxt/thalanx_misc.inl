#include "numa/boot/error.hpp"

namespace numa {
  template<typename T>
  void thalanx_channel<T>::send(T data)
  {
    {
      std::unique_lock<std::mutex> lk{mtx_};
      cv_.wait(lk, [this] { return !data_.has_value(); });

      data_ = std::move(data);
    }

    cv_.notify_one();
  }

  template<typename T>
  T thalanx_channel<T>::receive()
  {
    std::unique_lock<std::mutex> lk{mtx_};
    cv_.wait(lk, [this] { return data_.has_value(); });

    T retval = std::move(*data_);
    data_.reset();

    lk.unlock();

    cv_.notify_one();

    return retval;
  }

  template<std::Invocable F>
  void soft_mutex::critical_section(F f)
  {
    bool const value = flag_.test_and_set(std::memory_order_acquire);

    if (value) {
      return;
    }

    try {
      f();

      flag_.clear(std::memory_order_release);
    }
    catch (...) {
      flag_.clear(std::memory_order_release);
      throw;
    }
  }
}
