namespace numa {
  /*
   * phase_delegate, execute
   */
  inline void phase_delegate::handle_status(
      phase_level_t const lvl, std::string_view const sv)
  {
    ASSERT(handler_status_ != nullptr);
    return handler_status_(self_, lvl, sv);
  }

  inline void phase_delegate::handle_progress(progress_stack const& ps)
  {
    ASSERT(handler_progress_ != nullptr);
    return handler_progress_(self_, ps);
  }

  /*
   * phase_delegate, execute if
   */
  inline void phase_delegate::handle_status_if(
      phase_level_t const lvl, std::string_view const sv)
  {
    if (handler_status_ != nullptr) {
      return handler_status_(self_, lvl, sv);
    }
  }

  inline void phase_delegate::handle_progress_if(progress_stack const& ps)
  {
    if (handler_progress_ != nullptr) {
      return handler_progress_(self_, ps);
    }
  }

  /*
   * phase_delegate constructor
   */
  inline phase_delegate::phase_delegate(
      void* const self,
      handler_status const h_status,
      handler_progress const h_progress)
    : self_(self), handler_status_(h_status), handler_progress_(h_progress)
  {
  }

  /*
   * phase_delegate_target
   */
  template<typename T>
  phase_delegate phase_delegate_target<T>::get_delegate()
  {
    return phase_delegate{this, &thunk_handle_status, &thunk_handle_progress};
  }

  template<typename T>
  void phase_delegate_target<T>::thunk_handle_status(
      void* self, phase_level_t const lvl, std::string_view const sv)
  {
    static_cast<T*>(self)->handle_status(lvl, sv);
  }

  template<typename T>
  void phase_delegate_target<T>::thunk_handle_progress(
      void* self, progress_stack const& ps)
  {
    static_cast<T*>(self)->handle_progress(ps);
  }
}
