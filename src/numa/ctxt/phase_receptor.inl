namespace numa {
  template<phase_level_t Level, hash_value_t PhaseId>
  inline void phase_receptor::notify_push(
      c_phase_level_t<Level>, c_hash_value_t<PhaseId>)
  {
    if constexpr (Level >= phase_boundary_stack) {
      stack_.push_back(PhaseId);
    }

    if (cur_level_record_ <= Level) {
      ASSERT(cur_phase_record_strand_ != nullptr);
      cur_phase_record_strand_->notify_now(PhaseId);
    }
  }

  template<phase_level_t Level, hash_value_t PhaseIdLowBit>
  inline void phase_receptor::notify_pop(
      c_phase_level_t<Level>, c_hash_value_t<PhaseIdLowBit>)
  {
    if constexpr (Level >= phase_boundary_stack) {
      ASSERT(
          (!stack_.empty()) && (set_bit(stack_.back(), 0_i) == PhaseIdLowBit));
      stack_.pop_back();
    }

    if (cur_level_record_ <= Level) {
      ASSERT(cur_phase_record_strand_ != nullptr);
      cur_phase_record_strand_->notify_now(PhaseIdLowBit);
    }
  }

  template<phase_level_t Level, hash_value_t PhaseId, typename... Ts>
  inline void phase_receptor::notify_status(
      c_phase_level_t<Level>, c_hash_value_t<PhaseId>, Ts&&... str_args)
  {
    if (Level < cur_level_record_) {
      return;
    }

    thread_local std::ostringstream oss;
    oss.str({});
    (oss << ... << std::forward<Ts>(str_args));

    ASSERT(cur_phase_receptor_main_ != nullptr);
    cur_phase_receptor_main_->update_status(Level, oss.str());
  }

  template<phase_level_t Level, hash_value_t PhaseId, typename... Ts>
  inline void phase_receptor::notify_args(
      c_phase_level_t<Level>,
      c_hash_value_t<PhaseId>,
      std::string_view names,
      Ts&&... str_args)
  {
    if (Level < cur_level_record_) {
      return;
    }

    ASSERT((!stack_.empty()) && (stack_.back() == PhaseId));

    ASSERT(cur_phase_record_strand_ != nullptr);
    cur_phase_record_strand_->notify_args(names, std::forward<Ts>(str_args)...);
  }

  template<phase_level_t Level, hash_value_t PhaseId>
  inline void phase_receptor::notify_progress_push(
      c_phase_level_t<Level>, c_hash_value_t<PhaseId>)
  {
    if (cur_level_progress_ <= Level) {
      ASSERT(cur_progress_stack_strand_ != nullptr);
      cur_progress_stack_strand_->push_back(PhaseId);
    }
  }

  template<phase_level_t Level, hash_value_t PhaseIdLowBit>
  inline void phase_receptor::notify_progress_pop(
      c_phase_level_t<Level>, c_hash_value_t<PhaseIdLowBit>)
  {
    if (cur_level_progress_ <= Level) {
      ASSERT(cur_progress_stack_strand_ != nullptr);
      cur_progress_stack_strand_->pop_back(PhaseIdLowBit);

      ASSERT(cur_phase_receptor_main_ != nullptr);
      cur_phase_receptor_main_->update_progress();
    }
  }

  template<phase_level_t Level, hash_value_t PhaseId>
  inline void phase_receptor::notify_progress(
      c_phase_level_t<Level>,
      c_hash_value_t<PhaseId>,
      double const base,
      double const next,
      double const total_progress)
  {
    if (cur_level_progress_ <= Level) {
      ASSERT(cur_progress_stack_strand_ != nullptr);
      cur_progress_stack_strand_->process(base, next, total_progress);

      ASSERT(cur_phase_receptor_main_ != nullptr);
      cur_phase_receptor_main_->update_progress();
    }
  }

  inline void phase_receptor::handle_fork(
      index_t const parent_tree_id,
      std::span<index_t const> const child_tree_ids,
      fork_type const ft,
      index_t const num_jobs)
  {
    if (cur_phase_receptor_main_ != nullptr) {
      auto& fork_tree = cur_phase_receptor_main_->progress().data();

      switch (ft) {
        case fork_type::START: {
          ASSERT(!child_tree_ids.empty());

          index_t const num_threads =
              static_cast<index_t>(child_tree_ids.size());

          double const total_progress =
              static_cast<double>(std::max(num_jobs, num_threads));

          fork_tree.get(parent_tree_id).strand_.push_back(0);
          fork_tree.get(parent_tree_id)
              .strand_.process(0.0, 1.0, total_progress);

          fork_tree.insert_first_child(
              stack_tree_explicit, parent_tree_id, child_tree_ids.front());

          for (std::ptrdiff_t i = 1; i < child_tree_ids.size(); ++i) {
            fork_tree.insert_next_sibling(
                stack_tree_explicit, child_tree_ids[i - 1], child_tree_ids[i]);
          }
        } break;

        case fork_type::STOP: {
          double progress_made = 0.0;
          fork_tree.for_each_child(
              parent_tree_id, [&progress_made](auto& e, index_t const id) {
                auto& strand = e.strand_;
                strand.finalize();
                progress_made += (1.0 + strand.history());
              });

          fork_tree.erase_all_children(parent_tree_id);

          auto& parent_strand = fork_tree.get(parent_tree_id).strand_;
          parent_strand.finish_upstream(progress_made);
          parent_strand.pop_back(1);

          cur_phase_receptor_main_->update_progress();
        } break;
      }
    }
  }

  inline void phase_receptor::handle_fork_became_active(index_t const id) const
  {
    if (cur_phase_receptor_main_ != nullptr) {
      if (id == stack_tree_invalid_id) {
        cur_progress_stack_strand_ = nullptr;
      }
      else {
        cur_progress_stack_strand_ =
            &(cur_phase_receptor_main_->progress().data().get(id).strand_);
      }
    }
  }

  /*
   * phase_receptor_main
   */
  inline phase_record const& phase_receptor_main::record() const
  {
    return phase_record_;
  }

  inline progress_stack& phase_receptor_main::progress()
  {
    return progress_stack_;
  }

  inline progress_stack const& phase_receptor_main::progress() const
  {
    return progress_stack_;
  }
}
