#ifndef INCLUDE_GUARD_NUMA_CTXT_PROGRESS_STACK_STRAND_H_
#define INCLUDE_GUARD_NUMA_CTXT_PROGRESS_STACK_STRAND_H_

#include "numa/tmpl/hash.hpp"

#include <atomic>
#include <deque>
#include <iosfwd>
#include <ostream>

namespace numa {
  class progress_stack_strand {
   public:
    struct entry {
      hash_value_t id_;

      double cur_begin_ = 0.0;
      double cur_end_   = 0.0;

      double cur_progress_   = 0.0;
      double total_progress_ = 1.0;

      entry(hash_value_t id);
    };

    progress_stack_strand();

    progress_stack_strand(progress_stack_strand&&)      = delete;
    progress_stack_strand(progress_stack_strand const&) = delete;
    progress_stack_strand& operator=(progress_stack_strand&&) = delete;
    progress_stack_strand& operator=(progress_stack_strand const&) = delete;

    std::atomic<double> const& status() const;

    std::deque<entry> const& data() const;

    void finalize();

    double history() const;
    void finish_upstream(double progress_made);

    void push_back(hash_value_t id);
    void pop_back(hash_value_t id_low_bit);
    void process(double begin, double end, double total_progress);

    void update(double status);

    bool is_empty() const;

   private:
    double filter(double status);
    double filter_length(double length);

    void checkout_history(entry const& e);

    std::atomic<double> status_ = 0.0;

    /*
     * As multiple threads may be updating the status_ fields
     * at the same time, a data structure is needed that does
     * not invalidate references on resize.
     */
    std::deque<entry> data_;

    double history_ = 0.0;
  };

  void to_stream_percentage(std::ostream&, double status);

  void to_pretty(std::ostream&, progress_stack_strand const&);
}

#include "progress_stack_strand.inl"

#endif
