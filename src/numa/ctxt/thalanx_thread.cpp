#include "thalanx_thread.hpp"

#include "thalanx.hpp"

#include "numa/boot/error.hpp"

namespace numa {
  void detail_thalanx_registry_execute_fork_per_thread_start_slave(
      index_t const parent_tree_id, index_t const child_tree_id)
  {
    thalanx_registry::execute_fork_per_thread(
        parent_tree_id, child_tree_id, fork_type::START, fork_rank::SLAVE);
  }

  void detail_thalanx_registry_execute_fork_per_thread_stop_slave(
      index_t const parent_tree_id, index_t const child_tree_id)
  {
    thalanx_registry::execute_fork_per_thread(
        parent_tree_id, child_tree_id, fork_type::STOP, fork_rank::SLAVE);
  }

  class thread_core {
   public:
    thread_core(thalanx_thread* tth) : tth_(tth) {}

    void operator()()
    {
      thalanx_thread::this_thread_id_      = tth_->id_;
      thalanx_thread::this_thread_tree_id_ = stack_tree_invalid_id;

      loop();
    }

   private:
    void loop()
    {
      bool stop = false;
      while (!stop) {
        auto dd = tth_->channel_.receive();

        check_stack_id(dd.stack_id_, dd.task_.valid());

        if (!dd.task_.valid()) {
          ASSERT(dd.reserve_.empty());
          stop = true;
        }
        else {
          std::optional<thalanx> tx;

          if (!dd.reserve_.empty()) {
            tx.emplace(
                std::move(dd.reserve_), thalanx::no_execute_handlers_t{});
          }

          std::function<void()> callback = [this] {
            ++(thalanx_thread::this_thread_stack_id_);

            this->loop();

            --(thalanx_thread::this_thread_stack_id_);
          };

          dd.task_(std::move(callback));
        }

        check_stack_id(dd.stack_id_, dd.task_.valid());
      }
    }

    void check_stack_id(index_t const stack_id, bool const valid_task) const
    {
      if (stack_id < 0) {
        ASSERT(stack_id == -1);
        ASSERT(valid_task);
      }
      else {
        index_t const expected_stack_id = [valid_task] {
          if (valid_task) {
            return (thalanx_thread::this_thread_stack_id_ + 1);
          }
          else {
            return (thalanx_thread::this_thread_stack_id_);
          }
        }();

        (void)expected_stack_id;
        ASSERT(stack_id == expected_stack_id);
      }
    }

    thalanx_thread* tth_;
  };

  thalanx_thread::thalanx_thread()
    : id_(next_id_.fetch_add(1)), thread_(thread_core{this})
  {
  }

  thalanx_thread::~thalanx_thread()
  {
    try {
      ASSERT(stack_id_ == 0);
      pop_stack(0);
      thread_.join();
    }
    catch (...) {
    }
  }

  void thalanx_thread::pop_stack(index_t const stack_id)
  {
    ASSERT(stack_id_ == stack_id);

    data dd({}, stack_id, {});

    channel_.send(std::move(dd));

    --stack_id_;
  }

  thalanx_thread::data::data(
      std::packaged_task<void(std::function<void()>)> task,
      index_t const stack_id,
      std::vector<std::shared_ptr<thalanx_thread>> reserve)
    : task_(std::move(task)), stack_id_(stack_id), reserve_(std::move(reserve))
  {
  }

  thalanx_thread::remote_stack::remote_stack(
      std::future<void> fut,
      index_t const stack_id,
      thalanx_thread* const parent)
    : fut_(std::move(fut)), stack_id_(stack_id), parent_(parent)
  {
    ASSERT(parent_ != nullptr);
  }

  thalanx_thread::remote_stack::remote_stack(remote_stack&& other)
    : fut_(std::move(other.fut_))
    , stack_id_(other.stack_id_)
    , parent_(other.parent_)
  {
    other.parent_ = nullptr;
  }

  auto thalanx_thread::remote_stack::operator=(remote_stack&& other)
      -> remote_stack&
  {
    fut_      = std::move(other.fut_);
    stack_id_ = other.stack_id_;
    parent_   = other.parent_;

    other.parent_ = nullptr;

    return (*this);
  }

  thalanx_thread::remote_stack::~remote_stack()
  {
    if (parent_ != nullptr) {
      parent_->pop_stack(stack_id_);
      fut_.get();
    }
  }

  std::atomic<index_t> thalanx_thread::next_id_ = 1;

  /*
   * Set to zero, as needed by the master-thread.
   * For all other threads, this will be reset in
   * the thread_core functor.
   */
  thread_local index_t thalanx_thread::this_thread_id_ = 0;

  thread_local index_t thalanx_thread::this_thread_tree_id_ = 0;

  thread_local index_t thalanx_thread::this_thread_stack_id_ = 0;
}
