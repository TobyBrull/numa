namespace numa {
  /*
   * context_base<T>
   */
  template<typename T>
  T* context_base<T>::thread_local_ptr()
  {
    return context_base<T>::current_;
  }

  template<typename T>
  T& context_base<T>::get()
  {
    ASSERT(context_base<T>::current_ != nullptr);
    return *(context_base<T>::current_);
  }

  template<typename T>
  T* context_base<T>::previous() const
  {
    return previous_;
  }

  template<typename T>
  context_base<T>::context_base(T* const previous) : previous_(previous)
  {
  }

  template<typename T>
  thread_local T* context_base<T>::current_ = [] {
    static_assert(
        std::is_base_of_v<context_base<T>, T>,
        "T must be derived from context_base<T>.");

    static_assert(
        std::is_constructible_v<context<T>, root_t>,
        "If T must be constructible from context<T> and root_t.");

    static thread_local context<T> retval(root);

    return &retval;
  }();

  template<typename T>
  bool ATTRIBUTE_USED context_base<T>::registered_ = [] {
    return thalanx_registry::register_context<context<T>>();
  }();

  /*
   * context<T> members.
   */
  template<typename T, typename U>
  context<T, U>::context(root_t r) : U(context_base<T>{nullptr}, r)
  {
  }

  template<typename T, typename U>
  template<typename... Args>
  context<T, U>::context(Args&&... args)
    : U(context_base<T>{context_base<T>::thread_local_ptr()},
        std::forward<Args>(args)...)
  {
    context_base<T>::current_ = this;

    static_cast<T*>(this)->context_became_active();

    remotes_ = thalanx::push_stack<context<T>>();
  }

  template<typename T, typename U>
  context<T, U>::~context()
  {
    T* prev = this->previous();

    /*
     * If not a root context.
     */
    if (prev != nullptr) {
      context_base<T>::current_ = prev;

      prev->context_became_active();
    }
  }
}
