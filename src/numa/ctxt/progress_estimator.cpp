#include "progress_estimator.hpp"

namespace numa {
  progress_estimator::duration_t progress_estimator::now()
  {
    return std::chrono::steady_clock::now().time_since_epoch();
  }

  progress_estimator::progress_estimator(
      duration_t const half_life, duration_t const base)
    : base_(base), lambda_(std::log(2) / half_life.count())
  {
    THROW_IF_NOT(
        (duration_t{0.001} <= half_life) && (half_life <= duration_t{86400.0}),
        "numa::progress_estimator: half_life has to be "
        "in interval [0.001, 86400.0] seconds");

    ASSERT(0.0 < lambda_);
  }

  void progress_estimator::operator()(double const status, duration_t const dur)
  {
    double const t1 = (dur - base_).count();
    double const y1 = status;

    double const h = (t1 - t0_);
    if (h > 0.0) {
      double const lh = (lambda_ * h);

      double const decay = std::exp(-lh);

      double const X_0_t0 = -std::expm1(-lh) / lambda_;

      double const X_1_t0 = (t1 - t0_ * decay - X_0_t0) / lambda_;

      double const X_2_t0 =
          (t1 * t1 - t0_ * t0_ * decay - 2.0 * X_1_t0) / lambda_;

      double const xi = ((y1 - y0_) / h);
      double const v  = (y0_ - xi * t0_);

      double const C_ty = (v * X_1_t0 + xi * X_2_t0);
      double const C_y  = (v * X_0_t0 + xi * X_1_t0);

      int_ty_ = decay * int_ty_ + C_ty;
      int_y_  = decay * int_y_ + C_y;
    }

    t0_ = t1;
    y0_ = y1;
  }

  auto
  progress_estimator::seconds_left(duration_t const now, mode const t) const
      -> duration_t
  {
    double const X_0 = -std::expm1(-lambda_ * t0_) / lambda_;
    double const X_1 = (t0_ - X_0) / lambda_;
    double const X_2 = (t0_ * t0_ - 2.0 * X_1) / lambda_;

    auto const [alpha, beta] = [&] {
      switch (t) {
        /*
         * Straight-forward linear-regression, with intercept.
         */
        case mode::simple: {
          double const det = (X_0 * X_2 - X_1 * X_1);

          if (det == 0.0) {
            return std::pair{0.0, 0.0};
          }

          double const alpha = (X_2 * int_y_ - X_1 * int_ty_) / det;
          double const beta  = (-X_1 * int_y_ + X_0 * int_ty_) / det;

          return std::pair{alpha, beta};
        }

        /*
         * As mode::simple, but constraining the regression line
         * to go through the most recent observation.
         */
        case mode::anchored: {
          double const nom =
              (int_ty_ - (y0_ * X_1) - (t0_ * int_y_) + (t0_ * y0_ * X_0));

          double const denom = (X_2 - (2.0 * t0_ * X_1) + (t0_ * t0_ * X_0));

          double const beta  = (nom / denom);
          double const alpha = (y0_ - beta * t0_);

          return std::pair{alpha, beta};
        }
      }

      UNREACHABLE();
    }();

    if ((beta > 0.0) && (alpha < 1.0)) {
      double const hits_one = (1.0 - alpha) / beta;

      return duration_t{hits_one} - (now - base_);
    }
    else {
      return duration_t{-1.0};
    }
  }
}

namespace numa {
  void to_stream_duration_remaining(
      std::ostream& os, progress_estimator::duration_t dur)
  {
    constexpr auto max_duration = std::chrono::years{10000};

    if ((dur < progress_estimator::duration_t::zero()) ||
        (dur > max_duration)) {
      os << "--h --m --s";
    }
    else {
      auto const hh = std::chrono::floor<std::chrono::hours>(dur);
      dur -= hh;
      auto const mm = std::chrono::floor<std::chrono::minutes>(dur);
      dur -= mm;
      auto const ss = std::chrono::duration_cast<std::chrono::seconds>(dur);

      bool had_output = false;

      ASSERT(hh >= std::chrono::hours::zero());
      if (hh > std::chrono::hours::zero()) {
        os << std::setw(2) << hh.count() << "h ";
        had_output = true;
      }
      else {
        os << "    ";
      }

      ASSERT(mm >= std::chrono::minutes::zero());
      if ((mm > std::chrono::minutes::zero()) || (had_output)) {
        os << std::setw(2) << mm.count() << "m ";
        had_output = true;
      }
      else {
        os << "    ";
      }

      os << std::setw(2) << ss.count() << 's';
    }
  }
}
