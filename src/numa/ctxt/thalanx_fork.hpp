#ifndef INCLUDE_GUARD_NUMA_CTXT_THALANX_FORK_H_
#define INCLUDE_GUARD_NUMA_CTXT_THALANX_FORK_H_

#include "thalanx_detail.hpp"

namespace numa {
  /*
   *   0   1   2   3   4
   * +---+---+---+---+---+
   * | o | x | x | x | x |
   * +---+---+---+---+---+
   */

  /*
   * If sizeof... (Fs) == 2, thalanx::size () == 7, and
   * chunking = {3, 5, 7}
   * then:
   *  * F will run with 2 sleepers,
   *  * Fs_1 will run with 1 sleeper, and
   *  * Fs_2 will run with 1 sleepers.
   */
  template<Chunking C, typename... Fs>
  requires(std::Invocable<Fs>&&...) auto thalanx_fork(C const& c, Fs&&... fs);

  /*
   * Common functor with static arguments
   */
  template<Chunking C, typename F, typename... Args>
  requires(std::Invocable<F, Args>&&...) auto thalanx_fork(
      C const& c, F&& f, Args&&... args);

  /*
   * Common functor with dynamic integer range
   *
   * TODO: Use of Chunking for @p size demands that
   * Chunking can not only for for index_t.
   */
  template<
      Chunking C,
      SubMode S,
      std::Invocable<index_t, index_t> F,
      Chunking RangeC>
  auto thalanx_fork(
      C const& c,
      index_t num_teams,
      S,
      F&& f,
      RangeC const& range_c,
      index_t size);

  /*
   * Common functor with dynamic arguments
   */
  template<Chunking C, typename T, std::Invocable<T> F>
  auto thalanx_fork(C const& c, F&& f, std::span<T> args);

  template<
      Chunking C,
      typename T,
      std::Invocable<std::span<T>> F,
      Chunking RangeC>
  auto thalanx_fork(
      C const& c,
      index_t num_teams,
      F&& f,
      RangeC const& range_c,
      std::span<T> args);
}

#include "thalanx_fork.inl"

#endif
