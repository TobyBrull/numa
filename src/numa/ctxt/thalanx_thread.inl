namespace numa {
  inline index_t thalanx_thread::this_thread_id()
  {
    return this_thread_id_;
  }

  inline index_t thalanx_thread::id() const
  {
    return id_;
  }

  inline index_t thalanx_thread::this_thread_tree_id()
  {
    return this_thread_tree_id_;
  }

  void detail_thalanx_registry_execute_fork_per_thread_start_slave(
      index_t parent_tree_id, index_t child_tree_id);

  void detail_thalanx_registry_execute_fork_per_thread_stop_slave(
      index_t parent_tree_id, index_t child_tree_id);

  template<std::Invocable F>
  auto thalanx_thread::dispatch(
      F&& f,
      index_t parent_tree_id,
      index_t child_tree_id,
      std::vector<std::shared_ptr<thalanx_thread>> reserve)
  {
    using R = decltype(f());

    std::packaged_task<R()> pt(
        [parent_tree_id, child_tree_id, f = std::forward<F>(f)]() mutable {
          ASSERT(thalanx_thread::this_thread_tree_id_ == stack_tree_invalid_id);

          scope_guard sg1{
              [&] { thalanx_thread::this_thread_tree_id_ = child_tree_id; },
              [&] {
                thalanx_thread::this_thread_tree_id_ = stack_tree_invalid_id;
              }};

          scope_guard sg2{
              [&] {
                detail_thalanx_registry_execute_fork_per_thread_start_slave(
                    parent_tree_id, child_tree_id);
              },
              [&] {
                detail_thalanx_registry_execute_fork_per_thread_stop_slave(
                    parent_tree_id, child_tree_id);
              }};

          return f();
        });

    auto retval = pt.get_future();

    std::packaged_task<void(std::function<void()>)> task(
        [pt = std::move(pt)](auto) mutable { pt(); });

    data dd(std::move(task), -1, std::move(reserve));

    channel_.send(std::move(dd));

    return retval;
  }

  template<typename T, typename... Args>
  auto thalanx_thread::push_stack(Args&&... args) -> remote_stack
  {
    ++stack_id_;

    std::promise<void> stack_done;
    auto stack_done_fut = stack_done.get_future();

    std::packaged_task<void(std::function<void()>)> task(
        /*
         * Use perfect capture in C++20.
         */
        [&stack_done,
         args_tuple = std::tuple(std::forward<Args>(args)...)](auto callback) {
          std::apply(
              [&stack_done, &callback](auto&... args) {
                T stack_frame(args...);

                stack_done.set_value();

                callback();
              },
              args_tuple);
        });

    data dd(std::move(task), stack_id_, {});

    auto fut = dd.task_.get_future();

    channel_.send(std::move(dd));

    stack_done_fut.wait();

    return remote_stack(std::move(fut), stack_id_, this);
  }
}
