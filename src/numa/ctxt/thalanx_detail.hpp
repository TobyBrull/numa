#ifndef INCLUDE_GUARD_NUMA_CTXT_THALANX_DETAIL_H_
#define INCLUDE_GUARD_NUMA_CTXT_THALANX_DETAIL_H_

#include "thalanx.hpp"

#include "numa/tmpl/integer_list.hpp"
#include "numa/tmpl/type_traits.hpp"

namespace numa::detail {
  struct void_return_t {
  };

  template<typename T>
  auto void_return_get_future(std::future<T>&);

  template<typename F>
  struct void_return_invoke_result {
    using R = std::invoke_result_t<F>;

    using type = std::conditional_t<std::Same<R, void>, void_return_t, R>;
  };

  template<typename F>
  using void_return_invoke_result_t =
      typename void_return_invoke_result<F>::type;

  template<typename... Ts>
  auto void_return_make_tuple(std::tuple<Ts...>&& tt);

  template<typename T>
  struct vector_wrap {
    vector_wrap() = default;
    vector_wrap(index_t const size) : vector_(size){};

    void reserve(index_t const size)
    {
      vector_.reserve(size);
    }

    std::vector<T> vector_;
  };

  template<>
  struct vector_wrap<void> {
    vector_wrap() = default;
    vector_wrap(index_t const size){};

    void reserve(index_t const) {}
  };

  template<typename F, typename T>
  struct arg_functor {
    F& f_;
    T arg_;

    arg_functor(F& f, T&& arg) : f_(f), arg_(std::forward<T>(arg)) {}

    auto operator()()
    {
      return f_(std::forward<T>(arg_));
    }
  };

  class thalanx_impl_scope {
   public:
    thalanx_impl_scope(
        std::span<index_t const> const chunking, index_t const num_jobs);

    template<typename F>
    auto launch_slave(index_t const team_id, F&& f);

    template<typename F>
    auto launch_master(F&& f);

    ~thalanx_impl_scope();

   private:
    std::vector<std::shared_ptr<thalanx_thread>> const& rsv_;
    std::vector<std::tuple<index_t, index_t>> rsv_inds_;
    std::vector<index_t> tree_ids_;
    index_t parent_tree_id_;
  };
}

#include "thalanx_detail.inl"

#endif
