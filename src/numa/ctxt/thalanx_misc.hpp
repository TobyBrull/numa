#ifndef INCLUDE_GUARD_NUMA_CTXT_THALANX_CHANNEL_H_
#define INCLUDE_GUARD_NUMA_CTXT_THALANX_CHANNEL_H_

#include "numa/boot/bits.hpp"
#include "numa/boot/fwd.hpp"

#include <condition_variable>
#include <memory>
#include <mutex>
#include <optional>
#include <vector>

namespace numa {
  /*
   * Very simple single producer, single consumer channel,
   * that can can buffer at most one value of type T.
   */
  template<typename T>
  class thalanx_channel {
   public:
    /*
     * Blocks until data was received.
     */
    void send(T data);

    /*
     * Blocks until data was sent.
     */
    T receive();

   private:
    std::mutex mtx_;
    std::condition_variable cv_;

    std::optional<T> data_;
  };

  /*
   * Non-blocking mutex that skips the critical-section
   * if the mutex is already occupied.
   */
  class soft_mutex {
   public:
    soft_mutex() = default;

    template<std::Invocable F>
    void critical_section(F f);

   private:
    std::atomic_flag flag_ = ATOMIC_FLAG_INIT;
  };
}

#include "thalanx_misc.inl"

#endif
