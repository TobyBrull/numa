#ifndef INCLUDE_GUARD_NUMA_CTXT_PROGRESS_STACK_H_
#define INCLUDE_GUARD_NUMA_CTXT_PROGRESS_STACK_H_

#include "progress_stack_strand.hpp"

#include "numa/boot/stack_tree.hpp"

namespace numa {
  enum class progress_merge_mode { SUM = 0, MAX = 1, MIN = 2 };

  class progress_stack {
   public:
    struct entry {
      index_t tid_;
      progress_stack_strand strand_;
    };

    progress_stack(progress_merge_mode, index_t root_id);

    double status() const;

    stack_tree<entry>& data();
    stack_tree<entry> const& data() const;

    void update_from(index_t tree_id);

   private:
    progress_merge_mode pmm_;

    stack_tree<entry> data_;
  };

  void to_pretty(std::ostream&, progress_stack const&);
}

#include "progress_stack.inl"

#endif
