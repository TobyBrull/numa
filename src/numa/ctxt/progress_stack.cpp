#include "progress_stack.hpp"

namespace numa {
  void to_pretty(std::ostream& os, progress_stack const& ps)
  {
    ps.data().for_each(
        [&os](auto const& e, index_t const id, index_t const depth) {
          os << "------- [" << id << ", " << e.tid_ << "] -------\n";
          to_pretty(os, e.strand_);
          os << '\n';
        });
    os << "================== [" << ps.status() << "] ===================\n";
  }
}
