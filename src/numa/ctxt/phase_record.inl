namespace numa {
  inline std::multimap<index_t, phase_record_strand> const&
  phase_record::strands() const
  {
    return strands_;
  }

  inline std::multimap<index_t, phase_record_strand>& phase_record::strands()
  {
    return strands_;
  }

  inline phase_record_strand const& phase_record::strand() const
  {
    THROW_IF_NOT(
        strands_.size() == 1,
        "numa::phase_record: phase_record does not have "
        "single unique phase_record_strand");

    return strands_.begin()->second;
  }

  inline phase_record_strand& phase_record::strand()
  {
    THROW_IF_NOT(
        strands_.size() == 1,
        "numa::phase_record: phase_record does not have "
        "single unique phase_record_strand");

    return strands_.begin()->second;
  }
}
