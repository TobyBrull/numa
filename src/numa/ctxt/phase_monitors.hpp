#ifndef INCLUDE_GUARD_NUMA_CTXT_PHASE_MONITORS_H_
#define INCLUDE_GUARD_NUMA_CTXT_PHASE_MONITORS_H_

#include "phase_receptor.hpp"
#include "progress_estimator.hpp"
#include "terminal.hpp"

namespace numa {
  class phase_monitor_debug
    : private phase_delegate_target<phase_monitor_debug> {
   public:
    phase_monitor_debug(std::ostream& os, phase_level_t min_level = c_trace);

   private:
    friend phase_delegate_target<phase_monitor_debug>;

    void handle_status(phase_level_t, std::string_view sv);

    void handle_progress(progress_stack const& ps);

    std::ostream& os_;

    context<phase_receptor, phase_receptor_main> scope_;
  };

  class phase_monitor_observe
    : private phase_delegate_target<phase_monitor_observe> {
   public:
    phase_monitor_observe(
        std::ostream& os,
        phase_level_t min_level       = c_trace,
        phase_level_t interrupt_level = c_error);

   private:
    friend phase_delegate_target<phase_monitor_observe>;

    void handle_status(phase_level_t, std::string_view sv);

    void handle_progress(progress_stack const& ps);

    void redraw(progress_stack const* ps);

    std::ostream& os_;

    phase_level_t interrupt_level_;

    double status_ = 0.0;
    progress_estimator pe_;

    std::map<phase_level_t, std::string> messages_;

    int cursor_ = 0;
    int offset_ = 0;

    context<phase_receptor, phase_receptor_main> scope_;
  };

  class phase_monitor_profile
    : private phase_delegate_target<phase_monitor_profile> {
   public:
    phase_monitor_profile(phase_level_t min_level = c_profile);

    numa::phase_record const& phase_record() const;

   private:
    friend phase_delegate_target<phase_monitor_profile>;

    [[noreturn]] void handle_status(phase_level_t, std::string_view sv);
    [[noreturn]] void handle_progress(progress_stack const& ps);

    context<phase_receptor, phase_receptor_main> scope_;
  };

  struct progress_result {
    double status_;
    progress_estimator::duration_t duration_left_;
  };

  void to_pretty(std::ostream&, progress_result const&);
}

#endif
