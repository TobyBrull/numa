#ifndef INCLUDE_GUARD_NUMA_CTXT_THALANX_REMOTE_STACKS_H_
#define INCLUDE_GUARD_NUMA_CTXT_THALANX_REMOTE_STACKS_H_

#include "thalanx_thread.hpp"

namespace numa {
  /*
   * Should really be implemented using C's alloca().
   */
  template<typename T>
  class stack_vector {
   public:
    stack_vector(index_t capacity = 0);

    stack_vector(stack_vector&&) = default;
    stack_vector& operator=(stack_vector&&) = default;

    stack_vector(stack_vector const&) = delete;
    stack_vector& operator=(stack_vector const&) = delete;

    ~stack_vector();

    void push_back(T elem);

    template<typename... Args>
    void emplace_back(Args&&... args);

   private:
    std::vector<T> data_;
  };

  using thalanx_remote_stacks = stack_vector<thalanx_thread::remote_stack>;
}

#include "thalanx_remote_stacks.inl"

#endif
