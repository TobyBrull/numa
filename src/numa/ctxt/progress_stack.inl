namespace numa {
  inline progress_stack::progress_stack(
      progress_merge_mode const pmm, index_t const root_id)
    : pmm_(pmm), data_(stack_tree_explicit, root_id)
  {
  }

  inline auto progress_stack::data() -> stack_tree<entry>&
  {
    return data_;
  }

  inline auto progress_stack::data() const -> stack_tree<entry> const&
  {
    return data_;
  }

  inline double progress_stack::status() const
  {
    return data_.get(data_.root_id()).strand_.status();
  }

  inline void progress_stack::update_from(index_t const tree_id)
  {
    ASSERT(data_.is_valid(tree_id));

    index_t const parent_id = data_.get_parent(tree_id);

    if (parent_id == stack_tree_invalid_id) {
      return;
    }

    index_t child_id = data_.get_first_child(parent_id);

    double status = 0.0;

    while (child_id != stack_tree_invalid_id) {
      double const temp = data_.get(child_id).strand_.status();

      switch (pmm_) {
        case progress_merge_mode::SUM: {
          status += temp;
        } break;

        case progress_merge_mode::MAX: {
          status = std::max(status, temp);
        } break;

        case progress_merge_mode::MIN: {
          status = std::min(status, temp);
        } break;
      }

      child_id = data_.get_next_sibling(child_id);
    }

    data_.get(parent_id).strand_.update(status);

    update_from(parent_id);
  }
}
