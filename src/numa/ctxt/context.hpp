#ifndef INCLUDE_GUARD_NUMA_CTXT_CONTEXT_H_
#define INCLUDE_GUARD_NUMA_CTXT_CONTEXT_H_

#include "thalanx.hpp"
#include "thalanx_registry.hpp"

#include "numa/boot/bits.hpp"

namespace numa {
  struct root_t {
  };

  namespace {
    constexpr root_t root;
  }

  /*
   * Base.
   */
  template<typename T>
  class context_base {
   public:
    context_base(context_base&&) = default;

    context_base(context_base const&) = delete;
    context_base& operator=(context_base const&) = delete;
    context_base& operator=(context_base&&) = delete;

    static T* thread_local_ptr();

    static T& get();

    T* previous() const;

   private:
    template<typename OtherT, typename OtherU>
    friend class context;

    /*
     * May be overwritten by derived classes.
     *
     * Called whenevever this context becomes active,
     * except for the root. This is to avoid static
     * initialization race conditions.
     */
    constexpr void context_became_active() const {}

    context_base(T* previous);

    T* previous_ = nullptr;

    static thread_local T* current_;
    static bool registered_;
  };

  template<typename T, typename U = T>
  requires std::is_convertible_v<U*, T*> class context : public U {
   public:
    /*
     * TODO: Don't call thalanx::push_stack from this one.
     */
    // context (thalanx_remote_t, context const& T);

    context(root_t);

    template<typename... Args>
    context(Args&&... args);

    context(context const&) = delete;
    context(context&&)      = delete;
    context& operator=(context const&) = delete;
    context& operator=(context&&) = delete;

    ~context();

   private:
    thalanx_remote_stacks remotes_;
  };
}

#include "context.inl"

#endif
