namespace numa::detail {
  template<typename T>
  auto void_return_get_future(std::future<T>& fut)
  {
    if constexpr (std::Same<T, void>) {
      fut.get();
      return void_return_t{};
    }
    else {
      return fut.get();
    }
  }

  template<typename... Ts, index_t... NonVoidIs>
  auto
  make_tuple_void_return_impl(std::tuple<Ts...>&& t, index_list_t<NonVoidIs...>)
  {
    return std::tuple{std::move(std::get<NonVoidIs>(t))...};
  }

  template<typename... Ts>
  auto void_return_make_tuple(std::tuple<Ts...>&& tt)
  {
    constexpr type_list_t<decay_t<Ts>...> tl;

    auto non_void_il =
        filter_out(index_list_iota(c_index<sizeof...(Ts)>), [tl](auto const i) {
          return (get(tl, i) == c_type<void_return_t>);
        });

    if constexpr (!is_empty(non_void_il)) {
      return make_tuple_void_return_impl(std::move(tt), non_void_il);
    }
  }

  template<typename F>
  auto thalanx_impl_scope::launch_slave(index_t const team_id, F&& f)
  {
    auto slave_reserve = [&](auto const& tt) {
      auto const [from, till] = tt;
      ASSERT(from > 0);

      std::vector<std::shared_ptr<thalanx_thread>> retval;

      ASSERT(till > from);
      retval.reserve(till - from - 1);

      for (index_t i = from; i < (till - 1); ++i) {
        retval.push_back(rsv_[i]);
      }

      return retval;
    }(rsv_inds_[team_id]);

    return rsv_[std::get<0>(rsv_inds_[team_id]) - 1]->dispatch(
        std::forward<F>(f),
        parent_tree_id_,
        tree_ids_[team_id],
        std::move(slave_reserve));
  }

  template<typename F>
  auto thalanx_impl_scope::launch_master(F&& f)
  {
    auto const [from, till] = rsv_inds_.front();
    ASSERT(from == 0);

    std::vector<std::shared_ptr<thalanx_thread>> kept;

    ASSERT(till > from);
    kept.reserve(till - from - 1);

    for (index_t i = 0; i < (till - 1); ++i) {
      kept.push_back(rsv_[i]);
    }

    thalanx scp(std::move(kept), thalanx::no_execute_handlers_t{});

    scope_guard sg1{
        [&] { thalanx_thread::this_thread_tree_id_ = tree_ids_.front(); },
        [&] {
          thalanx_thread::this_thread_tree_id_ = parent_tree_id_;
        }};

    scope_guard sg2{
        [&] {
          thalanx_registry::execute_fork_per_thread(
              parent_tree_id_,
              tree_ids_.front(),
              fork_type::START,
              fork_rank::MASTER);
        },
        [&] {
          thalanx_registry::execute_fork_per_thread(
              parent_tree_id_,
              tree_ids_.front(),
              fork_type::STOP,
              fork_rank::MASTER);
        }};

    return std::forward<F>(f)();
  }
}
