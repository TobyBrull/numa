#ifndef INCLUDE_GUARD_NUMA_CTXT_ENV_VAR_DETAIL_H_
#define INCLUDE_GUARD_NUMA_CTXT_ENV_VAR_DETAIL_H_

#include "context.hpp"

#include <functional>
#include <string_view>

namespace numa::detail {
  class env_var_core : public context_base<env_var_core> {
   public:
    static std::string_view get(std::string_view name);

    template<typename F>
    requires std::InvocableR<
        F,
        bool,
        std::unordered_map<std::string, std::string> const&> static void
    traverse(F&& f);

    using map_type = std::unordered_map<std::string, std::string>;

   protected:
    using base = context_base<env_var_core>;

    env_var_core(base, thalanx_remote_t, env_var_core const& s);
    env_var_core(base, root_t);
    env_var_core(base, map_type data);

   private:
    struct node {
      node(map_type data, std::shared_ptr<const node> previous);

      map_type data_;

      std::shared_ptr<const node> previous_;
    };

    std::shared_ptr<const node> node_;
  };
}

#include "env_var_detail.inl"

#endif
