#include "chunking.hpp"

namespace numa {
  bool
  chunking_is_valid(index_t const size, std::span<index_t const> const chunking)
  {
    if (chunking.empty()) {
      return false;
    }

    if (!(chunking.front() > 0)) {
      return false;
    }

    for (std::ptrdiff_t i = 1; i < chunking.size(); ++i) {
      if (!(chunking[i - 1] < chunking[i])) {
        return false;
      }
    }

    if (!(chunking.back() == size)) {
      return false;
    }

    return true;
  }
}
