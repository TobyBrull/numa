namespace numa::detail {
  template<typename F>
  requires std::InvocableR<
      F,
      bool,
      std::unordered_map<std::string, std::string> const&> void
  env_var_core::traverse(F&& f)
  {
    node const* p = base::get().node_.get();

    while (p != nullptr) {
      auto const cont = f(p->data_);

      if (cont) {
        return;
      }

      p = p->previous_.get();
    }
  }
}
