#include "phase_receptor.hpp"

namespace numa {
  std::vector<std::string> phase_receptor::meh_stack()
  {
    std::vector<std::string> retval;

    for (auto const id: stack_) {
      retval.emplace_back(phase_registry::lookup_phase(id).name_);
    }

    return retval;
  }

  std::string phase_receptor::meh_str(char const delimiter)
  {
    std::string retval;

    for (auto const id: stack_) {
      retval += delimiter;
      retval += phase_registry::lookup_phase(id).name_;
    }

    return retval;
  }

  phase_receptor::phase_receptor(context_base<phase_receptor> b)
    : context_base<phase_receptor>(std::move(b))
  {
  }

  phase_receptor::phase_receptor(context_base<phase_receptor> b, root_t)
    : context_base<phase_receptor>(std::move(b))
  {
  }

  void phase_receptor::context_became_active()
  {
    /*
     * Only used by root-context.
     */

    ASSERT(phase_receptor_main_ == nullptr);
    ASSERT(phase_record_strand_ptr_ == nullptr);
    ASSERT(progress_stack_strand_ptr_ == nullptr);

    cur_phase_receptor_main_   = phase_receptor_main_;
    cur_phase_record_strand_   = phase_record_strand_ptr_;
    cur_progress_stack_strand_ = progress_stack_strand_ptr_;

    cur_level_record_   = c_off;
    cur_level_progress_ = c_off;
  }

  thread_local std::vector<hash_value_t> phase_receptor::stack_ = [] {
    std::vector<hash_value_t> retval;
    retval.reserve(200);
    return retval;
  }();

  thread_local phase_receptor_main* phase_receptor::cur_phase_receptor_main_ =
      nullptr;

  thread_local phase_record_strand* phase_receptor::cur_phase_record_strand_ =
      nullptr;

  thread_local progress_stack_strand*
      phase_receptor::cur_progress_stack_strand_ = nullptr;

  thread_local phase_level_t phase_receptor::cur_level_record_ =
      phase_level_t::off;

  thread_local phase_level_t phase_receptor::cur_level_progress_ =
      phase_level_t::off;

  /*
   * phase_receptor_main
   */
  phase_receptor_main::phase_receptor_main(
      base b,
      phase_level_t const level_record,
      phase_level_t const level_progress,
      phase_delegate const phase_delegate)
    : phase_receptor(std::move(b))
    , level_record_(level_record)
    , level_progress_(level_progress)
    , phase_record_()
    , progress_stack_(
          progress_merge_mode::SUM, thalanx_thread::this_thread_tree_id())
    , phase_delegate_(phase_delegate)
  {
    phase_receptor_main_ = this;

    phase_record_strand_ptr_ =
        phase_record_.draw_strand(thalanx_thread::this_thread_id());

    if (level_record_ < phase_level_t::off) {
      phase_record_strand_ptr_->reserve(20);
    }

    progress_stack_strand_ptr_ =
        &progress_stack_.data()
             .get(thalanx_thread::this_thread_tree_id())
             .strand_;
  }

  phase_receptor_main::~phase_receptor_main()
  {
    ASSERT(stack_.empty());
  }

  void phase_receptor_main::update_status(
      phase_level_t const lvl, std::string_view const msg)
  {
    {
      std::unique_lock lk{delegate_mtx_};

      phase_delegate_.handle_status_if(lvl, msg);
    }
  }

  void phase_receptor_main::update_progress()
  {
    progress_stack_.update_from(thalanx_thread::this_thread_tree_id());

    {
      std::unique_lock lk{delegate_mtx_};

      phase_delegate_.handle_progress_if(progress_stack_);
    }

    //    progress_mutex_.critical_section ([&]
    //        {
    //          phase_delegate_.handle_progress_if (progress_stack_);
    //        });
  }

  void phase_receptor_main::context_became_active()
  {
    ASSERT(phase_receptor_main_ == this);

    cur_phase_receptor_main_   = phase_receptor_main_;
    cur_phase_record_strand_   = phase_record_strand_ptr_;
    cur_progress_stack_strand_ = progress_stack_strand_ptr_;

    cur_level_record_   = level_record_;
    cur_level_progress_ = level_progress_;
  }

  /*
   * phase_receptor_strand
   */
  phase_receptor_strand::phase_receptor_strand(
      base b, thalanx_remote_t, phase_receptor const& pm)
    : phase_receptor(std::move(b))
  {
    phase_receptor_main_ = pm.phase_receptor_main_;

    if (phase_receptor_main_ != nullptr) {
      phase_record_strand_ptr_ =
          phase_receptor_main_->phase_record_.draw_strand(
              thalanx_thread::this_thread_id());
    }
  }

  void phase_receptor_strand::context_became_active()
  {
    cur_phase_receptor_main_   = phase_receptor_main_;
    cur_phase_record_strand_   = phase_record_strand_ptr_;
    cur_progress_stack_strand_ = progress_stack_strand_ptr_;

    if (phase_receptor_main_ != nullptr) {
      cur_level_record_   = phase_receptor_main_->level_record_;
      cur_level_progress_ = phase_receptor_main_->level_progress_;
    }
    else {
      cur_level_record_   = c_off;
      cur_level_progress_ = c_off;
    }
  }
}
