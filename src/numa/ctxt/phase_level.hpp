#ifndef INCLUDE_GUARD_NUMA_CTXT_PHASE_LEVEL_H_
#define INCLUDE_GUARD_NUMA_CTXT_PHASE_LEVEL_H_

#include "numa/tmpl/literals.hpp"

namespace numa {
  /*
   * Definition of phase types.
   */
  enum class phase_level_t {
    all = 0,

    profile = 1,
    trace   = 2,

    note = 3,

    paragraph = 4,
    section   = 5,
    chapter   = 6,
    volume    = 7,

    warning  = 9,
    error    = 10,
    critical = 11,

    overhead = 12,

    off = 13
  };

  template<phase_level_t v>
  using c_phase_level_t = c_integer_t<phase_level_t, v>;

  template<phase_level_t v>
  constexpr inline auto c_phase_level = c_phase_level_t<v>{};

  constexpr inline c_phase_level_t<phase_level_t::all> c_all = {};

  constexpr inline c_phase_level_t<phase_level_t::profile> c_profile = {};
  constexpr inline c_phase_level_t<phase_level_t::trace> c_trace     = {};

  constexpr inline c_phase_level_t<phase_level_t::note> c_note = {};

  constexpr inline c_phase_level_t<phase_level_t::paragraph> c_paragraph = {};
  constexpr inline c_phase_level_t<phase_level_t::section> c_section     = {};
  constexpr inline c_phase_level_t<phase_level_t::chapter> c_chapter     = {};
  constexpr inline c_phase_level_t<phase_level_t::volume> c_volume       = {};

  constexpr inline c_phase_level_t<phase_level_t::warning> c_warning   = {};
  constexpr inline c_phase_level_t<phase_level_t::error> c_error       = {};
  constexpr inline c_phase_level_t<phase_level_t::critical> c_critical = {};

  constexpr inline c_phase_level_t<phase_level_t::off> c_off = {};

  /*
   * Boundaries
   */
  constexpr inline auto phase_boundary_stack   = c_paragraph;
  constexpr inline auto phase_boundary_handler = c_note;
  constexpr inline auto phase_boundary_enabled = NUMA_PHASE_BOUNDARY_ENABLE;

  void to_pretty(std::ostream&, phase_level_t);
  void from_pretty(std::istream&, phase_level_t&);
}

#include "phase_level.inl"

#endif
