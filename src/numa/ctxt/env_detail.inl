namespace numa::detail {
  /*
   * static part
   */
  template<typename T>
  env_core<T>::env_core(base b, thalanx_remote_t, env_core<T> const& s)
    : base(std::move(b)), value_{s.value_}
  {
  }

  template<typename T>
  env_core<T>::env_core(base b, root_t) : base(std::move(b)), value_{}
  {
  }

  template<typename T>
  template<typename U, typename... Us>
  requires(!std::is_same_v<std::decay_t<U>, thalanx_remote_t>)
      env_core<T>::env_core(base b, U&& arg, Us&&... args)
    : base(std::move(b))
    , value_{std::forward<U>(arg), std::forward<Us>(args)...}
  {
  }

  template<typename T>
  void env_core<T>::context_became_active()
  {
    cur_value_ = value_;
  }

  template<typename T>
  T const& env_impl(c_type_t<T>)
  {
    return env_core<T>::cur_value_;
  }

  template<typename T>
  thread_local T env_core<T>::cur_value_ = {};
}
