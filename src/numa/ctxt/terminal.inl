namespace numa {
  inline void terminal_lines_up(std::ostream& os, int const count)
  {
    if (count > 0) {
      os << "\e[" << std::to_string(count) << "A";
    }
    else if (count < 0) {
      os << "\e[" << std::to_string(-count) << "B";
    }
  }

  inline disable_cursor::disable_cursor(std::ostream& os) : os_(os)
  {
    os_ << terminal_hide_cursor;
  }

  inline disable_cursor::~disable_cursor()
  {
    os_ << terminal_show_cursor;
  }
}
