#include "phase_registry.hpp"

namespace numa {
  bool phase_registry::register_phase(
      hash_value_t const phase_id,
      std::string location,
      std::string name,
      phase_level_t level)
  {
    const auto [it, inserted] = map_phase_.emplace(phase_id, entry{});

    THROW_IF_NOT(
        inserted,
        "phase_registry: hash collision between phases \"",
        location,
        "\" and \"",
        it->second.location_,
        "\"");

    it->second.location_ = std::move(location);
    it->second.name_     = std::move(name);
    it->second.level_    = level;

    return inserted;
  }

  std::unordered_map<hash_value_t, phase_registry::entry>
      phase_registry::map_phase_ = [] {
        std::unordered_map<hash_value_t, phase_registry::entry> retval;

        retval.emplace(
            0,
            entry{FILE_LINE ":overhead", "overhead", phase_level_t::overhead});

        return retval;
      }();
}
