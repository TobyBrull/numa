#include "env_var.hpp"

extern char** environ;

using namespace std::literals;

namespace numa {
  namespace {
    std::unordered_map<std::string, std::string> make_map_impl(
        std::initializer_list<std::pair<std::string, std::string>> const& ilist)
    {
      std::unordered_map<std::string, std::string> retval;
      retval.reserve(ilist.size());

      for (auto const& [name, value]: ilist) {
        retval.emplace(name, value);
      }

      return retval;
    }
  }

  env_var_scope::env_var_scope(
      std::initializer_list<std::pair<std::string, std::string>> ilist)
    : env_var_scope(make_map_impl(ilist))
  {
  }

  env_var_scope::env_var_scope(std::unordered_map<std::string, std::string> map)
    : scope_(std::move(map))
  {
  }

  namespace detail {
    void to_pretty(
        std::ostream& os,
        std::unordered_map<std::string, std::string> const& data)
    {
      for (auto const& [k, v]: data) {
        os << k << ": " << v << '\n';
      }
    }
  }

  void env_var_scope::to_pretty(std::ostream& os)
  {
    traverse([&os](auto const& data) {
      os << "-------\n";
      numa::detail::to_pretty(os, data);

      return false;
    });
  }

  void env_var_scope::to_stream_unique(std::ostream& os)
  {
    std::unordered_map<std::string, std::string> mm;

    traverse([&mm](auto const& data) {
      for (auto const& [k, v]: data) {
        if (mm.count(k) == 0) {
          mm[k] = v;
        }
      }

      return false;
    });

    numa::detail::to_pretty(os, mm);
  }

  namespace {
    void env_var_parse(
        std::unordered_map<std::string, std::string>& retval, char const* arg)
    {
      /*
       * Each entry has the form "NAME=VALUE".
       */
      std::string entry(arg);

      auto const pos = entry.find('=');

      if (pos != std::string::npos) {
        ASSERT(entry.size() >= pos + 1);

        auto name = entry.substr(0, pos);
        entry.erase(0, pos + 1);

        retval.emplace(std::move(name), std::move(entry));
      }
    }
  }

  std::unordered_map<std::string, std::string> env_var_map_system()
  {
    std::unordered_map<std::string, std::string> retval;

    for (char** p_entry = environ; *p_entry != nullptr; ++p_entry) {
      env_var_parse(retval, *p_entry);
    }

    return retval;
  }

  std::unordered_map<std::string, std::string>
  env_var_map_command_line(int const argc, char const** const argv)
  {
    std::unordered_map<std::string, std::string> retval;

    for (int i = 1; i < argc; ++i) {
      env_var_parse(retval, argv[i]);
    }

    return retval;
  }

  std::unordered_map<std::string, std::string> env_var_map_test(
      int const argc,
      char const** const argv,
      std::initializer_list<std::pair<std::string, std::string>> const il)
  {
    for (int i = 1; i < argc; ++i) {
      if (argv[i] == "test"s) {
        return make_map_impl(il);
      }
    }

    return {};
  }
}
