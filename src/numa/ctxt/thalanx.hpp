#ifndef INCLUDE_GUARD_NUMA_CTXT_THALANX_H_
#define INCLUDE_GUARD_NUMA_CTXT_THALANX_H_

#include "chunking.hpp"
#include "thalanx_registry.hpp"
#include "thalanx_thread.hpp"

#include "numa/boot/error.hpp"

namespace numa {
  namespace detail {
    class thalanx_impl_scope;
  }

  class thalanx {
   public:
    thalanx(index_t size);
    thalanx(std::vector<std::shared_ptr<thalanx_thread>> reserve);

    struct no_execute_handlers_t {
    };

    thalanx(
        std::vector<std::shared_ptr<thalanx_thread>> reserve,
        no_execute_handlers_t);

    ~thalanx();

    static std::vector<std::shared_ptr<thalanx_thread>> const& reserve();

    template<typename T>
    static thalanx_remote_stacks push_stack();

    /*
     * Always at least 1.
     */
    static index_t size();

    static stack_tree<index_t> const& tree();

   private:
    std::vector<std::shared_ptr<thalanx_thread>> reserve_;

    thalanx_remote_stacks remotes_;

    thalanx* previous_;

    static thread_local thalanx* current_;

    friend class thalanx_fork;
    friend class detail::thalanx_impl_scope;
    static stack_tree<index_t> tree_;
  };
}

#include "thalanx.inl"

#endif
