namespace numa {
  /*
   * thalanx
   */
  inline stack_tree<index_t> const& thalanx::tree()
  {
    return tree_;
  }

  template<typename T>
  thalanx_remote_stacks thalanx::push_stack()
  {
    auto const* parent = T::thread_local_ptr();
    ASSERT(parent != nullptr);

    using S = thalanx_strand_t<T>;

    if (thalanx* tx = current_; tx != nullptr) {
      thalanx_remote_stacks retval(tx->reserve_.size());

      for (auto& th: tx->reserve_) {
        retval.push_back(
            th->push_stack<S>(thalanx_remote_t{}, std::cref(*parent)));
      }

      return retval;
    }
    else {
      return {};
    }
  }
}
