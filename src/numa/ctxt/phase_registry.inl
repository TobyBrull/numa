namespace numa {
  inline phase_registry::entry const&
  phase_registry::lookup_phase(hash_value_t const phase_id)
  {
    return map_phase_.at(phase_id);
  }
}
