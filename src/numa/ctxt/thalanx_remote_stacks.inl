namespace numa {
  template<typename T>
  stack_vector<T>::stack_vector(index_t const capacity)
  {
    data_.reserve(capacity);
  }

  template<typename T>
  stack_vector<T>::~stack_vector()
  {
    while (!data_.empty()) {
      data_.erase(data_.end() - 1);
    }
  }

  template<typename T>
  void stack_vector<T>::push_back(T x)
  {
    data_.emplace_back(std::move(x));
  }

  template<typename T>
  template<typename... Args>
  void stack_vector<T>::emplace_back(Args&&... args)
  {
    data_.emplace_back(std::forward<Args>(args)...);
  }
}
