#include "thalanx_detail.hpp"

namespace numa::detail {
  thalanx_impl_scope::thalanx_impl_scope(
      std::span<index_t const> const chunking, index_t const num_jobs)
    : rsv_(thalanx::reserve())
    , rsv_inds_(chunking.size())
    , tree_ids_(chunking.size())
    , parent_tree_id_(thalanx_thread::this_thread_tree_id())
  {
    ASSERT(chunking_is_valid(thalanx::size(), chunking));

    /*
     * Setup global stack_tree.
     */
    {
      rsv_inds_[0] = std::tuple{0, chunking[0]};
      tree_ids_[0] = thalanx::tree_.insert_first_child(
          parent_tree_id_, thalanx_thread::this_thread_id());

      for (index_t i = 1; i < chunking.size(); ++i) {
        ASSERT(!tree_ids_.empty());

        index_t const from = chunking[i - 1];
        index_t const till = chunking[i];

        rsv_inds_[i] = std::tuple{from, till};

        tree_ids_[i] = thalanx::tree_.insert_next_sibling(
            tree_ids_[i - 1], rsv_[chunking[i - 1] - 1]->id());
      }
    }

    /*
     * Setup specialised stack_trees.
     */
    thalanx_registry::execute_fork_master(
        parent_tree_id_, tree_ids_, fork_type::START, num_jobs);
  }

  thalanx_impl_scope::~thalanx_impl_scope()
  {
    /*
     * Clean up specialised stack_trees.
     */
    thalanx_registry::execute_fork_master(
        parent_tree_id_, tree_ids_, fork_type::STOP, 0);

    /*
     * Clean up global stack_tree.
     */
    thalanx::tree_.erase_all_children(parent_tree_id_);
  }
}
