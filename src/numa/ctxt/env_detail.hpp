#ifndef INCLUDE_GUARD_NUMA_CTXT_ENV_DETAIL_H_
#define INCLUDE_GUARD_NUMA_CTXT_ENV_DETAIL_H_

#include "context.hpp"

#include "numa/tmpl/literals.hpp"

namespace numa::detail {
  template<typename T>
  class env_core : public context_base<env_core<T>> {
   protected:
    using base = context_base<env_core<T>>;

    env_core(base, thalanx_remote_t, env_core<T> const& s);
    env_core(base, root_t);

    template<typename U, typename... Us>
    requires(!std::is_same_v<std::decay_t<U>, thalanx_remote_t>)
        env_core(base, U&& arg, Us&&... args);

   private:
    friend class context<env_core<T>>;

    void context_became_active();

    T value_ = {};

    template<typename U>
    friend U const& env_impl(c_type_t<U>);

    static thread_local T cur_value_;
  };

  template<typename T>
  T const& env_impl(c_type_t<T>);
}

#include "env_detail.inl"

#endif
