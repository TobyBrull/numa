namespace numa {
  inline progress_stack_strand::entry::entry(hash_value_t const id) : id_(id) {}

  inline std::atomic<double> const& progress_stack_strand::status() const
  {
    return status_;
  }

  inline bool progress_stack_strand::is_empty() const
  {
    return data_.empty();
  }

  inline auto progress_stack_strand::data() const -> std::deque<entry> const&
  {
    return data_;
  }

  inline void progress_stack_strand::finalize()
  {
    ASSERT(data_.size() == 1);
    auto& e = data_.front();

    checkout_history(e);
  }

  inline double progress_stack_strand::history() const
  {
    return history_;
  }

  inline void progress_stack_strand::finish_upstream(double const progress_made)
  {
    ASSERT(!data_.empty());
    auto& e = data_.back();

    history_ += filter_length(std::max(0.0, progress_made - e.total_progress_));
  }

  inline void progress_stack_strand::push_back(hash_value_t const id)
  {
    data_.emplace_back(id);

    update(0.0);
  }

  inline void progress_stack_strand::pop_back(hash_value_t const id_low_bit)
  {
    {
      ASSERT(!data_.empty());
      auto& e = data_.back();

      checkout_history(e);

      ASSERT(set_bit(e.id_, 0_i) == id_low_bit);
    }

    data_.pop_back();

    {
      ASSERT(!data_.empty());
      auto& e = data_.back();

      e.cur_progress_ += 1.0;
    }

    update(0.0);
  }

  inline void progress_stack_strand::process(
      double const begin, double const end, double const total_progress)
  {
    ASSERT(!data_.empty());
    auto& e = data_.back();

    checkout_history(e);

    e.cur_begin_      = begin;
    e.cur_end_        = end;
    e.cur_progress_   = 0.0;
    e.total_progress_ = total_progress;

    update(0.0);
  }

  inline double progress_stack_strand::filter(double status)
  {
    for (auto it = data_.rbegin(); it != data_.rend(); ++it) {
      auto& e = (*it);

      status += e.cur_progress_;
      status /= e.total_progress_;
      status = e.cur_begin_ + (e.cur_end_ - e.cur_begin_) * status;
    }

    return status;
  }

  inline double progress_stack_strand::filter_length(double length)
  {
    for (auto it = data_.rbegin(); it != data_.rend(); ++it) {
      auto& e = (*it);

      length /= e.total_progress_;
      length *= (e.cur_end_ - e.cur_begin_);
    }

    return length;
  }

  inline void progress_stack_strand::checkout_history(entry const& e)
  {
    double const excess_progress = (e.cur_progress_ - e.total_progress_);

    if (excess_progress > 0.0) {
      history_ += filter_length(excess_progress);
    }
  }

  inline void progress_stack_strand::update(double status)
  {
    status_.store(filter(status) + history_);
  }
}
