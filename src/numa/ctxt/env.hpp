#ifndef INCLUDE_GUARD_NUMA_CTXT_ENV_H_
#define INCLUDE_GUARD_NUMA_CTXT_ENV_H_

#include "env_detail.hpp"

#include <sstream>
#include <string>
#include <unordered_map>

namespace numa {
  /*
   * static part
   */
  template<typename T>
  class env_scope {
   public:
    template<typename... Us>
    env_scope(c_type_t<T>, Us&&... args);

    template<typename... Us>
    env_scope(Us&&... args);

   private:
    context<detail::env_core<T>> scope_;
  };

  template<typename T>
  T const& env(c_type_t<T> = {});
}

#include "env.inl"

#endif
