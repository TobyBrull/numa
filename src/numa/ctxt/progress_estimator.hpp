#ifndef INCLUDE_GUARD_NUMA_CTXT_PROGRESS_ESTIMATOR_H_
#define INCLUDE_GUARD_NUMA_CTXT_PROGRESS_ESTIMATOR_H_

#include "numa/boot/error.hpp"

#include <chrono>
#include <cmath>

namespace numa {
  class progress_estimator {
   public:
    using duration_t = std::chrono::duration<double, std::ratio<1>>;

    constexpr static duration_t default_half_life = duration_t{10.0};

    static duration_t now();

    progress_estimator(
        duration_t half_life = default_half_life, duration_t base = now());

    void operator()(double status, duration_t when = now());

    enum mode { simple, anchored };

    duration_t
    seconds_left(duration_t from = now(), mode = mode::anchored) const;

   private:
    duration_t base_ = {};

    double t0_ = 0.0;
    double y0_ = 0.0;

    double lambda_ = 0.0;

    double int_ty_ = 0.0;
    double int_y_  = 0.0;
  };

  void to_stream_duration_remaining(
      std::ostream& os, progress_estimator::duration_t dur);
}

#endif
