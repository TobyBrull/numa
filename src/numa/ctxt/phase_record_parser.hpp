#ifndef INCLUDE_GUARD_NUMA_CTXT_PHASE_LOG_PARSER_H_
#define INCLUDE_GUARD_NUMA_CTXT_PHASE_LOG_PARSER_H_

#include "phase_record_strand.hpp"

#include "numa/tmpl/hash.hpp"

#include "numa/boot/stack_tree.hpp"

namespace numa {
  enum class trace_event_format { duration, complete };

  class phase_record_parser {
   public:
    using time_point_type = phase_record_strand::time_point_type;
    using duration_type   = phase_record_strand::duration_type;

    phase_record_parser(duration_type offset);

    void process_phase_record_strand(
        index_t thread_id, phase_record_strand const& pls);

    void to_json(std::ostream& os, trace_event_format const tef);

   private:
    void process_phase(index_t thread_id, phase_record_strand::entry const& le);

    std::size_t process_args(
        std::size_t arg_i,
        std::vector<phase_record_strand::entry_args> const& args);

    void to_json_trace_events_duration(std::ostream& os) const;
    void to_json_trace_events_complete(std::ostream& os) const;
    void to_json_stack_frames(std::ostream& os) const;

    void push_frame(index_t thread_id, phase_record_strand::entry const& le);
    void pop_frame(index_t thread_id, phase_record_strand::entry const& le);

    constexpr static index_t null_frame_id = -1;

    struct stack_frame {
      index_t frame_id_;

      std::size_t entry_trace_events_ind_;
    };

    struct entry_trace_events {
      hash_value_t phase_id_;

      std::size_t partner_;

      index_t stack_frame_id_;
      index_t thread_id_;
      time_point_type time_point_;
      std::string pre_json_args_;
    };

   private:
    duration_type offset_;

    std::vector<stack_frame> stack_;
    std::vector<entry_trace_events> entries_trace_events_;
    stack_tree<hash_value_t> stack_frames_;
  };
}

#endif
