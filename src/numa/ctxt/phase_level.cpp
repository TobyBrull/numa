#include "phase_level.hpp"

#include "numa/core/serialize_json.hpp"

namespace numa {
  void to_pretty(std::ostream& os, phase_level_t const level)
  {
    switch (level) {
      case phase_level_t::all:
        os << "all";
        break;
      case phase_level_t::profile:
        os << "profile";
        break;
      case phase_level_t::trace:
        os << "trace";
        break;
      case phase_level_t::note:
        os << "note";
        break;
      case phase_level_t::paragraph:
        os << "paragraph";
        break;
      case phase_level_t::section:
        os << "section";
        break;
      case phase_level_t::chapter:
        os << "chapter";
        break;
      case phase_level_t::volume:
        os << "volume";
        break;
      case phase_level_t::warning:
        os << "warning";
        break;
      case phase_level_t::error:
        os << "error";
        break;
      case phase_level_t::critical:
        os << "critical";
        break;
      case phase_level_t::overhead:
        os << "overhead";
        break;
      case phase_level_t::off:
        os << "off";
        break;
    }
  }

  void from_pretty(std::istream& is, phase_level_t& pl)
  {
    peek_till_non_whitespace(is);
    auto temp = read_while(is, [](char const c) { return std::isalnum(c); });

    if (temp == "all") {
      pl = phase_level_t::all;
    }
    else if (temp == "profile") {
      pl = phase_level_t::profile;
    }
    else if (temp == "trace") {
      pl = phase_level_t::trace;
    }
    else if (temp == "note") {
      pl = phase_level_t::note;
    }
    else if (temp == "paragraph") {
      pl = phase_level_t::paragraph;
    }
    else if (temp == "section") {
      pl = phase_level_t::section;
    }
    else if (temp == "chapter") {
      pl = phase_level_t::chapter;
    }
    else if (temp == "volume") {
      pl = phase_level_t::volume;
    }
    else if (temp == "warning") {
      pl = phase_level_t::warning;
    }
    else if (temp == "error") {
      pl = phase_level_t::error;
    }
    else if (temp == "critical") {
      pl = phase_level_t::critical;
    }
    else if (temp == "overhead") {
      pl = phase_level_t::overhead;
    }
    else if (temp == "off") {
      pl = phase_level_t::off;
    }
  }
}
