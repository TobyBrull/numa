#ifndef INCLUDE_GUARD_NUMA_CTXT_PHASE_DELEGATE_H_
#define INCLUDE_GUARD_NUMA_CTXT_PHASE_DELEGATE_H_

#include "phase_record.hpp"
#include "progress_stack.hpp"

namespace numa {
  class phase_delegate {
   public:
    using handler_status   = void (*)(void*, phase_level_t, std::string_view);
    using handler_progress = void (*)(void*, progress_stack const&);

    phase_delegate() = default;

    /*
     * Execute
     */
    void handle_status(phase_level_t, std::string_view sv);
    void handle_progress(progress_stack const&);

    /*
     * Execute if not null
     */
    void handle_status_if(phase_level_t, std::string_view sv);
    void handle_progress_if(progress_stack const&);

   private:
    template<typename T>
    friend class phase_delegate_target;

    phase_delegate(void* self, handler_status, handler_progress);

    void* self_ = nullptr;

    handler_status handler_status_     = nullptr;
    handler_progress handler_progress_ = nullptr;
  };

  template<typename T>
  class phase_delegate_target {
   public:
    phase_delegate get_delegate();

   private:
    static void
    thunk_handle_status(void* self, phase_level_t, std::string_view);

    static void thunk_handle_progress(void* self, progress_stack const&);
  };
}

#include "phase_delegate.inl"

#endif
