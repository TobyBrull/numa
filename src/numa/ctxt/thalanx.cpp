#include "thalanx.hpp"

namespace numa {
  /*
   * thalanx
   */
  namespace {
    std::vector<std::shared_ptr<thalanx_thread>>
    make_thread_vector(index_t const n)
    {
      std::vector<std::shared_ptr<thalanx_thread>> retval;
      retval.reserve(n);

      for (index_t i = 0; i < n; ++i) {
        retval.push_back(std::make_shared<thalanx_thread>());
      }

      return retval;
    }
  }

  thalanx::thalanx(index_t const size) : thalanx(make_thread_vector(size - 1))
  {
    THROW_IF_NOT(
        size >= 1, "numa::thalanx: size has to be greater or equal to 1.");
  }

  thalanx::thalanx(std::vector<std::shared_ptr<thalanx_thread>> reserve)
    : reserve_(std::move(reserve)), previous_(current_)
  {
    current_ = this;

    remotes_ = thalanx_registry::execute_handlers(reserve_);
  }

  thalanx::thalanx(
      std::vector<std::shared_ptr<thalanx_thread>> reserve,
      no_execute_handlers_t)
    : reserve_(std::move(reserve)), previous_(current_)
  {
    current_ = this;
  }

  thalanx::~thalanx()
  {
    current_ = previous_;
  }

  std::vector<std::shared_ptr<thalanx_thread>> const& thalanx::reserve()
  {
    if (current_ == nullptr) {
      static std::vector<std::shared_ptr<thalanx_thread>> const empty;

      return empty;
    }
    else {
      return current_->reserve_;
    }
  }

  index_t thalanx::size()
  {
    if (current_ == nullptr) {
      return 1;
    }
    else {
      return current_->reserve_.size() + 1;
    }
  }

  thread_local thalanx* thalanx::current_ = nullptr;

  stack_tree<index_t> thalanx::tree_{0};
}
