#ifndef INCLUDE_GUARD_NUMA_CTXT_PHASE_RECEPTOR_H_
#define INCLUDE_GUARD_NUMA_CTXT_PHASE_RECEPTOR_H_

#include "context.hpp"
#include "phase_delegate.hpp"

#include <functional>

namespace numa {
  class phase_receptor_main;

  class phase_receptor : public context_base<phase_receptor> {
   public:
    static std::vector<std::string> meh_stack();
    static std::string meh_str(char delimiter = '/');

   protected:
    friend class phase_receptor_strand;

    phase_receptor(context_base<phase_receptor>);
    phase_receptor(context_base<phase_receptor>, root_t);

    phase_receptor_main* phase_receptor_main_         = nullptr;
    phase_record_strand* phase_record_strand_ptr_     = nullptr;
    progress_stack_strand* progress_stack_strand_ptr_ = nullptr;

    /*
     * thread_local members
     */
    static thread_local std::vector<hash_value_t> stack_;

    static thread_local phase_receptor_main* cur_phase_receptor_main_;

    static thread_local phase_record_strand* cur_phase_record_strand_;
    static thread_local progress_stack_strand* cur_progress_stack_strand_;

    static thread_local phase_level_t cur_level_record_;
    static thread_local phase_level_t cur_level_progress_;

   private:
    template<typename OtherT, typename OtherU>
    requires std::is_convertible_v<OtherU*, OtherT*> friend class context;

    virtual void context_became_active();

    /*
     * Interface with phase.
     */
    template<typename Level, typename Name, typename PT, typename Loc>
    friend class phase;

    template<phase_level_t Level, typename... Ts>
    friend void phase_status(c_phase_level_t<Level>, Ts&&... vals);

    template<phase_level_t Level, hash_value_t PhaseId>
    static inline void
        notify_push(c_phase_level_t<Level>, c_hash_value_t<PhaseId>);

    template<phase_level_t Level, hash_value_t PhaseIdLowBit>
    static inline void
        notify_pop(c_phase_level_t<Level>, c_hash_value_t<PhaseIdLowBit>);

    template<phase_level_t Level, hash_value_t PhaseId, typename... Ts>
    static inline void notify_status(
        c_phase_level_t<Level>, c_hash_value_t<PhaseId>, Ts&&... str_args);

    template<phase_level_t Level, hash_value_t PhaseId, typename... Ts>
    static inline void notify_args(
        c_phase_level_t<Level>,
        c_hash_value_t<PhaseId>,
        std::string_view names,
        Ts&&... str_args);

    template<phase_level_t Level, hash_value_t PhaseId>
    static inline void
        notify_progress_push(c_phase_level_t<Level>, c_hash_value_t<PhaseId>);

    template<phase_level_t Level, hash_value_t PhaseId>
    static inline void
        notify_progress_pop(c_phase_level_t<Level>, c_hash_value_t<PhaseId>);

    template<phase_level_t Level, hash_value_t PhaseId>
    static inline void notify_progress(
        c_phase_level_t<Level>,
        c_hash_value_t<PhaseId>,
        double base,
        double next,
        double total_progress);

    friend class thalanx_registry;

    void handle_fork(
        index_t parent_tree_id,
        std::span<index_t const> child_tree_ids,
        fork_type const ft,
        index_t const num_jobs);
    void handle_fork_became_active(index_t id) const;
  };

  class phase_receptor_main : public phase_receptor {
   public:
    phase_record const& record() const;

    progress_stack& progress();
    progress_stack const& progress() const;

   protected:
    using base = context_base<phase_receptor>;

    phase_receptor_main(
        base,
        phase_level_t level_record,
        phase_level_t level_progress,
        phase_delegate);

    ~phase_receptor_main();

   private:
    friend class phase_receptor;
    friend class phase_receptor_strand;

    /*
     * Multi-threading-safe.
     */
    void update_status(phase_level_t, std::string_view);
    void update_progress();

    phase_level_t level_record_   = phase_level_t::off;
    phase_level_t level_progress_ = phase_level_t::off;

    phase_record phase_record_;
    progress_stack progress_stack_;

    // soft_mutex progress_mutex_;
    std::mutex delegate_mtx_;
    phase_delegate phase_delegate_;

    virtual void context_became_active() override final;
  };

  class phase_receptor_strand : public phase_receptor {
   protected:
    using base = context_base<phase_receptor>;

    phase_receptor_strand(base, thalanx_remote_t, phase_receptor const&);

   private:
    virtual void context_became_active() override final;
  };

  template<>
  struct thalanx_strand<context<phase_receptor>> {
    using type = context<phase_receptor, phase_receptor_strand>;
  };
}

#include "phase_receptor.inl"

#endif
