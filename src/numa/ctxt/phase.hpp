#ifndef INCLUDE_GUARD_NUMA_CTXT_PHASE_H_
#define INCLUDE_GUARD_NUMA_CTXT_PHASE_H_

#include "phase_receptor.hpp"

#include "numa/tmpl/type_traits.hpp"

namespace numa {
/*
 * Google-code base: 2bn LOC
 * Assumption: One would never write NUMA_PHASE more than every 100 LOC
 * This would require approximately 20mn hashes.
 *
 * Be aware that the code here uses one bit of the hash value to distinguish
 * between phase construction and phase destruction.
 *
 * According to http://preshing.com/20110504/hash-collision-probabilities/,
 * with 64bit hashes the collision probabilities are:
 *    Making 1.92mn hashes: 1 in 10mn
 *    Making 19.2mn hashes: 1 in 100k
 *    Making 192mn hashes: 1 in 1k
 *
 * Even if there is a hash collision, this is easy to fix.
 */

/*
 * Definition of phase.
 */
#define NUMA_LOCATION (FILE_LINE ""_s)

  template<typename Level, typename Name, typename PT, typename Loc>
  class phase;

  template<phase_level_t Level, char... Name, char... Loc>
  class phase<
      c_phase_level_t<Level>,
      c_string_t<Name...>,
      void,
      c_string_t<Loc...>> {
   public:
    inline phase(
        c_phase_level_t<Level>, c_string_t<Name...>, c_string_t<Loc...>);

    inline ~phase();

    constexpr static auto level    = c_phase_level<Level>;
    constexpr static auto name     = c_string<Name...>;
    constexpr static auto location = c_string<Loc...>;

    constexpr static auto phase_id =
        c_hash_value<clear_bit(hash(location + ":"_s + name), 0_i)>;

    template<typename... Ts>
    void status(Ts&&... str_args) const;

    template<typename... Ts>
    void args(std::string_view names, Ts&&... str_args) const;

#define NUMA_ARGS(...) args(#__VA_ARGS__, __VA_ARGS__)

   private:
    inline void notify_monitor() const;

    static bool registered_;
  };

  template<phase_level_t Level, char... Name, std::Arithmetic PT, char... Loc>
  class phase<
      c_phase_level_t<Level>,
      c_string_t<Name...>,
      PT,
      c_string_t<Loc...>>
    : public phase<
          c_phase_level_t<Level>,
          c_string_t<Name...>,
          void,
          c_string_t<Loc...>> {
   public:
    using baseclass = phase<
        c_phase_level_t<Level>,
        c_string_t<Name...>,
        void,
        c_string_t<Loc...>>;

    using baseclass::level;
    using baseclass::location;
    using baseclass::name;
    using baseclass::phase_id;

    inline phase(
        c_phase_level_t<Level>,
        c_string_t<Name...>,
        PT total,
        c_string_t<Loc...>);

    inline ~phase();

    void start(PT upcoming, double total_progress = 1.0);
    void set(PT begin, PT end, double total_progress = 1.0);

   private:
    inline void notify_monitor(double total_progress) const;

    PT total_ = {};
    PT begin_ = {};
    PT end_   = {};
  };

  template<typename Level, typename Name, typename Loc>
  phase(Level, Name, Loc) -> phase<Level, Name, void, Loc>;

  template<typename Level, typename Name, typename PT, typename Loc>
  phase(Level, Name, PT, Loc) -> phase<Level, Name, PT, Loc>;

  template<phase_level_t Level, typename... Ts>
  void phase_status(c_phase_level_t<Level>, Ts&&... str_args);

#define NUMA_VAR(x) #x " = ", x

#define NUMA_PROFILE_VAR(...) \
  numa::phase_status(c_profile, NUMA_VAR(__VA_ARGS__))
#define NUMA_TRACE_VAR(...) numa::phase_status(c_trace, NUMA_VAR(__VA_ARGS__))
}

#include "phase.inl"

#endif
