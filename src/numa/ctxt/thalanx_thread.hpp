#ifndef INCLUDE_GUARD_NUMA_CTXT_THALANX_THREAD_H_
#define INCLUDE_GUARD_NUMA_CTXT_THALANX_THREAD_H_

#include "thalanx_misc.hpp"

#include "numa/boot/bits.hpp"
#include "numa/boot/fwd.hpp"
#include "numa/boot/stack_tree.hpp"

#include <functional>
#include <future>
#include <thread>

namespace numa {
  namespace detail {
    class thalanx_impl_scope;
  }

  class thalanx_thread {
   public:
    class remote_stack {
     public:
      ~remote_stack();

      remote_stack(remote_stack&&);
      remote_stack& operator=(remote_stack&&);

      remote_stack(remote_stack const&) = delete;
      remote_stack& operator=(remote_stack const&) = delete;

     private:
      friend class thalanx_thread;

      remote_stack(
          std::future<void> fut, index_t stack_id, thalanx_thread* parent);

      std::future<void> fut_;
      index_t stack_id_;
      thalanx_thread* parent_;
    };

    thalanx_thread();
    ~thalanx_thread();

    /*
     * The internal std::thread takes a pointer this class.
     */
    thalanx_thread(thalanx_thread&&) = delete;
    thalanx_thread& operator=(thalanx_thread&&) = delete;
    thalanx_thread(thalanx_thread const&)       = delete;
    thalanx_thread& operator=(thalanx_thread const&) = delete;

    static index_t this_thread_id();
    index_t id() const;

    /*
     * Is stack_tree_invalid_id unless this thread is active
     * (and also a short period before and after becoming active).
     */
    static index_t this_thread_tree_id();

    template<std::Invocable F>
    auto dispatch(
        F&& f,
        index_t parent_tree_id,
        index_t child_tree_id,
        std::vector<std::shared_ptr<thalanx_thread>> reserve = {});

    template<typename T, typename... Args>
    remote_stack push_stack(Args&&... args);

   private:
    friend class thread_core;
    friend class thalanx_fork;
    friend class detail::thalanx_impl_scope;

    void pop_stack(index_t stack_id);

    static std::atomic<index_t> next_id_;
    thread_local static index_t this_thread_id_;
    index_t id_;

    thread_local static index_t this_thread_tree_id_;

    thread_local static index_t this_thread_stack_id_;
    index_t stack_id_ = 0;

    struct data {
      data(
          std::packaged_task<void(std::function<void()>)> task,
          index_t stack_id,
          std::vector<std::shared_ptr<thalanx_thread>> reserve);

      std::packaged_task<void(std::function<void()>)> task_;
      index_t stack_id_;
      std::vector<std::shared_ptr<thalanx_thread>> reserve_;
    };

    thalanx_channel<data> channel_;

    std::thread thread_;
  };
}

#include "thalanx_thread.inl"

#endif
