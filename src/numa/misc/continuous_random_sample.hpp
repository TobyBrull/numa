#ifndef INCLUDE_GUARD_NUMA_MISC_CONTINUOUS_RANDOM_SAMPLE_H_
#define INCLUDE_GUARD_NUMA_MISC_CONTINUOUS_RANDOM_SAMPLE_H_

#include "runtime_distribution.hpp"

namespace numa::misc {
  template<typename T>
  class continuous_random_sample {
   public:
    void observe(T observation);

    std::vector<double> histogram(...) const;

    std::vector<T> const& data() const;

    runtime_distribution<T> to_runtime_distribution() const;

   private:
    std::vector<T> data_;
  };
}

#include "continuous_random_sample.inl"

#endif
