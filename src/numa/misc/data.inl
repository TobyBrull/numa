#include "numa/cell/reform.hpp"

#include "numa/core/random.hpp"

#include <string>

namespace numa::misc {
  template<typename T>
  std::vector<T> std_iota_vector(size_t n, T init)
  {
    std::vector<T> retval(n, init);

    std::iota(retval.begin(), retval.end(), init);

    return retval;
  }

  inline std::vector<std::string> std_iota_vector_string(size_t n, int init)
  {
    std::vector<std::string> retval(n);

    for (auto& val: retval) {
      val = std::to_string(init++);
    }

    return retval;
  }

  template<typename T, typename URBG>
  std::vector<T>
  std_random_vector(size_t const n, T const upper_bound, URBG&& g)
  {
    std::vector<T> retval;
    retval.reserve(n);

    for (size_t i = 0; i < n; ++i) {
      retval.push_back(random<T>(upper_bound, g));
    }

    return retval;
  }

  template<typename T, index_t N>
  tensor<T, N> iota_tensor(index<N> size, T init)
  {
    tensor<T, N> retval;

    retval.reform()(resize<N>(size));

    for (auto& val: retval.span()) {
      val = init;
      ++init;
    }

    return retval;
  }
}
