#ifndef INCLUDE_GUARD_NUMA_MISC_TIMER_H_
#define INCLUDE_GUARD_NUMA_MISC_TIMER_H_

#include "benchmark_context.hpp"

#include "numa/cell/make_cell.hpp"
#include "numa/cell/reform.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/structure.hpp"
#include "numa/cell/tensor.hpp"

#include "numa/core/accumulators.hpp"
#include "numa/core/zip.hpp"

#include "numa/ctxt/phase.hpp"

#include "numa/boot/fwd.hpp"

#include "continuous_random_sample.hpp"

#include <ostream>

namespace numa::misc {
  template<typename F>
  continuous_random_sample<benchmark_duration>
  benchmark_quick(F&& f, index_t repetitions);

  template<typename F>
  double measure_gflops(index_t repeat, index_t flops_per_f, F&& f);

  /*
   * Real deal benchmark
   */

  DEFINE_MARK(trial);
  DEFINE_MARK(effort);
  DEFINE_MARK(mandatory);
  DEFINE_MARK(candidate);
  DEFINE_MARK(run_time);

  template<typename... Ts>
  using benchmark_setup = structure<
      label<m_trial_t, vector<std::tuple<Ts...>>>,
      field<m_effort_t, vector<double>, m_trial_t>,
      field<m_mandatory_t, vector<bool>, m_trial_t>>;

  template<typename... Ts>
  using benchmark_result = structure<
      label<m_trial_t, vector<std::tuple<Ts...>>>,
      label<m_candidate_t, vector<std::string>>,
      field<
          m_run_time_t,
          matrix<benchmark_duration>,
          m_trial_t,
          m_candidate_t>>;

  /*
   * Cartesian product construction
   */

  template<typename T>
  struct benchmark_axis : public benchmark_setup<T> {
    struct base_tag_t {
    };

    template<std::Range Keys, std::Range Effort, std::Range Mandatory>
    benchmark_axis(
        base_tag_t,
        Keys const& keys,
        Effort const& effort,
        Mandatory const& mandatory);

    benchmark_axis(
        std::initializer_list<T> keys,
        std::initializer_list<double> effort,
        std::initializer_list<bool> mandatory);

    /*
     * [mandatory_ == -1]: all [keys_] are mandatory
     * [mandatory_ ==  0]: none of the [keys_] are mandatory
     * [mandatory_ ==  3]: the first three [keys_] are mandatory
     */
    benchmark_axis(
        std::initializer_list<T> keys,
        std::initializer_list<double> effort,
        index_t mandatory = -1);

    template<std::Range Keys, typename F>
    benchmark_axis(base_tag_t, Keys const& keys, F&& f, index_t mandatory = -1);

    template<typename F>
    benchmark_axis(
        std::initializer_list<T> keys, F&& f, index_t mandatory = -1);

    benchmark_axis(std::initializer_list<T> keys);

    template<typename F>
    static benchmark_axis<T> parse(std::string_view, F&& f);
  };

  template<typename... Ts>
  benchmark_setup<Ts...>
  make_benchmark_setup(benchmark_axis<Ts> const&... axes);

  template<typename Setup, typename F, typename Comparer>
  class benchmark;

  template<typename... Ts, std::Invocable<Ts const&...> F, typename Comparer>
  class benchmark<benchmark_setup<Ts...>, F, Comparer> {
   public:
    benchmark(
        benchmark_setup<Ts...>,
        F,
        benchmark_axis<identifier> candidates,
        Comparer);

    template<typename... Candidates>
    benchmark_result<Ts...> operator()(Candidates const&...) const;

   private:
    using FTuple = std::invoke_result_t<F, Ts const&...>;

    benchmark_setup<Ts...> setup_;
    F f_;
    benchmark_axis<identifier> candidates_;
    Comparer comparer_;

    double setup_effort_ = 0.0;
  };

  template<typename... Ts, std::Invocable<Ts const&...> F, typename Comparer>
  benchmark(benchmark_setup<Ts...>, F, Comparer)
      -> benchmark<benchmark_setup<Ts...>, F, Comparer>;

  template<typename... Ts, std::Invocable<Ts const&...> F, typename Comparer>
  benchmark(benchmark_setup<Ts...>, F, benchmark_axis<identifier>, Comparer)
      -> benchmark<benchmark_setup<Ts...>, F, Comparer>;
}

#include "benchmark.inl"

#endif
