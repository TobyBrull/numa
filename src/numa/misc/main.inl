namespace numa::misc {
  namespace detail {
    void handler_terminate();
    void register_signal_handlers();
  }

  template<typename F>
  int main(int const argc, char const** argv, F&& f)
  try {
    using namespace numa;
    using namespace numa::misc;

    std::set_terminate(detail::handler_terminate);
    detail::register_signal_handlers();

    env_var_scope ev_0(env_var_map_system());
    env_var_scope ev_1(env_var_map_command_line(argc, argv));

    std::optional<disable_cursor> dc;
    std::variant<
        std::monostate,
        phase_monitor_debug,
        phase_monitor_observe,
        phase_monitor_profile>
        pm;

    auto const phase_level =
        env_var_default<phase_level_t>("phase_level", c_note);

    if (phase_level < c_off) {
      auto const phase_monitor = env_var_default("phase_monitor", "observe");

      if (phase_monitor == "debug") {
        pm.emplace<phase_monitor_debug>(std::cout, phase_level);
      }
      else if (phase_monitor == "observe") {
        dc.emplace(std::cout);
        pm.emplace<phase_monitor_observe>(std::cout, phase_level);
      }
      else if (phase_monitor == "profile") {
        pm.emplace<phase_monitor_profile>(phase_level);
      }
      else {
        THROW("numa::main: unknown phase_monitor='", phase_monitor, "'");
      }
    }

    if constexpr (c_type<std::invoke_result_t<F>> == c_type<void>) {
      f();
    }
    else {
      auto const result = f();

      auto const result_dest = env_var("result");
      if (result_dest.empty()) {
        std::cout << result << '\n';
      }
      else {
        std::ofstream ofs{std::string{result_dest}};
        ofs << result;
      }
    }

    return 0;
  }
  catch (std::exception const& e) {
    std::cerr << "Caught std::exception: " << e.what() << '\n';
    return 1;
  }
  catch (...) {
    std::cerr << "Caught unknown exception\n";
    return 1;
  }
}
