#ifndef INCLUDE_GUARD_NUMA_MISC_MAIN_H_
#define INCLUDE_GUARD_NUMA_MISC_MAIN_H_

#include "numa/core/serialize_json.hpp"

#include "numa/ctxt/env_var.hpp"
#include "numa/ctxt/phase_monitors.hpp"

#include <csignal>
#include <fstream>
#include <iostream>

namespace numa::misc {
  template<typename F>
  int main(int argc, char const** argv, F&& f);
}

#include "main.inl"

#endif
