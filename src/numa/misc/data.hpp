#ifndef INCLUDE_GUARD_NUMA_MISC_DATA_H_
#define INCLUDE_GUARD_NUMA_MISC_DATA_H_

#include "numa/cell/tensor.hpp"

#include <numeric>
#include <vector>

namespace numa::misc {
  template<typename T>
  std::vector<T> std_iota_vector(size_t n, T init);

  std::vector<std::string> std_iota_vector_string(size_t n, int init);

  template<typename T, typename URBG>
  std::vector<T> std_random_vector(size_t n, T upper_bound, URBG&& g);

  template<typename T, index_t N>
  tensor<T, N> iota_tensor(index<N> size, T init);
}

#include "data.inl"

#endif
