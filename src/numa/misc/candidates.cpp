#include "candidates.hpp"

#include "numa/core/generators.hpp"

#include "numa/ctxt/thalanx.hpp"

#include <random>

namespace numa::misc {
  void to_pretty(std::ostream& os, supported_type const st)
  {
    switch (st) {
      case supported_type::float_t:
        os << "float";
        break;
      case supported_type::double_t:
        os << "double";
        break;
    }
  }

  void from_pretty(std::istream& is, supported_type& st)
  {
    auto const id = make_from_pretty<identifier>(is);

    if (id.data() == "float") {
      st = supported_type::float_t;
    }
    else if (id.data() == "double") {
      st = supported_type::double_t;
    }
    else {
      is.setstate(std::ios::failbit);
    }
  }

  void to_json(std::ostream& os, supported_type const st)
  {
    os << '"';
    to_pretty(os, st);
    os << '"';
  }

  void from_json(std::istream& is, supported_type& st)
  {
    std::string temp;
    numa::from_json(is, temp);

    std::istringstream iss(temp);
    from_pretty(iss, st);
  }

  std::tuple<
      matrix_supported_type,
      matrix_supported_type,
      supported_type,
      index_t,
      cpu_target>
  benchmark_map_dot::operator()(
      index_t const n,
      index_t const p,
      supported_type const st,
      index_t const num_threads,
      cpu_target const ct) const
  {
    std::default_random_engine eng;

    matrix_supported_type A, B;

    switch (st) {
      case supported_type::float_t: {
        A = make_cell<matrix<float>>(n, p)(g_random_test(eng));
        B = make_cell<matrix<float>>(p, n)(g_random_test(eng));
      } break;

      case supported_type::double_t: {
        A = make_cell<matrix<double>>(n, p)(g_random_test(eng));
        B = make_cell<matrix<double>>(p, n)(g_random_test(eng));
      } break;
    }

    return std::tuple{std::move(A), std::move(B), st, num_threads, ct};
  }
}
