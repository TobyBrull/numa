#ifndef INCLUDE_GUARD_NUMA_MISC_DISCRETE_RANDOM_SAMPLE_H_
#define INCLUDE_GUARD_NUMA_MISC_DISCRETE_RANDOM_SAMPLE_H_

#include "discrete_distribution.hpp"

#include "numa/boot/fwd.hpp"

#include <unordered_map>

namespace numa::misc {
  /**
   * Keeps track of observations from a discrete random variable.
   * Several distributions can be estimated from the observations
   * that were made.
   */
  template<typename T>
  class discrete_random_sample {
   public:
    /**
     * Add the given @p observation to the sample.
     */
    void operator()(T observation);

    /**
     * Remove all observations from the sample and put this
     * object into its default constructed state.
     */
    void clear();

    /**
     * The number of distinct values that has been observed.
     */
    index_t size() const;

    /**
     * Get the actual number of observations that have been made
     * for @p value.
     */
    index_t count(T const& value) const;

    /**
     * Get the total number of observations made by this sample.
     */
    index_t total_count() const;

    /**
     * Get the underlying data.
     */
    std::unordered_map<T, index_t> const& all_counts() const;

    /**
     * Convert the random-sample to a distribution such that the
     * probability of each value is proportional to the number of
     * time that it has been observed.
     *
     * Precondition: At least one observation has been made.
     */
    discrete_distribution<T> to_distribution() const;

   private:
    std::unordered_map<T, index_t> counts_;
    index_t total_count_ = 0;
  };
}

#include "discrete_random_sample.inl"

#endif
