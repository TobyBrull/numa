namespace numa::misc {
  inline auto f_effort_supported_type()
  {
    return [](supported_type const st) {
      switch (st) {
        case supported_type::float_t:
          return 1.0;
        case supported_type::double_t:
          return 2.0;
      }

      UNREACHABLE();
    };
  }

  template<typename F>
  candidate<F>::candidate(identifier name, F&& f)
    : name_(std::move(name)), f_(std::forward<F>(f))
  {
  }

  template<typename F>
  identifier const& candidate<F>::name() const
  {
    return name_;
  }

  template<typename F>
  template<typename... Ts>
  auto candidate<F>::operator()(Ts&&... values) const
  {
    return f_(std::forward<Ts>(values)...);
  }

  /*
   * candidate_dot
   */

  template<typename F>
  candidate_dot<F>::candidate_dot(identifier name, F&& f)
    : name_(std::move(name)), f_(std::forward<F>(f))
  {
  }

  template<typename F>
  identifier const& candidate_dot<F>::name() const
  {
    return name_;
  }

  template<typename F>
  matrix_supported_type candidate_dot<F>::operator()(
      matrix_supported_type const& A_st,
      matrix_supported_type const& B_st,
      supported_type,
      index_t const num_threads,
      cpu_target const ct) const
  {
    cpu_target_scope s_(ct);
    thalanx t_(num_threads);

    return std::visit(
        [&]<typename T>(matrix<T> const& A) -> matrix_supported_type {
          matrix<T> const& B = std::get<matrix<T>>(B_st);

          auto const A_span = A.span_const();
          auto const B_span = B.span_const();

          auto retval = make_cell<matrix<T>>(A.size()[0], B.size()[1])();
          auto const retval_span = retval.span();

          {
            auto t = benchmark_context::timer();
            f_(retval_span, A_span, B_span);
          }

          return retval;
        },
        A_st);
  }

  inline auto f_is_close_supported_type(double const tol_pow)
  {
    return [tol_pow](
               matrix_supported_type const& lhs_st,
               matrix_supported_type const& rhs_st) -> bool {
      ASSERT(lhs_st.index() == rhs_st.index());

      double const tol = [&] {
        return std::visit(
            [&]<typename T>(matrix<T> const&) {
              return std::pow(std::numeric_limits<T>::epsilon(), tol_pow);
            },
            lhs_st);
      }();

      double retval = 0.0;

      std::visit(
          [&]<typename T>(T const& lhs) {
            auto const& rhs = std::get<T>(rhs_st);

            for_each(lhs, rhs)([&](double const x, double const y) {
              retval = std::max(retval, std::abs(x - y));
            });
          },
          lhs_st);

      return (retval < tol);
    };
  }
}
