#include "fibonacci.hpp"

namespace numa::misc {
  index_t fibonacci_fast(index_t const n)
  {
    // TODO
    if (n <= 1) {
      return 1;
    }
    else {
      return fibonacci_fast(n - 1) + fibonacci_fast(n - 2);
    }
  }

  index_t fibonacci(index_t const n)
  {
    if (n <= 1) {
      return 1;
    }
    else if (n <= 6) {
      return fibonacci(n - 1) + fibonacci(n - 2);
    }
    else {
      phase p{c_paragraph, "fibonacci"_s, 3, NUMA_LOCATION};

      p.start(2);
      const auto f1 = fibonacci(n - 1);

      p.start(1);
      const auto f2 = fibonacci(n - 2);

      return f1 + f2;
    }
  }
}
