#ifndef INCLUDE_GUARD_NUMA_MISC_MEMORY_H_
#define INCLUDE_GUARD_NUMA_MISC_MEMORY_H_

#include "numa/cell/t_span.hpp"

#include "numa/core/memory.hpp"

#include "numa/ctxt/context.hpp"

#include <ostream>

namespace numa::misc {
  enum class memory_operation { allocate, deallocate };

  std::ostream& operator<<(std::ostream& os, memory_operation mo);

  struct memory_log_entry {
    memory_log_entry(
        memory_operation mem_op,
        void const* ptr,
        index_t count,
        std::align_val_t alignment);

    memory_operation mem_op;
    void const* ptr;
    index_t count;
    std::align_val_t alignment;

    bool satisfies(
        memory_operation req_mem_op,
        index_t req_count,
        std::optional<std::align_val_t> alignment = {}) const;
  };

  std::ostream& operator<<(std::ostream& os, memory_log_entry const& mle);

  class allocator_tracer_monitor
    : public context_base<allocator_tracer_monitor> {
   public:
    std::vector<memory_log_entry> const& log() const;

    /**
     * Deletes the complete log. This doesn't affect keeping track
     * of the active memory chunks.
     */
    void clear_log();

    /**
     * Returns a vector of all active memory chunks, i.e., of all
     * memory chunks that were allocated but not yet deallocated.
     *
     * Precondition: reinterpret_cast<>ing these memory chunks to T
     * must not lead to undefined behaviour.
     */
    template<typename T>
    std::vector<vector_span<T const>> active_as() const;

    /**
     * The number of active memory chunks and thus the size of the
     * vector that would be returned by active_as.
     */
    index_t num_active() const;

    template<typename T>
    void dump_as(std::ostream& os) const;

   protected:
    using base = context_base<allocator_tracer_monitor>;

    allocator_tracer_monitor(
        base, thalanx_remote_t, allocator_tracer_monitor const& s);
    allocator_tracer_monitor(base, root_t);
    allocator_tracer_monitor(base);

    ~allocator_tracer_monitor();

   private:
    template<Allocator Alloc>
    friend class allocator_tracer;

    void notify(
        memory_operation mem_op, void* ptr, index_t count, std::align_val_t al);

    std::vector<memory_log_entry> log_;
    std::vector<memory_log_entry> active_;
  };

  template<Allocator Alloc>
  class allocator_tracer : public Alloc {
   public:
    using Alloc::Alloc;

    template<std::align_val_t AL>
    [[nodiscard]] void* allocate(index_t count);

    template<std::align_val_t AL>
    void deallocate(void* p, index_t count) noexcept;
  };
}

#include "memory.inl"

#endif
