#ifndef INCLUDE_GUARD_NUMA_MISC_BENCHMARK_CONTEXT_DETAIL_H_
#define INCLUDE_GUARD_NUMA_MISC_BENCHMARK_CONTEXT_DETAIL_H_

#include "benchmark_types.hpp"

#include "numa/ctxt/context.hpp"

namespace numa::misc::detail {
  class benchmark_context_core : public context_base<benchmark_context_core> {
   public:
    std::optional<benchmark_duration> duration() const;

    void notify(benchmark_duration duration);

   protected:
    using base = context_base<benchmark_context_core>;

    benchmark_context_core(
        base, thalanx_remote_t, benchmark_context_core const&);

    benchmark_context_core(base, root_t);
    benchmark_context_core(base);

   private:
    std::optional<benchmark_duration> duration_;
  };
}

#endif
