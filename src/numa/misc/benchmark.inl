namespace numa::misc {
  template<typename F>
  continuous_random_sample<benchmark_duration>
  benchmark_quick(F&& f, index_t const repetitions)
  {
    auto const reference = f();

    continuous_random_sample<benchmark_duration> retval;

    for (index_t rep = 0; rep < repetitions; ++rep) {
      auto const start_time = benchmark_clock::now();
      auto const result     = f();
      auto const end_time   = benchmark_clock::now();

      THROW_IF_NOT(
          result == reference, "benchmark: result not equal to reference");

      retval.observe(end_time - start_time);
    }

    return retval;
  }

  template<typename F>
  double measure_gflops(index_t const repeat, index_t const flops_per_f, F&& f)
  {
    f();

    auto const start_time = benchmark_clock::now();
    for (index_t r = 0; r < repeat; ++r) {
      f();
    }
    auto const end_time = benchmark_clock::now();

    double const secs_elapsed =
        std::chrono::duration_cast<std::chrono::duration<double>>(
            end_time - start_time)
            .count();

    double const flops  = (repeat * flops_per_f) / secs_elapsed;
    double const gflops = flops * 1e-9;

    return gflops;
  }

  template<typename T>
  template<std::Range Keys, std::Range Effort, std::Range Mandatory>
  benchmark_axis<T>::benchmark_axis(
      base_tag_t,
      Keys const& keys,
      Effort const& effort,
      Mandatory const& mandatory)
  {
    for (auto const& [k, e, m]: make_zip_range(keys, effort, mandatory)) {
      emplace_back(
          this->vary_axes(),
          init<m_trial_t>(k),
          init<m_effort_t>(e),
          init<m_mandatory_t>(m));
    }
  }

  template<typename T>
  benchmark_axis<T>::benchmark_axis(
      std::initializer_list<T> keys,
      std::initializer_list<double> effort,
      std::initializer_list<bool> mandatory)
    : benchmark_axis(base_tag_t{}, keys, effort, mandatory)
  {
  }

  namespace detail {
    vector<bool>
    make_mandatory_vector(index_t const mandatory, index_t const size)
    {
      auto retval = make_cell<vector<bool>>(size)(f_const_t{true});

      THROW_IF_NOT(
          mandatory <= size,
          "numa::misc::benchmark_axis: requires [mandatory < size]");

      if (mandatory >= 0) {
        auto const s = retval.span().axes().sub(as_till, mandatory, size);
        for_each(s)(f_const_t{false});
      }

      return retval;
    }
  }

  template<typename T>
  benchmark_axis<T>::benchmark_axis(
      std::initializer_list<T> keys,
      std::initializer_list<double> effort,
      index_t const mandatory)
    : benchmark_axis(
          base_tag_t{},
          keys,
          effort,
          detail::make_mandatory_vector(mandatory, keys.size()))
  {
  }

  namespace detail {
    template<typename T, std::Range Keys, typename F>
    vector<double> make_effort_vector(Keys const& keys, F&& f)
    {
      vector<double> retval;

      for (auto const& k: keys) {
        emplace_back(retval.vary_axes(), f(k));
      }

      return retval;
    }
  }

  template<typename T>
  template<std::Range Keys, typename F>
  benchmark_axis<T>::benchmark_axis(
      base_tag_t, Keys const& keys, F&& f, index_t const mandatory)
    : benchmark_axis(
          base_tag_t{},
          keys,
          detail::make_effort_vector<T>(keys, std::forward<F>(f)),
          detail::make_mandatory_vector(mandatory, keys.size()))
  {
  }

  template<typename T>
  template<typename F>
  benchmark_axis<T>::benchmark_axis(
      std::initializer_list<T> keys, F&& f, index_t const mandatory)
    : benchmark_axis(base_tag_t{}, keys, std::forward<F>(f), mandatory)
  {
  }

  template<typename T>
  benchmark_axis<T>::benchmark_axis(std::initializer_list<T> keys)
    : benchmark_axis(
          base_tag_t{},
          keys,
          make_cell<vector<double>>(static_cast<index_t>(keys.size()))(
              f_const_t{1.0}),
          detail::make_mandatory_vector(-1, keys.size()))
  {
  }

  template<typename T>
  template<typename F>
  benchmark_axis<T> benchmark_axis<T>::parse(std::string_view const sv, F&& f)
  {
    std::vector<T> retval;
    index_t mandatory = -1;

    std::istringstream iss{std::string{sv}};

    char const c = peek_till_non_whitespace(iss);

    if (c == '[') {
      retval = make_from_pretty<std::vector<T>>(iss);

      mandatory = static_cast<index_t>(retval.size());

      char const next_c = peek_till_non_whitespace(iss);
      if (next_c == '[') {
        std::vector<T> const non_mandatory =
            make_from_pretty<std::vector<T>>(iss);

        retval.insert(retval.end(), non_mandatory.begin(), non_mandatory.end());
      }
    }
    else {
      retval.emplace_back();
      from_pretty(iss, retval.back());
    }

    return benchmark_axis<T>(
        base_tag_t{}, std::move(retval), std::forward<F>(f), mandatory);
  }

  namespace detail {
    template<index_t I, typename... Ts, typename U, typename... Us>
    void make_benchmark_setup_impl(
        benchmark_setup<Ts...>& retval,
        double const effort,
        bool const mandatory,
        std::tuple<Ts...> key,
        benchmark_axis<U> const& axis,
        benchmark_axis<Us> const&... axes)
    {
      if constexpr (sizeof...(Us) > 0) {
        for (auto const& [k, e, m]: make_zip_range(
                 axis.get(m_trial),
                 axis.get(m_effort),
                 axis.get(m_mandatory))) {
          std::get<I>(key) = std::get<0>(k);

          make_benchmark_setup_impl<I + 1>(
              retval, effort * e, mandatory && m, key, axes...);
        }
      }
      else {
        for (auto const& [k, e, m]: make_zip_range(
                 axis.get(m_trial),
                 axis.get(m_effort),
                 axis.get(m_mandatory))) {
          std::get<I>(key) = std::get<0>(k);

          emplace_back(
              retval.vary_axes(),
              init<m_trial_t>(key),
              init<m_effort_t>(effort * e),
              init<m_mandatory_t>(mandatory && m));
        }
      }
    }
  }

  template<typename... Ts>
  benchmark_setup<Ts...> make_benchmark_setup(benchmark_axis<Ts> const&... axes)
  {
    index_t const N = (axes.get(m_trial).size()[0] * ...);

    benchmark_setup<Ts...> retval;

    reserve(retval.vary_axes(), N);

    detail::make_benchmark_setup_impl<0>(
        retval, 1.0, true, std::tuple<Ts...>{}, axes...);

    return retval;
  }

  template<typename... Ts, std::Invocable<Ts const&...> F, typename Comparer>
  benchmark<benchmark_setup<Ts...>, F, Comparer>::benchmark(
      benchmark_setup<Ts...> setup,
      F f,
      benchmark_axis<identifier> candidates,
      Comparer comparer)
    : setup_(std::move(setup))
    , f_(std::move(f))
    , candidates_(std::move(candidates))
    , comparer_(std::move(comparer))
  {
    /*
     * Sort [setup_] by [m_mandatory_t] and the ones that are non-mandatory by
     * effort.
     */
    zip_sort_tuple(
        [](auto const& lhs, auto const& rhs) {
          if ((std::get<2>(lhs) == true) && (std::get<2>(rhs) == false)) {
            return true;
          }
          else if ((std::get<2>(lhs) == false) && (std::get<2>(rhs) == true)) {
            return false;
          }
          else {
            return (std::get<1>(lhs) < std::get<1>(rhs));
          }
        },
        setup_.get(m_trial),
        setup_.get(m_effort),
        setup_.get(m_mandatory));

    setup_effort_ = [&] {
      auto acc = a_plus<double>();
      for_each(setup_.get(m_effort))(acc);
      return acc.get();
    }();
  }

  namespace detail {
    template<typename FTuple, typename... Candidates>
    class candidates_return_type;

    template<typename... Us, typename Candidate, typename... Candidates>
    struct candidates_return_type<std::tuple<Us...>, Candidate, Candidates...> {
      using R = std::invoke_result_t<Candidate, Us...>;

      static_assert(
          ((c_type<R> == c_type<std::invoke_result_t<Candidates, Us...>>)&&...),
          "numa::misc::benchmark: all candidates must "
          "have the same return type");
    };
  }

  template<typename... Ts, std::Invocable<Ts const&...> F, typename Comparer>
  template<typename... Candidates>
  benchmark_result<Ts...>
  benchmark<benchmark_setup<Ts...>, F, Comparer>::operator()(
      Candidates const&... candidates) const
  {
    std::tuple<Candidates const&...> candidates_tuple = {candidates...};

    using R = typename detail::candidates_return_type<FTuple, Candidates...>::R;

    index_t const n = setup_.size(m_trial);
    index_t const K = candidates_.size();

    /*
     * Vector of length equal to the candidates_ member, with each
     * corresponding entry containing the index in the Candidates...
     * variadic pack.
     */
    auto const [candidates_index, candidate_effort] = [&] {
      double effort = 0.0;

      vector<index_t> retval;
      resize(retval.vary_axes(), K);

      for (index_t k = 0; k < K; ++k) {
        auto const& c = std::get<0>(candidates_.get(m_trial)[k]);

        bool found = false;
        for_each_argument_indexed(
            [&](index_t const i, auto const& candidate) {
              if (candidate.name() == c) {
                THROW_IF(
                    found,
                    "numa::misc::benchmark: two candidates "
                    "have the same name '",
                    c,
                    "'");

                retval[k] = i;
                effort += candidates_.get(m_effort)[k];

                found = true;
              }
            },
            candidates...);

        THROW_IF_NOT(
            found,
            "numa::misc::benchmark: did not find candidate "
            "with name '",
            c,
            "'");
      }

      return std::tuple{std::move(retval), effort};
    }();

    phase ph{c_chapter, "benchmark"_s, setup_effort_, NUMA_LOCATION};

    benchmark_result<Ts...> retval;
    reserve(retval.vary_axes(), index(n, K));
    resize(retval.vary_axes(m_candidate), index(K));

    for_each(retval.get(m_candidate), candidates_.get(m_trial))(
        f_assign_t{f_get<0>});

    for (index_t i = 0; i < n; ++i) {
      auto const& tt      = setup_.get(m_trial)[i];
      double const effort = setup_.get(m_effort)[i];

      auto const& mandatory = setup_.get(m_mandatory)[i];
      (void)mandatory;

      ph.start(effort);

      emplace_back(
          retval.vary_axes(m_trial),
          init<m_trial_t>(tt),
          init<m_run_time_t>(
              benchmark_duration(std::numeric_limits<double>::quiet_NaN())));

      auto const mapped_tt = std::apply(f_, tt);

      index_t const repeat = 1;

      {
        phase ph_trial{c_section, "trial"_s, candidate_effort, NUMA_LOCATION};

        std::optional<R> reference;

        for (index_t k = 0; k < K; ++k) {
          ph_trial.start(candidates_.get(m_effort)[k]);

          index_t const j = candidates_index[k];

          auto const candidate = [&](auto&&... args) {
            auto result = dynamicizer<sizeof...(Candidates)>(
                [&]<index_t J>(auto&&...) {
                  return std::get<J>(candidates_tuple)(
                      std::forward<decltype(args)>(args)...);
                },
                j);

            if (!reference) {
              reference = std::move(result);
            }
            else {
              bool const good = comparer_(reference.value(), result);

              THROW_IF_NOT(good, "numa::misc::benchmark: results differ");
            }
          };

          auto const dur = std::apply(
              [&](auto&... xs) {
                phase ph_run{c_section, "repeat"_s, 1 + repeat, NUMA_LOCATION};

                /*
                 * warm-up
                 */
                ph_run.start(1);
                {
                  candidate(xs...);
                }

                ASSERT(repeat == 1);
                benchmark_context bc;

                for (index_t r = 0; r < repeat; ++r) {
                  ph_run.start(1);
                  {
                    auto t = bc.timer();
                    candidate(xs...);
                  }
                }

                return bc.duration();
              },
              mapped_tt);

          retval.get(m_run_time)(i, k) = dur;
        }
      }
    }

    return retval;
  }
}
