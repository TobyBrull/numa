#include <algorithm>

namespace numa::misc {
  template<typename T>
  runtime_distribution<T>::runtime_distribution(std::vector<T> vec)
  {
    mode_ = [&vec] {
      ASSERT(vec.size() >= 10);

      auto it        = vec.begin();
      auto const nth = it + static_cast<int>(vec.size() * 0.7);

      std::nth_element(it, nth, vec.end());

      T mean    = T(0.0);
      int count = 0;

      for (; it != nth; ++it) {
        mean += (*it);
        ++count;
      }

      return mean / static_cast<double>(count);
    }();
  }

  template<typename T>
  T runtime_distribution<T>::mode() const
  {
    return mode_;
  }
}
