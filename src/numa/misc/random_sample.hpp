#ifndef INCLUDE_GUARD_NUMA_MISC_RANDOM_SAMPLE_H_
#define INCLUDE_GUARD_NUMA_MISC_RANDOM_SAMPLE_H_

#include "continuous_random_sample.hpp"
#include "discrete_random_sample.hpp"

namespace numa::misc {
  template<typename T>
  struct is_discrete
    : public std::disjunction<std::is_integral<T>, std::is_enum<T>> {
  };

  template<typename T>
  inline constexpr bool is_discrete_v = is_discrete<T>::value;

  template<typename T>
  struct is_continuous : public std::is_floating_point<T> {
  };

  template<typename T>
  inline constexpr bool is_continuous_v = is_continuous<T>::value;

  template<typename T>
  struct invalid_random_sample;

  /**
   * Convenience overloads.
   */
  template<typename T>
  using random_sample = std::conditional_t<
      is_continuous_v<T>,
      continuous_random_sample<T>,
      std::conditional_t<
          is_discrete_v<T>,
          discrete_random_sample<T>,
          invalid_random_sample<T>>>;
}

#endif
