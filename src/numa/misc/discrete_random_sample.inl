namespace numa::misc {
  template<typename T>
  void discrete_random_sample<T>::operator()(T observation)
  {
    auto const [find_it, inserted] = counts_.emplace(std::move(observation), 0);

    (void)inserted;

    ++(find_it->second);

    ++total_count_;
  }

  template<typename T>
  void discrete_random_sample<T>::clear()
  {
    counts_.clear();

    total_count_ = 0;
  }

  template<typename T>
  index_t discrete_random_sample<T>::size() const
  {
    return static_cast<index_t>(counts_.size());
  }

  template<typename T>
  index_t discrete_random_sample<T>::count(T const& value) const
  {
    auto const find_it = counts_.find(value);

    if (find_it == counts_.end()) {
      return 0;
    }
    else {
      return find_it->second;
    }
  }

  template<typename T>
  index_t discrete_random_sample<T>::total_count() const
  {
    return total_count_;
  }

  template<typename T>
  std::unordered_map<T, index_t> const&
  discrete_random_sample<T>::all_counts() const
  {
    return counts_;
  }

  template<typename T>
  discrete_distribution<T> discrete_random_sample<T>::to_distribution() const
  {
    discrete_distribution<T> retval;

    for (auto const& [value, count]: counts_) {
      retval.add(value, count);
    }

    return retval;
  }
}
