#include "main.hpp"

namespace numa::misc::detail {
  void handler_terminate()
  {
    std::cout << terminal_show_cursor << "Terminate called!\n";

    std::abort();
  }

  extern "C" void handler_signal(int const sig)
  {
    /*
     * Undefined behaviour.
     */

    std::cout << terminal_show_cursor << "Received signal: ";

    switch (sig) {
      /*
       * Guaranteed by C++
       */
      case SIGABRT: {
        std::cout << "SIGABRT";
      } break;
      case SIGFPE: {
        std::cout << "SIGFPE";
      } break;
      case SIGILL: {
        std::cout << "SIGILL";
      } break;
      case SIGINT: {
        std::cout << "SIGINT";
      } break;
      case SIGSEGV: {
        std::cout << "SIGSEGV";
      } break;
      case SIGTERM: {
        std::cout << "SIGTERM";
      } break;

      /*
       * Linux specific
       */
      case SIGPIPE: {
        std::cout << "SIGPIPE";
      } break;

      default: {
        std::cout << sig;
      } break;
    }

    std::cout << '\n';

    std::exit(100 + sig);
  }

  void register_signal_handlers()
  {
    std::signal(SIGABRT, detail::handler_signal);
    std::signal(SIGFPE, detail::handler_signal);
    std::signal(SIGILL, detail::handler_signal);
    std::signal(SIGINT, detail::handler_signal);
    std::signal(SIGSEGV, detail::handler_signal);
    std::signal(SIGTERM, detail::handler_signal);

    std::signal(SIGPIPE, detail::handler_signal);
  }
}
