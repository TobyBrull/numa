#ifndef INCLUDE_GUARD_NUMA_MISC_BENCHMARK_TYPES_H_
#define INCLUDE_GUARD_NUMA_MISC_BENCHMARK_TYPES_H_

#include <chrono>

namespace numa::misc {
  using benchmark_clock = std::chrono::high_resolution_clock;

  using benchmark_duration = std::chrono::duration<double>;
}

#endif
