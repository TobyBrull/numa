#ifndef INCLUDE_GUARD_NUMA_MISC_FUZZ_CELL_H_
#define INCLUDE_GUARD_NUMA_MISC_FUZZ_CELL_H_

#include "discrete_distribution.hpp"
#include "memory.hpp"
#include "type.hpp"

#include "numa/cell/make_cell.hpp"
#include "numa/cell/reform.hpp"

#include "numa/core/random.hpp"
#include "numa/core/serialize_json.hpp"

#include "numa/ctxt/phase.hpp"

#include "numa/tmpl/dynamicizer.hpp"

#include "numa/boot/fwd.hpp"

#include <string_view>

namespace numa::misc {
  template<template<typename> class CellTemplate>
  void fuzz_cell(
      index_t repetitions, double throw_probability, std::string_view name);
}

#include "fuzz_cell.inl"

#endif
