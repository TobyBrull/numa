#include "numa/core/algorithms.hpp"

#include "numa/boot/error.hpp"

#include <algorithm>

namespace numa::misc {
  inline bool type_log_entry::satisfies(
      type_operation const req_type_op, std::size_t const req_hash_value) const
  {
    return ((req_type_op == type_op) && (req_hash_value == hash_value));
  }

  inline type_throw_config uniform_type_throw_config(double const probability)
  {
    type_throw_config retval;

    retval.emplace(type_operation::default_construct, probability);
    retval.emplace(type_operation::construct, probability);
    retval.emplace(type_operation::copy_construct, probability);
    retval.emplace(type_operation::move_construct, probability);
    retval.emplace(type_operation::copy_assign, probability);
    retval.emplace(type_operation::move_assign, probability);

    return retval;
  }

  /*
   * Implementation of type_monitor.
   */
  inline type_monitor::type_monitor(
      base b, thalanx_remote_t, type_monitor const&)
    : type_monitor(std::move(b))
  {
  }

  inline type_monitor::type_monitor(base b, root_t) : type_monitor(std::move(b))
  {
  }

  inline type_monitor::type_monitor(base b, type_throw_config config)
    : base(std::move(b)), eng_(0)
  {
    THROW_IF_NOT(
        config.find(type_operation::destruct) == config.end(),
        "type_monitor: may never throw in destructors");

    for (auto const [type_op, prob]: config) {
      (void)type_op;
      (void)prob;

      THROW_IF_NOT(
          (0.0 <= prob) && (prob <= 1.0),
          "type_monitor: probability outside of [0, 1]");
    }

    config_ = std::move(config);
  }

  inline type_monitor::~type_monitor()
  {
    /*
     * Maybe the constructor shouldn't throw?
     */
    THROW_IF_NOT(active_.empty(), "type_monitor: active objects in destructor");
  }

  inline std::vector<type_log_entry> const& type_monitor::log() const
  {
    return log_;
  }

  inline std::unordered_map<void const*, type_log_entry> const&
  type_monitor::active() const
  {
    return active_;
  }

  inline std::vector<std::size_t> type_monitor::sorted_hash_values() const
  {
    std::vector<std::size_t> retval;
    retval.reserve(active_.size());

    for (auto const& [ptr, tle]: active_) {
      (void)ptr;
      retval.emplace_back(tle.hash_value);
    }

    sort(retval);

    return retval;
  }

  inline void type_monitor::dump(std::ostream& os) const
  {
    to_json(os, "/=========== type_monitor ===========\n");

    for (auto const& tle: log()) {
      to_json(os, tle);
      to_json(os, '\n');
    }

    to_json(os, "----------- active ------------\n");

    for (auto const& [address, tle]: active()) {
      (void)address;

      to_json(os, tle);
      to_json(os, '\n');
    }

    to_json(os, "\\===============================================\n");
  }

  inline void type_monitor::clear_log()
  {
    log_.clear();
  }

  inline void type_monitor::notify(
      type_operation type_op,
      void const* address,
      void const* other,
      std::size_t const hash_value)
  {
    base::get().notify_impl(type_op, address, other, hash_value);
  }

  inline void type_monitor::notify_impl(
      type_operation const type_op,
      void const* const address,
      void const* const other,
      std::size_t const hash_value)
  {
    /*
     * For construct type_operations check that they are not on an address
     * that already contains an active object. For the other type_operations,
     * checked the opposite.
     */
    auto const find_it = active_.find(address);

    switch (type_op) {
      case type_operation::default_construct:
      case type_operation::construct:
      case type_operation::copy_construct:
      case type_operation::move_construct: {
        THROW_IF_NOT(
            find_it == active_.end(),
            "type_monitor: trying to construct on already used memory");
      } break;

      case type_operation::copy_assign:
      case type_operation::move_assign: {
        THROW_IF_NOT(
            find_it != active_.end(),
            "type_monitor: trying to assign to invalid memory");
      } break;

      case type_operation::destruct: {
        THROW_IF_NOT(
            find_it != active_.end(),
            "type_monitor: trying to destruct invalid memory");
      } break;
    }

    /*
     * Maybe throw an exception, depending on pseudo-random numbers.
     */
    if (auto const find_it = config_.find(type_op); find_it != config_.end()) {
      double const prob = (find_it->second);

      if (prob == 0.0) {
        ;
      }
      else if (prob == 1.0) {
        throw type_exception();
      }
      else {
        double const rr = std::uniform_real_distribution<>(0.0, 1.0)(eng_);

        if (rr <= prob) {
          throw type_exception();
        }
      }
    }

    /*
     * Process the notification and update log and active-state.
     */
    type_log_entry const new_tle{type_op, address, other, hash_value};

    log_.push_back(new_tle);

    switch (type_op) {
      case type_operation::default_construct:
      case type_operation::construct:
      case type_operation::copy_construct:
      case type_operation::move_construct: {
        auto const [it, done] = active_.emplace(address, new_tle);

        (void)it;
        (void)done;
        ASSERT(done);
      } break;

      case type_operation::copy_assign:
      case type_operation::move_assign:
      case type_operation::destruct: {
        ASSERT(find_it != active_.end());

        if (type_op != type_operation::destruct) {
          find_it->second = new_tle;
        }
        else {
          active_.erase(find_it);
        }
      } break;
    }
  }

  /*
   * Implementation of tracer
   */

  template<typename T>
  tracer<T>::tracer() requires std::is_default_constructible_v<class_wrapper<T>>
    : base{}
  {
    type_monitor::notify(
        type_operation::default_construct,
        this,
        nullptr,
        hash(static_cast<T const&>(*this)));
  }

  template<typename T>
  template<typename U, typename... Us>
  tracer<T>::tracer(U&& value, Us&&... values) requires(
      !std::is_same_v<remove_const_ref_t<U>, tracer>)
    : base{std::forward<U>(value), std::forward<Us>(values)...}
  {
    type_monitor::notify(
        type_operation::construct,
        this,
        nullptr,
        hash(static_cast<T const&>(*this)));
  }

  template<typename T>
  tracer<T>::tracer(tracer const& other) requires std::is_copy_constructible_v<
      class_wrapper<T>> : base(other)
  {
    type_monitor::notify(
        type_operation::copy_construct,
        this,
        &other,
        hash(static_cast<T const&>(*this)));
  }

  template<typename T>
  tracer<T>& tracer<T>::operator=(
      tracer const& other) requires std::is_copy_assignable_v<class_wrapper<T>>
  {
    static_cast<base&>(*this) = other;

    type_monitor::notify(
        type_operation::copy_assign,
        this,
        &other,
        hash(static_cast<T const&>(*this)));

    return (*this);
  }

  template<typename T>
  tracer<T>::tracer(
      tracer&& other) requires std::is_move_constructible_v<class_wrapper<T>>
    : base(std::move(other))
  {
    type_monitor::notify(
        type_operation::move_construct,
        this,
        &other,
        hash(static_cast<T const&>(*this)));
  }

  template<typename T>
  tracer<T>& tracer<T>::operator=(
      tracer&& other) requires std::is_move_assignable_v<class_wrapper<T>>
  {
    static_cast<base&>(*this) = std::move(other);

    type_monitor::notify(
        type_operation::move_assign,
        this,
        &other,
        hash(static_cast<T const&>(*this)));

    return (*this);
  }

  template<typename T>
  tracer<T>::~tracer()
  {
    type_monitor::notify(
        type_operation::destruct,
        this,
        nullptr,
        hash(static_cast<T const&>(*this)));
  }

  /*
   * Class types
   */

  inline value_int::value_int() noexcept : value_(default_value) {}

  inline value_int::value_int(int const value) : value_(value)
  {
    THROW_IF_NOT(value >= 0, "value_int: values must not be negative");
  }

  inline value_int::value_int(value_int const& value) noexcept
    : value_(value.value_)
  {
  }

  inline value_int& value_int::operator=(value_int const& value) noexcept
  {
    value_ = value.value_;

    return (*this);
  }

  inline value_int::value_int(value_int&& value) noexcept : value_(value.value_)
  {
    value.value_ = moved_from_value;
  }

  inline value_int& value_int::operator=(value_int&& value) noexcept
  {
    value_ = value.value_;

    value.value_ = moved_from_value;

    return (*this);
  }

  inline value_int::~value_int() noexcept {}

  inline int value_int::value() const
  {
    return value_;
  }

  inline non_copy_int::non_copy_int() noexcept : value_(default_value) {}

  inline non_copy_int::non_copy_int(int const value) : value_(value)
  {
    THROW_IF_NOT(value >= 0, "non_copy_int: values must not be negative");
  }

  inline non_copy_int::non_copy_int(non_copy_int&& value) noexcept
    : value_(value.value_)
  {
    value.value_ = moved_from_value;
  }

  inline non_copy_int& non_copy_int::operator=(non_copy_int&& value) noexcept
  {
    value_ = value.value_;

    value.value_ = moved_from_value;

    return (*this);
  }

  inline non_copy_int::~non_copy_int() {}

  inline int non_copy_int::value() const
  {
    return value_;
  }

  namespace literals {
    inline traced_int operator"" _ti(unsigned long long int i)
    {
      return traced_int(static_cast<int>(i));
    }

    inline traced_non_copy operator"" _tnc(unsigned long long int i)
    {
      return traced_non_copy(static_cast<int>(i));
    }
  }
}

namespace numa {
  inline void
  to_json(std::ostream& os, misc::type_operation const to, int const)
  {
    switch (to) {
      case misc::type_operation::default_construct:
        os << "default_construct";
        break;
      case misc::type_operation::construct:
        os << "        construct";
        break;
      case misc::type_operation::copy_construct:
        os << "   copy_construct";
        break;
      case misc::type_operation::move_construct:
        os << "   move_construct";
        break;
      case misc::type_operation::copy_assign:
        os << "      copy_assign";
        break;
      case misc::type_operation::move_assign:
        os << "      move_assign";
        break;
      case misc::type_operation::destruct:
        os << "         destruct";
        break;
    }
  }

  inline void
  to_json(std::ostream& os, misc::type_log_entry const& tle, int const)
  {
    to_json(os, '[');
    to_json(os, tle.type_op);
    to_json(os, ", ");
    to_json(os, tle.address);
    to_json(os, ", ");
    to_json(os, tle.other);
    to_json(os, ", ");
    to_json(os, tle.hash_value);
    to_json(os, ']');
  }

  template<typename T>
  void to_json(std::ostream& os, misc::tracer<T> const& t, int const)
  {
    os << static_cast<T const&>(t);
  }

  inline void to_json(std::ostream& os, misc::non_copy_int const& vi, int const)
  {
    os << vi.value();
  }

  inline void to_json(std::ostream& os, misc::value_int const& vi, int const)
  {
    os << vi.value();
  }
}
