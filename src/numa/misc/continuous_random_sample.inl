namespace numa::misc {
  template<typename T>
  void continuous_random_sample<T>::observe(T observation)
  {
    data_.push_back(std::move(observation));
  }

  template<typename T>
  std::vector<T> const& continuous_random_sample<T>::data() const
  {
    return data_;
  }

  template<typename T>
  runtime_distribution<T>
  continuous_random_sample<T>::to_runtime_distribution() const
  {
    return runtime_distribution<T>(data_);
  }
}
