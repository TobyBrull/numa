#include <algorithm>
#include <exception>
#include <iostream>

namespace numa::misc {
  inline std::ostream& operator<<(std::ostream& os, memory_operation const mo)
  {
    switch (mo) {
      case memory_operation::allocate:
        os << " allocate ";
        break;
      case memory_operation::deallocate:
        os << "deallocate";
        break;
    }

    return os;
  }

  inline memory_log_entry::memory_log_entry(
      memory_operation const mem_op,
      void const* const ptr,
      index_t const count,
      std::align_val_t const alignment)
    : mem_op(mem_op), ptr(ptr), count(count), alignment(alignment)
  {
  }

  inline bool memory_log_entry::satisfies(
      memory_operation const req_mem_op,
      index_t const req_count,
      std::optional<std::align_val_t> req_alignment) const
  {
    if (req_mem_op != mem_op) {
      return false;
    }

    if (req_count != count) {
      return false;
    }

    if (req_alignment.has_value()) {
      if (req_alignment.value() != alignment) {
        return false;
      }
    }

    return true;
  }

  inline std::ostream& operator<<(std::ostream& os, memory_log_entry const& mle)
  {
    os << '[' << mle.mem_op << ", " << mle.ptr << ", " << mle.count << ", "
       << static_cast<int>(mle.alignment) << ']';

    return os;
  }

  inline allocator_tracer_monitor::allocator_tracer_monitor(
      base b, thalanx_remote_t, allocator_tracer_monitor const&)
    : allocator_tracer_monitor(std::move(b))
  {
  }

  inline allocator_tracer_monitor::allocator_tracer_monitor(base b, root_t)
    : allocator_tracer_monitor(std::move(b))
  {
  }

  inline allocator_tracer_monitor::allocator_tracer_monitor(base b)
    : base(std::move(b))
  {
  }

  inline allocator_tracer_monitor::~allocator_tracer_monitor()
  {
    /*
     * Don't throw in destructor.
     */
    THROW_IF_NOT(
        num_active() == 0,
        "allocator_tracer_monitor: active memory in destructor");
  }

  inline std::vector<memory_log_entry> const&
  allocator_tracer_monitor::log() const
  {
    return log_;
  }

  inline void allocator_tracer_monitor::clear_log()
  {
    log_.clear();
  }

  template<typename T>
  std::vector<vector_span<T const>> allocator_tracer_monitor::active_as() const
  {
    std::vector<vector_span<T const>> retval;
    retval.reserve(active_.size());

    for (auto const& mle: active_) {
      ASSERT((mle.count % sizeof(T)) == 0);

      retval.emplace_back(
          reinterpret_cast<T const*>(mle.ptr),
          static_cast<index_t>(mle.count / sizeof(T)),
          1);
    }

    return retval;
  }

  inline index_t allocator_tracer_monitor::num_active() const
  {
    return active_.size();
  }

  template<typename T>
  void allocator_tracer_monitor::dump_as(std::ostream& os) const
  {
    os << "/=========== allocator-tracer-scope ===========\n";

    for (auto const& mle: log()) {
      os << mle << '\n';
    }

    os << "----------- active ------------\n";

    for (auto const& vs: active_as<T>()) {
      os << static_cast<void const*>(vs.origin) << '\n';
      os << vs << '\n';
    }

    os << "\\===============================================\n";
  }

  inline void allocator_tracer_monitor::notify(
      memory_operation const mem_op,
      void* const ptr,
      index_t const count,
      std::align_val_t const al)
  {
    if (ptr == nullptr) {
      THROW_IF(
          count != 0,
          "allocator_tracer_monitor: expected count of zero "
          "when deallocating nullptr");

      return;
    }

    switch (mem_op) {
      case memory_operation::allocate: {
        log_.emplace_back(mem_op, ptr, count, al);
        active_.emplace_back(mem_op, ptr, count, al);
      } break;

      case memory_operation::deallocate: {
        log_.emplace_back(mem_op, ptr, count, al);

        auto const find_it = std::find_if(
            active_.begin(), active_.end(), [ptr](auto const& mle) {
              return (mle.ptr == ptr);
            });

        THROW_IF_NOT(
            find_it != active_.end(),
            "allocator_tracer_monitor: trying to deallocate unknown memory");

        ASSERT(find_it->mem_op == memory_operation::allocate);

        THROW_IF_NOT(
            find_it->count == count,
            "allocator_tracer_monitor: wrong size given to deallocate");

        THROW_IF_NOT(
            find_it->alignment == al,
            "allocator_tracer_monitor: wrong alignment given to deallocate");

        active_.erase(find_it);
      } break;
    }
  }

  template<Allocator Alloc>
  template<std::align_val_t AL>
  [[nodiscard]] void* allocator_tracer<Alloc>::allocate(index_t const count)
  {
    void* retval = static_cast<Alloc*>(this)->template allocate<AL>(count);

    try {
      context<allocator_tracer_monitor>::get().notify(
          memory_operation::allocate, retval, count, AL);
    }
    catch (...) {
      static_cast<Alloc*>(this)->template deallocate<AL>(retval, count);

      throw;
    }

    return retval;
  }

  template<Allocator Alloc>
  template<std::align_val_t AL>
  void allocator_tracer<Alloc>::deallocate(
      void* const p, index_t const count) noexcept
  {
    try {
      context<allocator_tracer_monitor>::get().notify(
          memory_operation::deallocate, p, count, AL);
    }
    catch (...) {
      std::cout << "allocator_tracer::deallocate caught exception; "
                   "terminate called\n";
      std::terminate();
    }

    auto* const base = static_cast<Alloc*>(this);

    static_assert(noexcept(base->template deallocate<AL>(p, count)));

    base->template deallocate<AL>(p, count);
  }
}
