#ifndef INCLUDE_GUARD_NUMA_MISC_FIBONACCI_H_
#define INCLUDE_GUARD_NUMA_MISC_FIBONACCI_H_

#include "numa/ctxt/phase.hpp"
#include "numa/ctxt/thalanx_fork.hpp"

namespace numa::misc {
  index_t fibonacci_fast(index_t const n);

  index_t fibonacci(index_t const n);

  template<typename C>
  requires ChunkingFunctor<C> index_t fibonacci_th(index_t const n, C const& c);

  template<typename C>
  requires ChunkingFunctor<C>
      index_t fibonacci_th_dynamic(index_t const n, C const& c);
}

#include "fibonacci.inl"

#endif
