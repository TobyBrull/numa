namespace numa::misc {
  template<typename C>
  requires ChunkingFunctor<C> index_t fibonacci_th(index_t const n, C const& c)
  {
    phase p{c_section, "fibonacci_th"_s, NUMA_LOCATION};

    if ((n <= 5) || thalanx::size() == 1) {
      return fibonacci(n);
    }
    else {
      auto const [f_1, f_2] = thalanx_fork(
          c,
          [&c](index_t const m) { return fibonacci_th(m, c); },
          n - 2,
          n - 1);

      return (f_1 + f_2);
    }
  }

  template<typename C>
  requires ChunkingFunctor<C>
      index_t fibonacci_th_dynamic(index_t const n, C const& c)
  {
    phase p{c_section, "fibonacci_th_dynamic"_s, NUMA_LOCATION};

    if ((n <= 5) || thalanx::size() == 1) {
      return fibonacci(n);
    }
    else {
      std::array<index_t, 2> temp = {n - 2, n - 1};

      auto const result_vec = thalanx_fork(
          c,
          [&c](index_t const m) { return fibonacci_th_dynamic(m, c); },
          std::span<index_t>{temp});

      ASSERT(result_vec.size() == 2);

      return result_vec.front() + result_vec.back();
    }
  }
}
