#include "benchmark_context.hpp"

namespace numa::misc {
  benchmark_duration benchmark_context::duration() const
  {
    auto const opt_dur = scope_.duration();

    if (opt_dur.has_value()) {
      return opt_dur.value();
    }
    else {
      return benchmark_duration(std::numeric_limits<double>::quiet_NaN());
    }
  }
}
