namespace numa::misc::detail {
  enum class cell_fuzz_step {
    shrink_to_fit,
    clear,
    clear_hard,
    resize,
    resize_hard,
    reserve,
    reserve_hard,
    insert_size,
    erase_size,
    move
  };

  inline std::ostream& operator<<(std::ostream& os, cell_fuzz_step const fs)
  {
    switch (fs) {
      case cell_fuzz_step::shrink_to_fit:
        os << "shrink_to_fit";
        break;
      case cell_fuzz_step::clear:
        os << "        clear";
        break;
      case cell_fuzz_step::clear_hard:
        os << "   clear_hard";
        break;
      case cell_fuzz_step::resize:
        os << "       resize";
        break;
      case cell_fuzz_step::resize_hard:
        os << "  resize_hard";
        break;
      case cell_fuzz_step::reserve:
        os << "      reserve";
        break;
      case cell_fuzz_step::reserve_hard:
        os << " reserve_hard";
        break;
      case cell_fuzz_step::insert_size:
        os << "  insert_size";
        break;
      case cell_fuzz_step::erase_size:
        os << "   erase_size";
        break;
      case cell_fuzz_step::move:
        os << "         move";
        break;
    }

    return os;
  }

  template<Cell C>
  class cell_fuzzer {
   public:
    cell_fuzzer(C& cell) : cell_(cell)
    {
      dd_.add(cell_fuzz_step::shrink_to_fit, 0.1);
      dd_.add(cell_fuzz_step::clear, 0.01);
      dd_.add(cell_fuzz_step::clear_hard, 0.01);
      dd_.add(cell_fuzz_step::resize, 10.0);
      dd_.add(cell_fuzz_step::resize_hard, 10.0);
      dd_.add(cell_fuzz_step::reserve, 2.0);
      dd_.add(cell_fuzz_step::reserve_hard, 2.0);
      dd_.add(cell_fuzz_step::insert_size, 10.0);
      dd_.add(cell_fuzz_step::erase_size, 10.0);
      dd_.add(cell_fuzz_step::move, 0.001);
    }

    constexpr static index_t Na = C::axes_info.num_axes;

    template<typename URBG>
    void operator()(URBG&& g) const
    {
      index_t const K_minus_1 = random<index_t>(Na, g);

      dynamicizer<Na>(*this, K_minus_1, std::forward<URBG>(g));
    }

    template<index_t K_minus_1, typename URBG>
    void operator()(URBG&& g) const
    {
      constexpr auto K = K_minus_1 + 1;

      auto const ax = random_variation<K, Na>(g);

      dynamicizer_list<Na>(*this, ax, std::forward<URBG>(g));
    }

    template<typename IndexList, typename URBG>
    void operator()(URBG&& g) const
    {
      apply_impl(IndexList{}, std::forward<URBG>(g));
    }

    template<index_t... Axes, typename URBG>
    void apply_impl(index_list_t<Axes...>, URBG&& g) const
    {
      if constexpr (is_variation<Na>(index_list<Axes...>)) {
        phase_status(c_trace, "axes = ", index(Axes...));
        constexpr index_list_t<Axes...> ax;
        constexpr index_t K = size(ax);

        auto const max_ind  = 20 * index<K>(one);
        int const max_value = 100;

        auto const fuzz_step = dd_(g);

        phase_status(c_trace, "fuzz-step = ", fuzz_step);

        switch (fuzz_step) {
          case cell_fuzz_step::shrink_to_fit: {
            numa::shrink_to_fit(cell_.vary_axes(ax));
          } break;

          case cell_fuzz_step::clear: {
            numa::clear(cell_.vary_axes(ax));
          } break;

          case cell_fuzz_step::clear_hard: {
            numa::clear_hard(cell_.vary_axes(ax));
          } break;

          case cell_fuzz_step::resize: {
            auto const size  = random(max_ind, g);
            auto const value = random<int>(max_value, g);

            phase_status(c_trace, "size = ", size);

            numa::resize(cell_.vary_axes(ax), size, value);
          } break;

          case cell_fuzz_step::resize_hard: {
            auto const size  = random(max_ind, g);
            auto const value = random<int>(max_value, g);

            phase_status(c_trace, "size = ", size);

            numa::resize_hard(cell_.vary_axes(ax), size, value);
          } break;

          case cell_fuzz_step::reserve: {
            auto const min_capa = random(max_ind, g);

            phase_status(c_trace, "min-capa = ", min_capa);

            numa::reserve(cell_.vary_axes(ax), min_capa);
          } break;

          case cell_fuzz_step::reserve_hard: {
            auto const new_capa = random(max_ind, g);

            phase_status(c_trace, "new-capa = ", new_capa);

            numa::reserve_hard(cell_.vary_axes(ax), new_capa);
          } break;

          case cell_fuzz_step::insert_size: {
            auto const ax_size = select_at(cell_.size(), ax);

            auto const base = random(ax_size + index<K>(one), g);
            auto const size = random(max_ind, g);

            phase_status(c_trace, "base = ", base);
            phase_status(c_trace, "size = ", size);

            auto const value = random<int>(max_value, g);

            numa::insert_size(cell_.vary_axes(ax), base, size, value);
          } break;

          case cell_fuzz_step::erase_size: {
            auto const ax_size = select_at(cell_.size(), ax);

            auto const base = random(ax_size + index<K>(one), g);
            auto const size = random(ax_size - base + index<K>(one), g);

            phase_status(c_trace, "base = ", base);
            phase_status(c_trace, "size = ", size);

            numa::erase_size(cell_.vary_axes(ax), base, size);
          } break;

          case cell_fuzz_step::move: {
            C temp;
            numa::resize(temp.vary_axes(), cell_.size());

            copy_from(cell_, temp);

            cell_ = std::move(temp);
          } break;
        }
      }
    }

   private:
    C& cell_;
    discrete_distribution<cell_fuzz_step> dd_;
  };

  template<Cell C>
  void check_properly_constructed(
      C const& cell,
      std::unordered_map<void const*, type_log_entry> const& active)
  {
    auto const exp_size = C::required_elements(cell.size());

    /*
     * The following checks whether the active objects are exactly
     * in the places that are expected.
     */
    auto const active_pointers = [&] {
      std::vector<const void*> retval;
      retval.reserve(exp_size);

      for (const auto [ptr, entry]: active) {
        (void)entry;
        retval.push_back(ptr);
      }

      sort(retval);

      return retval;
    }();

    const auto span_pointers = [&] {
      std::vector<void const*> retval;
      retval.reserve(exp_size);

      for (auto const& val: cell.span()) {
        retval.push_back(&val);
      }

      return retval;
    }();

    THROW_IF_NOT(
        active_pointers == span_pointers,
        "fuzz_cell: not all constructed objects in the expected places");
  }
}

namespace numa::misc {
  template<template<typename, Allocator> class CellTemplate>
  void fuzz_cell(
      index_t const repetitions,
      double const throw_probability,
      std::string_view const name)
  {
    using C = CellTemplate<traced_int, allocator_tracer<default_allocator>>;

    context<allocator_tracer_monitor> ats;

    context<type_monitor> ctx(uniform_type_throw_config(throw_probability));

    C cell;

    phase_status(c_note, name, ": doing ", repetitions, " repetitions.");

    detail::cell_fuzzer fuzzer(cell);

    std::default_random_engine eng(0);

    int num_exceptions = 0;

    phase p{c_chapter, "fuzz_cell"_s, repetitions, NUMA_LOCATION};

    for (index_t r = 0; r < repetitions; ++r) {
      p.start(1);
      p.status("@ r = ", r, "; exceptions = ", num_exceptions);
      phase_status(c_trace, "@ size = ", cell.size());
      phase_status(c_trace, "@ capa = ", cell.capacity());
      phase_status(c_trace, "@ eng = ", eng);

      try {
        fuzzer(eng);
      }
      catch (type_exception const&) {
        phase_status(c_trace, "thrown");

        ++num_exceptions;
      }

      detail::check_properly_constructed(cell, ctx.active());

      auto const active = ats.active_as<traced_int>();

      THROW_IF_NOT(
          active.size() <= 1,
          "fuzz_cell: more than one active memory allocation");

      if (active.size() == 1) {
        THROW_IF_NOT(
            active.front().size()[0] >= C::required_elements(cell.size()),
            "fuzz_cell: less memory allocated than should be necessary");
      }

      ctx.clear_log();
      ats.clear_log();
    }
  }
}
