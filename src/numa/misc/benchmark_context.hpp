#ifndef INCLUDE_GUARD_NUMA_MISC_BENCHMARK_CONTEXT_H_
#define INCLUDE_GUARD_NUMA_MISC_BENCHMARK_CONTEXT_H_

#include "benchmark_context_detail.hpp"

namespace numa::misc {
  class benchmark_context {
   public:
    benchmark_duration duration() const;

    static auto timer();

    static void notify(benchmark_duration);

   private:
    context<detail::benchmark_context_core> scope_;
  };
}

#include "benchmark_context.inl"

#endif
