#ifndef INCLUDE_GUARD_NUMA_MISC_CANDIDATES_H_
#define INCLUDE_GUARD_NUMA_MISC_CANDIDATES_H_

#include "benchmark_context.hpp"

#include "numa/lina/cpu_target.hpp"
#include "numa/lina/cpu_target_scope.hpp"

#include "numa/cell/make_cell.hpp"
#include "numa/cell/tensor.hpp"

#include <variant>

namespace numa::misc {
  enum class supported_type { float_t, double_t };

  void to_pretty(std::ostream& os, supported_type);
  void from_pretty(std::istream& is, supported_type&);

  void to_json(std::ostream& os, supported_type);
  void from_json(std::istream& is, supported_type&);

  auto f_effort_supported_type();

  using matrix_supported_type = std::variant<matrix<float>, matrix<double>>;

  struct benchmark_map_dot {
    std::tuple<
        matrix_supported_type,
        matrix_supported_type,
        supported_type,
        index_t,
        cpu_target>
    operator()(
        index_t n, index_t p, supported_type, index_t num_threads, cpu_target)
        const;
  };

  template<typename F>
  class candidate {
   public:
    candidate(identifier name, F&& f);

    identifier const& name() const;

    template<typename... Ts>
    auto operator()(Ts&&...) const;

   private:
    identifier name_;
    F f_;
  };

  template<typename F>
  class candidate_dot {
   public:
    candidate_dot(identifier name, F&& f);

    identifier const& name() const;

    matrix_supported_type operator()(
        matrix_supported_type const& A_st,
        matrix_supported_type const& B_st,
        supported_type,
        index_t num_threads,
        cpu_target) const;

   private:
    identifier name_;
    F f_;
  };

  inline auto f_is_close_supported_type(double tol_pow = 0.75);
}

#include "candidates.inl"

#endif
