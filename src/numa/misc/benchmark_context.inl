namespace numa::misc {
  namespace detail {
    class benchmark_timer {
     public:
      benchmark_timer() : start_(benchmark_clock::now()) {}

      ~benchmark_timer()
      {
        auto const end = benchmark_clock::now();

        context_base<benchmark_context_core>::get().notify(
            std::chrono::duration_cast<benchmark_duration>(end - start_));
      }

     private:
      benchmark_clock::time_point start_;
    };
  }

  inline auto benchmark_context::timer()
  {
    return detail::benchmark_timer();
  }

  inline void benchmark_context::notify(benchmark_duration const dur)
  {
    context_base<detail::benchmark_context_core>::get().notify(dur);
  }
}
