#ifndef INCLUDE_GUARD_NUMA_MISC_DISCRETE_DISTRIBUTION_H_
#define INCLUDE_GUARD_NUMA_MISC_DISCRETE_DISTRIBUTION_H_

#include "numa/boot/fwd.hpp"

#include <ostream>
#include <vector>

namespace numa::misc {
  /**
   * Maps T to weights of type double. Random keys can be drawn
   * from the set in such a way that keys with a higher weight have a
   * higher likelihood of being selected, proportional to their weight.
   */
  template<typename T>
  class discrete_distribution {
   public:
    using result_type = T;

    /**
     * Adds the given @p weight to the given @p value in this
     * distribution.
     *
     * Precondition: @p weight is positive and the weight of @p value
     * is zero.
     */
    void add(T value, double weight);

    /**
     * Remove all weight from this distribution to put it in is
     * default constructed state.
     */
    void clear();

    /**
     * Returns the number of value for which there are non-zero
     * weights in this distribution.
     */
    index_t size() const;

    /**
     * Get the weight for the given value. Note, that this weight may
     * differ from the weight that was passed to the map up to machine
     * precision.
     */
    double weight(T const& value) const;

    /**
     * Get the total weight of all items in the map.
     */
    double total_weight() const;

    /**
     * Get vector of all weights in the map in unspecified order.
     * Also, this may contain duplicates.
     */
    std::vector<std::pair<T, double>> all_weights() const;

    /**
     * Get vector of all probabilities in the map. This is equal to
     * the result of all_weights() divided by total_weight().
     */
    std::vector<std::pair<T, double>> all_probabilities() const;

    /**
     * Scale the weights. This will not (more precisely: only up to
     * machine precision) change the probabilities.
     */
    void operator*=(double factor);

    /**
     * Adds all the weights from @p other to this distribution.
     */
    void operator+=(discrete_distribution const& other);

    /**
     * Draw a random value from the map, where for each value the
     * likelihood of being drawn is equal to the weight divided
     * by the total weight.
     *
     * Precondition: There is at least one item in the map.
     */
    template<typename URBG>
    T const& operator()(URBG&& g) const;

   private:
    std::vector<std::pair<T, double>> cum_weights_;
  };

  /**
   * Show a nice overview of the weights and probabilities in @p dd.
   */
  template<typename T>
  std::ostream&
  operator<<(std::ostream& os, discrete_distribution<T> const& dd);

  template<typename T>
  double dist(
      discrete_distribution<T> const& lhs, discrete_distribution<T> const& rhs);
}

#include "discrete_distribution.inl"

#endif
