#include "benchmark_context_detail.hpp"

namespace numa::misc::detail {
  std::optional<benchmark_duration> benchmark_context_core::duration() const
  {
    return duration_;
  }

  void benchmark_context_core::notify(benchmark_duration const duration)
  {
    if (!duration_.has_value()) {
      duration_ = duration;
    }
  }

  benchmark_context_core::benchmark_context_core(
      base b, thalanx_remote_t, benchmark_context_core const&)
    : benchmark_context_core(std::move(b))
  {
  }

  benchmark_context_core::benchmark_context_core(base b, root_t)
    : benchmark_context_core(std::move(b))
  {
  }

  benchmark_context_core::benchmark_context_core(base b)
    : context_base<benchmark_context_core>(std::move(b))
  {
  }
}
