#ifndef INCLUDE_GUARD_NUMA_MISC_TYPE_H_
#define INCLUDE_GUARD_NUMA_MISC_TYPE_H_

#include "numa/ctxt/context.hpp"

#include "numa/core/serialize_json.hpp"

#include <ostream>
#include <random>
#include <unordered_map>
#include <vector>

namespace numa::misc {
  enum class type_operation {
    default_construct,
    construct,
    copy_construct,
    move_construct,
    copy_assign,
    move_assign,
    destruct
  };

  struct type_log_entry {
    /**
     * The kind of operation performed.
     */
    type_operation type_op;

    /**
     * The address of the concerned object.
     */
    void const* address;

    /**
     * The address of the other object, if any; otherwise nullptr.
     */
    void const* other;

    /**
     * hash of the object
     */
    std::size_t hash_value;

    bool
    satisfies(type_operation req_type_op, std::size_t req_hash_value) const;
  };

  class type_exception : public std::exception {
  };

  /**
   * This configuration determines the likelyhood with which the
   * individual operations of the controlled types throw. All
   * double values have to be in the closed interval [0, 1]. Missing
   * values are interpreted as 0.
   */
  using type_throw_config = std::unordered_map<type_operation, double>;

  /**
   * Get a configuration in which all type_operations have the same
   * probability, except type_operation::destruct, which is not present
   * in the returned value.
   */
  type_throw_config uniform_type_throw_config(double probability);

  class type_monitor : public context_base<type_monitor> {
   public:
    std::vector<type_log_entry> const& log() const;

    std::unordered_map<void const*, type_log_entry> const& active() const;

    std::vector<std::size_t> sorted_hash_values() const;

    void dump(std::ostream& os) const;

    void clear_log();

   protected:
    using base = context_base<type_monitor>;

    type_monitor(base, thalanx_remote_t, type_monitor const& s);
    type_monitor(base, root_t);
    type_monitor(base, type_throw_config config = {});

    ~type_monitor();

   private:
    template<typename T>
    friend class tracer;

    static void notify(
        type_operation type_op,
        void const* address,
        void const* other,
        std::size_t hash_value);

    void notify_impl(
        type_operation type_op,
        void const* address,
        void const* other,
        std::size_t hash_value);

    type_throw_config config_;

    std::default_random_engine eng_;

    std::vector<type_log_entry> log_;
    std::unordered_map<void const*, type_log_entry> active_;
  };

  template<typename T>
  class tracer : public class_wrapper<T> {
   public:
    using base = class_wrapper<T>;

    explicit tracer() requires std::is_default_constructible_v<
        class_wrapper<T>>;

    template<typename U, typename... Us>
    explicit tracer(U&& value, Us&&... values) requires(
        !std::is_same_v<remove_const_ref_t<U>, tracer>);

    tracer(
        tracer const&) requires std::is_copy_constructible_v<class_wrapper<T>>;

    tracer& operator=(
        tracer const&) requires std::is_copy_assignable_v<class_wrapper<T>>;

    tracer(tracer&&) requires std::is_move_constructible_v<class_wrapper<T>>;

    tracer&
    operator=(tracer&&) requires std::is_move_assignable_v<class_wrapper<T>>;

    ~tracer();
  };

  /*
   * Class types
   */
  static constexpr int default_value    = -1;
  static constexpr int moved_from_value = -2;

  class value_int {
   public:
    value_int() noexcept;
    value_int(int value);

    value_int(value_int const&) noexcept;
    value_int& operator=(value_int const&) noexcept;

    value_int(value_int&&) noexcept;
    value_int& operator=(value_int&&) noexcept;

    ~value_int();

    int value() const;

   private:
    int value_;
  };

  class non_copy_int {
   public:
    non_copy_int() noexcept;
    non_copy_int(int value);

    non_copy_int(non_copy_int const&) = delete;
    non_copy_int& operator=(non_copy_int const&) = delete;

    non_copy_int(non_copy_int&&) noexcept;
    non_copy_int& operator=(non_copy_int&&) noexcept;

    ~non_copy_int();

    int value() const;

   private:
    int value_;
  };

  using traced_int      = tracer<value_int>;
  using traced_non_copy = tracer<non_copy_int>;

  namespace literals {
    traced_int operator"" _ti(unsigned long long int);
    traced_non_copy operator"" _tnc(unsigned long long int);
  }
}

namespace std {
  template<typename T>
  requires(
      std::is_same_v<T, numa::misc::value_int> ||
      std::is_same_v<T, numa::misc::non_copy_int>) struct hash<T> {
    using argument_type = T;

    std::size_t operator()(T const& value) const noexcept
    {
      return std::hash<int>{}(value.value());
    }
  };
}

namespace numa {
  void to_json(std::ostream& os, misc::type_operation to, int = 0);

  void to_json(std::ostream& os, misc::type_log_entry const& tle, int = 0);

  template<typename T>
  void to_json(std::ostream& os, misc::tracer<T> const& t, int = 0);

  void to_json(std::ostream& os, misc::value_int const& vi, int = 0);

  void to_json(std::ostream& os, misc::non_copy_int const& vi, int = 0);
}

#include "type.inl"

#endif
