#include "numa/boot/error.hpp"
#include "numa/core/algorithms.hpp"
#include "numa/core/functors.hpp"
#include "numa/core/range.hpp"

#include <algorithm>
#include <iomanip>
#include <random>

namespace numa::misc {
  template<typename T>
  void discrete_distribution<T>::add(T value, double const weight)
  {
    ASSERT(find_if(cum_weights_, element_find<0>(value)) == cum_weights_.end());
    ASSERT(weight > 0.0);

    if (cum_weights_.empty()) {
      cum_weights_.emplace_back(std::move(value), weight);
    }
    else {
      cum_weights_.emplace_back(
          std::move(value), cum_weights_.back().second + weight);
    }
  }

  template<typename T>
  void discrete_distribution<T>::clear()
  {
    cum_weights_.clear();
  }

  template<typename T>
  index_t discrete_distribution<T>::size() const
  {
    return static_cast<index_t>(cum_weights_.size());
  }

  template<typename T>
  double discrete_distribution<T>::weight(T const& value) const
  {
    auto const find_it = std::find_if(
        cum_weights_.begin(), cum_weights_.end(), element_find<0>(value));

    if (find_it == cum_weights_.end()) {
      return 0.0;
    }
    else if (find_it == cum_weights_.begin()) {
      return (find_it->second);
    }
    else {
      auto pre_it = find_it;
      --pre_it;

      return (find_it->second - pre_it->second);
    }
  }

  template<typename T>
  double discrete_distribution<T>::total_weight() const
  {
    if (cum_weights_.empty()) {
      return 0.0;
    }
    else {
      return cum_weights_.back().second;
    }
  }

  template<typename T>
  std::vector<std::pair<T, double>>
  discrete_distribution<T>::all_weights() const
  {
    std::vector<std::pair<T, double>> retval;
    retval.reserve(cum_weights_.size());

    double last = 0.0;
    for (auto const& [value, cum_weight]: cum_weights_) {
      retval.emplace_back(value, cum_weight - last);
      last = cum_weight;
    }

    return retval;
  }

  template<typename T>
  std::vector<std::pair<T, double>>
  discrete_distribution<T>::all_probabilities() const
  {
    auto retval = all_weights();

    double const tw = total_weight();

    for (auto& [value, weight]: retval) {
      (void)value;
      weight /= tw;
    }

    return retval;
  }

  template<typename T>
  template<typename URBG>
  T const& discrete_distribution<T>::operator()(URBG&& g) const
  {
    ASSERT(!cum_weights_.empty());

    double const rr = std::uniform_real_distribution<>(0.0, total_weight())(g);

    auto const find_it = std::lower_bound(
        cum_weights_.begin(),
        cum_weights_.end(),
        rr,
        [](auto const& pair, double const rr) { return (pair.second < rr); });

    ASSERT(find_it != cum_weights_.end());

    return (find_it->first);
  }

  template<typename T>
  double
  dist(discrete_distribution<T> const& lhs, discrete_distribution<T> const& rhs)
  {
    auto vec_lhs = lhs.all_probabilities();
    auto vec_rhs = rhs.all_probabilities();

    auto const comp = element_compare<0>(f_is_less);

    sort(vec_lhs, comp);
    sort(vec_rhs, comp);

    double retval = 0.0;

    combine_sorted_strictly(
        vec_lhs,
        vec_rhs,
        comp,
        [&retval](auto const& p) { retval += p.second; },
        [&retval](auto const& lhs, auto const& rhs) {
          ASSERT(lhs.first == rhs.first);
          retval += std::abs(lhs.second - rhs.second);
        });

    return retval;
  }

  template<typename T>
  std::ostream& operator<<(std::ostream& os, discrete_distribution<T> const& dd)
  {
    auto const weights = dd.all_weights();
    double const tw    = dd.total_weight();

    os << "/========== discrete_random_sample ==========\n";
    for (auto const& [value, weight]: weights) {
      os << std::setw(15) << value << ": " << std::setw(15) << weight << " ("
         << std::setw(6) << std::setprecision(4) << (weight / tw * 100.0)
         << "%)\n";
    }
    os << "\\============================================\n";

    return os;
  }
}
