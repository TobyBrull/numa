#ifndef INCLUDE_GUARD_NUMA_MISC_RUNTIME_DISTRIBUTION_H_
#define INCLUDE_GUARD_NUMA_MISC_RUNTIME_DISTRIBUTION_H_

#include <vector>

namespace numa::misc {
  template<typename T>
  class runtime_distribution {
   public:
    runtime_distribution(std::vector<T> vec);

    T mode() const;

   private:
    T mode_ = T(0);
  };
}

#include "runtime_distribution.inl"

#endif
