namespace numa {
//#define REFORM_NAMESPACE numa::unified
#define REFORM_NAMESPACE numa::recursive

  template<typename Target>
  requires(!std::is_reference_v<Target>) void shrink_to_fit(Target&& tgt)
  {
    numa::reserve_hard(std::forward<Target>(tgt), tgt.size());
  }

  template<typename Target>
  requires(!std::is_reference_v<Target>) void clear(Target&& tgt)
  {
    constexpr index_t Na = Target::sel_num_axes;

    numa::resize(std::forward<Target>(tgt), index<Na>(zero));
  }

  template<typename Target>
  requires(!std::is_reference_v<Target>) void clear_hard(Target&& tgt)
  {
    constexpr index_t Na = Target::sel_num_axes;

    numa::resize_hard(std::forward<Target>(tgt), index<Na>(zero));
  }

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void resize(
      Target&& tgt,
      index<Target::sel_num_axes> const& new_size,
      Initers const&... initers)
  {
    REFORM_NAMESPACE::resize(std::forward<Target>(tgt), new_size, initers...);
  }

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void resize_hard(
      Target&& tgt,
      index<Target::sel_num_axes> const& new_size,
      Initers const&... initers)
  {
    REFORM_NAMESPACE::resize_hard(
        std::forward<Target>(tgt), new_size, initers...);
  }

  template<typename Target>
  requires(!std::is_reference_v<Target>) void reserve(
      Target&& tgt, index<Target::sel_num_axes> const& min_capa)
  {
    if (tgt.must_reallocate() || !is_bounded(min_capa, tgt.capacity())) {
      numa::reserve_hard(std::forward<Target>(tgt), min_capa);
    }
  }

  template<typename Target>
  requires(!std::is_reference_v<Target>) void reserve_hard(
      Target&& tgt, index<Target::sel_num_axes> const& new_capa)
  {
    REFORM_NAMESPACE::reserve_hard(std::forward<Target>(tgt), new_capa);
  }

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void insert_till(
      Target&& tgt,
      index<Target::sel_num_axes> const& base,
      index<Target::sel_num_axes> const& till,
      Initers const&... initers)
  {
    numa::insert_size(std::forward<Target>(tgt), base, till - base, initers...);
  }

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void insert_size(
      Target&& tgt,
      index<Target::sel_num_axes> const& base,
      index<Target::sel_num_axes> const& size,
      Initers const&... initers)
  {
    REFORM_NAMESPACE::insert_size(
        std::forward<Target>(tgt), base, size, initers...);
  }

  template<typename Target>
  requires(!std::is_reference_v<Target>) void erase_till(
      Target&& tgt,
      index<Target::sel_num_axes> const& base,
      index<Target::sel_num_axes> const& till)
  {
    numa::erase_size(std::forward<Target>(tgt), base, till - base);
  }

  template<typename Target>
  requires(!std::is_reference_v<Target>) void erase_size(
      Target&& tgt,
      index<Target::sel_num_axes> const& base,
      index<Target::sel_num_axes> const& size)
  {
    REFORM_NAMESPACE::erase_size(std::forward<Target>(tgt), base, size);
  }

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void emplace_back(
      Target&& tgt, Initers const&... initers)
  {
    numa::insert_size(
        std::forward<Target>(tgt),
        tgt.size(),
        index<Target::sel_num_axes>(one),
        initers...);
  }

#undef REFORM_NAMESPACE
}
