#ifndef INCLUDE_GUARD_NUMA_CELL_SPARSE_TENSOR_H_
#define INCLUDE_GUARD_NUMA_CELL_SPARSE_TENSOR_H_

#include "fwd.hpp"
#include "growth_strategy.hpp"
#include "tensor_facade.hpp"

#include "numa/cell/for_each.hpp"
#include "numa/cell/reform_detail.hpp"

#include "numa/core/algorithms.hpp"
#include "numa/core/functors.hpp"
#include "numa/core/memory.hpp"
#include "numa/core/range.hpp"
#include "numa/core/uninitialized.hpp"
#include "numa/core/zip.hpp"

#include "numa/tmpl/dynamicizer.hpp"
#include "numa/tmpl/vary_axes_info.hpp"

#include "numa/boot/chunk.hpp"

#include <map>

namespace numa {
  template<typename T, index_t N, typename X>
  constexpr std::align_val_t tensor_sparse_alignment();

  template<typename T, index_t N, typename X, Allocator Alloc>
  class tensor_sparse_unvaried : public disable_copy_move {
   public:
    static_assert(N >= 1);
    static_assert(is_value_type<T>());

    using value_type   = T;
    using element_type = T;

    constexpr static auto axes_info = axes_info_tensor<N>();

    constexpr static index_t required_bytes(index_t storage_size);

    index<N, X> const& size() const noexcept;
    constexpr static index<N, X> capacity() noexcept;

    index_t storage_size() const noexcept;
    index_t storage_capacity() const noexcept;

    T& operator[](index_t ind);
    T const& operator[](index_t ind) const;

    range<T*> values();
    range<T const*> values() const;
    range<T const*> values_const() const;

    range<index<N, X>*> indices();
    range<index<N, X> const*> indices() const;
    range<index<N, X> const*> indices_const() const;

    using iterator_type       = tensor_sparse_iterator<T, N, X>;
    using iterator_const_type = tensor_sparse_iterator<T const, N, X>;

    iterator_type begin();
    iterator_const_type begin() const;
    iterator_const_type begin_const() const;

    iterator_type end();
    iterator_const_type end() const;
    iterator_const_type end_const() const;

    bool is_sorted_strictly() const noexcept;

    iterator_type lower_bound(index<N, X> const& ind);
    iterator_const_type lower_bound(index<N, X> const& ind) const;

    void push_back(index<N, X> const& ind, T value);
    void push_back_sorted(index<N, X> const& ind, T value);

    void pop_back();

    template<typename U, Allocator OtherAlloc, typename Transformer = f_id_t>
    void insert(
        tensor_sparse_unvaried<U, N, X, OtherAlloc> const& other,
        Transformer f = {});

    void insert(index<N, X> const& ind, T value);
    void insert_range(...);

    void erase(...);

    template<typename F>
    void erase_if(F&& f);

    void storage_resize(
        index_t storage_size, index<N, X> const& ind = {}, T value = {});

    void storage_reserve(index_t storage_capacity);

    void sort_strictly();

    Alloc const& allocator() const;

    memory<Alloc, tensor_sparse_alignment<T, N, X>()> release() noexcept;

   protected:
    tensor_sparse_unvaried(
        void* ptr,
        index_t storage_size,
        index_t storage_capacity,
        index<N, X> const& size,
        Alloc alloc) noexcept;

    void* ptr_                = nullptr;
    index_t storage_size_     = 0;
    index_t storage_capacity_ = 0;
    index<N, X> size_         = zero;
    Alloc alloc_;
  };

  template<
      typename T,
      index_t N,
      typename X      = index_t,
      Allocator Alloc = default_allocator>
  class tensor_sparse
    : public tensor_sparse_unvaried<T, N, X, Alloc>
    , public vary_axes_facade<tensor_sparse<T, N, X, Alloc>, N> {
   public:
    tensor_sparse(Alloc alloc = {}) noexcept;

    tensor_sparse(tensor_sparse const&) = delete;
    tensor_sparse& operator=(tensor_sparse const&) = delete;

    tensor_sparse(tensor_sparse&&) noexcept;
    tensor_sparse& operator=(tensor_sparse&&) noexcept;

    ~tensor_sparse();

    constexpr static auto vary_axes_info = vary_axes_info_dynamic<N>();

    /*
     * unvaried
     */
    using unvaried_type = tensor_sparse_unvaried<T, N, X, Alloc>;

    unvaried_type& unvaried();
    unvaried_type const& unvaried() const;
    unvaried_type const& unvaried_const() const;

    template<index_t... Axes>
    requires(is_variation<N>(index_list<Axes...>).value) auto vary_axes_impl(
        index_list_t<Axes...>);

   private:
    template<typename, typename, index_t, typename, Allocator, typename>
    friend class tensor_sparse_axes;
  };

  template<
      typename T,
      typename X      = index_t,
      Allocator Alloc = default_allocator>
  using sparse_cuboid = tensor_sparse<T, 3, X, Alloc>;

  template<
      typename T,
      typename X      = index_t,
      Allocator Alloc = default_allocator>
  using sparse_matrix = tensor_sparse<T, 2, X, Alloc>;

  template<
      typename T,
      typename X      = index_t,
      Allocator Alloc = default_allocator>
  using sparse_vector = tensor_sparse<T, 1, X, Alloc>;

  template<
      typename T,
      index_t N,
      typename X,
      typename U,
      Allocator AllocLhs,
      Allocator AllocRhs>
  bool operator==(
      tensor_sparse_unvaried<T, N, X, AllocLhs> const& lhs,
      tensor_sparse_unvaried<U, N, X, AllocRhs> const& rhs);

  template<
      typename T,
      index_t N,
      typename X,
      typename U,
      Allocator AllocLhs,
      Allocator AllocRhs>
  bool operator!=(
      tensor_sparse_unvaried<T, N, X, AllocLhs> const& lhs,
      tensor_sparse_unvaried<U, N, X, AllocRhs> const& rhs);

  /*
   * make-copy
   */
  template<
      typename T,
      index_t N,
      typename X,
      Allocator Alloc,
      Allocator NewAlloc>
  tensor_sparse<T, N, X, NewAlloc> make_copy(
      tensor_sparse_unvaried<T, N, X, Alloc> const& st, NewAlloc new_alloc);

  template<typename T, index_t N, typename X, Allocator Alloc>
  tensor_sparse<T, N, X, Alloc>
  make_copy(tensor_sparse_unvaried<T, N, X, Alloc> const& st);

  //  template<WeakCell SparseTensorType,
  //           typename T, index_t N, Allocator Alloc>
  //  auto make_cell (tensor_sparse_unvaried<T, N, Alloc> const& st);
  //
  //  template<WeakCell SparseTensorType,
  //           typename... Ts, index_t N, Allocator... Allocs>
  //  auto make_cell_sorted_strictly (
  //      tensor_sparse_unvaried<Ts, N, Allocs> const&... st);

  /*
   * add-strictly-sorted
   */
  //  template<Allocator Alloc, typename... Ts>
  //  struct add_sorted_strictly
  //  {
  //    template<typename T, index_t N>
  //    add_sorted_strictly (tensor_sparse_unvaried<T, N> const& st, Alloc
  //    alloc);
  //
  //    template<typename T, index_t N>
  //    add_sorted_strictly (tensor_sparse_unvaried<T, N, Alloc> const& st);
  //
  //    template<typename T, index_t N>
  //    add_sorted_strictly<Alloc, Ts..., tensor_sparse_unvaried<T, N> const&>
  //    operator () (tensor_sparse_unvaried<T, N> const& st) const;
  //
  //    template<typename T, index_t N, Allocator OtherAlloc>
  //    add_sorted_strictly<Alloc, Ts..., tensor_sparse_unvaried<T, N>>
  //    operator () (tensor_sparse<T, N, OtherAlloc>&& st) const;
  //
  //    auto run () const;
  //
  //    Alloc alloc_;
  //    std::tuple<Ts...> spans_tuple_;
  //  };

  template<
      typename T,
      typename U,
      index_t N,
      typename X,
      Allocator AllocLhs,
      Allocator AllocRhs,
      Allocator Alloc = default_allocator>
  tensor_sparse<std::common_type_t<T, U>, N, X, Alloc>
  simple_add_sorted_strictly(
      T const& lhs_factor,
      tensor_sparse_unvaried<T, N, X, AllocLhs> const& lhs,
      U const& rhs_factor,
      tensor_sparse_unvaried<U, N, X, AllocRhs> const& rhs,
      Alloc alloc = {});
}

#include "tensor_sparse.inl"

#endif
