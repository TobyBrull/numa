#ifndef INCLUDE_GUARD_NUMA_CELL_BAND_TENSOR_H_
#define INCLUDE_GUARD_NUMA_CELL_BAND_TENSOR_H_

#include "t_span_staged.hpp"
#include "tensor_facade.hpp"

#include "numa/core/memory.hpp"

#include "numa/tmpl/vary_axes_info.hpp"

namespace numa {
  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  class tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>
    : public unvaried_facade<
          tensor_staged_unvaried<
              T,
              Nd,
              index_list_t<Ls...>,
              index_list_t<Rs...>>,
          T,
          Nd / 2,
          Nd>
    , public disable_copy_move {
   public:
    static constexpr index_t Na = Nd / 2;

    using ls_t = index_list_t<Ls...>;
    using rs_t = index_list_t<Rs...>;

    static_assert(is_valid_band_parameters<Nd>(ls_t{}, rs_t{}));

    constexpr static auto axes_info = axes_info_mirror_tensor<Na>();

    index<Na> const& size() const;
    index<Na> const& capacity() const;

    /*
     * span
     */
    using span_type       = t_span_staged<T, Nd, ls_t, rs_t>;
    using span_const_type = t_span_staged<T const, Nd, ls_t, rs_t>;

    span_type span();
    span_const_type span() const;
    span_const_type span_const() const;

    /*
     * iterator
     */
    using iterator_type       = tensor_staged_iterator<T, Nd, ls_t, rs_t>;
    using iterator_const_type = tensor_staged_iterator<T const, Nd, ls_t, rs_t>;

    iterator_type begin();
    iterator_const_type begin() const;
    iterator_const_type begin_const() const;

    iterator_type end();
    iterator_const_type end() const;
    iterator_const_type end_const() const;

    /*
     * implementation of unvaried-facade
     */
    T* get_ptr_impl(index<Nd> const& ind);
    T const* const_get_ptr_impl(index<Nd> const& ind) const;

    bool is_bounded_impl(index<Nd> const& ind) const;

    auto iterator_impl(index<Nd> const& ind);
    auto const_iterator_impl(index<Nd> const& ind) const;

   protected:
    tensor_staged_unvaried(
        T* ptr, index<Na> const& size, index<Na> const& capacity);

    index<Nd> stride() const;

    T* ptr_             = nullptr;
    index<Na> size_     = zero;
    index<Na> capacity_ = zero;
  };

  template<
      typename T,
      index_t Na,
      typename LowerBound,
      typename UpperBound,
      Allocator Alloc = default_allocator>
  class tensor_staged;

  template<
      typename T,
      index_t Nd,
      index_t... Ls,
      index_t... Rs,
      Allocator Alloc>
  class tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>
    : public tensor_staged_unvaried<
          T,
          Nd,
          index_list_t<Ls...>,
          index_list_t<Rs...>>
    , public vary_axes_facade<
          tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>,
          Nd / 2> {
   public:
    static constexpr index_t Na = Nd / 2;

    using ls_t = index_list_t<Ls...>;
    using rs_t = index_list_t<Rs...>;

    static_assert(is_valid_band_parameters<Nd>(ls_t{}, rs_t{}));

    tensor_staged(Alloc alloc = {}) noexcept;

    tensor_staged(tensor_staged const&) = delete;
    tensor_staged& operator=(tensor_staged const&) = delete;

    tensor_staged(tensor_staged&&) noexcept;
    tensor_staged& operator=(tensor_staged&&) noexcept;

    ~tensor_staged();

    Alloc const& allocator() const;

    constexpr static index_t required_elements(index<Na> const& size);

    constexpr static auto vary_axes_info = vary_axes_info_dynamic<Na>();

    using unvaried_type = tensor_staged_unvaried<T, Nd, ls_t, rs_t>;

    unvaried_type& unvaried();
    unvaried_type const& unvaried() const;
    unvaried_type const& unvaried_const() const;

    template<index_t... Axes>
    auto vary_axes_impl(index_list_t<Axes...>) requires(
        is_variation<Na>(index_list<Axes...>).value);

   private:
    template<typename, index_t, typename, typename, Allocator, typename>
    friend class tensor_staged_axes;

    Alloc alloc_;
  };

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  struct span_to_cell<
      t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>> {
    using type = tensor_staged<
        std::remove_const_t<T>,
        Nd,
        index_list_t<Ls...>,
        index_list_t<Rs...>>;
  };

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  struct span_to_cell<
      tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>> {
    using type = tensor_staged<
        std::remove_const_t<T>,
        Nd,
        index_list_t<Ls...>,
        index_list_t<Rs...>>;
  };

  template<typename T, index_t Bandwidth, Allocator Alloc = default_allocator>
  using matrix_staged = tensor_staged<
      T,
      2,
      index_list_t<Bandwidth>,
      index_list_t<Bandwidth>,
      Alloc>;

  template<typename T, index_t Nd, Allocator Alloc = default_allocator>
  using tensor_diagonal = tensor_staged<
      T,
      Nd,
      index_list_uniform_t<Nd / 2, 0>,
      index_list_uniform_t<Nd / 2, 0>,
      Alloc>;

  template<typename T, Allocator Alloc = default_allocator>
  using matrix_diagonal = tensor_diagonal<T, 2, Alloc>;

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  bool operator==(
      tensor_staged_unvaried<
          T,
          Nd,
          index_list_t<Ls...>,
          index_list_t<Rs...>> const& lhs,
      tensor_staged_unvaried<
          T,
          Nd,
          index_list_t<Ls...>,
          index_list_t<Rs...>> const& rhs);

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  bool operator!=(
      tensor_staged_unvaried<
          T,
          Nd,
          index_list_t<Ls...>,
          index_list_t<Rs...>> const& lhs,
      tensor_staged_unvaried<
          T,
          Nd,
          index_list_t<Ls...>,
          index_list_t<Rs...>> const& rhs);
}

#include "tensor_staged.inl"

#endif
