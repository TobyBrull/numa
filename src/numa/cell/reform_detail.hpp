#ifndef INCLUDE_GUARD_NUMA_CELL_REFORM_DETAIL_H_
#define INCLUDE_GUARD_NUMA_CELL_REFORM_DETAIL_H_

#include "concepts_reform.hpp"
#include "concepts_reform_recursive.hpp"

namespace numa::detail {
  template<typename Tgt, index_t N, typename... Initers>
  requires(!std::is_reference_v<Tgt>) void initial_resize(
      Tgt&& tgt,
      index<N> const& size,
      index<N> const& capacity,
      Initers const&... initers);

  template<typename Structure, index_t N, typename... Initers>
  requires(Structure::vary_axes_info.num_axes == N) void initial_resize_is_not_fixed(
      Structure& strct,
      index<N> const& size,
      index<N> const& capacity,
      Initers const&... initers);
}

#include "reform_detail.inl"

#endif
