namespace numa::recursive {
  namespace detail {
    enum class resize_type { normal, hard };

    template<typename Target, typename... Initers>
    void resize_impl(
        Target& tgt,
        resize_type const rt,
        index<Target::sel_num_axes> const new_size,
        Initers const&... initers)
    {
      constexpr index_t Na = tgt.sel_num_axes;

      auto const size = tgt.size();
      auto const capa = tgt.capacity();

      bool const has_wrong_capacity = [&] {
        switch (rt) {
          case resize_type::normal:
            return !is_bounded(new_size, capa);

          case resize_type::hard:
            return (capa != new_size);
        }

        UNREACHABLE();
      }();

      bool const does_reallocate =
          (tgt.must_reallocate() || has_wrong_capacity);

      if (does_reallocate) {
        auto const new_capa = [&] {
          switch (rt) {
            case resize_type::normal:
              return growth_strategy(capa, new_size);

            case resize_type::hard:
              return new_size;
          }

          UNREACHABLE();
        }();

        auto tloc = tgt.translocation(new_capa);

        if constexpr (std::is_same_v<
                          decltype(tloc),
                          translocation_forbidden_t>) {
          THROW("numa::resize() attempted to do a translocation on a cell "
                "that does not allow this");
        }
        else {
          /*
           * Move the part that can be kept.
           */
          auto const min_size = min(size, new_size);

          tloc.move(zero, min_size, zero);

          tloc.set_size(min_size);

          /*
           * Construct remainder.
           */
          index_t i = 0;

          try {
            for (; i < Na; ++i) {
              if (size[i] < new_size[i]) {
                auto const base = size[i] * index<Na>(unit, i);
                auto const till = splice(new_size, min_size, i + 1);

                tloc.fill(base, till - base, initers...);
              }
            }
          }
          catch (...) {
            for (index_t j = 0; j < i; ++j) {
              if (size[j] < new_size[j]) {
                auto const base = size[j] * index<Na>(unit, j);
                auto const till = splice(new_size, min_size, j + 1);

                tloc.destruct(base, till - base);
              }
            }

            throw;
          }

          tloc.set_size(new_size);
        }
      }
      else {
        static_assert(Na >= 1);
        ASSERT(is_bounded(new_size, capa));

        auto const min_size = min(size, new_size);

        /*
         * Destruct the parts that are not needed any longer.
         * None of the operations in this scope can throw.
         */
        {
          index_t i = (Na - 1);

          while (true) {
            if (new_size[i] < size[i]) {
              auto const base = new_size[i] * index<Na>(unit, i);
              auto const till = splice(size, min_size, i + 1);

              tgt.destruct(base, till - base);
            }

            if (i == 0) {
              break;
            }

            --i;
          }
        }

        tgt.set_size(min_size);

        /*
         * Construct the newly needed parts.
         */
        index_t i = 0;

        try {
          for (; i < Na; ++i) {
            if (size[i] < new_size[i]) {
              auto const base = size[i] * index<Na>(unit, i);
              auto const till = splice(new_size, min_size, i + 1);

              tgt.fill_construct(base, till - base, initers...);
            }
          }
        }
        catch (...) {
          for (index_t j = 0; j < i; ++j) {
            if (size[j] < new_size[j]) {
              auto const base = size[j] * index<Na>(unit, j);
              auto const till = splice(new_size, min_size, j + 1);

              tgt.destruct(base, till - base);
            }
          }

          throw;
        }

        tgt.set_size(new_size);
      }
    }
  }

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void resize(
      Target&& tgt,
      index<Target::sel_num_axes> const& new_size,
      Initers const&... initers)
  {
    constexpr index_t Na = Target::sel_num_axes;

    if constexpr (numa::detail::has_resize<Target, index<Na>, Initers...>) {
      tgt.resize(new_size, initers...);
    }
    else if constexpr (RecursiveReformTarget<Target>) {
      tgt.recurse(
          [](auto&& tgt, auto const& loc_new_size, auto const&... initers) {
            numa::recursive::resize(
                std::forward<decltype(tgt)>(tgt), loc_new_size, initers...);
          },
          std::tuple<Initers const&...>(initers...),
          new_size);
    }
    else {
      static_assert(CellReformTarget<Target>);

      detail::resize_impl(
          tgt, detail::resize_type::normal, new_size, initers...);

      ASSERT(tgt.size() == new_size);
    }
  }

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void resize_hard(
      Target&& tgt,
      index<Target::sel_num_axes> const& new_size,
      Initers const&... initers)
  {
    constexpr index_t Na = Target::sel_num_axes;

    if constexpr (numa::detail::
                      has_resize_hard<Target, index<Na>, Initers...>) {
      tgt.resize_hard(new_size, initers...);
    }
    else if constexpr (RecursiveReformTarget<Target>) {
      tgt.recurse(
          [](auto&& tgt, auto const& loc_new_size, auto const&... initers) {
            numa::recursive::resize_hard(
                std::forward<decltype(tgt)>(tgt), loc_new_size, initers...);
          },
          std::tuple<Initers const&...>(initers...),
          new_size);
    }
    else {
      static_assert(CellReformTarget<Target>);

      detail::resize_impl(tgt, detail::resize_type::hard, new_size, initers...);

      ASSERT(tgt.size() == new_size);
    }
  }

  template<typename Target>
  requires(!std::is_reference_v<Target>) void reserve_hard(
      Target&& tgt, index<Target::sel_num_axes> const& req_capa)
  {
    constexpr index_t Na = Target::sel_num_axes;

    if constexpr (numa::detail::has_reserve_hard<Target, index<Na>>) {
      tgt.reserve_hard(req_capa);
    }
    else if constexpr (RecursiveReformTarget<Target>) {
      tgt.recurse(
          [](auto&& tgt, auto const& loc_req_capa) {
            numa::recursive::reserve_hard(
                std::forward<decltype(tgt)>(tgt), loc_req_capa);
          },
          std::tuple<>(),
          req_capa);
    }
    else {
      static_assert(CellReformTarget<Target>);

      auto const size = tgt.size();
      auto const capa = tgt.capacity();

      auto const new_capa = max(size, req_capa);

      if (tgt.must_reallocate() || (new_capa != capa)) {
        auto tloc = tgt.translocation(new_capa);

        if constexpr (std::is_same_v<
                          decltype(tloc),
                          translocation_forbidden_t>) {
          THROW("reserve_hard attempting translocation on forbidden");
        }
        else {
          tloc.move(zero, size, zero);

          tloc.set_size(size);
        }
      }

      ASSERT(is_bounded(new_capa, tgt.capacity()));
    }
  }

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void insert_size(
      Target&& tgt,
      index<Target::sel_num_axes> const& insert_base,
      index<Target::sel_num_axes> const& insert_size,
      Initers const&... initers)
  {
    constexpr index_t Na = Target::sel_num_axes;

    if constexpr (numa::detail::has_insert_size<
                      Target,
                      index<Na>,
                      index<Na>,
                      Initers...>) {
      tgt.insert_size(insert_base, insert_size, initers...);
    }
    else if constexpr (RecursiveReformTarget<Target>) {
      tgt.recurse(
          [&](auto&& tgt,
              auto const& loc_i_base,
              auto const& loc_i_size,
              auto const&... initers) {
            numa::recursive::insert_size(
                std::forward<decltype(tgt)>(tgt),
                loc_i_base,
                loc_i_size,
                initers...);
          },
          std::tuple<Initers const&...>(initers...),
          insert_base,
          insert_size);
    }
    else {
      static_assert(CellReformTarget<Target>);

      constexpr index_t Na = Target::sel_num_axes;

      auto const capa     = tgt.capacity();
      auto const old_size = tgt.size();

      auto const new_size = old_size + insert_size;

      if (tgt.must_reallocate() || !is_bounded(new_size, capa)) {
        if constexpr (std::is_same_v<
                          decltype(tgt.translocation(new_size)),
                          translocation_forbidden_t>) {
          THROW("insert_size attempting translocation on forbidden");
        }
        else {
          auto const new_capa = growth_strategy(capa, new_size);

          using numa::detail::make_reform_axis_array;
          using numa::detail::translate_insert_size;

          tgt.translate(
              new_size,
              new_capa,
              make_reform_axis_array<Na, translate_insert_size>(
                  old_size, insert_base, insert_size),
              initers...);
        }
      }
      else {
        using numa::detail::inplace_insert_size;
        using numa::detail::make_reform_axis_array;

        tgt.inplace(
            new_size,
            make_reform_axis_array<Na, inplace_insert_size>(
                old_size, insert_base, insert_size),
            initers...);
      }

      ASSERT(tgt.size() == new_size);
    }
  }

  namespace detail {
    template<typename Translocation, index_t N, index_t I>
    void erase_size_impl_translocation(
        Translocation& tloc,
        index<N> const& size,
        index<N> const& e_base,
        index<N> const& e_size,
        index<N>& t_base,
        index<N>& t_size,
        index<N>& t_dest,
        c_index_t<I>)
    {
      if constexpr (I == N) {
        tloc.move(t_base, t_size, t_dest);
      }
      else {
        t_base[I] = 0;
        t_size[I] = e_base[I];
        t_dest[I] = 0;

        erase_size_impl_translocation(
            tloc, size, e_base, e_size, t_base, t_size, t_dest, c_index<I + 1>);

        index_t const kk = (e_base[I] + e_size[I]);
        t_base[I]        = kk;
        t_size[I]        = size[I] - kk;
        t_dest[I]        = e_base[I];

        erase_size_impl_translocation(
            tloc, size, e_base, e_size, t_base, t_size, t_dest, c_index<I + 1>);
      }
    }
  }

  template<typename Target>
  requires(!std::is_reference_v<Target>) void erase_size(
      Target&& tgt,
      index<Target::sel_num_axes> const& erase_base,
      index<Target::sel_num_axes> const& erase_size)
  {
    constexpr index_t Na = Target::sel_num_axes;

    if constexpr (numa::detail::has_erase_size<Target, index<Na>, index<Na>>) {
      tgt.erase_size(erase_base, erase_size);
    }
    else if constexpr (RecursiveReformTarget<Target>) {
      tgt.recurse(
          [&](auto&& tgt,
              auto const& loc_base,
              auto const& loc_size,
              auto const&... initers) {
            numa::recursive::erase_size(
                std::forward<decltype(tgt)>(tgt),
                loc_base,
                loc_size,
                initers...);
          },
          std::tuple<>(),
          erase_base,
          erase_size);
    }
    else {
      constexpr index_t Na = Target::sel_num_axes;

      auto const size = tgt.size();
      auto const capa = tgt.capacity();

      static_assert(CellReformTarget<Target>);

      if (tgt.must_reallocate()) {
        auto tloc = tgt.translocation(capa);

        if constexpr (std::is_same_v<
                          decltype(tloc),
                          translocation_forbidden_t>) {
          THROW("erase_size attempting translocation on forbidden");
        }
        else {
          index<Na> t_base;
          index<Na> t_size;
          index<Na> t_dest;

          detail::erase_size_impl_translocation(
              tloc,
              size,
              erase_base,
              erase_size,
              t_base,
              t_size,
              t_dest,
              c_index<0>);

          tloc.set_size(size - erase_size);
        }
      }
      else {
        using numa::detail::inplace_erase_size;

        tgt.inplace(
            size - erase_size,
            numa::detail::make_reform_axis_array<Na, inplace_erase_size>(
                size, erase_base, erase_size));
      }
    }
  }
}
