#ifndef INCLUDE_GUARD_NUMA_CELL_CONCEPTS_REFORM_H_
#define INCLUDE_GUARD_NUMA_CELL_CONCEPTS_REFORM_H_

namespace numa::detail {
#define MEMBER_FUNCTION_CHECK(memfun)                              \
  template<typename Target, typename... Ts>                        \
  concept has_##memfun = requires(Target tgt, Ts const&... params) \
  {                                                                \
    {tgt.memfun(params...)};                                       \
  };

  MEMBER_FUNCTION_CHECK(resize);
  MEMBER_FUNCTION_CHECK(resize_hard);
  MEMBER_FUNCTION_CHECK(reserve_hard);
  MEMBER_FUNCTION_CHECK(insert_size);
  MEMBER_FUNCTION_CHECK(erase_size);

#undef MEMBER_FUNCTION_CHECK
}

#endif
