#ifndef INCLUDE_GUARD_NUMA_CELL_FIXED_CAPACITY_TENSOR_H_
#define INCLUDE_GUARD_NUMA_CELL_FIXED_CAPACITY_TENSOR_H_

#include "fwd.hpp"
#include "tensor.hpp"

#include "numa/core/index_detail.hpp"

#include "numa/tmpl/vary_axes_info.hpp"

namespace numa {
  template<typename T, index_t... Sizes>
  class tensor_fixed_capacity_unvaried
    : public unvaried_facade<
          tensor_fixed_capacity_unvaried<T, Sizes...>,
          T,
          sizeof...(Sizes),
          sizeof...(Sizes)>
    , public disable_copy_move {
   public:
    constexpr static index_t N = sizeof...(Sizes);

    static_assert(N >= 1);

    constexpr static auto axes_info = axes_info_tensor<N>();

    index<N> const& size() const;
    constexpr index<N> capacity() const;

    /*
     * span
     */
    using span_type       = t_span<T, N>;
    using span_const_type = t_span<T const, N>;

    span_type span();
    span_const_type span() const;
    span_const_type span_const() const;

    /*
     * iterator
     */
    using iterator_type = tensor_fixed_capacity_iterator<T, Sizes...>;
    using iterator_const_type =
        tensor_fixed_capacity_iterator<T const, Sizes...>;

    iterator_type begin();
    iterator_const_type begin() const;
    iterator_const_type begin_const() const;

    iterator_type end();
    iterator_const_type end() const;
    iterator_const_type end_const() const;

    /*
     * implementation of unvaried-facade
     */
    T* get_ptr_impl(index<N> const& ind);
    T const* const_get_ptr_impl(index<N> const& ind) const;

    bool is_bounded_impl(index<N> const& ind) const;

    auto iterator_impl(index<N> const& ind);
    auto const_iterator_impl(index<N> const& ind) const;

   protected:
    tensor_fixed_capacity_unvaried(index<N> const& size = zero);

    index_t end_offset() const;

    T* ptr();
    T const* const_ptr() const;

    alignas(T) char m_data[(Sizes * ...) * sizeof(T)];

    index<N> size_ = zero;
  };

  template<typename T, index_t... Sizes>
  class tensor_fixed_capacity
    : public tensor_fixed_capacity_unvaried<T, Sizes...>
    , public vary_axes_facade<
          tensor_fixed_capacity<T, Sizes...>,
          sizeof...(Sizes)> {
   public:
    constexpr static index_t N = sizeof...(Sizes);

    tensor_fixed_capacity();

    tensor_fixed_capacity(tensor_fixed_capacity const&);
    tensor_fixed_capacity& operator=(tensor_fixed_capacity const&);

    tensor_fixed_capacity(tensor_fixed_capacity&&);
    tensor_fixed_capacity& operator=(tensor_fixed_capacity&&);

    ~tensor_fixed_capacity();

    constexpr static index_t required_elements(index<N> const& size);

    constexpr static auto vary_axes_info =
        vary_axes_info_fixed_capacity<Sizes...>();

    using unvaried_type = tensor_fixed_capacity_unvaried<T, Sizes...>;

    unvaried_type& unvaried();
    unvaried_type const& unvaried() const;
    unvaried_type const& unvaried_const() const;

    template<index_t... Axes>
    constexpr auto vary_axes_impl(index_list_t<Axes...>) requires(
        is_variation<N>(index_list<Axes...>).value);

   private:
    template<typename, typename, index_t...>
    friend class tensor_fixed_capacity_axes;
  };

  /*
   * stack specializations
   */
  template<typename T, index_t I0>
  using vector_fixed_capacity = tensor_fixed_capacity<T, I0>;

  template<typename T, index_t I0, index_t I1>
  using matrix_fixed_capacity = tensor_fixed_capacity<T, I0, I1>;

  template<typename T, index_t I0, index_t I1, index_t I2>
  using cuboid_fixed_capacity = tensor_fixed_capacity<T, I0, I1, I2>;

  template<typename T, index_t... Sizes>
  bool operator==(
      tensor_fixed_capacity_unvaried<T, Sizes...> const& lhs,
      tensor_fixed_capacity_unvaried<T, Sizes...> const& rhs);

  template<typename T, index_t... Sizes>
  bool operator!=(
      tensor_fixed_capacity_unvaried<T, Sizes...> const& lhs,
      tensor_fixed_capacity_unvaried<T, Sizes...> const& rhs);
}

#include "tensor_fixed_capacity.inl"

#endif
