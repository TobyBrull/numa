namespace numa {
  /*
   * Implementation of tensor-span
   */
  template<typename T, index_t N>
  constexpr t_span<T, N>::t_span(
      T* const origin, index<N> const& size, index<N> const& stride)
    : origin_(origin), size_(size), stride_(stride)
  {
  }

  template<typename T, index_t N>
  constexpr t_span<T, N>::operator t_span<T const, N>() const
      requires(!is_const<T>().value)
  {
    return {origin_, size_, stride_};
  }

  template<typename T, index_t N>
  T* t_span<T, N>::get_ptr_impl(index<N> const& ind) const
  {
    return (origin_ + dot(ind, stride_));
  }

  template<typename T, index_t N>
  bool t_span<T, N>::is_bounded_impl(index<N> const& ind) const
  {
    if constexpr (N > 0) {
      return is_bounded(index<N>(zero), ind) && is_bounded_strictly(ind, size_);
    }
    else {
      return true;
    }
  }

  template<typename T, index_t N>
  auto t_span<T, N>::iterator_impl(index<N> const& ind) const
  {
    return t_span_iterator<T, N>{get_ptr_impl(ind), ind, size_, stride_};
  }

  template<typename T, index_t N>
  auto t_span<T, N>::begin() const
  {
    return t_span_iterator<T, N>{origin_, zero, size_, stride_};
  }

  template<typename T, index_t N>
  auto t_span<T, N>::end() const
  {
    if (is_empty(size_)) {
      return begin();
    }
    else {
      return t_span_iterator<T, N>{
          get_ptr_impl(size_ - index<N>(one)) + 1,
          detail::end_index(index<N>(zero), size_),
          size_,
          stride_};
    }
  }

  template<typename T, index_t N>
  template<index_t... Axes>
  requires(is_variation<N>(index_list<Axes...>)
               .value) auto t_span<T, N>::axes_impl(index_list_t<Axes...>) const
  {
    return t_span_axes<T, N, index_list_t<Axes...>>{this};
  }

  template<typename T, index_t N>
  template<index_t... Dims>
  requires(is_variation<N>(index_list<Dims...>)
               .value) auto t_span<T, N>::dims_impl(index_list_t<Dims...>
                                                        sel_dims) const
  {
    return this->axes_impl(sel_dims);
  }

  template<typename T, index_t N>
  t_span<T, N> t_span<T, N>::transposed() const requires((N % 2) == 0)
  {
    return as_t_span<N>(index_list_transposition<N / 2>());
  }

  template<typename T, index_t N>
  template<index_t M, index_t... Axes>
  t_span<T, M> t_span<T, N>::as_t_span(index_list_t<Axes...> ax) const requires(
      (sizeof...(Axes) == N) && is_variation<M>(index_list<Axes...>).value)
  {
    static_assert(N <= M);

    return t_span<T, M>(
        origin_,
        join<M>(size_, ax, index<M - N>(one)),
        join<M>(stride_, ax, index<M - N>(zero)));
  }

  template<typename T, index_t N>
  template<index_t... Axes>
  t_span<T, 3> t_span<T, N>::as_cuboid_span(index_list_t<Axes...> ax) const
      requires(
          (sizeof...(Axes) == N) && is_variation<3>(index_list<Axes...>).value)
  {
    static_assert(N <= 3);

    return as_t_span<3>(ax);
  }

  template<typename T, index_t N>
  template<index_t... Axes>
  t_span<T, 2> t_span<T, N>::as_matrix_span(index_list_t<Axes...> ax) const
      requires(
          (sizeof...(Axes) == N) && is_variation<2>(index_list<Axes...>).value)
  {
    static_assert(N <= 2);

    return as_t_span<2>(ax);
  }

  template<typename T, index_t N>
  t_span<T, 1> t_span<T, N>::as_vector_span() const
  {
    /*
     * Nothing interesting can be done by calling as_vector_span on a
     * vector_span. This is different from calling as_matrix_span on a
     * matrix_span, which can be used to transpose the span.
     *
     * For this reason, here, N has to be smaller than 1 while in
     * as_matrix_span and as_cuboid_span N can also be equal to 2 or 3.
     *
     * Thus, as only N == 0 is valid here, there is no point in this
     * function taking a parameter.
     */
    static_assert(N < 1);

    return as_t_span<1>(index_list<>);
  }

  template<typename T, index_t N, typename U>
  requires((c_type<decay_t<T>> == c_type<decay_t<U>>).value) bool
  operator==(t_span<T, N> const& lhs, t_span<U, N> const& rhs)
  {
    if (lhs.size() != rhs.size()) {
      return false;
    }

    return all_of(f_is_equal, lhs, rhs);
  }

  template<typename T, index_t N, typename U>
  requires((c_type<decay_t<T>> == c_type<decay_t<U>>).value) bool
  operator!=(t_span<T, N> const& lhs, t_span<U, N> const& rhs)
  {
    return !(lhs == rhs);
  }

  /*
   * Implementation of tensor-span-axes
   */
  template<typename T, index_t N, index_t... SelectedAxes>
  t_span_axes<T, N, index_list_t<SelectedAxes...>>::t_span_axes(
      t_span<T, N> const* span)
    : span_(span)
  {
  }

  template<typename T, index_t N, index_t... SelectedAxes>
  t_span<T, N>
  t_span_axes<T, N, index_list_t<SelectedAxes...>>::transposed() const
      requires(K == 2)
  {
    constexpr index_list_t<SelectedAxes...> ax;
    constexpr auto plb_ax = plumb(c_index<N>, ax);

    auto const ax_size   = select_at(span_->size(), ax);
    auto const ax_stride = select_at(span_->stride_, ax);

    auto const plb_size   = select_at(span_->size(), plb_ax);
    auto const plb_stride = select_at(span_->stride_, plb_ax);

    auto const reord_size   = select_at(ax_size, index_list_riota<K>());
    auto const reord_stride = select_at(ax_stride, index_list_riota<K>());

    auto const rv_size   = join<N>(reord_size, ax, plb_size);
    auto const rv_stride = join<N>(reord_stride, ax, plb_stride);

    return t_span<T, N>(span_->origin_, rv_size, rv_stride);
  }

  template<typename T, index_t N, index_t... SelectedAxes>
  template<index_t... Axes>
  requires(
      (sizeof...(Axes) == sizeof...(SelectedAxes)) &&
      is_permutation(index_list<Axes...>).value)
      t_span<T, N> t_span_axes<T, N, index_list_t<SelectedAxes...>>::reordered(
          index_list_t<Axes...> reordering) const
  {
    constexpr index_list_t<SelectedAxes...> ax;
    constexpr auto plb_ax = plumb(c_index<N>, ax);

    ASSERT(is_sorted(ax));
    ASSERT(is_permutation(reordering));

    auto const ax_size   = select_at(span_->size(), ax);
    auto const ax_stride = select_at(span_->stride_, ax);

    auto const plb_size   = select_at(span_->size(), plb_ax);
    auto const plb_stride = select_at(span_->stride_, plb_ax);

    auto const reord_size   = select_at(ax_size, reordering);
    auto const reord_stride = select_at(ax_stride, reordering);

    auto const rv_size   = join<N>(reord_size, ax, plb_size);
    auto const rv_stride = join<N>(reord_stride, ax, plb_stride);

    return t_span<T, N>(span_->origin_, rv_size, rv_stride);
  }

  template<typename T, index_t N, index_t... SelectedAxes>
  t_span<T, N>
  t_span_axes<T, N, index_list_t<SelectedAxes...>>::reversed() const
  {
    constexpr index_list_t<SelectedAxes...> ax;

    auto new_stride = span_->stride_;
    auto new_origin = span_->origin_;

    auto const ax_stride = select_at(span_->stride_, ax);

    if (!is_empty(span_->size())) {
      ASSERT(!is_empty(ax_stride));

      auto const ax_size_m_1 = select_at(span_->size(), ax) - index<K>(one);

      new_origin += dot(ax_size_m_1, ax_stride);
    }

    overwrite(new_stride, ax, -ax_stride);

    return t_span<T, N>(new_origin, span_->size(), new_stride);
  }

  template<typename T, index_t N, index_t... SelectedAxes>
  template<SubMode SM>
  t_span<T, N> t_span_axes<T, N, index_list_t<SelectedAxes...>>::sub(
      SM, index<K> const& from, index<K> const& size_or_till) const
  {
    constexpr index_list_t<SelectedAxes...> ax;

    auto const all_from = join<N>(from, ax, zero);

    auto const size = sub_mode_convert(SM{}, from, size_or_till, as_size);

    auto all_size = span_->size();
    overwrite(all_size, ax, size);

    ASSERT(is_bounded(index<N>(zero), all_from));
    ASSERT(is_bounded(all_from + all_size, span_->size()));

    T* const rv_origin = span_->get_ptr(all_from);

    return t_span<T, N>(rv_origin, all_size, span_->stride_);
  }

  template<typename T, index_t N, index_t... SelectedAxes>
  template<typename... Index>
  requires constructs_index_v<sizeof...(SelectedAxes), Index...> t_span<T, N>
  t_span_axes<T, N, index_list_t<SelectedAxes...>>::strided(Index... inds) const
  {
    constexpr index_list_t<SelectedAxes...> ax_list;

    index<K> const ind(inds...);

    ASSERT(is_bounded_strictly(index<K>(zero), ind));

    auto rv_stride = span_->stride_;
    auto rv_size   = span_->size();

    for_each_indexed(ax_list, [&](auto const num_ax, auto const ax) {
      rv_stride[ax.value] = ind[num_ax.value] * rv_stride[ax.value];

      rv_size[ax.value] = (rv_size[ax.value] - 1) / ind[num_ax.value] + 1;
    });

    return t_span<T, N>(span_->origin_, rv_size, rv_stride);
  }

  template<typename T, index_t N, index_t... SelectedAxes>
  template<typename... Index>
  requires constructs_index_v<N - sizeof...(SelectedAxes), Index...> auto
  t_span_axes<T, N, index_list_t<SelectedAxes...>>::span_along(
      Index... inds) const
  {
    constexpr index_list_t<SelectedAxes...> ax;
    constexpr auto plb_ax = plumb(c_index<N>, ax);

    index<N - K> const ind(inds...);

    return t_span<T, K>(
        span_->get_ptr(join<N>(ind, plb_ax, zero)),
        select_at(span_->size(), ax),
        select_at(span_->stride_, ax));
  }

  template<typename T, index_t N, index_t... SelectedAxes>
  template<typename... Index>
  requires constructs_index_v<sizeof...(SelectedAxes), Index...> auto
  t_span_axes<T, N, index_list_t<SelectedAxes...>>::span_plumb(
      Index... inds) const
  {
    constexpr index_list_t<SelectedAxes...> ax;
    constexpr auto plb_ax = plumb(c_index<N>, ax);

    index<K> const ind(inds...);

    return t_span<T, N - K>(
        span_->get_ptr(join<N>(ind, ax, zero)),
        select_at(span_->size(), plb_ax),
        select_at(span_->stride_, plb_ax));
  }

  template<typename T, index_t N, index_t... SelectedAxes>
  template<typename F>
  void
  t_span_axes<T, N, index_list_t<SelectedAxes...>>::for_each_along(F&& f) const
  {
    constexpr index_list_t<SelectedAxes...> ax;
    constexpr auto plb_ax = plumb(c_index<N>, ax);

    t_span<T, N - K> master(
        span_->origin_,
        select_at(span_->size(), plb_ax),
        select_at(span_->stride_, plb_ax));

    auto const size   = select_at(span_->size(), ax);
    auto const stride = select_at(span_->stride_, ax);

    for (auto& val: master) {
      f(t_span<T, K>(&val, size, stride));
    }
  }

  template<typename T, index_t N, index_t... SelectedAxes>
  template<typename F>
  void
  t_span_axes<T, N, index_list_t<SelectedAxes...>>::for_each_plumb(F&& f) const
  {
    constexpr index_list_t<SelectedAxes...> ax;
    constexpr auto plb_ax = plumb(c_index<N>, ax);

    t_span<T, K> master(
        span_->origin_,
        select_at(span_->size(), ax),
        select_at(span_->stride_, ax));

    auto const plb_size   = select_at(span_->size(), plb_ax);
    auto const plb_stride = select_at(span_->stride_, plb_ax);

    for (auto& val: master) {
      f(t_span<T, N - K>(&val, plb_size, plb_stride));
    }
  }

  /*
   * tensor-span-iterator
   */
  template<typename T, index_t N>
  class t_span_iterator : public iterator_facade<t_span_iterator<T, N>, T> {
   private:
    /*
     * The logic implemented here only works for N >= 1.
     * An explicit specialisation for N == 0 is available below.
     */
    static_assert(N >= 1);

   public:
    t_span_iterator(
        T* ptr,
        index<N> const& current,
        index<N> const& size,
        index<N> const& stride)
      : ptr_(ptr), current_(current), size_(size), stride_(stride)
    {
    }

    T* ptr() const
    {
      return ptr_;
    }

    index<N> const& pos() const
    {
      return current_;
    }

    void increment()
    {
      ptr_ +=
          detail::tensor_increment(current_, index<N>(zero), size_, stride_);
    }

    void decrement()
    {
      ptr_ +=
          detail::tensor_decrement(current_, index<N>(zero), size_, stride_);
    }

    void advance(index_t offset)
    {
      ptr_ += detail::tensor_advance(
          current_, offset, index<N>(zero), size_, stride_);
    }

    index_t distance_to(t_span_iterator const& other) const
    {
      return detail::tensor_distance_to(
          current_, other.current_, index<N>(zero), size_);
    }

    bool equal_to(t_span_iterator const& other) const
    {
      return (ptr_ == other.ptr_);
    }

    T& dereference() const
    {
      return (*ptr_);
    }

   private:
    T* ptr_;
    index<N> current_;
    index<N> size_;
    index<N> stride_;
  };

  /*
   * Partial specialisation for scalar-spans
   */
  template<typename T>
  class t_span_iterator<T, 0>
    : public iterator_facade<t_span_iterator<T, 0>, T> {
   public:
    t_span_iterator(T* const ptr, index<0>, index<0>, index<0>) : ptr_(ptr) {}

    T* ptr() const
    {
      return ptr_;
    }

    index<0> pos() const
    {
      return {};
    }

    void increment()
    {
      ++(ptr_);
    }
    void decrement()
    {
      --(ptr_);
    }
    void advance(index_t const offset)
    {
      (ptr_) += offset;
    }
    index_t distance_to(t_span_iterator const& other) const
    {
      return (ptr_ - other.ptr_);
    }

    bool equal_to(t_span_iterator const& other) const
    {
      return (ptr_ == other.ptr_);
    }

    T& dereference() const
    {
      return (*ptr_);
    }

   private:
    T* ptr_;
  };

  /*
   * Implementation of for-each-bundle
   */
  template<typename... Ts, index_t N>
  struct for_each_bundle_impl<t_span<Ts, N>...> {
    static_assert(
        sizeof...(Ts) >= 1,
        "Need at least one tensor-span for for-each-bundle");

    /**
     * Assumes that all tensor-spans have the same size and calls the
     * functor @p f for each corresponding tuple of elements.
     *
     * The order on which the elements are passed to @p f is unspecified.
     */
    template<typename F, typename FC, typename U, typename... Us>
    static void apply(F&& f, FC&& fc, U& span, Us&... spans)
    {
      if constexpr (sizeof...(Ts) != 0) {
        if constexpr (N == 0) {
          auto fef =
              wrap_for_each_functor<t_span<Ts, N>...>(std::forward<F>(f));

          fef(*span.origin_, *spans.origin_...);
        }
        else {
          ASSERT(is_all_equal(span.size(), spans.size()...));

          if (is_empty(span.size())) {
            return;
          }

          iterator_based_for_each(
              std::forward<F>(f), std::forward<FC>(fc), span, spans...);
        }
      }
    }

    template<typename F, typename FC, typename... Us>
    static void apply_indexed(F&& f, FC&& fc, Us&... spans)
    {
      if constexpr (sizeof...(Ts) != 0) {
        iterator_based_for_each_indexed(
            std::forward<F>(f), std::forward<FC>(fc), spans...);
      }
    }
  };

  /*
   * Implementation of is_aliasing
   */
  template<typename T, index_t N>
  constexpr bool is_aliasing(t_span<T, N> const& span)
  {
    if constexpr (N == 0) {
      return false;
    }
    else if constexpr (N == 1) {
      return (span.size_[0] > 1) && (span.stride_[0] == 0);
    }
    else if constexpr (N == 2) {
      if (is_empty(span.size_)) {
        return false;
      }

      auto const str = abs(span.stride_);

      if (str[0] == 0) {
        if (span.size_[0] > 1) {
          return true;
        }
        else {
          return is_aliasing(
              vector_span<T>(span.origin_, span.size_[1], str[1]));
        }
      }

      if (str[1] == 0) {
        if (span.size_[1] > 1) {
          return true;
        }
        else {
          return is_aliasing(
              vector_span<T>(span.origin_, span.size_[0], str[0]));
        }
      }

      auto const l = std::lcm(str[0], str[1]);

      return ((l / str[0]) < span.size_[0]) && ((l / str[1]) < span.size_[1]);
    }
    else {
      NOT_IMPLEMENTED();
    }
  }
}
