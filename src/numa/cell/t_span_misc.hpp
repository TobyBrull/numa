#ifndef INCLUDE_GUARD_NUMA_CELL_SPAN_MISC_H_
#define INCLUDE_GUARD_NUMA_CELL_SPAN_MISC_H_

#include "concepts.hpp"
#include "fwd.hpp"

namespace numa {
  template<Cell C>
  auto span(C& c);

  template<Cell C>
  auto span(C const& c);

  template<Cell C>
  auto span_const(C const& c);
}

#include "span_misc.inl"

#endif
