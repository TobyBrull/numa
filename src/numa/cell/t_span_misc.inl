namespace numa {
  template<Cell C>
  auto span(C& c)
  {
    return c.span();
  }

  template<Cell C>
  auto span(const C& c)
  {
    return c.span();
  }

  template<Cell C>
  auto span_const(const C& c)
  {
    return c.span_const();
  }
}
