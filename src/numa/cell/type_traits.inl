namespace numa {
  template<WeakSpan T, WeakSpan... Ts>
  constexpr auto is_same_num_dims(type_list_t<T, Ts...>)
  {
    return c_bool<((T::axes_info.num_dims == Ts::axes_info.num_dims) && ...)>;
  }

  /*
   * is-same-template
   */
  namespace detail {
    template<WeakSpan... Ts>
    struct is_same_template : public std::false_type {
    };

    template<
        template<typename, side, strictly>
        typename Template,
        typename... Ts,
        side... Side,
        strictly... Strictly>
    struct is_same_template<Template<Ts, Side, Strictly>...>
      : public std::true_type {
    };

    template<
        template<typename, IndexList, IndexList>
        typename Template,
        typename... Ts,
        IndexList... Ls,
        IndexList... Rs>
    struct is_same_template<Template<Ts, Ls, Rs>...> : public std::true_type {
    };

    template<
        template<typename, major, index_t>
        typename Template,
        typename... Ts,
        major... Majors,
        index_t... Qs>
    struct is_same_template<Template<Ts, Majors, Qs>...>
      : public std::true_type {
    };

    template<
        template<typename, index_t...>
        typename Template,
        typename... Ts,
        index_t... Sizes>
    struct is_same_template<Template<Ts, Sizes...>...> : public std::true_type {
    };

    template<
        template<typename, index_t, typename...>
        typename Template,
        typename... Ts,
        index_t N,
        typename... RestT>
    requires(
        sizeof...(RestT) >=
        1) struct is_same_template<Template<Ts, N, RestT...>...>
      : public std::true_type {
    };
  }

  template<WeakSpan... Ts>
  constexpr auto is_same_template(type_list_t<Ts...>)
  {
    return c_bool<detail::is_same_template<Ts...>::value>;
  }

  namespace detail {
    template<typename T>
    struct is_structure_impl : public c_false_t {
    };

    template<typename... Fields>
    struct is_structure_impl<structure<Fields...>> : public c_true_t {
    };
  }

  template<Span T>
  constexpr auto is_structure(c_type_t<T>)
  {
    return c_bool<detail::is_structure_impl<T>::value>;
  }
}
