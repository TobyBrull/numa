#ifndef INCLUDE_GUARD_NUMA_CELL_STRUCTURE_H_
#define INCLUDE_GUARD_NUMA_CELL_STRUCTURE_H_

#include "structure_facade.hpp"
#include "structure_unvaried.hpp"

namespace numa {
  template<typename... Fields>
  class structure
    : public structure_unvaried<Fields...>
    , public structure_vary_axes_facade<structure<Fields...>, Fields...> {
   public:
    using base = structure_unvaried<Fields...>;

    using structure_info_t = typename base::structure_info_t;

    constexpr static auto structure_info = base::structure_info;
    constexpr static auto vary_axes_info = structure_info.vary_axes_info();

    constexpr static index_t Na = structure_info.num_axes;

    template<index_t K>
    using structure_t = typename structure_info_t::template structure_t<K>;

    template<typename FieldMark>
    using structure_by_mark_t =
        typename structure_info_t::template structure_by_mark_t<FieldMark>;

    /*
     * c-tors
     */
    template<typename... Initers>
    structure(Initers const&... initers);

    /*
     * unvaried access
     */
    using unvaried_type = structure_unvaried<Fields...>;

    unvaried_type& unvaried();
    unvaried_type const& unvaried() const;
    unvaried_type const& unvaried_const() const;

    /*
     * replace
     */
    template<typename FieldMark>
    auto replace(FieldMark, structure_by_mark_t<FieldMark>&& replace) requires(
        is_mark(c_type<FieldMark>).value);

    template<typename FieldMark>
    auto replace(structure_by_mark_t<FieldMark>&& replace) requires(
        is_mark(c_type<FieldMark>).value);

    template<index_t K>
    auto replace(c_index_t<K>, structure_t<K>&& replace) requires(
        K < structure_info.num_fields);

    template<index_t K>
    auto
    replace(structure_t<K>&& replace) requires(K < structure_info.num_fields);

    /*
     * vary_axes_impl
     */
    template<index_t... Axes>
    constexpr auto vary_axes_impl(index_list_t<Axes...> = {}) requires(
        is_variation<Na>(index_list<Axes...>).value);

   private:
    template<typename, typename...>
    friend class structure_axes;
  };
}

#include "structure.inl"

#endif
