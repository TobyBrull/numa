namespace numa {
  template<side Side, typename F, typename T, typename Iter>
  class t_span_adaptor_mirror_iterator
    : public iterator_facade<
          t_span_adaptor_mirror_iterator<Side, F, T, Iter>,
          value_type_t<Iter>> {
   private:
    bool is_valid_pos()
    {
      if ((iter_ == end_) && (!mirror_)) {
        return true;
      }

      auto const p = iter_.pos();

      if (!mirror_) {
        return is_bounded_triangular<Side>(p);
      }
      else {
        return is_bounded_triangular_strictly<Side>(p);
      }
    }

   public:
    t_span_adaptor_mirror_iterator(
        Iter const& iter,
        bool const mirror,
        Iter const& begin,
        Iter const& end,
        F f)
      : iter_(iter), mirror_(mirror), begin_(begin), end_(end), f_(std::move(f))
    {
      ASSERT(is_valid_pos());
    }

    auto pos() const
    {
      if (!mirror_) {
        return iter_.pos();
      }
      else {
        return mirrored(iter_.pos());
      }
    }

    void increment()
    {
      if (!mirror_ && is_bounded_triangular_strictly<Side>(iter_.pos())) {
        mirror_ = true;
      }
      else {
        do {
          ++iter_;
        } while ((iter_ != end_) && !is_bounded_triangular<Side>(iter_.pos()));

        mirror_ = false;
      }
    }

    void decrement()
    {
      if (mirror_) {
        mirror_ = false;
      }
      else {
        do {
          --iter_;
        } while ((iter_ != begin_) &&
                 !is_bounded_triangular<Side>(iter_.pos()));

        mirror_ = is_bounded_triangular_strictly<Side>(iter_.pos());
      }
    }

    auto equal_to(t_span_adaptor_mirror_iterator const& other) const
    {
      return iter_.equal_to(other.iter_);
    }

    T dereference() const
    {
      if (!mirror_) {
        return iter_.dereference();
      }
      else {
        return f_(iter_.dereference());
      }
    }

   private:
    Iter iter_;
    bool mirror_;

    Iter begin_;
    Iter end_;

    F f_;
  };

  template<Span S, side Side, typename F>
  t_span_adaptor_mirror<S, Side, F>::t_span_adaptor_mirror(S const& span, F f)
    : span_(span), f_(std::move(f))
  {
    const auto form = S::axes_info.size_to_form(span_.size());

    THROW_IF_NOT(
        is_mirrored(form), "t_span_adaptor_mirror: needs mirrored size");
  }

  template<Span S, side Side, typename F>
  auto t_span_adaptor_mirror<S, Side, F>::size() const -> index<Na>
  {
    const auto form = S::axes_info.size_to_form(span_.size());

    const auto [l, r] = split<Na>(form);
    (void)r;

    ASSERT(l == r);

    return l;
  }

  template<Span S, side Side, typename F>
  auto
  t_span_adaptor_mirror<S, Side, F>::get_ptr_impl(index<Nd> const& ind) const
      -> element_type*
  {
    return span_.get_ptr(ind);
  }

  template<Span S, side Side, typename F>
  bool
  t_span_adaptor_mirror<S, Side, F>::is_bounded_impl(index<Nd> const& ind) const
  {
    if (is_bounded_triangular<Side>(ind)) {
      return span_.is_bounded(ind);
    }
    else {
      return span_.is_bounded(mirrored(ind));
    }
  }

  template<Span S, side Side, typename F>
  auto
  t_span_adaptor_mirror<S, Side, F>::iterator_impl(index<Nd> const& ind) const
  {
    if (is_bounded_triangular<Side>(ind)) {
      return t_span_adaptor_mirror_iterator<
          Side,
          F,
          value_type,
          inner_iterator_type>{
          span_.iterator(ind), false, span_.begin(), span_.end(), f_};
    }
    else {
      return t_span_adaptor_mirror_iterator<
          Side,
          F,
          value_type,
          inner_iterator_type>{
          span_.iterator(mirrored(ind)), true, span_.begin(), span_.end(), f_};
    }
  }

  template<Span S, side Side, typename F>
  auto t_span_adaptor_mirror<S, Side, F>::operator[](index<Nd> const& ind) const
      -> value_type
  {
    if (is_bounded_triangular<Side>(ind)) {
      return *get_ptr_impl(ind);
    }
    else {
      return f_(*get_ptr_impl(mirrored(ind)));
    }
  }

  template<Span S, side Side, typename F>
  auto t_span_adaptor_mirror<S, Side, F>::begin() const
  {
    auto it     = span_.begin();
    auto end_it = span_.end();

    while ((it != end_it) && !is_bounded_triangular<Side>(it.pos())) {
      ++it;
    }

    return t_span_adaptor_mirror_iterator<
        Side,
        F,
        value_type,
        inner_iterator_type>{
        std::move(it), false, span_.begin(), std::move(end_it), f_};
  }

  template<Span S, side Side, typename F>
  auto t_span_adaptor_mirror<S, Side, F>::end() const
  {
    return t_span_adaptor_mirror_iterator<
        Side,
        F,
        value_type,
        inner_iterator_type>{
        span_.end(), false, span_.begin(), span_.end(), f_};
  }

  template<Span S, side Side, typename F>
  class for_each_bundle_impl<t_span_adaptor_mirror<S, Side, F>>
    : public iterator_based_for_each_bundle_impl_facade {
  };

  template<Span S>
  requires(!Cell<S>) auto upper_symmetric(S const& span)
  {
    return t_span_adaptor_mirror<S, side::right, f_id_t>{span};
  }

  template<Span S>
  requires(!Cell<S>) auto lower_symmetric(S const& span)
  {
    return t_span_adaptor_mirror<S, side::left, f_id_t>{span};
  }

  template<Span S>
  requires(!Cell<S>) auto upper_hermitian(S const& span)
  {
    return t_span_adaptor_mirror<S, side::right, f_conj_t>{span};
  }

  template<Span S>
  requires(!Cell<S>) auto lower_hermitian(S const& span)
  {
    return t_span_adaptor_mirror<S, side::left, f_conj_t>{span};
  }

  template<typename F, Span S>
  requires(!Cell<S>) auto upper_mirror(S const& span, F f)
  {
    return t_span_adaptor_mirror<S, side::right, F>{span, std::move(f)};
  }

  template<typename F, Span S>
  requires(!Cell<S>) auto lower_mirror(S const& span, F f)
  {
    return t_span_adaptor_mirror<S, side::left, F>{span, std::move(f)};
  }

  /*
   * Cell wrappers.
   */
  template<Cell C>
  auto upper_symmetric(C const& cell)
  {
    return upper_symmetric(cell.span());
  }

  template<Cell C>
  auto lower_symmetric(C const& cell)
  {
    return lower_symmetric(cell.span());
  }

  template<Cell C>
  auto upper_hermitian(C const& cell)
  {
    return upper_hermitian(cell.span());
  }

  template<Cell C>
  auto lower_hermitian(C const& cell)
  {
    return lower_hermitian(cell.span());
  }

  template<typename F, Cell C>
  auto upper_mirror(C const& cell, F f)
  {
    return upper_mirror(cell.span(), std::move(f));
  }

  template<typename F, Cell C>
  auto lower_mirror(C const& cell, F f)
  {
    return lower_mirror(cell.span(), std::move(f));
  }
}
