namespace numa::unified {
  namespace detail {
    enum class resize_type { normal, hard };

    template<typename Target, typename... Ts>
    void resize_impl(
        Target& tgt,
        resize_type const rt,
        index<Target::sel_num_axes> const& new_size,
        Ts const&... values)
    {
      constexpr index_t Na = Target::sel_num_axes;

      auto const capa = tgt.capacity();

      bool const has_wrong_capacity = [&] {
        switch (rt) {
          case resize_type::normal:
            return !is_bounded(new_size, capa);

          case resize_type::hard:
            return (capa != new_size);
        }

        UNREACHABLE();
      }();

      bool const does_reallocate =
          (tgt.must_reallocate() || has_wrong_capacity);

      auto const old_size = tgt.size();

      if (does_reallocate) {
        auto const new_capa = [&] {
          switch (rt) {
            case resize_type::normal:
              return growth_strategy(capa, new_size);

            case resize_type::hard:
              return new_size;
          }

          UNREACHABLE();
        }();

        using numa::detail::translate_resize;

        tgt.translate(
            new_size,
            new_capa,
            numa::detail::make_reform_axis_array<Na, translate_resize>(
                old_size, new_size),
            values...);
      }
      else {
        if (tgt.size() != new_size) {
          using numa::detail::inplace_resize;

          tgt.inplace(
              new_size,
              numa::detail::make_reform_axis_array<Na, inplace_resize>(
                  old_size, new_size),
              values...);
        }
      }

      ASSERT(tgt.size() == new_size);
    }
  }

  template<typename Target, typename... Ts>
  void resize(
      Target&& tgt,
      index<Target::sel_num_axes> const& new_size,
      Ts const&... values) requires(!std::is_reference_v<Target>)
  {
    static_assert(ReformTarget<Target>);

    detail::resize_impl(tgt, detail::resize_type::normal, new_size, values...);
  }

  template<typename Target, typename... Ts>
  void resize_hard(
      Target&& tgt,
      index<Target::sel_num_axes> const& new_size,
      Ts const&... values) requires(!std::is_reference_v<Target>)
  {
    static_assert(ReformTarget<Target>);

    detail::resize_impl(tgt, detail::resize_type::hard, new_size, values...);
  }

  template<typename Target>
  void reserve_hard(
      Target&& tgt,
      index<Target::sel_num_axes> const&
          requested_capa) requires(!std::is_reference_v<Target>)
  {
    constexpr index_t Na = Target::sel_num_axes;

    static_assert(ReformTarget<Target>);

    auto const size = tgt.size();
    auto const capa = tgt.capacity();

    auto const new_capa = max(size, requested_capa);

    if (tgt.must_reallocate() || (new_capa != capa)) {
      using numa::detail::translate_trivial;

      tgt.translate(
          size,
          new_capa,
          numa::detail::make_reform_axis_array<Na, translate_trivial>(size));
    }
  }

  template<typename Target, typename... Ts>
  void insert_size(
      Target&& tgt,
      index<Target::sel_num_axes> const& base,
      index<Target::sel_num_axes> const& size,
      Ts const&... values) requires(!std::is_reference_v<Target>)
  {
    constexpr index_t Na = Target::sel_num_axes;

    static_assert(ReformTarget<Target>);

    auto const capa     = tgt.capacity();
    auto const old_size = tgt.size();

    auto const new_size = old_size + size;

    if (tgt.must_reallocate() || !is_bounded(new_size, capa)) {
      auto const new_capa = growth_strategy(capa, new_size);

      using numa::detail::translate_insert_size;

      tgt.translate(
          new_size,
          new_capa,
          numa::detail::make_reform_axis_array<Na, translate_insert_size>(
              old_size, base, size),
          values...);
    }
    else {
      using numa::detail::inplace_insert_size;

      tgt.inplace(
          new_size,
          numa::detail::make_reform_axis_array<Na, inplace_insert_size>(
              old_size, base, size),
          values...);
    }

    ASSERT(tgt.size() == new_size);
  }

  template<typename Target>
  void erase_size(
      Target&& tgt,
      index<Target::sel_num_axes> const& base,
      index<Target::sel_num_axes> const&
          size) requires(!std::is_reference_v<Target>)
  {
    constexpr index_t Na = Target::sel_num_axes;

    static_assert(ReformTarget<Target>);

    auto const capa     = tgt.capacity();
    auto const old_size = tgt.size();

    auto const new_size = old_size - size;

    ASSERT(is_bounded(index<Na>(zero), new_size));

    if (tgt.must_reallocate()) {
      using numa::detail::translate_erase_size;

      tgt.translate(
          new_size,
          capa,
          numa::detail::make_reform_axis_array<Na, translate_erase_size>(
              old_size, base, size));
    }
    else {
      using numa::detail::inplace_erase_size;

      tgt.inplace(
          new_size,
          numa::detail::make_reform_axis_array<Na, inplace_erase_size>(
              old_size, base, size));
    }

    ASSERT(tgt.size() == new_size);
  }
}
