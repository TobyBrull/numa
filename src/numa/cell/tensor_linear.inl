namespace numa {
  namespace detail {
    template<typename T>
    T computation_core_incr(
        T const& front, difference_type_t<T> const& incr, index_t const pos)
    {
      return front + incr * pos;
    }
  }

  /*
   * lin-range-base-span
   */
  template<typename T>
  T& vector_linear_incr_unvaried<T>::front()
  {
    return front_;
  }

  template<typename T>
  T const& vector_linear_incr_unvaried<T>::front() const
  {
    return front_;
  }

  template<typename T>
  auto vector_linear_incr_unvaried<T>::incr() -> difference_type_t<T>&
  {
    return incr_;
  }

  template<typename T>
  auto vector_linear_incr_unvaried<T>::incr() const
      -> difference_type_t<T> const&
  {
    return incr_;
  }

  template<typename T>
  T vector_linear_incr_unvaried<T>::back() const
  {
    if (is_empty(size_)) {
      return front_;
    }
    else {
      return front_ + (size_[0] - 1) * incr_;
    }
  }

  template<typename T>
  index<1> vector_linear_incr_unvaried<T>::size() const
  {
    return size_;
  }

  template<typename T>
  constexpr index<1> vector_linear_incr_unvaried<T>::capacity()
  {
    return std::numeric_limits<index_t>::max();
  }

  template<typename T>
  auto vector_linear_incr_unvaried<T>::begin() const -> iterator_const_type
  {
    return begin_const();
  }

  template<typename T>
  auto vector_linear_incr_unvaried<T>::begin_const() const
      -> iterator_const_type
  {
    return iterator_const_type(front_, incr_, 0);
  }

  template<typename T>
  auto vector_linear_incr_unvaried<T>::end() const -> iterator_const_type
  {
    return end_const();
  }

  template<typename T>
  auto vector_linear_incr_unvaried<T>::end_const() const -> iterator_const_type
  {
    return iterator_const_type(front_, incr_, this->size_[0]);
  }

  template<typename T>
  T vector_linear_incr_unvaried<T>::operator()(index<1> const& ind) const
  {
    return operator[](ind);
  }

  template<typename T>
  T vector_linear_incr_unvaried<T>::operator[](index<1> const& ind) const
  {
    return detail::computation_core_incr(front_, incr_, ind[0]);
  }

  template<typename T>
  T vector_linear_incr_unvaried<T>::at(index<1> const& ind) const
  {
    THROW_IF_NOT(
        (0 <= ind[0]) && (ind[0] < size_[0]),
        "lin-range-incr: out-of-bounds access");

    return operator[](ind);
  }

  template<typename T>
  auto vector_linear_incr_unvaried<T>::iterator(index<1> const& ind) const
      -> iterator_const_type
  {
    return const_iterator(ind);
  }

  template<typename T>
  auto vector_linear_incr_unvaried<T>::const_iterator(index<1> const& ind) const
      -> iterator_const_type
  {
    return iterator_const_type(*this, ind[0]);
  }

  template<typename T>
  vector_linear_incr_unvaried<T>::vector_linear_incr_unvaried(
      index<1> const size, T front, difference_type_t<T> incr)
    : size_(size), front_(std::move(front)), incr_(incr)
  {
  }

  /*
   * lin-range
   */
  template<typename T>
  vector_linear_incr<T>::vector_linear_incr(
      index<1> const size, T front, difference_type_t<T> incr)
    : vector_linear_incr_unvaried<T>(size, std::move(front), std::move(incr))
  {
  }

  template<typename T>
  vector_linear_incr<T>::vector_linear_incr(vector_linear_incr const& other)
    : vector_linear_incr_unvaried<T>(other.size_, other.front_, other.incr_)
  {
  }

  template<typename T>
  auto vector_linear_incr<T>::operator=(vector_linear_incr const& other)
      -> vector_linear_incr<T>&
  {
    this->size_  = other.size_;
    this->front_ = other.front_;
    this->incr_  = other.incr_;

    return (*this);
  }

  template<typename T>
  vector_linear_incr<T>::vector_linear_incr(vector_linear_incr&& other)
    : vector_linear_incr_unvaried<T>(
          other.size_, std::move(other.front_), std::move(other.incr_))
  {
  }

  template<typename T>
  auto vector_linear_incr<T>::operator=(vector_linear_incr&& other)
      -> vector_linear_incr&
  {
    this->size_  = other.size_;
    this->front_ = std::move(other.front_);
    this->incr_  = std::move(other.incr_);

    return (*this);
  }

  template<typename T>
  constexpr index_t vector_linear_incr<T>::required_elements(index<1>)
  {
    return 0;
  }

  template<typename T>
  auto vector_linear_incr<T>::unvaried() -> unvaried_type&
  {
    return (*this);
  }

  template<typename T>
  auto vector_linear_incr<T>::unvaried() const -> unvaried_type const&
  {
    return (*this);
  }

  template<typename T>
  auto vector_linear_incr<T>::unvaried_const() const -> unvaried_type const&
  {
    return (*this);
  }

  template<typename T>
  template<index_t... Axes>
  requires(is_variation<1>(index_list<Axes...>)
               .value) constexpr auto vector_linear_incr<T>::
      vary_axes_impl(index_list_t<Axes...>)
  {
    return vector_linear_incr_axes<T, index_list_t<Axes...>>{this};
  }

  template<typename T, index_t... SelAxes>
  class vector_linear_incr_axes<T, index_list_t<SelAxes...>>
    : public cell_axes_facade_light<
          vector_linear_incr_axes<T, index_list_t<SelAxes...>>,
          vector_linear_incr<T>,
          index_list_t<SelAxes...>> {
   public:
    using baseclass = cell_axes_facade_light<
        vector_linear_incr_axes<T, index_list_t<SelAxes...>>,
        vector_linear_incr<T>,
        index_list_t<SelAxes...>>;

    using baseclass::baseclass;

    using baseclass::sel_num_axes;

    using parent = typename baseclass::parent;

    auto size() const
    {
      return select_at(this->cell_->size(), index_list<SelAxes...>);
    }

    auto capacity() const
    {
      return select_at(this->cell_->capacity(), index_list<SelAxes...>);
    }

    bool must_reallocate() const
    {
      return false;
    }

    void move_assign(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        index<sel_num_axes> const& target);

    template<typename... Args>
    void fill_assign(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        Args const&... args);

    void move_construct(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        index<sel_num_axes> const& target);

    template<typename... Args>
    void fill_construct(
        index<sel_num_axes> const&, index<sel_num_axes> const&, Args const&...)
    {
      static_assert(sizeof...(Args) == 0);

      no_op;
    }

    void
    destruct(index<sel_num_axes> const& base, index<sel_num_axes> const& form);

    void set_size(index<sel_num_axes> const& new_size)
    {
      overwrite(this->cell_->size_, index_list<SelAxes...>, new_size);
    }

    translocation_forbidden_t translocation(index<sel_num_axes> const&)
    {
      return {};
    }
  };

  /*
   * lin-range-incr-iterator
   */
  template<typename T>
  class vector_linear_incr_iterator
    : public iterator_facade<vector_linear_incr_iterator<T>, T> {
   public:
    vector_linear_incr_iterator(
        T front, difference_type_t<T> incr, index_t const pos)
      : front_(front), incr_(incr), pos_(pos)
    {
    }

    index<1> pos() const
    {
      return pos_;
    }

    void increment()
    {
      ++pos_;
    }

    void decrement()
    {
      --pos_;
    }

    void advance(index_t offset)
    {
      pos_ += offset;
    }

    index_t distance_to(vector_linear_incr_iterator const& other) const
    {
      return (other.pos_ - pos_);
    }

    bool equal_to(vector_linear_incr_iterator const& other) const
    {
      return (pos_ == other.pos_);
    }

    T dereference() const
    {
      return detail::computation_core_incr(front_, incr_, pos_);
    }

   private:
    T front_;

    difference_type_t<T> incr_;

    index_t pos_;
  };

  /*
   * for-each
   */
  template<typename... Ts>
  class for_each_bundle_impl<vector_linear_incr_unvaried<Ts>...>
    : public iterator_based_for_each_bundle_impl_facade {
  };

  template<typename T>
  bool operator==(
      vector_linear_incr_unvaried<T> const& lhs,
      vector_linear_incr_unvaried<T> const& rhs)
  {
    return (lhs.size() == rhs.size()) && (lhs.front() == rhs.front()) &&
        (lhs.incr() == rhs.incr());
  }

  template<typename T>
  bool operator!=(
      vector_linear_incr_unvaried<T> const& lhs,
      vector_linear_incr_unvaried<T> const& rhs)
  {
    return !(lhs == rhs);
  }
}
