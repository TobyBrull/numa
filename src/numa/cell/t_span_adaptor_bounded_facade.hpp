#ifndef INCLUDE_GUARD_NUMA_CELL_T_SPAN_ADAPTOR_BOUNDED_FACADE_H_
#define INCLUDE_GUARD_NUMA_CELL_T_SPAN_ADAPTOR_BOUNDED_FACADE_H_

#include "fwd.hpp"

#include "for_each.hpp"
#include "misc.hpp"
#include "t_span_facade.hpp"
#include "type_traits.hpp"

namespace numa {
  template<Span S, typename Raster>
  struct t_span_adaptor_bounded_facade
    : public t_span_facade<
          t_span_adaptor_bounded_facade<S, Raster>,
          element_type_t<S>,
          S::axes_info.num_axes,
          S::axes_info.num_dims> {
   public:
    constexpr static index_t Nd     = S::axes_info.num_dims;
    constexpr static auto axes_info = axes_info_tensor<Nd>();

    using element_type = element_type_t<S>;

    S span_;
    Raster raster_;

    t_span_adaptor_bounded_facade() = default;

    t_span_adaptor_bounded_facade(S const& span, Raster const& raster = {});

    index<Nd> size() const;

    element_type* get_ptr_impl(index<Nd> const& ind) const;
    bool is_bounded_impl(index<Nd> const& ind) const;
    auto iterator_impl(index<Nd> const& ind) const;

    auto begin() const;
    auto end() const;

   private:
    using inner_iterator_type = decltype(span_.begin());
  };
}

#include "t_span_adaptor_bounded_facade.inl"

#endif
