namespace numa {
  template<typename T, typename U>
  requires(
      WeakSpan<decay_t<T>>&& WeakSpan<U>&& is_same_num_dims<decay_t<T>, U>()
          .value) void
  operator+=(T&& weak_span, U const& rhs)
  {
    for_each(std::forward<T>(weak_span), rhs)(
        [](auto& x, const auto y) { x += y; });
  }

  template<typename T, typename U>
  requires(
      WeakSpan<decay_t<T>>&& WeakSpan<U>&& is_same_num_dims<decay_t<T>, U>()
          .value) void
  operator-=(T&& weak_span, U const& rhs)
  {
    for_each(std::forward<T>(weak_span), rhs)(
        [](auto& x, const auto y) { x -= y; });
  }

  template<typename T, typename U>
  requires(WeakSpan<decay_t<T>> && !WeakSpan<U>) void
  operator*=(T&& weak_span, U const& factor)
  {
    for_each(std::forward<T>(weak_span))([&factor](auto& x) { x *= factor; });
  }

  template<typename T, typename U>
  requires(WeakSpan<decay_t<T>> && !WeakSpan<U>) void
  operator/=(T&& weak_span, U const& divisor)
  {
    for_each(std::forward<T>(weak_span))([&divisor](auto& x) { x /= divisor; });
  }

  template<WeakSpan T, typename U>
  requires(Span<decay_t<U>>&& is_same_num_dims<T, decay_t<U>>()
               .value) void copy_from(const T& from, U&& into)
  {
    for_each(from, std::forward<U>(into))(
        [](const auto& source, auto& target) { target = source; });
  }

  template<typename T, Span U>
  requires(WeakSpan<decay_t<T>>&& is_same_num_dims<decay_t<T>, U>()
               .value) void copy_into(T&& into, U const& from)
  {
    for_each(std::forward<T>(into), from)(
        [](auto& target, const auto& source) { target = source; });
  }

  template<WeakSpan T, typename U>
  requires(Span<decay_t<U>>&& is_same_num_dims<T, decay_t<U>>()
               .value) void move_from(const T& from, U&& into)
  {
    for_each(from, std::forward<U>(into))(
        [](auto& source, auto& target) { target = std::move(source); });
  }

  template<typename T, Span U>
  requires(WeakSpan<decay_t<T>>&& is_same_num_dims<decay_t<T>, U>()
               .value) void move_into(T&& into, U const& from)
  {
    for_each(std::forward<T>(into), from)(
        [](auto& target, auto& source) { target = std::move(source); });
  }

  template<typename Predicate, Span... Ts>
  requires(is_same_num_dims<Ts...>().value) bool all_of(
      const Predicate pred, Ts const&... spans)
  {
    bool retval = true;

    for_each(spans...)([&pred, &retval](auto&&... values) {
      if (!pred(std::forward<decltype(values)>(values)...)) {
        retval = false;
        return for_each_continue{false};
      }
      else {
        return for_each_continue{true};
      }
    });

    return retval;
  }

  template<typename Predicate, Span... Ts>
  requires(is_same_num_dims<Ts...>().value) bool any_of(
      const Predicate pred, Ts const&... spans)
  {
    bool retval = false;

    for_each(spans...)([&pred, &retval](auto&&... values) {
      if (pred(std::forward<decltype(values)>(values)...)) {
        retval = true;
        return for_each_continue{false};
      }
      else {
        return for_each_continue{true};
      }
    });

    return retval;
  }

  template<typename Predicate, Span... Ts>
  requires(is_same_num_dims<Ts...>().value) index_t
      count_if(const Predicate pred, Ts const&... spans)
  {
    index_t retval = 0;

    for_each(spans...)([&pred, &retval](auto&&... values) {
      if (pred(std::forward<decltype(values)>(values)...)) {
        ++retval;
      }
    });

    return retval;
  }
}
