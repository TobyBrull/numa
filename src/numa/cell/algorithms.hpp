#ifndef INCLUDE_GUARD_NUMA_CELL_ALGORITHMS_H_
#define INCLUDE_GUARD_NUMA_CELL_ALGORITHMS_H_

#include "numa/cell/for_each.hpp"

#include "numa/boot/index.hpp"

#include "type_traits.hpp"

#include <type_traits>

namespace numa {
  /**
   * Tensor addition.
   */
  template<typename T, typename U>
  requires(
      WeakSpan<decay_t<T>>&& WeakSpan<U>&& is_same_num_dims<decay_t<T>, U>()
          .value) void
  operator+=(T&& weak_span, U const& rhs);

  /**
   * Tensor subtraction.
   */
  template<typename T, typename U>
  requires(
      WeakSpan<decay_t<T>>&& WeakSpan<U>&& is_same_num_dims<decay_t<T>, U>()
          .value) void
  operator-=(T&& weak_span, U const& rhs);

  /**
   * Scalar multiplication.
   */
  template<typename T, typename U>
  requires(WeakSpan<decay_t<T>> && !WeakSpan<U>) void
  operator*=(T&& weak_span, U const& factor);

  /**
   * Scalar division.
   */
  template<typename T, typename U>
  requires(WeakSpan<decay_t<T>> && !WeakSpan<U>) void
  operator/=(T&& weak_span, U const& divisor);

  /**
   * Copy span.
   */
  template<WeakSpan T, typename U>
  requires(Span<decay_t<U>>&& is_same_num_dims<T, decay_t<U>>()
               .value) void copy_from(T const& from, U&& into);

  template<typename T, Span U>
  requires(WeakSpan<decay_t<T>>&& is_same_num_dims<decay_t<T>, U>()
               .value) void copy_into(T&& into, U const& from);

  /**
   * Move span.
   */
  template<WeakSpan T, typename U>
  requires(Span<decay_t<U>>&& is_same_num_dims<T, decay_t<U>>()
               .value) void move_from(T const& from, U&& into);

  template<typename T, Span U>
  requires(WeakSpan<decay_t<T>>&& is_same_num_dims<decay_t<T>, U>()
               .value) void move_into(T&& into, U const& from);

  /**
   *
   */
  template<typename Predicate, Span... Ts>
  requires(is_same_num_dims<Ts...>().value) bool all_of(
      const Predicate pred, Ts const&... spans);

  template<typename Predicate, Span... Ts>
  requires(is_same_num_dims<Ts...>().value) bool any_of(
      const Predicate pred, Ts const&... spans);

  template<typename Predicate, Span... Ts>
  requires(is_same_num_dims<Ts...>().value) index_t
      count_if(const Predicate pred, Ts const&... spans);
}

#include "algorithms.inl"

#endif
