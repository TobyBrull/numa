#ifndef INCLUDE_GUARD_NUMA_CELL_SERIALIZE_JSON_H_
#define INCLUDE_GUARD_NUMA_CELL_SERIALIZE_JSON_H_

#include "concepts.hpp"
#include "make_cell.hpp"
#include "structure.hpp"
#include "tensor_sparse.hpp"

#include "numa/core/serialize_json.hpp"

namespace numa {
  /*
   * Generic span
   */
  template<Span T>
  void to_json(std::ostream& os, T const& span, int indent = 0);

  template<Cell T>
  requires(!Span<T>) void to_json(
      std::ostream& os, T const& cell, int indent = 0);

  template<typename T>
  requires Span<decay_t<T>> void from_json(std::istream& is, T&& span);

  template<Cell T>
  void from_json(std::ostream& is, T& cell);

  template<Cell T>
  T make_from_json_impl(c_type_t<T>, std::istream& is);

  /*
   * Specialization for sparse-tensor
   */
  template<typename T, index_t N, typename X, Allocator Alloc>
  void to_json(
      std::ostream& os,
      tensor_sparse_unvaried<T, N, X, Alloc> const& st,
      int indent = 0);

  template<typename T, index_t N, typename X, Allocator Alloc>
  void from_json(std::istream& is, tensor_sparse_unvaried<T, N, X, Alloc>& st);

  template<typename T, index_t N, typename X, Allocator Alloc>
  tensor_sparse<T, N, X, Alloc> make_from_json_impl(
      c_type_t<tensor_sparse<T, N, X, Alloc>>, std::istream& is);

  /*
   * Specialization for lin-range
   */
  template<typename T>
  void to_json(
      std::ostream& os,
      vector_linear_incr_unvaried<T> const& lr,
      int indent = 0);

  template<typename T>
  void from_json(std::istream& is, vector_linear_incr_unvaried<T>& lr);

  template<typename T>
  vector_linear_incr<T>
  make_from_json_impl(c_type_t<vector_linear_incr<T>>, std::istream& is);

  /*
   * Specialization for structure
   */
  template<typename... Fields>
  void to_json(
      std::ostream& os,
      structure_unvaried<Fields...> const& strct,
      int indent = 0);

  template<typename... Fields>
  void from_json(std::istream& is, structure_unvaried<Fields...>& strct);

  template<typename... Fields>
  structure<Fields...>
  make_from_json_impl(c_type_t<structure<Fields...>>, std::istream& is);
}

#include "serialize_json.inl"

#endif
