#ifndef INCLUDE_GUARD_NUMA_CELL_GROWTH_STRATEGY_H_
#define INCLUDE_GUARD_NUMA_CELL_GROWTH_STRATEGY_H_

#include "numa/boot/index.hpp"

namespace numa {
  constexpr inline index_t growth_strategy(index_t capa, index_t new_size);

  template<index_t N>
  index<N> growth_strategy(index<N> capa, index<N> new_size);
}

#include "growth_strategy.inl"

#endif
