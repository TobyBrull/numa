namespace numa {
  template<typename... Fields>
  constexpr index_t structure_unvaried<Fields...>::required_bytes(
      index<structure_info.num_axes> const& size)
  {
    index_t retval = 0;

    structure_info.for_each_field(
        [&](auto const field_ind,
            auto const,
            auto const,
            auto const global_axes) {
          using FieldType =
              decltype(numa::get<field_ind.value>(type_list_t<Fields...>{}));

          using S = typename FieldType::structure_t;

          auto const strct_size = select_at(size, global_axes);

          if constexpr (is_structure<S>()) {
            retval += S::required_bytes(strct_size);
          }
          else {
            static_assert(
                Cell<S>,
                "all substructures must be structures or cells "
                "for structure_unvaried::required_bytes");

            retval += S::required_elements(strct_size) *
                sizeof(typename S::value_type);
          }
        },
        index_list_iota<structure_info.num_axes>());

    return retval;
  }

  template<typename... Fields>
  constexpr index_t structure_unvaried<Fields...>::required_elements(
      index<structure_info.num_axes> const& size)
  {
    static_assert(
        !std::is_same_v<value_type, void>,
        "structure_unvaried::required_elements: may only be called on "
        "structure "
        "in which all sub-value-types are the same");

    index_t retval = 0;

    structure_info.for_each_field(
        [&](auto const field_ind,
            auto const,
            auto const,
            auto const global_axes) {
          using FieldType =
              decltype(numa::get<field_ind.value>(type_list_t<Fields...>{}));

          using S = typename FieldType::structure_t;

          auto const strct_size = select_at(size, global_axes);

          retval += S::required_elements(strct_size);
        },
        index_list_iota<structure_info.num_axes>());

    return retval;
  }

  namespace detail {
    template<typename Field, typename... Initers>
    auto make_structure_member(c_type_t<Field>, Initers const&... initers)
    {
      using structure_t = typename Field::structure_t;

      return recursive_initializer_for<Field>(
          [&](auto const&... initers_or_args) {
            if constexpr (std::is_constructible_v<
                              structure_t,
                              decltype(initers_or_args)...>) {
              return structure_t(initers_or_args...);
            }
            else {
              return structure_t();
            }
          },
          initers...);
    }
  }

  template<typename... Fields>
  template<typename... Initers>
  structure_unvaried<Fields...>::structure_unvaried(Initers const&... initers)
    : data_{detail::make_structure_member(c_type_t<Fields>{}, initers...)...}
  {
  }

  /*
   *
   */
  template<typename... Fields>
  template<typename AxisMark>
  requires(is_mark(c_type<AxisMark>).value) index_t
      structure_unvaried<Fields...>::size(AxisMark axis_mark) const
  {
    constexpr auto list = structure_info.find_axes(axis_mark);

    static_assert(!is_empty(list), "structure_unvaried: no such axis");

    constexpr auto tt = numa::get<0>(list);

    constexpr index_t field_ind = std::get<0>(tt).value;
    constexpr index_t ax_ind    = std::get<1>(tt).value;

    return std::get<field_ind>(data_).size()[ax_ind];
  }

  template<typename... Fields>
  template<index_t I>
  index_t structure_unvaried<Fields...>::size(c_index_t<I>) const
      requires(I < structure_info.num_axes)
  {
    return size(numa::get<I>(structure_info.axis_marks));
  }

  template<typename... Fields>
  auto structure_unvaried<Fields...>::size() const
      -> index<structure_info.num_axes>
  {
    index<structure_info.num_axes> retval = zero;

    for_each_indexed(
        structure_info.axis_marks, [&](auto const i, auto const axis_mark) {
          retval[i.value] = this->size(axis_mark);
        });

    return retval;
  }

  /*
   *
   */
  template<typename... Fields>
  template<typename AxisMark>
  requires(is_mark(c_type<AxisMark>).value) index_t
      structure_unvaried<Fields...>::capacity(AxisMark axis_mark) const
  {
    return capacity<find(structure_info.axis_marks, AxisMark{}).value>();
  }

  template<typename... Fields>
  template<index_t I>
  index_t structure_unvaried<Fields...>::capacity(c_index_t<I>) const
      requires(I < structure_info.num_axes)
  {
    index_t retval = -1;

    structure_info.for_each_field(
        [&](auto const field_ind,
            auto const,
            auto const local_axes,
            auto const) {
          auto const capa = select_at(
              std::get<field_ind.value>(data_).capacity(), local_axes);

          if (retval == -1) {
            retval = min(capa);
          }
          else {
            retval = std::min(retval, min(capa));
          }
        },
        index_list<I>);

    return retval;
  }

  template<typename... Fields>
  auto structure_unvaried<Fields...>::capacity() const
      -> index<structure_info.num_axes>
  {
    index<structure_info.num_axes> retval = zero;

    for_each_indexed(
        structure_info.axis_marks, [&](auto const i, auto const axis_mark) {
          retval[i.value] = this->capacity(axis_mark);
        });

    return retval;
  }

  /*
   * get
   */
  template<typename... Fields>
  template<typename FieldMark>
  requires(is_mark(c_type<FieldMark>).value) decltype(auto)
      structure_unvaried<Fields...>::get(FieldMark)
  {
    return get<find(structure_info.field_marks, FieldMark{}).value>();
  }

  template<typename... Fields>
  template<index_t K>
  decltype(auto) structure_unvaried<Fields...>::get(c_index_t<K>) requires(
      K < structure_info.num_fields)
  {
    return std::get<K>(data_).unvaried();
  }

  template<typename... Fields>
  template<typename FieldMark>
  requires(is_mark(c_type<FieldMark>).value) decltype(auto)
      structure_unvaried<Fields...>::get(FieldMark) const
  {
    return get<find(structure_info.field_marks, FieldMark{}).value>();
  }

  template<typename... Fields>
  template<index_t K>
  decltype(auto) structure_unvaried<Fields...>::get(c_index_t<K>) const
      requires(K < structure_info.num_fields)
  {
    return std::get<K>(data_).unvaried();
  }

  template<typename... Fields>
  template<typename FieldMark>
  requires(is_mark(c_type<FieldMark>).value) decltype(auto)
      structure_unvaried<Fields...>::const_get(FieldMark) const
  {
    return get<find(structure_info.field_marks, FieldMark{}).value>();
  }

  template<typename... Fields>
  template<index_t K>
  decltype(auto) structure_unvaried<Fields...>::const_get(c_index_t<K>) const
      requires(K < structure_info.num_fields)
  {
    return std::get<K>(data_).unvaried();
  }

  /*
   *
   */
  template<typename... Fields>
  template<typename FieldMark>
  requires(is_mark(c_type<FieldMark>).value) decltype(auto)
      structure_unvaried<Fields...>::const_cell(FieldMark) const
  {
    constexpr index_t K = find(structure_info.field_marks, FieldMark{}).value;

    return std::get<K>(data_);
  }

  template<typename... Fields>
  template<index_t K>
  decltype(auto) structure_unvaried<Fields...>::const_cell(c_index_t<K>) const
      requires(K < structure_info.num_fields)
  {
    return std::get<K>(data_);
  }
}

#include "algorithms.hpp"

namespace numa {
  template<typename... Fields>
  bool operator==(
      structure_unvaried<Fields...> const& lhs,
      structure_unvaried<Fields...> const& rhs)
  {
    if (lhs.size() != rhs.size()) {
      return false;
    }

    constexpr auto structure_info = structure<Fields...>::structure_info;

    bool retval = true;

    for_each(index_list_iota<structure_info.num_fields>(), [&](auto const ax) {
      if (lhs.get(ax) != rhs.get(ax)) {
        retval = false;
      }
    });

    return retval;
  }

  template<typename... Fields>
  bool operator!=(
      structure_unvaried<Fields...> const& lhs,
      structure_unvaried<Fields...> const& rhs)
  {
    return !(lhs == rhs);
  }
}
