#ifndef INCLUDE_GUARD_NUMA_CELL_STRUCTURE_UNVARIED_H_
#define INCLUDE_GUARD_NUMA_CELL_STRUCTURE_UNVARIED_H_

#include "concepts.hpp"
#include "fwd.hpp"
#include "structure_info.hpp"

#include "numa/cell/reform_detail.hpp"
#include "numa/cell/type_traits.hpp"

#include "numa/core/memory.hpp"
#include "numa/core/uninitialized.hpp"

#include "numa/tmpl/integer_list.hpp"

namespace numa {
  template<typename... Fields>
  class structure_unvaried {
   public:
    using structure_info_t = structure_info_type<type_list_t<Fields...>>;

    constexpr static structure_info_t structure_info = {};

    constexpr static index_t
    required_bytes(index<structure_info.num_axes> const& size);

    constexpr static index_t
    required_elements(index<structure_info.num_axes> const& size);

    using value_type = typename structure_info_t::value_type;

    /*
     * size
     */
    index<structure_info.num_axes> size() const;

    template<typename AxisMark>
    requires(is_mark(c_type<AxisMark>).value) index_t size(AxisMark = {}) const;

    template<index_t I>
    index_t size(c_index_t<I> = {}) const requires(I < structure_info.num_axes);

    /*
     * capacity
     */
    index<structure_info.num_axes> capacity() const;

    template<typename AxisMark>
    requires(is_mark(c_type<AxisMark>).value) index_t
        capacity(AxisMark = {}) const;

    template<index_t I>
    index_t capacity(c_index_t<I> = {}) const
        requires(I < structure_info.num_axes);

    /*
     * get (cell-span)
     */
    template<typename FieldMark>
    requires(is_mark(c_type<FieldMark>).value) decltype(auto)
        get(FieldMark = {});

    template<index_t K>
    decltype(auto)
        get(c_index_t<K> = {}) requires(K < structure_info.num_fields);

    template<typename FieldMark>
    requires(is_mark(c_type<FieldMark>).value) decltype(auto)
        get(FieldMark = {}) const;

    template<index_t K>
    decltype(auto) get(c_index_t<K> = {}) const
        requires(K < structure_info.num_fields);

    template<typename FieldMark>
    requires(is_mark(c_type<FieldMark>).value) decltype(auto)
        const_get(FieldMark = {}) const;

    template<index_t K>
    decltype(auto) const_get(c_index_t<K> = {}) const
        requires(K < structure_info.num_fields);

    /*
     * const-cell
     */
    template<typename FieldMark>
    requires(is_mark(c_type<FieldMark>).value) decltype(auto)
        const_cell(FieldMark = {}) const;

    template<index_t K>
    decltype(auto) const_cell(c_index_t<K> = {}) const
        requires(K < structure_info.num_fields);

   protected:
    template<typename... Initers>
    structure_unvaried(Initers const&... initers);

    std::tuple<typename Fields::structure_t...> data_;
  };
}

namespace numa {
  /*
   * TODO: should be part of algorithms.hpp ????
   */
  template<typename... Fields>
  bool operator==(
      structure_unvaried<Fields...> const& lhs,
      structure_unvaried<Fields...> const& rhs);

  template<typename... Fields>
  bool operator!=(
      structure_unvaried<Fields...> const& lhs,
      structure_unvaried<Fields...> const& rhs);
}

#include "structure_unvaried.inl"

#endif
