namespace numa {
  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  constexpr t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      t_span_staged(
          T* const origin, index<Na> const& size, index<Nd> const& stride)
    : origin(origin), size_(size), stride_(stride)
  {
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto
  t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::size() const
      -> index<Na> const&
  {
    return size_;
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  T*
  t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::get_ptr_impl(
      index<Nd> const& ind) const
  {
    return origin + dot(ind, stride_);
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  bool t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      is_bounded_impl(index<Nd> const& ind) const
  {
    return is_bounded_staged<Nd, side::right>(ls_t{}, rs_t{}, ind, size_);
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto
  t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::iterator_impl(
      index<Nd> const& ind) const
  {
    auto const [l_stride, r_stride] = split<Na>(stride_);
    auto const [l, r]               = split<Na>(ind);
    auto const diag                 = (l - r);

    return tensor_staged_iterator<T, Nd, ls_t, rs_t>(
        diag, r, l_stride, l_stride + r_stride, size_, get_ptr_impl(ind));
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto
  t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::begin() const
  {
    auto const [l_stride, r_stride] = split<Na>(stride_);
    T* ptr = get_ptr_impl(join(index((-Rs)...), index<Na>(zero)));

    return tensor_staged_iterator<T, Nd, ls_t, rs_t>(
        index((-Rs)...),
        index<Na>(zero),
        l_stride,
        l_stride + r_stride,
        size_,
        ptr);
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto
  t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::end() const
  {
    if (is_empty(size_)) {
      return begin();
    }
    else {
      index<Na> const l_pos((-Rs)...);
      index<Na> const r_pos = detail::end_index(index<Na>(zero), size_);

      auto const [l_stride, r_stride] = split<Na>(stride_);

      T* ptr =
          get_ptr_impl(join(size_ + index(Ls...), size_) - index<Nd>(one)) + 1;

      return tensor_staged_iterator<T, Nd, ls_t, rs_t>(
          l_pos, r_pos, l_stride, l_stride + r_stride, size_, ptr);
    }
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  template<index_t... Axes>
  auto
      t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::axes_impl(
          index_list_t<Axes...>) const
      requires(is_variation<Na>(index_list<Axes...>).value)
  {
    return t_span_staged_axes<T, Nd, ls_t, rs_t, index_list_t<Axes...>>{this};
  }

  /*
   * Implementation of band-tensor-span-iterator.
   */
  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  class tensor_staged_iterator<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>
    : public iterator_facade<
          tensor_staged_iterator<
              T,
              Nd,
              index_list_t<Ls...>,
              index_list_t<Rs...>>,
          T> {
   public:
    using ls_t = index_list_t<Ls...>;
    using rs_t = index_list_t<Rs...>;

    static_assert(is_valid_band_parameters<Nd>(ls_t{}, rs_t{}));

    constexpr static index_t Na = Nd / 2;

    tensor_staged_iterator(
        index<Na> const& diag_current,
        index<Na> const& r_current,
        index<Na> const& l_stride,
        index<Na> const& r_stride,
        index<Na> const& size,
        T* const ptr)
      : diag_current_(diag_current)
      , r_current_(r_current)
      , l_stride_(l_stride)
      , r_stride_(r_stride)
      , size_(size)
      , ptr_(ptr)
    {
    }

    index<Nd> pos() const
    {
      return join(diag_current_ + r_current_, r_current_);
    }

    void increment()
    {
      static_assert(Nd >= 1);

      index_t ptr_offset =
          detail::tensor_increment(diag_current_, l_base, l_till, l_stride_);

      if (detail::is_end_index(diag_current_, l_base, l_till)) {
        diag_current_ = l_base;

        ptr_offset = detail::tensor_increment(
            r_current_, index<Na>(zero), size_, r_stride_);

        if (!detail::is_end_index(r_current_, index<Na>(zero), size_)) {
          ptr_offset += dot(index((-Rs - Ls)...), l_stride_);
        }
        else {
          ptr_offset = 1;
        }
      }

      ptr_ += ptr_offset;
    }

    void decrement()
    {
      if (detail::is_end_index(r_current_, index<Na>(zero), size_)) {
        diag_current_ = index(Ls...);
        r_current_    = (size_ - index<Na>(one));
        ptr_ -= 1;
      }
      else if (diag_current_ == l_base) {
        index_t ptr_offset = detail::tensor_decrement(
            r_current_, index<Na>(zero), size_, r_stride_);

        ptr_offset += dot(index((Rs + Ls)...), l_stride_);

        ptr_ += ptr_offset;
        diag_current_ = index(Ls...);
      }
      else {
        ptr_ +=
            detail::tensor_decrement(diag_current_, l_base, l_till, l_stride_);
      }
    }

    void advance(index_t offset)
    {
      if ((offset < 0) &&
          detail::is_end_index(r_current_, index<Na>(zero), size_)) {
        ++offset;
        decrement();
      }

      index_t l_jumps       = (offset % r_step);
      index_t const r_jumps = (offset / r_step);

      ptr_ += detail::tensor_advance(
          r_current_, r_jumps, index<Na>(zero), size_, r_stride_);

      while (l_jumps > 0) {
        increment();
        --l_jumps;
      }

      while (l_jumps < 0) {
        decrement();
        ++l_jumps;
      }
    }

    index_t distance_to(tensor_staged_iterator const& other) const
    {
      return detail::tensor_distance_to(
                 this->diag_current_, other.diag_current_, l_base, l_till) +
          r_step *
          detail::tensor_distance_to(
              this->r_current_, other.r_current_, index<Na>(zero), size_);
    }

    bool equal_to(tensor_staged_iterator const& other) const
    {
      return (ptr_ == other.ptr_);
    }

    T& dereference() const
    {
      return (*ptr_);
    }

   private:
    constexpr static index<Na> l_till = index<Na>((Ls + 1)...);
    constexpr static index<Na> l_base = index<Na>((-Rs)...);
    constexpr static index_t r_step   = ((Rs + Ls + 1) * ...);

    index<Na> diag_current_;
    index<Na> r_current_;

    index<Na> l_stride_;
    index<Na> r_stride_;
    index<Na> size_;

    T* ptr_;
  };

  /*
   * for-each
   */
  template<typename... Ts, index_t Nd, index_t... Ls, index_t... Rs>
  class for_each_bundle_impl<
      t_span_staged<Ts, Nd, index_list_t<Ls...>, index_list_t<Rs...>>...>
    : public iterator_based_for_each_bundle_impl_facade {
  };

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs, typename U>
  requires((c_type<decay_t<T>> == c_type<decay_t<U>>).value) bool operator==(
      t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>> const& lhs,
      t_span_staged<U, Nd, index_list_t<Ls...>, index_list_t<Rs...>> const& rhs)
  {
    if (lhs.size() != rhs.size()) {
      return false;
    }

    return all_of(f_is_equal, lhs, rhs);
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs, typename U>
  requires((c_type<decay_t<T>> == c_type<decay_t<U>>).value) bool operator!=(
      t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>> const& lhs,
      t_span_staged<U, Nd, index_list_t<Ls...>, index_list_t<Rs...>> const& rhs)
  {
    return !(lhs == rhs);
  }

  /*
   * Implementation of band-tensor-span-axes
   */
  template<
      typename T,
      index_t Nd,
      index_t... Ls,
      index_t... Rs,
      index_t... SelectedAxes>
  t_span_staged_axes<
      T,
      Nd,
      index_list_t<Ls...>,
      index_list_t<Rs...>,
      index_list_t<SelectedAxes...>>::t_span_staged_axes(span_type const* span)
    : span_(span)
  {
  }

  template<
      typename T,
      index_t Nd,
      index_t... Ls,
      index_t... Rs,
      index_t... SelectedAxes>
  template<SubMode SM>
  auto t_span_staged_axes<
      T,
      Nd,
      index_list_t<Ls...>,
      index_list_t<Rs...>,
      index_list_t<SelectedAxes...>>::
      sub(SM, index<Ka> const& from, index<Ka> const& size_or_till) const
  {
    return span_type{
        span_->get_ptr_impl(join(from, from)),
        sub_mode_convert(SM{}, from, size_or_till, as_size),
        span_->stride_};
  }
}
