namespace numa {
  /*
   * for_each_bundle
   */

  namespace detail {
    template<typename T>
    constexpr auto map_bundle_type_to_impl(c_type_t<T> = {})
    {
      using decayed_T = decay_t<T>;

      if constexpr (WeakCell<decayed_T>) {
        return c_type<decay_t<typename decayed_T::unvaried_type>>;
      }
      else {
        return c_type<decayed_T>;
      }
    }

    template<typename T>
    using map_bundle_type_to_impl_t =
        typename decltype(map_bundle_type_to_impl<T>())::type;
  }

  template<typename... Ts>
  template<typename F, typename FC>
  void for_each_bundle<Ts...>::operator()(F&& f, FC&& fc) const
  {
    std::apply(
        [&](auto&... spans) {
          for_each_bundle_impl<detail::map_bundle_type_to_impl_t<Ts>...>::apply(
              std::forward<F>(f), std::forward<FC>(fc), spans...);
        },
        spans_tuple_);
  }

  template<typename... Ts>
  template<typename F, typename FC>
  void for_each_bundle<Ts...>::indexed(F&& f, FC&& fc) const
  {
    std::apply(
        [&](auto&... spans) {
          for_each_bundle_impl<detail::map_bundle_type_to_impl_t<Ts>...>::
              apply_indexed(std::forward<F>(f), std::forward<FC>(fc), spans...);
        },
        spans_tuple_);
  }

  /*
   * for_each
   */

  template<typename... Ts>
  requires(WeakSpan<decay_t<Ts>>&&...)
      for_each_bundle<Ts...> for_each(Ts&&... weak_spans)
  {
    return for_each_bundle<Ts...>{{std::forward<Ts>(weak_spans)...}};
  }

  /*
   * iterator_based_for_each...
   */

  template<typename F, typename FC, typename... Ts>
  requires GenericForEach<decay_t<Ts>...> void
  iterator_based_for_each(F&& f, FC&& fc, Ts&... spans)
  {
    iterator_based_for_each_indexed(
        prepend_dummy_parameter(std::forward<F>(f)),
        prepend_dummy_parameter(std::forward<FC>(fc)),
        spans...);
  }

  template<typename F, typename FC, typename... Ts>
  requires GenericForEach<decay_t<Ts>...> void
  iterator_based_for_each_indexed(F&& f, FC&& fc, Ts&... spans)
  {
    auto fef = wrap_for_each_functor_indexed<Ts...>(std::forward<F>(f));

    constexpr index_t Nd =
        std::tuple_element_t<0, std::tuple<Ts...>>::axes_info.num_dims;

    if constexpr (Nd == 0) {
      fef(index<0>(), *spans.origin_...);
    }
    else {
      ASSERT(is_all_equal(spans.size()...));

      std::tuple iters(spans.begin()...);

      try {
        std::tuple const ends(spans.end()...);

        while (std::get<0>(iters) != std::get<0>(ends)) {
          auto const do_cont = std::apply(
              [&fef](auto& it, auto&... its) {
                auto const retval = fef(it.pos(), *it, *its...);

                ++it;
                (++its, ...);

                return retval;
              },
              iters);

          if (!do_cont.continue_) {
            break;
          }
        }
      }
      catch (...) {
        std::tuple jters(spans.begin()...);

        while (std::get<0>(jters) != std::get<0>(iters)) {
          std::apply(
              [&fc](auto& jt, auto&... jts) {
                fc(jt.pos(), *jt, *jts...);

                ++jt;
                (++jts, ...);
              },
              jters);
        }

        throw;
      }
    }
  }

  /*
   * generic_for_each...
   */

  template<typename F, typename FC, typename... Ts>
  requires GenericForEach<decay_t<Ts>...> void
  generic_for_each(F&& f, FC&& fc, Ts&... spans)
  {
    generic_for_each_indexed(
        prepend_dummy_parameter(std::forward<F>(f)),
        prepend_dummy_parameter(std::forward<FC>(fc)),
        spans...);
  }

  template<typename F, typename FC, typename T, typename... Ts>
  requires GenericForEach<decay_t<T>, decay_t<Ts>...> void
  generic_for_each_indexed(F&& f, FC&& fc, T& span, Ts&... spans)
  {
    if constexpr (sizeof...(Ts) == 0) {
      iterator_based_for_each_indexed(
          std::forward<F>(f), std::forward<FC>(fc), span, spans...);
    }
    else {
#ifdef NUMA_ENABLE_ASSERTS
      auto const form = T::axes_info.size_to_form(span.size());
#endif
      ASSERT(((form == Ts::axes_info.size_to_form(spans.size())) && ...));

      auto const wrap = [&](auto func) {
        return [func = std::move(func),
                &spans...](auto const ind, auto&& value) mutable {
          return func(ind, std::forward<decltype(value)>(value), spans[ind]...);
        };
      };

      for_each(span).indexed(
          wrap(std::forward<F>(f)), wrap(std::forward<FC>(fc)));
    }
  }

  /*
   * facades
   */

  template<typename F, typename FC, typename... Us>
  void generic_for_each_bundle_impl_facade::apply(F&& f, FC&& fc, Us&... spans)
  {
    generic_for_each(std::forward<F>(f), std::forward<FC>(fc), spans...);
  }

  template<typename F, typename FC, typename... Us>
  void generic_for_each_bundle_impl_facade::apply_indexed(
      F&& f, FC&& fc, Us&... spans)
  {
    generic_for_each_indexed(
        std::forward<F>(f), std::forward<FC>(fc), spans...);
  }

  template<typename F, typename FC, typename... Us>
  void iterator_based_for_each_bundle_impl_facade::apply(
      F&& f, FC&& fc, Us&... spans)
  {
    iterator_based_for_each(std::forward<F>(f), std::forward<FC>(fc), spans...);
  }

  template<typename F, typename FC, typename... Us>
  void iterator_based_for_each_bundle_impl_facade::apply_indexed(
      F&& f, FC&& fc, Us&... spans)
  {
    iterator_based_for_each_indexed(
        std::forward<F>(f), std::forward<FC>(fc), spans...);
  }

  template<typename F, typename FC, typename... Us>
  void
  span_based_for_each_bundle_impl_facade::apply(F&& f, FC&& fc, Us&... spans)
  {
    for_each(spans.span()...)(std::forward<F>(f), std::forward<FC>(fc));
  }

  template<typename F, typename FC, typename... Us>
  void span_based_for_each_bundle_impl_facade::apply_indexed(
      F&& f, FC&& fc, Us&... spans)
  {
    for_each(spans.span()...).indexed(std::forward<F>(f), std::forward<FC>(fc));
  }

  /*
   * prepend_dummy_parameter
   */

  template<typename F>
  auto prepend_dummy_parameter(F&& f)
  {
    return [f = std::tuple<F>(std::forward<F>(f))](
               auto const&, auto&&... params) mutable {
      return std::get<0>(f)(std::forward<decltype(params)>(params)...);
    };
  }

  namespace detail {
    template<typename F>
    auto wrap_for_each_continue_true(F&& f)
    {
      return [f = std::tuple<F>(std::forward<F>(f))](auto&&... values) mutable {
        std::get<0>(f)(std::forward<decltype(values)>(values)...);
        return for_each_continue{true};
      };
    }
  }

  template<WeakSpan... Ts, typename F>
  auto wrap_for_each_functor(F&& f)
  {
    using ResultType = std::invoke_result_t<F, typename Ts::element_type&...>;

    if constexpr (std::is_same_v<ResultType, void>) {
      return detail::wrap_for_each_continue_true(std::forward<F>(f));
    }
    else if constexpr (std::is_same_v<ResultType, for_each_continue>) {
      return std::forward<F>(f);
    }
    else {
      return [f = std::tuple<F>(std::forward<F>(f))](
                 auto& value, auto&&... values) mutable {
        value =
            std::get<0>(f)(value, std::forward<decltype(values)>(values)...);

        return for_each_continue{true};
      };
    }
  }

  template<WeakSpan T, WeakSpan... Ts, typename F>
  auto wrap_for_each_functor_indexed(F&& f)
  {
    constexpr index_t Nd = T::axes_info.num_dims;

    using ResultType = std::invoke_result_t<
        F,
        index<Nd>&,
        typename T::element_type&,
        typename Ts::element_type&...>;

    if constexpr (std::is_same_v<ResultType, void>) {
      return detail::wrap_for_each_continue_true(std::forward<F>(f));
    }
    else if constexpr (std::is_same_v<ResultType, for_each_continue>) {
      return std::forward<F>(f);
    }
    else {
      return [f = std::forward<F>(f)](
                 auto&& ind, auto& value, auto&&... values) mutable {
        value = f(ind, value, values...);

        return for_each_continue{true};
      };
    }
  }
}
