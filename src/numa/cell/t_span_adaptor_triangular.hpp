#ifndef INCLUDE_GUARD_NUMA_CELL_T_SPAN_ADAPTOR_TRIANGULAR_H_
#define INCLUDE_GUARD_NUMA_CELL_T_SPAN_ADAPTOR_TRIANGULAR_H_

#include "t_span_adaptor_bounded_facade.hpp"

namespace numa {
  template<Span S, side Side, strictly Strict>
  struct t_span_adaptor_triangular
    : public t_span_adaptor_bounded_facade<
          S,
          raster_triangular<Side, Strict, S::axes_info.num_dims>> {
   public:
    static_assert(
        S::axes_info.num_dims % 2 == 0,
        "t_span_adaptor_triangular: "
        "needs span with even number of dimensions");

    using base = t_span_adaptor_bounded_facade<
        S,
        raster_triangular<Side, Strict, S::axes_info.num_dims>>;

    using base::base;
  };

  /*
   * For spans.
   */
  template<Span S>
  auto upper(S const& span);

  template<Span S>
  auto lower(S const& span);

  template<Span S>
  auto upper_strictly(S const& span);

  template<Span S>
  auto lower_strictly(S const& span);

  /*
   * For const cells.
   */
  template<Cell C>
  auto upper(C const& cell);

  template<Cell C>
  auto lower(C const& cell);

  template<Cell C>
  auto upper_strictly(C const& cell);

  template<Cell C>
  auto lower_strictly(C const& cell);

  /*
   * For mutable cells.
   */
  template<Cell C>
  auto upper(C& cell);

  template<Cell C>
  auto lower(C& cell);

  template<Cell C>
  auto upper_strictly(C& cell);

  template<Cell C>
  auto lower_strictly(C& cell);
}

#include "t_span_adaptor_triangular.inl"

#endif
