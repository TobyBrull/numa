#ifndef INCLUDE_GUARD_NUMA_CELL_FOR_EACH_H_
#define INCLUDE_GUARD_NUMA_CELL_FOR_EACH_H_

#include "concepts.hpp"
#include "fwd.hpp"
#include "type_traits.hpp"

#include "numa/tmpl/type_list.hpp"

namespace numa {
  template<typename... Ts>
  struct for_each_bundle {
    std::tuple<Ts...> spans_tuple_;

    template<typename F, typename FC = f_no_op_t>
    void operator()(F&& f, FC&& fc = FC{}) const;

    template<typename F, typename FC = f_no_op_t>
    void indexed(F&& f, FC&& fc = FC{}) const;
  };

  template<typename... Ts>
  requires(WeakSpan<decay_t<Ts>>&&...)
      for_each_bundle<Ts...> for_each(Ts&&... weak_spans);

  template<typename... Ts>
  concept GenericForEach = (WeakSpan<Ts> && ...) &&
      is_same_num_dims<Ts...>().value && (sizeof...(Ts) >= 1);

  template<typename F, typename FC, typename... Ts>
  requires GenericForEach<decay_t<Ts>...> void
  iterator_based_for_each(F&& f, FC&& fc, Ts&... spans);

  template<typename F, typename FC, typename... Ts>
  requires GenericForEach<decay_t<Ts>...> void
  iterator_based_for_each_indexed(F&& f, FC&& fc, Ts&... spans);

  template<typename F, typename FC, typename... Ts>
  requires GenericForEach<decay_t<Ts>...> void
  generic_for_each(F&& f, FC&& fc, Ts&... spans);

  template<typename F, typename FC, typename T, typename... Ts>
  requires GenericForEach<decay_t<T>, decay_t<Ts>...> void
  generic_for_each_indexed(F&& f, FC&& fc, T& span, Ts&... spans);

  template<typename T, typename... Ts>
  concept GenericForEachBundle = GenericForEach<T, Ts...> &&
      (Span<Ts> && ...) && is_decayed<T>() && (is_decayed<Ts>() && ...) &&
      (sizeof...(Ts) >= 1) && !is_same_template<T, Ts...>().value;

  struct generic_for_each_bundle_impl_facade {
    template<typename F, typename FC, typename... Us>
    static void apply(F&& f, FC&& fc, Us&... spans);

    template<typename F, typename FC, typename... Us>
    static void apply_indexed(F&& f, FC&& fc, Us&... spans);
  };

  struct iterator_based_for_each_bundle_impl_facade {
    template<typename F, typename FC, typename... Us>
    static void apply(F&& f, FC&& fc, Us&... spans);

    template<typename F, typename FC, typename... Us>
    static void apply_indexed(F&& f, FC&& fc, Us&... spans);
  };

  struct span_based_for_each_bundle_impl_facade {
    template<typename F, typename FC, typename... Us>
    static void apply(F&& f, FC&& fc, Us&... spans);

    template<typename F, typename FC, typename... Us>
    static void apply_indexed(F&& f, FC&& fc, Us&... spans);
  };

  template<typename T, typename... Ts>
  requires GenericForEachBundle<T, Ts...> struct for_each_bundle_impl<T, Ts...>
    : public generic_for_each_bundle_impl_facade {
  };

  template<typename F>
  auto prepend_dummy_parameter(F&& f);

  struct for_each_continue {
    bool continue_;
  };

  template<WeakSpan... Ts, typename F>
  auto wrap_for_each_functor(F&& f);

  template<WeakSpan T, WeakSpan... Ts, typename F>
  auto wrap_for_each_functor_indexed(F&& f);
}

#include "for_each.inl"

#endif
