#ifndef INCLUDE_GUARD_NUMA_CELL_REFORM_H_
#define INCLUDE_GUARD_NUMA_CELL_REFORM_H_

#include "reform_recursive.hpp"
#include "reform_unified.hpp"

#include "numa/boot/bits.hpp"
#include "numa/boot/error.hpp"
#include "numa/boot/index.hpp"
#include "numa/boot/span.hpp"

namespace numa {
  template<typename Target>
  requires(!std::is_reference_v<Target>) void shrink_to_fit(Target&& tgt);

  template<typename Target>
  requires(!std::is_reference_v<Target>) void clear(Target&& tgt);

  template<typename Target>
  requires(!std::is_reference_v<Target>) void clear_hard(Target&& tgt);

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void resize(
      Target&& tgt,
      index<Target::sel_num_axes> const& new_size,
      Initers const&... initers);

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void resize_hard(
      Target&& tgt,
      index<Target::sel_num_axes> const& new_size,
      Initers const&... initers);

  template<typename Target>
  requires(!std::is_reference_v<Target>) void reserve(
      Target&& tgt, index<Target::sel_num_axes> const& min_capacity);

  template<typename Target>
  requires(!std::is_reference_v<Target>) void reserve_hard(
      Target&& tgt, index<Target::sel_num_axes> const& new_capacity);

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void insert_till(
      Target&& tgt,
      index<Target::sel_num_axes> const& from,
      index<Target::sel_num_axes> const& till,
      Initers const&... initers);

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void insert_size(
      Target&& tgt,
      index<Target::sel_num_axes> const& from,
      index<Target::sel_num_axes> const& size,
      Initers const&... initers);

  template<typename Target>
  requires(!std::is_reference_v<Target>) void erase_till(
      Target&& tgt,
      index<Target::sel_num_axes> const& from,
      index<Target::sel_num_axes> const& till);

  template<typename Target>
  requires(!std::is_reference_v<Target>) void erase_size(
      Target&& tgt,
      index<Target::sel_num_axes> const& from,
      index<Target::sel_num_axes> const& size);

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void emplace_back(
      Target&& tgt, Initers const&... initers);

  template<index_t Na>
  void pop_back(index<Na> count);

  template<index_t Na, typename... Initers>
  void insert_every_other(
      index<Na> first,
      index<Na> step,
      index<Na> insert,
      const Initers... initers);

  void erase_at(std::span<index_t const> erase_inds);

  void erase_if(std::span<int const> erase);

  template<index_t Na, typename... Initers>
  void erase_every_other(index<Na> step, index<Na> insert);
}

#include "reform.inl"

#endif
