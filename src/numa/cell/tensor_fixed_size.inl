namespace numa {
  template<typename T, index_t... Sizes>
  class tensor_fixed_size_iterator
    : public iterator_facade<tensor_fixed_size_iterator<T, Sizes...>, T> {
   public:
    constexpr static index_t N = sizeof...(Sizes);

    static_assert(N >= 1);

    tensor_fixed_size_iterator(T* ptr, index<N> const& current)
      : ptr_(ptr), current_(current)
    {
    }

    T* ptr() const
    {
      return ptr_;
    }

    void increment()
    {
      detail::tensor_increment(
          current_, index<N>(zero), index(Sizes...), index<N>(zero));

      ++ptr_;
    }

    void decrement()
    {
      detail::tensor_decrement(
          current_, index<N>(zero), index(Sizes...), index<N>(zero));

      --ptr_;
    }

    void advance(index_t const offset)
    {
      detail::tensor_advance(
          current_, offset, index<N>(zero), index(Sizes...), index<N>(zero));

      ptr_ += offset;
    }

    index_t distance_to(tensor_fixed_size_iterator const& other) const
    {
      return detail::tensor_distance_to(
          current_, other.current_, index<N>(zero), index(Sizes...));
    }

    index<N> const& pos() const
    {
      return current_;
    }

    bool equal_to(tensor_fixed_size_iterator const& other) const
    {
      return (ptr_ == other.ptr_);
    }

    T& dereference() const
    {
      return (*ptr_);
    }

   private:
    T* ptr_;
    index<N> current_;
  };

  /*
   * unvaried
   */
  template<typename T, index_t... Sizes>
  constexpr auto tensor_fixed_size_unvaried<T, Sizes...>::size() -> index<N>
  {
    return index<N>(Sizes...);
  }

  template<typename T, index_t... Sizes>
  constexpr auto tensor_fixed_size_unvaried<T, Sizes...>::capacity() -> index<N>
  {
    return size();
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_size_unvaried<T, Sizes...>::span() -> span_type
  {
    return {ptr(), size(), detail::capacity_to_stride(capacity())};
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_size_unvaried<T, Sizes...>::span() const -> span_const_type
  {
    return span_const();
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_size_unvaried<T, Sizes...>::span_const() const
      -> span_const_type
  {
    return {const_ptr(), size(), detail::capacity_to_stride(capacity())};
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_size_unvaried<T, Sizes...>::begin() -> iterator_type
  {
    return iterator_type(ptr(), zero);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_size_unvaried<T, Sizes...>::begin() const
      -> iterator_const_type
  {
    return begin_const();
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_size_unvaried<T, Sizes...>::begin_const() const
      -> iterator_const_type
  {
    return iterator_const_type(const_ptr(), zero);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_size_unvaried<T, Sizes...>::end() -> iterator_type
  {
    return iterator_type(ptr() + (Sizes * ...), zero);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_size_unvaried<T, Sizes...>::end() const
      -> iterator_const_type
  {
    return end_const();
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_size_unvaried<T, Sizes...>::end_const() const
      -> iterator_const_type
  {
    return iterator_const_type(const_ptr() + (Sizes * ...), zero);
  }

  template<typename T, index_t... Sizes>
  T* tensor_fixed_size_unvaried<T, Sizes...>::ptr()
  {
    return static_cast<T*>(static_cast<void*>(&m_data[0]));
  }

  template<typename T, index_t... Sizes>
  T const* tensor_fixed_size_unvaried<T, Sizes...>::const_ptr() const
  {
    return static_cast<T const*>(static_cast<void const*>(&m_data[0]));
  }

  template<typename T, index_t... Sizes>
  T* tensor_fixed_size_unvaried<T, Sizes...>::get_ptr_impl(index<N> const& ind)
  {
    constexpr static auto stride = detail::capacity_to_stride(capacity());
    return ptr() + dot(ind, stride);
  }

  template<typename T, index_t... Sizes>
  T const* tensor_fixed_size_unvaried<T, Sizes...>::const_get_ptr_impl(
      index<N> const& ind) const
  {
    constexpr static auto stride = detail::capacity_to_stride(capacity());
    return const_ptr() + dot(ind, stride);
  }

  template<typename T, index_t... Sizes>
  bool tensor_fixed_size_unvaried<T, Sizes...>::is_bounded_impl(
      index<N> const& ind) const
  {
    return is_bounded(index<N>(zero), ind) &&
        is_bounded_strictly(ind, index(Sizes...));
  }

  template<typename T, index_t... Sizes>
  auto
  tensor_fixed_size_unvaried<T, Sizes...>::iterator_impl(index<N> const& ind)
  {
    return iterator_type(get_ptr_impl(ind), ind);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_size_unvaried<T, Sizes...>::const_iterator_impl(
      index<N> const& ind) const
  {
    return iterator_const_type(const_get_ptr_impl(ind), ind);
  }

  /*
   * cell
   */
  template<typename T, index_t... Sizes>
  template<typename... Args>
  tensor_fixed_size<T, Sizes...>::tensor_fixed_size(Args const&... args)
  {
    for_each(this->span())(f_fill_construct_t{args...}, f_destruct);
  }

  template<typename T, index_t... Sizes>
  template<typename F>
  tensor_fixed_size<T, Sizes...>::tensor_fixed_size(expert_t, F&& f)
  {
    f(this->span());
  }

  template<typename T, index_t... Sizes>
  tensor_fixed_size<T, Sizes...>::tensor_fixed_size(
      tensor_fixed_size const& other)
  {
    for_each(this->span(), other.span())(f_copy_construct_t{}, f_destruct);
  }

  template<typename T, index_t... Sizes>
  tensor_fixed_size<T, Sizes...>&
  tensor_fixed_size<T, Sizes...>::operator=(tensor_fixed_size const& other)
  {
    for_each(this->span(), other.span())(f_copy_assign, f_destruct);

    return *this;
  }

  template<typename T, index_t... Sizes>
  tensor_fixed_size<T, Sizes...>::tensor_fixed_size(tensor_fixed_size&& other)
  {
    for_each(this->span(), other.span())(f_move_construct, f_destruct);
  }

  template<typename T, index_t... Sizes>
  tensor_fixed_size<T, Sizes...>&
  tensor_fixed_size<T, Sizes...>::operator=(tensor_fixed_size&& other)
  {
    for_each(this->span(), other.span())(f_move_assign, f_destruct);

    return *this;
  }

  template<typename T, index_t... Sizes>
  tensor_fixed_size<T, Sizes...>::~tensor_fixed_size()
  {
    for_each(this->span())(f_destruct);
  }

  template<typename T, index_t... Sizes>
  constexpr index_t
  tensor_fixed_size<T, Sizes...>::required_elements(index<N> const& size)
  {
    ASSERT(size == index(Sizes...));

    return 0;
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_size<T, Sizes...>::unvaried() -> unvaried_type&
  {
    return (*this);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_size<T, Sizes...>::unvaried() const -> unvaried_type const&
  {
    return (*this);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_size<T, Sizes...>::unvaried_const() const
      -> unvaried_type const&
  {
    return (*this);
  }

  template<typename T, index_t... Sizes>
  template<index_t... Axes>
  constexpr auto tensor_fixed_size<T, Sizes...>::vary_axes_impl(
      index_list_t<Axes...>) requires(is_variation<N>(index_list<Axes...>)
                                          .value)
  {
    return tensor_fixed_size_axes<T, index_list_t<Axes...>, Sizes...>{this};
  }

  template<typename T, index_t... SelAxes, index_t... Sizes>
  class tensor_fixed_size_axes<T, index_list_t<SelAxes...>, Sizes...>
    : public cell_axes_facade_light<
          tensor_fixed_size_axes<T, index_list_t<SelAxes...>, Sizes...>,
          tensor_fixed_size<T, Sizes...>,
          index_list_t<SelAxes...>> {
   public:
    using baseclass = cell_axes_facade_light<
        tensor_fixed_size_axes<T, index_list_t<SelAxes...>, Sizes...>,
        tensor_fixed_size<T, Sizes...>,
        index_list_t<SelAxes...>>;

    using baseclass::baseclass;

    using baseclass::sel_num_axes;

    using parent = typename baseclass::parent;

    auto size() const
    {
      return select_at(index(Sizes...), index_list<SelAxes...>);
    }

    auto capacity() const
    {
      return select_at(index(Sizes...), index_list<SelAxes...>);
    }

    bool must_reallocate() const
    {
      return false;
    }

    void move_assign(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        index<sel_num_axes> const& target);

    template<typename... Args>
    void fill_assign(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        Args const&... args);

    void move_construct(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        index<sel_num_axes> const& target);

    template<typename... Args>
    void fill_construct(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        Args const&... args)
    {
      for_each(baseclass::get_span(base, form, *this->cell_))(
          f_fill_construct_t{args...}, f_destruct);
    }

    void
    destruct(index<sel_num_axes> const& base, index<sel_num_axes> const& form);

    void set_size(index<sel_num_axes> const& new_size)
    {
      THROW_IF_NOT(
          new_size == size(), "cannot change size of fixed-size-tensor");
    }

    translocation_forbidden_t translocation(index<sel_num_axes> const&)
    {
      return {};
    }

    template<typename TupleLike, typename... Args>
    void inplace(
        index<sel_num_axes> const& new_size,
        TupleLike const& inplace_fs,
        Args const&... args) requires(std::tuple_size_v<TupleLike> == sel_num_axes);
  };

  /*
   * for-each
   *
   * fixed-size-tensor is its own cell-span!
   */
  template<typename... Ts, index_t... Sizes>
  struct for_each_bundle_impl<tensor_fixed_size_unvaried<Ts, Sizes...>...>
    : public iterator_based_for_each_bundle_impl_facade {
  };

  template<typename T, index_t... Sizes>
  bool operator==(
      tensor_fixed_size_unvaried<T, Sizes...> const& lhs,
      tensor_fixed_size_unvaried<T, Sizes...> const& rhs)
  {
    if (lhs.size() != rhs.size()) {
      return false;
    }

    return all_of(f_is_equal, lhs, rhs);
  }

  template<typename T, index_t... Sizes>
  bool operator!=(
      tensor_fixed_size_unvaried<T, Sizes...> const& lhs,
      tensor_fixed_size_unvaried<T, Sizes...> const& rhs)
  {
    return !(lhs == rhs);
  }
}
