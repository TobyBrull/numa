#ifndef INCLUDE_GUARD_NUMA_CELL_CELL_FACADE_H_
#define INCLUDE_GUARD_NUMA_CELL_CELL_FACADE_H_

#include "concepts.hpp"
#include "fwd.hpp"
#include "reform_axis.hpp"
#include "t_span_facade.hpp"

#include "numa/core/memory.hpp"
#include "numa/core/uninitialized.hpp"

#include "numa/tmpl/axes_info.hpp"
#include "numa/tmpl/integer_list.hpp"

#include "numa/boot/index.hpp"

namespace numa {
  struct disable_copy_move {
    disable_copy_move() = default;

    disable_copy_move(disable_copy_move const&) = delete;
    disable_copy_move& operator=(disable_copy_move const&) = delete;

    disable_copy_move(disable_copy_move&&) = delete;
    disable_copy_move& operator=(disable_copy_move&&) = delete;
  };

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  struct unvaried_facade {
   public:
    static_assert(Na >= 1);
    static_assert(Nd >= Na);
    static_assert(is_value_type<T>());

    using value_type   = T;
    using element_type = T;

    /**
     * Index-based access
     */
    template<typename... Index>
    requires constructs_index_v<Nd, Index...> T& operator()(Index... inds);

    template<typename... Index>
    requires constructs_index_v<Nd, Index...> T const&
    operator()(Index... inds) const;

    /**
     * Index-based access with range-check.
     */
    template<typename... Index>
    requires constructs_index_v<Nd, Index...> T& at(Index... inds);

    template<typename... Index>
    requires constructs_index_v<Nd, Index...> T const& at(Index... inds) const;

    /**
     * Check if given index can be dereferenced.
     */
    template<typename... Index>
    requires constructs_index_v<Nd, Index...> bool
    is_bounded(Index... inds) const;

    /**
     * Index-based access.
     */
    T& operator[](index<Nd> const& ind);

    T const& operator[](index<Nd> const& ind) const;

    /**
     * Access to data pointer
     */
    T* data();

    T const* data() const;

    /**
     * Index-based acces via pointer.
     */
    template<typename... Index>
    requires constructs_index_v<Nd, Index...> T* get_ptr(Index... inds);

    template<typename... Index>
    requires constructs_index_v<Nd, Index...> T const*
    get_ptr(Index... inds) const;

    /**
     * Get iterator to the specified index.
     */
    template<typename... Index>
    requires constructs_index_v<Nd, Index...> auto iterator(Index... inds);

    template<typename... Index>
    requires constructs_index_v<Nd, Index...> auto
    iterator(Index... inds) const;

    template<typename... Index>
    requires constructs_index_v<Nd, Index...> auto
    const_iterator(Index... inds) const;

    auto cbegin() const;
    auto cend() const;

   private:
    Subclass& subclass();
    Subclass const& subclass() const;
  };

  template<typename Subclass, index_t Na>
  struct vary_axes_facade {
   public:
    constexpr auto vary_axis() requires(Na == 1);

    constexpr auto vary_axes();

    template<index_t... SelAxes>
    requires(is_variation<Na>(index_list<SelAxes...>)
                 .value) constexpr auto vary_axes();

    template<index_t... SelAxes>
    requires(is_variation<Na>(index_list<SelAxes...>)
                 .value) constexpr auto vary_axes(c_index_t<SelAxes>...);

    template<index_t... SelAxes>
    requires(is_variation<Na>(index_list<SelAxes...>)
                 .value) constexpr auto vary_axes(index_list_t<SelAxes...>);

    template<bool... SelAxes>
    requires(sizeof...(SelAxes) == Na) constexpr auto vary_axes(
        c_bool_t<SelAxes>...);

    template<bool... SelAxes>
    requires(sizeof...(SelAxes) == Na) constexpr auto vary_axes(
        bool_list_t<SelAxes...>);
  };

  template<typename Subclass, typename C, index_t... SelAxes>
  class cell_axes_facade_light<Subclass, C, index_list_t<SelAxes...>> {
   public:
    constexpr explicit cell_axes_facade_light(C* cell);

    constexpr static index_list_t<SelAxes...> sel_axes = {};

    constexpr static index_t sel_num_axes = sizeof...(SelAxes);

    cell_axes_facade_light(cell_axes_facade_light const&) = delete;
    cell_axes_facade_light& operator=(cell_axes_facade_light const&) = delete;

    cell_axes_facade_light(cell_axes_facade_light&&);
    cell_axes_facade_light& operator=(cell_axes_facade_light&&);

    using value_type = typename C::value_type;
    using parent     = C;

    static auto get_span(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        C& cell);

   protected:
    C* cell_ = nullptr;
  };

  template<typename Subclass, typename C, index_t... SelAxes>
  class cell_axes_facade<Subclass, C, index_list_t<SelAxes...>>
    : public cell_axes_facade_light<Subclass, C, index_list_t<SelAxes...>> {
   public:
    using baseclass =
        cell_axes_facade_light<Subclass, C, index_list_t<SelAxes...>>;

    using baseclass::baseclass;

    using baseclass::sel_num_axes;

    using value_type = typename baseclass::value_type;

    using reform_mode_t = reform_mode_for<allocator_t<C>, value_type>;

    Subclass&& with(reform_mode_t ref_mode) &&;

    /*
     * For recursive reform.
     */
    auto size() const;
    auto capacity() const;
    bool must_reallocate() const;

    void move_assign(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        index<sel_num_axes> const& target);

    template<typename... Args>
    void fill_assign(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        Args const&... args);

    void move_construct(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        index<sel_num_axes> const& target);

    template<typename... Args>
    void fill_construct(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        Args const&... args);

    void
    destruct(index<sel_num_axes> const& base, index<sel_num_axes> const& form);

    void set_size(index<sel_num_axes> const& new_size);

    auto translocation(index<sel_num_axes> const& new_capacity);

    /*
     * For unified reform.
     */
    template<typename TupleLike, typename... Args>
    void translate(
        index<sel_num_axes> const& new_size,
        index<sel_num_axes> const& new_capacity,
        TupleLike const& translate_fs,
        Args const&... args) requires(std::tuple_size_v<TupleLike> == sel_num_axes);

    template<typename TupleLike, typename... Args>
    void inplace(
        index<sel_num_axes> const& new_size,
        TupleLike const& inplace_fs,
        Args const&... args) requires(std::tuple_size_v<TupleLike> == sel_num_axes);

   private:
    reform_mode_t ref_mode_ = default_reform_mode;
  };

  template<typename Subclass, typename C, index_t... SelAxes>
  class cell_axes_facade_translocation<Subclass, C, index_list_t<SelAxes...>> {
   public:
    constexpr explicit cell_axes_facade_translocation(C* first, C second);

    using value_type = typename C::value_type;

    constexpr static index_list_t<SelAxes...> sel_axes = {};

    constexpr static index_t sel_num_axes = sizeof...(SelAxes);

    cell_axes_facade_translocation(cell_axes_facade_translocation const&) =
        delete;

    cell_axes_facade_translocation&
    operator=(cell_axes_facade_translocation const&) = delete;

    cell_axes_facade_translocation(cell_axes_facade_translocation&&);

    cell_axes_facade_translocation& operator=(cell_axes_facade_translocation&&);

    ~cell_axes_facade_translocation();

    template<typename... Args>
    void fill(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        Args const&... args);

    void move(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        index<sel_num_axes> const& dest);

    void
    destruct(index<sel_num_axes> const& base, index<sel_num_axes> const& form);

    void set_size(index<sel_num_axes> const& new_size);

    /*
     * Helper
     */
    auto first_span();
    auto second_span();

   private:
    void swap();

    C* first_ = nullptr;
    C second_;
  };
}

#include "tensor_facade.inl"

#endif
