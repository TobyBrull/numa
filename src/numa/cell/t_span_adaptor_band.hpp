#ifndef INCLUDE_GUARD_NUMA_CELL_T_SPAN_ADAPTOR_BAND_H_
#define INCLUDE_GUARD_NUMA_CELL_T_SPAN_ADAPTOR_BAND_H_

#include "t_span_adaptor_bounded_facade.hpp"

namespace numa {
  template<Span S, IndexList Ls, IndexList Rs>
  struct t_span_adaptor_band<S, Ls, Rs>
    : public t_span_adaptor_bounded_facade<
          S,
          raster_band<Ls, Rs, S::axes_info.num_dims>> {
   public:
    constexpr static index_t Nd = S::axes_info.num_dims;
    static_assert(is_valid_band_parameters<Nd>(Ls{}, Rs{}));

    t_span_adaptor_band(S const& span);
  };

  /*
   * For spans.
   */
  template<Span S>
  auto diagonal(S const& span);

  template<index_t Bandwidth, Span S>
  auto band(S const& span, c_index_t<Bandwidth> = {});

  template<index_t Bandwidth, Span S>
  auto band_upper(S const& span, c_index_t<Bandwidth> = {});

  template<index_t Bandwidth, Span S>
  auto band_lower(S const& span, c_index_t<Bandwidth> = {});

  template<index_t Bandwidth, Span S>
  auto band_upper_strictly(S const& span, c_index_t<Bandwidth> = {});

  template<index_t Bandwidth, Span S>
  auto band_lower_strictly(S const& span, c_index_t<Bandwidth> = {});

  template<Span S, index_t... Ls, index_t... Rs>
  auto band(S const& span, index_list_t<Ls...> = {}, index_list_t<Rs...> = {});

  /*
   * For const cells.
   */
  template<Cell C>
  auto diagonal(C const& cell);

  template<index_t Bandwidth, Cell C>
  auto band(C const& cell, c_index_t<Bandwidth> = {});

  template<index_t Bandwidth, Cell C>
  auto band_upper(C const& cell, c_index_t<Bandwidth> = {});

  template<index_t Bandwidth, Cell C>
  auto band_lower(C const& cell, c_index_t<Bandwidth> = {});

  template<index_t Bandwidth, Cell C>
  auto band_upper_strictly(C const& cell, c_index_t<Bandwidth> = {});

  template<index_t Bandwidth, Cell C>
  auto band_lower_strictly(C const& cell, c_index_t<Bandwidth> = {});

  template<Cell C, index_t... Ls, index_t... Rs>
  auto band(C const& cell, index_list_t<Ls...> = {}, index_list_t<Rs...> = {});

  /*
   * For mutable cells.
   */
  template<Cell C>
  auto diagonal(C& cell);

  template<index_t Bandwidth, Cell C>
  auto band(C& cell, c_index_t<Bandwidth> = {});

  template<index_t Bandwidth, Cell C>
  auto band_upper(C& cell, c_index_t<Bandwidth> = {});

  template<index_t Bandwidth, Cell C>
  auto band_lower(C& cell, c_index_t<Bandwidth> = {});

  template<index_t Bandwidth, Cell C>
  auto band_upper_strictly(C& cell, c_index_t<Bandwidth> = {});

  template<index_t Bandwidth, Cell C>
  auto band_lower_strictly(C& cell, c_index_t<Bandwidth> = {});

  template<Cell C, index_t... Ls, index_t... Rs>
  auto band(C& cell, index_list_t<Ls...> = {}, index_list_t<Rs...> = {});
}

#include "t_span_adaptor_band.inl"

#endif
