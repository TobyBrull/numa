namespace numa {
  template<typename T, index_t N>
  tensor_uniform_unvaried<T, N>::tensor_uniform_unvaried(
      uninitialized_t, index<N> const& size)
    : size_(size)
  {
    no_op;
  }

  template<typename T, index_t N>
  tensor_uniform_unvaried<T, N>::tensor_uniform_unvaried(
      T value, index<N> const& size)
    : value_(std::move(value)), size_(size)
  {
  }

  template<typename T, index_t N>
  T& tensor_uniform_unvaried<T, N>::value()
  {
    return value_;
  }

  template<typename T, index_t N>
  T const& tensor_uniform_unvaried<T, N>::value() const
  {
    return value_;
  }

  template<typename T, index_t N>
  index<N> const& tensor_uniform_unvaried<T, N>::size() const
  {
    return size_;
  }

  template<typename T, index_t N>
  constexpr index<N> tensor_uniform_unvaried<T, N>::capacity()
  {
    return (index<N>(one) * std::numeric_limits<index_t>::max());
  }

  template<typename T, index_t N>
  auto tensor_uniform_unvaried<T, N>::span() -> span_type
  {
    return span_type{&value_, size_, index<N>(zero)};
  }

  template<typename T, index_t N>
  auto tensor_uniform_unvaried<T, N>::span() const -> span_const_type
  {
    return span_const();
  }

  template<typename T, index_t N>
  auto tensor_uniform_unvaried<T, N>::span_const() const -> span_const_type
  {
    return span_const_type{&value_, size_, index<N>(zero)};
  }

  template<typename T, index_t N>
  auto tensor_uniform_unvaried<T, N>::begin() const -> iterator_const_type
  {
    return begin_const();
  }

  template<typename T, index_t N>
  auto tensor_uniform_unvaried<T, N>::begin_const() const -> iterator_const_type
  {
    return span_const().begin();
  }

  template<typename T, index_t N>
  auto tensor_uniform_unvaried<T, N>::end() const -> iterator_const_type
  {
    return end_const();
  }

  template<typename T, index_t N>
  auto tensor_uniform_unvaried<T, N>::end_const() const -> iterator_const_type
  {
    return span_const().end();
  }

  template<typename T, index_t N>
  T* tensor_uniform_unvaried<T, N>::get_ptr_impl(index<N> const& ind)
  {
    return &value_;
  }

  template<typename T, index_t N>
  T const*
  tensor_uniform_unvaried<T, N>::const_get_ptr_impl(index<N> const& ind) const
  {
    return &value_;
  }

  template<typename T, index_t N>
  bool tensor_uniform_unvaried<T, N>::is_bounded_impl(index<N> const& ind) const
  {
    return is_bounded(index<N>(zero), ind) && is_bounded_strictly(ind, size_);
  }

  /*
   * cell
   */
  template<typename T, index_t N>
  tensor_uniform<T, N>::tensor_uniform(uninitialized_t u, index<N> const& size)
    : tensor_uniform_unvaried<T, N>(u, size)
  {
  }

  template<typename T, index_t N>
  tensor_uniform<T, N>::tensor_uniform(T value, index<N> const& size)
    : tensor_uniform_unvaried<T, N>(std::move(value), size)
  {
  }

  template<typename T, index_t N>
  tensor_uniform<T, N>::tensor_uniform(tensor_uniform const& other)
    : tensor_uniform_unvaried<T, N>(other.value_, other.size_)
  {
  }

  template<typename T, index_t N>
  auto tensor_uniform<T, N>::operator=(tensor_uniform const& other)
      -> tensor_uniform&
  {
    this->value_ = other.value_;
    this->size_  = other.size_;

    return (*this);
  }

  template<typename T, index_t N>
  tensor_uniform<T, N>::tensor_uniform(tensor_uniform&& other)
    : tensor_uniform_unvaried<T, N>(std::move(other.value_), other.size_)
  {
  }

  template<typename T, index_t N>
  auto tensor_uniform<T, N>::operator=(tensor_uniform&& other)
      -> tensor_uniform&
  {
    this->value_ = std::move(other.value_);
    this->size_  = other.size_;

    return (*this);
  }

  template<typename T, index_t N>
  constexpr index_t
  tensor_uniform<T, N>::required_elements(index<N> const& size)
  {
    return 0;
  }

  template<typename T, index_t N>
  auto tensor_uniform<T, N>::unvaried() -> unvaried_type&
  {
    return (*this);
  }

  template<typename T, index_t N>
  auto tensor_uniform<T, N>::unvaried() const -> unvaried_type const&
  {
    return (*this);
  }

  template<typename T, index_t N>
  auto tensor_uniform<T, N>::unvaried_const() const -> unvaried_type const&
  {
    return (*this);
  }

  template<typename T, index_t N>
  template<index_t... Axes>
  requires(is_variation<N>(index_list<Axes...>)
               .value) constexpr auto tensor_uniform<T, N>::
      vary_axes_impl(index_list_t<Axes...>)
  {
    return tensor_uniform_axes<T, N, index_list_t<Axes...>>{this};
  }

  template<typename T, index_t N, index_t... SelAxes>
  class tensor_uniform_axes<T, N, index_list_t<SelAxes...>>
    : public cell_axes_facade_light<
          tensor_uniform_axes<T, N, index_list_t<SelAxes...>>,
          tensor_uniform<T, N>,
          index_list_t<SelAxes...>> {
   public:
    using baseclass = cell_axes_facade_light<
        tensor_uniform_axes<T, N, index_list_t<SelAxes...>>,
        tensor_uniform<T, N>,
        index_list_t<SelAxes...>>;

    using baseclass::baseclass;

    using baseclass::sel_num_axes;

    auto size() const
    {
      return select_at(this->cell_->size_, index_list<SelAxes...>);
    }

    constexpr auto capacity() const
    {
      return select_at(this->cell_->capacity(), index_list<SelAxes...>);
    }

    bool must_reallocate() const
    {
      return false;
    }

    void move_assign(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        index<sel_num_axes> const& target);

    template<typename... Args>
    void fill_assign(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        Args const&... args);

    void move_construct(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        index<sel_num_axes> const& target);

#define ERR_MSG "uniform-tensor axes may not be called with initializers"

    template<typename... Args>
    void fill_construct(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        Args const&... args)
    {
      static_assert(sizeof...(Args) == 0, ERR_MSG);

      no_op;
    }

    void
    destruct(index<sel_num_axes> const& base, index<sel_num_axes> const& form)
    {
      no_op;
    }

    void set_size(index<sel_num_axes> const& new_size)
    {
      overwrite(this->cell_->size_, index_list<SelAxes...>, new_size);
    }

    translocation_forbidden_t translocation(index<sel_num_axes> const&)
    {
      return {};
    }

    template<typename TupleLike, typename... Args>
    void inplace(
        index<sel_num_axes> const& new_size,
        TupleLike const&,
        Args const&... args) requires(std::tuple_size_v<TupleLike> == sel_num_axes)
    {
      static_assert(sizeof...(Args) == 0, ERR_MSG);

      set_size(new_size);
    }

#undef ERR_MSG
  };

  template<typename T, index_t N>
  bool operator==(
      tensor_uniform_unvaried<T, N> const& lhs,
      tensor_uniform_unvaried<T, N> const& rhs)
  {
    if (lhs.size() != rhs.size()) {
      return false;
    }

    return lhs.values() == rhs.values();
  }

  template<typename T, index_t N>
  bool operator!=(
      tensor_uniform_unvaried<T, N> const& lhs,
      tensor_uniform_unvaried<T, N> const& rhs)
  {
    return !(lhs == rhs);
  }
}
