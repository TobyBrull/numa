namespace numa {
  template<typename... Fields>
  template<typename... Initers>
  structure<Fields...>::structure(Initers const&... initers) : base(initers...)
  {
    /*
     * TODO: there should be just one trivial-resize for each structure.
     */
    structure_info.for_each_field_contrary_recursive(
        [this](
            auto const field_ind,
            auto const local_axes,
            auto const size,
            auto const&... initers_or_args) {
          numa::detail::initial_resize(
              std::get<field_ind.value>(this->data_).vary_axes(local_axes),
              as_index(size),
              as_index(size),
              initers_or_args...);
        },
        [](auto const vai) { return vai.is_fixed_size(); },
        initers...);

    structure_info.for_each_field_contrary_recursive(
        [this](
            auto const field_ind,
            auto const local_axes,
            auto const capacity,
            auto const&... initers_or_args) {
          numa::detail::initial_resize(
              std::get<field_ind.value>(this->data_).vary_axes(local_axes),
              index<numa::size(local_axes)>(zero),
              as_index(capacity),
              initers_or_args...);
        },
        [](auto const vai) { return vai.is_fixed_capacity(); },
        initers...);
  }

  template<typename... Fields>
  auto structure<Fields...>::unvaried() -> unvaried_type&
  {
    return (*this);
  }

  template<typename... Fields>
  auto structure<Fields...>::unvaried() const -> unvaried_type const&
  {
    return (*this);
  }

  template<typename... Fields>
  auto structure<Fields...>::unvaried_const() const -> unvaried_type const&
  {
    return (*this);
  }

  /*
   * replace-child
   */
  template<typename... Fields>
  template<typename FieldMark>
  auto structure<Fields...>::replace(
      FieldMark,
      structure_by_mark_t<FieldMark>&& obj) requires(is_mark(c_type<FieldMark>)
                                                         .value)
  {
    return replace<FieldMark>(std::move(obj));
  }

  template<typename... Fields>
  template<typename FieldMark>
  auto
  structure<Fields...>::replace(structure_by_mark_t<FieldMark>&& obj) requires(
      is_mark(c_type<FieldMark>).value)
  {
    return replace<find(structure_info.field_marks, FieldMark{}).value>(
        std::move(obj));
  }

  template<typename... Fields>
  template<index_t K>
  auto structure<Fields...>::replace(
      c_index_t<K>,
      structure_t<K>&& obj) requires(K < structure_info.num_fields)
  {
    return replace<K>(std::move(obj));
  }

  template<typename... Fields>
  template<index_t K>
  auto structure<Fields...>::replace(structure_t<K>&& obj) requires(
      K < structure_info.num_fields)
  {
    auto const sz = this->get(c_index<K>).size();

    THROW_IF(
        obj.size() != sz, "structure::replace: needs replacement of same size");

    auto& strct = std::get<K>(this->data_);

    auto retval = std::move(strct);

    strct = std::move(obj);

    return retval;
  }

  /*
   *
   */
  template<typename... Fields>
  template<index_t... Axes>
  constexpr auto
      structure<Fields...>::vary_axes_impl(index_list_t<Axes...>) requires(
          is_variation<Na>(index_list<Axes...>).value)
  {
    return structure_axes<index_list_t<Axes...>, Fields...>{this};
  }

  template<index_t... Axes, typename... Fields>
  class structure_axes<index_list_t<Axes...>, Fields...> {
   public:
    using structure_info_t = structure_info_type<type_list_t<Fields...>>;

    constexpr static structure_info_t structure_info = {};

    constexpr static index_list_t<Axes...> selected_axes = {};

    constexpr explicit structure_axes(structure<Fields...>* strct)
      : strct_(strct)
    {
    }

    structure_axes(structure_axes const&) = delete;
    structure_axes& operator=(structure_axes const&) = delete;

    structure_axes(structure_axes&&);
    structure_axes& operator=(structure_axes&&);

    template<typename F, typename... Initers, typename... Indices>
    void recurse(
        F&& f,
        std::tuple<Initers const&...> initers_tuple,
        Indices const&... inds)
    {
      structure_info.for_each_field(
          [&](auto const field_ind,
              auto const sel_exts,
              auto const local_axes,
              auto const global_axes) {
            using Field =
                typename structure_info_t::template field_t<field_ind.value>;

            recursive_initializer_for_tuple<Field>(
                [&, inds...](auto const&... initers_or_args) {
                  f(std::get<field_ind.value>(strct_->data_)
                        .vary_axes(local_axes),
                    select_at(inds, sel_exts)...,
                    initers_or_args...);
                },
                initers_tuple);
          },
          selected_axes);
    }

    constexpr static index_t sel_num_axes = sizeof...(Axes);

    using reform_mode_t = reform_mode<null_allocator, alignment_of<char>>;

    structure_axes&& with(reform_mode_t ref_mode) &&
    {
      ref_mode_ = std::move(ref_mode);
      return static_cast<structure_axes&&>(*this);
    }

    index<sel_num_axes> size() const
    {
      return select_at(strct_->size(), selected_axes);
    }

    index<sel_num_axes> capacity() const
    {
      return select_at(strct_->capacity(), selected_axes);
    }

    bool must_reallocate() const
    {
      return false;
    }

   private:
    reform_mode_t ref_mode_ = default_reform_mode;

    structure<Fields...>* strct_ = nullptr;
  };
}
