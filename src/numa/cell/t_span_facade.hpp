#ifndef INCLUDE_GUARD_NUMA_CELL_SUBSCRIPT_FACADE_H_
#define INCLUDE_GUARD_NUMA_CELL_SUBSCRIPT_FACADE_H_

#include "numa/core/iterator_facade.hpp"

#include "numa/tmpl/axes_info.hpp"

namespace numa {
  template<typename Subclass, typename T, index_t Na, index_t Nd>
  struct t_span_facade {
   public:
    static_assert(Nd >= 0);
    static_assert(std::is_reference_v<T> == false);

    using element_type = T;
    using value_type   = std::remove_cv_t<T>;

    /**
     * Index-based access
     */
    template<typename... Index>
    requires constructs_index_v<Nd, Index...> decltype(auto)
    operator()(Index... inds) const;

    /**
     * Index-based access with range-check.
     */
    template<typename... Index>
    requires constructs_index_v<Nd, Index...> decltype(auto)
    at(Index... inds) const;

    /**
     * Check if given index can be dereferenced.
     */
    template<typename... Index>
    requires constructs_index_v<Nd, Index...> bool
    is_bounded(Index... inds) const;

    /**
     * Index-based access.
     */
    T& operator[](index<Nd> const& ind) const;

    /**
     * Index-based acces via pointer.
     */
    template<typename... Index>
    requires constructs_index_v<Nd, Index...> T* get_ptr(Index... inds) const;

    /**
     * Index-based acces via pointer with range-check.
     */
    template<typename... Index>
    requires constructs_index_v<Nd, Index...> T*
    get_ptr_checked(Index... inds) const;

    /**
     * Get iterator to the specified index.
     */
    template<typename... Index>
    requires constructs_index_v<Nd, Index...> auto
    iterator(Index... inds) const;

    auto axis() const requires(Na == 1);

    auto axes() const;

    template<index_t... SelAxes>
    requires(is_variation<Na>(index_list<SelAxes...>).value) auto axes() const;

    template<index_t... SelAxes>
    requires(is_variation<Na>(index_list<SelAxes...>).value) auto axes(
        c_index_t<SelAxes>...) const;

    template<index_t... SelAxes>
    requires(is_variation<Na>(index_list<SelAxes...>).value) auto axes(
        index_list_t<SelAxes...>) const;

    template<bool... SelAxes>
    requires(sizeof...(SelAxes) == Na) auto axes();

    template<bool... SelAxes>
    requires(sizeof...(SelAxes) == Na) auto axes(c_bool_t<SelAxes>...);

    template<bool... SelAxes>
    requires(sizeof...(SelAxes) == Na) auto axes(bool_list_t<SelAxes...>);

    auto dims() const;

    template<index_t... SelDims>
    requires(is_variation<Nd>(index_list<SelDims...>).value) auto dims() const;

    template<index_t... SelDims>
    requires(is_variation<Nd>(index_list<SelDims...>).value) auto dims(
        c_index_t<SelDims>...) const;

    template<index_t... SelDims>
    requires(is_variation<Nd>(index_list<SelDims...>).value) auto dims(
        index_list_t<SelDims...>) const;

    template<bool... SelAxes>
    requires(sizeof...(SelAxes) == Na) auto dims();

    template<bool... SelAxes>
    requires(sizeof...(SelAxes) == Na) auto dims(c_bool_t<SelAxes>...);

    template<bool... SelAxes>
    requires(sizeof...(SelAxes) == Na) auto dims(bool_list_t<SelAxes...>);

   private:
    Subclass& span_subclass();
    Subclass const& span_subclass() const;
  };

  template<typename Subclass, typename OuterSpanType, typename T, index_t Nd>
  class t_span_form_facade
    : public t_span_facade<
          t_span_form_facade<Subclass, OuterSpanType, T, Nd>,
          T,
          Nd,
          Nd> {
   public:
    constexpr static auto axes_info = axes_info_tensor<Nd>();

    OuterSpanType outer_span;
    index<Nd> base;
    index<Nd> till;

    constexpr t_span_form_facade() = default;

    constexpr t_span_form_facade(
        OuterSpanType const& outer_span,
        index<Nd> const& base,
        index<Nd> const& till);

    constexpr index<Nd> size() const;

    T* get_ptr_impl(index<Nd> const& ind) const;
    bool is_bounded_impl(index<Nd> const& ind) const;
    auto iterator_impl(index<Nd> const& ind) const;

    auto begin() const;
    auto end() const;
  };
}

#include "t_span_facade.inl"

#endif
