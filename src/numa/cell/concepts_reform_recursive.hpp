#ifndef INCLUDE_GUARD_NUMA_CELL_CONCEPTS_RECURSIVE_H_
#define INCLUDE_GUARD_NUMA_CELL_CONCEPTS_RECURSIVE_H_

#include "fwd.hpp"

#include "numa/boot/index.hpp"

namespace numa::recursive {
  /*
   * Translocation
   */
  template<typename T, index_t Na>
  concept RealTranslocationImpl =
      requires(T tloc, index<Na> size_ind, typename T::value_type value)
  {
    {
      T::sel_num_axes
    }
    ->std::convertible_to<index_t>;

    {tloc.fill(size_ind, size_ind, std::move(value))};
    {tloc.move(size_ind, size_ind, size_ind)};

    {tloc.destruct(size_ind, size_ind)};

    {tloc.set_size(size_ind)};
  };

  template<typename T>
  concept RealTranslocation = RealTranslocationImpl<T, T::sel_num_axes>;

  template<typename T>
  concept Translocation =
      std::is_same_v<T, translocation_forbidden_t> || RealTranslocation<T>;

  /*
   * Cell reform target
   */
  template<typename T, index_t Na>
  concept CellReformTargetImpl =
      requires(T tgt, index<Na> size_ind, typename T::value_type value)
  {
    {
      T::sel_num_axes
    }
    ->std::convertible_to<index_t>;

    /**
     * Not allowed to throw.
     */
    {
      tgt.size()
    }
    ->std::convertible_to<index<Na>>;
    {
      tgt.capacity()
    }
    ->std::convertible_to<index<Na>>;
    {
      tgt.must_reallocate()
    }
    ->std::convertible_to<bool>;

    /**
     * (a) Inplace reform
     */
    {tgt.move_assign(size_ind, size_ind, size_ind)};
    {tgt.fill_assign(size_ind, size_ind, std::move(value))};

    {tgt.move_construct(size_ind, size_ind, size_ind)};
    {tgt.fill_construct(size_ind, size_ind, std::move(value))};

    /**
     * Not allowed to throw.
     */
    {tgt.destruct(size_ind, size_ind)};
    {tgt.set_size(size_ind)};

    /**
     * (b) Translocating reform
     */
    {
      tgt.translocation(size_ind)
    }
    ->Translocation;
  };

  template<typename T>
  concept CellReformTarget = CellReformTargetImpl<T, T::sel_num_axes>;

  /*
   * Recursive reform target
   */
  struct some_reform_functor {
    template<typename Tgt>
    void operator()(Tgt&&)
    {
    }
  };

  template<typename Target, index_t Na>
  concept RecursiveReformTargetImpl = requires(Target tgt)
  {
    {tgt.recurse(
        some_reform_functor{},
        std::tuple<int const&>{0},
        index<2>(zero),
        index<2>(zero))};
  };

  template<typename Target>
  concept RecursiveReformTarget =
      RecursiveReformTargetImpl<Target, Target::sel_num_axes>;
}

#endif
