#ifndef INCLUDE_GUARD_NUMA_CELL_REFORM_AXIS_H_
#define INCLUDE_GUARD_NUMA_CELL_REFORM_AXIS_H_

#include "concepts.hpp"

#include "numa/core/uninitialized.hpp"

#include "numa/tmpl/literals.hpp"

#include "numa/boot/error.hpp"

#include <tuple>

namespace numa::detail {
  enum class op_mode {
    none           = 0,
    move_assign    = 1,
    move_construct = 2,
    fill_assign    = 3,
    fill_construct = 4,
    destruct       = 5,
    never          = 6
  };

  constexpr inline op_mode combine(op_mode lhs, op_mode rhs);

  template<typename Target>
  concept AxisTarget =
      requires(Target tgt, op_mode mode, index_t source, index_t target)
  {
    {tgt.apply(mode, source, target)};
  };

  /*
   * inplace functors
   */
  struct inplace_trivial {
    index_t old_size;

    inline void operator()(AxisTarget auto& tgt) const;
  };

  struct inplace_resize {
    index_t old_size;
    index_t new_size;

    inline void operator()(AxisTarget auto& tgt) const;
  };

  struct inplace_insert_size {
    index_t old_size;
    index_t base;
    index_t size;

    inline void operator()(AxisTarget auto& tgt) const;
  };

  struct inplace_erase_size {
    index_t old_size;
    index_t base;
    index_t size;

    inline void operator()(AxisTarget auto& tgt) const;
  };

  /*
   * translate functors
   */
  struct translate_trivial {
    index_t old_size;

    inline void operator()(AxisTarget auto& tgt) const;
  };

  struct translate_resize {
    index_t old_size;
    index_t new_size;

    inline void operator()(AxisTarget auto& tgt) const;
  };

  struct translate_insert_size {
    index_t old_size;
    index_t base;
    index_t size;

    inline void operator()(AxisTarget auto& tgt) const;
  };

  struct translate_erase_size {
    index_t old_size;
    index_t base;
    index_t size;

    inline void operator()(AxisTarget auto& tgt) const;
  };

  template<index_t N, Span S, typename... Args>
  void execute_op_mode(
      op_mode mode,
      index<N> const& from,
      index<N> const& to,
      S& first,
      S& second,
      Args const&... args);

  template<typename F, typename TupleLike>
  void apply_axis_target(F f, TupleLike const& tuple_fs);

  template<index_t Na, typename F, typename... Indices>
  auto make_reform_axis_array(Indices const&... inds);
}

#include "reform_axis.inl"

#endif
