#ifndef INCLUDE_GUARD_NUMA_CELL_REFORM_RECURSIVE_H_
#define INCLUDE_GUARD_NUMA_CELL_REFORM_RECURSIVE_H_

#include "concepts_reform.hpp"
#include "concepts_reform_recursive.hpp"
#include "growth_strategy.hpp"
#include "reform_axis.hpp"

#include "numa/boot/bits.hpp"
#include "numa/boot/error.hpp"
#include "numa/boot/index.hpp"

namespace numa::recursive {
  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void resize(
      Target&& tgt,
      index<Target::num_axes()> const& new_size,
      Initers const&... initers);

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void resize_hard(
      Target&& tgt,
      index<Target::num_axes()> const& new_size,
      Initers const&... initers);

  template<typename Target>
  requires(!std::is_reference_v<Target>) void reserve_hard(
      Target&& tgt, index<Target::num_axes()> const& new_capacity);

  template<typename Target, typename... Initers>
  requires(!std::is_reference_v<Target>) void insert_size(
      Target&& tgt,
      index<Target::num_axes()> const& base,
      index<Target::num_axes()> const& size,
      Initers const&... initers);

  template<typename Target>
  requires(!std::is_reference_v<Target>) void erase_size(
      Target&& tgt,
      index<Target::num_axes()> const& base,
      index<Target::num_axes()> const& size);
}

#include "reform_recursive.inl"

#endif
