namespace numa {
  /*
   * Regarding triangular.
   */

  template<side Side, strictly Strict, index_t Nd>
  constexpr bool is_bounded_triangular(index<Nd> const& ind)
  {
    static_assert((Nd % 2) == 0);

    const auto [l, r] = split<Nd / 2>(ind);

    if constexpr ((Side == side::right) && (Strict == strictly::no)) {
      return is_before(l, r);
    }
    else if constexpr ((Side == side::left) && (Strict == strictly::no)) {
      return is_before(r, l);
    }
    else if constexpr ((Side == side::right) && (Strict == strictly::yes)) {
      return is_before_strictly(l, r);
    }
    else if constexpr ((Side == side::left) && (Strict == strictly::yes)) {
      return is_before_strictly(r, l);
    }
  }

  template<side Side, strictly Strict, index_t Nd>
  constexpr bool
  raster_triangular<Side, Strict, Nd>::is_bounded(index<Nd> const& ind)
  {
    return is_bounded_triangular<Side, Strict>(ind);
  }

  template<side Side, index_t Nd>
  constexpr bool is_bounded_triangular(index<Nd> const& ind)
  {
    return is_bounded_triangular<Side, strictly::no, Nd>(ind);
  }

  template<side Side, index_t Nd>
  constexpr bool is_bounded_triangular_strictly(index<Nd> const& ind)
  {
    return is_bounded_triangular<Side, strictly::yes, Nd>(ind);
  }

  template<index_t Nd>
  constexpr bool is_mirrored(index<Nd> const& ind)
  {
    static_assert((Nd % 2) == 0);

    const auto [l, r] = split<Nd / 2>(ind);

    return (l == r);
  }

  template<index_t Nd>
  constexpr index<Nd> mirrored(index<Nd> const& ind)
  {
    static_assert((Nd % 2) == 0);

    const auto [l, r] = split<Nd / 2>(ind);

    return join(r, l);
  }

  /*
   * Regarding band.
   */
  template<index_t Nd, index_t... Ls, index_t... Rs>
  constexpr auto is_valid_band_parameters(
      index_list_t<Ls...>, index_list_t<Rs...>, c_index_t<Nd>)
  {
    return c_bool < ((Nd % 2) == 0) && (sizeof...(Ls) == (Nd / 2)) &&
        (sizeof...(Rs) == (Nd / 2)) && (((-Ls <= Rs) && ...)) > ;
  }

  template<index_t Nd, side Side, index_t... Ls, index_t... Rs>
  requires(is_valid_band_parameters<Nd>(index_list<Ls...>, index_list<Rs...>).value) constexpr bool is_bounded_staged(
      index_list_t<Ls...>,
      index_list_t<Rs...>,
      index<Nd> const& ind,
      index<Nd / 2> const& size)
  {
    auto const [l, r] = split<Nd / 2>(ind);

    if constexpr (Side == side::left) {
      auto const diag = (l - r);

      return is_bounded(diag, index(Ls...)) &&
          is_bounded(-index(Rs...), diag) &&
          is_bounded(index<Nd / 2>(zero), l) && is_bounded_strictly(l, size);
    }
    else {
      auto const diag = (l - r);

      return is_bounded(diag, index(Ls...)) &&
          is_bounded(-index(Rs...), diag) &&
          is_bounded(index<Nd / 2>(zero), r) && is_bounded_strictly(r, size);
    }
  }

  template<index_t Nd, index_t... Ls, index_t... Rs>
  requires(is_valid_band_parameters<Nd>(index_list<Ls...>, index_list<Rs...>).value) constexpr bool is_bounded_band(
      index_list_t<Ls...> ls,
      index_list_t<Rs...> rs,
      index<Nd> const& ind,
      index<Nd> const& form)
  {
    auto const [l, r] = split<Nd / 2>(ind);

    auto const diag = (l - r);

    return is_bounded(diag, index(Ls...)) && is_bounded(-index(Rs...), diag) &&
        is_bounded(index<Nd>(zero), ind) && is_bounded_strictly(ind, form);
  }

  template<IndexList Ls, IndexList Rs, index_t Nd>
  constexpr bool raster_band<Ls, Rs, Nd>::is_bounded(index<Nd> const& ind) const
  {
    return is_bounded_band<Nd>(Ls{}, Rs{}, ind, form_);
  }

  template<index_t Nd>
  constexpr bool is_bounded_sub(
      index<Nd> const& ind, index<Nd> const& from, index<Nd> const& till)
  {
    return numa::is_bounded(from, ind) && numa::is_bounded_strictly(ind, till);
  }
}
