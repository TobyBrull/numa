#ifndef INCLUDE_GUARD_NUMA_CELL_MAKE_CELL_H_
#define INCLUDE_GUARD_NUMA_CELL_MAKE_CELL_H_

#include "for_each.hpp"
#include "reform_detail.hpp"

#include "numa/core/functors.hpp"

namespace numa {
  /*
   * Span based.
   */
  template<Cell C, typename... Ts>
  struct make_cell_span_bundle {
    std::tuple<Ts...> spans_tuple_;

    using value_type = typename C::value_type;

    template<typename F = f_const_t<value_type>>
    C operator()(F&& f = {}) const;

    template<typename F>
    C indexed(F&& f) const;
  };

  template<typename T, typename... Ts>
  requires(Span<decay_t<T>> && (WeakSpan<decay_t<Ts>> && ...)) auto make_cell(
      T&& span, Ts&&... spans);

  template<Cell C, typename... Ts>
  requires(WeakSpan<decay_t<Ts>>&&...) auto make_cell(Ts&&... spans);

  /*
   * Index based.
   */
  template<Cell C>
  struct make_cell_index_bundle {
   public:
    constexpr static index_t Na = C::axes_info.num_axes;

    using value_type = typename C::value_type;

    make_cell_index_bundle(index<Na> const& size);

    template<typename F = f_const_t<value_type>>
    C operator()(F&& f = {});

    template<typename F>
    C indexed(F&& f);

   private:
    index<Na> size_;
  };

  template<Cell C, typename... Index>
  requires constructs_index_v<C::axes_info.num_axes, Index...> auto
  make_cell(Index... inds);

  /*
   * Others.
   */
  template<typename T>
  requires Span<decay_t<T>> auto make_copy(T&& span);

  template<Cell C, typename U>
  requires Span<decay_t<U>> C make_copy(U&& span);

  template<typename T>
  requires Span<decay_t<T>> auto make_move(T&& span);

  template<Cell C, typename U>
  requires Span<decay_t<U>> C make_move(U&& span);
}

#include "make_cell.inl"

#endif
