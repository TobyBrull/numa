#ifndef INCLUDE_GUARD_NUMA_CELL_MISC_H_
#define INCLUDE_GUARD_NUMA_CELL_MISC_H_

#include "numa/tmpl/integer_list.hpp"

#include "numa/boot/index.hpp"

namespace numa {
  enum class side { left = 0, right = 1 };

  enum class strictly { yes = 0, no = 1 };

  enum class major { left = 0, right = 1 };

  /*
   * Regarding triangular.
   */

  template<side Side, strictly Strict, index_t Nd>
  constexpr bool is_bounded_triangular(index<Nd> const& ind);

  template<side Side, strictly Strict, index_t Nd>
  struct raster_triangular {
    constexpr static inline index_t N = Nd;

    constexpr static bool is_bounded(index<Nd> const& ind);
  };

  template<side Side, index_t Nd>
  constexpr bool is_bounded_triangular(index<Nd> const& ind);

  template<side Side, index_t Nd>
  constexpr bool is_bounded_triangular_strictly(index<Nd> const& ind);

  template<index_t Nd>
  constexpr bool is_mirrored(index<Nd> const& ind);

  template<index_t Nd>
  constexpr index<Nd> mirrored(index<Nd> const& ind);

  /*
   * Regarding band.
   */
  template<index_t Nd, index_t... Ls, index_t... Rs>
  constexpr auto is_valid_band_parameters(
      index_list_t<Ls...>, index_list_t<Rs...>, c_index_t<Nd> = {});

  template<index_t Nd, side Side, index_t... Ls, index_t... Rs>
  requires(is_valid_band_parameters<Nd>(index_list<Ls...>, index_list<Rs...>).value) constexpr bool is_bounded_staged(
      index_list_t<Ls...>,
      index_list_t<Rs...>,
      index<Nd> const& ind,
      index<Nd / 2> const& size);

  template<index_t Nd, index_t... Ls, index_t... Rs>
  requires(is_valid_band_parameters<Nd>(index_list<Ls...>, index_list<Rs...>).value) constexpr bool is_bounded_band(
      index_list_t<Ls...>,
      index_list_t<Rs...>,
      index<Nd> const& ind,
      index<Nd> const& form);

  template<IndexList Ls, IndexList Rs, index_t Nd>
  requires(is_valid_band_parameters<Nd>(Ls{}, Rs{}).value) struct raster_band {
    constexpr static inline index_t N = Nd;

    constexpr bool is_bounded(index<Nd> const& ind) const;

    index<Nd> form_;
  };

  /*
   * Regarding sub.
   */

  template<index_t Nd>
  constexpr bool is_bounded_sub(
      index<Nd> const& ind, index<Nd> const& from, index<Nd> const& till);
}

#include "misc.inl"

#endif
