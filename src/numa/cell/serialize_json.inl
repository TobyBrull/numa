/*
 * Generic.
 */
namespace numa {
  namespace detail {
    struct IteratorBasedToJsonForEach {
     public:
      template<
          typename SpanT,
          typename FuncOpen,
          typename FuncElem,
          typename FuncMid,
          typename FuncClose>
      static void apply(
          SpanT& span,
          FuncOpen open,
          FuncElem elem,
          FuncMid mid,
          FuncClose close)
      {
        constexpr index_t Nd = SpanT::axes_info.num_dims;
        static_assert(Nd >= 1);

        for (index_t i = Nd; i > 0; --i) {
          open(i - 1);
        }

        std::optional<index<Nd>> last;
        bool first_on_level_0 = true;

        for_each(span).indexed([&](auto const& pos, auto&& val) {
          index_t highest_level = -1;

          for (index_t i = 1; i < Nd; ++i) {
            if (last && (pos[i] != last.value()[i])) {
              highest_level = (i - 1);
            }
          }

          if (highest_level >= 0) {
            for (index_t i = 0; i <= highest_level; ++i) {
              close(i);
            }

            mid(highest_level + 1);

            for (index_t i = highest_level + 1; i > 0; --i) {
              open(i - 1);
            }

            first_on_level_0 = true;
          }

          if (!first_on_level_0) {
            mid(0);
          }
          else {
            first_on_level_0 = false;
          }

          elem(val);

          last = pos;
        });

        for (index_t i = 0; i < Nd; ++i) {
          close(i);
        }
      }
    };

    template<typename ToJsonForEach, typename SpanT>
    void to_json_impl(std::ostream& os, SpanT& span, int const indent)
    {
      constexpr index_t Nd = SpanT::axes_info.num_dims;

      os << "{\n" << std::string(indent + 2, ' ') << "\"size\": ";

      to_json(os, span.size());

      os << ",\n"
         << std::string(indent + 2, ' ') << "\"values\":\n"
         << std::string(indent + 4, ' ');

      if constexpr (Nd == 0) {
        to_json(os, span[index<0>()]);
      }
      else {
        ToJsonForEach::apply(
            span,
            [&](index_t const) { os << '['; },
            [&](auto const& val) { to_json(os, val, indent + 4); },
            [&](index_t const level) {
              if (level == 0) {
                os << ", ";
              }
              else {
                os << ",\n" << std::string(Nd - level + indent + 4, ' ');
              }
            },
            [&](index_t const level) {
              if (level > 0) {
                os << '\n' << std::string(Nd - 1 - level + indent + 4, ' ');
              }

              os << ']';
            });
      }

      os << '\n' << std::string(indent, ' ') << '}';
    }
  }

  template<Span T>
  void to_json(std::ostream& os, T const& span, int const indent)
  {
    detail::to_json_impl<detail::IteratorBasedToJsonForEach>(os, span, indent);
  }

  template<Cell T>
  requires(!Span<T>) void to_json(
      std::ostream& os, T const& cell, int const indent)
  {
    to_json(os, cell.unvaried(), indent);
  }

  namespace detail {
    template<index_t Na>
    index<Na> from_json_size_header(std::istream& is)
    {
      read_till(is, '{');
      forward_read_till(is, "\"size\"");
      read_till(is, ':');

      auto const size = make_from_json<index<Na>>(is);

      read_till(is, ',');

      return size;
    }

    template<typename ToJsonForEach, typename SpanT>
    void from_json_span_core(std::istream& is, SpanT& span)
    {
      constexpr index_t Nd = SpanT::axes_info.num_dims;

      forward_read_till(is, "\"values\"");
      read_till(is, ':');

      if constexpr (Nd == 0) {
        from_json(is, span[index<0>()]);
      }
      else {
        ToJsonForEach::apply(
            span,
            [&](index_t const) { read_till(is, '['); },
            [&](auto& val) { from_json(is, val); },
            [&](index_t const level) { read_till(is, ','); },
            [&](index_t const level) { read_till(is, ']'); });
      }

      read_till(is, '}');
    }
  }

  template<typename T>
  requires Span<decay_t<T>> void from_json(std::istream& is, T&& span)
  {
    auto const size =
        detail::from_json_size_header<decay_t<T>::axes_info.num_axes>(is);

    THROW_IF_NOT(
        size == span.size(),
        "numa::from_json: trying to read into span of wrong size");

    detail::from_json_span_core<detail::IteratorBasedToJsonForEach>(is, span);
  }

  template<Cell T>
  void from_json(std::istream& is, T& cell)
  {
    from_json(is, cell.unvaried());
  }

  template<Cell T>
  T make_from_json_impl(c_type_t<T>, std::istream& is)
  {
    auto const size = detail::from_json_size_header<T::axes_info.num_axes>(is);

    auto retval = make_cell<T>(size)();

    detail::from_json_span_core<detail::IteratorBasedToJsonForEach>(is, retval);

    return retval;
  }

  /*
   * Specialization for sparse-tensor
   */
  template<typename T, index_t N, typename X, Allocator Alloc>
  void to_json(
      std::ostream& os,
      tensor_sparse_unvaried<T, N, X, Alloc> const& st,
      int const indent)
  {
    os << "{\n" << std::string(indent + 2, ' ') << "\"size\": ";
    to_json(os, st.size());

    os << ",\n" << std::string(indent + 2, ' ') << "\"storage_size\": ";
    to_json(os, st.storage_size());

    os << ",\n" << std::string(indent + 2, ' ') << "\"indices\": [";

    {
      bool first = true;
      for (auto const& ind: st.indices()) {
        if (!first) {
          os << ", ";
        }

        to_json(os, ind);

        first = false;
      }
    }

    os << "],\n" << std::string(indent + 2, ' ') << "\"values\": [";

    {
      bool first = true;
      for (auto const& val: st.values()) {
        if (!first) {
          os << ", ";
        }

        os << val;

        first = false;
      }
    }

    os << "]\n";
    os << std::string(indent, ' ') << "}";
  }

  namespace detail {
    template<index_t N, typename X>
    std::pair<index<N, X>, index_t>
    from_json_tensor_sparse_header(std::istream& is)
    {
      read_till(is, '{');
      forward_read_till(is, "\"size\"");
      read_till(is, ':');

      auto const size = make_from_json<index<N>>(is);

      read_till(is, ',');
      forward_read_till(is, "\"storage_size\"");
      read_till(is, ':');

      auto const storage_size = make_from_json<index_t>(is);

      return std::pair(size, storage_size);
    }

    template<typename T, typename F>
    void from_json_tensor_sparse_core_single(
        std::istream& is,
        std::string_view const sv,
        index_t const storage_size,
        F f)
    {
      forward_read_till(is, sv);
      read_till(is, ':');

      read_till(is, '[');

      T value = {};

      bool first = true;
      for (index_t i = 0; i < storage_size; ++i) {
        if (!first) {
          read_till(is, ',');
        }
        else {
          first = false;
        }

        from_json(is, value);

        f(value);
      }

      read_till(is, ']');
    }

    template<typename T, index_t N, typename X>
    void from_json_tensor_sparse_core(
        std::istream& is,
        index_t const storage_size,
        range<index<N, X>*> indices,
        range<T*> values)
    {
      ASSERT(storage_size == indices.size());
      ASSERT(storage_size == values.size());

      auto [ind_begin, ind_end] = indices.as_tuple();
      auto [val_begin, val_end] = values.as_tuple();

      (void)ind_end;
      (void)val_end;

      read_till(is, ',');

      detail::from_json_tensor_sparse_core_single<index<N, X>>(
          is, "\"indices\"", storage_size, [&](index<N, X> const& ind) {
            ASSERT(ind_begin != ind_end);

            *ind_begin = ind;

            ++ind_begin;
          });

      read_till(is, ',');

      detail::from_json_tensor_sparse_core_single<T>(
          is, "\"values\"", storage_size, [&](T const& val) {
            ASSERT(val_begin != val_end);

            *val_begin = val;

            ++val_begin;
          });

      read_till(is, '}');
    }
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  void from_json(std::istream& is, tensor_sparse_unvaried<T, N, X, Alloc>& st)
  {
    auto const [size, storage_size] =
        detail::from_json_tensor_sparse_header<N, X>(is);

    THROW_IF_NOT(
        size == st.size(), "from_json: trying to read into span of wrong size");

    st.storage_resize(storage_size);

    detail::from_json_tensor_sparse_core(
        is, storage_size, st.indices(), st.values());
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  tensor_sparse<T, N, X, Alloc>
  make_from_json_impl(c_type_t<tensor_sparse<T, N, X, Alloc>>, std::istream& is)
  {
    auto const [size, storage_size] =
        detail::from_json_tensor_sparse_header<N, X>(is);

    tensor_sparse<T, N, X, Alloc> retval;

    retval.storage_resize(storage_size);

    retval.vary_axes().resize(size);

    detail::from_json_tensor_sparse_core(
        is, storage_size, retval.indices(), retval.values());

    return retval;
  }

  /*
   * Specialization for lin-range
   */
  template<typename T>
  void to_json(
      std::ostream& os, vector_linear_incr_unvaried<T> const& lr, int indent)
  {
    os << "{\n" << std::string(indent + 2, ' ') << "\"size\": ";
    to_json(os, lr.size());

    os << ",\n" << std::string(indent + 2, ' ') << "\"front\": ";
    to_json(os, lr.front());

    os << ",\n" << std::string(indent + 2, ' ') << "\"incr\": ";
    to_json(os, lr.incr());

    os << '\n' << std::string(indent, ' ') << '}';
  }

  namespace detail {
    template<typename T>
    void from_json_vector_linear_incr_core(
        std::istream& is, vector_linear_incr_unvaried<T>& lr)
    {
      forward_read_till(is, "\"front\"");
      read_till(is, ':');

      from_json(is, lr.front());

      read_till(is, ',');
      forward_read_till(is, "\"incr\"");
      read_till(is, ':');

      from_json(is, lr.incr());

      read_till(is, '}');
    }
  }

  template<typename T>
  void from_json(std::istream& is, vector_linear_incr_unvaried<T>& lr)
  {
    auto const size = detail::from_json_size_header<1>(is);

    THROW_IF_NOT(
        size == lr.size(),
        "from_json: trying to read into structure of wrong size");

    detail::from_json_vector_linear_incr_core(is, lr);
  }

  template<typename T>
  vector_linear_incr<T>
  make_from_json_impl(c_type_t<vector_linear_incr<T>>, std::istream& is)
  {
    auto const size = detail::from_json_size_header<1>(is);

    vector_linear_incr<T> retval(size);

    detail::from_json_vector_linear_incr_core(is, retval);

    return retval;
  }

  /*
   * Specialization for structure
   */
  template<typename... Fields>
  void to_json(
      std::ostream& os,
      structure_unvaried<Fields...> const& strct,
      int const indent)
  {
    os << "{\n" << std::string(indent + 2, ' ') << "\"size\": ";
    to_json(os, strct.size());

    for_each_indexed(
        type_list_t<Fields...>{}, [&](auto const ext, auto const field) {
          os << ",\n"
             << std::string(indent + 2, ' ') << '\"'
             << static_cast<std::string>(field.mark.name) << "\": ";

          to_json(os, strct.get(ext), indent + 2);
        });

    os << '\n' << std::string(indent, ' ') << '}';
  }

  namespace detail {
    template<typename... Fields>
    void from_json_structure_core(
        std::istream& is, structure_unvaried<Fields...>& strct)
    {
      for_each_indexed(
          type_list_t<Fields...>{}, [&](auto const ext, auto const field) {
            if (ext.value > 0) {
              read_till(is, ',');
            }

            auto const temp =
                "\"" + static_cast<std::string>(field.mark.name) + "\"";

            forward_read_till(is, temp);
            read_till(is, ':');

            from_json(is, strct.get(ext));
          });

      read_till(is, '}');
    }
  }

  template<typename... Fields>
  void from_json(std::istream& is, structure_unvaried<Fields...>& strct)
  {
    constexpr index_t Na = strct.structure_info.vary_axes_info().num_axes;

    auto const size = detail::from_json_size_header<Na>(is);

    THROW_IF_NOT(
        size == strct.size(),
        "from_json: trying to read into structure of wrong size");

    detail::from_json_structure_core(is, strct);
  }

  template<typename... Fields>
  structure<Fields...>
  make_from_json_impl(c_type_t<structure<Fields...>>, std::istream& is)
  {
    structure<Fields...> retval;

    constexpr index_t Na = retval.structure_info.vary_axes_info().num_axes;

    auto const size = detail::from_json_size_header<Na>(is);

    detail::initial_resize_is_not_fixed(retval, size, size);

    detail::from_json_structure_core(is, retval);

    return retval;
  }
}
