#ifndef INCLUDE_GUARD_NUMA_CELL_BAND_TENSOR_SPAN_H_
#define INCLUDE_GUARD_NUMA_CELL_BAND_TENSOR_SPAN_H_

#include "for_each.hpp"
#include "fwd.hpp"
#include "misc.hpp"
#include "t_span_facade.hpp"

#include "numa/core/index_detail.hpp"
#include "numa/core/iterator_facade.hpp"

#include "numa/tmpl/axes_info.hpp"

namespace numa {
  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  struct t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>
    : public t_span_facade<
          t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>,
          T,
          Nd / 2,
          Nd> {
    using ls_t = index_list_t<Ls...>;
    using rs_t = index_list_t<Rs...>;

    static_assert(is_valid_band_parameters<Nd>(ls_t{}, rs_t{}));

    constexpr static index_t Na = Nd / 2;

    constexpr static auto axes_info = axes_info_mirror_tensor<Na>();

    using span_type = t_span_staged<std::remove_const_t<T>, Nd, ls_t, rs_t>;
    using span_const_type = t_span_staged<T const, Nd, ls_t, rs_t>;

    T* origin         = nullptr;
    index<Na> size_   = zero;
    index<Nd> stride_ = zero;

    constexpr t_span_staged() = default;

    constexpr t_span_staged(
        T* origin, index<Na> const& size, index<Nd> const& stride);

    index<Na> const& size() const;

    T* get_ptr_impl(index<Nd> const& ind) const;
    bool is_bounded_impl(index<Nd> const& ind) const;
    auto iterator_impl(index<Nd> const& ind) const;

    auto begin() const;
    auto end() const;

    template<index_t... Axes>
    auto axes_impl(index_list_t<Axes...>) const
        requires(is_variation<Na>(index_list<Axes...>).value);
  };

  template<typename T, index_t Bandwidth>
  using matrix_span_staged =
      t_span_staged<T, 2, index_list_t<Bandwidth>, index_list_t<Bandwidth>>;

  template<typename T, index_t Nd>
  using t_span_diagonal = t_span_staged<
      T,
      Nd,
      index_list_uniform_t<Nd / 2, 0>,
      index_list_uniform_t<Nd / 2, 0>>;

  template<typename T>
  using matrix_span_diagonal = t_span_diagonal<T, 2>;

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs, typename U>
  requires((c_type<decay_t<T>> == c_type<decay_t<U>>).value) bool operator==(
      t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>> const& lhs,
      t_span_staged<U, Nd, index_list_t<Ls...>, index_list_t<Rs...>> const&
          rhs);

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs, typename U>
  requires((c_type<decay_t<T>> == c_type<decay_t<U>>).value) bool operator!=(
      t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>> const& lhs,
      t_span_staged<U, Nd, index_list_t<Ls...>, index_list_t<Rs...>> const&
          rhs);

  template<
      typename T,
      index_t Nd,
      index_t... Ls,
      index_t... Rs,
      index_t... SelectedAxes>
  class t_span_staged_axes<
      T,
      Nd,
      index_list_t<Ls...>,
      index_list_t<Rs...>,
      index_list_t<SelectedAxes...>> {
   public:
    constexpr static index_t Ka = sizeof...(SelectedAxes);

    using span_type =
        t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>;

    template<SubMode SM>
    auto sub(SM, index<Ka> const& base, index<Ka> const& size_or_till) const;

   private:
    friend struct t_span_staged<
        T,
        Nd,
        index_list_t<Ls...>,
        index_list_t<Rs...>>;

    t_span_staged_axes(span_type const* span);

    span_type const* span_;
  };
}

#include "t_span_staged.inl"

#endif
