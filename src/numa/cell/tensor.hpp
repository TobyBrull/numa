#ifndef INCLUDE_GUARD_NUMA_CELL_TENSOR_H_
#define INCLUDE_GUARD_NUMA_CELL_TENSOR_H_

#include "t_span.hpp"
#include "tensor_facade.hpp"

#include "numa/tmpl/vary_axes_info.hpp"

namespace numa {
  /*

  using index_t = int64_t;
  constexpr index_t dyn = -1;

  template<typename X = index_t, X... Is>
  struct index_as {
    public:
      std::array<index_t, ...> data
  };

  template<index_t N>
  using index_of = index_as<dyn, ..., dyn>;

  concept index = is_index_as...;

  template<typename T, index SizeT, index StrideT>
    requires SizeT::size() == StrideT::size()
  struct span {
    public:
      T* data = nullptr;
      SizeT size = zero;
      StrideT stride = zero;

      template<index Ind>
      T& at(Ind const& ind) const ->(out_of_bounds) {
        if (0 <= ind && ind < size) {
          return *(data + dot(ind, stride));
        }
        else {
          return(out_of_bounds);
        }
      }
  };

  template<typename T, index SizeT, major Major = major::left>
    requires !std::is_const<T>
  struct dynamic_tensor : public span<T, SizeT, to_stride_t<SizeT>> {
    std::memory_resource* m_mem_res;
    index_t m_capacity;

    void resize();
  };

  template<typename T, index SizeT, major Major = major::left>
    requires !std::is_const<T> && !is_any_dyn<SizeT>()
  struct automatic_tensor : public span<T, SizeT, to_stride_t<SizeT>> {
    index_t m_capacity;

    void resize();
  };

  using index1 = index_of<1>;
  using index2 = index_of<2>;
  using index3 = index_of<3>;

  template<typename T> using vector = dynamic_tensor<T, index1>;
  template<typename T> using matrix = dynamic_tensor<T, index2>;
  template<typename T> using cuboid = dynamic_tensor<T, index3>;
  template<typename T, index_t N> using tensor = dynamic_tensor<T, index_of<N>>;

  using vector_f = vector<float>
  using vector_d = vector<double>
  using vector_cf = vector<complex_f>
  using vector_cd = vector<complex_d>

  template<typename T, index_t I1>
  using vec = automatic_tensor<T, I1>;

  template<typename T, index_t I1, index_t I2>
  using mat = automatic_tensor<T, I1, I2>;

  template<typename T, index_t I1, index_t I2, index_t I3>
  using cub = automatic_tensor<T, I1, I2, I3>;

  template<typename T, index_t... Is>
  using ten = automatic_tensor<T, Is...>;

  using vec2f = vec<float, 2>;
  using vec3f = vec<float, 3>;
  using vec4f = vec<float, 4>;
  using mat4f = mat<float, 4, 4>;

  */

  template<typename T, index_t N>
  class tensor_unvaried
    : public unvaried_facade<tensor_unvaried<T, N>, T, N, N>
    , public disable_copy_move {
   public:
    static_assert(N >= 1);

    constexpr static auto axes_info = axes_info_tensor<N>();

    index<N> const& size() const;
    index<N> const& capacity() const;

    /*
     * span
     */
    using span_type       = t_span<T, N>;
    using span_const_type = t_span<const T, N>;

    span_type span();
    span_const_type span() const;
    span_const_type span_const() const;

    /*
     * iterator
     */
    using iterator_type       = t_span_iterator<T, N>;
    using iterator_const_type = t_span_iterator<const T, N>;

    iterator_type begin();
    iterator_const_type begin() const;
    iterator_const_type begin_const() const;

    iterator_type end();
    iterator_const_type end() const;
    iterator_const_type end_const() const;

    /*
     * implementation of unvaried-facade
     */
    T* get_ptr_impl(index<N> const& ind);
    T const* const_get_ptr_impl(index<N> const& ind) const;

    bool is_bounded_impl(index<N> const& ind) const;

    auto iterator_impl(index<N> const& ind);
    auto const_iterator_impl(index<N> const& ind) const;

   protected:
    tensor_unvaried(
        T* ptr, index<N> const& size, index<N> const& capacity) noexcept;

    T* ptr_            = nullptr;
    index<N> size_     = zero;
    index<N> capacity_ = zero;
  };

  template<typename T, index_t N, Allocator Alloc = default_allocator>
  class tensor
    : public tensor_unvaried<T, N>
    , public vary_axes_facade<tensor<T, N, Alloc>, N> {
   public:
    tensor(Alloc alloc = {}) noexcept;

    tensor(tensor const& other) = delete;
    tensor& operator=(tensor const& other) = delete;

    tensor(tensor&& other) noexcept;
    tensor& operator=(tensor&& other) noexcept;

    ~tensor();

    Alloc const& allocator() const;

    constexpr static index_t required_elements(index<N> const& size);

    constexpr static auto vary_axes_info = vary_axes_info_dynamic<N>();

    memory_for<Alloc, T> release();

    using unvaried_type = tensor_unvaried<T, N>;

    unvaried_type& unvaried();
    unvaried_type const& unvaried() const;
    unvaried_type const& unvaried_const() const;

    template<index_t... Axes>
    requires(is_variation<N>(index_list<Axes...>)
                 .value) constexpr auto vary_axes_impl(index_list_t<Axes...>);

   private:
    template<typename, index_t, Allocator, typename>
    friend class tensor_axes;

    Alloc alloc_;
  };

  template<typename T, index_t N>
  struct span_to_cell<t_span<T, N>> {
    using type = tensor<std::remove_const_t<T>, N>;
  };

  template<typename T, index_t N>
  struct span_to_cell<tensor_unvaried<T, N>> {
    using type = tensor<std::remove_const_t<T>, N>;
  };

  template<typename T, Allocator Alloc = default_allocator>
  using cuboid = tensor<T, 3, Alloc>;

  template<typename T, Allocator Alloc = default_allocator>
  using matrix = tensor<T, 2, Alloc>;

  template<typename T, Allocator Alloc = default_allocator>
  using vector = tensor<T, 1, Alloc>;

  template<typename T, index_t N>
  bool operator==(
      tensor_unvaried<T, N> const& lhs, tensor_unvaried<T, N> const& rhs);

  template<typename T, index_t N>
  bool operator!=(
      tensor_unvaried<T, N> const& lhs, tensor_unvaried<T, N> const& rhs);
}

#include "tensor.inl"

#endif
