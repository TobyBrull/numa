namespace numa {
  template<index_t Nd, typename Iter>
  class t_span_adaptor_sub_iterator
    : public iterator_facade<
          t_span_adaptor_sub_iterator<Nd, Iter>,
          value_type_t<Iter>> {
   public:
    t_span_adaptor_sub_iterator(
        Iter const& iter,
        Iter const& begin,
        Iter const& end,
        index<Nd> const& from,
        index<Nd> const& till)
      : iter_(iter), begin_(begin), end_(end), from_(from), till_(till)
    {
      ASSERT((iter_ == end_) || is_bounded_sub(iter_.pos(), from_, till_));
    }

    auto ptr() const
    {
      return iter_.ptr();
    }

    decltype(auto) pos() const
    {
      return iter_.pos() - from_;
    }

    void increment()
    {
      do {
        ++iter_;
      } while ((iter_ != end_) && !is_bounded_sub(iter_.pos(), from_, till_));
    }

    void decrement()
    {
      do {
        --iter_;
      } while ((iter_ != begin_) && !is_bounded_sub(iter_.pos(), from_, till_));
    }

    auto equal_to(t_span_adaptor_sub_iterator const& other) const
    {
      return iter_.equal_to(other.iter_);
    }

    decltype(auto) dereference() const
    {
      return iter_.dereference();
    }

   private:
    Iter iter_;
    Iter begin_;
    Iter end_;
    index<Nd> from_;
    index<Nd> till_;
  };

  template<Span S>
  template<SubMode SM>
  t_span_adaptor_sub<S>::t_span_adaptor_sub(
      S const& span, SM, index<Nd> const& from, index<Nd> const& size_or_till)
    : span_(span)
    , from_(from)
    , till_(sub_mode_convert(SM{}, from, size_or_till, as_till))
  {
  }

  template<Span S>
  auto t_span_adaptor_sub<S>::size() const -> index<Nd>
  {
    return (till_ - from_);
  }

  template<Span S>
  auto t_span_adaptor_sub<S>::get_ptr_impl(index<Nd> const& ind) const
      -> element_type*
  {
    return span_.get_ptr(ind + from_);
  }

  template<Span S>
  bool t_span_adaptor_sub<S>::is_bounded_impl(index<Nd> const& ind) const
  {
    return is_bounded_sub<Nd>(ind, zero, size());
  }

  template<Span S>
  auto t_span_adaptor_sub<S>::iterator_impl(index<Nd> const& ind) const
  {
    ASSERT(is_bounded_sub<Nd>(ind, zero, size()));

    return t_span_adaptor_sub_iterator<Nd, inner_iterator_type>{
        span_.iterator(ind), span_.begin(), span_.end(), from_, till_};
  }

  template<Span S>
  auto t_span_adaptor_sub<S>::begin() const
  {
    auto it     = span_.begin();
    auto end_it = span_.end();

    while ((it != end_it) && !is_bounded_sub(it.pos(), from_, till_)) {
      ++it;
    }

    return t_span_adaptor_sub_iterator<Nd, inner_iterator_type>{
        std::move(it), span_.begin(), std::move(end_it), from_, till_};
  }

  template<Span S>
  auto t_span_adaptor_sub<S>::end() const
  {
    return t_span_adaptor_sub_iterator<Nd, inner_iterator_type>{
        span_.end(), span_.begin(), span_.end(), from_, till_};
  }

  template<Span... S>
  class for_each_bundle_impl<t_span_adaptor_sub<S>...>
    : public generic_for_each_bundle_impl_facade {
  };

  template<Span S, SubMode SM>
  auto adaptor_sub(
      S const& span,
      SM,
      index<S::axes_info.num_dims> const& from,
      index<S::axes_info.num_dims> const& size_or_till)
  {
    return t_span_adaptor_sub<S>(span, SM{}, from, size_or_till);
  }

  template<Cell C, SubMode SM>
  auto adaptor_sub(
      C& cell,
      SM,
      index<C::axes_info.num_dims> const& from,
      index<C::axes_info.num_dims> const& size_or_till)
  {
    return adaptor_sub(cell.span(), SM{}, from, size_or_till);
  }
}
