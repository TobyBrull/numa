#ifndef INCLUDE_GUARD_NUMA_CELL_REFORM_UNIFIED_H_
#define INCLUDE_GUARD_NUMA_CELL_REFORM_UNIFIED_H_

#include "concepts_reform_unified.hpp"
#include "growth_strategy.hpp"
#include "reform_axis.hpp"

#include "numa/boot/bits.hpp"
#include "numa/boot/index.hpp"

namespace numa::unified {
  template<typename Target, typename... Ts>
  void resize(
      Target&& tgt,
      index<Target::sel_num_axes> const& new_size,
      Ts const&... values) requires(!std::is_reference_v<Target>);

  template<typename Target, typename... Ts>
  void resize_hard(
      Target&& tgt,
      index<Target::sel_num_axes> const& new_size,
      Ts const&... values) requires(!std::is_reference_v<Target>);

  template<typename Target>
  void reserve_hard(
      Target&& tgt,
      index<Target::sel_num_axes> const&
          new_capacity) requires(!std::is_reference_v<Target>);

  template<typename Target, typename... Ts>
  void insert_size(
      Target&& tgt,
      index<Target::sel_num_axes> const& from,
      index<Target::sel_num_axes> const& size,
      Ts const&... values) requires(!std::is_reference_v<Target>);

  template<typename Target>
  void erase_size(
      Target&& tgt,
      index<Target::sel_num_axes> const& from,
      index<Target::sel_num_axes> const&
          size) requires(!std::is_reference_v<Target>);
}

#include "reform_unified.inl"

#endif
