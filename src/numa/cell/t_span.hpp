#ifndef INCLUDE_GUARD_NUMA_CELL_TENSOR_SPAN_H_
#define INCLUDE_GUARD_NUMA_CELL_TENSOR_SPAN_H_

#include "algorithms.hpp"
#include "for_each.hpp"
#include "fwd.hpp"
#include "t_span_facade.hpp"

#include "numa/core/index_detail.hpp"
#include "numa/core/iterator_facade.hpp"

#include "numa/tmpl/axes_info.hpp"

#include <numeric>
#include <tuple>

namespace numa {
  template<typename T, index_t N>
  struct t_span : public t_span_facade<t_span<T, N>, T, N, N> {
    constexpr static auto axes_info = axes_info_tensor<N>();

    T* origin_       = nullptr;
    index<N> size_   = zero;
    index<N> stride_ = zero;

    using span_type       = t_span<std::remove_const_t<T>, N>;
    using span_const_type = t_span<T const, N>;

    constexpr t_span() = default;

    constexpr t_span(T* origin, index<N> const& size, index<N> const& stride);

    constexpr operator t_span<T const, N>() const
        requires(!is_const<T>().value);

    index<N> const& size() const
    {
      return size_;
    }

    T* get_ptr_impl(index<N> const& ind) const;
    bool is_bounded_impl(index<N> const& ind) const;
    auto iterator_impl(index<N> const& ind) const;

    auto begin() const;
    auto end() const;

    template<index_t... Axes>
    requires(is_variation<N>(index_list<Axes...>).value) auto axes_impl(
        index_list_t<Axes...>) const;

    template<index_t... Dims>
    requires(is_variation<N>(index_list<Dims...>).value) auto dims_impl(
        index_list_t<Dims...>) const;

    t_span<T, N> transposed() const requires((N % 2) == 0);

    template<index_t M, index_t... Axes>
    t_span<T, M> as_t_span(index_list_t<Axes...> = {}) const requires(
        (sizeof...(Axes) == N) && is_variation<M>(index_list<Axes...>).value);

    template<index_t... Axes>
    t_span<T, 3> as_cuboid_span(index_list_t<Axes...> = {}) const requires(
        (sizeof...(Axes) == N) && is_variation<3>(index_list<Axes...>).value);

    template<index_t... Axes>
    t_span<T, 2> as_matrix_span(index_list_t<Axes...> = {}) const requires(
        (sizeof...(Axes) == N) && is_variation<2>(index_list<Axes...>).value);

    t_span<T, 1> as_vector_span() const;
  };

  template<typename T>
  using cuboid_span = t_span<T, 3>;
  template<typename T>
  using matrix_span = t_span<T, 2>;
  template<typename T>
  using vector_span = t_span<T, 1>;
  template<typename T>
  using scalar_span = t_span<T, 0>;

  template<typename T, index_t N, typename U>
  requires((c_type<decay_t<T>> == c_type<decay_t<U>>).value) bool
  operator==(t_span<T, N> const& lhs, t_span<U, N> const& rhs);

  template<typename T, index_t N, typename U>
  requires((c_type<decay_t<T>> == c_type<decay_t<U>>).value) bool
  operator!=(t_span<T, N> const& lhs, t_span<U, N> const& rhs);

  template<typename T, index_t N, index_t... SelectedAxes>
  class t_span_axes<T, N, index_list_t<SelectedAxes...>> {
   public:
    constexpr static index_t K = sizeof...(SelectedAxes);

    static_assert((0 < K) && (K <= N));

    t_span<T, N> transposed() const requires(K == 2);

    template<index_t... Axes>
    requires(
        (sizeof...(Axes) == sizeof...(SelectedAxes)) &&
        is_permutation(index_list<Axes...>).value)
        t_span<T, N> reordered(index_list_t<Axes...> = {}) const;

    t_span<T, N> reversed() const;

    template<SubMode SM>
    t_span<T, N>
    sub(SM, index<K> const& from, index<K> const& size_or_till) const;

    template<typename... Index>
    requires constructs_index_v<sizeof...(SelectedAxes), Index...> t_span<T, N>
    strided(Index... inds) const;

    template<typename... Index>
    requires constructs_index_v<N - sizeof...(SelectedAxes), Index...> auto
    span_along(Index... inds) const;

    template<typename... Index>
    requires constructs_index_v<sizeof...(SelectedAxes), Index...> auto
    span_plumb(Index... inds) const;

    template<typename F>
    void for_each_along(F&& f) const;

    template<typename F>
    void for_each_plumb(F&& f) const;

   private:
    friend struct t_span<T, N>;

    t_span_axes(t_span<T, N> const* span);

    t_span<T, N> const* span_;
  };

  /**
   * Determines if any two of the elements in the t_span
   * refer to the same memory-object.
   */
  template<typename T, index_t N>
  constexpr bool is_aliasing(t_span<T, N> const& span);
}

#include "t_span.inl"

#endif
