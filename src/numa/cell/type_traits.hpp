#ifndef INCLUDE_GUARD_NUMA_CELL_TYPE_TRAITS_H_
#define INCLUDE_GUARD_NUMA_CELL_TYPE_TRAITS_H_

#include "misc.hpp"

#include "numa/tmpl/type_list.hpp"
#include "numa/tmpl/type_traits.hpp"

#include "concepts.hpp"

namespace numa {
  template<WeakSpan T, WeakSpan... Ts>
  constexpr auto is_same_num_dims(type_list_t<T, Ts...> = {});

  template<WeakSpan... Ts>
  constexpr auto is_same_template(type_list_t<Ts...> = {});

  template<Span T>
  constexpr auto is_structure(c_type_t<T> = {});
}

#include "type_traits.inl"

#endif
