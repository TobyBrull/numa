namespace numa {
  namespace detail {
    template<typename T>
    struct is_field_impl : public c_false_t {
    };

    template<typename Mark, typename S, typename... AxisMarks>
    struct is_field_impl<field<Mark, S, AxisMarks...>> : public c_true_t {
    };
  }

  template<typename T>
  constexpr auto is_field(c_type_t<T>)
  {
    return c_bool<detail::is_field_impl<T>::value>;
  }

  namespace detail {
    template<typename... Retvals>
    constexpr auto
    unique_axis_marks_impl(type_list_t<Retvals...> retval, type_list_t<>)
    {
      return retval;
    }

    template<typename... Retvals, typename Field, typename... Fields>
    constexpr auto unique_axis_marks_impl(
        type_list_t<Retvals...> retval, type_list_t<Field, Fields...>)
    {
      return unique_axis_marks_impl(
          unique(concat(retval, Field::axis_marks)), type_list_t<Fields...>{});
    }
  }

  template<typename... Fields>
  constexpr auto
  structure_info_type<type_list_t<Fields...>>::unique_axis_marks_impl()
  {
    return detail::unique_axis_marks_impl({}, info_list);
  }

  namespace detail {
    template<typename Retval>
    constexpr auto unique_value_type_impl(c_type_t<Retval>, type_list_t<>)
    {
      return c_type<Retval>;
    }

    template<typename Retval, typename Field, typename... Fields>
    constexpr auto
        unique_value_type_impl(c_type_t<Retval>, type_list_t<Field, Fields...>)
    {
      using S = typename Field::structure_t;

      using value_type = typename S::value_type;

      if constexpr (std::is_same_v<Retval, undefined_t>) {
        return unique_value_type_impl(
            c_type<value_type>, type_list_t<Fields...>{});
      }
      else if constexpr (std::is_same_v<Retval, value_type>) {
        return unique_value_type_impl(
            c_type<value_type>, type_list_t<Fields...>{});
      }
      else {
        return unique_value_type_impl(c_type<void>, type_list_t<Fields...>{});
      }
    }
  }

  template<typename... Fields>
  constexpr auto
  structure_info_type<type_list_t<Fields...>>::unique_value_type_impl()
  {
    return detail::unique_value_type_impl(
        c_type<undefined_t>, type_list_t<Fields...>{});
  }

  template<typename... Fields>
  template<typename AxisMark>
  requires(is_mark(c_type<AxisMark>).value) constexpr auto structure_info_type<
      type_list_t<Fields...>>::find_axes(AxisMark am)
  {
    constexpr auto retval =
        for_each_indexed(info_list, [](auto const e, auto field) {
          constexpr auto ax_ind = numa::find(field.axis_marks, AxisMark{});

          if constexpr (!is_c_null(decay(c_type_t{ax_ind}))) {
            return std::tuple(e, c_index<ax_ind.value>);
          }
        });

    static_assert(
        !is_empty(retval),
        "structure_info::find_axes: cannot find axes for given mark");

    return retval;
  }

  template<typename... Fields>
  constexpr auto structure_info_type<type_list_t<Fields...>>::vary_axes_info()
  {
    constexpr auto retval = for_each(axis_marks, [](auto am) {
      constexpr auto list_of_tuples =
          structure_info_type<type_list_t<Fields...>>::find_axes(am);

      constexpr auto list_of_vat = for_each(list_of_tuples, [&](auto const tt) {
        using F = decltype(get<std::get<0>(tt).value>(info_list));
        using S = typename F::structure_t;

        constexpr auto va = S::vary_axes_info;

        constexpr index_t loc_ax = std::get<1>(tt).value;

        return std::tuple{
            get<loc_ax>(va.mode_list), get<loc_ax>(va.value_list)};
      });

      return combine(list_of_vat);
    });

    return numa::vary_axes_info<decay_t<decltype(retval)>>{};
  }

  template<typename... Fields>
  template<index_t... Axes>
  constexpr void
  structure_info_type<type_list_t<Fields...>>::static_assert_none_is_fixed_size(
      index_list_t<Axes...> sel)
  {
    constexpr auto sel_fs =
        select_at(vary_axes_info().is_fixed_size(), index_list<Axes...>);

    static_assert(
        none_of(sel_fs), "structure_info_type: cannot select fixed-size axis");
  }

  template<typename... Fields>
  template<typename F, index_t... Axes>
  constexpr void structure_info_type<type_list_t<Fields...>>::for_each_field(
      F&& f, index_list_t<Axes...>)
  {
    for_each_indexed(info_list, [&](auto const field_ind, auto const field) {
      constexpr auto result = for_each_indexed(
          field.axis_marks, [](auto const local_axis, auto am) {
            constexpr index_t global_axis = find(axis_marks, am);

            constexpr auto sel_ext =
                find(index_list<Axes...>, c_type_t{c_index<global_axis>});

            if constexpr (!is_c_null(decay(c_type_t{sel_ext}))) {
              return std::tuple(sel_ext, local_axis, c_index<global_axis>);
            }
          });

      if constexpr (numa::size(result) > 0) {
        f(field_ind,
          for_each(result, f_get<0>),
          for_each(result, f_get<1>),
          for_each(result, f_get<2>));
      }
    });
  }

  template<typename... Fields>
  template<typename F, typename VaryAxesInfoPredicate, typename... Initers>
  constexpr void structure_info_type<type_list_t<Fields...>>::
      for_each_field_contrary_recursive(
          F&& f, VaryAxesInfoPredicate p, Initers const&... initers)
  {
    constexpr auto global_vai = vary_axes_info();

    for_each_field(
        [&](auto const field_ind,
            auto const,
            auto const local_axes,
            auto const global_axes) {
          constexpr auto local_vai =
              structure_t<field_ind.value>::vary_axes_info;

          auto const keep = select_at(logical_not(p(local_vai)), local_axes);

          if constexpr (count_if(keep) > 0_i) {
            constexpr auto local_sub_axes  = select_if(local_axes, keep);
            constexpr auto global_sub_axes = select_if(global_axes, keep);

            constexpr auto sub_value_list =
                select_at(global_vai.value_list, global_sub_axes);

            using Field = field_t<field_ind.value>;

            recursive_initializer_for<Field>(
                [&f, field_ind, local_sub_axes, sub_value_list](
                    auto const&... initers_or_args) {
                  f(field_ind,
                    local_sub_axes,
                    sub_value_list,
                    initers_or_args...);
                },
                initers...);
          }
        },
        select_if(index_list_iota<num_axes>(), p(global_vai)));
  }

  template<typename Field, typename F, typename... InitersOrArgs>
  requires(is_field<Field>().value) auto recursive_initializer_for(
      F&& f, InitersOrArgs const&... initers_or_args)
  {
    using structure_t = typename Field::structure_t;

    if constexpr (WeakCell<structure_t>) {
      using field_mark_t = typename Field::mark_t;
      using value_type_t = typename structure_t::value_type;

      return std::apply(
          f, initializer_for<field_mark_t, value_type_t>(initers_or_args...));
    }
    else {
      return f(initers_or_args...);
    }
  }

  template<typename Field, typename F, typename... InitersOrArgs>
  requires(is_field<Field>().value) auto recursive_initializer_for_tuple(
      F&& f, std::tuple<InitersOrArgs const&...> initers_or_args_tuple)
  {
    return std::apply(
        [&f](auto const&... initers_or_args) {
          recursive_initializer_for<Field>(f, initers_or_args...);
        },
        initers_or_args_tuple);
  }
}
