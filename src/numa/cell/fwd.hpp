#ifndef INCLUDE_GUARD_NUMA_CELL_FWD_H_
#define INCLUDE_GUARD_NUMA_CELL_FWD_H_

#include "numa/boot/fwd.hpp"
#include "numa/core/concepts.hpp"

namespace numa {
  struct translocation_forbidden_t {
  };

  /*
   * General
   */
  template<typename Subclass, typename C, typename SelectedAxes>
  class cell_axes_facade_light;

  template<typename Subclass, typename C, typename SelectedAxes>
  class cell_axes_facade;

  template<typename Subclass, typename C, typename SelectedAxes>
  class cell_axes_facade_translocation;

  /**
   * Related to tensor.
   */
  template<typename T, index_t N, Allocator Alloc>
  class tensor;

  template<typename T, index_t N, Allocator Alloc, typename SelectedAxes>
  class tensor_axes;

  template<typename T, index_t... Sizes>
  class tensor_fixed_size;

  template<typename T, index_t... Sizes>
  class tensor_fixed_size_iterator;

  template<typename T, typename SelectedAxes, index_t... Sizes>
  class tensor_fixed_size_axes;

  template<typename T, index_t... Sizes>
  class tensor_fixed_capacity;

  template<typename T, index_t... Sizes>
  class tensor_fixed_capacity_iterator;

  template<typename T, typename SelectedAxes, index_t... Sizes>
  class tensor_fixed_capacity_axes;

  template<typename T, index_t N>
  class tensor_uniform;

  template<typename T, index_t N, typename SelectedAxes>
  class tensor_uniform_axes;

  template<typename T, index_t N>
  class tensor_unvaried;

  template<typename T, index_t N, typename SelectedAxes>
  class tensor_unvaried_axes;

  template<typename T, index_t N>
  struct t_span;

  template<typename T, index_t N, typename SelectedAxes>
  class t_span_axes;

  template<typename T, index_t N>
  class t_span_iterator;

  /**
   * Related to tensor_staged.
   */
  template<typename T, index_t Nd, typename Ls, typename Rs>
  class tensor_staged_unvaried;

  template<typename T, index_t Nd, typename Ls, typename Rs, Allocator Alloc>
  class tensor_staged;

  template<
      typename T,
      index_t Nd,
      typename Ls,
      typename Rs,
      Allocator Alloc,
      typename SelectedAxes>
  class tensor_staged_axes;

  template<typename T, index_t Nd, typename Ls, typename Rs>
  struct t_span_staged;

  template<
      typename T,
      index_t Nd,
      typename Ls,
      typename Rs,
      typename SelectedAxes>
  class t_span_staged_axes;

  template<
      typename T,
      index_t Nd,
      typename Ls,
      typename Rs,
      typename SelectedDims>
  class t_span_staged_dims;

  template<typename T, index_t Nd, typename Ls, typename Rs>
  class tensor_staged_iterator;

  template<typename T, index_t Nd, typename Ls, typename Rs>
  struct t_span_staged_form;

  template<typename T, index_t Nd, typename Ls, typename Rs>
  class t_span_staged_form_iterator;

  /**
   * Related to lin_space_....
   */
  template<typename T>
  struct vector_linear_incr_unvaried;

  template<typename T>
  class vector_linear_incr;

  template<typename T, typename SelectedAxes>
  class vector_linear_incr_axes;

  template<typename T>
  class vector_linear_incr_iterator;

  /**
   * Related to tensor_sparse.
   */
  template<typename T, index_t N, typename X, Allocator Alloc>
  class tensor_sparse;

  template<typename T, index_t N, typename X>
  class tensor_sparse_iterator;

  template<
      typename Subclass,
      typename T,
      index_t N,
      typename X,
      Allocator Alloc,
      typename SelectedAxes>
  class tensor_sparse_axes;

  /**
   * Related to structure
   */
  template<typename... Fields>
  class structure_unvaried;

  template<typename... Fields>
  class structure;

  template<typename SelectedAxes, typename... Fields>
  class structure_axes;

  template<typename FieldList>
  class structure_info_type;

  template<typename Subclass, typename... Fields>
  struct structure_vary_axes_facade;

  /*
   * for-each related
   */
  template<typename... Ts>
  struct for_each_bundle_impl;

  /*
   * span-to-cell related
   */
  template<typename T>
  struct span_to_cell {
    using type = undefined_t;
  };

  template<typename T>
  using span_to_cell_t = typename span_to_cell<T>::type;

  /*
   * The adaptors.
   */
  template<typename S, typename LowerBound, typename UpperBound>
  struct t_span_adaptor_band;

  template<
      typename S,
      index_t Nd,
      typename LowerBound,
      typename UpperBound,
      typename Iter>
  struct t_span_adaptor_band_iterator;
}

#endif
