namespace numa {
  /*
   * Span based.
   */
  template<Cell C, typename... Ts>
  template<typename F>
  C make_cell_span_bundle<C, Ts...>::operator()(F&& f) const
  {
    C retval;

    std::apply(
        [&](auto const& span, auto const&... spans) {
          using T = decay_t<std::tuple_element_t<0, std::tuple<Ts...>>>;

          auto const form = T::axes_info.size_to_form(span.size());
          auto const size = C::axes_info.form_to_size(form);

          detail::initial_resize_is_not_fixed(retval, size, size);

          for_each(retval.unvaried(), span, spans...)(
              [&f](auto& val, auto const&... vals) { val = f(vals...); });
        },
        spans_tuple_);

    return retval;
  }

  template<Cell C, typename... Ts>
  template<typename F>
  C make_cell_span_bundle<C, Ts...>::indexed(F&& f) const
  {
    C retval;

    std::apply(
        [&](auto const& span, auto const&... spans) {
          auto const size = span.size();

          detail::initial_resize_is_not_fixed(retval, size, size);

          for_each(retval.unvaried(), span, spans...)
              .indexed([&f](auto const& ind, auto& val, auto const&... vals) {
                val = f(ind, vals...);
              });
        },
        spans_tuple_);

    return retval;
  }

  template<typename T, typename... Ts>
  requires(Span<decay_t<T>> && (WeakSpan<decay_t<Ts>> && ...)) auto make_cell(
      T&& span, Ts&&... spans)
  {
    return make_cell_span_bundle<
        span_to_cell_t<detail::map_bundle_type_to_impl_t<T>>,
        T,
        Ts...>{{std::forward<T>(span), std::forward<Ts>(spans)...}};
  }

  template<Cell C, typename... Ts>
  requires(WeakSpan<decay_t<Ts>>&&...) auto make_cell(Ts&&... spans)
  {
    return make_cell_span_bundle<C, Ts...>{{std::forward<Ts>(spans)...}};
  }

  /*
   * Index based.
   */
  template<Cell C>
  make_cell_index_bundle<C>::make_cell_index_bundle(
      index<make_cell_index_bundle<C>::Na> const& size)
    : size_(size)
  {
  }

  template<Cell C>
  template<typename F>
  C make_cell_index_bundle<C>::operator()(F&& f)
  {
    C retval;

    detail::initial_resize_is_not_fixed(retval, size_, size_);

    for_each(retval.unvaried())(
        [&f](auto& val, auto const&... vals) { val = f(vals...); });

    return retval;
  }

  template<Cell C, typename... Index>
  requires constructs_index_v<C::axes_info.num_axes, Index...> auto
  make_cell(Index... inds)
  {
    index<C::axes_info.num_axes> const ind(inds...);

    return make_cell_index_bundle<C>(ind);
  }

  /*
   * Others.
   */
  template<typename T>
  requires Span<decay_t<T>> auto make_copy(T&& span)
  {
    return make_cell(std::forward<T>(span))(
        [](auto const& val) { return val; });
  }

  template<Cell C, typename U>
  requires Span<decay_t<U>> C make_copy(U&& span)
  {
    return make_cell<C>(std::forward<U>(span))(
        [](auto const& val) { return val; });
  }

  template<typename T>
  requires Span<decay_t<T>> auto make_move(T&& span)
  {
    return make_cell(std::forward<T>(span))(
        [](auto&& val) { return std::move(val); });
  }

  template<Cell C, typename U>
  requires Span<decay_t<U>> C make_move(U&& span)
  {
    return make_cell<C>(std::forward<U>(span))(
        [](auto&& val) { return std::move(val); });
  }
}
