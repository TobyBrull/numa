namespace numa {
  /*
   * span-facade-light
   */
  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<typename... Index>
  requires constructs_index_v<Nd, Index...> decltype(auto)
  t_span_facade<Subclass, T, Na, Nd>::operator()(Index... inds) const
  {
    return span_subclass().operator[](index<Nd>(inds...));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<typename... Index>
  requires constructs_index_v<Nd, Index...> decltype(auto)
  t_span_facade<Subclass, T, Na, Nd>::at(Index... inds) const
  {
    index<Nd> const ind(inds...);

    BOUNDS_CHECK(span_subclass().is_bounded_impl(ind));

    return span_subclass().operator[](ind);
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<typename... Index>
  requires constructs_index_v<Nd, Index...> bool
  t_span_facade<Subclass, T, Na, Nd>::is_bounded(Index... inds) const
  {
    return span_subclass().is_bounded_impl(index<Nd>(inds...));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  Subclass& t_span_facade<Subclass, T, Na, Nd>::span_subclass()
  {
    return static_cast<Subclass&>(*this);
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  Subclass const& t_span_facade<Subclass, T, Na, Nd>::span_subclass() const
  {
    return static_cast<Subclass const&>(*this);
  }

  /*
   * span-facade
   */
  template<typename Subclass, typename T, index_t Na, index_t Nd>
  T& t_span_facade<Subclass, T, Na, Nd>::operator[](index<Nd> const& ind) const
  {
    return *(this->span_subclass().get_ptr_impl(ind));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<typename... Index>
  requires constructs_index_v<Nd, Index...> T*
  t_span_facade<Subclass, T, Na, Nd>::get_ptr(Index... inds) const
  {
    return this->span_subclass().get_ptr_impl(index<Nd>(inds...));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<typename... Index>
  requires constructs_index_v<Nd, Index...> T*
  t_span_facade<Subclass, T, Na, Nd>::get_ptr_checked(Index... inds) const
  {
    index<Nd> const ind(inds...);

    BOUNDS_CHECK(this->span_subclass().is_bounded_impl(ind));

    return this->span_subclass().get_ptr_impl(ind);
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<typename... Index>
  requires constructs_index_v<Nd, Index...> auto
  t_span_facade<Subclass, T, Na, Nd>::iterator(Index... inds) const
  {
    return this->span_subclass().iterator_impl(index<Nd>(inds...));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  auto t_span_facade<Subclass, T, Na, Nd>::axes() const
  {
    return this->span_subclass().axes_impl(index_list_iota<Na>());
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<index_t... SelAxes>
  requires(is_variation<Na>(index_list<SelAxes...>)
               .value) auto t_span_facade<Subclass, T, Na, Nd>::axes() const
  {
    return this->span_subclass().axes_impl(index_list<SelAxes...>);
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<index_t... SelAxes>
  requires(is_variation<Na>(index_list<SelAxes...>)
               .value) auto t_span_facade<Subclass, T, Na, Nd>::
      axes(c_index_t<SelAxes>... sel_axes) const
  {
    return this->span_subclass().axes_impl(index_list<SelAxes...>);
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<index_t... SelAxes>
  requires(is_variation<Na>(index_list<SelAxes...>)
               .value) auto t_span_facade<Subclass, T, Na, Nd>::
      axes(index_list_t<SelAxes...> el) const
  {
    return this->span_subclass().axes_impl(el);
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  auto t_span_facade<Subclass, T, Na, Nd>::dims() const
  {
    return this->span_subclass().dims_impl(index_list_iota<Nd>());
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<index_t... SelDims>
  requires(is_variation<Nd>(index_list<SelDims...>)
               .value) auto t_span_facade<Subclass, T, Na, Nd>::dims() const
  {
    return this->span_subclass().dims_impl(index_list<SelDims...>);
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<index_t... SelDims>
  requires(is_variation<Nd>(index_list<SelDims...>)
               .value) auto t_span_facade<Subclass, T, Na, Nd>::
      dims(c_index_t<SelDims>... sel_dims) const
  {
    return this->span_subclass().dims_impl(type_list_t{sel_dims...});
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<index_t... SelDims>
  requires(is_variation<Nd>(index_list<SelDims...>)
               .value) auto t_span_facade<Subclass, T, Na, Nd>::
      dims(index_list_t<SelDims...> el) const
  {
    return this->span_subclass().dims_impl(el);
  }

  /*
   * Implementation form-span-facade-iterator
   */
  template<typename FormSpanType, typename SpanIterType, typename T, index_t Nd>
  class t_span_form_facade_iterator
    : public iterator_facade<
          t_span_form_facade_iterator<FormSpanType, SpanIterType, T, Nd>,
          T> {
   public:
    using this_iterator_facade = iterator_facade<
        t_span_form_facade_iterator<FormSpanType, SpanIterType, T, Nd>,
        T>;

    t_span_form_facade_iterator(
        FormSpanType const* form_span, SpanIterType iter)
      : form_span_(form_span), iter_(iter)
    {
      advance_to_valid();
    }

    index<Nd> pos() const
    {
      return iter_.pos();
    }

    void increment()
    {
      ++iter_;

      advance_to_valid();
    }

    bool equal_to(t_span_form_facade_iterator const& other) const
    {
      return (iter_ == other.iter_);
    }

    decltype(auto) dereference() const
    {
      return (*iter_);
    }

   private:
    void advance_to_valid()
    {
      while (iter_ != form_span_->outer_span.end()) {
        if (form_span_->is_bounded(iter_.pos() - form_span_->base)) {
          break;
        }

        ++iter_;
      }
    }

    FormSpanType const* form_span_;

    SpanIterType iter_;
  };

  /*
   * Implementation of form-span-facade
   */
  template<typename Subclass, typename OuterSpanType, typename T, index_t Nd>
  constexpr t_span_form_facade<Subclass, OuterSpanType, T, Nd>::
      t_span_form_facade(
          OuterSpanType const& outer_span,
          index<Nd> const& base,
          index<Nd> const& till)
    : outer_span(outer_span), base(base), till(till)
  {
  }

  template<typename Subclass, typename OuterSpanType, typename T, index_t Nd>
  constexpr index<Nd>
  t_span_form_facade<Subclass, OuterSpanType, T, Nd>::size() const
  {
    return (till - base);
  }

  template<typename Subclass, typename OuterSpanType, typename T, index_t Nd>
  T* t_span_form_facade<Subclass, OuterSpanType, T, Nd>::get_ptr_impl(
      index<Nd> const& ind) const
  {
    return outer_span.get_ptr_impl(base + ind);
  }

  template<typename Subclass, typename OuterSpanType, typename T, index_t Nd>
  bool t_span_form_facade<Subclass, OuterSpanType, T, Nd>::is_bounded_impl(
      index<Nd> const& ind) const
  {
    return static_cast<Subclass const*>(this)->is_bounded_impl(ind);
  }

  template<typename Subclass, typename OuterSpanType, typename T, index_t Nd>
  auto t_span_form_facade<Subclass, OuterSpanType, T, Nd>::iterator_impl(
      index<Nd> const& ind) const
  {
    return t_span_form_facade_iterator<
        Subclass,
        typename OuterSpanType::iterator,
        T,
        Nd>(
        static_cast<Subclass const*>(this),
        outer_span.iterator_impl(base + ind));
  }

  template<typename Subclass, typename OuterSpanType, typename T, index_t Nd>
  auto t_span_form_facade<Subclass, OuterSpanType, T, Nd>::begin() const
  {
    return t_span_form_facade_iterator<
        Subclass,
        decltype(outer_span.begin()),
        T,
        Nd>(static_cast<Subclass const*>(this), outer_span.begin());
  }

  template<typename Subclass, typename OuterSpanType, typename T, index_t Nd>
  auto t_span_form_facade<Subclass, OuterSpanType, T, Nd>::end() const
  {
    return t_span_form_facade_iterator<
        Subclass,
        decltype(outer_span.end()),
        T,
        Nd>(static_cast<Subclass const*>(this), outer_span.end());
  }
}
