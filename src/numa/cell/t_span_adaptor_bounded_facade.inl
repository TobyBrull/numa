namespace numa {
  template<typename Raster, typename Iter>
  class t_span_adaptor_bounded_facade_iterator
    : public iterator_facade<
          t_span_adaptor_bounded_facade_iterator<Raster, Iter>,
          value_type_t<Iter>> {
   public:
    t_span_adaptor_bounded_facade_iterator(
        Iter const& iter,
        Iter const& begin,
        Iter const& end,
        Raster const& raster)
      : iter_(iter), begin_(begin), end_(end), raster_(raster)
    {
      ASSERT((iter_ == end_) || (raster_.is_bounded(iter_.pos())));
    }

    auto ptr() const
    {
      return iter_.ptr();
    }

    decltype(auto) pos() const
    {
      return iter_.pos();
    }

    void increment()
    {
      do {
        ++iter_;
      } while ((iter_ != end_) && !raster_.is_bounded(iter_.pos()));
    }

    void decrement()
    {
      do {
        --iter_;
      } while ((iter_ != begin_) && !raster_.is_bounded(iter_.pos()));
    }

    auto equal_to(t_span_adaptor_bounded_facade_iterator const& other) const
    {
      return iter_.equal_to(other.iter_);
    }

    decltype(auto) dereference() const
    {
      return iter_.dereference();
    }

   private:
    Iter iter_;
    Iter begin_;
    Iter end_;
    Raster raster_;
  };

  template<Span S, typename Raster>
  t_span_adaptor_bounded_facade<S, Raster>::t_span_adaptor_bounded_facade(
      S const& span, Raster const& raster)
    : span_(span), raster_(raster)
  {
  }

  template<Span S, typename Raster>
  auto t_span_adaptor_bounded_facade<S, Raster>::size() const -> index<Nd>
  {
    return S::axes_info.size_to_form(span_.size());
  }

  template<Span S, typename Raster>
  auto t_span_adaptor_bounded_facade<S, Raster>::get_ptr_impl(
      index<Nd> const& ind) const -> element_type*
  {
    return span_.get_ptr(ind);
  }

  template<Span S, typename Raster>
  bool t_span_adaptor_bounded_facade<S, Raster>::is_bounded_impl(
      index<Nd> const& ind) const
  {
    return span_.is_bounded(ind) && raster_.is_bounded(ind);
  }

  template<Span S, typename Raster>
  auto t_span_adaptor_bounded_facade<S, Raster>::iterator_impl(
      index<Nd> const& ind) const
  {
    ASSERT((raster_.is_bounded(ind)));

    return t_span_adaptor_bounded_facade_iterator<Raster, inner_iterator_type>{
        span_.iterator(ind), span_.begin(), span_.end(), raster_};
  }

  template<Span S, typename Raster>
  auto t_span_adaptor_bounded_facade<S, Raster>::begin() const
  {
    auto it     = span_.begin();
    auto end_it = span_.end();

    while ((it != end_it) && !raster_.is_bounded(it.pos())) {
      ++it;
    }

    return t_span_adaptor_bounded_facade_iterator<Raster, inner_iterator_type>{
        std::move(it), span_.begin(), std::move(end_it), raster_};
  }

  template<Span S, typename Raster>
  auto t_span_adaptor_bounded_facade<S, Raster>::end() const
  {
    return t_span_adaptor_bounded_facade_iterator<Raster, inner_iterator_type>{
        span_.end(), span_.begin(), span_.end(), raster_};
  }
}
