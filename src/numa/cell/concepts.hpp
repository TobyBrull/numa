#ifndef INCLUDE_GUARD_NUMA_CELL_CONCEPTS_H_
#define INCLUDE_GUARD_NUMA_CELL_CONCEPTS_H_

#include "numa/boot/index.hpp"
#include "numa/core/concepts.hpp"
#include "numa/core/functors.hpp"
#include "numa/core/memory.hpp"
#include "numa/tmpl/axes_info.hpp"
#include "numa/tmpl/type_traits.hpp"

#include "numa/cell/concepts_reform_recursive.hpp"

namespace numa {
  /*
   * allocator
   */
  template<typename T>
  concept HasAllocator = requires(T obj)
  {
    {
      obj.allocator()
    }
    ->Allocator;
  };

  template<typename T>
  using allocator_t = std::conditional_t<
      HasAllocator<T>,
      decay_t<decltype(std::declval<T>().allocator())>,
      void>;

  template<typename T>
  constexpr inline bool is_trivial_allocator_v =
      (std::is_same_v<allocator_t<T>, void> ||
       std::is_same_v<allocator_t<T>, null_allocator>);

  /*
   * weak-span and span
   */
  template<typename T>
  concept WeakSpan = requires(
      T weak_span,
      typename T::value_type value,
      typename T::element_type element)
  {
    {T::axes_info};

    requires is_axes_info(decay(c_type_t{T::axes_info})).value;

    {
      weak_span.size()
    }
    ->std::convertible_to<index<T::axes_info.num_axes>>;
  };

  template<typename T>
  concept Span = requires(
      T span, typename T::value_type value, typename T::element_type element)
  {
    requires WeakSpan<T>;

    {
      span.at(iota)
    }
    ->std::convertible_to<typename T::element_type const&>;

    {
      span[index<T::axes_info.num_dims>(zero)]
    }
    ->std::convertible_to<typename T::element_type const&>;
  };

  /*
   * weak-cell and cell
   */
  template<typename T>
  concept WeakCell = requires(T weak_cell, typename T::value_type value)
  {
    requires !std::is_copy_constructible_v<T> || !HasAllocator<T>;
    requires std::is_move_constructible_v<T>;

    requires !HasAllocator<T> ||
        (std::is_nothrow_default_constructible_v<T> &&
         std::is_nothrow_move_constructible_v<T> &&
         std::is_nothrow_move_assignable_v<T>);

    requires !HasAllocator<T> ||
        (
            //          !std::is_constructible_v<typename T::unvaried_type> &&
            //          // GCC bug
            !std::is_copy_constructible_v<typename T::unvaried_type> &&
            !std::is_move_constructible_v<typename T::unvaried_type>);

    requires WeakSpan<T>;

    {T::vary_axes_info};

    requires is_vary_axes_info(decay(c_type_t{T::vary_axes_info})).value;

    {
      weak_cell.size()
    }
    ->std::convertible_to<index<T::axes_info.num_axes>>;
    {
      weak_cell.capacity()
    }
    ->std::convertible_to<index<T::axes_info.num_axes>>;

    {
      weak_cell.unvaried()
    }
    ->std::same_as<typename T::unvaried_type&>;
    {
      weak_cell.unvaried_const()
    }
    ->std::same_as<typename T::unvaried_type const&>;

    requires WeakSpan<typename T::unvaried_type>;
  };

  template<typename T>
  concept Cell = requires(T cell)
  {
    requires WeakCell<T>;

    requires Span<T>;

    requires(
        c_type<index<T::axes_info.num_axes>> == decay<decltype(cell.size())>())
        .value;

    requires Span<typename T::unvaried_type>;

    {
      T::required_elements(cell.size())
    }
    ->std::convertible_to<index_t>;
    {
      T::required_elements(cell.capacity())
    }
    ->std::convertible_to<index_t>;

    {
      cell.vary_axes()
    }
    ->recursive::CellReformTarget;
  };
}

#endif
