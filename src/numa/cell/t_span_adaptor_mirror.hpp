#ifndef INCLUDE_GUARD_NUMA_CELL_T_SPAN_ADAPTOR_MIRROR_H_
#define INCLUDE_GUARD_NUMA_CELL_T_SPAN_ADAPTOR_MIRROR_H_

#include "fwd.hpp"

#include "for_each.hpp"
#include "misc.hpp"
#include "t_span_facade.hpp"
#include "type_traits.hpp"

namespace numa {
  template<Span S, side Side, typename F>
  struct t_span_adaptor_mirror
    : public t_span_facade<
          t_span_adaptor_mirror<S, Side, F>,
          element_type_t<S>,
          S::axes_info.num_dims / 2,
          S::axes_info.num_dims> {
   public:
    constexpr static index_t Nd = S::axes_info.num_dims;

    static_assert(
        Nd % 2 == 0,
        "t_span_adaptor_mirror: "
        "needs span with even number of dimensions");

    constexpr static index_t Na = Nd / 2;

    constexpr static auto axes_info = axes_info_mirror_tensor<Na>();

    using element_type = element_type_t<S>;
    using value_type   = value_type_t<S>;

    t_span_adaptor_mirror() = default;

    t_span_adaptor_mirror(S const& span, F f = {});

    index<Na> size() const;

    element_type* get_ptr_impl(index<Nd> const& ind) const;
    bool is_bounded_impl(index<Nd> const& ind) const;
    auto iterator_impl(index<Nd> const& ind) const;

    value_type operator[](index<Nd> const& ind) const;

    auto begin() const;
    auto end() const;

   private:
    /*
     * Private so that the span_ always has mirrored size.
     */
    S span_;
    F f_;

    using inner_iterator_type = decltype(span_.begin());
  };

  /*
   * For spans.
   */
  template<Span S>
  requires(!Cell<S>) auto upper_symmetric(S const& span);

  template<Span S>
  requires(!Cell<S>) auto lower_symmetric(S const& span);

  template<Span S>
  requires(!Cell<S>) auto upper_hermitian(S const& span);

  template<Span S>
  requires(!Cell<S>) auto lower_hermitian(S const& span);

  template<typename F, Span S>
  requires(!Cell<S>) auto upper_mirror(S const& span, F f = {});

  template<typename F, Span S>
  requires(!Cell<S>) auto lower_mirror(S const& span, F f = {});

  /*
   * For const cells.
   */
  template<Cell C>
  auto upper_symmetric(C const& cell);

  template<Cell C>
  auto lower_symmetric(C const& cell);

  template<Cell C>
  auto upper_hermitian(C const& cell);

  template<Cell C>
  auto lower_hermitian(C const& cell);

  template<typename F, Cell C>
  auto upper_mirror(C const& cell, F f = {});

  template<typename F, Cell C>
  auto lower_mirror(C const& cell, F f = {});
}

#include "t_span_adaptor_mirror.inl"

#endif
