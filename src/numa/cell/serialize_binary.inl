namespace numa {
  template<Span T>
  void to_binary(std::ostream& os, T const& span)
  {
    to_binary(os, span.size());

    for_each(span)([&](auto const& x) { to_binary(os, x); });
  }

  template<Cell T>
  requires(!Span<T>) void to_binary(std::ostream& os, T const& cell)
  {
    to_binary(os, cell.unvaried());
  }

  template<typename T>
  requires Span<decay_t<T>> void from_binary(std::istream& is, T&& span)
  {
    constexpr index_t N = decay_t<T>::axes_info.num_axes;

    auto const size = make_from_binary<index<N>>(is);

    THROW_IF_NOT(
        size == span.size(),
        "numa::from_binary: trying to read into span of wrong size");

    for_each(span)([&](auto& x) { from_binary(is, x); });
  }

  template<Cell T>
  void from_binary(std::istream& is, T& cell)
  {
    from_json(is, cell.unvaried());
  }

  template<Cell T>
  T make_from_binary_impl(c_type_t<T>, std::istream& is)
  {
    constexpr index_t N = decay_t<T>::axes_info.num_axes;

    auto const size = make_from_binary<index<N>>(is);

    auto retval = make_cell<T>(size)();

    for_each(retval)([&](auto& x) { from_binary(is, x); });

    return retval;
  }
}
