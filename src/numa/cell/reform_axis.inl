namespace numa::detail {
  enum class combine_mode { none = 0, move = 1, fill = 2 };

  enum class combine_type { none = 0, construct = 1, destruct = 2, never = 3 };

  constexpr inline combine_mode
  combine(combine_mode const lhs, combine_mode const rhs)
  {
    return std::max(lhs, rhs);
  }

  constexpr inline combine_type
  combine(combine_type const lhs, combine_type const rhs)
  {
    if ((lhs == combine_type::never) || (rhs == combine_type::never)) {
      return combine_type::never;
    }

    if (lhs == combine_type::none) {
      return rhs;
    }

    if (rhs == combine_type::none) {
      return lhs;
    }

    if (lhs != rhs) {
      return combine_type::never;
    }

    ASSERT(
        (lhs == rhs) &&
        ((lhs == combine_type::construct) || (lhs == combine_type::destruct)));

    return lhs;
  }

  constexpr inline std::pair<combine_mode, combine_type>
  decompose(op_mode const om)
  {
    switch (om) {
      case op_mode::none:
        return {combine_mode::none, combine_type::none};

      case op_mode::move_assign:
        return {combine_mode::move, combine_type::none};

      case op_mode::move_construct:
        return {combine_mode::move, combine_type::construct};

      case op_mode::fill_assign:
        return {combine_mode::fill, combine_type::none};

      case op_mode::fill_construct:
        return {combine_mode::fill, combine_type::construct};

      case op_mode::destruct:
        return {combine_mode::none, combine_type::destruct};

      case op_mode::never:
        return {combine_mode::none, combine_type::never};

      default:
        UNREACHABLE();
    }
  }

  constexpr inline op_mode
  recompose(combine_mode const cm, combine_type const ct)
  {
    switch (ct) {
      case combine_type::none: {
        switch (cm) {
          case combine_mode::none:
            return op_mode::none;

          case combine_mode::move:
            return op_mode::move_assign;

          case combine_mode::fill:
            return op_mode::fill_assign;
        }
      }

      case combine_type::construct: {
        switch (cm) {
          case combine_mode::none:
            UNREACHABLE();

          case combine_mode::move:
            return op_mode::move_construct;

          case combine_mode::fill:
            return op_mode::fill_construct;
        }
      }

      case combine_type::destruct:
        return op_mode::destruct;

      case combine_type::never:
        return op_mode::never;

      default:
        UNREACHABLE();
    }
  }

  constexpr inline op_mode combine(op_mode const lhs, op_mode const rhs)
  {
    auto const [l_mode, l_type] = decompose(lhs);
    auto const [r_mode, r_type] = decompose(rhs);

    return recompose(combine(l_mode, r_mode), combine(l_type, r_type));
  }

  /*
   * inplace reforms
   */
  inline void inplace_trivial::operator()(AxisTarget auto& tgt) const
  {
    for (index_t i = 0; i < old_size; ++i) {
      tgt.apply(op_mode::none, i, i);
    }
  }

  inline void inplace_resize::operator()(AxisTarget auto& tgt) const
  {
    ASSERT(0 <= new_size);

    if (new_size < old_size) {
      for (index_t i = 0; i < new_size; ++i) {
        tgt.apply(op_mode::none, i, i);
      }

      for (index_t i = new_size; i < old_size; ++i) {
        tgt.apply(op_mode::destruct, i, i);
      }
    }
    else if (old_size <= new_size) {
      for (index_t i = 0; i < old_size; ++i) {
        tgt.apply(op_mode::none, i, i);
      }

      for (index_t i = old_size; i < new_size; ++i) {
        tgt.apply(op_mode::fill_construct, i, i);
      }
    }
  }

  inline void inplace_insert_size::operator()(AxisTarget auto& tgt) const
  {
    ASSERT((0 <= base) && (base <= old_size));
    ASSERT(0 <= size);

    for (index_t i = 0; i < base; ++i) {
      tgt.apply(op_mode::none, i, i);
    }

    for (index_t i = (old_size - 1); i >= base; --i) {
      index_t const from = i;
      index_t const to   = (i + size);

      if (to < old_size) {
        tgt.apply(op_mode::move_assign, from, to);
      }
      else {
        tgt.apply(op_mode::move_construct, from, to);
      }
    }

    for (index_t i = base; i < (base + size); ++i) {
      if (i < old_size) {
        tgt.apply(op_mode::fill_assign, i, i);
      }
      else {
        tgt.apply(op_mode::fill_construct, i, i);
      }
    }
  }

  inline void inplace_erase_size::operator()(AxisTarget auto& tgt) const
  {
    ASSERT((0 <= base) && ((base + size) <= old_size));
    ASSERT(0 <= size);

    for (index_t i = 0; i < base; ++i) {
      tgt.apply(op_mode::none, i, i);
    }

    for (index_t i = (base + size); i < old_size; ++i) {
      tgt.apply(op_mode::move_assign, i, i - size);
    }

    for (index_t i = (old_size - size); i < old_size; ++i) {
      tgt.apply(op_mode::destruct, i, i);
    }
  }

  /*
   * Translate functions.
   */
  inline void translate_trivial::operator()(AxisTarget auto& tgt) const
  {
    for (index_t i = 0; i < old_size; ++i) {
      tgt.apply(op_mode::move_construct, i, i);
    }
  }

  inline void translate_resize::operator()(AxisTarget auto& tgt) const
  {
    ASSERT(0 <= new_size);

    index_t const size_1 = std::min(new_size, old_size);

    for (index_t i = 0; i < size_1; ++i) {
      tgt.apply(op_mode::move_construct, i, i);
    }

    for (index_t i = size_1; i < new_size; ++i) {
      tgt.apply(op_mode::fill_construct, i, i);
    }
  }

  inline void translate_insert_size::operator()(AxisTarget auto& tgt) const
  {
    ASSERT((0 <= base) && (base <= old_size));
    ASSERT(0 <= size);

    for (index_t i = 0; i < base; ++i) {
      tgt.apply(op_mode::move_construct, i, i);
    }

    for (index_t i = base; i < (base + size); ++i) {
      tgt.apply(op_mode::fill_construct, i, i);
    }

    for (index_t i = base; i < old_size; ++i) {
      tgt.apply(op_mode::move_construct, i, i + size);
    }
  }

  inline void translate_erase_size::operator()(AxisTarget auto& tgt) const
  {
    ASSERT((0 <= base) && ((base + size) <= old_size));
    ASSERT(0 <= size);

    for (index_t i = 0; i < base; ++i) {
      tgt.apply(op_mode::move_construct, i, i);
    }

    for (index_t i = (base + size); i < old_size; ++i) {
      index_t const j = (i - size);

      tgt.apply(op_mode::move_construct, i, j);
    }
  }

  template<index_t N, Span S, typename... Args>
  void execute_op_mode(
      op_mode const mode,
      index<N> const& from,
      index<N> const& to,
      S& first,
      S& second,
      Args const&... args)
  {
    switch (mode) {
      case op_mode::fill_assign:
        numa::fill_assign(second(to), args...);
        break;

      case op_mode::fill_construct:
        numa::fill_construct(second(to), args...);
        break;

      case op_mode::move_assign:
        numa::move_assign(second(to), first(from));
        break;

      case op_mode::move_construct:
        numa::move_construct(second(to), first(from));
        break;

      case op_mode::destruct:
        numa::destruct(second(to));
        break;

      case op_mode::none:
      case op_mode::never:
        break;
    }
  }

  template<typename F, typename TupleLike>
  struct axis_target_applier {
    constexpr static index_t N = std::tuple_size_v<TupleLike>;
    static_assert(N > 0);

    F f;
    TupleLike const& tuple_fs;
    index<N> from = zero;
    index<N> to   = zero;

    axis_target_applier(F f, TupleLike const& tuple_fs)
      : f(f), tuple_fs(tuple_fs)
    {
    }

    template<index_t I = N - 1>
    void execute(
        c_index_t<I> = {}, detail::op_mode const tm = detail::op_mode::none)
    {
      target<I> tgt{this, tm};

      std::get<I>(tuple_fs)(tgt);
    }

    template<index_t I>
    struct target {
      axis_target_applier* s; // s for super-class
      detail::op_mode tm;

      void
      apply(detail::op_mode const mode, index_t const src, index_t const tgt)
      {
        s->from[I] = src;
        s->to[I]   = tgt;

        auto const new_tm = combine(tm, mode);

        if constexpr (I == 0) {
          s->f(new_tm, s->from, s->to);
        }
        else {
          s->execute(c_index<I - 1>, new_tm);
        }
      }
    };
  };

  template<typename F, typename TupleLike>
  void apply_axis_target(F f, TupleLike const& tuple_fs)
  {
    axis_target_applier ata(f, tuple_fs);

    try {
      ata.execute();
    }
    catch (...) {
      auto const final_to = ata.to;
      bool finalise       = false;

      axis_target_applier cleaner(
          [&](auto const mode, auto const&, auto const& to) {
            if (!finalise && (to == final_to)) {
              finalise = true;
            }

            if (finalise) {
              if ((mode == detail::op_mode::move_assign) ||
                  (mode == detail::op_mode::fill_assign) ||
                  (mode == detail::op_mode::destruct) ||
                  (mode == detail::op_mode::none)) {
                f(detail::op_mode::destruct, to, to);
              }
            }
            else {
              if ((mode != detail::op_mode::destruct) &&
                  (mode != detail::op_mode::never)) {
                f(detail::op_mode::destruct, to, to);
              }
            }
          },
          tuple_fs);

      cleaner.execute();

      throw;
    }
  }

  template<typename F, index_t I, typename... Indices>
  auto apply_reform_axis_functor(Indices const&... inds)
  {
    return F{inds[I]...};
  }

  template<index_t Na, typename F, typename... Indices, std::size_t... Is>
  auto make_reform_axis_array_impl(
      std::index_sequence<Is...>, Indices const&... inds)
  {
    using value_type = decltype(F{inds[0]...});

    return std::array<value_type, Na>{
        apply_reform_axis_functor<F, Is>(inds...)...};
  }

  template<index_t Na, typename F, typename... Indices>
  auto make_reform_axis_array(Indices const&... inds)
  {
    return make_reform_axis_array_impl<Na, F>(
        std::make_index_sequence<Na>(), inds...);
  }
}
