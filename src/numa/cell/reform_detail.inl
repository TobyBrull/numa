namespace numa::detail {
  template<typename Tgt, index_t N, typename... Initers>
  requires(!std::is_reference_v<Tgt>) void initial_resize(
      Tgt&& tgt,
      index<N> const& size,
      index<N> const& capacity,
      Initers const&... initers)
  {
    ASSERT(is_bounded(size, capacity));

    ASSERT(tgt.size() == zero);

    constexpr index_t Na = Tgt::sel_num_axes;

    if constexpr (numa::detail::has_resize<Tgt, index<Na>, Initers...>) {
      tgt.resize(size, initers...);
    }
    else if constexpr (recursive::RecursiveReformTarget<Tgt>) {
      tgt.recurse(
          [](auto&& tgt,
             auto const& loc_size,
             auto const& loc_capa,
             auto const&... initers) {
            numa::detail::initial_resize(
                std::forward<decltype(tgt)>(tgt),
                loc_size,
                loc_capa,
                initers...);
          },
          std::tuple<Initers const&...>(initers...),
          size,
          capacity);
    }
    else {
      static_assert(recursive::CellReformTarget<Tgt>);

      if (is_bounded(capacity, tgt.capacity())) {
        tgt.fill_construct(zero, size, initers...);

        tgt.set_size(size);
      }
      else {
        auto tloc = tgt.translocation(capacity);

        if constexpr (std::is_same_v<
                          decltype(tloc),
                          translocation_forbidden_t>) {
          THROW("numa::initial_resize() attempted to do a "
                "translocation on a cell that does not allow this");
        }
        else {
          tloc.fill(zero, size, initers...);

          tloc.set_size(size);
        }
      }
    }

    ASSERT(tgt.size() == size);
  }

  template<typename Structure, index_t N, typename... Initers>
  requires(Structure::vary_axes_info.num_axes == N) void initial_resize_is_not_fixed(
      Structure& strct,
      index<N> const& size,
      index<N> const& capacity,
      Initers const&... initers)
  {
    constexpr auto fs         = Structure::vary_axes_info.is_fixed_size();
    constexpr auto value_list = Structure::vary_axes_info.value_list;

    THROW_IF_NOT(
        select_if(size, fs) == as_index(select_if(value_list, fs)),
        "initial_resize: trying to resize fixed-size axis to different size");

    constexpr auto sel = logical_not(fs);

    if constexpr (count_if(sel)) {
      initial_resize(
          strct.vary_axes(sel),
          select_if(size, sel),
          select_if(capacity, sel),
          initers...);
    }
  }
}
