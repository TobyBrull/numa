#ifndef INCLUDE_GUARD_NUMA_CELL_T_SPAN_ADAPTOR_SUB_H_
#define INCLUDE_GUARD_NUMA_CELL_T_SPAN_ADAPTOR_SUB_H_

#include "t_span_adaptor_bounded_facade.hpp"

namespace numa {
  template<Span S>
  struct t_span_adaptor_sub
    : public t_span_facade<
          t_span_adaptor_sub<S>,
          element_type_t<S>,
          S::axes_info.num_axes,
          S::axes_info.num_dims> {
   public:
    constexpr static index_t Nd     = S::axes_info.num_dims;
    constexpr static auto axes_info = axes_info_tensor<Nd>();

    using element_type = element_type_t<S>;

    S span_;
    index<Nd> from_;
    index<Nd> till_;

    t_span_adaptor_sub() = default;

    template<SubMode SM>
    t_span_adaptor_sub(
        S const& span,
        SM,
        index<Nd> const& from,
        index<Nd> const& size_of_till);

    index<Nd> size() const;

    element_type* get_ptr_impl(index<Nd> const& ind) const;
    bool is_bounded_impl(index<Nd> const& ind) const;
    auto iterator_impl(index<Nd> const& ind) const;

    auto begin() const;
    auto end() const;

   private:
    using inner_iterator_type = decltype(span_.begin());
  };

  template<Span S, SubMode SM>
  auto adaptor_sub(
      S const& span,
      SM,
      index<S::axes_info.num_dims> const& from,
      index<S::axes_info.num_dims> const& size_or_till);

  template<Cell C, SubMode SM>
  auto adaptor_sub(
      C& cell,
      SM,
      index<C::axes_info.num_dims> const& from,
      index<C::axes_info.num_dims> const& size_or_till);
}

#include "t_span_adaptor_sub.inl"

#endif
