namespace numa {
  template<Span... S, side... Side, strictly... Strict>
  class for_each_bundle_impl<t_span_adaptor_triangular<S, Side, Strict>...>
    : public generic_for_each_bundle_impl_facade {
  };

  template<Span S>
  auto upper(S const& span)
  {
    return t_span_adaptor_triangular<S, side::right, strictly::no>{span};
  }

  template<Span S>
  auto lower(S const& span)
  {
    return t_span_adaptor_triangular<S, side::left, strictly::no>{span};
  }

  template<Span S>
  auto upper_strictly(S const& span)
  {
    return t_span_adaptor_triangular<S, side::right, strictly::yes>{span};
  }

  template<Span S>
  auto lower_strictly(S const& span)
  {
    return t_span_adaptor_triangular<S, side::left, strictly::yes>{span};
  }

  template<Cell C>
  auto upper(C& cell)
  {
    return upper(cell.span());
  }

  template<Cell C>
  auto lower(C& cell)
  {
    return lower(cell.span());
  }

  template<Cell C>
  auto upper_strictly(C& cell)
  {
    return upper_strictly(cell.span());
  }

  template<Cell C>
  auto lower_strictly(C& cell)
  {
    return lower_strictly(cell.span());
  }
}
