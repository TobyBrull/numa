#ifndef INCLUDE_GUARD_NUMA_CELL_UNIFORM_TENSOR_H_
#define INCLUDE_GUARD_NUMA_CELL_UNIFORM_TENSOR_H_

#include "fwd.hpp"

#include "tensor_facade.hpp"

#include "numa/tmpl/vary_axes_info.hpp"

namespace numa {
  template<typename T, index_t N>
  class tensor_uniform_unvaried
    : public unvaried_facade<tensor_uniform<T, N>, T, N, N>
    , public disable_copy_move {
   public:
    static_assert(N >= 1);

    constexpr static auto axes_info = axes_info_tensor<N>();

    T& value();
    T const& value() const;

    index<N> const& size() const;
    constexpr static index<N> capacity();

    /*
     * span
     */
    using span_type       = t_span<T, N>;
    using span_const_type = t_span<const T, N>;

    span_type span();
    span_const_type span() const;
    span_const_type span_const() const;

    /*
     * iterator
     */
    using iterator_const_type = t_span_iterator<T const, N>;

    iterator_const_type begin() const;
    iterator_const_type begin_const() const;

    iterator_const_type end() const;
    iterator_const_type end_const() const;

    /*
     * implementation of unvaried-facade
     */
    T* get_ptr_impl(index<N> const& ind);
    T const* const_get_ptr_impl(index<N> const& ind) const;

    bool is_bounded_impl(index<N> const& ind) const;

    auto iterator_impl(index<N> const& ind);
    auto const_iterator_impl(index<N> const& ind);

   protected:
    tensor_uniform_unvaried(uninitialized_t, index<N> const& size);
    tensor_uniform_unvaried(T value, index<N> const& size);

    T value_;

    index<N> size_;
  };

  template<typename T, index_t N>
  class tensor_uniform
    : public tensor_uniform_unvaried<T, N>
    , public vary_axes_facade<tensor_uniform<T, N>, N> {
   public:
    tensor_uniform(uninitialized_t, index<N> const& size = zero);
    tensor_uniform(T value = {}, index<N> const& size = zero);

    tensor_uniform(tensor_uniform const&);
    tensor_uniform& operator=(tensor_uniform const&);

    tensor_uniform(tensor_uniform&&);
    tensor_uniform& operator=(tensor_uniform&&);

    constexpr static index_t required_elements(index<N> const& size);

    constexpr static auto vary_axes_info = vary_axes_info_dynamic<N>();

    using unvaried_type = tensor_uniform_unvaried<T, N>;

    unvaried_type& unvaried();
    unvaried_type const& unvaried() const;
    unvaried_type const& unvaried_const() const;

    template<index_t... Axes>
    requires(is_variation<N>(index_list<Axes...>)
                 .value) constexpr auto vary_axes_impl(index_list_t<Axes...>);

   private:
    template<typename, index_t, typename>
    friend class tensor_uniform_axes;
  };

  /*
   * specializations
   */
  template<typename T>
  using vector_uniform = tensor_uniform<T, 1>;

  template<typename T>
  using matrix_uniform = tensor_uniform<T, 2>;

  template<typename T>
  using cuboid_uniform = tensor_uniform<T, 3>;

  template<typename T, index_t N>
  bool operator==(
      tensor_uniform_unvaried<T, N> const& lhs,
      tensor_uniform_unvaried<T, N> const& rhs);

  template<typename T, index_t N>
  bool operator!=(
      tensor_uniform_unvaried<T, N> const& lhs,
      tensor_uniform_unvaried<T, N> const& rhs);
}

#include "tensor_uniform.inl"

#endif
