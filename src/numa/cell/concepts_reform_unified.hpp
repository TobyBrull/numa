#ifndef INCLUDE_GUARD_NUMA_CELL_CONCEPTS_UNIFIED_H_
#define INCLUDE_GUARD_NUMA_CELL_CONCEPTS_UNIFIED_H_

#include "numa/boot/index.hpp"

#include "reform_axis.hpp"

namespace numa::unified {
  /*
   * Reform target
   */
  template<typename Target, index_t Na>
  concept ReformTargetImpl = requires(Target tgt, index<Na> ind)
  {
    {
      Target::sel_num_axes
    }
    ->std::convertible_to<index_t>;

    /*
     * Not allowed to throw.
     */
    {
      tgt.size()
    }
    ->std::convertible_to<index<Na>>;
    {
      tgt.capacity()
    }
    ->std::convertible_to<index<Na>>;
    {
      tgt.must_reallocate()
    }
    ->std::convertible_to<bool>;

    /*
     * Allowed to throw.
     */
    {tgt.translate(ind, ind, std::array<detail::translate_trivial, Na>{})};

    {tgt.inplace(ind, std::array<detail::inplace_trivial, Na>{})};
  };

  template<typename Target>
  concept ReformTarget = ReformTargetImpl<Target, Target::sel_num_axes>;
}

#endif
