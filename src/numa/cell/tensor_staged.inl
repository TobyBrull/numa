namespace numa {
  /*
   * span
   */
  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      size() const -> index<Na> const&
  {
    return size_;
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      capacity() const -> index<Na> const&
  {
    return capacity_;
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      span() -> span_type
  {
    auto const stride_              = stride();
    auto const [l_stride, r_stride] = split<Na>(stride_);
    (void)r_stride;

    T* ptr = static_cast<T*>(this->ptr_) + dot(index(Rs...), l_stride);

    return span_type(ptr, this->size_, stride_);
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      span() const -> span_const_type
  {
    return span_const();
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      span_const() const -> span_const_type
  {
    auto const stride_              = stride();
    auto const [l_stride, r_stride] = split<Na>(stride_);
    (void)r_stride;

    T const* ptr =
        static_cast<T const*>(this->ptr_) + dot(index(Rs...), l_stride);

    return span_const_type(ptr, this->size_, stride_);
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      begin() -> iterator_type
  {
    return span().begin();
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      begin() const -> iterator_const_type
  {
    return begin_const();
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      begin_const() const -> iterator_const_type
  {
    return span_const().begin();
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto
  tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::end()
      -> iterator_type
  {
    return span().end();
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto
  tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::end()
      const -> iterator_const_type
  {
    return end_const();
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      end_const() const -> iterator_const_type
  {
    return span_const().end();
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  T* tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      get_ptr_impl(index<Nd> const& ind)
  {
    return span().get_ptr_impl(ind);
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  T const*
  tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      const_get_ptr_impl(index<Nd> const& ind) const
  {
    return span_const().get_ptr_impl(ind);
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  bool tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      is_bounded_impl(index<Nd> const& ind) const
  {
    return span().is_bounded_impl(ind);
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      iterator_impl(index<Nd> const& ind)
  {
    return span().iterator_impl(ind);
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      const_iterator_impl(index<Nd> const& ind) const
  {
    return span_const().iterator_impl(ind);
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  auto tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      stride() const -> index<Nd>
  {
    auto const l_stride = detail::capacity_to_stride(index((Rs + Ls + 1)...));

    constexpr index_t r_step = ((Rs + Ls + 1) * ...);

    auto const r_stride = detail::capacity_to_stride(this->capacity_) * r_step;

    return join(l_stride, r_stride - l_stride);
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>::
      tensor_staged_unvaried(
          T* ptr, index<Na> const& size, index<Na> const& capacity)
    : ptr_(ptr), size_(size), capacity_(capacity)
  {
  }

  /*
   * cell
   */
  template<
      typename T,
      index_t Nd,
      index_t... Ls,
      index_t... Rs,
      Allocator Alloc>
  tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>::
      tensor_staged(Alloc alloc) noexcept
    : tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>(
          nullptr, zero, zero)
    , alloc_(std::move(alloc))
  {
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs, typename Alloc>
  tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>::
      tensor_staged(tensor_staged&& other) noexcept
    : tensor_staged_unvaried<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>(
          other.ptr_, other.size_, other.capacity_)
  {
    other.ptr_      = nullptr;
    other.size_     = zero;
    other.capacity_ = zero;
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs, typename Alloc>
  auto tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>::
  operator=(tensor_staged&& other) noexcept -> tensor_staged&
  {
    for_each (*this)(f_destruct);

    deallocate_for<T>(alloc_, this->ptr_, required_elements(this->capacity()));

    this->ptr_      = other.ptr_;
    this->size_     = other.size_;
    this->capacity_ = other.capacity_;

    other.ptr_      = nullptr;
    other.size_     = zero;
    other.capacity_ = zero;

    return (*this);
  }

  template<
      typename T,
      index_t Nd,
      index_t... Ls,
      index_t... Rs,
      Allocator Alloc>
  tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>::
      ~tensor_staged()
  {
    for_each (*this)(f_destruct);

    deallocate_for<T>(alloc_, this->ptr_, required_elements(this->capacity()));
  }

  template<
      typename T,
      index_t Nd,
      index_t... Ls,
      index_t... Rs,
      Allocator Alloc>
  Alloc const&
  tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>::
      allocator() const
  {
    return alloc_;
  }

  template<
      typename T,
      index_t Nd,
      index_t... Ls,
      index_t... Rs,
      Allocator Alloc>
  constexpr index_t
  tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>::
      required_elements(index<Na> const& size)
  {
    return ((Rs + Ls + 1) * ...) * product(size);
  }

  template<
      typename T,
      index_t Nd,
      index_t... Ls,
      index_t... Rs,
      Allocator Alloc>
  auto tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>::
      unvaried() -> unvaried_type&
  {
    return (*this);
  }

  template<
      typename T,
      index_t Nd,
      index_t... Ls,
      index_t... Rs,
      Allocator Alloc>
  auto tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>::
      unvaried() const -> unvaried_type const&
  {
    return (*this);
  }

  template<
      typename T,
      index_t Nd,
      index_t... Ls,
      index_t... Rs,
      Allocator Alloc>
  auto tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>::
      unvaried_const() const -> unvaried_type const&
  {
    return (*this);
  }

  template<
      typename T,
      index_t Nd,
      index_t... Ls,
      index_t... Rs,
      Allocator Alloc>
  template<index_t... SelAxes>
  auto tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>::
      vary_axes_impl(index_list_t<SelAxes...>) requires(
          is_variation<Na>(index_list<SelAxes...>).value)
  {
    return tensor_staged_axes<
        T,
        Nd,
        ls_t,
        rs_t,
        Alloc,
        index_list_t<SelAxes...>>{this};
  }

  /*
   * for-each
   */
  template<typename... Ts, index_t Nd, index_t... Ls, index_t... Rs>
  struct for_each_bundle_impl<tensor_staged_unvaried<
      Ts,
      Nd,
      index_list_t<Ls...>,
      index_list_t<Rs...>>...>
    : public iterator_based_for_each_bundle_impl_facade {
  };

  namespace detail {
    template<
        typename T,
        index_t Nd,
        index_t... Ls,
        index_t... Rs,
        typename... Args>
    void tensor_staged_execute_op_mode(
        op_mode const mode,
        index<Nd / 2> const& from,
        index<Nd / 2> const& to,
        t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>& first,
        t_span_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>>& second,
        Args const&... args)
    {
      index const base((-Rs)...);
      index const till((Ls + 1)...);

      index<Nd / 2> const dummy(zero);

      auto ind = base;

      try {
        for (; !is_end_index(ind, base, till);
             tensor_increment(ind, base, till, dummy)) {
          auto const all_from = join(from + ind, from);
          auto const all_to   = join(to + ind, to);

          execute_op_mode(mode, all_from, all_to, first, second, args...);
        }
      }
      catch (...) {
        if ((mode == detail::op_mode::move_construct) ||
            (mode == detail::op_mode::fill_construct)) {
          auto jnd = base;

          for (; jnd != ind; tensor_increment(jnd, base, till, dummy)) {
            auto const all_to = join(to + jnd, to);

            execute_op_mode(op_mode::destruct, all_to, all_to, first, second);
          }
        }

        throw;
      }
    }
  }

  template<
      typename T,
      index_t Nd,
      index_t... Ls,
      index_t... Rs,
      Allocator Alloc,
      index_t... SelAxes>
  class tensor_staged_axes<
      T,
      Nd,
      index_list_t<Ls...>,
      index_list_t<Rs...>,
      Alloc,
      index_list_t<SelAxes...>>
    : public cell_axes_facade<
          tensor_staged_axes<
              T,
              Nd,
              index_list_t<Ls...>,
              index_list_t<Rs...>,
              Alloc,
              index_list_t<SelAxes...>>,
          tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>,
          index_list_t<SelAxes...>> {
   public:
    using cell_type =
        tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>;

    using facade = cell_axes_facade<
        tensor_staged_axes<
            T,
            Nd,
            index_list_t<Ls...>,
            index_list_t<Rs...>,
            Alloc,
            index_list_t<SelAxes...>>,
        cell_type,
        index_list_t<SelAxes...>>;

    using facade::facade;

    static constexpr index_t Na = Nd / 2;

    constexpr static index_t Ka = sizeof...(SelAxes);

    template<typename TupleLike, typename... Args>
    void translate(
        index<Ka> const& new_size,
        index<Ka> const& new_capa,
        TupleLike const& translate_fs,
        Args const&... args) requires(std::tuple_size_v<TupleLike> == Ka)
    {
      auto tloc = this->translocation(new_capa);

      tloc.set_size(new_size);

      auto first  = tloc.first_span();
      auto second = tloc.second_span();

      auto all_tup = detail::expand_reform_axis_tuple(
          axes_info_tensor<Nd / 2>(),
          facade::sel_axes,
          translate_fs,
          numa::detail::translate_trivial{},
          this->cell_->size());

      try {
        detail::apply_axis_target(
            [&](auto const mode, auto const& from, auto const& to) {
              detail::tensor_staged_execute_op_mode(
                  mode, from, to, first, second, args...);
            },
            all_tup);
      }
      catch (...) {
        tloc.set_size(zero);
        throw;
      }
    }

    template<typename TupleLike, typename... Args>
    void inplace(
        index<Ka> const& new_size,
        TupleLike const& inplace_fs,
        Args const&... args) requires(std::tuple_size_v<TupleLike> == Ka)
    {
      auto all_tup = detail::expand_reform_axis_tuple(
          axes_info_tensor<Na>(),
          facade::sel_axes,
          inplace_fs,
          numa::detail::inplace_trivial{},
          this->cell_->size());

      auto span = this->cell_->span();

      index_list_t<SelAxes...> const ax;
      overwrite(span.size_, ax, select_at(this->cell_->capacity(), ax));

      try {
        detail::apply_axis_target(
            [&](auto const mode, auto const& from, auto const& to) {
              detail::tensor_staged_execute_op_mode(
                  mode, from, to, span, span, args...);
            },
            all_tup);
      }
      catch (...) {
        this->set_size(zero);
        throw;
      }

      this->set_size(new_size);
    }

    static auto
    get_span(index<Ka> const& base, index<Ka> const& form, cell_type& cell)
    {
      index_list_t<SelAxes...> ax;

      auto span = cell.span();
      overwrite(span.size_, ax, select_at(cell.capacity(), ax));

      return span.axes(ax).sub(as_size, base, form);
    }

    static void set_ptr(
        tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>&
            cell,
        T* ptr)
    {
      cell.ptr_ = ptr;
    }

    static void set_size_total(
        tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>&
            cell,
        index<Na> const& new_size_total)
    {
      cell.size_ = new_size_total;
    }

    static void set_capacity_total(
        tensor_staged<T, Nd, index_list_t<Ls...>, index_list_t<Rs...>, Alloc>&
            cell,
        index<Na> const& new_cap_total)
    {
      cell.capacity_ = new_cap_total;
    }
  };

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  bool operator==(
      tensor_staged_unvaried<
          T,
          Nd,
          index_list_t<Ls...>,
          index_list_t<Rs...>> const& lhs,
      tensor_staged_unvaried<
          T,
          Nd,
          index_list_t<Ls...>,
          index_list_t<Rs...>> const& rhs)
  {
    if (lhs.size() != rhs.size()) {
      return false;
    }

    return all_of(f_is_equal, lhs, rhs);
  }

  template<typename T, index_t Nd, index_t... Ls, index_t... Rs>
  bool operator!=(
      tensor_staged_unvaried<
          T,
          Nd,
          index_list_t<Ls...>,
          index_list_t<Rs...>> const& lhs,
      tensor_staged_unvaried<
          T,
          Nd,
          index_list_t<Ls...>,
          index_list_t<Rs...>> const& rhs)
  {
    return !(lhs == rhs);
  }
}
