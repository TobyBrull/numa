#ifndef INCLUDE_GUARD_NUMA_CELL_STRUCTURE_FACADE_H_
#define INCLUDE_GUARD_NUMA_CELL_STRUCTURE_FACADE_H_

#include <numa/cell/structure_info.hpp>
#include <numa/tmpl/mark.hpp>

namespace numa {
  template<typename Subclass, typename... Fields>
  struct structure_vary_axes_facade {
   public:
    using structure_info_t = structure_info_type<type_list_t<Fields...>>;

    constexpr static structure_info_t structure_info = {};

    constexpr static index_t Na = structure_info.num_axes;

    /*
     * vary_axes
     */
    constexpr auto vary_axes();

    template<typename... AxisMarks>
    constexpr auto vary_axes() requires((is_mark<AxisMarks>() && ...).value);

    template<typename... AxisMarks>
    constexpr auto
    vary_axes(AxisMarks...) requires((is_mark<AxisMarks>() && ...).value);

    template<index_t... Axes>
    constexpr auto vary_axes(index_list_t<Axes...> = {}) requires(
        is_variation<Na>(index_list<Axes...>).value);

    template<index_t... Axes>
    constexpr auto vary_axes(c_index_t<Axes>...) requires(
        is_variation<Na>(index_list<Axes...>).value);

    template<bool... SelAxes>
    constexpr auto
    vary_axes(c_bool_t<SelAxes>...) requires(sizeof...(SelAxes) == Na);

    template<bool... SelAxes>
    constexpr auto
        vary_axes(bool_list_t<SelAxes...>) requires(sizeof...(SelAxes) == Na);
  };
}

#include "structure_facade.inl"

#endif
