namespace numa {
  constexpr inline index_t
  growth_strategy(index_t const capa, index_t const new_size)
  {
    if (new_size > capa) {
      return std::max(new_size, 2 * capa);
    }
    else {
      return capa;
    }
  }

  template<index_t N>
  index<N> growth_strategy(index<N> capa, index<N> const new_size)
  {
    for (index_t i = 0; i < N; ++i) {
      capa[i] = growth_strategy(capa[i], new_size[i]);
    }

    return capa;
  }
}
