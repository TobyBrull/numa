#ifndef INCLUDE_GUARD_NUMA_CELL_LIN_RANGE_H_
#define INCLUDE_GUARD_NUMA_CELL_LIN_RANGE_H_

#include "for_each.hpp"
#include "fwd.hpp"
#include "tensor_facade.hpp"

#include "numa/core/iterator_facade.hpp"

#include "numa/tmpl/vary_axes_info.hpp"

namespace numa {
  template<typename T>
  struct vector_linear_incr_unvaried
    : public unvaried_facade<vector_linear_incr_unvaried<T>, T, 1, 1>
    , public disable_copy_move {
   public:
    constexpr static auto axes_info = axes_info_tensor<1>();

    T& front();
    T const& front() const;

    difference_type_t<T>& incr();
    difference_type_t<T> const& incr() const;

    T back() const;

    index<1> size() const;
    constexpr static index<1> capacity();

    /*
     * iterator
     */
    using iterator_const_type = vector_linear_incr_iterator<T>;

    iterator_const_type begin() const;
    iterator_const_type begin_const() const;

    iterator_const_type end() const;
    iterator_const_type end_const() const;

    /*
     * implementation of unvaried-facade
     */
    T operator()(index<1> const& ind) const;
    T operator[](index<1> const& ind) const;
    T at(index<1> const& ind) const;

    iterator_const_type iterator(index<1> const& ind) const;
    iterator_const_type const_iterator(index<1> const& ind) const;

   protected:
    vector_linear_incr_unvaried(
        index<1> size, T front, difference_type_t<T> incr);

    index<1> size_             = 0;
    T front_                   = {};
    difference_type_t<T> incr_ = {};
  };

  template<typename T>
  class vector_linear_incr
    : public vector_linear_incr_unvaried<T>
    , public vary_axes_facade<vector_linear_incr<T>, 1> {
   public:
    vector_linear_incr(
        index<1> size = 0, T front = {}, difference_type_t<T> incr = {});

    vector_linear_incr(vector_linear_incr const&);
    vector_linear_incr& operator=(vector_linear_incr const&);

    vector_linear_incr(vector_linear_incr&&);
    vector_linear_incr& operator=(vector_linear_incr&&);

    constexpr static index_t required_elements(index<1> size);

    constexpr static auto vary_axes_info = vary_axes_info_dynamic<1>();

    using unvaried_type = vector_linear_incr_unvaried<T>;

    unvaried_type& unvaried();
    unvaried_type const& unvaried() const;
    unvaried_type const& unvaried_const() const;

    template<index_t... Axes>
    requires(is_variation<1>(index_list<Axes...>)
                 .value) constexpr auto vary_axes_impl(index_list_t<Axes...>);

   private:
    template<typename, typename>
    friend class vector_linear_incr_axes;
  };

  template<typename T>
  bool operator==(
      vector_linear_incr_unvaried<T> const& lhs,
      vector_linear_incr_unvaried<T> const& rhs);

  template<typename T>
  bool operator!=(
      vector_linear_incr_unvaried<T> const& lhs,
      vector_linear_incr_unvaried<T> const& rhs);

  //  template<typename T>
  //  struct vector_linear_back;
}

#include "tensor_linear.inl"

#endif
