namespace numa {
  /*
   *
   */
  template<typename Subclass, typename... Fields>
  constexpr auto structure_vary_axes_facade<Subclass, Fields...>::vary_axes()
  {
    return vary_axes(index_list_iota<Na>());
  }

  template<typename Subclass, typename... Fields>
  template<typename... AxisMarks>
  constexpr auto
  structure_vary_axes_facade<Subclass, Fields...>::vary_axes() requires(
      (is_mark<AxisMarks>() && ...).value)
  {
    return vary_axes(AxisMarks{}...);
  }

  template<typename Subclass, typename... Fields>
  template<typename... AxisMarks>
  constexpr auto structure_vary_axes_facade<Subclass, Fields...>::vary_axes(
      AxisMarks... ams) requires((is_mark<AxisMarks>() && ...).value)
  {
    return this->vary_axes(
        type_list_t{find(structure_info.axis_marks, ams)...});
  }

  template<typename Subclass, typename... Fields>
  template<index_t... Axes>
  constexpr auto structure_vary_axes_facade<Subclass, Fields...>::vary_axes(
      index_list_t<Axes...> el) requires(is_variation<Na>(index_list<Axes...>)
                                             .value)
  {
    return static_cast<Subclass*>(this)->vary_axes_impl(el);
  }

  template<typename Subclass, typename... Fields>
  template<index_t... Axes>
  constexpr auto structure_vary_axes_facade<Subclass, Fields...>::vary_axes(
      c_index_t<Axes>...) requires(is_variation<Na>(index_list<Axes...>).value)
  {
    return vary_axes(index_list<Axes...>);
  }

  template<typename Subclass, typename... Fields>
  template<bool... SelAxes>
  constexpr auto structure_vary_axes_facade<Subclass, Fields...>::vary_axes(
      c_bool_t<SelAxes>...) requires(sizeof...(SelAxes) == Na)
  {
    constexpr auto el = select_if(index_list_iota<Na>(), bool_list<SelAxes...>);

    return vary_axes(el);
  }

  template<typename Subclass, typename... Fields>
  template<bool... SelAxes>
  constexpr auto structure_vary_axes_facade<Subclass, Fields...>::vary_axes(
      bool_list_t<SelAxes...> bl) requires(sizeof...(SelAxes) == Na)
  {
    constexpr auto el = select_if(index_list_iota<Na>(), bl);

    return vary_axes(el);
  }
}
