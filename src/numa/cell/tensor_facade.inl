namespace numa {
  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<typename... Index>
  requires constructs_index_v<Nd, Index...> T&
  unvaried_facade<Subclass, T, Na, Nd>::operator()(Index... inds)
  {
    return *(subclass().get_ptr_impl(index<Nd>(inds...)));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<typename... Index>
  requires constructs_index_v<Nd, Index...> T const&
  unvaried_facade<Subclass, T, Na, Nd>::operator()(Index... inds) const
  {
    return *(subclass().const_get_ptr_impl(index<Nd>(inds...)));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<typename... Index>
  requires constructs_index_v<Nd, Index...> T&
  unvaried_facade<Subclass, T, Na, Nd>::at(Index... inds)
  {
    index<Nd> const ind(inds...);

    BOUNDS_CHECK(subclass().is_bounded_impl(ind));

    return *(subclass().get_ptr_impl(index<Nd>(ind)));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<typename... Index>
  requires constructs_index_v<Nd, Index...> T const&
  unvaried_facade<Subclass, T, Na, Nd>::at(Index... inds) const
  {
    index<Nd> const ind(inds...);

    BOUNDS_CHECK(subclass().is_bounded_impl(ind));

    return *(subclass().const_get_ptr_impl(index<Nd>(ind)));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<typename... Index>
  requires constructs_index_v<Nd, Index...> bool
  unvaried_facade<Subclass, T, Na, Nd>::is_bounded(Index... inds) const
  {
    return subclass().is_bounded_impl(index<Nd>(inds...));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  T& unvaried_facade<Subclass, T, Na, Nd>::operator[](index<Nd> const& ind)
  {
    return *(subclass().get_ptr_impl(ind));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  T const&
  unvaried_facade<Subclass, T, Na, Nd>::operator[](index<Nd> const& ind) const
  {
    return *(subclass().const_get_ptr_impl(ind));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  T* unvaried_facade<Subclass, T, Na, Nd>::data()
  {
    return subclass().get_ptr_impl(zero);
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  T const* unvaried_facade<Subclass, T, Na, Nd>::data() const
  {
    return subclass().const_get_ptr_impl(zero);
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<typename... Index>
  requires constructs_index_v<Nd, Index...> T*
  unvaried_facade<Subclass, T, Na, Nd>::get_ptr(Index... inds)
  {
    return subclass().get_ptr_impl(index<Nd>(inds...));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  template<typename... Index>
  requires constructs_index_v<Nd, Index...> T const*
  unvaried_facade<Subclass, T, Na, Nd>::get_ptr(Index... inds) const
  {
    return subclass().const_get_ptr_impl(index<Nd>(inds...));
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  auto unvaried_facade<Subclass, T, Na, Nd>::cbegin() const
  {
    return subclass().begin_const();
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  auto unvaried_facade<Subclass, T, Na, Nd>::cend() const
  {
    return subclass().end_const();
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  Subclass& unvaried_facade<Subclass, T, Na, Nd>::subclass()
  {
    return static_cast<Subclass&>(*this);
  }

  template<typename Subclass, typename T, index_t Na, index_t Nd>
  Subclass const& unvaried_facade<Subclass, T, Na, Nd>::subclass() const
  {
    return static_cast<Subclass const&>(*this);
  }

  /*
   * Implementation of facade-vary-axes.
   */
  template<typename Subclass, index_t Na>
  constexpr auto vary_axes_facade<Subclass, Na>::vary_axis() requires(Na == 1)
  {
    return vary_axes();
  }

  template<typename Subclass, index_t Na>
  constexpr auto vary_axes_facade<Subclass, Na>::vary_axes()
  {
    return static_cast<Subclass*>(this)->vary_axes_impl(index_list_iota<Na>());
  }

  template<typename Subclass, index_t Na>
  template<index_t... SelAxes>
  requires(
      is_variation<Na>(index_list<SelAxes...>)
          .value) constexpr auto vary_axes_facade<Subclass, Na>::vary_axes()
  {
    return static_cast<Subclass*>(this)->vary_axes_impl(
        index_list_t<SelAxes...>());
  }

  template<typename Subclass, index_t Na>
  template<index_t... SelAxes>
  requires(is_variation<Na>(index_list<SelAxes...>)
               .value) constexpr auto vary_axes_facade<Subclass, Na>::
      vary_axes(c_index_t<SelAxes>... sel_axes)
  {
    return static_cast<Subclass*>(this)->vary_axes_impl(
        type_list_t{sel_axes...});
  }

  template<typename Subclass, index_t Na>
  template<index_t... SelAxes>
  requires(is_variation<Na>(index_list<SelAxes...>)
               .value) constexpr auto vary_axes_facade<Subclass, Na>::
      vary_axes(index_list_t<SelAxes...> el)
  {
    return static_cast<Subclass*>(this)->vary_axes_impl(el);
  }

  template<typename Subclass, index_t Na>
  template<bool... SelAxes>
  requires(sizeof...(SelAxes) == Na) constexpr auto vary_axes_facade<
      Subclass,
      Na>::vary_axes(c_bool_t<SelAxes>...)
  {
    constexpr auto el = select_if(index_list_iota<Na>(), bool_list<SelAxes...>);

    return static_cast<Subclass*>(this)->vary_axes_impl(el);
  }

  template<typename Subclass, index_t Na>
  template<bool... SelAxes>
  requires(sizeof...(SelAxes) == Na) constexpr auto vary_axes_facade<
      Subclass,
      Na>::vary_axes(bool_list_t<SelAxes...> bl)
  {
    constexpr auto el = select_if(index_list_iota<Na>(), bl);

    return static_cast<Subclass*>(this)->vary_axes_impl(el);
  }

  /*
   * Implementation of cell-axes-facade-light.
   */
  template<typename Subclass, typename C, index_t... SelAxes>
  constexpr cell_axes_facade_light<Subclass, C, index_list_t<SelAxes...>>::
      cell_axes_facade_light(C* const cell)
    : cell_(cell)
  {
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  auto cell_axes_facade_light<Subclass, C, index_list_t<SelAxes...>>::get_span(
      index<sel_num_axes> const& base, index<sel_num_axes> const& form, C& cell)
  {
    index_list_t<SelAxes...> ax;

    auto span = cell.span();
    overwrite(span.size_, ax, select_at(cell.capacity(), ax));

    return span.dims(ax).sub(as_size, base, form);
  }

  /*
   * Implementation of cell-axes-facade.
   */
  template<typename Subclass, typename C, index_t... SelAxes>
  Subclass&& cell_axes_facade<Subclass, C, index_list_t<SelAxes...>>::with(
      reform_mode_t ref_mode) &&
  {
    ref_mode_ = std::move(ref_mode);
    return static_cast<Subclass&&>(*this);
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  auto cell_axes_facade<Subclass, C, index_list_t<SelAxes...>>::size() const
  {
    return select_at(this->cell_->size(), index_list_t<SelAxes...>{});
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  auto cell_axes_facade<Subclass, C, index_list_t<SelAxes...>>::capacity() const
  {
    return select_at(this->cell_->capacity(), index_list_t<SelAxes...>{});
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  bool
  cell_axes_facade<Subclass, C, index_list_t<SelAxes...>>::must_reallocate()
      const
  {
    return reform_mode_must_reallocate_for<value_type>(this->ref_mode_);
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  template<typename... Args>
  void cell_axes_facade<Subclass, C, index_list_t<SelAxes...>>::fill_construct(
      index<sel_num_axes> const& base,
      index<sel_num_axes> const& form,
      Args const&... args)
  {
    for_each(Subclass::get_span(base, form, *this->cell_))(
        f_fill_construct_t{args...}, f_destruct);
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  void cell_axes_facade<Subclass, C, index_list_t<SelAxes...>>::destruct(
      index<sel_num_axes> const& base, index<sel_num_axes> const& form)
  {
    for_each(Subclass::get_span(base, form, *this->cell_))(f_destruct);
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  void cell_axes_facade<Subclass, C, index_list_t<SelAxes...>>::set_size(
      index<sel_num_axes> const& new_size)
  {
    auto size = this->cell_->size();
    overwrite(size, index_list_t<SelAxes...>{}, new_size);

    Subclass::set_size_total(*(this->cell_), size);
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  auto cell_axes_facade<Subclass, C, index_list_t<SelAxes...>>::translocation(
      index<sel_num_axes> const& new_capa)
  {
    auto second = [&] {
      constexpr index_list_t<SelAxes...> ax;

      auto new_cap_total = this->cell_->capacity();

      overwrite(new_cap_total, ax, new_capa);

      auto alloc = this->cell_->allocator();

      auto* ptr = reform_mode_allocate_for<value_type>(
          alloc,
          C::required_elements(new_cap_total),
          std::move(this->ref_mode_));

      C retval(std::move(alloc));

      Subclass::set_ptr(retval, ptr);
      Subclass::set_capacity_total(retval, new_cap_total);

      auto size = this->cell_->size();
      overwrite(size, ax, index<sel_num_axes>(zero));
      Subclass::set_size_total(retval, size);

      return retval;
    }();

    return cell_axes_facade_translocation<
        Subclass,
        C,
        index_list_t<SelAxes...>>(this->cell_, std::move(second));
  }

  namespace detail {
    template<
        index_t... Axes,
        index_t... SelAxes,
        typename TupleLike,
        typename DefaultType>
    auto expand_reform_axis_tuple(
        axes_info<index_list_t<Axes...>> ai,
        index_list_t<SelAxes...> sel_axes,
        TupleLike const& tup,
        DefaultType const& default_value,
        index<sizeof...(Axes)> const& form)
    {
      auto retval = ai.expand_tuple(sel_axes, tup, default_value);

      tuple_for_each_indexed(
          [&](auto const ind, auto& ref_axis) {
            ref_axis.old_size = form[ind.value];
          },
          retval);

      return retval;
    }
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  template<typename TupleLike, typename... Args>
  void cell_axes_facade<Subclass, C, index_list_t<SelAxes...>>::translate(
      index<sel_num_axes> const& new_size,
      index<sel_num_axes> const& new_capacity,
      TupleLike const& translate_fs,
      Args const&... args) requires(std::tuple_size_v<TupleLike> == sel_num_axes)
  {
    auto tloc = translocation(new_capacity);

    tloc.set_size(new_size);

    auto first  = tloc.first_span();
    auto second = tloc.second_span();

    auto const all_tup = detail::expand_reform_axis_tuple(
        C::axes_info,
        this->sel_axes,
        translate_fs,
        detail::translate_trivial{},
        C::axes_info.size_to_form(first.size()));

    try {
      detail::apply_axis_target(
          [&](auto const mode, auto const& from, auto const& to) {
            if (second.is_bounded(to)) {
              detail::execute_op_mode(mode, from, to, first, second, args...);
            }
          },
          all_tup);
    }
    catch (...) {
      tloc.set_size(zero);
      throw;
    }
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  template<typename TupleLike, typename... Args>
  void cell_axes_facade<Subclass, C, index_list_t<SelAxes...>>::inplace(
      index<sel_num_axes> const& new_size,
      TupleLike const& inplace_fs,
      Args const&... args) requires(std::tuple_size_v<TupleLike> == sel_num_axes)
  {
    auto all_tup = detail::expand_reform_axis_tuple(
        C::axes_info,
        this->sel_axes,
        inplace_fs,
        detail::inplace_trivial{},
        C::axes_info.size_to_form(this->cell_->size()));

    auto span = this->cell_->span();

    index_list_t<SelAxes...> const ax;
    overwrite(span.size_, ax, select_at(this->cell_->capacity(), ax));

    try {
      detail::apply_axis_target(
          [&](auto const mode, auto const& from, auto const& to) {
            if (span.is_bounded(to)) {
              detail::execute_op_mode(mode, from, to, span, span, args...);
            }
          },
          all_tup);
    }
    catch (...) {
      static_cast<Subclass*>(this)->set_size(zero);
      throw;
    }

    static_cast<Subclass*>(this)->set_size(new_size);
  }

  /*
   * Translocation
   */
  template<typename Subclass, typename C, index_t... SelAxes>
  constexpr cell_axes_facade_translocation<
      Subclass,
      C,
      index_list_t<SelAxes...>>::
      cell_axes_facade_translocation(C* first, C second)
    : first_(first), second_(std::move(second))
  {
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  cell_axes_facade_translocation<Subclass, C, index_list_t<SelAxes...>>::
      cell_axes_facade_translocation(cell_axes_facade_translocation&& other)
    : first_(other.first_), second_(std::move(other.second_))
  {
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  auto cell_axes_facade_translocation<Subclass, C, index_list_t<SelAxes...>>::
  operator=(cell_axes_facade_translocation&& other)
      -> cell_axes_facade_translocation&
  {
    first_       = other.first_;
    other.first_ = nullptr;

    second_ = std::move(other.second_);
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  cell_axes_facade_translocation<Subclass, C, index_list_t<SelAxes...>>::
      ~cell_axes_facade_translocation()
  {
    swap();
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  template<typename... Args>
  void
  cell_axes_facade_translocation<Subclass, C, index_list_t<SelAxes...>>::fill(
      index<sel_num_axes> const& base,
      index<sel_num_axes> const& form,
      Args const&... args)
  {
    for_each(Subclass::get_span(base, form, second_))(
        f_fill_construct_t{args...}, f_destruct);
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  void
  cell_axes_facade_translocation<Subclass, C, index_list_t<SelAxes...>>::move(
      index<sel_num_axes> const& base,
      index<sel_num_axes> const& form,
      index<sel_num_axes> const& dest)
  {
    for_each(
        Subclass::get_span(dest, form, second_),
        Subclass::get_span(base, form, *first_))(f_move_construct, f_destruct);
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  void cell_axes_facade_translocation<Subclass, C, index_list_t<SelAxes...>>::
      destruct(index<sel_num_axes> const& base, index<sel_num_axes> const& form)
  {
    for_each(Subclass::get_span(base, form, second_))(f_destruct);
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  void cell_axes_facade_translocation<Subclass, C, index_list_t<SelAxes...>>::
      set_size(index<sel_num_axes> const& new_size)
  {
    auto size = second_.size();
    overwrite(size, index_list_t<SelAxes...>{}, new_size);

    Subclass::set_size_total(second_, size);
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  auto cell_axes_facade_translocation<Subclass, C, index_list_t<SelAxes...>>::
      first_span()
  {
    return first_->span();
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  auto cell_axes_facade_translocation<Subclass, C, index_list_t<SelAxes...>>::
      second_span()
  {
    return second_.span();
  }

  template<typename Subclass, typename C, index_t... SelAxes>
  void
  cell_axes_facade_translocation<Subclass, C, index_list_t<SelAxes...>>::swap()
  {
    std::swap(*first_, second_);
  }
}
