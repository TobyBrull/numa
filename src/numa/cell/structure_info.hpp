#ifndef INCLUDE_GUARD_NUMA_CELL_STRUCTURE_INFO_H_
#define INCLUDE_GUARD_NUMA_CELL_STRUCTURE_INFO_H_

#include "concepts.hpp"
#include "fwd.hpp"

#include "numa/core/uninitialized.hpp"

#include "numa/tmpl/integer_list.hpp"
#include "numa/tmpl/mark.hpp"
#include "numa/tmpl/vary_axes_info.hpp"

#include <tuple>

namespace numa {
  template<typename Mark, typename S, typename... AxisMarks>
  requires((is_mark(c_type<Mark>) && (is_mark(c_type<AxisMarks>) && ...))
               .value) struct field {
    constexpr static literal_tag_t literal_tag = {};

    using type = field;

    static_assert(S::vary_axes_info.num_axes == sizeof...(AxisMarks));

    using mark_t        = Mark;
    using structure_t   = S;
    using axis_marks_tl = type_list_t<AxisMarks...>;

    static constexpr mark_t mark              = {};
    static constexpr axis_marks_tl axis_marks = {};
  };

  template<typename AxisMark, typename S>
  using label = field<AxisMark, S, AxisMark>;

  /*
   *
   */
  template<typename T>
  constexpr auto is_field(c_type_t<T> = {});

  template<typename... Fields>
  struct structure_info_type<type_list_t<Fields...>> {
   private:
    constexpr static auto unique_axis_marks_impl();
    constexpr static auto unique_value_type_impl();

   public:
    using info_list_t = type_list_t<Fields...>;

    constexpr static info_list_t info_list = {};

    using value_type = typename decltype(unique_value_type_impl())::type;

    constexpr static auto axis_marks  = unique_axis_marks_impl();
    constexpr static auto field_marks = type_list_t(Fields::mark...);

    constexpr static index_t num_axes   = numa::size(axis_marks);
    constexpr static index_t num_fields = sizeof...(Fields);

    template<index_t K>
    using field_t = decltype(numa::get<K>(type_list_t<Fields...>{}));

    template<index_t K>
    using structure_t = typename field_t<K>::structure_t;

    template<typename FieldMark>
    requires(is_mark(c_type<FieldMark>).value) using structure_by_mark_t =
        structure_t<find(field_marks, FieldMark{}).value>;

    constexpr static auto vary_axes_info();

    static_assert(
        (is_field<Fields>() && ...),
        "structure_info: trying to instantiate with "
        "template parameters that are not fields");

    static_assert(
        numa::size(unique(field_marks)) == numa::size(field_marks),
        "structure_info: trying to field-marks that are not unique");

    template<index_t... Axes>
    constexpr static void
    static_assert_none_is_fixed_size(index_list_t<Axes...> sel = {});

    /*
     * Returns a type-list of tuples, akin to
     *
     * type_list_t (std::tuple (3_i, 1_i), ...)
     *
     * where each tuple contains first the field-index and
     * then the axis-index inside that field.
     */
    template<typename AxisMark>
    requires(is_mark(c_type<AxisMark>).value) constexpr static auto find_axes(
        AxisMark = {});

    /*
     * For each field, calls @p f with:
     *    i  ) the index of the field.
     *    ii ) the index-list of axes that one has to select of the
     *         field-structure, if the axes @p sel are selected
     *         at super-structure level.
     *    iii) the index-list of axes at super-structure level
     *         corresponding to ii); this thus has the same size as
     *         the index-list in ii).
     */
    template<typename F, index_t... Axes>
    constexpr static void for_each_field(F&& f, index_list_t<Axes...> sel);

    /*
     * @p p is expected to take one parameter, which is of type
     * vary_axes_info, and return a bool_list_t of the same size.
     *
     * For each field, calls @p f is called with:
     *    i  ) the index of the field.
     *    ii ) the index-list of axes for which @p is false in the
     *         local structure, but true in the global structure.
     *    iii) the index-list of value from the global vary-axes-info.
     *    iv ) a variadic pack of initers-or-args, as obtained by
     *         transforming the @p initers... by a call to
     *         recursive_initializer_for.
     */
    template<typename F, typename VaryAxesInfoPredicate, typename... Initers>
    constexpr static void for_each_field_contrary_recursive(
        F&& f, VaryAxesInfoPredicate p, Initers const&... initers);
  };

  template<typename Field, typename F, typename... InitersOrArgs>
  requires(is_field<Field>().value) auto recursive_initializer_for(
      F&& f, InitersOrArgs const&... initers_or_args);

  template<typename Field, typename F, typename... InitersOrArgs>
  requires(is_field<Field>().value) auto recursive_initializer_for_tuple(
      F&& f, std::tuple<InitersOrArgs const&...> initers_or_args_tuple);
}

#include "structure_info.inl"

#endif
