namespace numa {
  template<typename T, index_t N, typename X>
  class tensor_sparse_iterator
    : public iterator_facade<tensor_sparse_iterator<T, N, X>, T> {
   public:
    static_assert(N >= 1);

    tensor_sparse_iterator(T* ptr, index<N, X> const* ptr_ind)
      : ptr_(ptr), ptr_ind_(ptr_ind)
    {
    }

    T* ptr() const
    {
      return ptr_;
    }

    void increment()
    {
      ++ptr_;
      ++ptr_ind_;
    }

    void decrement()
    {
      --ptr_;
      --ptr_ind_;
    }

    void advance(index_t const offset)
    {
      ptr_ += offset;
      ptr_ind_ += offset;
    }

    index_t distance_to(tensor_sparse_iterator const& other) const
    {
      return (other.ptr_ - ptr_);
    }

    index<N, X> const& pos() const
    {
      return (*ptr_ind_);
    }

    bool equal_to(tensor_sparse_iterator const& other) const
    {
      return (ptr_ == other.ptr_);
    }

    T& dereference() const
    {
      return (*ptr_);
    }

   private:
    T* ptr_;
    index<N, X> const* ptr_ind_;
  };

  template<typename T, index_t N, typename X>
  constexpr std::align_val_t tensor_sparse_alignment()
  {
    return std::max(alignment_of<T>, alignment_of<index<N, X>>);
  }

  namespace detail {
    template<typename T, index_t N, typename X>
    constexpr std::pair<index_t, index_t>
    compute_vals_and_inds_mem(index_t const capacity)
    {
      constexpr std::align_val_t AL = tensor_sparse_alignment<T, N, X>();

      index_t const req_elem = capacity;

      ASSERT(req_elem >= 0);
      static_assert(sizeof(T) > 0);

      index_t const i_AL = static_cast<index_t>(AL);

      index_t const values_mem =
          chunk_required<index_t>(req_elem * sizeof(T), i_AL) * i_AL;

      index_t const indices_mem =
          chunk_required<index_t>(req_elem * sizeof(index<N, X>), i_AL) * i_AL;

      return std::pair(values_mem, indices_mem);
    }

    template<typename T, index_t N, typename X>
    T* compute_value_ptr(void* ptr, index_t const capa) noexcept
    {
      auto const [vals_mem, inds_mem] =
          compute_vals_and_inds_mem<T, N, X>(capa);
      (void)vals_mem;

      char* c_ptr = static_cast<char*>(ptr) + inds_mem;

      return static_cast<T*>(static_cast<void*>(c_ptr));
    }

    template<typename T, index_t N, typename X>
    T const* compute_value_ptr(void const* ptr, index_t const capa) noexcept
    {
      auto const [vals_mem, inds_mem] =
          compute_vals_and_inds_mem<T, N, X>(capa);
      (void)vals_mem;

      char const* c_ptr = static_cast<char const*>(ptr) + inds_mem;

      return static_cast<T const*>(static_cast<void const*>(c_ptr));
    }
  }

  /*
   * unvaried
   */
  template<typename T, index_t N, typename X, Allocator Alloc>
  constexpr index_t
  tensor_sparse_unvaried<T, N, X, Alloc>::required_bytes(index_t storage_size)
  {
    auto const [new_vals_mem, new_inds_mem] =
        detail::compute_vals_and_inds_mem<T, N, X>(storage_size);

    return (new_vals_mem + new_inds_mem);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  index<N, X> const&
  tensor_sparse_unvaried<T, N, X, Alloc>::size() const noexcept
  {
    return size_;
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  constexpr index<N, X>
  tensor_sparse_unvaried<T, N, X, Alloc>::capacity() noexcept
  {
    return (index<N, X>(one) * std::numeric_limits<index_t>::max());
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  index_t tensor_sparse_unvaried<T, N, X, Alloc>::storage_size() const noexcept
  {
    return storage_size_;
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  index_t
  tensor_sparse_unvaried<T, N, X, Alloc>::storage_capacity() const noexcept
  {
    return storage_capacity_;
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  range<T*> tensor_sparse_unvaried<T, N, X, Alloc>::values()
  {
    T* ptr = detail::compute_value_ptr<T, N, X>(ptr_, storage_capacity_);

    return range<T*>(ptr, ptr + storage_size_);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  range<T const*> tensor_sparse_unvaried<T, N, X, Alloc>::values() const
  {
    return values_const();
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  range<T const*> tensor_sparse_unvaried<T, N, X, Alloc>::values_const() const
  {
    T const* ptr = detail::compute_value_ptr<T, N, X>(ptr_, storage_capacity_);

    return range<T const*>(ptr, ptr + storage_size_);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  range<index<N, X>*> tensor_sparse_unvaried<T, N, X, Alloc>::indices()
  {
    auto const ptr = static_cast<index<N, X>*>(ptr_);

    return range<index<N, X>*>(ptr, ptr + storage_size_);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  range<index<N, X> const*>
  tensor_sparse_unvaried<T, N, X, Alloc>::indices() const
  {
    return indices_const();
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  range<index<N, X> const*>
  tensor_sparse_unvaried<T, N, X, Alloc>::indices_const() const
  {
    auto const ptr = static_cast<index<N, X> const*>(ptr_);

    return range<index<N, X> const*>(ptr, ptr + storage_size_);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  auto tensor_sparse_unvaried<T, N, X, Alloc>::begin() -> iterator_type
  {
    return iterator_type(values().begin(), indices().begin());
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  auto tensor_sparse_unvaried<T, N, X, Alloc>::begin() const
      -> iterator_const_type
  {
    return begin_const();
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  auto tensor_sparse_unvaried<T, N, X, Alloc>::begin_const() const
      -> iterator_const_type
  {
    return iterator_const_type(values().begin(), indices().begin());
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  auto tensor_sparse_unvaried<T, N, X, Alloc>::end() -> iterator_type
  {
    return iterator_type(values().end(), indices().end());
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  auto tensor_sparse_unvaried<T, N, X, Alloc>::end() const
      -> iterator_const_type
  {
    return end_const();
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  auto tensor_sparse_unvaried<T, N, X, Alloc>::end_const() const
      -> iterator_const_type
  {
    return iterator_const_type(values().end(), indices().end());
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  bool
  tensor_sparse_unvaried<T, N, X, Alloc>::is_sorted_strictly() const noexcept
  {
    auto const i_range = indices();
    return numa::is_sorted_strictly(i_range);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  auto
  tensor_sparse_unvaried<T, N, X, Alloc>::lower_bound(index<N, X> const& ind)
      -> iterator_type
  {
    ASSERT(is_sorted_strictly());

    auto const i_range = indices();
    auto const lb_it   = numa::lower_bound(i_range, ind);
    auto const diff    = (lb_it - indices().begin());
    return iterator_type(values().begin() + diff, lb_it);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  auto tensor_sparse_unvaried<T, N, X, Alloc>::lower_bound(
      index<N, X> const& ind) const -> iterator_const_type
  {
    ASSERT(is_sorted_strictly());

    auto const i_range = indices();
    auto const lb_it   = numa::lower_bound(i_range, ind);
    auto const diff    = (lb_it - indices().begin());
    return iterator_type(values().begin() + diff, lb_it);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  template<typename U, Allocator OtherAlloc, typename Transformer>
  void tensor_sparse_unvaried<T, N, X, Alloc>::insert(
      tensor_sparse_unvaried<U, N, X, OtherAlloc> const& other, Transformer f)
  {
    ASSERT(this->size() == other.size());

    auto const k = other.storage_size_;

    this->storage_reserve(this->storage_size_ + k);

    auto i_range = this->indices();
    auto v_range = this->values();

    auto const o_i_range = other.indices_const();
    auto const o_v_range = other.values_const();

    std::uninitialized_copy(o_v_range.begin(), o_v_range.end(), v_range.end());

    /*
     * This function assumes that the following doesn't throw.
     */
    std::uninitialized_copy(o_i_range.begin(), o_i_range.end(), i_range.end());

    this->storage_size_ += k;

    for (auto& val: range(v_range.end(), v_range.end() + k)) {
      val = f(val);
    }
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  template<typename F>
  void tensor_sparse_unvaried<T, N, X, Alloc>::erase_if(F&& f)
  {
    auto i_range  = this->indices();
    auto v_range  = this->values();
    auto const zr = make_zip_range(i_range, v_range);

    auto const new_end =
        std::remove_if(zr.begin(), zr.end(), [&](auto const& proxy) {
          auto const& ind   = proxy.template get<0>();
          auto const& value = proxy.template get<1>();

          return f(ind, value);
        });

    for (auto& [ind, val]: range(new_end, zr.end())) {
      numa::destruct(ind);
      numa::destruct(val);
    }

    this->storage_size_ = (new_end - zr.begin());
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  void tensor_sparse_unvaried<T, N, X, Alloc>::sort_strictly()
  {
    zip_sort_tuple(
        element_compare<0>(f_is_less), this->indices(), this->values());

    index_t const count = zip_unique_tuple(
        [](auto const& lhs, auto const& rhs) {
          if (std::get<0>(lhs) == std::get<0>(rhs)) {
            std::get<1>(lhs) += std::get<1>(rhs);
            return true;
          }
          else {
            return false;
          }
        },
        this->indices(),
        this->values());

    ASSERT(count <= this->storage_size_);

    if (count < this->storage_size_) {
      {
        index<N, X>* ptr = this->indices().begin();
        std::destroy(ptr + count, ptr + this->storage_size_);
      }

      {
        T* ptr = this->values().begin();
        std::destroy(ptr + count, ptr + this->storage_size_);
      }

      this->storage_size_ = count;
    }
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  void tensor_sparse_unvaried<T, N, X, Alloc>::push_back(
      index<N, X> const& ind, T value)
  {
    ASSERT(this->storage_size_ <= this->storage_capacity_);

    if (this->storage_size_ == this->storage_capacity_) {
      auto const new_capa =
          growth_strategy(this->storage_capacity_, this->storage_size_ + 1);

      this->storage_reserve(new_capa);
    }

    ASSERT(this->storage_size_ < this->storage_capacity_);

    {
      T* const ptr = this->values().begin() + this->storage_size_;
      numa::move_construct(*ptr, value);
    }

    /*
     * Assumes that this does not throw.
     */
    {
      index<N, X>* const ptr = this->indices().begin() + this->storage_size_;
      numa::copy_construct(*ptr, ind);
    }

    ++(this->storage_size_);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  void tensor_sparse_unvaried<T, N, X, Alloc>::pop_back()
  {
    ASSERT(this->storage_size_ > 0);

    {
      T* const ptr = this->values().begin() + this->storage_size_ - 1;
      numa::destruct(*ptr);
    }

    {
      index<N, X>* const ptr =
          this->indices().begin() + this->storage_size_ - 1;
      numa::destruct(*ptr);
    }

    --(this->storage_size_);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  void tensor_sparse_unvaried<T, N, X, Alloc>::storage_resize(
      index_t const new_size, index<N, X> const& ind, T value)
  {
    while (new_size < this->storage_size()) {
      pop_back();
    }

    while (new_size > this->storage_size()) {
      push_back(ind, value);
    }
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  void tensor_sparse_unvaried<T, N, X, Alloc>::storage_reserve(
      index_t const new_capa)
  {
    if (this->storage_capacity_ < new_capa) {
      constexpr std::align_val_t AL = tensor_sparse_alignment<T, N, X>();

      auto const [new_vals_mem, new_inds_mem] =
          detail::compute_vals_and_inds_mem<T, N, X>(new_capa);

      auto const [vals_mem, inds_mem] =
          detail::compute_vals_and_inds_mem<T, N, X>(this->storage_capacity_);

      void* new_ptr = allocate<AL>(alloc_, new_vals_mem + new_inds_mem);

      T* const vals_ptr = this->values().begin();
      T* const new_vals_ptr =
          detail::compute_value_ptr<T, N, X>(new_ptr, new_capa);

      index<N, X>* const inds_ptr     = this->indices().begin();
      index<N, X>* const new_inds_ptr = static_cast<index<N, X>*>(new_ptr);

      try {
        std::uninitialized_move(
            vals_ptr, vals_ptr + this->storage_size_, new_vals_ptr);
      }
      catch (...) {
        deallocate<AL>(alloc_, new_ptr, new_vals_mem + new_inds_mem);

        throw;
      }

      /*
       * Assumes that this doesn't throw.
       */
      {
        std::destroy(vals_ptr, vals_ptr + this->storage_size_);

        std::uninitialized_move(
            inds_ptr, inds_ptr + this->storage_size_, new_inds_ptr);

        std::destroy(inds_ptr, inds_ptr + this->storage_size_);
      }

      deallocate<AL>(alloc_, this->ptr_, vals_mem + inds_mem);

      this->ptr_              = new_ptr;
      this->storage_capacity_ = new_capa;
    }
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  Alloc const& tensor_sparse_unvaried<T, N, X, Alloc>::allocator() const
  {
    return alloc_;
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  memory<Alloc, tensor_sparse_alignment<T, N, X>()>
  tensor_sparse_unvaried<T, N, X, Alloc>::release() noexcept
  {
    {
      index<N, X>* ptr = this->indices().begin();
      std::destroy(ptr, ptr + this->storage_size_);
    }

    {
      T* ptr = this->values().begin();
      std::destroy(ptr, ptr + this->storage_size_);
    }

    auto const [vals_mem, inds_mem] =
        detail::compute_vals_and_inds_mem<T, N, X>(this->storage_capacity_);

    memory<Alloc, tensor_sparse_alignment<T, N, X>()> retval(
        alloc_, this->ptr_, vals_mem + inds_mem);

    this->ptr_              = nullptr;
    this->storage_capacity_ = 0;
    this->storage_size_     = 0;
    this->size_             = zero;

    return retval;
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  tensor_sparse_unvaried<T, N, X, Alloc>::tensor_sparse_unvaried(
      void* const ptr,
      index_t const storage_size,
      index_t const storage_capacity,
      index<N, X> const& size,
      Alloc alloc) noexcept
    : ptr_(ptr)
    , storage_size_(storage_size)
    , storage_capacity_(storage_capacity)
    , size_(size)
    , alloc_(std::move(alloc))
  {
  }

  /*
   * cell
   */
  template<typename T, index_t N, typename X, Allocator Alloc>
  auto tensor_sparse<T, N, X, Alloc>::unvaried() -> unvaried_type&
  {
    return (*this);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  auto tensor_sparse<T, N, X, Alloc>::unvaried() const -> unvaried_type const&
  {
    return (*this);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  auto tensor_sparse<T, N, X, Alloc>::unvaried_const() const
      -> unvaried_type const&
  {
    return (*this);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  template<index_t... Axes>
  requires(is_variation<N>(index_list<Axes...>)
               .value) auto tensor_sparse<T, N, X, Alloc>::
      vary_axes_impl(index_list_t<Axes...>)
  {
    return tensor_sparse_axes<
        tensor_sparse<T, N, X, Alloc>,
        T,
        N,
        X,
        Alloc,
        index_list_t<Axes...>>{this};
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  tensor_sparse<T, N, X, Alloc>::tensor_sparse(Alloc alloc) noexcept
    : tensor_sparse_unvaried<T, N, X, Alloc>(
          nullptr, 0, 0, zero, std::move(alloc))
  {
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  tensor_sparse<T, N, X, Alloc>::tensor_sparse(tensor_sparse&& other) noexcept
    : tensor_sparse_unvaried<T, N, X, Alloc>(
          other.ptr_,
          other.storage_size_,
          other.storage_capacity_,
          other.size_,
          std::move(other.alloc_))
  {
    other.ptr_              = nullptr;
    other.storage_size_     = 0;
    other.storage_capacity_ = 0;
    other.size_             = zero;
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  tensor_sparse<T, N, X, Alloc>&
  tensor_sparse<T, N, X, Alloc>::operator=(tensor_sparse&& other) noexcept
  {
    this->ptr_              = other.ptr_;
    this->storage_size_     = other.storage_size_;
    this->storage_capacity_ = other.storage_capacity_;
    this->size_             = other.size_;
    this->alloc_            = std::move(other.alloc_);

    other.ptr_              = nullptr;
    other.storage_size_     = 0;
    other.storage_capacity_ = 0;
    other.size_             = zero;

    return (*this);
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  tensor_sparse<T, N, X, Alloc>::~tensor_sparse()
  {
    this->release();
  }

  /*
   * Implementation of sparse-tensor-axes
   */
  template<
      typename Subclass,
      typename T,
      index_t N,
      typename X,
      Allocator Alloc,
      index_t... Axes>
  class tensor_sparse_axes<Subclass, T, N, X, Alloc, index_list_t<Axes...>> {
   public:
    constexpr static index_t K = sizeof...(Axes);

    static_assert(K >= 1);

    tensor_sparse_axes(tensor_sparse_axes const&) = delete;
    tensor_sparse_axes(tensor_sparse_axes&&)      = delete;
    tensor_sparse_axes& operator=(tensor_sparse_axes const&) = delete;
    tensor_sparse_axes& operator=(tensor_sparse_axes&&) = delete;

    explicit tensor_sparse_axes(tensor_sparse<T, N, X, Alloc>* const base)
      : base_(base)
    {
    }

    using reform_mode_t = reform_mode<Alloc, alignment_of<T>>;

    tensor_sparse_axes&& with(reform_mode_t ref_mode) &&
    {
      ref_mode_ = std::move(ref_mode);
      return (*this);
    }

    bool must_reallocate() const
    {
      return reform_mode_must_reallocate_for<T>(ref_mode_);
    }

    constexpr static index_t sel_num_axes = K;
    constexpr static index_t sel_num_dims = K;

    constexpr index<K, X> size() const
    {
      constexpr index_list_t<Axes...> ax;

      return select_at(base_->size_, ax);
    }

    constexpr index<K, X> capacity() const
    {
      return index<K, X>(one) * std::numeric_limits<index_t>::max();
    }

#define ERR_MSG "sparse-tensor axes may not be called with initializers"

    template<typename... Args>
    void resize(index<K, X> const& new_size, Args const&...)
    {
      static_assert(sizeof...(Args) == 0, ERR_MSG);

      constexpr index_list_t<Axes...> ax;

      auto const old_size = select_at(base_->size_, ax);

      if (!(is_bounded(old_size, new_size))) {
        base_->erase_if([ax, new_size](auto const& ind, auto const&) {
          return !is_bounded_strictly(select_at(ind, ax), new_size);
        });
      }

      overwrite(base_->size_, ax, new_size);
    }

    template<typename... Args>
    void resize_hard(index<K, X> const& new_size, Args const&...)
    {
      static_assert(sizeof...(Args) == 0, ERR_MSG);

      resize(new_size);

      if (new_size == index<K, X>(zero)) {
        base_->release();
      }
    }

    void reserve_hard(index<K, X> const&)
    {
      no_op;
    }

    template<typename... Args>
    void insert_size(
        index<K, X> const& base, index<K, X> const& insert_size, Args const&...)
    {
      static_assert(sizeof...(Args) == 0, ERR_MSG);

      constexpr index_list_t<Axes...> ax;

      for (auto& ind: base_->indices()) {
        auto sub_ind = select_at(ind, ax);

        for (index_t i = 0; i < K; ++i) {
          if (base[i] <= sub_ind[i]) {
            sub_ind[i] += insert_size[i];
          }
        }

        overwrite(ind, ax, sub_ind);
      }

      auto const sub_size = select_at(base_->size_, ax);

      overwrite(base_->size_, ax, sub_size + insert_size);
    }

#undef ERR_MSG

    void erase_size(index<K, X> const& base, index<K, X> const& erase_size)
    {
      constexpr index_list_t<Axes...> ax;

      auto const erase_till = (base + erase_size);

      {
        base_->erase_if([ax, base, erase_till](auto const& ind, auto const&) {
          auto const sub_ind = select_at(ind, ax);

          for (index_t i = 0; i < K; ++i) {
            if ((base[i] <= sub_ind[i]) && (sub_ind[i] < erase_till[i])) {
              return true;
            }
          }

          return false;
        });
      }

      for (auto& ind: base_->indices()) {
        auto sub_ind = select_at(ind, ax);

        for (index_t i = 0; i < K; ++i) {
          if (erase_till[i] <= sub_ind[i]) {
            sub_ind[i] -= erase_size[i];
          }
        }

        overwrite(ind, ax, sub_ind);
      }

      auto const old_size = select_at(base_->size_, ax);

      overwrite(base_->size_, ax, old_size - erase_size);
    }

   private:
    tensor_sparse<T, N, X, Alloc>* base_;
    reform_mode_t ref_mode_;
  };

  template<typename T>
  struct is_tensor_sparse_type : public std::false_type {
  };

  template<typename T, index_t N, typename X, Allocator Alloc>
  struct is_tensor_sparse_type<tensor_sparse<T, N, X, Alloc>>
    : public std::true_type {
  };

  template<
      typename T,
      index_t N,
      typename X,
      typename U,
      Allocator AllocLhs,
      Allocator AllocRhs>
  bool operator==(
      tensor_sparse_unvaried<T, N, X, AllocLhs> const& lhs,
      tensor_sparse_unvaried<U, N, X, AllocRhs> const& rhs)
  {
    if ((lhs.size() != rhs.size()) ||
        (lhs.storage_size() != rhs.storage_size())) {
      return false;
    }

    if (!numa::equal(lhs.indices(), rhs.indices())) {
      return false;
    }

    return numa::equal(lhs.values(), rhs.values());
  }

  template<
      typename T,
      index_t N,
      typename X,
      typename U,
      Allocator AllocLhs,
      Allocator AllocRhs>
  bool operator!=(
      tensor_sparse_unvaried<T, N, X, AllocLhs> const& lhs,
      tensor_sparse_unvaried<U, N, X, AllocRhs> const& rhs)
  {
    return !(lhs == rhs);
  }

  template<
      typename T,
      index_t N,
      typename X,
      Allocator Alloc,
      Allocator NewAlloc>
  tensor_sparse<T, N, X, NewAlloc> make_copy(
      tensor_sparse_unvaried<T, N, X, Alloc> const& st, NewAlloc new_alloc)
  {
    tensor_sparse<T, N, X, NewAlloc> retval(std::move(new_alloc));

    retval.storage_reserve(st.storage_size());

    numa::detail::initial_resize(retval.vary_axes(), st.size(), st.size());

    retval.insert(st);

    return retval;
  }

  template<typename T, index_t N, typename X, Allocator Alloc>
  tensor_sparse<T, N, X, Alloc>
  make_copy(tensor_sparse_unvaried<T, N, X, Alloc> const& st)
  {
    return make_copy(st, st.allocator());
  }

  //  template<WeakCell SparseTensorType,
  //           typename T, index_t N, Allocator Alloc>
  //  auto make_cell (tensor_sparse_unvaried<T, N, X, Alloc> const& st)
  //  {
  //  }
  //
  //  template<WeakCell SparseTensorType,
  //           typename... Ts, index_t N, Allocator... Allocs>
  //  auto
  //  make_cell_sorted_strictly (
  //      tensor_sparse_unvaried<Ts, N, Allocs> const&... sts)
  //  {
  //    return make_cell_span_bundle<
  //        SparseTensorType, tensor_sparse_unvaried<Ts, N, Allocs>...> {{
  //            sts... }};
  //  }

  /*
   * Implementation of for-each-bundles, etc.
   */
  template<typename T, index_t N, typename X, Allocator Alloc>
  struct for_each_bundle_impl<tensor_sparse_unvaried<T, N, X, Alloc>> {
    template<typename F, typename FC, typename U>
    static void apply(F&& f, FC&& fc, U& st)
    {
      auto fef = wrap_for_each_functor<tensor_sparse_unvaried<T, N, X, Alloc>>(
          std::forward<F>(f));

      for (auto& value: st.values()) {
        if (!fef(value).continue_) {
          return;
        }
      }
    }

    template<typename F, typename FC, typename U>
    static void apply_indexed(F&& f, FC&& fc, U& st)
    {
      auto fef =
          wrap_for_each_functor_indexed<tensor_sparse_unvaried<T, N, X, Alloc>>(
              std::forward<F>(f));

      auto* v_ptr           = st.values().begin();
      auto* const end_v_ptr = v_ptr + st.storage_size();

      auto* i_ptr = st.indices().begin();

      for (; v_ptr != end_v_ptr; ++v_ptr, ++i_ptr) {
        if (!fef(*i_ptr, *v_ptr).continue_) {
          return;
        }
      }
    }
  };

  namespace detail {
    template<typename X, typename WeakSpan, typename... WeakSpans, typename F>
    auto wrap_sparse_for_each_functor_indexed(F&& f)
    {
      constexpr index_t N = WeakSpan::axes_info.num_dims;

      using ResultType = std::invoke_result_t<
          F,
          index<N, X>,
          typename WeakSpan::element_type*,
          typename WeakSpans::element_type*...>;

      if constexpr (std::is_same_v<ResultType, void>) {
        return [f = std::forward<F>(f)](auto&&... values) mutable {
          f(std::forward<decltype(values)>(values)...);
          return true;
        };
      }
      else {
        static_assert(std::is_same_v<ResultType, bool>);

        return std::forward<F>(f);
      }
    }

    template<
        index_t N,
        typename X,
        typename F,
        std::size_t... Is,
        typename... Ts>
    void for_each_indexed_impl(F&& f, std::index_sequence<Is...>, Ts&... spans)
    {
      ASSERT(is_all_equal(spans.size()...));

      auto fef = wrap_sparse_for_each_functor_indexed<X, decay_t<Ts>...>(
          std::forward<F>(f));

      static_assert(sizeof...(Is) == sizeof...(Ts));

      std::array<index<N, X> const*, sizeof...(Is)> ind_its{
          spans.indices().begin()...};

      const std::array<index<N, X> const*, sizeof...(Is)> ind_end_its{
          (spans.indices().begin() + spans.storage_size())...};

      std::tuple val_its{spans.values().begin()...};

      while (ind_its != ind_end_its) {
        std::size_t const min_ind = [&] {
          std::size_t retval = 0;
          bool inited        = false;

          for (std::size_t i = 0; i < sizeof...(Is); ++i) {
            if (ind_its[i] != ind_end_its[i]) {
              if (!inited) {
                retval = i;
                inited = true;
              }
              else {
                if (*ind_its[i] < *ind_its[retval]) {
                  retval = i;
                }
              }
            }
          }

          return retval;
        }();

        ASSERT(ind_its[min_ind] != ind_end_its[min_ind]);

        std::tuple min_val_its = val_its;

        for (std::size_t i = 0; i < sizeof...(Is); ++i) {
          if ((ind_its[i] != ind_end_its[i]) &&
              !(*ind_its[min_ind] < *ind_its[i])) {
            if (i != min_ind) {
              ++ind_its[i];
            }

            dynamic_tuple_at([](auto& it) { ++it; }, i, val_its);
          }
          else {
            dynamic_tuple_at([](auto& it) { it = nullptr; }, i, min_val_its);
          }
        }

        index<N, X> const act_ind = (*ind_its[min_ind]);

        if (!fef(act_ind, std::get<Is>(min_val_its)...)) {
          return;
        }

        ++ind_its[min_ind];
      }
    }
  }

  template<typename... Ts, index_t N, typename X, Allocator... Allocs>
  requires(sizeof...(Ts) >= 2) struct for_each_bundle_impl<
      tensor_sparse_unvaried<Ts, N, X, Allocs>...> {
    template<typename F, typename FC, typename... Us>
    static void apply(F&& f, FC&& fc, Us&... sts)
    {
      apply_indexed(
          prepend_dummy_parameter(std::forward<F>(f)),
          std::forward<FC>(fc),
          sts...);
    }

    template<typename F, typename FC, typename... Us>
    static void apply_indexed(F&& f, FC&& fc, Us&... sts)
    {
      detail::for_each_indexed_impl<N, X>(
          std::forward<F>(f),
          std::make_index_sequence<sizeof...(sts)>(),
          sts...);
    }
  };

  template<
      typename T,
      typename U,
      index_t N,
      typename X,
      Allocator AllocLhs,
      Allocator AllocRhs,
      Allocator Alloc = default_allocator>
  tensor_sparse<std::common_type_t<T, U>, N, X, Alloc>
  simple_add_sorted_strictly(
      T const& lhs_factor,
      tensor_sparse_unvaried<T, N, X, AllocLhs> const& lhs,
      U const& rhs_factor,
      tensor_sparse_unvaried<U, N, X, AllocRhs> const& rhs,
      Alloc alloc)
  {
    ASSERT(lhs.size() == rhs.size());

    tensor_sparse<std::common_type_t<T, U>, N, X, Alloc> retval(
        std::move(alloc));

    numa::detail::initial_resize(retval.vary_axes(), lhs.size(), lhs.size());

    for_each(lhs, rhs).indexed([&](auto const ind, T const* a, U const* b) {
      if (a == nullptr) {
        retval.push_back(ind, rhs_factor * (*b));
      }
      else if (b == nullptr) {
        retval.push_back(ind, lhs_factor * (*a));
      }
      else {
        retval.push_back(ind, (lhs_factor * (*a)) + (rhs_factor * (*b)));
      }
    });

    return retval;
  }
}
