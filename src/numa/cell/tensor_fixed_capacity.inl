namespace numa {
  template<typename T, index_t... Sizes>
  class tensor_fixed_capacity_iterator
    : public iterator_facade<tensor_fixed_capacity_iterator<T, Sizes...>, T> {
   public:
    static constexpr index_t N = sizeof...(Sizes);

    static_assert(N >= 1);

    tensor_fixed_capacity_iterator(
        T* ptr, index<N> const& current, index<N> const& size)
      : ptr_(ptr), current_(current), size_(size)
    {
    }

    T* ptr() const
    {
      return ptr_;
    }

    void increment()
    {
      ptr_ += detail::tensor_increment(
          current_,
          index<N>(zero),
          size_,
          detail::capacity_to_stride(index(Sizes...)));
    }

    void decrement()
    {
      ptr_ += detail::tensor_decrement(
          current_,
          index<N>(zero),
          size_,
          detail::capacity_to_stride(index(Sizes...)));
    }

    void advance(index_t const offset)
    {
      ptr_ += detail::tensor_advance(
          current_,
          offset,
          index<N>(zero),
          size_,
          detail::capacity_to_stride(index(Sizes...)));
    }

    index_t distance_to(tensor_fixed_capacity_iterator const& other) const
    {
      return detail::tensor_distance_to(
          current_, other.current_, index<N>(zero), size_);
    }

    index<N> const& pos() const
    {
      return current_;
    }

    bool equal_to(tensor_fixed_capacity_iterator const& other) const
    {
      return (ptr_ == other.ptr_);
    }

    T& dereference() const
    {
      return (*ptr_);
    }

   private:
    T* ptr_;
    index<N> current_;
    index<N> size_;
  };

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity_unvaried<T, Sizes...>::size() const
      -> index<N> const&
  {
    return this->size_;
  }

  template<typename T, index_t... Sizes>
  constexpr auto tensor_fixed_capacity_unvaried<T, Sizes...>::capacity() const
      -> index<N>
  {
    return index<N>(Sizes...);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity_unvaried<T, Sizes...>::span() -> span_type
  {
    return {ptr(), size(), detail::capacity_to_stride(index(Sizes...))};
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity_unvaried<T, Sizes...>::span() const
      -> span_const_type
  {
    return span_const();
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity_unvaried<T, Sizes...>::span_const() const
      -> span_const_type
  {
    return {const_ptr(), size(), detail::capacity_to_stride(index(Sizes...))};
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity_unvaried<T, Sizes...>::begin() -> iterator_type
  {
    return iterator_type(ptr(), index<N>(zero), size_);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity_unvaried<T, Sizes...>::begin() const
      -> iterator_const_type
  {
    return begin_const();
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity_unvaried<T, Sizes...>::begin_const() const
      -> iterator_const_type
  {
    return iterator_const_type(const_ptr(), index<N>(zero), size_);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity_unvaried<T, Sizes...>::end() -> iterator_type
  {
    return iterator_type(ptr() + end_offset(), index<N>(zero), size_);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity_unvaried<T, Sizes...>::end() const
      -> iterator_const_type
  {
    return iterator_const_type(
        const_ptr() + end_offset(), index<N>(zero), size_);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity_unvaried<T, Sizes...>::end_const() const
      -> iterator_const_type
  {
    return iterator_const_type(
        const_ptr() + end_offset(), index<N>(zero), size_);
  }

  template<typename T, index_t... Sizes>
  T*
  tensor_fixed_capacity_unvaried<T, Sizes...>::get_ptr_impl(index<N> const& ind)
  {
    return ptr() + dot(ind, detail::capacity_to_stride(index(Sizes...)));
  }

  template<typename T, index_t... Sizes>
  T const* tensor_fixed_capacity_unvaried<T, Sizes...>::const_get_ptr_impl(
      index<N> const& ind) const
  {
    return const_ptr() + dot(ind, detail::capacity_to_stride(index(Sizes...)));
  }

  template<typename T, index_t... Sizes>
  bool tensor_fixed_capacity_unvaried<T, Sizes...>::is_bounded_impl(
      index<N> const& ind) const
  {
    return is_bounded(index<N>(zero), ind) && is_bounded_strictly(ind, size());
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity_unvaried<T, Sizes...>::iterator_impl(
      index<N> const& ind)
  {
    return iterator_type(get_ptr_impl(ind), ind, size_);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity_unvaried<T, Sizes...>::const_iterator_impl(
      index<N> const& ind) const
  {
    return iterator_const_type(const_get_ptr_impl(ind), ind, size_);
  }

  template<typename T, index_t... Sizes>
  tensor_fixed_capacity_unvaried<T, Sizes...>::tensor_fixed_capacity_unvaried(
      index<N> const& size)
    : size_(size)
  {
  }

  template<typename T, index_t... Sizes>
  index_t tensor_fixed_capacity_unvaried<T, Sizes...>::end_offset() const
  {
    return dot(size() - index<N>(one),
               detail::capacity_to_stride(index(Sizes...))) +
        1;
  }

  template<typename T, index_t... Sizes>
  T* tensor_fixed_capacity_unvaried<T, Sizes...>::ptr()
  {
    return static_cast<T*>(static_cast<void*>(&m_data[0]));
  }

  template<typename T, index_t... Sizes>
  T const* tensor_fixed_capacity_unvaried<T, Sizes...>::const_ptr() const
  {
    return static_cast<T const*>(static_cast<void const*>(&m_data[0]));
  }

  /*
   * cell
   */
  template<typename T, index_t... Sizes>
  tensor_fixed_capacity<T, Sizes...>::tensor_fixed_capacity()
    : tensor_fixed_capacity_unvaried<T, Sizes...>()
  {
  }

  template<typename T, index_t... Sizes>
  tensor_fixed_capacity<T, Sizes...>::tensor_fixed_capacity(
      tensor_fixed_capacity const& other)
    : tensor_fixed_capacity_unvaried<T, Sizes...>(other.size_)
  {
    auto ss  = this->span();
    ss.size_ = other.size();

    for_each(ss, other.span())(f_copy_construct, f_destruct);

    this->size_ = other.size_;
  }

  /*
   * TODO: assignment operators should not first destruct everything
   * and the reconstruct.
   */
  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity<T, Sizes...>::operator=(
      tensor_fixed_capacity const& other) -> tensor_fixed_capacity&
  {
    auto ss = this->span();

    for_each(ss)(f_destruct);

    ss.size_ = other.size();

    for_each(ss, other.span())(f_copy_assign, f_destruct);

    this->size_ = other.size_;

    return *this;
  }

  template<typename T, index_t... Sizes>
  tensor_fixed_capacity<T, Sizes...>::tensor_fixed_capacity(
      tensor_fixed_capacity&& other)
  {
    auto ss  = this->span();
    ss.size_ = other.size();

    for_each(ss, other.span())(f_move_construct, f_destruct);

    this->size_ = other.size_;
  }

  template<typename T, index_t... Sizes>
  auto
  tensor_fixed_capacity<T, Sizes...>::operator=(tensor_fixed_capacity&& other)
      -> tensor_fixed_capacity&
  {
    auto ss = this->span();

    for_each(ss)(f_destruct);

    ss.size_ = other.size();

    for_each(ss, other.span())(f_move_assign, f_destruct);

    this->size_ = other.size_;

    return *this;
  }

  template<typename T, index_t... Sizes>
  tensor_fixed_capacity<T, Sizes...>::~tensor_fixed_capacity()
  {
    for_each(this->span())(f_destruct);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity<T, Sizes...>::unvaried() -> unvaried_type&
  {
    return (*this);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity<T, Sizes...>::unvaried() const
      -> const unvaried_type&
  {
    return (*this);
  }

  template<typename T, index_t... Sizes>
  auto tensor_fixed_capacity<T, Sizes...>::unvaried_const() const
      -> const unvaried_type&
  {
    return (*this);
  }

  template<typename T, index_t... Sizes>
  constexpr index_t
  tensor_fixed_capacity<T, Sizes...>::required_elements(index<N> const& size)
  {
    ASSERT(size <= index(Sizes...));

    return 0;
  }

  template<typename T, index_t... Sizes>
  template<index_t... Axes>
  constexpr auto tensor_fixed_capacity<T, Sizes...>::vary_axes_impl(
      index_list_t<Axes...>) requires(is_variation<N>(index_list<Axes...>)
                                          .value)
  {
    return tensor_fixed_capacity_axes<T, index_list_t<Axes...>, Sizes...>{this};
  }

  template<typename T, index_t... SelAxes, index_t... Sizes>
  class tensor_fixed_capacity_axes<T, index_list_t<SelAxes...>, Sizes...>
    : public cell_axes_facade_light<
          tensor_fixed_capacity_axes<T, index_list_t<SelAxes...>, Sizes...>,
          tensor_fixed_capacity<T, Sizes...>,
          index_list_t<SelAxes...>> {
   public:
    using baseclass = cell_axes_facade_light<
        tensor_fixed_capacity_axes<T, index_list_t<SelAxes...>, Sizes...>,
        tensor_fixed_capacity<T, Sizes...>,
        index_list_t<SelAxes...>>;

    using baseclass::baseclass;

    using baseclass::sel_num_axes;

    using parent = typename baseclass::parent;

    auto size() const
    {
      return select_at(this->cell_->size_, index_list<SelAxes...>);
    }

    auto capacity() const
    {
      return select_at(index(Sizes...), index_list<SelAxes...>);
    }

    bool must_reallocate() const
    {
      return false;
    }

    void move_assign(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        index<sel_num_axes> const& target);

    template<typename... Args>
    void fill_assign(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        Args const&... args);

    void move_construct(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        index<sel_num_axes> const& target);

    template<typename... Args>
    void fill_construct(
        index<sel_num_axes> const& base,
        index<sel_num_axes> const& form,
        Args const&... args)
    {
      for_each(baseclass::get_span(base, form, *this->cell_))(
          f_fill_construct_t{args...}, f_destruct);
    }

    void
    destruct(index<sel_num_axes> const& base, index<sel_num_axes> const& form)
    {
      for_each(baseclass::get_span(base, form, *this->cell_))(f_destruct);
    }

    void set_size(index<sel_num_axes> const& new_size)
    {
      overwrite(this->cell_->size_, index_list<SelAxes...>, new_size);
    }

    translocation_forbidden_t translocation(index<sel_num_axes> const&)
    {
      return {};
    }

    template<typename TupleLike, typename... Args>
    void inplace(
        index<sel_num_axes> const& new_size,
        TupleLike const& inplace_fs,
        Args const&... args) requires(std::tuple_size_v<TupleLike> == sel_num_axes)
    {
      auto all_tup = detail::expand_reform_axis_tuple(
          parent::axes_info,
          baseclass::sel_axes,
          inplace_fs,
          detail::inplace_trivial{},
          this->cell_->size());

      auto span = this->cell_->span();

      index_list_t<SelAxes...> ax;
      overwrite(span.size_, ax, select_at(this->cell_->capacity(), ax));

      try {
        detail::apply_axis_target(
            [&](auto const mode, auto const& from, auto const& to) {
              detail::execute_op_mode(mode, from, to, span, span, args...);
            },
            all_tup);
      }
      catch (...) {
        set_size(zero);
        throw;
      }

      set_size(new_size);
    }
  };

  /*
   * for-each
   */
  template<typename... Ts, index_t... Sizes>
  struct for_each_bundle_impl<tensor_fixed_capacity_unvaried<Ts, Sizes...>...>
    : public iterator_based_for_each_bundle_impl_facade {
  };

  template<typename T, index_t... Sizes>
  bool operator==(
      tensor_fixed_capacity_unvaried<T, Sizes...> const& lhs,
      tensor_fixed_capacity_unvaried<T, Sizes...> const& rhs)
  {
    if (lhs.size() != rhs.size()) {
      return false;
    }

    return all_of(f_is_equal, lhs, rhs);
  }

  template<typename T, index_t... Sizes>
  bool operator!=(
      tensor_fixed_capacity_unvaried<T, Sizes...> const& lhs,
      tensor_fixed_capacity_unvaried<T, Sizes...> const& rhs)
  {
    return !(lhs == rhs);
  }
}
