namespace numa {
  template<Span S, IndexList Ls, IndexList Rs>
  t_span_adaptor_band<S, Ls, Rs>::t_span_adaptor_band(S const& span)
    : t_span_adaptor_bounded_facade<
          S,
          raster_band<Ls, Rs, S::axes_info.num_dims>>(
          span,
          raster_band<Ls, Rs, S::axes_info.num_dims>{
              S::axes_info.size_to_form(span.size())})
  {
  }

  template<Span... S, IndexList... Ls, IndexList... Rs>
  class for_each_bundle_impl<t_span_adaptor_band<S, Ls, Rs>...>
    : public generic_for_each_bundle_impl_facade {
  };

  /*
   * Free functions.
   */
  template<Span S>
  auto diagonal(S const& span)
  {
    constexpr index_t Nd = S::axes_info.num_dims;

    return t_span_adaptor_band<
        S,
        index_list_uniform_t<Nd / 2, 0>,
        index_list_uniform_t<Nd / 2, 0>>(span);
  }

  template<index_t Bandwidth, Span S>
  auto band(S const& span, c_index_t<Bandwidth>)
  {
    constexpr index_t Nd = S::axes_info.num_dims;

    return t_span_adaptor_band<
        S,
        index_list_uniform_t<Nd / 2, Bandwidth>,
        index_list_uniform_t<Nd / 2, Bandwidth>>(span);
  }

  template<index_t Bandwidth, Span S>
  auto band_upper(S const& span, c_index_t<Bandwidth>)
  {
    constexpr index_t Nd = S::axes_info.num_dims;

    return t_span_adaptor_band<
        S,
        index_list_uniform_t<Nd / 2, 0>,
        index_list_uniform_t<Nd / 2, Bandwidth>>(span);
  }

  template<index_t Bandwidth, Span S>
  auto band_lower(S const& span, c_index_t<Bandwidth>)
  {
    constexpr index_t Nd = S::axes_info.num_dims;

    return t_span_adaptor_band<
        S,
        index_list_uniform_t<Nd / 2, Bandwidth>,
        index_list_uniform_t<Nd / 2, 0>>(span);
  }

  template<index_t Bandwidth, Span S>
  auto band_upper_strictly(S const& span, c_index_t<Bandwidth>)
  {
    constexpr index_t Nd = S::axes_info.num_dims;

    return t_span_adaptor_band<
        S,
        index_list_uniform_t<Nd / 2, -1>,
        index_list_uniform_t<Nd / 2, Bandwidth>>(span);
  }

  template<index_t Bandwidth, Span S>
  auto band_lower_strictly(S const& span, c_index_t<Bandwidth>)
  {
    constexpr index_t Nd = S::axes_info.num_dims;

    return t_span_adaptor_band<
        S,
        index_list_uniform_t<Nd / 2, Bandwidth>,
        index_list_uniform_t<Nd / 2, -1>>(span);
  }

  template<Span S, index_t... Ls, index_t... Rs>
  auto band(S const& span, index_list_t<Ls...>, index_list_t<Rs...>)
  {
    return t_span_adaptor_band<S, index_list_t<Ls...>, index_list_t<Rs...>>(
        span);
  }

  /*
   * Mutable cell wrappers.
   */
  template<Cell C>
  auto diagonal(C& cell)
  {
    return diagonal(cell.span());
  }

  template<index_t Bandwidth, Cell C>
  auto band(C& cell, c_index_t<Bandwidth> bw)
  {
    return band(cell.span(), bw);
  }

  template<index_t Bandwidth, Cell C>
  auto band_upper(C& cell, c_index_t<Bandwidth> bw)
  {
    return band_upper(cell.span(), bw);
  }

  template<index_t Bandwidth, Cell C>
  auto band_lower(C& cell, c_index_t<Bandwidth> bw)
  {
    return band_lower(cell.span(), bw);
  }

  template<index_t Bandwidth, Cell C>
  auto band_upper_strictly(C& cell, c_index_t<Bandwidth> bw)
  {
    return band_upper_strictly(cell.span(), bw);
  }

  template<index_t Bandwidth, Cell C>
  auto band_lower_strictly(C& cell, c_index_t<Bandwidth> bw)
  {
    return band_lower_strictly(cell.span(), bw);
  }

  template<Cell C, index_t... Ls, index_t... Rs>
  auto band(C& cell, index_list_t<Ls...> ls, index_list_t<Rs...> rs)
  {
    return band(cell.span(), ls, rs);
  }
}
