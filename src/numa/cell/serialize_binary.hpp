#ifndef INCLUDE_GUARD_NUMA_CELL_SERIALIZE_BINARY_H_
#define INCLUDE_GUARD_NUMA_CELL_SERIALIZE_BINARY_H_

#include "concepts.hpp"
#include "make_cell.hpp"
#include "structure.hpp"
#include "tensor_sparse.hpp"

#include "numa/core/serialize_binary.hpp"

namespace numa {
  /*
   * Generic span
   */
  template<Span T>
  void to_binary(std::ostream&, T const& span);

  template<Cell T>
  requires(!Span<T>) void to_binary(std::ostream&, T const& cell);

  template<typename T>
  requires Span<decay_t<T>> void from_binary(std::istream&, T&& span);

  template<Cell T>
  void from_binary(std::istream&, T& cell);

  template<Cell T>
  T make_from_binary_impl(c_type_t<T>, std::istream&);
}

#include "serialize_binary.inl"

#endif
