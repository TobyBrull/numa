namespace numa {
  /*
   * tensor-unvaried
   */
  template<typename T, index_t N>
  index<N> const& tensor_unvaried<T, N>::size() const
  {
    return size_;
  }

  template<typename T, index_t N>
  index<N> const& tensor_unvaried<T, N>::capacity() const
  {
    return capacity_;
  }

  template<typename T, index_t N>
  auto tensor_unvaried<T, N>::span() -> span_type
  {
    return {
        static_cast<T*>(this->ptr_),
        this->size_,
        detail::capacity_to_stride(this->capacity_)};
  }

  template<typename T, index_t N>
  auto tensor_unvaried<T, N>::span() const -> span_const_type
  {
    return span_const();
  }

  template<typename T, index_t N>
  auto tensor_unvaried<T, N>::span_const() const -> span_const_type
  {
    return {
        static_cast<T const*>(this->ptr_),
        this->size_,
        detail::capacity_to_stride(this->capacity_)};
  }

  template<typename T, index_t N>
  auto tensor_unvaried<T, N>::begin() -> iterator_type
  {
    return t_span_iterator<T, N>(
        this->ptr_, zero, size_, detail::capacity_to_stride(capacity_));
  }

  template<typename T, index_t N>
  auto tensor_unvaried<T, N>::begin() const -> iterator_const_type
  {
    return begin_const();
  }

  template<typename T, index_t N>
  auto tensor_unvaried<T, N>::begin_const() const -> iterator_const_type
  {
    return t_span_iterator<T const, N>(
        this->ptr_, zero, size_, detail::capacity_to_stride(capacity_));
  }

  template<typename T, index_t N>
  auto tensor_unvaried<T, N>::end() -> iterator_type
  {
    if (is_empty(this->size_)) {
      return begin();
    }
    else {
      return t_span_iterator<T, N>{
          get_ptr_impl(this->size_ - index<N>(one)) + 1,
          detail::end_index(index<N>(zero), this->size_),
          size_,
          detail::capacity_to_stride(capacity_)};
    }
  }

  template<typename T, index_t N>
  auto tensor_unvaried<T, N>::end() const -> iterator_const_type
  {
    return end_const();
  }

  template<typename T, index_t N>
  auto tensor_unvaried<T, N>::end_const() const -> iterator_const_type
  {
    if (is_empty(this->size_)) {
      return begin();
    }
    else {
      return t_span_iterator<T const, N>{
          const_get_ptr_impl(this->size_ - index<N>(one)) + 1,
          detail::end_index(index<N>(zero), this->size_),
          size_,
          detail::capacity_to_stride(capacity_)};
    }
  }

  template<typename T, index_t N>
  T* tensor_unvaried<T, N>::get_ptr_impl(index<N> const& ind)
  {
    return this->ptr_ + detail::capacity_dot(ind, this->capacity_);
  }

  template<typename T, index_t N>
  T const* tensor_unvaried<T, N>::const_get_ptr_impl(index<N> const& ind) const
  {
    return this->ptr_ + detail::capacity_dot(ind, this->capacity_);
  }

  template<typename T, index_t N>
  bool tensor_unvaried<T, N>::is_bounded_impl(index<N> const& ind) const
  {
    return is_bounded(index<N>(zero), ind) && is_bounded_strictly(ind, size_);
  }

  template<typename T, index_t N>
  auto tensor_unvaried<T, N>::iterator_impl(index<N> const& ind)
  {
    return iterator_type(
        get_ptr_impl(ind), ind, size_, detail::capacity_to_stride(capacity_));
  }

  template<typename T, index_t N>
  auto tensor_unvaried<T, N>::const_iterator_impl(index<N> const& ind) const
  {
    return iterator_const_type(
        const_get_ptr_impl(ind),
        ind,
        size_,
        detail::capacity_to_stride(capacity_));
  }

  template<typename T, index_t N>
  tensor_unvaried<T, N>::tensor_unvaried(
      T* ptr, index<N> const& size, index<N> const& capacity) noexcept
    : ptr_(ptr), size_(size), capacity_(capacity)
  {
  }

  /*
   * for-each
   */
  template<typename... Ts, index_t N>
  struct for_each_bundle_impl<tensor_unvaried<Ts, N>...>
    : public span_based_for_each_bundle_impl_facade {
  };

  /*
   * tensor
   */
  template<typename T, index_t N, Allocator Alloc>
  tensor<T, N, Alloc>::tensor(Alloc alloc) noexcept
    : tensor_unvaried<T, N>(nullptr, zero, zero), alloc_(std::move(alloc))
  {
  }

  template<typename T, index_t N, Allocator Alloc>
  tensor<T, N, Alloc>::tensor(tensor&& other) noexcept
    : tensor_unvaried<T, N>(other.ptr_, other.size_, other.capacity_)
    , alloc_(std::move(other.alloc_))
  {
    other.ptr_      = nullptr;
    other.size_     = zero;
    other.capacity_ = zero;
  }

  template<typename T, index_t N, Allocator Alloc>
  auto tensor<T, N, Alloc>::operator=(tensor&& other) noexcept -> tensor&
  {
    for_each (*this)(f_destruct);

    deallocate_for<T>(alloc_, this->ptr_, required_elements(this->capacity()));

    this->ptr_      = other.ptr_;
    this->size_     = other.size_;
    this->capacity_ = other.capacity_;

    alloc_ = std::move(other.alloc_);

    other.ptr_      = nullptr;
    other.size_     = zero;
    other.capacity_ = zero;

    return (*this);
  }

  template<typename T, index_t N, Allocator Alloc>
  tensor<T, N, Alloc>::~tensor()
  {
    for_each (*this)(f_destruct);

    deallocate_for<T>(alloc_, this->ptr_, required_elements(this->capacity()));
  }

  template<typename T, index_t N, Allocator Alloc>
  Alloc const& tensor<T, N, Alloc>::allocator() const
  {
    return alloc_;
  }

  template<typename T, index_t N, Allocator Alloc>
  constexpr index_t tensor<T, N, Alloc>::required_elements(index<N> const& size)
  {
    return product(size);
  }

  template<typename T, index_t N, Allocator Alloc>
  memory_for<Alloc, T> tensor<T, N, Alloc>::release()
  {
    for_each (*this)(f_destruct);

    memory_for<Alloc, T> retval(
        alloc_, this->ptr_, required_elements(this->capacity()) * sizeof(T));

    this->ptr_      = nullptr;
    this->size_     = zero;
    this->capacity_ = zero;

    return retval;
  }

  template<typename T, index_t N, Allocator Alloc>
  auto tensor<T, N, Alloc>::unvaried() -> unvaried_type&
  {
    return (*this);
  }

  template<typename T, index_t N, Allocator Alloc>
  auto tensor<T, N, Alloc>::unvaried() const -> unvaried_type const&
  {
    return (*this);
  }

  template<typename T, index_t N, Allocator Alloc>
  auto tensor<T, N, Alloc>::unvaried_const() const -> unvaried_type const&
  {
    return (*this);
  }

  template<typename T, index_t N, Allocator Alloc>
  template<index_t... Axes>
  requires(is_variation<N>(index_list<Axes...>)
               .value) constexpr auto tensor<T, N, Alloc>::
      vary_axes_impl(index_list_t<Axes...>)
  {
    return tensor_axes<T, N, Alloc, index_list_t<Axes...>>{this};
  }

  template<typename T, index_t N, Allocator Alloc, index_t... SelAxes>
  class tensor_axes<T, N, Alloc, index_list_t<SelAxes...>>
    : public cell_axes_facade<
          tensor_axes<T, N, Alloc, index_list_t<SelAxes...>>,
          tensor<T, N, Alloc>,
          index_list_t<SelAxes...>> {
   public:
    using facade = cell_axes_facade<
        tensor_axes<T, N, Alloc, index_list_t<SelAxes...>>,
        tensor<T, N, Alloc>,
        index_list_t<SelAxes...>>;

    using facade::facade;

    constexpr static index_t Ka = sizeof...(SelAxes);

    static void set_ptr(tensor<T, N, Alloc>& cell, T* ptr)
    {
      cell.ptr_ = ptr;
    }

    static void
    set_size_total(tensor<T, N, Alloc>& cell, index<N> const& new_size_total)
    {
      cell.size_ = new_size_total;
    }

    static void
    set_capacity_total(tensor<T, N, Alloc>& cell, index<N> const& new_cap_total)
    {
      cell.capacity_ = new_cap_total;
    }
  };

  template<typename T, index_t N>
  bool
  operator==(tensor_unvaried<T, N> const& lhs, tensor_unvaried<T, N> const& rhs)
  {
    if (lhs.size() != rhs.size()) {
      return false;
    }

    return all_of(f_is_equal, lhs, rhs);
  }

  template<typename T, index_t N>
  bool
  operator!=(tensor_unvaried<T, N> const& lhs, tensor_unvaried<T, N> const& rhs)
  {
    return !(lhs == rhs);
  }
}
