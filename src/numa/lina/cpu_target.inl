namespace numa {
  inline auto f_effort_cpu_target()
  {
    return [](cpu_target const ct) {
      switch (ct) {
        case cpu_target::generic:
          return 2.0;
        case cpu_target::sse4_2:
          return 1.0;
        case cpu_target::avx:
          return 0.5;
        case cpu_target::avx2:
          return 0.25;
        case cpu_target::avx512:
          return 0.125;
      }

      UNREACHABLE();
    };
  }
}
