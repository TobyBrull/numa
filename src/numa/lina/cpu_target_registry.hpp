#ifndef INCLUDE_GUARD_NUMA_LINA_CPU_TARGET_REGISTRY_H_
#define INCLUDE_GUARD_NUMA_LINA_CPU_TARGET_REGISTRY_H_

#include "cpu_target.hpp"

#include "numa/tmpl/type_list.hpp"

#include <functional>
#include <map>

namespace numa {
  class cpu_target_registry {
   public:
    template<typename Func>
    static bool
    register_function(Func** ptr, std::map<cpu_target, Func*> cpu_targets);

    template<typename Func>
    static bool register_function(
        Func** ptr,
        std::initializer_list<std::pair<cpu_target, Func*>> specials);

    template<typename... Ts>
    static auto pattern(Ts...);

    static void execute(cpu_target);

   private:
    using handler_t = std::function<void(cpu_target)>;

    static std::vector<handler_t>& handlers();
  };
}

#include "cpu_target_registry.inl"

#endif
