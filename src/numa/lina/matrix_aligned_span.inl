namespace numa {
  namespace detail {
    template<index_t Q>
    inline index_t matrix_aligned_ptr_offset(
        index_t const major_block,
        index_t const major_inner,
        index_t const minor_pos,
        index_t const minor_capacity)
    {
      index_t retval = major_inner;
      retval += minor_pos * Q;
      retval += major_block * Q * minor_capacity;
      return retval;
    }

    template<major Major, index_t Q>
    inline index_t
    matrix_aligned_ptr_offset(index<2> const& ind, index_t const minor_capacity)
    {
      if constexpr (Major == major::left) {
        index_t const major_block = ind[0] / Q;
        index_t const major_inner = ind[0] % Q;
        index_t const minor_pos   = ind[1];

        return matrix_aligned_ptr_offset<Q>(
            major_block, major_inner, minor_pos, minor_capacity);
      }
      else {
        index_t const major_block = ind[1] / Q;
        index_t const major_inner = ind[1] % Q;
        index_t const minor_pos   = ind[0];

        return matrix_aligned_ptr_offset<Q>(
            major_block, major_inner, minor_pos, minor_capacity);
      }
    }
  }

  template<typename T, major Major, index_t Q>
  class matrix_aligned_span_iterator
    : public iterator_facade<matrix_aligned_span_iterator<T, Major, Q>, T> {
   public:
    matrix_aligned_span_iterator(
        T* ptr,
        index<2> const& ind,
        index<2> const& size,
        index_t minor_capacity)
      : ptr_(ptr), minor_capacity_(minor_capacity)
    {
      if constexpr (Major == major::left) {
        major_block_size_ = size[0] / Q;
        major_inner_size_ = size[0] % Q;
        minor_size_       = size[1];
      }
      else {
        major_block_size_ = size[1] / Q;
        major_inner_size_ = size[1] % Q;
        minor_size_       = size[0];
      }

      if (ind == index(-1, -1)) {
        major_block_ = 0;
        major_inner_ = 0;
        minor_pos_   = 0;

        if ((size[0] > 0) && (size[1] > 0)) {
          major_block_ = (major_block_size_ + 1);

          ptr_ += detail::matrix_aligned_ptr_offset<Major, Q>(
              size - index<2>(one), minor_capacity_);

          ptr_ += 1;
        }
      }
      else {
        if constexpr (Major == major::left) {
          major_block_ = ind[0] / Q;
          major_inner_ = ind[0] % Q;
          minor_pos_   = ind[1];
        }
        else {
          major_block_ = ind[1] / Q;
          major_inner_ = ind[1] % Q;
          minor_pos_   = ind[0];
        }

        ptr_ += detail::matrix_aligned_ptr_offset<Q>(
            major_block_, major_inner_, minor_pos_, minor_capacity_);
      }
    }

    T* ptr() const
    {
      return ptr_;
    }

    index<2> pos() const
    {
      if constexpr (Major == major::left) {
        return {major_block_ * Q + major_inner_, minor_pos_};
      }
      else {
        return {minor_pos_, major_block_ * Q + major_inner_};
      }
    }

    void increment()
    {
      ++ptr_;
      ++major_inner_;

      if ((major_inner_ == Q) ||
          ((major_block_ == major_block_size_) &&
           (major_inner_ == major_inner_size_))) {
        bool const in_last_block_which_is_partial = (major_inner_ != Q);

        major_inner_ = 0;
        ++minor_pos_;

        if (minor_pos_ == minor_size_) {
          minor_pos_ = 0;

          bool const is_exact = (major_inner_size_ == 0);

          if ((!is_exact && (major_block_ < major_block_size_)) ||
              (is_exact && (major_block_ + 1 < major_block_size_))) {
            ptr_ += Q * (minor_capacity_ - minor_size_);
          }

          ++major_block_;
        }
        else if (in_last_block_which_is_partial) {
          ptr_ += (Q - major_inner_size_);
        }
      }
    }

    void decrement()
    {
      NOT_IMPLEMENTED();
    }

    void advance(index_t const offset)
    {
      NOT_IMPLEMENTED();
    }

    index_t distance_to(matrix_aligned_span_iterator const& other) const
    {
      NOT_IMPLEMENTED();
    }

    bool equal_to(matrix_aligned_span_iterator const& other) const
    {
      return (ptr_ == other.ptr_);
    }

    T& dereference() const
    {
      return (*ptr_);
    }

   private:
    T* ptr_ = nullptr;

    index_t major_block_ = 0;
    index_t major_inner_ = 0;
    index_t minor_pos_   = 0;

    index_t major_block_size_ = 0;
    index_t major_inner_size_ = 0;
    index_t minor_size_       = 0;

    index_t minor_capacity_ = 0;
  };

  template<typename T, major Major, index_t Q>
  constexpr matrix_aligned_span<T, Major, Q>::matrix_aligned_span(
      T* const origin, index<2> const& size, index_t const minor_capacity)
    : origin_(origin), size_(size), minor_capacity_(minor_capacity)
  {
    if constexpr (Major == major::left) {
      ASSERT(minor_capacity_ >= size_[1]);
    }
    else {
      ASSERT(minor_capacity_ >= size_[0]);
    }
  }

  template<typename T, major Major, index_t Q>
  constexpr matrix_aligned_span<T, Major, Q>::
  operator matrix_aligned_span<T const, Major, Q>() const
      requires(!is_const<T>().value)
  {
    return {origin_, size_, minor_capacity_};
  }

  template<typename T, major Major, index_t Q>
  T* matrix_aligned_span<T, Major, Q>::get_ptr_impl(index<2> const& ind) const
  {
    return origin_ +
        detail::matrix_aligned_ptr_offset<Major, Q>(ind, minor_capacity_);
  }

  template<typename T, major Major, index_t Q>
  bool
  matrix_aligned_span<T, Major, Q>::is_bounded_impl(index<2> const& ind) const
  {
    return is_bounded_sub<2>(ind, zero, size_);
  }

  template<typename T, major Major, index_t Q>
  auto
  matrix_aligned_span<T, Major, Q>::iterator_impl(index<2> const& ind) const
  {
    return matrix_aligned_span_iterator<T, Major, Q>{
        origin_, ind, size_, minor_capacity_};
  }

  template<typename T, major Major, index_t Q>
  auto matrix_aligned_span<T, Major, Q>::begin() const
  {
    return matrix_aligned_span_iterator<T, Major, Q>{
        origin_, index<2>(zero), size_, minor_capacity_};
  }

  template<typename T, major Major, index_t Q>
  auto matrix_aligned_span<T, Major, Q>::end() const
  {
    return matrix_aligned_span_iterator<T, Major, Q>{
        origin_, index(-1, -1), size_, minor_capacity_};
  }

  template<typename... Ts, major Major, index_t Q>
  struct for_each_bundle_impl<matrix_aligned_span<Ts, Major, Q>...>
    : public iterator_based_for_each_bundle_impl_facade {
  };
}
