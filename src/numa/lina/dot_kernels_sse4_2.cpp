#include "dot_kernels_sse4_2.hpp"

#include "dot_kernels_misc.hpp"

#include <x86intrin.h>

//#define IACA_DISABLE
//#include "/home/toby/thirdparty/iaca-lin64/iacaMarks.h"
// IACA_START;
//"movl $111, %%ebx \n\t"
//".byte 0x64, 0x67, 0x90 \n\t"
// IACA_END;
//"movl $222, %%ebx \n\t"
//".byte 0x64, 0x67, 0x90 \n\t"

namespace numa {
  namespace {
    void dot_kernel_sse4_2_float(
        float* __restrict__ p_buffer,
        float const* __restrict__ p_A,
        float const* __restrict__ p_B,
        index_t m) noexcept
    {
      __asm__ volatile(
          "                                               \n\t"
          "mov %[A], %%r14                                \n\t"
          "mov %[B], %%r15                                \n\t"
          "                                               \n\t"
          "xorps %%xmm4, %%xmm4                           \n\t"
          "xorps %%xmm5, %%xmm5                           \n\t"
          "xorps %%xmm6, %%xmm6                           \n\t"
          "xorps %%xmm7, %%xmm7                           \n\t"
          "xorps %%xmm8, %%xmm8                           \n\t"
          "xorps %%xmm9, %%xmm9                           \n\t"
          "xorps %%xmm10, %%xmm10                         \n\t"
          "xorps %%xmm11, %%xmm11                         \n\t"
          "xorps %%xmm12, %%xmm12                         \n\t"
          "xorps %%xmm13, %%xmm13                         \n\t"
          "xorps %%xmm14, %%xmm14                         \n\t"
          "xorps %%xmm15, %%xmm15                         \n\t"
          "                                               \n\t"
          "5:                                             \n\t"
          "                                               \n\t"
          // m = 0
          "                                               \n\t"
          "movss 0x00(%%r15), %%xmm0                      \n\t"
          "shufps $0, %%xmm0, %%xmm0                      \n\t"
          "movss 0x04(%%r15), %%xmm1                      \n\t"
          "shufps $0, %%xmm1, %%xmm1                      \n\t"
          "                                               \n\t"
          "movaps 0x00(%%r14), %%xmm2                     \n\t"
          "movaps %%xmm2, %%xmm3                          \n\t"
          "mulps %%xmm0, %%xmm2                           \n\t"
          "addps %%xmm2, %%xmm4                           \n\t"
          "mulps %%xmm1, %%xmm3                           \n\t"
          "addps %%xmm3, %%xmm5                           \n\t"
          "                                               \n\t"
          "movaps 0x10(%%r14), %%xmm2                     \n\t"
          "movaps %%xmm2, %%xmm3                          \n\t"
          "mulps %%xmm0, %%xmm2                           \n\t"
          "addps %%xmm2, %%xmm6                           \n\t"
          "mulps %%xmm1, %%xmm3                           \n\t"
          "addps %%xmm3, %%xmm7                           \n\t"
          "                                               \n\t"
          "prefetcht0 0x100(%%r15)                        \n\t"
          "                                               \n\t"
          "movaps 0x20(%%r14), %%xmm2                     \n\t"
          "movaps %%xmm2, %%xmm3                          \n\t"
          "mulps %%xmm0, %%xmm2                           \n\t"
          "addps %%xmm2, %%xmm8                           \n\t"
          "mulps %%xmm1, %%xmm3                           \n\t"
          "addps %%xmm3, %%xmm9                           \n\t"
          "                                               \n\t"
          "prefetcht0 0x200(%%r14)                        \n\t"
          "                                               \n\t"
          "movaps 0x30(%%r14), %%xmm2                     \n\t"
          "movaps %%xmm2, %%xmm3                          \n\t"
          "mulps %%xmm0, %%xmm2                           \n\t"
          "addps %%xmm2, %%xmm10                          \n\t"
          "mulps %%xmm1, %%xmm3                           \n\t"
          "addps %%xmm3, %%xmm11                          \n\t"
          "                                               \n\t"
          "lea 0x008(%%r15), %%r15                        \n\t"
          "lea 0x040(%%r14), %%r14                        \n\t"
          "                                               \n\t"
          "sub $1, %[m]                                   \n\t"
          "ja 5b                                          \n\t"
          "                                               \n\t"
          "                                               \n\t"
          // write back
          "movups %%xmm4, 0x00(%[r])                      \n\t"
          "movups %%xmm5, 0x40(%[r])                      \n\t"
          "                                               \n\t"
          "movups %%xmm6, 0x10(%[r])                      \n\t"
          "movups %%xmm7, 0x50(%[r])                      \n\t"
          "                                               \n\t"
          "movups %%xmm8, 0x20(%[r])                      \n\t"
          "movups %%xmm9, 0x60(%[r])                      \n\t"
          "                                               \n\t"
          "movups %%xmm10, 0x30(%[r])                     \n\t"
          "movups %%xmm11, 0x70(%[r])                     \n\t"
          "                                               \n\t"
          : [r] "+r"(p_buffer), [A] "+r"(p_A), [B] "+r"(p_B), [m] "+r"(m)
          :
          : "memory",
            "r12",
            "r13",
            "r14",
            "r15",
            "xmm0",
            "xmm1",
            "xmm2",
            "xmm3",
            "xmm4",
            "xmm5",
            "xmm6",
            "xmm7",
            "xmm8",
            "xmm9",
            "xmm10",
            "xmm11",
            "xmm12",
            "xmm13",
            "xmm14",
            "xmm15");
    }
  }

  void dot_kernel_sse4_2<float>::run(
      matrix_span<float> const& retval,
      matrix_aligned_span<float const, major::left, Q_left> mas_A,
      matrix_aligned_span<float const, major::right, Q_right> mas_B)
  {
    dot_kernel_from_blocked(retval, mas_A, mas_B, dot_kernel_sse4_2_float);
  }

  namespace {
    void dot_kernel_sse4_2_double(
        double* __restrict__ p_buffer,
        double const* __restrict__ p_A,
        double const* __restrict__ p_B,
        index_t m) noexcept
    {
      __asm__ volatile(
          "                                               \n\t"
          "mov %[A], %%r14                                \n\t"
          "mov %[B], %%r15                                \n\t"
          "                                               \n\t"
          "xorpd %%xmm4, %%xmm4                           \n\t"
          "xorpd %%xmm5, %%xmm5                           \n\t"
          "xorpd %%xmm6, %%xmm6                           \n\t"
          "xorpd %%xmm7, %%xmm7                           \n\t"
          "xorpd %%xmm8, %%xmm8                           \n\t"
          "xorpd %%xmm9, %%xmm9                           \n\t"
          "xorpd %%xmm10, %%xmm10                         \n\t"
          "xorpd %%xmm11, %%xmm11                         \n\t"
          "xorpd %%xmm12, %%xmm12                         \n\t"
          "xorpd %%xmm13, %%xmm13                         \n\t"
          "xorpd %%xmm14, %%xmm14                         \n\t"
          "xorpd %%xmm15, %%xmm15                         \n\t"
          "                                               \n\t"
          "5:                                             \n\t"
          "                                               \n\t"
          // m = 0
          "                                               \n\t"
          "movapd 0x00(%%r15), %%xmm0                     \n\t"
          "movapd %%xmm0, %%xmm1                          \n\t"
          "unpcklpd %%xmm0, %%xmm0                        \n\t"
          "unpckhpd %%xmm1, %%xmm1                        \n\t"
          "                                               \n\t"
          "movapd 0x00(%%r14), %%xmm2                     \n\t"
          "movapd %%xmm2, %%xmm3                          \n\t"
          "mulpd %%xmm0, %%xmm2                           \n\t"
          "addpd %%xmm2, %%xmm4                           \n\t"
          "mulpd %%xmm1, %%xmm3                           \n\t"
          "addpd %%xmm3, %%xmm5                           \n\t"
          "                                               \n\t"
          "movapd 0x10(%%r14), %%xmm2                     \n\t"
          "movapd %%xmm2, %%xmm3                          \n\t"
          "mulpd %%xmm0, %%xmm2                           \n\t"
          "addpd %%xmm2, %%xmm6                           \n\t"
          "mulpd %%xmm1, %%xmm3                           \n\t"
          "addpd %%xmm3, %%xmm7                           \n\t"
          "                                               \n\t"
          "prefetcht0 0x100(%%r15)                        \n\t"
          "                                               \n\t"
          "movapd 0x20(%%r14), %%xmm2                     \n\t"
          "movapd %%xmm2, %%xmm3                          \n\t"
          "mulpd %%xmm0, %%xmm2                           \n\t"
          "addpd %%xmm2, %%xmm8                           \n\t"
          "mulpd %%xmm1, %%xmm3                           \n\t"
          "addpd %%xmm3, %%xmm9                           \n\t"
          "                                               \n\t"
          "prefetcht0 0x200(%%r14)                        \n\t"
          "                                               \n\t"
          "movapd 0x30(%%r14), %%xmm2                     \n\t"
          "movapd %%xmm2, %%xmm3                          \n\t"
          "mulpd %%xmm0, %%xmm2                           \n\t"
          "addpd %%xmm2, %%xmm10                          \n\t"
          "mulpd %%xmm1, %%xmm3                           \n\t"
          "addpd %%xmm3, %%xmm11                          \n\t"
          "                                               \n\t"
          "lea 0x010(%%r15), %%r15                        \n\t"
          "lea 0x040(%%r14), %%r14                        \n\t"
          "                                               \n\t"
          "sub $1, %[m]                                   \n\t"
          "ja 5b                                          \n\t"
          "                                               \n\t"
          "                                               \n\t"
          // write back
          "movupd %%xmm4, 0x00(%[r])                      \n\t"
          "movupd %%xmm5, 0x40(%[r])                      \n\t"
          "                                               \n\t"
          "movupd %%xmm6, 0x10(%[r])                      \n\t"
          "movupd %%xmm7, 0x50(%[r])                      \n\t"
          "                                               \n\t"
          "movupd %%xmm8, 0x20(%[r])                      \n\t"
          "movupd %%xmm9, 0x60(%[r])                      \n\t"
          "                                               \n\t"
          "movupd %%xmm10, 0x30(%[r])                     \n\t"
          "movupd %%xmm11, 0x70(%[r])                     \n\t"
          "                                               \n\t"
          : [r] "+r"(p_buffer), [A] "+r"(p_A), [B] "+r"(p_B), [m] "+r"(m)
          :
          : "memory",
            "r12",
            "r13",
            "r14",
            "r15",
            "xmm0",
            "xmm1",
            "xmm2",
            "xmm3",
            "xmm4",
            "xmm5",
            "xmm6",
            "xmm7",
            "xmm8",
            "xmm9",
            "xmm10",
            "xmm11",
            "xmm12",
            "xmm13",
            "xmm14",
            "xmm15");
    }
  }

  void dot_kernel_sse4_2<double>::run(
      matrix_span<double> const& retval,
      matrix_aligned_span<double const, major::left, Q_left> mas_A,
      matrix_aligned_span<double const, major::right, Q_right> mas_B)
  {
    dot_kernel_from_blocked(retval, mas_A, mas_B, dot_kernel_sse4_2_double);
  }
}
