#include "dot_kernels.hpp"

#include "cpu_target_registry.hpp"

namespace numa {
  template<typename T>
  thread_local void (*dot_kernel_cpu_target<T>::run)(
      matrix_span<T> const& retval,
      matrix_span<T const> const& A,
      matrix_span<T const> const& B) = nullptr;

  template<typename T>
  thread_local typename dot_kernel_cpu_target<T>::matrix_left (
      *dot_kernel_cpu_target<T>::allocate_left)(index<2> const& size) = nullptr;

  template<typename T>
  thread_local typename dot_kernel_cpu_target<T>::matrix_right (
      *dot_kernel_cpu_target<T>::allocate_right)(index<2> const& size) =
      nullptr;

  template<typename T>
  thread_local void (*dot_kernel_cpu_target<T>::copy_left)(
      typename dot_kernel_cpu_target<T>::matrix_span_left const& retval,
      matrix_span<T const> const& A) = nullptr;

  template<typename T>
  thread_local void (*dot_kernel_cpu_target<T>::copy_right)(
      typename dot_kernel_cpu_target<T>::matrix_span_right const& retval,
      matrix_span<T const> const& A) = nullptr;

  template<typename T>
  thread_local void (*dot_kernel_cpu_target<T>::run_raw)(
      matrix_span<T> const& retval,
      typename dot_kernel_cpu_target<T>::matrix_span_left const& A,
      typename dot_kernel_cpu_target<T>::matrix_span_right const& B) = nullptr;

  template<typename T>
  bool ATTRIBUTE_USED dot_kernel_cpu_target<T>::registered_ = [] {
    auto const pattern = cpu_target_registry::pattern(
        c_generic,
        c_type<dot_kernel_generic<T>>,
        c_sse4_2,
        c_type<dot_kernel_sse4_2<T>>,
        c_avx,
        c_type<dot_kernel_avx<T>>,
        c_avx2,
        c_type<dot_kernel_avx2<T>>);

    bool retval = true;

#define REGISTER(member, ...)                  \
  retval =                                     \
      retval &&                                \
      cpu_target_registry::register_function(  \
          &(dot_kernel_cpu_target<T>::member), \
          pattern.get([]<typename K>(c_type_t<K>) { return __VA_ARGS__; }));

    REGISTER(run, dot_kernel_run<K>);
    REGISTER(allocate_left, dot_kernel_allocate<K, major::left>);
    REGISTER(allocate_right, dot_kernel_allocate<K, major::right>);
    REGISTER(copy_left, dot_kernel_copy<K, major::left>);
    REGISTER(copy_right, dot_kernel_copy<K, major::right>);
    REGISTER(run_raw, dot_kernel_run_raw<K>);

#undef REGISTER

    return retval;
  }();

  template struct dot_kernel_cpu_target<float>;
  template struct dot_kernel_cpu_target<double>;
}
