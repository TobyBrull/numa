#ifndef INCLUDE_GUARD_NUMA_LINA_DOT_KERNELS_H_
#define INCLUDE_GUARD_NUMA_LINA_DOT_KERNELS_H_

#include "dot_kernels_avx.hpp"
#include "dot_kernels_avx2.hpp"
#include "dot_kernels_generic.hpp"
#include "dot_kernels_sse4_2.hpp"

#include "matrix_aligned.hpp"

#include "numa/cell/make_cell.hpp"

#include "numa/boot/chunk.hpp"

namespace numa {
  /*
   * machine-specific
   */

  template<typename Kernel>
  void dot_kernel_run(
      matrix_span<typename Kernel::value_type> const& retval,
      matrix_span<typename Kernel::value_type const> const& A,
      matrix_span<typename Kernel::value_type const> const& B);

  template<typename Kernel, major Major>
  matrix_aligned_variant<typename Kernel::value_type, Major>
  dot_kernel_allocate(index<2> const& size);

  template<typename Kernel, major Major>
  void dot_kernel_copy(
      matrix_aligned_span_variant<typename Kernel::value_type, Major> const&
          retval,
      matrix_span<typename Kernel::value_type const> const& A);

  template<typename Kernel>
  void dot_kernel_run_raw(
      matrix_span<typename Kernel::value_type> const& retval,
      matrix_aligned_span_variant<
          typename Kernel::value_type,
          major::left> const& A,
      matrix_aligned_span_variant<
          typename Kernel::value_type,
          major::right> const& B);

  constexpr inline auto dot_kernel_cpu_target_types = type_list<float, double>;

  template<typename T>
  struct dot_kernel_cpu_target {
    static thread_local void (*run)(
        matrix_span<T> const& retval,
        matrix_span<T const> const& A,
        matrix_span<T const> const& B);

    using matrix_left  = matrix_aligned_variant<T, major::left>;
    using matrix_right = matrix_aligned_variant<T, major::right>;

    using matrix_span_left  = matrix_aligned_span_variant<T, major::left>;
    using matrix_span_right = matrix_aligned_span_variant<T, major::right>;

    static thread_local matrix_left (*allocate_left)(index<2> const& size);

    static thread_local matrix_right (*allocate_right)(index<2> const& size);

    static thread_local void (*copy_left)(
        matrix_span_left const& retval, matrix_span<T const> const& A);

    static thread_local void (*copy_right)(
        matrix_span_right const& retval, matrix_span<T const> const& A);

    static thread_local void (*run_raw)(
        matrix_span<T> const& retval,
        matrix_span_left const& A,
        matrix_span_right const& B);

    static bool registered_;
  };

  extern template struct dot_kernel_cpu_target<float>;
  extern template struct dot_kernel_cpu_target<double>;
}

#include "dot_kernels.inl"

#endif
