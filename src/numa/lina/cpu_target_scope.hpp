#ifndef INCLUDE_GUARD_NUMA_LINA_CPU_TARGET_SCOPE_H_
#define INCLUDE_GUARD_NUMA_LINA_CPU_TARGET_SCOPE_H_

#include "cpu_target_scope_detail.hpp"

namespace numa {
  class cpu_target_scope {
   public:
    cpu_target_scope(cpu_target ct);

   private:
    context<detail::cpu_target_core> scope_;
  };
}

#endif
