#ifndef INCLUDE_GUARD_NUMA_LINA_CPU_TARGET_H_
#define INCLUDE_GUARD_NUMA_LINA_CPU_TARGET_H_

#include "numa/core/serialize_json.hpp"

#include "numa/tmpl/literals.hpp"

#include "numa/boot/fwd.hpp"

#include <iosfwd>
#include <string>
#include <vector>

#include <stdint.h>
#include <x86intrin.h>

namespace numa {
  enum class cpu_target {
    generic = 0,
    sse4_2  = 1,
    avx     = 2,
    avx2    = 3,
    avx512  = 4
  };

  template<cpu_target v>
  using c_cpu_target_t = c_integer_t<cpu_target, v>;

  template<cpu_target v>
  constexpr inline auto c_cpu_target = c_cpu_target_t<v>{};

  constexpr inline c_cpu_target_t<cpu_target::generic> c_generic = {};
  constexpr inline c_cpu_target_t<cpu_target::sse4_2> c_sse4_2   = {};
  constexpr inline c_cpu_target_t<cpu_target::avx> c_avx         = {};
  constexpr inline c_cpu_target_t<cpu_target::avx2> c_avx2       = {};
  constexpr inline c_cpu_target_t<cpu_target::avx512> c_avx512   = {};

  void to_pretty(std::ostream&, cpu_target);
  void from_pretty(std::istream&, cpu_target&);

  void to_json(std::ostream&, cpu_target);
  void from_json(std::istream&, cpu_target&);

  auto f_effort_cpu_target();

  /*
   * Sorted in order or desirability.
   */
  std::vector<cpu_target> const& cpu_target_all();

  bool cpu_target_is_compatible(cpu_target ct_query, cpu_target ct_with);

  cpu_target cpu_target_default();
}

#include "cpu_target.inl"

#endif
