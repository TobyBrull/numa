#include "cpu_target_scope_detail.hpp"

namespace numa::detail {
  cpu_target_core::cpu_target_core(
      base b, thalanx_remote_t, cpu_target_core const& s)
    : cpu_target_core(std::move(b), s.ct_)
  {
  }

  cpu_target_core::cpu_target_core(base b, root_t)
    : cpu_target_core(std::move(b), cpu_target_default())
  {
  }

  cpu_target_core::cpu_target_core(base b, cpu_target const ct)
    : base(std::move(b)), ct_(ct)
  {
  }

  void cpu_target_core::context_became_active()
  {
    cpu_target_registry::execute(ct_);
  }
}
