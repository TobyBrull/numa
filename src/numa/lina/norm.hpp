#ifndef INCLUDE_GUARD_NUMA_LINA_NORM_H_
#define INCLUDE_GUARD_NUMA_LINA_NORM_H_

namespace numa {
  double norm_one(double A);
  double norm_inf(double A);

  double norm_one(double A, double B);
  double norm_inf(double A, double B);

  double norm_two();

  //  THROW_IF_NOT (for_each (A) (a_abs_max) < 1e-4, "A to big");
  //  THROW_IF_NOT (for_each (A) (a_frobenius) < 1e-4, "A to big");
  //  THROW_IF_NOT (for_each (A, B) (a_abs_max) < 1e-4, "A and B differ");
  //  THROW_IF_NOT (for_each (A, B) (a_frobenius) < 1e-4, "A and B differ");
}

#include "norm.inl"

#endif
