#ifndef INCLUDE_GUARD_NUMA_LINA_DOT_H_
#define INCLUDE_GUARD_NUMA_LINA_DOT_H_

#include "dot_kernels.hpp"
#include "policy.hpp"

#include "numa/cell/t_span.hpp"

#include "numa/core/functors.hpp"

#include "numa/tmpl/integer_list.hpp"

namespace numa {
  /*
   * retval <- alpha * retval + beta * A * B???
   */
  template<
      typename T,
      typename U,
      index_t N,
      typename V,
      index_t M,
      IndexList Ns = index_list_t<N - 1>,
      IndexList Ms = index_list_t<0>,
      Policy P     = policy_default_t,
      index_t K    = N - size<Ns>() + M - size<Ms>()>
  requires(
      ((size<Ns>() == size<Ms>()) &&
       (all_of(Ns{}, f_is_less_than_t{c_index<N>})) &&
       (all_of(Ms{}, f_is_less_than_t{c_index<M>})) && (is_unique<Ns>()) &&
       (is_unique<Ms>()))
          .value) void dot(t_span<T, K> retval, t_span<const U, N> A, t_span<const V, M> B, P = {}, Ns = {}, Ms = {});

  // void dot (t_span, t_span_diagonal);

  void make_dot();
}

#include "dot.inl"

#endif
