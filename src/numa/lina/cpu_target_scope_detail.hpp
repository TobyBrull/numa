#ifndef INCLUDE_GUARD_NUMA_LINA_CPU_TARGET_SCOPE_DETAIL_H_
#define INCLUDE_GUARD_NUMA_LINA_CPU_TARGET_SCOPE_DETAIL_H_

#include "cpu_target_registry.hpp"

#include "numa/ctxt/context.hpp"

namespace numa::detail {
  class cpu_target_core : public context_base<cpu_target_core> {
   protected:
    using base = context_base<cpu_target_core>;

    cpu_target_core(base, thalanx_remote_t, cpu_target_core const& s);
    cpu_target_core(base, root_t);
    cpu_target_core(base, cpu_target ct);

   private:
    friend class context<cpu_target_core>;

    void context_became_active();

    cpu_target ct_;
  };
}

#endif
