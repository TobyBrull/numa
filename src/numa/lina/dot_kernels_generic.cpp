#include "dot_kernels_generic.hpp"

namespace numa {
  template<typename T>
  void dot_kernel_generic<T>::run(
      matrix_span<T> const& retval,
      matrix_aligned_span<T const, major::left, Q_left> mas_A,
      matrix_aligned_span<T const, major::right, Q_right> mas_B)
  {
    index_t const n = retval.size_[0];
    index_t const m = mas_A.size_[1];
    index_t const p = retval.size_[1];

    for (index_t i = 0; i < n; ++i) {
      for (index_t k = 0; k < p; ++k) {
        for (index_t j = 0; j < m; ++j) {
          retval(i, k) += mas_A(i, j) * mas_B(j, k);
        }
      }
    }
  }

  template struct dot_kernel_generic<float>;
  template struct dot_kernel_generic<double>;
}
