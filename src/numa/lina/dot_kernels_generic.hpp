#ifndef INCLUDE_GUARD_NUMA_LINA_DOT_KERNELS_GENERIC_H_
#define INCLUDE_GUARD_NUMA_LINA_DOT_KERNELS_GENERIC_H_

#include "matrix_aligned_span.hpp"

#include "numa/cell/t_span.hpp"

#include "numa/boot/fwd.hpp"

namespace numa {
  template<typename T>
  struct dot_kernel_generic {
    using value_type = T;

    constexpr inline static index_t Q_left  = 1;
    constexpr inline static index_t Q_right = 1;

    static void
    run(matrix_span<T> const& retval,
        matrix_aligned_span<T const, major::left, Q_left> mas_A,
        matrix_aligned_span<T const, major::right, Q_right> mas_B);
  };
}

#endif
