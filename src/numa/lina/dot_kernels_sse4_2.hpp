#ifndef INCLUDE_GUARD_NUMA_LINA_DOT_KERNELS_SSE4_2_H_
#define INCLUDE_GUARD_NUMA_LINA_DOT_KERNELS_SSE4_2_H_

#include "matrix_aligned_span.hpp"

#include "numa/cell/t_span.hpp"

#include "numa/boot/fwd.hpp"

namespace numa {
  template<typename T>
  struct dot_kernel_sse4_2;

  template<>
  struct dot_kernel_sse4_2<float> {
    using value_type = float;

    constexpr inline static index_t Q_left  = 16;
    constexpr inline static index_t Q_right = 2;

    static void
    run(matrix_span<float> const& retval,
        matrix_aligned_span<float const, major::left, Q_left> mas_A,
        matrix_aligned_span<float const, major::right, Q_right> mas_B);
  };

  template<>
  struct dot_kernel_sse4_2<double> {
    using value_type = double;

    constexpr inline static index_t Q_left  = 8;
    constexpr inline static index_t Q_right = 2;

    static void
    run(matrix_span<double> const& retval,
        matrix_aligned_span<double const, major::left, Q_left> mas_A,
        matrix_aligned_span<double const, major::right, Q_right> mas_B);
  };
}

#endif
