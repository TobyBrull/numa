#include "cpu_target_registry.hpp"

namespace numa {
  auto cpu_target_registry::handlers() -> std::vector<handler_t>&
  {
    static std::vector<handler_t> instance = [] {
      std::vector<handler_t> retval;
      retval.reserve(5);
      return retval;
    }();

    return instance;
  }

  void cpu_target_registry::execute(cpu_target const ct)
  {
    for (auto const& handler: handlers()) {
      handler(ct);
    }
  }
}
