namespace numa {
  template<typename T, major Major, index_t Q>
  index<2> const& matrix_aligned_unvaried<T, Major, Q>::size() const
  {
    return size_;
  }

  template<typename T, major Major, index_t Q>
  index<2> const& matrix_aligned_unvaried<T, Major, Q>::capacity() const
  {
    return capacity_;
  }

  template<typename T, major Major, index_t Q>
  auto matrix_aligned_unvaried<T, Major, Q>::span() -> span_type
  {
    return span_type{ptr_, size_, minor_capacity()};
  }

  template<typename T, major Major, index_t Q>
  auto matrix_aligned_unvaried<T, Major, Q>::span() const -> span_const_type
  {
    return span_const();
  }

  template<typename T, major Major, index_t Q>
  auto matrix_aligned_unvaried<T, Major, Q>::span_const() const
      -> span_const_type
  {
    return span_const_type{ptr_, size_, minor_capacity()};
  }

  template<typename T, major Major, index_t Q>
  T* matrix_aligned_unvaried<T, Major, Q>::get_ptr_impl(index<2> const& ind)
  {
    return ptr_ +
        detail::matrix_aligned_ptr_offset<Major, Q>(ind, minor_capacity());
  }

  template<typename T, major Major, index_t Q>
  T const* matrix_aligned_unvaried<T, Major, Q>::const_get_ptr_impl(
      index<2> const& ind) const
  {
    return ptr_ +
        detail::matrix_aligned_ptr_offset<Major, Q>(ind, minor_capacity());
  }

  template<typename T, major Major, index_t Q>
  bool matrix_aligned_unvaried<T, Major, Q>::is_bounded_impl(
      index<2> const& ind) const
  {
    return is_bounded_sub<2>(ind, zero, size_);
  }

  template<typename T, major Major, index_t Q>
  index_t matrix_aligned_unvaried<T, Major, Q>::minor_capacity() const
  {
    if constexpr (Major == major::left) {
      return capacity_[1];
    }
    else {
      return capacity_[0];
    }
  }

  template<typename T, major Major, index_t Q>
  matrix_aligned_unvaried<T, Major, Q>::matrix_aligned_unvaried(
      T* const ptr, index<2> const& size, index<2> const& capacity) noexcept
    : ptr_(ptr), size_(size), capacity_(capacity)
  {
  }

  /*
   * for-each
   */
  template<typename... Ts, major Major, index_t Q>
  struct for_each_bundle_impl<matrix_aligned_unvaried<Ts, Major, Q>...>
    : public span_based_for_each_bundle_impl_facade {
  };

  /*
   * matrix_aligned
   */
  template<typename T, major Major, index_t Q, Allocator Alloc>
  matrix_aligned<T, Major, Q, Alloc>::matrix_aligned(Alloc alloc) noexcept
    : matrix_aligned_unvaried<T, Major, Q>(nullptr, zero, zero)
    , alloc_(std::move(alloc))
  {
  }

  template<typename T, major Major, index_t Q, Allocator Alloc>
  matrix_aligned<T, Major, Q, Alloc>::matrix_aligned(
      matrix_aligned&& other) noexcept
    : matrix_aligned_unvaried<T, Major, Q>(
          other.ptr_, other.size_, other.capacity_)
    , alloc_(std::move(alloc_))
  {
    other.ptr_      = nullptr;
    other.size_     = zero;
    other.capacity_ = zero;
  }

  template<typename T, major Major, index_t Q, Allocator Alloc>
  matrix_aligned<T, Major, Q, Alloc>&
  matrix_aligned<T, Major, Q, Alloc>::operator=(matrix_aligned&& other) noexcept
  {
    for_each (*this)(f_destruct);

    deallocate_for<T>(alloc_, this->ptr_, required_elements(this->capacity()));

    this->ptr_      = other.ptr_;
    this->size_     = other.size_;
    this->capacity_ = other.capacity_;

    alloc_ = std::move(other.alloc_);

    other.ptr_      = nullptr;
    other.size_     = zero;
    other.capacity_ = zero;

    return (*this);
  }

  template<typename T, major Major, index_t Q, Allocator Alloc>
  matrix_aligned<T, Major, Q, Alloc>::~matrix_aligned()
  {
    for_each (*this)(f_destruct);

    deallocate_for<T>(alloc_, this->ptr_, required_elements(this->capacity()));
  }

  template<typename T, major Major, index_t Q, Allocator Alloc>
  Alloc const& matrix_aligned<T, Major, Q, Alloc>::allocator() const
  {
    return alloc_;
  }

  template<typename T, major Major, index_t Q, Allocator Alloc>
  constexpr index_t
  matrix_aligned<T, Major, Q, Alloc>::required_elements(index<2> const& size)
  {
    if constexpr (Major == major::left) {
      return chunk_required_size(size[0], Q) * size[1];
    }
    else {
      return size[0] * chunk_required_size(size[1], Q);
    }
  }

  template<typename T, major Major, index_t Q, Allocator Alloc>
  auto matrix_aligned<T, Major, Q, Alloc>::unvaried() -> unvaried_type&
  {
    return (*this);
  }

  template<typename T, major Major, index_t Q, Allocator Alloc>
  auto matrix_aligned<T, Major, Q, Alloc>::unvaried() const
      -> unvaried_type const&
  {
    return (*this);
  }

  template<typename T, major Major, index_t Q, Allocator Alloc>
  auto matrix_aligned<T, Major, Q, Alloc>::unvaried_const() const
      -> unvaried_type const&
  {
    return (*this);
  }

  template<
      typename T,
      major Major,
      index_t Q,
      Allocator Alloc,
      IndexList SelAxes>
  class matrix_aligned_axes
    : public cell_axes_facade<
          matrix_aligned_axes<T, Major, Q, Alloc, SelAxes>,
          matrix_aligned<T, Major, Q, Alloc>,
          SelAxes> {
   public:
    using facade = cell_axes_facade<
        matrix_aligned_axes<T, Major, Q, Alloc, SelAxes>,
        matrix_aligned<T, Major, Q, Alloc>,
        SelAxes>;

    using facade::facade;

    constexpr static index_t Ka = numa::size(SelAxes{});

    using cell_type = matrix_aligned<T, Major, Q, Alloc>;

    static auto
    get_span(index<Ka> const& base, index<Ka> const& form, cell_type& cell)
    {
      constexpr SelAxes ax;

      auto main_span = cell.span();
      overwrite(main_span.size_, ax, select_at(cell.capacity(), ax));

      index<2> all_base = zero;
      index<2> all_form = cell.size();

      overwrite(all_base, ax, base);
      overwrite(all_form, ax, form);

      return adaptor_sub(main_span, as_size, all_base, all_form);
    }

    static void set_ptr(matrix_aligned<T, Major, Q, Alloc>& cell, T* ptr)
    {
      cell.ptr_ = ptr;
    }

    static void set_size_total(
        matrix_aligned<T, Major, Q, Alloc>& cell,
        index<2> const& new_size_total)
    {
      cell.size_ = new_size_total;
    }

    static void set_capacity_total(
        matrix_aligned<T, Major, Q, Alloc>& cell, index<2> const& new_cap_total)
    {
      cell.capacity_ = new_cap_total;
    }
  };

  template<typename T, major Major, index_t Q, Allocator Alloc>
  template<index_t... Axes>
  requires(is_variation<2>(index_list<Axes...>)
               .value) constexpr auto matrix_aligned<T, Major, Q, Alloc>::
      vary_axes_impl(index_list_t<Axes...>)
  {
    return matrix_aligned_axes<T, Major, Q, Alloc, index_list_t<Axes...>>{this};
  }

  template<typename T, major Major>
  matrix_aligned_span_variant<T, Major>
  span(matrix_aligned_variant<T, Major>& mav)
  {
    return std::visit(
        [](auto&& ma) {
          return matrix_aligned_span_variant<T, Major>{ma.span()};
        },
        mav);
  }
}
