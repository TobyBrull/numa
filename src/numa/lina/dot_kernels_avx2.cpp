#include "dot_kernels_avx2.hpp"

#include "dot_kernels_misc.hpp"

#include <x86intrin.h>

namespace numa {
  namespace {
    void dot_kernel_avx2_float(
        float* __restrict__ p_buffer,
        float const* __restrict__ p_A,
        float const* __restrict__ p_B,
        index_t m) noexcept
    {
      __asm__ volatile(
          "                                               \n\t"
          "lea 0x00(%[A]), %%r14                          \n\t"
          "mov %[B], %%r15                                \n\t"
          "                                               \n\t"
          "vxorpd %%ymm4,  %%ymm4,  %%ymm4                \n\t"
          "vxorpd %%ymm5,  %%ymm5,  %%ymm5                \n\t"
          "vxorpd %%ymm6,  %%ymm6,  %%ymm6                \n\t"
          "vxorpd %%ymm7,  %%ymm7,  %%ymm7                \n\t"
          "vxorpd %%ymm8,  %%ymm8,  %%ymm8                \n\t"
          "vxorpd %%ymm9,  %%ymm9,  %%ymm9                \n\t"
          "vxorpd %%ymm10, %%ymm10, %%ymm10               \n\t"
          "vxorpd %%ymm11, %%ymm11, %%ymm11               \n\t"
          "vxorpd %%ymm12, %%ymm12, %%ymm12               \n\t"
          "vxorpd %%ymm13, %%ymm13, %%ymm13               \n\t"
          "vxorpd %%ymm14, %%ymm14, %%ymm14               \n\t"
          "vxorpd %%ymm15, %%ymm15, %%ymm15               \n\t"
          "                                               \n\t"
          "5:                                             \n\t"
          "                                               \n\t"
          // m = 0
          "                                               \n\t"
          "vmovaps 0x00(%%r14), %%ymm0                    \n\t"
          "vmovaps 0x20(%%r14), %%ymm1                    \n\t"
          "vmovaps 0x40(%%r14), %%ymm2                    \n\t"
          "                                               \n\t"
          "vbroadcastss 0x00(%%r15), %%ymm3               \n\t"
          "vfmadd231ps %%ymm0, %%ymm3, %%ymm4             \n\t"
          "vfmadd231ps %%ymm1, %%ymm3, %%ymm5             \n\t"
          "vfmadd231ps %%ymm2, %%ymm3, %%ymm6             \n\t"
          "                                               \n\t"
          "vbroadcastss 0x04(%%r15), %%ymm3               \n\t"
          "vfmadd231ps %%ymm0, %%ymm3, %%ymm7             \n\t"
          "vfmadd231ps %%ymm1, %%ymm3, %%ymm8             \n\t"
          "vfmadd231ps %%ymm2, %%ymm3, %%ymm9             \n\t"
          "                                               \n\t"
          "vbroadcastss 0x08(%%r15), %%ymm3               \n\t"
          "vfmadd231ps %%ymm0, %%ymm3, %%ymm10            \n\t"
          "vfmadd231ps %%ymm1, %%ymm3, %%ymm11            \n\t"
          "vfmadd231ps %%ymm2, %%ymm3, %%ymm12            \n\t"
          "                                               \n\t"
          "vbroadcastss 0x0C(%%r15), %%ymm3               \n\t"
          "vfmadd231ps %%ymm0, %%ymm3, %%ymm13            \n\t"
          "vfmadd231ps %%ymm1, %%ymm3, %%ymm14            \n\t"
          "vfmadd231ps %%ymm2, %%ymm3, %%ymm15            \n\t"
          "                                               \n\t"
          "prefetcht0 0x0200(%%r15)                       \n\t"
          "prefetcht0 0x0600(%%r14)                       \n\t"
          "                                               \n\t"
          "lea 0x060(%%r14), %%r14                        \n\t"
          "lea 0x010(%%r15), %%r15                        \n\t"
          "                                               \n\t"
          "sub $1, %[m]                                   \n\t"
          "ja 5b                                          \n\t"
          "                                               \n\t"
          "                                               \n\t"
          // write back
          "vmovups %%ymm4, 0x000(%[r])                    \n\t"
          "vmovups %%ymm5, 0x020(%[r])                    \n\t"
          "vmovups %%ymm6, 0x040(%[r])                    \n\t"
          "                                               \n\t"
          "vmovups %%ymm7, 0x060(%[r])                    \n\t"
          "vmovups %%ymm8, 0x080(%[r])                    \n\t"
          "vmovups %%ymm9, 0x0A0(%[r])                    \n\t"
          "                                               \n\t"
          "vmovups %%ymm10, 0x0C0(%[r])                   \n\t"
          "vmovups %%ymm11, 0x0E0(%[r])                   \n\t"
          "vmovups %%ymm12, 0x100(%[r])                   \n\t"
          "                                               \n\t"
          "vmovups %%ymm13, 0x120(%[r])                   \n\t"
          "vmovups %%ymm14, 0x140(%[r])                   \n\t"
          "vmovups %%ymm15, 0x160(%[r])                   \n\t"
          "                                               \n\t"
          "                                               \n\t"
          "                                               \n\t"
          : [r] "+r"(p_buffer), [A] "+r"(p_A), [B] "+r"(p_B), [m] "+r"(m)
          :
          : "memory",
            "r12",
            "r13",
            "r14",
            "r15",
            "ymm0",
            "ymm1",
            "ymm2",
            "ymm3",
            "ymm4",
            "ymm5",
            "ymm6",
            "ymm7",
            "ymm8",
            "ymm9",
            "ymm10",
            "ymm11",
            "ymm12",
            "ymm13",
            "ymm14",
            "ymm15");
    }
  }

  void dot_kernel_avx2<float>::run(
      matrix_span<float> const& retval,
      matrix_aligned_span<float const, major::left, Q_left> mas_A,
      matrix_aligned_span<float const, major::right, Q_right> mas_B)
  {
    dot_kernel_from_blocked(retval, mas_A, mas_B, dot_kernel_avx2_float);
  }

  namespace {
    void dot_kernel_avx2_double(
        double* __restrict__ p_buffer,
        double const* __restrict__ p_A,
        double const* __restrict__ p_B,
        index_t m) noexcept
    {
      __asm__ volatile(
          "                                               \n\t"
          "lea 0x00(%[A]), %%r14                          \n\t"
          "mov %[B], %%r15                                \n\t"
          "                                               \n\t"
          "vxorpd %%ymm4,  %%ymm4,  %%ymm4                \n\t"
          "vxorpd %%ymm5,  %%ymm5,  %%ymm5                \n\t"
          "vxorpd %%ymm6,  %%ymm6,  %%ymm6                \n\t"
          "vxorpd %%ymm7,  %%ymm7,  %%ymm7                \n\t"
          "vxorpd %%ymm8,  %%ymm8,  %%ymm8                \n\t"
          "vxorpd %%ymm9,  %%ymm9,  %%ymm9                \n\t"
          "vxorpd %%ymm10, %%ymm10, %%ymm10               \n\t"
          "vxorpd %%ymm11, %%ymm11, %%ymm11               \n\t"
          "vxorpd %%ymm12, %%ymm12, %%ymm12               \n\t"
          "vxorpd %%ymm13, %%ymm13, %%ymm13               \n\t"
          "vxorpd %%ymm14, %%ymm14, %%ymm14               \n\t"
          "vxorpd %%ymm15, %%ymm15, %%ymm15               \n\t"
          "                                               \n\t"
          "5:                                             \n\t"
          "                                               \n\t"
          // m = 0
          "                                               \n\t"
          "vmovapd 0x00(%%r14), %%ymm0                    \n\t"
          "vmovapd 0x20(%%r14), %%ymm1                    \n\t"
          "vmovapd 0x40(%%r14), %%ymm2                    \n\t"
          "                                               \n\t"
          "vbroadcastsd 0x00(%%r15), %%ymm3               \n\t"
          "vfmadd231pd %%ymm0, %%ymm3, %%ymm4             \n\t"
          "vfmadd231pd %%ymm1, %%ymm3, %%ymm5             \n\t"
          "vfmadd231pd %%ymm2, %%ymm3, %%ymm6             \n\t"
          "                                               \n\t"
          "vbroadcastsd 0x08(%%r15), %%ymm3               \n\t"
          "vfmadd231pd %%ymm0, %%ymm3, %%ymm7             \n\t"
          "vfmadd231pd %%ymm1, %%ymm3, %%ymm8             \n\t"
          "vfmadd231pd %%ymm2, %%ymm3, %%ymm9             \n\t"
          "                                               \n\t"
          "vbroadcastsd 0x10(%%r15), %%ymm3               \n\t"
          "vfmadd231pd %%ymm0, %%ymm3, %%ymm10            \n\t"
          "vfmadd231pd %%ymm1, %%ymm3, %%ymm11            \n\t"
          "vfmadd231pd %%ymm2, %%ymm3, %%ymm12            \n\t"
          "                                               \n\t"
          "vbroadcastsd 0x18(%%r15), %%ymm3               \n\t"
          "vfmadd231pd %%ymm0, %%ymm3, %%ymm13            \n\t"
          "vfmadd231pd %%ymm1, %%ymm3, %%ymm14            \n\t"
          "vfmadd231pd %%ymm2, %%ymm3, %%ymm15            \n\t"
          "                                               \n\t"
          "prefetcht0 0x0200(%%r15)                       \n\t"
          "prefetcht0 0x0600(%%r14)                       \n\t"
          "                                               \n\t"
          "lea 0x060(%%r14), %%r14                        \n\t"
          "lea 0x020(%%r15), %%r15                        \n\t"
          "                                               \n\t"
          "sub $1, %[m]                                   \n\t"
          "ja 5b                                          \n\t"
          "                                               \n\t"
          "                                               \n\t"
          // write back
          "vmovupd %%ymm4, 0x000(%[r])                    \n\t"
          "vmovupd %%ymm5, 0x020(%[r])                    \n\t"
          "vmovupd %%ymm6, 0x040(%[r])                    \n\t"
          "                                               \n\t"
          "vmovupd %%ymm7, 0x060(%[r])                    \n\t"
          "vmovupd %%ymm8, 0x080(%[r])                    \n\t"
          "vmovupd %%ymm9, 0x0A0(%[r])                    \n\t"
          "                                               \n\t"
          "vmovupd %%ymm10, 0x0C0(%[r])                   \n\t"
          "vmovupd %%ymm11, 0x0E0(%[r])                   \n\t"
          "vmovupd %%ymm12, 0x100(%[r])                   \n\t"
          "                                               \n\t"
          "vmovupd %%ymm13, 0x120(%[r])                   \n\t"
          "vmovupd %%ymm14, 0x140(%[r])                   \n\t"
          "vmovupd %%ymm15, 0x160(%[r])                   \n\t"
          "                                               \n\t"
          "                                               \n\t"
          "                                               \n\t"
          : [r] "+r"(p_buffer), [A] "+r"(p_A), [B] "+r"(p_B), [m] "+r"(m)
          :
          : "memory",
            "r12",
            "r13",
            "r14",
            "r15",
            "ymm0",
            "ymm1",
            "ymm2",
            "ymm3",
            "ymm4",
            "ymm5",
            "ymm6",
            "ymm7",
            "ymm8",
            "ymm9",
            "ymm10",
            "ymm11",
            "ymm12",
            "ymm13",
            "ymm14",
            "ymm15");
    }
  }

  void dot_kernel_avx2<double>::run(
      matrix_span<double> const& retval,
      matrix_aligned_span<double const, major::left, Q_left> mas_A,
      matrix_aligned_span<double const, major::right, Q_right> mas_B)
  {
    dot_kernel_from_blocked(retval, mas_A, mas_B, dot_kernel_avx2_double);
  }
}
