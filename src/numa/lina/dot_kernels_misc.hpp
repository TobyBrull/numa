#ifndef INCLUDE_GUARD_NUMA_LINA_DOT_KERNELS_MISC_H_
#define INCLUDE_GUARD_NUMA_LINA_DOT_KERNELS_MISC_H_

#include "matrix_aligned_span.hpp"

#include "numa/cell/reform.hpp"
#include "numa/cell/t_span.hpp"
#include "numa/cell/tensor_fixed_capacity.hpp"
#include "numa/cell/tensor_fixed_size.hpp"

#include "numa/ctxt/thalanx_batch.hpp"

#include "numa/boot/chunk.hpp"
#include "numa/boot/fwd.hpp"

namespace numa {
  template<typename T, index_t Q_left, index_t Q_right, typename Blocked>
  void dot_kernel_from_blocked(
      matrix_span<T> const& retval,
      matrix_aligned_span<T const, major::left, Q_left> mas_A,
      matrix_aligned_span<T const, major::right, Q_right> mas_B,
      Blocked blocked);
}

#include "dot_kernels_misc.inl"

#endif
