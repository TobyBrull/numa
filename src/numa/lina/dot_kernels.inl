namespace numa {
  /*
   * machine-specific
   */

  template<typename Kernel>
  void dot_kernel_run(
      matrix_span<typename Kernel::value_type> const& retval,
      matrix_span<typename Kernel::value_type const> const& A,
      matrix_span<typename Kernel::value_type const> const& B)
  {
    using T = typename Kernel::value_type;

    auto const ma_A =
        make_copy<matrix_aligned<T, major::left, Kernel::Q_left>>(A);

    auto const ma_B =
        make_copy<matrix_aligned<T, major::right, Kernel::Q_right>>(B);

    Kernel::run(retval, ma_A.span(), ma_B.span());
  }

  template<typename Kernel, major Major>
  matrix_aligned_variant<typename Kernel::value_type, Major>
  dot_kernel_allocate(index<2> const& size)
  {
    using T = typename Kernel::value_type;

    constexpr index_t Q =
        (Major == major::left) ? Kernel::Q_left : Kernel::Q_right;

    return make_cell<matrix_aligned<T, Major, Q>>(size)();
  }

  template<typename Kernel, major Major>
  void dot_kernel_copy(
      matrix_aligned_span_variant<typename Kernel::value_type, Major> const&
          retval,
      matrix_span<typename Kernel::value_type const> const& A)
  {
    using T = typename Kernel::value_type;

    constexpr index_t Q =
        (Major == major::left) ? Kernel::Q_left : Kernel::Q_right;

    auto const& my_retval = std::get<matrix_aligned_span<T, Major, Q>>(retval);

    copy_from(A, my_retval);
  }

  template<typename Kernel>
  void dot_kernel_run_raw(
      matrix_span<typename Kernel::value_type> const& retval,
      matrix_aligned_span_variant<
          typename Kernel::value_type,
          major::left> const& A,
      matrix_aligned_span_variant<
          typename Kernel::value_type,
          major::right> const& B)
  {
    using T = typename Kernel::value_type;

    auto const& my_A =
        std::get<matrix_aligned_span<T, major::left, Kernel::Q_left>>(A);
    auto const& my_B =
        std::get<matrix_aligned_span<T, major::right, Kernel::Q_right>>(B);

    Kernel::run(retval, my_A, my_B);
  }
}
