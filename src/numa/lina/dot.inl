namespace numa {
  template<
      typename T,
      typename U,
      index_t N,
      typename V,
      index_t M,
      IndexList Ns,
      IndexList Ms,
      Policy P,
      index_t K>
  requires(
      ((size<Ns>() == size<Ms>()) &&
       (all_of(Ns{}, f_is_less_than_t{c_index<N>})) &&
       (all_of(Ms{}, f_is_less_than_t{c_index<M>})) && (is_unique<Ns>()) &&
       (is_unique<Ms>()))
          .value) void dot(t_span<T, K> retval, t_span<const U, N> A, t_span<const V, M> B, P, Ns, Ms)
  {
    static_assert(N == 2);
    static_assert(Ns{} == index_list<1>);

    static_assert(M == 2);
    static_assert(Ms{} == index_list<0>);

    for_each(retval)(f_const_t(T{}));

    if constexpr ((c_type<T> == c_type<U>)&&(c_type<T> == c_type<V>)&&numa::
                      contains(dot_kernel_cpu_target_types, c_type<T>)) {
      if ((A.size_[0] > 10) && (B.size_[0] > 10) && (B.size_[1] > 10)) {
        (*dot_kernel_cpu_target<T>::run)(retval, A, B);
        return;
      }
    }

    index_t const n = retval.size_[0];
    index_t const m = A.size_[1];
    index_t const p = retval.size_[1];

    for (index_t i = 0; i < n; ++i) {
      for (index_t j = 0; j < m; ++j) {
        for (index_t k = 0; k < p; ++k) {
          retval(i, k) += A(i, j) * B(j, k);
        }
      }
    }
  }
}
