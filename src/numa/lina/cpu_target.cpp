#include "cpu_target.hpp"

#include <iostream>

#include <x86intrin.h>

namespace numa {
  void to_pretty(std::ostream& os, cpu_target const ct)
  {
    switch (ct) {
      case cpu_target::generic:
        os << "generic";
        break;
      case cpu_target::sse4_2:
        os << "sse4_2";
        break;
      case cpu_target::avx:
        os << "avx";
        break;
      case cpu_target::avx2:
        os << "avx2";
        break;
      case cpu_target::avx512:
        os << "avx512";
        break;
    }
  }

  void from_pretty(std::istream& is, cpu_target& ct)
  {
    auto const id = make_from_pretty<identifier>(is);

    if (id.data() == "generic") {
      ct = cpu_target::generic;
    }
    else if (id.data() == "sse4_2") {
      ct = cpu_target::sse4_2;
    }
    else if (id.data() == "avx") {
      ct = cpu_target::avx;
    }
    else if (id.data() == "avx2") {
      ct = cpu_target::avx2;
    }
    else if (id.data() == "avx512") {
      ct = cpu_target::avx512;
    }
    else {
      is.setstate(std::ios::failbit);
    }
  }

  void to_json(std::ostream& os, cpu_target const ct)
  {
    os << '"';
    to_pretty(os, ct);
    os << '"';
  }

  void from_json(std::istream& is, cpu_target& ct)
  {
    std::string temp;
    numa::from_json(is, temp);

    std::istringstream iss(temp);
    from_pretty(iss, ct);
  }

  std::vector<cpu_target> const& cpu_target_all()
  {
    static std::vector<cpu_target> const retval = {
        cpu_target::avx512,
        cpu_target::avx2,
        cpu_target::avx,
        cpu_target::sse4_2,
        cpu_target::generic,
    };

    return retval;
  }

  bool
  cpu_target_is_compatible(cpu_target const ct_query, cpu_target const ct_with)
  {
    return (ct_query <= ct_with);
  }

  namespace {
    struct cpuid_info {
      uint32_t eax;
      uint32_t ebx;
      uint32_t ecx;
      uint32_t edx;
    };

    cpuid_info cpuid(uint32_t const cpuid_function_number)
    {
      cpuid_info retval;

      /*
       * ECX is set to zero for CPUID function 4
       */
      __asm__ volatile("cpuid"
                       : "=a"(retval.eax),
                         "=b"(retval.ebx),
                         "=c"(retval.ecx),
                         "=d"(retval.edx)
                       : "a"(cpuid_function_number), "c"(0));

      return retval;
    }

    int64_t xgetbv(int const ctr)
    {
      uint32_t a, d;

      __asm__ volatile("xgetbv" : "=a"(a), "=d"(d) : "c"(ctr));

      return a | (uint64_t(d) << 32);
    }
  }

  namespace {
    cpu_target cpu_target_default_impl()
    {
      auto const info_0 = cpuid(0);
      if (info_0.eax == 0) {
        return cpu_target::generic;
      }

      auto const info_1 = cpuid(1);

      bool const has_sse4_2 = ((info_1.ecx & (1 << 20)) != 0);
      if (!has_sse4_2) {
        return cpu_target::generic;
      }

      bool const has_osxsave = ((info_1.ecx & (1 << 27)) != 0);
      bool const on_mac_os   = ((xgetbv(0) & 6) == 6);
      bool const has_avx     = ((info_1.ecx & (1 << 28)) != 0);

      if (!has_osxsave || !on_mac_os || !has_avx) {
        return cpu_target::sse4_2;
      }

      auto const info_7 = cpuid(7);

      bool const has_avx2 = ((info_7.ebx & (1 << 5)) != 0);

      if (!has_avx2) {
        return cpu_target::avx;
      }

      bool const has_avx512 = ((info_7.ebx & (1 << 16)) != 0);

      if (!has_avx512) {
        return cpu_target::avx2;
      }
      else {
        return cpu_target::avx512;
      }
    }
  }

  cpu_target cpu_target_default()
  {
    static const cpu_target retval = cpu_target_default_impl();

    return retval;
  }
}
