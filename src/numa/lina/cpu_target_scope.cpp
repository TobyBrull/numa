#include "cpu_target_scope.hpp"

namespace numa {
  cpu_target_scope::cpu_target_scope(cpu_target const ct) : scope_(ct) {}
}
