namespace numa {
  template<typename Func>
  bool cpu_target_registry::register_function(
      Func** ptr, std::map<cpu_target, Func*> ct_map)
  {
    auto ptr_setter = [ptr](Func* p) {
      (*ptr) = p;
    };

    handlers().push_back([ptr_setter = std::move(ptr_setter),
                          ct_map = std::move(ct_map)](cpu_target const new_ct) {
      /*
       * Try exact match.
       */
      {
        auto const find_it = ct_map.find(new_ct);
        if (find_it != ct_map.end()) {
          ptr_setter(find_it->second);
          return;
        }
      }

      /*
       * Try compatible cpu_target, in order of desirability.
       */
      for (auto const ct: cpu_target_all()) {
        if (cpu_target_is_compatible(ct, new_ct)) {
          auto const find_it = ct_map.find(ct);
          if (find_it != ct_map.end()) {
            ptr_setter(find_it->second);
            return;
          }
        }
      }

      ptr_setter(nullptr);
    });

    handlers().back()(cpu_target_default());

    return !handlers().empty();
  }

  template<typename Func>
  bool cpu_target_registry::register_function(
      Func** ptr, std::initializer_list<std::pair<cpu_target, Func*>> specials)
  {
    std::map<cpu_target, Func*> mm;

    for (auto const& pp: specials) {
      mm.insert(pp);
    }

    return register_function(ptr, std::move(mm));
  }

  namespace detail {
    template<TypeList CpuTargets, TypeList Kernels>
    struct cpu_target_registry_pattern {
      constexpr cpu_target_registry_pattern(CpuTargets, Kernels) {}

      template<typename F>
      auto get(F f) const
      {
        constexpr auto kernel_0 = numa::get<0>(Kernels{});

        using Func = std::invoke_result_t<F, decltype(kernel_0)>;

        std::map<cpu_target, Func> retval;

        for_each_indexed(CpuTargets{}, [&](auto const ext, auto const ct) {
          constexpr auto t = numa::get(Kernels{}, ext);

          retval.emplace(ct, f(t));
        });

        return retval;
      }
    };
  }

  template<typename... Ts>
  auto cpu_target_registry::pattern(Ts...)
  {
    constexpr auto tl = type_list_t{Ts{}...};

    static_assert(size(tl) % 2 == 0);

    constexpr auto cpu_targets = stride(tl, 0_i, 2_i);
    constexpr auto kernels     = stride(tl, 1_i, 2_i);

    return detail::cpu_target_registry_pattern{cpu_targets, kernels};
  }
}
