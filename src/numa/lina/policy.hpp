#ifndef INCLUDE_GUARD_NUMA_LINA_POLICY_H_
#define INCLUDE_GUARD_NUMA_LINA_POLICY_H_

namespace numa {
  template<typename T>
  concept Policy = true;

  struct policy_default_t {
  };
}

#endif
