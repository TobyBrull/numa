#ifndef INCLUDE_GUARD_NUMA_LINA_MATRIX_ALIGNED_SPAN_H_
#define INCLUDE_GUARD_NUMA_LINA_MATRIX_ALIGNED_SPAN_H_

#include "numa/cell/for_each.hpp"
#include "numa/cell/misc.hpp"
#include "numa/cell/t_span.hpp"

#include <variant>

namespace numa {
  /*
   * Q = block-size
   */
  template<typename T, major Major, index_t Q>
  struct matrix_aligned_span
    : public t_span_facade<matrix_aligned_span<T, Major, Q>, T, 2, 2> {
    constexpr static auto axes_info = axes_info_tensor<2>();

    T* origin_              = nullptr;
    index<2> size_          = zero;
    index_t minor_capacity_ = 0;

    using span_type = matrix_aligned_span<std::remove_const_t<T>, Major, Q>;
    using span_const_type = matrix_aligned_span<T const, Major, Q>;

    constexpr matrix_aligned_span() = default;
    constexpr matrix_aligned_span(
        T* origin, index<2> const& size, index_t minor_capacity);

    constexpr operator matrix_aligned_span<T const, Major, Q>() const
        requires(!is_const<T>().value);

    index<2> const& size() const
    {
      return size_;
    }

    T* get_ptr_impl(index<2> const& ind) const;
    bool is_bounded_impl(index<2> const& ind) const;
    auto iterator_impl(index<2> const& ind) const;

    auto begin() const;
    auto end() const;
  };

  template<typename T, major Major>
  using matrix_aligned_span_variant = std::variant<
      matrix_aligned_span<T, Major, 1>,
      matrix_aligned_span<T, Major, 2>,
      matrix_aligned_span<T, Major, 4>,
      matrix_aligned_span<T, Major, 6>,
      matrix_aligned_span<T, Major, 8>,
      matrix_aligned_span<T, Major, 10>,
      matrix_aligned_span<T, Major, 12>,
      matrix_aligned_span<T, Major, 16>,
      matrix_aligned_span<T, Major, 24>,
      matrix_aligned_span<T, Major, 32>,
      matrix_aligned_span<T, Major, 64>>;
}

#include "matrix_aligned_span.inl"

#endif
