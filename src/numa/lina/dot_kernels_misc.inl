namespace numa {
  template<typename T, index_t Q_left, index_t Q_right, typename Blocked>
  void dot_kernel_from_blocked(
      matrix_span<T> const& retval,
      matrix_aligned_span<T const, major::left, Q_left> const mas_A,
      matrix_aligned_span<T const, major::right, Q_right> const mas_B,
      Blocked blocked)
  {
    index_t const n = retval.size_[0];
    index_t const m = mas_A.size_[1];
    index_t const p = retval.size_[1];

    index_t const ld_A = mas_A.minor_capacity_ * Q_left;
    index_t const ld_B = mas_B.minor_capacity_ * Q_right;

    index_t const blk_n = (n / Q_left);
    index_t const blk_p = (p / Q_right);

    /*
     * Fully blocked part.
     */
    thalanx_batch(
        chunking_equal{},
        thalanx::size(),
        as_size,
        [&](index_t const job, index_t const) mutable {
          index_t const i = job / blk_p;
          index_t const k = job % blk_p;

          T const* p_A = mas_A.origin_ + i * ld_A;
          T const* p_B = mas_B.origin_ + k * ld_B;

          matrix_fixed_size<T, Q_left, Q_right> buffer;
          T* const p_buffer = buffer.data();
          std::fill(p_buffer, p_buffer + (Q_left * Q_right), 0.0);

          blocked(p_buffer, p_A, p_B, m);

          auto retval_sub = retval.axes().sub(
              as_size, {i * Q_left, k * Q_right}, {Q_left, Q_right});

          copy_from(buffer, retval_sub);
        },
        chunking_equal{},
        blk_n * blk_p,
        blk_n * blk_p);

    /*
     * Partial column/full row.
     */
    if ((p % Q_right) != 0) {
      T const* p_A = mas_A.origin_;

      index_t const col = blk_p * Q_right;
      T const* p_B      = mas_B.get_ptr(0, col);

      index const rv_size(Q_left, p - col);

      matrix_fixed_capacity<T, Q_left, Q_right> buffer;
      resize(buffer.vary_axes(), rv_size);

      for (index_t i = 0; i < blk_n; ++i, p_A += ld_A) {
        for_each(buffer)(f_const_t(0.0));

        blocked(buffer.get_ptr(0, 0), p_A, p_B, m);

        copy_from(
            buffer, retval.axes().sub(as_size, {i * Q_left, col}, rv_size));
      }
    }

    /*
     * Partial row/full column
     */
    if ((n % Q_left) != 0) {
      index_t const row = blk_n * Q_left;
      T const* p_A      = mas_A.get_ptr(row, 0);

      T const* p_B = mas_B.origin_;

      index const rv_size(n - row, Q_right);

      matrix_fixed_capacity<T, Q_left, Q_right> buffer;
      resize(buffer.vary_axes(), rv_size);

      for (index_t k = 0; k < blk_p; ++k, p_B += ld_B) {
        for_each(buffer)(f_const_t(0.0));

        blocked(buffer.get_ptr(0, 0), p_A, p_B, m);

        copy_from(
            buffer, retval.axes().sub(as_size, {row, k * Q_right}, rv_size));
      }
    }

    /*
     * Partial row/partial column (lower/right corner)
     */
    if (((n % Q_left) != 0) && ((p % Q_right) != 0)) {
      index_t const row = blk_n * Q_left;
      T const* p_A      = mas_A.get_ptr(row, 0);

      index_t const col = blk_p * Q_right;
      T const* p_B      = mas_B.get_ptr(0, col);

      index const rv_size(n - row, p - col);

      matrix_fixed_capacity<T, Q_left, Q_right> buffer;
      resize(buffer.vary_axes(), rv_size);

      for_each(buffer)(f_const_t(0.0));

      blocked(buffer.get_ptr(0, 0), p_A, p_B, m);

      copy_from(buffer, retval.axes().sub(as_size, {row, col}, rv_size));
    }
  }
}
