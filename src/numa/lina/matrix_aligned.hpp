#ifndef INCLUDE_GUARD_NUMA_LINA_MATRIX_ALIGNED_H_
#define INCLUDE_GUARD_NUMA_LINA_MATRIX_ALIGNED_H_

#include "matrix_aligned_span.hpp"

#include "numa/cell/for_each.hpp"
#include "numa/cell/misc.hpp"
#include "numa/cell/t_span_adaptor_sub.hpp"
#include "numa/cell/tensor.hpp"

#include "numa/core/memory.hpp"
#include "numa/core/uninitialized.hpp"

#include "numa/tmpl/vary_axes_info.hpp"

#include "numa/boot/chunk.hpp"

namespace numa {
  /*
   * Q = block-size
   */
  template<typename T, major Major, index_t Q>
  class matrix_aligned_unvaried
    : public unvaried_facade<matrix_aligned_unvaried<T, Major, Q>, T, 2, 2>
    , public disable_copy_move {
   public:
    constexpr static auto axes_info = axes_info_tensor<2>();

    index<2> const& size() const;
    index<2> const& capacity() const;

    /*
     * span
     */
    using span_type       = matrix_aligned_span<T, Major, Q>;
    using span_const_type = matrix_aligned_span<const T, Major, Q>;

    span_type span();
    span_const_type span() const;
    span_const_type span_const() const;

    /*
     * implementation of unvaried-facade
     */
    T* get_ptr_impl(index<2> const& ind);
    T const* const_get_ptr_impl(index<2> const& ind) const;

    bool is_bounded_impl(index<2> const& ind) const;

   protected:
    matrix_aligned_unvaried(
        T* ptr, index<2> const& size, index<2> const& capacity) noexcept;

    index_t minor_capacity() const;

    T* ptr_            = nullptr;
    index<2> size_     = zero;
    index<2> capacity_ = zero;
  };

  /*
   * Some operations on x86 require 16-byte alignment, e.g.., movapd.
   */
  constexpr inline std::align_val_t alignment_x86{64};

  template<
      typename T,
      major Major,
      index_t Q,
      Allocator Alloc = over_aligned_allocator<alignment_x86>>
  class matrix_aligned
    : public matrix_aligned_unvaried<T, Major, Q>
    , public vary_axes_facade<matrix_aligned<T, Major, Q, Alloc>, 2> {
   public:
    matrix_aligned(Alloc alloc = {}) noexcept;

    matrix_aligned(matrix_aligned const& other) = delete;
    matrix_aligned& operator=(matrix_aligned const& other) = delete;

    matrix_aligned(matrix_aligned&& other) noexcept;
    matrix_aligned& operator=(matrix_aligned&& other) noexcept;

    ~matrix_aligned();

    Alloc const& allocator() const;

    constexpr static index_t required_elements(index<2> const& size);

    constexpr static auto vary_axes_info = vary_axes_info_dynamic<2>();

    memory_for<Alloc, T> release();

    using unvaried_type = matrix_aligned_unvaried<T, Major, Q>;

    unvaried_type& unvaried();
    unvaried_type const& unvaried() const;
    unvaried_type const& unvaried_const() const;

    template<index_t... Axes>
    requires(is_variation<2>(index_list<Axes...>)
                 .value) constexpr auto vary_axes_impl(index_list_t<Axes...>);

   private:
    template<typename, major, index_t, Allocator, IndexList>
    friend class matrix_aligned_axes;

    Alloc alloc_;
  };

  template<typename T, major Major>
  using matrix_aligned_variant = std::variant<
      matrix_aligned<T, Major, 1>,
      matrix_aligned<T, Major, 2>,
      matrix_aligned<T, Major, 4>,
      matrix_aligned<T, Major, 6>,
      matrix_aligned<T, Major, 8>,
      matrix_aligned<T, Major, 10>,
      matrix_aligned<T, Major, 12>,
      matrix_aligned<T, Major, 16>,
      matrix_aligned<T, Major, 24>,
      matrix_aligned<T, Major, 32>,
      matrix_aligned<T, Major, 64>>;

  template<typename T, major Major>
  matrix_aligned_span_variant<T, Major>
  span(matrix_aligned_variant<T, Major>& mav);
}

#include "matrix_aligned.inl"

#endif
