namespace numa {
  template<typename... Ts>
  overload<Ts...>::overload(Ts... ts) : Ts(std::move(ts))...
  {
  }

  template<typename F, typename... Ts>
  void for_each_argument(F&& f, Ts&&... args)
  {
    int unused[] = {0, (f(std::forward<Ts>(args)), 0)...};
    (void)unused;
  }

  namespace detail {
    template<index_t I, typename F, typename T, typename... Ts>
    void for_each_argument_indexed_impl(F&& f, T&& arg, Ts&&... args)
    {
      f(I, std::forward<T>(arg));

      if constexpr (sizeof...(Ts) > 0) {
        for_each_argument_indexed_impl<I + 1>(
            std::forward<F>(f), std::forward<Ts>(args)...);
      }
    }
  }

  template<typename F, typename... Ts>
  void for_each_argument_indexed(F&& f, Ts&&... args)
  {
    detail::for_each_argument_indexed_impl<0>(
        std::forward<F>(f), std::forward<Ts>(args)...);
  }

  template<typename T>
  constexpr T lcm(std::initializer_list<T> ilist)
  {
    auto it = ilist.begin();

    ASSERT(it != ilist.end());

    T retval = *it++;

    for (; it != ilist.end(); ++it) {
      retval = std::lcm(retval, *it);
    }

    return retval;
  }
}
