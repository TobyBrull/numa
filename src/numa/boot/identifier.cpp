#include "identifier.hpp"

namespace numa {
  bool is_identifier(std::string_view const sv)
  {
    if (sv.empty()) {
      return true;
    }

    if (std::isdigit(sv[0])) {
      return false;
    }

    for (char const c: sv) {
      if (!std::isalnum(c) && (c != '_')) {
        return false;
      }
    }

    return true;
  }
}
