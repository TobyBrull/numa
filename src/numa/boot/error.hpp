#ifndef INCLUDE_GUARD_NUMA_BOOT_ERROR_H_
#define INCLUDE_GUARD_NUMA_BOOT_ERROR_H_

#include "preprocessor.hpp"

#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <vector>

namespace numa {
#define DUMP_VAR(...) numa::detail::dump_var(#__VA_ARGS__, __VA_ARGS__)
#define DUMP_VEC(X)   numa::detail::dump_vec(#X, X)
#define DUMP(...)     numa::detail::dump(__VA_ARGS__)

#define TDUMP_VAR(...)                        \
  numa::detail::dump_var(                     \
      "TID, " #__VA_ARGS__,                   \
      numa::thalanx_thread::this_thread_id(), \
      __VA_ARGS__)
#define TDUMP(...)    \
  numa::detail::dump( \
      "[", numa::thalanx_thread::this_thread_id(), "] ", __VA_ARGS__)

  template<typename T>
  struct dump_type;

#define THROW_IF_NOT(x, ...) THROW_IF(!(x), __VA_ARGS__)
#define THROW_IF(x, ...)                        \
  do {                                          \
    if (x) {                                    \
      numa::detail::error_handler(__VA_ARGS__); \
    }                                           \
  } while (false)

#define THROW(...) THROW_IF(true, __VA_ARGS__)

#define PERFORMANCE_ISSUE()

#define NOT_IMPLEMENTED() THROW(FILE_LINE ": Not implemented")

#define UNREACHABLE() THROW(FILE_LINE ": Unreachable")

#define ILLEGAL_ALLOCATION() THROW(FILE_LINE ": Illegal allocation")

#define BOUNDS_CHECK(x) THROW_IF_NOT(x, FILE_LINE ": Out of bounds")

  /*
   * ifdef
   */
#ifdef NUMA_ENABLE_ASSERTS
#  define ASSERT(x)     THROW_IF_NOT(x, FILE_LINE ": " #  x)
#  define ASSERT_NOT(x) THROW_IF(x, FILE_LINE ": " #  x)

#else
#  define ASSERT(x)
#  define ASSERT_NOT(x)

#endif
}

#include "error.inl"

#endif
