#ifndef INCLUDE_GUARD_NUMA_BOOT_SPACESHIP_FACADE_H_
#define INCLUDE_GUARD_NUMA_BOOT_SPACESHIP_FACADE_H_

namespace numa {
  template<typename Sub>
  class spaceship_facade {
   public:
    friend bool operator==(Sub const& lhs, Sub const& rhs)
    {
      return lhs.equal_to(rhs);
    }

    friend bool operator!=(Sub const& lhs, Sub const& rhs)
    {
      return !lhs.equal_to(rhs);
    }

    friend bool operator<(Sub const& lhs, Sub const& rhs)
    {
      return lhs.compare(rhs) < 0;
    }

    friend bool operator<=(Sub const& lhs, Sub const& rhs)
    {
      return lhs.compare(rhs) <= 0;
    }

    friend bool operator>(Sub const& lhs, Sub const& rhs)
    {
      return lhs.compare(rhs) > 0;
    }

    friend bool operator>=(Sub const& lhs, Sub const& rhs)
    {
      return lhs.compare(rhs) >= 0;
    }
  };
}

#endif
