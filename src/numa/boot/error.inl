namespace numa::detail {
  template<typename T>
  std::string make_string(T&& value)
  {
    thread_local std::ostringstream oss;
    oss.str({});
    oss << std::setprecision(10);
    oss << std::forward<T>(value);
    return oss.str();
  }

  template<typename... Ts>
  std::vector<std::string> make_vector_of_strings(Ts&&... values)
  {
    std::vector<std::string> retval;
    retval.reserve(sizeof...(Ts));

    (retval.push_back(make_string(values)), ...);

    return retval;
  }

  void dump_impl(std::vector<std::string> const& strs);

  template<typename... Ts>
  void dump(Ts&&... values)
  {
    dump_impl(make_vector_of_strings(std::forward<Ts>(values)...));
  }

  void dump_var_impl(char const* names, std::vector<std::string> const& values);

  template<typename... Ts>
  void dump_var(char const* names, Ts&&... values)
  {
    dump_var_impl(names, make_vector_of_strings(std::forward<Ts>(values)...));
  }

  void dump_vec_impl(char const* name, std::vector<std::string> const& str_vec);

  template<typename T>
  void dump_vec(char const* name, std::vector<T> const& vec)
  {
    std::vector<std::string> str_vec;
    str_vec.reserve(vec.size());

    thread_local std::ostringstream oss;

    for (auto const& x: vec) {
      oss.str({});
      oss << x;
      str_vec.push_back(oss.str());
    }

    dump_vec_impl(name, str_vec);
  }

  [[noreturn]] void error_handler_str(std::string const& msg);

  template<typename... Ts>
  [[noreturn]] void error_handler(Ts&&... values)
  {
    thread_local std::ostringstream oss;

    oss.str({});
    (oss << ... << std::forward<Ts>(values));

    error_handler_str(oss.str());
  }
}
