#ifndef INCLUDE_GUARD_NUMA_BOOT_STACK_TREE_H_
#define INCLUDE_GUARD_NUMA_BOOT_STACK_TREE_H_

#include "bits.hpp"
#include "fwd.hpp"

#include <atomic>
#include <deque>
#include <mutex>
#include <optional>

namespace numa {
  constexpr static inline index_t stack_tree_invalid_id = -1;

  struct stack_tree_explicit_t {
  };

  namespace {
    constexpr stack_tree_explicit_t stack_tree_explicit = {};
  }

  template<typename T>
  class stack_tree {
   public:
    /*
     * Master interface
     */

    /*
     * Args construct the root node.
     * stack_tree can never be empty.
     */
    template<typename Arg, typename... Args>
    requires(!std::is_same_v<std::decay_t<Arg>, stack_tree_explicit_t>)
        stack_tree(Arg&&, Args&&...);

    template<typename... Args>
    stack_tree(stack_tree_explicit_t, index_t root_id, Args&&...);

    stack_tree(stack_tree&&)      = delete;
    stack_tree(stack_tree const&) = delete;
    stack_tree& operator=(stack_tree&&) = delete;
    stack_tree& operator=(stack_tree const&) = delete;

    template<typename... Args>
    index_t insert_first_child(index_t parent_id, Args&&...);

    template<typename... Args>
    void insert_first_child(
        stack_tree_explicit_t, index_t parent_id, index_t child_id, Args&&...);

    template<typename... Args>
    index_t insert_next_sibling(index_t prev_sibling_id, Args&&...);

    template<typename... Args>
    void insert_next_sibling(
        stack_tree_explicit_t,
        index_t prev_sibling_id,
        index_t next_sibling_id,
        Args&&...);

    /*
     * Common interface
     */
    bool is_valid(index_t) const;

    index_t root_id() const;

    index_t get_parent(index_t) const;
    index_t get_first_child(index_t) const;
    index_t get_next_sibling(index_t) const;

    index_t get_successor(index_t) const;

    index_t get_depth(index_t) const;

    index_t find_child(index_t, T const& value) const;

    T& get(index_t);
    T const& get(index_t) const;

    void erase_all_children(index_t);

    template<std::Invocable<T&, index_t> F>
    void for_each_child(index_t, F&& f);

    template<std::Invocable<T const&, index_t, index_t> F>
    void for_each(F&& f) const;

   private:
    struct node {
      index_t first_child_id_  = stack_tree_invalid_id;
      index_t next_sibling_id_ = stack_tree_invalid_id;

      index_t parent_id_ = stack_tree_invalid_id;

      std::optional<T> payload_;
    };

    void ensure_id(index_t);

    index_t const root_id_;

    mutable std::recursive_mutex mtx_;
    std::deque<node> nodes_;
  };

  template<typename T>
  void to_pretty(std::ostream& os, stack_tree<T> const&);
}

#include "stack_tree.inl"

#endif
