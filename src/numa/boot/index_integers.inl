namespace numa {
  namespace detail {
    template<typename X, index_t N, X... Ns>
    constexpr std::array<X, N>
        make_array_iota_impl(std::integer_sequence<X, Ns...>)
    {
      return {Ns...};
    }

    template<typename X, index_t N, X... Ns>
    constexpr std::array<X, N>
        make_array_one_impl(std::integer_sequence<X, Ns...>)
    {
      return {((Ns < N) ? 1 : 0)...};
    }

    template<typename X, index_t N, X... Ns>
    constexpr std::array<X, N>
    make_array_unit_impl(X const ind, std::integer_sequence<X, Ns...>)
    {
      return {((Ns == ind) ? 1 : 0)...};
    }
  }

  template<index_t N, typename... Indices>
  struct is_index_integers {
    constexpr static bool right_number = (sizeof...(Indices) == N);

    constexpr static bool is_int = (std::is_integral<Indices>::value && ...);

    constexpr static bool any_char =
        (std::is_same<Indices, char>::value || ...);

    constexpr static bool value = right_number && is_int && (!any_char);
  };
}
