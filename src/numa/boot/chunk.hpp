#ifndef INCLUDE_GUARD_NUMA_BOOT_CHUNK_H_
#define INCLUDE_GUARD_NUMA_BOOT_CHUNK_H_

#include "bits.hpp"
#include "concepts.hpp"
#include "fwd.hpp"
#include "modes.hpp"
#include "span.hpp"

#include <algorithm>
#include <cmath>

namespace numa {
  /**
   * Returns the number of chunks required to cover @p size
   * if the chunks have size @p chunk_size.
   */
  template<std::Integral I>
  constexpr I chunk_required(I size, I chunk_size);

  template<std::Integral I>
  constexpr I chunk_required_size(I size, I chunk_size);

  template<std::Integral I, SubMode SM, typename F, SubMode SI>
  constexpr std::invoke_result_t<F, I, I>
  chunk_invoke(SM, F&& f, SI, I const from, I const size_or_till);

  template<std::Integral I, std::Invocable<I, I> F>
  constexpr void chunk_equal(I size, I num_chunks, SubMode auto, F&& f);

  template<std::Integral I, std::Invocable<I, I> F>
  constexpr void
  chunk_nth(I size, I num_chunks, I n, I min, SubMode auto, F&& f);

  template<std::Integral I, std::Invocable<I, I> F, std::Range R>
  constexpr void chunk_approx(I size, R&& ratios, I min, SubMode auto, F&& f);

  /*
   * TODO
   */
  constexpr void chunk_geometric(OrdMode auto);
  constexpr void chunk_linear(OrdMode auto, LinMode auto);
}

#include "chunk.inl"

#endif
