#ifndef INCLUDE_GUARD_NUMA_BOOT_MODES_H_
#define INCLUDE_GUARD_NUMA_BOOT_MODES_H_

#include "fwd.hpp"
#include "index.hpp"

namespace numa {
  struct as_till_t {
  };
  struct as_size_t {
  };

  struct ascending_t {
  };
  struct descending_t {
  };

  struct as_incr_t {
  };
  struct as_back_t {
  };

  namespace {
    constexpr as_till_t as_till;
    constexpr as_size_t as_size;

    constexpr ascending_t ascending;
    constexpr descending_t descending;

    constexpr as_incr_t as_incr;
    constexpr as_back_t as_back;
  }

  template<typename T>
  concept SubMode = std::Same<T, as_till_t> || std::Same<T, as_size_t>;

  template<typename T>
  concept OrdMode = std::Same<T, ascending_t> || std::Same<T, descending_t>;

  template<typename T>
  concept LinMode = std::Same<T, as_incr_t> || std::Same<T, as_back_t>;

  /*
   * Helper Functions
   */
  template<SubMode SM, typename I, SubMode SM_Result>
  constexpr I
  sub_mode_convert(SM, I const from, I const size_or_till, SM_Result);

  template<SubMode SM, index_t N, typename X, SubMode SM_Result>
  constexpr index<N, X> sub_mode_convert(
      SM, index<N, X> const& from, index<N, X> const& size_or_till, SM_Result);
}

#include "modes.inl"

#endif
