#ifndef INCLUDE_GUARD_NUMA_BOOT_BITS_H_
#define INCLUDE_GUARD_NUMA_BOOT_BITS_H_

#include "error.hpp"
#include "fwd.hpp"

#include <numeric>

namespace numa {
  template<typename... Ts>
  struct overload : public Ts... {
    overload(Ts... ts);

    using Ts::operator()...;
  };

  template<typename F, typename... Ts>
  void for_each_argument(F&& f, Ts&&... args);

  template<typename F, typename... Ts>
  void for_each_argument_indexed(F&& f, Ts&&... args);

  template<typename T>
  using remove_const_ref_t = std::remove_const_t<std::remove_reference_t<T>>;

  template<typename T>
  constexpr T lcm(std::initializer_list<T> ilist);

  template<typename Cleanup>
  class scope_guard {
   public:
    scope_guard(Cleanup cleanup) : cleanup_(std::move(cleanup)) {}

    template<typename Init>
    scope_guard(Init init, Cleanup cleanup) : cleanup_(std::move(cleanup))
    {
      init();
    }

    scope_guard(scope_guard&&)      = delete;
    scope_guard(scope_guard const&) = delete;
    scope_guard& operator=(scope_guard&&) = delete;
    scope_guard& operator=(scope_guard const&) = delete;

    ~scope_guard()
    {
      cleanup_();
    }

   private:
    Cleanup cleanup_;
  };
}

namespace std {
  struct nonesuch {
    nonesuch()                = delete;
    ~nonesuch()               = delete;
    nonesuch(nonesuch const&) = delete;
    void operator=(nonesuch const&) = delete;
  };

  namespace detail {
    template<
        class Default,
        class AlwaysVoid,
        template<class...>
        class Op,
        class... Args>
    struct detector {
      using value_t = std::false_type;
      using type    = Default;
    };

    template<class Default, template<class...> class Op, class... Args>
    struct detector<Default, std::void_t<Op<Args...>>, Op, Args...> {
      using value_t = std::true_type;
      using type    = Op<Args...>;
    };
  }

  template<template<class...> class Op, class... Args>
  using is_detected =
      typename detail::detector<nonesuch, void, Op, Args...>::value_t;

  template<template<class...> class Op, class... Args>
  using detected_t =
      typename detail::detector<nonesuch, void, Op, Args...>::type;

  template<class Default, template<class...> class Op, class... Args>
  using detected_or = detail::detector<Default, void, Op, Args...>;

  template<template<class...> class Op, class... Args>
  constexpr static bool is_detected_v = is_detected<Op, Args...>::value;

  template<class Default, template<class...> class Op, class... Args>
  using detected_or_t = typename detected_or<Default, Op, Args...>::type;

  template<class Expected, template<class...> class Op, class... Args>
  using is_detected_exact = is_same<Expected, detected_t<Op, Args...>>;

  template<class Expected, template<class...> class Op, class... Args>
  constexpr static bool is_detected_exact_v =
      is_detected_exact<Expected, Op, Args...>::value;

  template<class To, template<class...> class Op, class... Args>
  using is_detected_convertible = is_convertible<detected_t<Op, Args...>, To>;

  template<class To, template<class...> class Op, class... Args>
  constexpr static bool is_detected_convertible_v =
      is_detected_convertible<To, Op, Args...>::value;
}

namespace numa {
  template<typename T, typename = void>
  struct is_defined : public std::false_type {
  };

  template<typename T>
  struct is_defined<T, std::enable_if_t<(sizeof(T) > 0)>>
    : public std::true_type {
  };

  template<typename T>
  class class_wrapper;

  template<typename T>
  requires std::is_class_v<T> class class_wrapper<T> : public T {
   public:
    using T::T;
  };

  template<typename T>
  requires(!std::is_class_v<T>) class class_wrapper<T> {
   public:
    constexpr class_wrapper(T const& value) : value_(value) {}

    constexpr operator T&()
    {
      return value_;
    }
    constexpr operator T const &() const
    {
      return value_;
    }

    constexpr T& value() &
    {
      return value_;
    }
    constexpr T const& value() const&
    {
      return value_;
    }

    constexpr T&& value() &&
    {
      return std::move(value_);
    }
    constexpr T const&& value() const&&
    {
      return std::move(value_);
    }

   private:
    T value_;
  };
}

namespace std {
  template<typename T>
  requires std::is_class_v<T> struct hash<numa::class_wrapper<T>> {
    using argument_type = numa::class_wrapper<T>;
    using result_type   = decltype(std::hash<T>{}(std::declval<T const&>()));

    result_type operator()(numa::class_wrapper<T> const& value) const noexcept
    {
      return std::hash<T>{}(static_cast<T const&>(value));
    }
  };

  template<typename T>
  requires(!std::is_class_v<T>) struct hash<numa::class_wrapper<T>> {
    using argument_type = numa::class_wrapper<T>;
    using result_type   = decltype(std::hash<T>{}(std::declval<T const&>()));

    result_type operator()(numa::class_wrapper<T> const& value) const noexcept
    {
      return std::hash<T>{}(value.value());
    }
  };
}

namespace numa {
  template<typename T>
  auto hash(T const& value)
  {
    return std::hash<T>{}(value);
  }
}

#include "bits.inl"

#endif
