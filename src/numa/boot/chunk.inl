namespace numa {
  template<std::Integral I>
  constexpr I chunk_required(I const size, I const chunk_size)
  {
    ASSERT(size >= 0);
    ASSERT(chunk_size > 0);

    if (size == 0) {
      return 0;
    }
    else {
      return (((size - 1) / chunk_size) + 1);
    }
  }

  template<std::Integral I>
  constexpr I chunk_required_size(I const size, I const chunk_size)
  {
    return chunk_size * chunk_required(size, chunk_size);
  }

  template<std::Integral I, SubMode SM, typename F, SubMode SI>
  constexpr std::invoke_result_t<F, I, I>
  chunk_invoke(SM, F&& f, SI, I const from, I const size_or_till)
  {
    return std::forward<F>(f)(
        from, sub_mode_convert(SI{}, from, size_or_till, SM{}));
  }

  template<std::Integral I, std::Invocable<I, I> F>
  constexpr void
  chunk_equal(I const size, I const num_chunks, SubMode auto sub_mode, F&& f)
  {
    ASSERT(size >= 0);
    ASSERT(num_chunks >= 1);

    I const min_size = (size / num_chunks);

    I const num_plus_one_size = size - (min_size * num_chunks);

    I from = 0;
    I till = 0;

    for (I i = 0; i < num_plus_one_size; ++i) {
      till += (min_size + 1);

      chunk_invoke<I>(sub_mode, f, as_till, from, till);
      from = till;
    }

    for (I i = num_plus_one_size; i < num_chunks; ++i) {
      till += min_size;

      chunk_invoke<I>(sub_mode, f, as_till, from, till);
      from = till;
    }
  }

  template<std::Integral I, std::Invocable<I, I> F>
  constexpr void chunk_nth(
      I const size,
      I const num_chunks,
      I const n,
      I const min,
      SubMode auto sub_mode,
      F&& f)
  {
    ASSERT(num_chunks >= 1);

    if (n < 0) {
      ASSERT(-n <= num_chunks);

      chunk_nth<I>(
          size, num_chunks, num_chunks + n, min, sub_mode, std::forward<F>(f));
    }
    else {
      ASSERT(0 <= min);
      ASSERT(n < num_chunks);

      if (size <= (num_chunks * min)) {
        return chunk_equal(size, num_chunks, sub_mode, std::forward<F>(f));
      }

      I cur = 0;

      for (I i = 0; i < n; ++i) {
        chunk_invoke<I>(sub_mode, f, as_size, cur, min);
        cur += min;
      }

      I const add_big_chunk = (size - (num_chunks * min));
      I const big_chunk     = (min + add_big_chunk);

      chunk_invoke<I>(sub_mode, f, as_size, cur, big_chunk);
      cur += big_chunk;

      for (I i = (n + 1); i < num_chunks; ++i) {
        chunk_invoke<I>(sub_mode, f, as_size, cur, min);
        cur += min;
      }
    }
  }

  template<std::Integral I, std::Invocable<I, I> F, std::Range R>
  constexpr void chunk_approx(
      I const size, R&& ratios, I const min, SubMode auto sub_mode, F&& f)
  {
    auto const [sum, num_chunks] = [&] {
      double sum   = 0.0;
      I num_chunks = 0;

      for (auto const& r: ratios) {
        double const dr = static_cast<double>(r);
        ASSERT(dr >= 0);
        sum += dr;
        ++num_chunks;
      }

      return std::tuple{sum, num_chunks};
    }();

    ASSERT(0 <= min);
    ASSERT(num_chunks >= 1);

    if (size <= (num_chunks * min)) {
      return chunk_equal(size, num_chunks, sub_mode, std::forward<F>(f));
    }

    I const bound_size = num_chunks * min;

    double const bound_sum =
        static_cast<double>(bound_size) / static_cast<double>(size) * sum;

    double const bound_sum_per_chunk =
        bound_sum / static_cast<double>(num_chunks);

    double const free_sum = [&] {
      double retval = 0.0;

      for (auto const& r: ratios) {
        double const dr = static_cast<double>(r);
        ASSERT(dr >= 0);

        if (0.0 < (dr - bound_sum_per_chunk)) {
          retval += (dr - bound_sum_per_chunk);
        }
      }

      return retval;
    }();

    I const free_size = size - bound_size;

    double const h = free_sum / static_cast<double>(free_size);

    /*
     * The factor (1.0 + 1e-9) is meant to favor chunks with a lower index in
     * the presence of round-off errors.
     */
    double buffer = (h / 2.0) * (1.0 + 1e-9);
    I size_left   = free_size;
    I cur         = 0;
    I i           = 0;

    for (auto const& r: ratios) {
      double const dr =
          std::max(0.0, static_cast<double>(r) - bound_sum_per_chunk);

      buffer += dr;

      I const c = [&] {
        if ((i + 1) == num_chunks) {
          return size_left;
        }
        else {
          I retval = static_cast<I>(std::floor(buffer / h));

          retval = std::min(size_left, retval);

          return retval;
        }
      }();

      buffer -= (c * h);
      size_left -= c;

      chunk_invoke<I>(sub_mode, f, as_size, cur, min + c);
      cur += (min + c);
      ++i;
    }
  }
}
