namespace numa {
  template<SubMode SM, typename I, SubMode SM_Result>
  constexpr I
  sub_mode_convert(SM, I const from, I const size_or_till, SM_Result)
  {
    if constexpr (std::Same<SM, SM_Result>) {
      return size_or_till;
    }
    else if constexpr (
        std::Same<SM, as_size_t> && std::Same<SM_Result, as_till_t>) {
      I const size = size_or_till;
      return (from + size);
    }
    else {
      static_assert(
          std::Same<SM, as_till_t> && std::Same<SM_Result, as_size_t>);

      I const till = size_or_till;
      return (till - from);
    }
  }

  template<SubMode SM, index_t N, typename X, SubMode SM_Result>
  constexpr index<N, X> sub_mode_convert(
      SM, index<N, X> const& from, index<N, X> const& size_or_till, SM_Result)
  {
    index<N, X> retval = zero;

    for (index_t i = 0; i < N; ++i) {
      retval[i] = sub_mode_convert(SM{}, from[i], size_or_till[i], SM_Result{});
    }

    return retval;
  }
}
