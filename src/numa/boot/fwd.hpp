#ifndef INCLUDE_GUARD_NUMA_BOOT_FWD_H_
#define INCLUDE_GUARD_NUMA_BOOT_FWD_H_

#include "concepts.hpp"

#include <cstdint>
#include <limits>

namespace numa {
  using index_t = int64_t;

  constexpr index_t index_dynamic = -1;

  using hash_value_t = uint64_t;

  struct uninitialized_t {
  };
  struct expert_t {
  };

  struct zero_t {
  };
  struct one_t {
  };
  struct iota_t {
  };
  struct riota_t {
  };
  struct unit_t {
  };

  struct null_t {
  };

  struct x_t {
  };

  struct undefined_t;

  namespace {
    constexpr uninitialized_t uninitialized;
    constexpr expert_t expert;

    constexpr zero_t zero;
    constexpr one_t one;
    constexpr iota_t iota;
    constexpr riota_t riota;
    constexpr unit_t unit;

    constexpr x_t x;

    constexpr null_t null;
  }
}

#define no_op

#endif
