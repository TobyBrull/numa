#ifndef INCLUDE_GUARD_NUMA_BOOT_PREPROCESSOR_H_
#define INCLUDE_GUARD_NUMA_BOOT_PREPROCESSOR_H_

namespace numa {
/*
 * double expansion trick
 */
#define TO_STRING_(x) #x
#define TO_STRING(x)  TO_STRING_(x)
#define FILE_LINE     __FILE__ ":" TO_STRING(__LINE__)

/*
 * Another preprocessor trick
 */
#define PP_CAT(a, b)      PP_CAT_I(a, b)
#define PP_CAT_I(a, b)    PP_CAT_II(~, a##b)
#define PP_CAT_II(p, res) res

#define UNIQUE_NAME(base) PP_CAT(base, __COUNTER__)

/*
 *
 */
#define ATTRIBUTE_USED __attribute__((__used__))
}

#endif
