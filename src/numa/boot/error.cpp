#include "error.hpp"

#include <iostream>
#include <mutex>
#include <regex>

namespace numa::detail {
  void dump_impl(std::vector<std::string> const& strs)
  {
    for (auto const& str: strs) {
      std::cout << str;
    }

    std::cout << '\n';
  }

  std::vector<std::string> chunk_names(std::string const& names)
  {
    static std::regex const re{"\\s*,\\s*"};
    std::sregex_token_iterator str_iter(names.begin(), names.end(), re, -1);
    std::sregex_token_iterator const str_end;

    std::vector<std::string> retval;

    while (str_iter != str_end) {
      retval.push_back(*str_iter);
      ++str_iter;
    }

    return retval;
  }

  void dump_vec_impl(char const* name, std::vector<std::string> const& str_vec)
  {
    std::cout << name << " = [";

    bool first = true;
    for (auto const& str: str_vec) {
      if (!first) {
        std::cout << ", ";
      }
      else {
        first = false;
      }

      std::cout << str;
    }

    std::cout << "]\n";
  }

  void
  dump_var_impl(char const* names_str, std::vector<std::string> const& values)
  {
    static std::mutex mtx;
    std::unique_lock lk{mtx};

    auto const& names = chunk_names(names_str);
    size_t const n    = names.size();

    THROW_IF_NOT(n == values.size(), "numa::dump_var: could not parse names");

    std::cout << "/--------------\n";
    for (size_t i = 0; i < n; ++i) {
      std::cout << '|' << names.at(i) << " = " << values.at(i) << '\n';
    }
    std::cout << "\\--------------\n";
  }

  [[noreturn]] void error_handler_str(std::string const& msg)
  {
    throw std::runtime_error(msg);
  }
}
