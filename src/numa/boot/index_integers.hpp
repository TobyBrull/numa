#ifndef INCLUDE_GUARD_NUMA_BOOT_INDEX_INTEGERS_H_
#define INCLUDE_GUARD_NUMA_BOOT_INDEX_INTEGERS_H_

#include "error.hpp"
#include "fwd.hpp"

#include "index_integers.inl"

namespace numa {
  template<index_t N, typename... Indices>
  constexpr bool is_index_integers_v = is_index_integers<N, Indices...>::value;
}

#endif
