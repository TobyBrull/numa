namespace numa {
  template<typename... T>
  struct is_index : public std::false_type {
  };

  template<index_t N, typename X>
  struct is_index<index<N, X>> : public std::true_type {
  };

  template<typename... T>
  inline constexpr bool is_index_v = is_index<T...>::value;

  template<typename... Index>
  struct is_unit_int : public std::false_type {
  };

  template<>
  struct is_unit_int<unit_t, index_t> : public std::true_type {
  };

  template<>
  struct is_unit_int<unit_t, int> : public std::true_type {
  };

  template<index_t N, typename... Index>
  struct constructs_index {
    constexpr static bool is_special =
        (is_unit_int<Index...>::value ||
         std::is_same_v<std::tuple<Index...>, std::tuple<zero_t>> ||
         std::is_same_v<std::tuple<Index...>, std::tuple<one_t>> ||
         std::is_same_v<std::tuple<Index...>, std::tuple<iota_t>>);

    constexpr static bool is_already = is_index<Index...>::value;

    constexpr static bool value =
        is_special || is_already || is_index_integers_v<N, Index...>;
  };

  template<index_t N, typename... Index>
  inline constexpr bool constructs_index_v =
      constructs_index<N, Index...>::value;

  template<index_t N, typename X>
  template<typename... Indices>
  requires is_index_integers_v<N, Indices...> constexpr index<N, X>::index(
      Indices... inds) noexcept
    : data_{inds...}
  {
  }

  template<index_t N, typename X>
  constexpr index<N, X>::index() noexcept : data_{}
  {
  }

  template<index_t N, typename X>
  index<N, X>::index(uninitialized_t) noexcept
  {
  }

  template<index_t N, typename X>
  constexpr index<N, X>::index(zero_t) noexcept : data_{}
  {
  }

  template<index_t N, typename X>
  constexpr index<N, X>::index(iota_t) noexcept
    : data_{detail::make_array_iota_impl<X, N>(
          std::make_integer_sequence<X, N>())}
  {
  }

  template<index_t N, typename X>
  constexpr index<N, X>::index(one_t) noexcept
    : data_{
          detail::make_array_one_impl<X, N>(std::make_integer_sequence<X, N>())}
  {
  }

  template<index_t N, typename X>
  constexpr index<N, X>::index(unit_t, index_t const ind)
    : data_{detail::make_array_unit_impl<X, N>(
          ind, std::make_integer_sequence<X, N>())}
  {
  }

  template<index_t N, typename X>
  constexpr X* index<N, X>::begin() noexcept
  {
    return data_.data();
  }

  template<index_t N, typename X>
  constexpr X* index<N, X>::end() noexcept
  {
    return data_.data() + N;
  }

  template<index_t N, typename X>
  constexpr X const* index<N, X>::begin() const noexcept
  {
    return cbegin();
  }

  template<index_t N, typename X>
  constexpr X const* index<N, X>::end() const noexcept
  {
    return cend();
  }

  template<index_t N, typename X>
  constexpr X const* index<N, X>::cbegin() const noexcept
  {
    return data_.data();
  }

  template<index_t N, typename X>
  constexpr X const* index<N, X>::cend() const noexcept
  {
    return data_.data() + N;
  }

  template<index_t N, typename X>
  constexpr index<N, X>::operator X() const noexcept requires(N == 1)
  {
    return data_[0];
  }

  template<index_t N, typename X>
  constexpr index_t index<N, X>::size() noexcept
  {
    return N;
  }

  template<index_t N, typename X>
  constexpr X& index<N, X>::operator[](const index_t ind)
  {
    return data_[ind];
  }

  template<index_t N, typename X>
  constexpr X index<N, X>::operator[](const index_t ind) const
  {
    return data_[ind];
  }

  /*
   * Implementation of free functions
   */
  template<index_t N, typename X>
  constexpr bool operator==(index<N, X> const& lhs, index<N, X> const& rhs)
  {
    for (index_t i = 0; i < N; ++i) {
      if (lhs[i] != rhs[i]) {
        return false;
      }
    }

    return true;
  }

  template<index_t N, typename X>
  constexpr bool operator==(index<N, X> const& lhs, zero_t)
  {
    return (lhs == index<N, X>(zero));
  }

  template<index_t N, typename X>
  constexpr bool operator!=(index<N, X> const& lhs, index<N, X> const& rhs)
  {
    return !(lhs == rhs);
  }

  template<index_t N, typename X>
  constexpr bool operator!=(index<N, X> const& lhs, zero_t)
  {
    return (lhs != index<N, X>(zero));
  }

  template<index_t N, typename X>
  constexpr bool operator<(index<N, X> const& lhs, index<N, X> const& rhs)
  {
    return is_before_strictly(lhs, rhs);
  }

  template<index_t N, typename X>
  constexpr bool operator>(index<N, X> const& lhs, index<N, X> const& rhs)
  {
    return is_before_strictly(rhs, lhs);
  }

  template<index_t N, typename X>
  constexpr bool operator<=(index<N, X> const& lhs, index<N, X> const& rhs)
  {
    return is_before(lhs, rhs);
  }

  template<index_t N, typename X>
  constexpr bool operator>=(index<N, X> const& lhs, index<N, X> const& rhs)
  {
    return is_before(rhs, lhs);
  }

  template<index_t N, typename X>
  constexpr bool is_before(index<N, X> const& lhs, index<N, X> const& rhs)
  {
    if constexpr (N == 0) {
      return true;
    }
    else {
      for (index_t i = N; 0 < i; --i) {
        if (lhs[i - 1] < rhs[i - 1]) {
          return true;
        }
        else if (lhs[i - 1] > rhs[i - 1]) {
          return false;
        }
      }

      return true;
    }
  }

  template<index_t N, typename X>
  constexpr bool
  is_before_strictly(index<N, X> const& lhs, index<N, X> const& rhs)
  {
    if constexpr (N == 0) {
      return false;
    }
    else {
      for (index_t i = N; 0 < i; --i) {
        if (lhs[i - 1] < rhs[i - 1]) {
          return true;
        }
        else if (lhs[i - 1] > rhs[i - 1]) {
          return false;
        }
      }

      return false;
    }
  }

  template<index_t N, typename X>
  constexpr bool is_bounded(index<N, X> const& lhs, index<N, X> const& rhs)
  {
    for (index_t i = 0; i < N; ++i) {
      if (!(lhs[i] <= rhs[i])) {
        return false;
      }
    }

    return true;
  }

  template<index_t N, typename X>
  constexpr bool
  is_bounded_strictly(index<N, X> const& lhs, index<N, X> const& rhs)
  {
    for (index_t i = 0; i < N; ++i) {
      if (!(lhs[i] < rhs[i])) {
        return false;
      }
    }

    if constexpr (N == 0) {
      return false;
    }
    else {
      return true;
    }
  }

  template<typename... IndexArgs>
  // requires (is_index_v<IndexArgs> && ...)
  constexpr bool is_all_equal(IndexArgs... index_args)
  {
    if constexpr (sizeof...(IndexArgs) == 0) {
      return true;
    }
    else {
      return [](auto const index_0, auto const... index_rest) {
        return ((index_0 == index_rest) && ...);
      }(index_args...);
    }
  }

  template<index_t N, typename X>
  constexpr index<N, X>
  operator+(index<N, X> const& lhs, index<N, X> const& rhs)
  {
    index<N, X> retval = lhs;

    for (index_t i = 0; i < N; ++i) {
      retval[i] += rhs[i];
    }

    return retval;
  }

  template<index_t N, typename X>
  constexpr index<N, X>
  operator-(index<N, X> const& lhs, index<N, X> const& rhs)
  {
    index<N, X> retval = lhs;

    for (index_t i = 0; i < N; ++i) {
      retval[i] -= rhs[i];
    }

    return retval;
  }

  template<index_t N, typename X>
  constexpr index<N, X> operator-(index<N, X> const& op)
  {
    index<N, X> retval = op;

    for (index_t i = 0; i < N; ++i) {
      retval[i] = -retval[i];
    }

    return retval;
  }

  template<index_t N, typename X, typename Y>
  constexpr index<N, Y> operator*(X const fact, index<N, Y> const& ind)
  {
    index<N, Y> retval = ind;

    for (auto& i: retval) {
      i *= fact;
    }

    return retval;
  }

  template<index_t N, typename X, typename Y>
  constexpr index<N, X> operator*(index<N, X> const& ind, Y const fact)
  {
    return fact * ind;
  }

  template<index_t N, typename X>
  constexpr void operator+=(index<N, X>& ind, index<N, X> const& rhs)
  {
    for (index_t i = 0; i < N; ++i) {
      ind[i] += rhs[i];
    }
  }

  template<index_t N, typename X>
  constexpr void operator-=(index<N, X>& ind, index<N, X> const& rhs)
  {
    for (index_t i = 0; i < N; ++i) {
      ind[i] -= rhs[i];
    }
  }

  template<index_t N, typename X, typename Y>
  constexpr void operator*=(index<N, X>& ind, Y const rhs)
  {
    for (index_t i = 0; i < N; ++i) {
      ind[i] *= rhs;
    }
  }

  template<index_t N, typename X, typename Y>
  constexpr void operator/=(index<N, X>& ind, Y const rhs)
  {
    for (index_t i = 0; i < N; ++i) {
      ind[i] /= rhs;
    }
  }

  template<index_t N, typename X>
  constexpr X dot(index<N, X> const& a, index<N, X> const& b)
  {
    X retval = 0;

    for (index_t i = 0; i < N; ++i) {
      retval += (a[i] * b[i]);
    }

    return retval;
  }

  template<index_t N, typename X>
  constexpr index<N, X> min(index<N, X> const& a, index<N, X> const& b)
  {
    index<N, X> retval = a;

    for (index_t i = 0; i < N; ++i) {
      retval[i] = std::min(retval[i], b[i]);
    }

    return retval;
  }

  template<index_t N, typename X>
  constexpr index<N, X> max(index<N, X> const& a, index<N, X> const& b)
  {
    index<N, X> retval = a;

    for (index_t i = 0; i < N; ++i) {
      retval[i] = std::max(retval[i], b[i]);
    }

    return retval;
  }

  template<index_t N, typename X>
  constexpr index<N, X> abs(index<N, X> const& a)
  {
    index<N, X> retval = a;

    for (index_t i = 0; i < N; ++i) {
      if (retval[i] < 0) {
        retval[i] = -retval[i];
      }
    }

    return retval;
  }

  template<index_t N, typename X>
  requires(N >= 1) constexpr X min(index<N, X> const& ind)
  {
    return ind[argmin(ind)];
  }

  template<index_t N, typename X>
  requires(N >= 1) constexpr index_t argmin(index<N, X> const& ind)
  {
    index_t retval = 0;

    for (index_t i = 1; i < N; ++i) {
      if (ind[i] < ind[retval]) {
        retval = i;
      }
    }

    return retval;
  }

  template<index_t N, typename X>
  requires(N >= 1) constexpr X max(index<N, X> const& ind)
  {
    return ind[argmax(ind)];
  }

  template<index_t N, typename X>
  requires(N >= 1) constexpr index_t argmax(index<N, X> const& ind)
  {
    index_t retval = 0;

    for (index_t i = 1; i < N; ++i) {
      if (ind[i] > ind[retval]) {
        retval = i;
      }
    }

    return retval;
  }

  template<index_t N, typename X>
  constexpr index<N, X>
  splice(index<N, X> const& left, index<N, X> const& right, index_t const ind)
  {
    index<N, X> retval = left;

    for (index_t i = ind; i < N; ++i) {
      retval[i] = right[i];
    }

    return retval;
  }

  template<index_t K, index_t N, typename X>
  constexpr std::pair<index<K, X>, index<N - K, X>>
  split(index<N, X> const& ind)
  {
    static_assert(K <= N);

    std::pair<index<K, X>, index<N - K, X>> retval(zero, zero);

    for (index_t i = 0; i < N; ++i) {
      if (i < K) {
        retval.first[i] = ind[i];
      }
      else {
        retval.second[i - K] = ind[i];
      }
    }

    return retval;
  }

  template<index_t K_0, index_t K_1, typename X>
  constexpr index<K_0 + K_1, X>
  join(const index<K_0, X>& ind_0, const index<K_1, X>& ind_1)
  {
    index<K_0 + K_1, X> retval(zero);

    for (index_t i = 0; i < K_0; ++i) {
      retval[i] = ind_0[i];
    }

    for (index_t i = 0; i < K_1; ++i) {
      retval[K_0 + i] = ind_1[i];
    }

    return retval;
  }

  template<index_t N, typename X>
  constexpr X product(index<N, X> const& size)
  {
    X retval = 1;

    for (const auto i: size) {
      retval *= i;
    }

    return retval;
  }

  template<index_t N, typename X>
  constexpr bool is_empty(index<N, X> const& size)
  {
    for (const auto i: size) {
      if (i == 0) {
        return true;
      }
    }

    return false;
  }
}
