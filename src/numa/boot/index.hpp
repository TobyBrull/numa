#ifndef INCLUDE_GUARD_NUMA_BOOT_INDEX_H_
#define INCLUDE_GUARD_NUMA_BOOT_INDEX_H_

#include "index_integers.hpp"

#include <array>

namespace numa {
  /**
   * Type-traits for index.
   */
  // template<index_t N, typename... Index>
  // inline constexpr bool constructs_index_v;

  // template<typename... T>
  // inline constexpr bool is_index_v;

  /*
   * index implementation
   */
  template<index_t N, typename X = index_t>
  class index {
   public:
    static_assert(N >= 0);
    static_assert(std::is_signed_v<X>, "index-type has to be signed");

    template<typename... Indices>
    requires is_index_integers_v<N, Indices...> constexpr index(
        Indices... inds) noexcept;

    constexpr index() noexcept;

    index(uninitialized_t) noexcept;

    constexpr index(zero_t) noexcept;
    constexpr index(one_t) noexcept;
    constexpr index(iota_t) noexcept;
    constexpr index(unit_t, index_t ind);

    constexpr X* begin() noexcept;
    constexpr X* end() noexcept;
    constexpr X const* begin() const noexcept;
    constexpr X const* end() const noexcept;
    constexpr X const* cbegin() const noexcept;
    constexpr X const* cend() const noexcept;

    constexpr operator X() const noexcept requires(N == 1);

    constexpr static index_t size() noexcept;

    constexpr X& operator[](index_t ind);
    constexpr X operator[](index_t ind) const;

   private:
    std::array<X, N> data_;
  };

  /**
   * Template deduction guide, which counts the number of parameters.
   */
  template<typename... Indices>
  requires is_index_integers_v<sizeof...(Indices), Indices...>
  index(Indices... inds) -> index<sizeof...(Indices), index_t>;

  index()->index<0, index_t>;

  /**
   * Comparison operators.
   */
  template<index_t N, typename X>
  constexpr bool operator==(index<N, X> const&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr bool operator==(index<N, X> const&, zero_t);

  template<index_t N, typename X>
  constexpr bool operator!=(index<N, X> const&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr bool operator!=(index<N, X> const&, zero_t);

  template<index_t N, typename X>
  constexpr bool operator<(index<N, X> const&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr bool operator>(index<N, X> const&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr bool operator<=(index<N, X> const&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr bool operator>=(index<N, X> const&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr bool is_before(index<N, X> const&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr bool is_before_strictly(index<N, X> const&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr bool is_bounded(index<N, X> const&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr bool is_bounded_strictly(index<N, X> const&, index<N, X> const&);

  template<typename... IndexArgs>
  // requires (is_index_v<IndexArgs> && ...)
  constexpr bool is_all_equal(IndexArgs... index_args);

  /**
   * Basic arithmetic operators, akin to those of a vector-space.
   */
  template<index_t N, typename X>
  constexpr index<N, X> operator+(index<N, X> const&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr index<N, X> operator-(index<N, X> const&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr index<N, X> operator-(index<N, X> const&);

  template<index_t N, typename X, typename Y>
  constexpr index<N, Y> operator*(X, index<N, Y> const&);

  template<index_t N, typename X, typename Y>
  constexpr index<N, X> operator*(index<N, X> const&, Y);

  template<index_t N, typename X>
  constexpr void operator+=(index<N, X>&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr void operator-=(index<N, X>&, index<N, X> const&);

  template<index_t N, typename X, typename Y>
  constexpr void operator*=(index<N, X>&, Y);

  template<index_t N, typename X, typename Y>
  constexpr void operator/=(index<N, X>&, Y);

  /**
   * More binary arithmetic operators.
   */
  template<index_t N, typename X>
  constexpr X dot(index<N, X> const&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr index<N, X> min(index<N, X> const&, index<N, X> const&);

  template<index_t N, typename X>
  constexpr index<N, X> max(index<N, X> const&, index<N, X> const&);

  /**
   * More unary arithmetic operators.
   */
  template<index_t N, typename X>
  constexpr index<N, X> abs(index<N, X> const&);

  template<index_t N, typename X>
  requires(N >= 1) constexpr X min(index<N, X> const&);

  template<index_t N, typename X>
  requires(N >= 1) constexpr index_t argmin(index<N, X> const&);

  template<index_t N, typename X>
  requires(N >= 1) constexpr X max(index<N, X> const&);

  template<index_t N, typename X>
  requires(N >= 1) constexpr index_t argmax(index<N, X> const&);

  /**
   * Returns the index with the elements [0, ind) from
   * @p left and the elements [ind, N) from @p right.
   *
   * The parameter @p ind has to be in [0, N].
   */
  template<index_t N, typename X>
  constexpr index<N, X>
  splice(index<N, X> const& left, index<N, X> const& right, index_t ind);

  template<index_t K, index_t N, typename X>
  constexpr std::pair<index<K, X>, index<N - K, X>> split(index<N, X> const&);

  template<index_t K_0, index_t K_1, typename X>
  constexpr index<K_0 + K_1, X>
  join(index<K_0, X> const&, index<K_1, X> const&);

  /**
   * Returns the product of the values in @p size. This results in the total
   * number of elements that can be stored in a dense tensor of this size.
   *
   * Using the convention that the product of zero factors is one,
   * this function always returns 1 for index<0>.
   */
  template<index_t N, typename X>
  constexpr X product(index<N, X> const&);

  /**
   * Returns true if any value is zero.
   *
   * Note, that this implies that index<0> is never empty.
   */
  template<index_t N, typename X>
  constexpr bool is_empty(index<N, X> const&);
}

#include "index.inl"

#endif
