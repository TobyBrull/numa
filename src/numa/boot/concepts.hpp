#ifndef INCLUDE_GUARD_NUMA_BOOT_CONCEPTS_H_
#define INCLUDE_GUARD_NUMA_BOOT_CONCEPTS_H_

#include <iterator>
#include <type_traits>

namespace std {
  template<typename T>
  concept Integral = std::is_integral<T>::value;

  template<typename T>
  concept FloatingPoint = std::is_floating_point<T>::value;

  template<typename T>
  concept Arithmetic = std::is_arithmetic<T>::value;

  template<typename F, typename... T>
  concept Invocable = std::is_invocable<F, T...>::value;

  template<typename F, typename R, typename... T>
  concept InvocableR = std::is_invocable_r<R, F, T...>::value;

  template<typename T, typename U>
  concept Same = std::is_same<T, U>::value;

  template<typename T>
  concept Range = requires(T&& t)
  {
    {std::begin(t)};
    {std::end(t)};
  };
}

#endif
