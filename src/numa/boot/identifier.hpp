#ifndef INCLUDE_GUARD_NUMA_BOOT_IDENTIFIER_H_
#define INCLUDE_GUARD_NUMA_BOOT_IDENTIFIER_H_

#include "error.hpp"
#include "fwd.hpp"
#include "spaceship_facade.hpp"

#include <string>

namespace numa {
  bool is_identifier(std::string_view);

  class identifier : public spaceship_facade<identifier> {
   public:
    identifier() = default;

    identifier(std::string);

    identifier(char const*);

    bool is_empty() const;

    operator std::string const &() const;

    std::string const& data() const;

    bool equal_to(identifier const&) const;
    int compare(identifier const&) const;

   private:
    std::string data_;
  };
}

#include "identifier.inl"

#endif
