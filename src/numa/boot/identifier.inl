namespace numa {
  inline identifier::identifier(std::string data) : data_(std::move(data))
  {
    THROW_IF_NOT(
        is_identifier(data_),
        "numa::identifier: may only contain [_a-zA-Z0-9] and "
        "may not start with a digit");
  }

  inline identifier::identifier(char const* data)
    : identifier(std::string(data))
  {
  }

  inline bool identifier::is_empty() const
  {
    return data_.empty();
  }

  inline identifier::operator std::string const &() const
  {
    return data_;
  }

  inline std::string const& identifier::data() const
  {
    return data_;
  }

  inline bool identifier::equal_to(identifier const& other) const
  {
    return data_ == other.data_;
  }

  inline int identifier::compare(identifier const& other) const
  {
    return data_.compare(other.data_);
  }
}
