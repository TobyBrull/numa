namespace numa {
  /*
   * stack_tree
   */
  template<typename T>
  template<typename Arg, typename... Args>
  requires(!std::is_same_v<std::decay_t<Arg>, stack_tree_explicit_t>)
      stack_tree<T>::stack_tree(Arg&& arg, Args&&... args)
    : stack_tree(
          stack_tree_explicit,
          0,
          std::forward<Arg>(arg),
          std::forward<Args>(args)...)
  {
  }

  template<typename T>
  template<typename... Args>
  stack_tree<T>::stack_tree(
      stack_tree_explicit_t, index_t const root_id, Args&&... args)
    : root_id_(root_id)
  {
    ensure_id(root_id);

    nodes_.at(root_id).payload_.emplace(std::forward<Args>(args)...);
  }

  template<typename T>
  template<typename... Args>
  index_t
  stack_tree<T>::insert_first_child(index_t const parent_id, Args&&... args)
  {
    std::unique_lock lk{mtx_};

    index_t const new_id = static_cast<index_t>(nodes_.size());

    insert_first_child(
        stack_tree_explicit, parent_id, new_id, std::forward<Args>(args)...);

    return new_id;
  }

  template<typename T>
  template<typename... Args>
  void stack_tree<T>::insert_first_child(
      stack_tree_explicit_t,
      index_t const parent_id,
      index_t const child_id,
      Args&&... args)
  {
    std::unique_lock lk{mtx_};

    ASSERT(is_valid(parent_id));
    ASSERT(!is_valid(child_id));

    ensure_id(child_id);

    auto& parent = nodes_.at(parent_id);
    auto& child  = nodes_.at(child_id);

    ASSERT(!child.payload_.has_value());
    child.payload_.emplace(std::forward<Args>(args)...);

    child.parent_id_ = parent_id;

    if (parent.first_child_id_ == stack_tree_invalid_id) {
      parent.first_child_id_ = child_id;
    }
    else {
      child.next_sibling_id_ = parent.first_child_id_;
      parent.first_child_id_ = child_id;
    }
  }

  template<typename T>
  template<typename... Args>
  index_t stack_tree<T>::insert_next_sibling(
      index_t const prev_sibling_id, Args&&... args)
  {
    std::unique_lock lk{mtx_};

    index_t const new_id = static_cast<index_t>(nodes_.size());

    insert_next_sibling(
        stack_tree_explicit,
        prev_sibling_id,
        new_id,
        std::forward<Args>(args)...);

    return new_id;
  }

  template<typename T>
  template<typename... Args>
  void stack_tree<T>::insert_next_sibling(
      stack_tree_explicit_t,
      index_t const prev_sibling_id,
      index_t const next_sibling_id,
      Args&&... args)
  {
    std::unique_lock lk{mtx_};

    ASSERT(is_valid(prev_sibling_id));
    ASSERT(!is_valid(next_sibling_id));

    ensure_id(next_sibling_id);

    auto& prev_sibling = nodes_[prev_sibling_id];
    auto& next_sibling = nodes_[next_sibling_id];

    ASSERT(!next_sibling.payload_.has_value());
    next_sibling.payload_.emplace(std::forward<Args>(args)...);

    next_sibling.parent_id_ = prev_sibling.parent_id_;

    if (prev_sibling.next_sibling_id_ == stack_tree_invalid_id) {
      prev_sibling.next_sibling_id_ = next_sibling_id;
    }
    else {
      next_sibling.next_sibling_id_ = prev_sibling.next_sibling_id_;
      prev_sibling.next_sibling_id_ = next_sibling_id;
    }
  }

  template<typename T>
  bool stack_tree<T>::is_valid(index_t const id) const
  {
    std::unique_lock lk{mtx_};

    if ((id < 0) || (static_cast<index_t>(nodes_.size()) <= id)) {
      return false;
    }

    return nodes_[id].payload_.has_value();
  }

  template<typename T>
  index_t stack_tree<T>::root_id() const
  {
    return root_id_;
  }

  template<typename T>
  index_t stack_tree<T>::get_parent(index_t const id) const
  {
    std::unique_lock lk{mtx_};

    ASSERT(is_valid(id));

    return nodes_[id].parent_id_;
  }

  template<typename T>
  index_t stack_tree<T>::get_first_child(index_t const id) const
  {
    std::unique_lock lk{mtx_};

    ASSERT(is_valid(id));

    return nodes_[id].first_child_id_;
  }

  template<typename T>
  index_t stack_tree<T>::get_next_sibling(index_t const id) const
  {
    std::unique_lock lk{mtx_};

    ASSERT(is_valid(id));

    return nodes_[id].next_sibling_id_;
  }

  template<typename T>
  index_t stack_tree<T>::get_successor(index_t const id) const
  {
    std::unique_lock lk{mtx_};

    ASSERT(is_valid(id));

    auto const& nn = nodes_[id];

    if (nn.first_child_id_ != stack_tree_invalid_id) {
      return nn.first_child_id_;
    }
    else if (nn.next_sibling_id_ != stack_tree_invalid_id) {
      return nn.next_sibling_id_;
    }
    else {
      index_t cur_id = nn.parent_id_;

      while (cur_id != stack_tree_invalid_id) {
        auto const& sibling_id = nodes_[cur_id].next_sibling_id_;

        if (sibling_id != stack_tree_invalid_id) {
          return sibling_id;
        }

        cur_id = nodes_[cur_id].parent_id_;
      }

      return stack_tree_invalid_id;
    }
  }

  template<typename T>
  index_t stack_tree<T>::get_depth(index_t id) const
  {
    std::unique_lock lk{mtx_};

    index_t retval = 0;

    while (true) {
      id = get_parent(id);

      if (id == stack_tree_invalid_id) {
        break;
      }

      ++retval;
    }

    return retval;
  }

  template<typename T>
  index_t stack_tree<T>::find_child(index_t const id, T const& value) const
  {
    std::unique_lock lk{mtx_};

    ASSERT(is_valid(id));

    index_t cur_id = nodes_[id].first_child_id_;

    while (cur_id != stack_tree_invalid_id) {
      node const& nn = nodes_[cur_id];

      ASSERT(nn.payload_.has_value());
      if (nn.payload_.value() == value) {
        return cur_id;
      }

      cur_id = nn.next_sibling_id_;
    }

    return stack_tree_invalid_id;
  }

  template<typename T>
  T& stack_tree<T>::get(index_t const id)
  {
    std::unique_lock lk{mtx_};

    ASSERT(is_valid(id));

    return nodes_[id].payload_.value();
  }

  template<typename T>
  T const& stack_tree<T>::get(index_t const id) const
  {
    std::unique_lock lk{mtx_};

    ASSERT(is_valid(id));

    return nodes_[id].payload_.value();
  }

  template<typename T>
  void stack_tree<T>::erase_all_children(index_t const id)
  {
    std::unique_lock lk{mtx_};

    ASSERT(is_valid(id));

    auto& nn = nodes_[id];

    if (nn.first_child_id_ == stack_tree_invalid_id) {
      return;
    }

    index_t cur_id     = nn.first_child_id_;
    nn.first_child_id_ = stack_tree_invalid_id;

    while (cur_id != stack_tree_invalid_id) {
      erase_all_children(cur_id);

      auto& cc = nodes_[cur_id];

      cc.payload_.reset();
      cc.parent_id_ = stack_tree_invalid_id;

      cur_id              = cc.next_sibling_id_;
      cc.next_sibling_id_ = stack_tree_invalid_id;
    }
  }

  template<typename T>
  template<std::Invocable<T&, index_t> F>
  void stack_tree<T>::for_each_child(index_t const parent_id, F&& f)
  {
    std::unique_lock lk{mtx_};

    ASSERT(is_valid(parent_id));

    index_t cur_id = nodes_[parent_id].first_child_id_;

    while (cur_id != stack_tree_invalid_id) {
      auto& nn = nodes_[cur_id];

      T& val = nn.payload_.value();
      f(val, cur_id);

      cur_id = nn.next_sibling_id_;
    }
  }

  template<typename T>
  template<std::Invocable<T const&, index_t, index_t> F>
  void stack_tree<T>::for_each(F&& f) const
  {
    std::unique_lock lk{mtx_};

    for (index_t id = root_id(); id != stack_tree_invalid_id;
         id         = get_successor(id)) {
      f(get(id), id, get_depth(id));
    }
  }

  template<typename T>
  void stack_tree<T>::ensure_id(index_t const id)
  {
    while (id >= static_cast<index_t>(nodes_.size())) {
      nodes_.emplace_back();
    }
  }

  template<typename T>
  void to_pretty(std::ostream& os, stack_tree<T> const& st)
  {
    st.for_each([&os](T const& node, index_t const id, index_t const depth) {
      os << std::string(2 * depth, ' ') << '[' << id << "] " << node << '\n';
    });
  }
}
