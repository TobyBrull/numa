{
  nixpkgs ? import <nixpkgs> {}
}:
rec {
  numa = nixpkgs.gcc10Stdenv.mkDerivation {
    name = "numa";
    src = ./numa.tar.gz;
    buildInputs = [
      nixpkgs.cmake
      nixpkgs.catch2
      nixpkgs.fmt
      ];
    doCheck = true;
    cmakeFlags = [
      "-DCMAKE_SKIP_BUILD_RPATH=OFF"
    ];
  };

  numa-container = nixpkgs.dockerTools.buildImage {
    name = "numa-container";
    tag = nixpkgs.lib.fileContents ./VERSION;
    contents = [ numa nixpkgs.bash ];
    config = {
      WorkingDir = "/";
    };
  };
}
