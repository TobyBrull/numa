#include "numa/boot/index.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE(
    "index constructor and operator", "[index][ctor][operators][constexpr]")
{
  static_assert(sizeof(index<1, int32_t>) == 1 * 4);
  static_assert(sizeof(index<1, int16_t>) == 1 * 2);
  static_assert(sizeof(index<4, int16_t>) == 4 * 2);
  static_assert(sizeof(index<3, int64_t>) == 3 * 8);

  static_assert(index<0>(zero) == index<0>());
  static_assert(index<0>(iota) == index{});
  static_assert(index<0>(one) == index<0>());
  static_assert(index<0>(unit, 0) == index<0>());
  static_assert(index<0>(one).size() == 0);

  static_assert(index<1>(zero) == index<1>(0));
  static_assert(index<1>(iota) == index<1>(0));
  static_assert(index<1>(one) == index<1>(1));
  static_assert(index<1>(unit, 0) == index<1>(1));
  static_assert(index<1>(unit, 2) == index<1>(0));
  static_assert(index<1>(one).size() == 1);

  static_assert(index<1>(one) == 1);

  index_t const i = index<1>(3);
  CHECK(i == 3);

  static_assert(index<2>(zero) == index<2>(0, 0));
  static_assert(index<2>(iota) == index<2>(0, 1));
  static_assert(index<2>(one) == index<2>(1, 1));
  static_assert(index<2>(unit, 0) == index<2>(1, 0));
  static_assert(index<2>(unit, 1) == index<2>(0, 1));
  static_assert(index<2>(unit, 2) == index<2>(0, 0));
  static_assert(index<2>(one).size() == 2);

  static_assert(index<3>(zero) == index<3>(0, 0, 0));
  static_assert(index<3>(iota) == index<3>(0, 1, 2));
  static_assert(index<3>(one) == index<3>(1, 1, 1));
  static_assert(index<3>(unit, 0) == index<3>(1, 0, 0));
  static_assert(index<3>(unit, 1) == index<3>(0, 1, 0));
  static_assert(index<3>(unit, 2) == index<3>(0, 0, 1));
  static_assert(index<4>(one).size() == 4);

  static_assert(index<0>() + index<0>() == index<0>());
  static_assert(index<0>() - index<0>() == index<0>());
  static_assert((7 * index<0>()) == index<0>());
  static_assert((index<0>() * 4) == index<0>());
  static_assert((-index<0>()) == index<0>());

  static_assert(index<1>(-1) + index<1>(3) == index<1>(2));
  static_assert(index<1>(-1) - index<1>(3) == index<1>(-4));
  static_assert(7 * index<1>(4) == index<1>(28));
  static_assert((-index<1>(4)) == index(-4));

  static_assert(index<2>(3, -2) + index<2>(-1, 7) == index<2>(2, 5));
  static_assert(index<2>(3, -2) - index<2>(-1, 7) == index<2>(4, -9));
  static_assert((2 * index<2>(4, 8)) == index<2>(8, 16));
  static_assert((index<2>(4, 8) * 2) == index<2>(8, 16));
  static_assert((-index<2>(4, 8)) == index(-4, -8));

  static_assert(
      -3 * index<3>(one) + 2 * index<3>(unit, 1) == index<3>(-3, -1, -3));

  static_assert(is_all_equal());
  static_assert(is_all_equal(index(3, 2), index(3, 2)));
  static_assert(is_all_equal(index(3, 2), index(3, 2), index(3, 2)));
  static_assert(!is_all_equal(index(3, 2), index(2, 3), index(3, 2)));
}

TEST_CASE("no-init index constructor", "[index][ctor][uninitialized][!mayfail]")
{
  index<3> ind(uninitialized);

  ind[0] = 10;
  ind[1] = 5;
  ind[2] = 2;

  CHECK(ind == index(10, 5, 2));
}

TEST_CASE("index compound assignment operators", "[index][operators]")
{
  {
    index<0> ind(zero);

    ind += index<0>();
    CHECK(ind == index<0>());
    ind -= index<0>();
    CHECK(ind == index<0>());
    ind *= 10;
    CHECK(ind == index<0>());
    ind /= 2;
    CHECK(ind == index<0>());
  }

  {
    index ind(3);

    ind += index(4);
    CHECK(ind == index(7));
    ind -= index(2);
    CHECK(ind == index(5));
    ind *= 10;
    CHECK(ind == index(50));
    ind /= 2;
    CHECK(ind == index(25));
  }

  {
    index ind(3, 2, 5);

    ind += index(4, 3, 2);
    CHECK(ind == index(7, 5, 7));
    ind -= index(2, 1, 4);
    CHECK(ind == index(5, 4, 3));
    ind *= 10;
    CHECK(ind == index(50, 40, 30));
    ind /= 2;
    CHECK(ind == index(25, 20, 15));
  }
}

TEST_CASE(
    "test the comparison operators",
    "[index][operators][is_bounded][is_before]"
    "[comparison]")
{
  static_assert(is_before(index<0>(zero), index<0>()));
  static_assert(!is_before_strictly(index<0>(zero), index<0>()));

  static_assert(is_before(index<1>(iota), index(0)));
  static_assert(is_before(index(-3), index(2)));
  static_assert(!is_before(index(-3), index(-4)));
  static_assert(!is_before_strictly(index<1>(zero), index(0)));
  static_assert(is_before_strictly(index(-3), index(2)));
  static_assert(!is_before_strictly(index(-3), index(-3)));

  static_assert(is_before(index(7, 2), index(7, 6)));
  static_assert(is_before(index(7, 6), index(7, 6)));
  static_assert(is_before(index(9, 5), index(7, 6)));
  static_assert(!is_before(index(8, 6), index(7, 6)));
  static_assert(!is_before(index(7, 7), index(7, 6)));
  static_assert(is_before_strictly(index<2>(iota), index(4, 2)));
  static_assert(is_before_strictly(index(7, 2), index(7, 6)));
  static_assert(!is_before_strictly(index(6, 2), index(6, 2)));
  static_assert(!is_before_strictly(index(7, 2), index(6, 2)));
  static_assert(!is_before_strictly(index(6, 3), index(6, 2)));

  constexpr index<4> iii(iota);

  static_assert(is_before(iii, index(4, 2, 3, 10)));
  static_assert(is_before(iii, index(-4, 0, -1, 4)));
  static_assert(is_before(iii, index(-6, -7, 3, 3)));
  static_assert(is_before(iii, index(1, 1, 2, 3)));
  static_assert(is_before(iii, index(0, 2, 2, 3)));
  static_assert(is_before(iii, index(0, 1, 2, 4)));
  static_assert(is_before(iii, index(0, 1, 2, 3)));
  static_assert(!is_before(iii, index(-1, 1, 2, 3)));
  static_assert(!is_before(iii, index(0, 1, 2, 2)));
  static_assert(!is_before(iii, index(7, 0, 2, 3)));
  static_assert(!is_before(iii, index(20, 20, 20, 2)));
  static_assert(is_before_strictly(iii, index(4, 2, 3, 10)));
  static_assert(is_before_strictly(iii, index(-4, 0, -1, 4)));
  static_assert(is_before_strictly(iii, index(-6, -7, 3, 3)));
  static_assert(is_before_strictly(iii, index(1, 1, 2, 3)));
  static_assert(is_before_strictly(iii, index(0, 2, 2, 3)));
  static_assert(is_before_strictly(iii, index(0, 1, 2, 4)));
  static_assert(!is_before_strictly(iii, index(0, 1, 2, 3)));
  static_assert(!is_before_strictly(iii, index(-1, 1, 2, 3)));
  static_assert(!is_before_strictly(iii, index(0, 1, 2, 2)));
  static_assert(!is_before_strictly(iii, index(7, 0, 2, 3)));
  static_assert(!is_before_strictly(iii, index(20, 20, 20, 2)));

  static_assert(is_bounded(index<0>(), index<0>()));
  static_assert(!is_bounded_strictly(index<0>(), index<0>())); // ??

  static_assert(is_bounded(index(3), index(4)));
  static_assert(is_bounded(index(3), index(3)));
  static_assert(!is_bounded(index(3), index(2)));
  static_assert(is_bounded_strictly(index(3), index(6)));
  static_assert(!is_bounded_strictly(index(6), index(6)));
  static_assert(!is_bounded_strictly(index(6), index(4)));

  static_assert(is_bounded(iii, index(4, 2, 3, 10)));
  static_assert(!is_bounded(iii, index(-4, 0, -1, 4)));
  static_assert(!is_bounded(iii, index(-6, -7, 3, 3)));
  static_assert(is_bounded(iii, index(1, 1, 2, 3)));
  static_assert(is_bounded(iii, index(0, 2, 2, 3)));
  static_assert(is_bounded(iii, index(0, 1, 2, 4)));
  static_assert(is_bounded(iii, index(0, 1, 2, 3)));
  static_assert(!is_bounded(iii, index(-1, 1, 2, 3)));
  static_assert(!is_bounded(iii, index(0, 1, 2, 2)));
  static_assert(!is_bounded(iii, index(7, 0, 2, 3)));
  static_assert(!is_bounded(iii, index(20, 20, 20, 2)));
  static_assert(is_bounded_strictly(iii, index(4, 2, 3, 10)));
  static_assert(!is_bounded_strictly(iii, index(-4, 0, -1, 4)));
  static_assert(!is_bounded_strictly(iii, index(-6, -7, 3, 3)));
  static_assert(is_bounded_strictly(iii, index(1, 2, 3, 4)));
  static_assert(!is_bounded_strictly(iii, index(0, 2, 3, 4)));
  static_assert(!is_bounded_strictly(iii, index(1, 1, 3, 4)));
  static_assert(!is_bounded_strictly(iii, index(1, 2, 2, 4)));
  static_assert(!is_bounded_strictly(iii, index(1, 2, 3, 3)));
  static_assert(!is_bounded_strictly(iii, index(0, 1, 2, 3)));
  static_assert(!is_bounded_strictly(iii, index(1, 1, 2, 3)));
  static_assert(!is_bounded_strictly(iii, index(0, 2, 2, 3)));
  static_assert(!is_bounded_strictly(iii, index(0, 1, 2, 4)));
  static_assert(!is_bounded_strictly(iii, index(0, 1, 2, 3)));
  static_assert(!is_bounded_strictly(iii, index(-1, 1, 2, 3)));
  static_assert(!is_bounded_strictly(iii, index(0, 1, 2, 2)));
  static_assert(!is_bounded_strictly(iii, index(7, 0, 2, 3)));
  static_assert(!is_bounded_strictly(iii, index(20, 20, 20, 2)));
}

TEST_CASE("more binary operations on index", "[index][operators][constexpr]")
{
  static_assert(dot(index<0>(), index<0>()) == 0);
  static_assert(min(index<0>(), index<0>()) == index<0>());
  static_assert(max(index<0>(), index<0>()) == index<0>());

  static_assert(dot(index<1>(-2), index<1>(3)) == -6);
  static_assert(min(index<1>(-2), index<1>(3)) == index<1>(-2));
  static_assert(max(index<1>(-2), index<1>(3)) == index<1>(3));

  static_assert(dot(index<3>(1, -4, 2), index<3>(-3, 2, 1)) == -9);
  static_assert(
      min(index<3>(1, -4, 2), index<3>(-3, 2, 1)) == index<3>(-3, -4, 1));
  static_assert(
      max(index<3>(1, -4, 2), index<3>(-3, 2, 1)) == index<3>(1, 2, 2));
}

TEST_CASE("more unary operations on index", "[index][operators][constexpr]")
{
  static_assert(abs(index<0>()) == index<0>());

  static_assert(abs(index<1>(-2)) == index<1>(2));
  static_assert(abs(index<1>(0)) == index<1>(0));
  static_assert(abs(index<1>(2)) == index<1>(2));

  static_assert(abs(index<3>(1, -4, 2)) == index<3>(1, 4, 2));
  static_assert(abs(index<3>(-1, 0, -2)) == index<3>(1, 0, 2));

  static_assert(min(index(-2)) == -2);
  static_assert(min(index(4, -1, 2)) == -1);

  static_assert(argmin(index(-2)) == 0);
  static_assert(argmin(index(4, -1, 2)) == 1);

  static_assert(max(index(-2)) == -2);
  static_assert(max(index(4, -1, 2)) == 4);

  static_assert(argmax(index(-2)) == 0);
  static_assert(argmax(index(4, -1, 2)) == 0);
}

TEST_CASE(
    "index product/empty/blank", "[index][product][empty][blank][constexpr]")
{
  static_assert(product(index<0>()) == 1);

  static_assert(product(index<1>(3)) == 3);
  static_assert(product(index<1>(0)) == 0);

  static_assert(product(index<2>(3, 2)) == 6);
  static_assert(product(index<2>(0, 1)) == 0);

  static_assert(product(index<5>(3, 2, 4, 2, 1)) == 48);
  static_assert(product(index<5>(3, 2, 1, 0, 7)) == 0);

  static_assert(is_empty(index<0>()) == false);

  static_assert(is_empty(index<1>(3)) == false);
  static_assert(is_empty(index<1>(0)) == true);

  static_assert(is_empty(index<3>(3, 2, 7)) == false);
  static_assert(is_empty(index<3>(3, 0, 7)) == true);
  static_assert(is_empty(index<3>(0, 0, 0)) == true);

  static_assert(index<0>() == zero);

  static_assert(index(2) != zero);
  static_assert(index(0) == zero);

  static_assert(index(3, 2, 7) != zero);
  static_assert(index(3, 0, 7) != zero);
  static_assert(index(0, 0, 0) == zero);
}

TEST_CASE("index access", "[index][access][constexpr]")
{
  static_assert(index<3>(7, 3, 1)[1] == 3);

  constexpr index<3> ind(3, 1, 4);
  static_assert(ind[2] == 4);

  static_assert(ind.begin() + 3 == ind.end());
}

TEST_CASE("index splice", "[index][splice][constexpr]")
{
  static_assert(splice(index<0>(), index<0>(), 0) == index<0>());
  static_assert(splice(index<1>(5), index<1>(2), 0) == index<1>(2));
  static_assert(splice(index<1>(5), index<1>(2), 1) == index<1>(5));

  static_assert(
      splice(index<5>(8, 3, 7, 1, 4), index<5>(1, 7, 2, 9, 6), 0) ==
      index<5>(1, 7, 2, 9, 6));
  static_assert(
      splice(index<5>(8, 3, 7, 1, 4), index<5>(1, 7, 2, 9, 6), 1) ==
      index<5>(8, 7, 2, 9, 6));
  static_assert(
      splice(index<5>(8, 3, 7, 1, 4), index<5>(1, 7, 2, 9, 6), 2) ==
      index<5>(8, 3, 2, 9, 6));
  static_assert(
      splice(index<5>(8, 3, 7, 1, 4), index<5>(1, 7, 2, 9, 6), 3) ==
      index<5>(8, 3, 7, 9, 6));
  static_assert(
      splice(index<5>(8, 3, 7, 1, 4), index<5>(1, 7, 2, 9, 6), 4) ==
      index<5>(8, 3, 7, 1, 6));
  static_assert(
      splice(index<5>(8, 3, 7, 1, 4), index<5>(1, 7, 2, 9, 6), 5) ==
      index<5>(8, 3, 7, 1, 4));
}

TEST_CASE("index split and join", "[index][split][join][constexpr]")
{
  static_assert(split<0>(index<0>()) == std::make_pair(index<0>(), index<0>()));

  static_assert(split<1>(index(3)) == std::make_pair(index(3), index<0>()));
  static_assert(split<0>(index(3)) == std::make_pair(index<0>(), index(3)));

  static_assert(
      split<0>(index(3, 8, 1, 2)) ==
      std::make_pair(index<0>(), index(3, 8, 1, 2)));
  static_assert(
      split<1>(index(3, 8, 1, 2)) == std::make_pair(index(3), index(8, 1, 2)));
  static_assert(
      split<2>(index(3, 8, 1, 2)) == std::make_pair(index(3, 8), index(1, 2)));
  static_assert(
      split<3>(index(3, 8, 1, 2)) == std::make_pair(index(3, 8, 1), index(2)));
  static_assert(
      split<4>(index(3, 8, 1, 2)) ==
      std::make_pair(index(3, 8, 1, 2), index<0>()));

  static_assert(join(index<0>(), index<0>()) == index<0>());

  static_assert(join(index(3), index<0>()) == index(3));
  static_assert(join(index<0>(), index(3)) == index(3));

  static_assert(join(index<0>(), index(3, 8, 1, 2)) == index(3, 8, 1, 2));
  static_assert(join(index(3), index(8, 1, 2)) == index(3, 8, 1, 2));
  static_assert(join(index(3, 8), index(1, 2)) == index(3, 8, 1, 2));
  static_assert(join(index(3, 8, 1), index(2)) == index(3, 8, 1, 2));
  static_assert(join(index<0>(), index(3, 8, 1, 2)) == index(3, 8, 1, 2));
}

TEST_CASE("index assignment", "[index][assign]")
{
  {
    index<3> ind = {3, 1, 2};

    ind = zero;

    CHECK(ind == index<3>(0, 0, 0));

    ind[1] = 333;

    CHECK(ind == index<3>(0, 333, 0));

    ind = iota;

    CHECK(ind == index<3>(0, 1, 2));
  }

  {
    index<0> ind;

    CHECK(ind == index<0>());
  }
}

TEST_CASE("index deduction guides", "[index][deduction_guide]")
{
  static_assert(index(2, 3) == index<2>(2, 3));
  static_assert(index(0, 1) == index<2>(iota));

  static_assert(dot(index(3, 2), index(2, 4)) == 14);
}
