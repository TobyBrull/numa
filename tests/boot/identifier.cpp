#include "numa/boot/identifier.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("identifier", "[identifier]")
{
  CHECK(is_identifier(""));

  CHECK(is_identifier("chicken"));
  CHECK(is_identifier("CHICKEN"));
  CHECK(is_identifier("ChiCKeN"));
  CHECK(is_identifier("Chi3CK8eN9"));
  CHECK(is_identifier("Chi3_CK8_eN9"));

  CHECK(!is_identifier("0chicken"));

  CHECK(is_identifier("_chicken"));
  CHECK(is_identifier("_0chicken"));
  CHECK(is_identifier("_0chicken123"));

  CHECK(is_identifier("_0c_hicken123"));
  CHECK(!is_identifier("_0c hicken123"));
  CHECK(!is_identifier("_0c|hicken123"));
  CHECK(!is_identifier("_0c?hicken123"));

  {
    identifier id;
    CHECK(id == identifier{""});
  }

  {
    identifier id("Hello");
    CHECK(id < identifier{"World"});
  }

  {
    identifier id("Hello");
    CHECK(id >= "Hell");
  }

  CHECK_THROWS(identifier("0chicken"));
}
