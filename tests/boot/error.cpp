#include "numa/boot/error.hpp"

#include "numa/boot/bits.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("dump and dump_var", "[dump][dump_var]")
{
  int const x         = 42;
  std::string const s = "Hello, World!";

  DUMP("Message: ", s);
  DUMP_VAR(x, s);
}

TEST_CASE("dump_type", "[dump_type]")
{
  static_assert(!is_defined<dump_type<int>>::value);
}
