#include "numa/boot/stack_tree.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("stack_tree", "[stack_tree]")
{
  stack_tree<char> st('Z');

  CHECK(st.insert_first_child(st.root_id(), 'b') == 1);
  CHECK(st.insert_next_sibling(1, 'c') == 2);
  CHECK(st.insert_first_child(st.root_id(), 'a') == 3);

  {
    index_t it = st.root_id();

    CHECK(st.get(it) == 'Z');

    it = st.get_successor(it);
    REQUIRE(it == 3);
    CHECK(st.get(it) == 'a');

    it = st.get_successor(it);
    REQUIRE(it == 1);
    CHECK(st.get(it) == 'b');

    it = st.get_successor(it);
    REQUIRE(it == 2);
    CHECK(st.get(it) == 'c');

    it = st.get_successor(it);
    CHECK(it == stack_tree_invalid_id);
  }

  CHECK(st.insert_first_child(2, 'h') == 4);
  CHECK(st.insert_first_child(2, 'g') == 5);
  CHECK(st.insert_first_child(2, 'f') == 6);
  CHECK(st.insert_next_sibling(4, 'i') == 7);

  CHECK(st.insert_first_child(1, 'e') == 8);
  CHECK(st.insert_first_child(1, 'd') == 9);

  {
    index_t it = st.root_id();

    CHECK(st.get(it) == 'Z');

    it = st.get_successor(it);
    REQUIRE(it == 3);
    CHECK(st.get(it) == 'a');

    it = st.get_successor(it);
    REQUIRE(it == 1);
    CHECK(st.get(it) == 'b');

    it = st.get_successor(it);
    REQUIRE(it == 9);
    CHECK(st.get(it) == 'd');

    it = st.get_successor(it);
    REQUIRE(it == 8);
    CHECK(st.get(it) == 'e');

    it = st.get_successor(it);
    REQUIRE(it == 2);
    CHECK(st.get(it) == 'c');

    it = st.get_successor(it);
    REQUIRE(it == 6);
    CHECK(st.get(it) == 'f');

    it = st.get_successor(it);
    REQUIRE(it == 5);
    CHECK(st.get(it) == 'g');

    it = st.get_successor(it);
    REQUIRE(it == 4);
    CHECK(st.get(it) == 'h');

    it = st.get_successor(it);
    REQUIRE(it == 7);
    CHECK(st.get(it) == 'i');

    it = st.get_successor(it);
    CHECK(it == stack_tree_invalid_id);
  }

  st.erase_all_children(2);
  st.erase_all_children(3);

  {
    index_t it = st.root_id();

    CHECK(st.get(it) == 'Z');

    it = st.get_successor(it);
    REQUIRE(it == 3);
    CHECK(st.get(it) == 'a');

    it = st.get_successor(it);
    REQUIRE(it == 1);
    CHECK(st.get(it) == 'b');

    it = st.get_successor(it);
    REQUIRE(it == 9);
    CHECK(st.get(it) == 'd');

    it = st.get_successor(it);
    REQUIRE(it == 8);
    CHECK(st.get(it) == 'e');

    it = st.get_successor(it);
    REQUIRE(it == 2);
    CHECK(st.get(it) == 'c');

    it = st.get_successor(it);
    CHECK(it == stack_tree_invalid_id);
  }

  CHECK(st.is_valid(st.root_id()));
  CHECK(st.is_valid(1));
  CHECK(st.is_valid(2));
  CHECK(st.is_valid(3));
  CHECK(!st.is_valid(4));
  CHECK(!st.is_valid(5));
  CHECK(!st.is_valid(6));
  CHECK(!st.is_valid(7));
  CHECK(st.is_valid(8));
  CHECK(st.is_valid(9));
  CHECK(!st.is_valid(10));
  CHECK(!st.is_valid(11));

  CHECK(st.get_parent(st.root_id()) == stack_tree_invalid_id);
  CHECK(st.get_parent(3) == st.root_id());
  CHECK(st.get_parent(1) == st.root_id());
  CHECK(st.get_parent(9) == 1);
  CHECK(st.get_parent(8) == 1);
  CHECK(st.get_parent(2) == st.root_id());

  CHECK(st.get_first_child(st.root_id()) == 3);
  CHECK(st.get_first_child(3) == stack_tree_invalid_id);
  CHECK(st.get_first_child(1) == 9);
  CHECK(st.get_first_child(9) == stack_tree_invalid_id);
  CHECK(st.get_first_child(8) == stack_tree_invalid_id);
  CHECK(st.get_first_child(2) == stack_tree_invalid_id);

  CHECK(st.get_next_sibling(st.root_id()) == stack_tree_invalid_id);
  CHECK(st.get_next_sibling(3) == 1);
  CHECK(st.get_next_sibling(1) == 2);
  CHECK(st.get_next_sibling(9) == 8);
  CHECK(st.get_next_sibling(8) == stack_tree_invalid_id);
  CHECK(st.get_next_sibling(2) == stack_tree_invalid_id);

  CHECK(st.find_child(st.root_id(), 'a') == 3);
  CHECK(st.find_child(st.root_id(), 'b') == 1);
  CHECK(st.find_child(st.root_id(), 'c') == 2);
  CHECK(st.find_child(st.root_id(), 'd') == stack_tree_invalid_id);
  CHECK(st.find_child(st.root_id(), 'Z') == stack_tree_invalid_id);

  CHECK(st.find_child(3, 'a') == stack_tree_invalid_id);
  CHECK(st.find_child(3, 'd') == stack_tree_invalid_id);
  CHECK(st.find_child(3, 'u') == stack_tree_invalid_id);

  CHECK(st.find_child(1, 'a') == stack_tree_invalid_id);
  CHECK(st.find_child(1, 'd') == 9);
  CHECK(st.find_child(1, 'e') == 8);
  CHECK(st.find_child(1, 'u') == stack_tree_invalid_id);

  {
    std::ostringstream oss;
    to_pretty(oss, st);

    CHECK(
        oss.str() ==
        "[0] Z\n"
        "  [3] a\n"
        "  [1] b\n"
        "    [9] d\n"
        "    [8] e\n"
        "  [2] c\n");
  }
}

TEST_CASE("stack_tree 2-by-2", "[stack_tree]")
{
  stack_tree<index_t> st(0);

  st.insert_first_child(st.root_id(), 2);
  st.insert_first_child(st.root_id(), 0);

  st.insert_first_child(2, 1);
  st.insert_first_child(2, 0);

  st.insert_first_child(1, 3);
  st.insert_first_child(1, 2);

  {
    std::ostringstream oss;
    to_pretty(oss, st);

    CHECK(
        oss.str() ==
        "[0] 0\n"
        "  [2] 0\n"
        "    [4] 0\n"
        "    [3] 1\n"
        "  [1] 2\n"
        "    [6] 2\n"
        "    [5] 3\n");
  }
}

TEST_CASE("mirror stack_tree 2-by-2", "[stack_tree][explicit]")
{
  stack_tree<char> st(stack_tree_explicit, 3, 'a');

  st.insert_first_child(stack_tree_explicit, 3, 5, 'c');
  st.insert_first_child(stack_tree_explicit, 3, 0, 'a');

  st.insert_first_child(stack_tree_explicit, 0, 6, 'b');
  st.insert_first_child(stack_tree_explicit, 0, 7, 'a');

  st.insert_first_child(stack_tree_explicit, 5, 1, 'd');
  st.insert_first_child(stack_tree_explicit, 5, 2, 'c');

  {
    std::ostringstream oss;
    to_pretty(oss, st);

    CHECK(
        oss.str() ==
        "[3] a\n"
        "  [0] a\n"
        "    [7] a\n"
        "    [6] b\n"
        "  [5] c\n"
        "    [2] c\n"
        "    [1] d\n");
  }

  st.insert_next_sibling(stack_tree_explicit, 2, 10, 'X');

  {
    std::ostringstream oss;
    to_pretty(oss, st);

    CHECK(
        oss.str() ==
        "[3] a\n"
        "  [0] a\n"
        "    [7] a\n"
        "    [6] b\n"
        "  [5] c\n"
        "    [2] c\n"
        "    [10] X\n"
        "    [1] d\n");
  }

  st.insert_next_sibling(stack_tree_explicit, 0, 9, 'Y');

  {
    std::ostringstream oss;
    to_pretty(oss, st);

    CHECK(
        oss.str() ==
        "[3] a\n"
        "  [0] a\n"
        "    [7] a\n"
        "    [6] b\n"
        "  [9] Y\n"
        "  [5] c\n"
        "    [2] c\n"
        "    [10] X\n"
        "    [1] d\n");
  }
}
