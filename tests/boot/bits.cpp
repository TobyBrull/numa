#include "numa/boot/bits.hpp"

#include "catch2/catch.hpp"

#include <sstream>

using namespace numa;

class class_undefined;
class class_defined {
};

TEST_CASE("overload", "[overload]")
{
  auto const f = overload(
      [&](int const i) { return std::to_string(i); },
      [&](std::string const& s) { return s; });

  CHECK(f(3) == "3");
  CHECK(f("Hello") == "Hello");
}

TEST_CASE("for_each_argument", "[for_each_argument]")
{
  std::ostringstream oss;

  for_each_argument(
      [&oss](auto const& x) { oss << x << ", "; }, 2, "Hello", 42);

  CHECK(oss.str() == "2, Hello, 42, ");
}

TEST_CASE("for_each_argument_indexed", "[for_each_argument_indexed]")
{
  std::ostringstream oss;

  for_each_argument_indexed(
      [&oss](index_t const i, auto const& x) { oss << i << ": " << x << ", "; },
      2,
      "Hello",
      42);

  CHECK(oss.str() == "0: 2, 1: Hello, 2: 42, ");
}

TEST_CASE("is_defined", "[is_defined]")
{
  static_assert(!is_defined<class_undefined>::value);
  static_assert(is_defined<class_defined>::value);
}

TEST_CASE("class-wrapper", "[class_wrapper]")
{
  {
    class_wrapper<std::string> str = "aaa";

    str.push_back('b');

    CHECK(str == "aaab");
    CHECK(hash(str) == hash(std::string("aaab")));
  }

  {
    class_wrapper<char> c = 'a';

    CHECK(c == 'a');
    CHECK(hash(c) == hash('a'));

    c = 'b';

    CHECK(c.value() == 'b');
    CHECK(hash(c) == hash('b'));

    c.value() = 'c';

    CHECK(c == 'c');
    CHECK(hash(c) == hash('c'));
  }
}

TEST_CASE("lcm", "[lcm]")
{
  static_assert(lcm({5}) == 5);
  static_assert(lcm({6, 9}) == 18);
  static_assert(lcm({5, 7, 14}) == 70);
  static_assert(lcm({2, 4, 8, 4}) == 8);
}
