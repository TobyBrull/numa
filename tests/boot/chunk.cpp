#include "numa/boot/chunk.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("chunk_required", "[chunk_required]")
{
  static_assert(chunk_required(0, 7) == 0);
  static_assert(chunk_required(1, 7) == 1);
  static_assert(chunk_required(2, 7) == 1);
  static_assert(chunk_required(3, 7) == 1);
  static_assert(chunk_required(4, 7) == 1);
  static_assert(chunk_required(5, 7) == 1);
  static_assert(chunk_required(6, 7) == 1);
  static_assert(chunk_required(7, 7) == 1);
  static_assert(chunk_required(8, 7) == 2);
  static_assert(chunk_required(9, 7) == 2);
  static_assert(chunk_required(10, 7) == 2);
  static_assert(chunk_required(11, 7) == 2);
  static_assert(chunk_required(12, 7) == 2);
  static_assert(chunk_required(13, 7) == 2);
  static_assert(chunk_required(14, 7) == 2);
  static_assert(chunk_required(15, 7) == 3);
}

namespace {
  template<typename I, typename J>
  struct collector {
    void operator()(I const i, J const j)
    {
      data_.emplace_back(i, j);
    }

    void clear()
    {
      data_.clear();
    }

    std::vector<std::pair<I, J>> data_;
  };
}

TEST_CASE("chunk_equal", "[chunk_equal]")
{
  collector<int, int> cc;

  using vec_t = std::vector<std::pair<int, int>>;

  chunk_equal(0, 1, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 0}});
  cc.clear();

  chunk_equal(1, 1, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 1}});
  cc.clear();

  chunk_equal(2, 1, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 2}});
  cc.clear();

  chunk_equal(7, 1, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 7}});
  cc.clear();

  chunk_equal(0, 3, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 0}, {0, 0}, {0, 0}});
  cc.clear();

  chunk_equal(1, 3, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 1}, {1, 1}, {1, 1}});
  cc.clear();

  chunk_equal(2, 3, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 2}});
  cc.clear();

  chunk_equal(3, 3, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 3}});
  cc.clear();

  chunk_equal(4, 3, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 2}, {2, 3}, {3, 4}});
  cc.clear();

  chunk_equal(5, 3, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 2}, {2, 4}, {4, 5}});
  cc.clear();

  chunk_equal(6, 3, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 2}, {2, 4}, {4, 6}});
  cc.clear();

  chunk_equal(7, 3, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 3}, {3, 5}, {5, 7}});
  cc.clear();

  chunk_equal(8, 3, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 3}, {3, 6}, {6, 8}});
  cc.clear();

  chunk_equal(9, 3, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 3}, {3, 6}, {6, 9}});
  cc.clear();

  chunk_equal(10, 3, as_till, cc);
  CHECK(cc.data_ == vec_t{{0, 4}, {4, 7}, {7, 10}});
  cc.clear();

  chunk_equal(10, 3, as_size, cc);
  CHECK(cc.data_ == vec_t{{0, 4}, {4, 3}, {7, 3}});
  cc.clear();
}

TEST_CASE("chunk_nth", "[chunk_nth]")
{
  collector<int, int> cc;

  using vec_t = std::vector<std::pair<int, int>>;

  {
    int const n   = 0;
    int const min = 1;

    chunk_nth(0, 1, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 0}});
    cc.clear();

    chunk_nth(1, 1, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}});
    cc.clear();

    chunk_nth(2, 1, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 2}});
    cc.clear();

    chunk_nth(7, 1, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 7}});
    cc.clear();

    chunk_nth(0, 3, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 0}, {0, 0}});
    cc.clear();

    chunk_nth(1, 3, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 1}, {1, 1}});
    cc.clear();

    chunk_nth(2, 3, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 2}});
    cc.clear();

    chunk_nth(3, 3, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 3}});
    cc.clear();

    chunk_nth(4, 3, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 3}, {3, 4}});
    cc.clear();

    chunk_nth(5, 3, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 3}, {3, 4}, {4, 5}});
    cc.clear();

    chunk_nth(10, 3, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 8}, {8, 9}, {9, 10}});
    cc.clear();
  }

  {
    int const n   = 1;
    int const min = 1;

    chunk_nth(0, 2, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 0}});
    cc.clear();

    chunk_nth(1, 2, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 1}});
    cc.clear();

    chunk_nth(2, 2, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}});
    cc.clear();

    chunk_nth(7, 2, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 7}});
    cc.clear();

    chunk_nth(0, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 0}, {0, 0}, {0, 0}});
    cc.clear();

    chunk_nth(1, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 1}, {1, 1}, {1, 1}});
    cc.clear();

    chunk_nth(2, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 2}, {2, 2}});
    cc.clear();

    chunk_nth(3, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 3}, {3, 3}});
    cc.clear();

    chunk_nth(4, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 3}, {3, 4}});
    cc.clear();

    chunk_nth(5, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 3}, {3, 4}, {4, 5}});
    cc.clear();

    chunk_nth(10, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 8}, {8, 9}, {9, 10}});
    cc.clear();
  }

  {
    int const n   = 1;
    int const min = 0;

    chunk_nth(0, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 0}, {0, 0}, {0, 0}});
    cc.clear();

    chunk_nth(1, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 1}, {1, 1}, {1, 1}});
    cc.clear();

    chunk_nth(2, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 2}, {2, 2}, {2, 2}});
    cc.clear();

    chunk_nth(5, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 5}, {5, 5}, {5, 5}});
    cc.clear();
  }

  {
    int const n   = 3;
    int const min = 1;

    chunk_nth(0, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 0}, {0, 0}, {0, 0}});
    cc.clear();

    chunk_nth(1, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 1}, {1, 1}, {1, 1}});
    cc.clear();

    chunk_nth(2, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 2}, {2, 2}});
    cc.clear();

    chunk_nth(3, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 3}, {3, 3}});
    cc.clear();

    chunk_nth(4, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 3}, {3, 4}});
    cc.clear();

    chunk_nth(5, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 3}, {3, 5}});
    cc.clear();

    chunk_nth(10, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 3}, {3, 10}});
    cc.clear();

    chunk_nth(10, 4, n, min, as_size, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 1}, {2, 1}, {3, 7}});
    cc.clear();
  }

  for (int n = -4; n <= 3; ++n) {
    int const min = 3;

    chunk_nth(0, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 0}, {0, 0}, {0, 0}});
    cc.clear();

    chunk_nth(1, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 1}, {1, 1}, {1, 1}});
    cc.clear();

    chunk_nth(2, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 2}, {2, 2}});
    cc.clear();

    chunk_nth(3, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 3}, {3, 3}});
    cc.clear();

    chunk_nth(4, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 3}, {3, 4}});
    cc.clear();

    chunk_nth(5, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 3}, {3, 4}, {4, 5}});
    cc.clear();

    chunk_nth(6, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 4}, {4, 5}, {5, 6}});
    cc.clear();

    chunk_nth(7, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 4}, {4, 6}, {6, 7}});
    cc.clear();

    chunk_nth(8, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 4}, {4, 6}, {6, 8}});
    cc.clear();

    chunk_nth(10, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 3}, {3, 6}, {6, 8}, {8, 10}});
    cc.clear();

    chunk_nth(12, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 3}, {3, 6}, {6, 9}, {9, 12}});
    cc.clear();
  }

  {
    int const n   = 2;
    int const min = 3;

    chunk_nth(13, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 3}, {3, 6}, {6, 10}, {10, 13}});
    cc.clear();

    chunk_nth(14, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 3}, {3, 6}, {6, 11}, {11, 14}});
    cc.clear();

    chunk_nth(15, 4, n, min, as_size, cc);
    CHECK(cc.data_ == vec_t{{0, 3}, {3, 3}, {6, 6}, {12, 3}});
    cc.clear();
  }

  /*
   * Negative n.
   */
  {
    int const n   = -1;
    int const min = 1;

    chunk_nth(10, 3, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 10}});
    cc.clear();
  }

  {
    int const n   = -2;
    int const min = 1;

    chunk_nth(10, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 9}, {9, 10}});
    cc.clear();
  }

  {
    int const n   = -4;
    int const min = 1;

    chunk_nth(10, 4, n, min, as_till, cc);
    CHECK(cc.data_ == vec_t{{0, 7}, {7, 8}, {8, 9}, {9, 10}});
    cc.clear();
  }
}

TEST_CASE("chunk_approx", "[chunk_approx]")
{
  collector<int, int> cc;

  using vec_t = std::vector<std::pair<int, int>>;

  {
    auto const chunk_approx_init =
        [&cc](int size, std::initializer_list<int> ilist) {
          chunk_approx(size, ilist, 0, as_size, cc);
        };

    auto const chunk_approx_init_till =
        [&cc](int size, std::initializer_list<int> ilist) {
          chunk_approx(size, ilist, 0, as_till, cc);
        };

    chunk_approx_init(13, {0, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 10}, {10, 3}});
    cc.clear();

    chunk_approx_init(0, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 0}, {0, 0}});
    cc.clear();

    chunk_approx_init(1, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 1}, {1, 0}});
    cc.clear();

    chunk_approx_init(2, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 2}, {2, 0}});
    cc.clear();

    chunk_approx_init(3, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 2}, {2, 1}});
    cc.clear();

    chunk_approx_init(4, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 3}, {3, 1}});
    cc.clear();

    chunk_approx_init(5, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 4}, {4, 1}});
    cc.clear();

    chunk_approx_init(6, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 5}, {5, 1}});
    cc.clear();

    chunk_approx_init(7, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 5}, {6, 1}});
    cc.clear();

    chunk_approx_init(8, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 5}, {6, 2}});
    cc.clear();

    chunk_approx_init(9, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 6}, {7, 2}});
    cc.clear();

    chunk_approx_init(10, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 7}, {8, 2}});
    cc.clear();

    chunk_approx_init(11, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 8}, {9, 2}});
    cc.clear();

    chunk_approx_init(12, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 8}, {9, 3}});
    cc.clear();

    chunk_approx_init(13, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 9}, {10, 3}});
    cc.clear();

    chunk_approx_init(14, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 10}, {11, 3}});
    cc.clear();

    chunk_approx_init(15, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 11}, {12, 3}});
    cc.clear();

    chunk_approx_init_till(16, {1, 10, 3});
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 13}, {13, 16}});
    cc.clear();
  }

  {
    auto const chunk_approx_init =
        [&cc](int size, std::initializer_list<int> ilist) {
          chunk_approx(size, ilist, 2, as_till, cc);
        };

    chunk_approx_init(0, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 0}, {0, 0}, {0, 0}});
    cc.clear();

    chunk_approx_init(1, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 1}, {1, 1}});
    cc.clear();

    chunk_approx_init(2, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 2}});
    cc.clear();

    chunk_approx_init(3, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 1}, {1, 2}, {2, 3}});
    cc.clear();

    chunk_approx_init(4, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 3}, {3, 4}});
    cc.clear();

    chunk_approx_init(5, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 4}, {4, 5}});
    cc.clear();

    chunk_approx_init(6, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 4}, {4, 6}});
    cc.clear();

    chunk_approx_init(7, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 5}, {5, 7}});
    cc.clear();

    chunk_approx_init(8, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 6}, {6, 8}});
    cc.clear();

    chunk_approx_init(9, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 6}, {6, 9}});
    cc.clear();

    chunk_approx_init(10, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 7}, {7, 10}});
    cc.clear();

    chunk_approx_init(11, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 7}, {7, 11}});
    cc.clear();

    chunk_approx_init(12, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 8}, {8, 12}});
    cc.clear();

    chunk_approx_init(13, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 9}, {9, 13}});
    cc.clear();

    chunk_approx_init(14, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 2}, {2, 9}, {9, 14}});
    cc.clear();

    chunk_approx_init(15, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 3}, {3, 10}, {10, 15}});
    cc.clear();

    chunk_approx_init(16, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 3}, {3, 11}, {11, 16}});
    cc.clear();

    chunk_approx_init(17, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 3}, {3, 11}, {11, 17}});
    cc.clear();

    chunk_approx_init(18, {1, 3, 2});
    CHECK(cc.data_ == vec_t{{0, 3}, {3, 12}, {12, 18}});
    cc.clear();
  }
}
