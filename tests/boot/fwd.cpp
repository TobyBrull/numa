#include "numa/boot/fwd.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("check basic properties of types", "[types][constexpr]")
{
  /**
   * Signedness of index_t is used in:
   *   - increment/decrement of random access iterators
   */
  static_assert(std::is_signed_v<index_t>);
  static_assert(std::is_signed_v<index_t>);
}
