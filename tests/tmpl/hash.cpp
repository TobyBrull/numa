#include "numa/tmpl/hash.hpp"

#include "catch2/catch.hpp"

using namespace numa;

template<typename H>
void
hash_test()
{
  //  if constexpr (sizeof (H) == 4)
  //  {
  //    static_assert (hash ("NuMa"_s) == 277196694U);
  //    static_assert (hash (""_s) == 0x811c9dc5U);
  //  }
  //  else if constexpr (sizeof (H) == 8)
  {
    static_assert(hash("NuMa"_s) == 9699028215918538486U);
    static_assert(hash(""_s) == 0xcbf29ce484222325U);
  }
}

TEST_CASE("literals hash", "[literals][hash]")
{
  static_assert(hash("Hello"_s) == hash("Hell"_s + "o"_s));
  static_assert(hash("Hello"_s) != hash("World"_s));

  hash_test<hash_value_t>();

  static_assert(
      clear_bit(0b001010010101010101UL, 0_i) == 0b001010010101010100UL);
  static_assert(
      clear_bit(0b001010010101010101UL, 1_i) == 0b001010010101010101UL);
  static_assert(
      clear_bit(0b001010010101010101UL, 2_i) == 0b001010010101010001UL);
  static_assert(
      clear_bit(0b001010010101010101UL, 3_i) == 0b001010010101010101UL);
  static_assert(
      clear_bit(0b001010010101010101UL, 4_i) == 0b001010010101000101UL);

  static_assert(set_bit(0b001010010101010101UL, 0_i) == 0b001010010101010101UL);
  static_assert(set_bit(0b001010010101010101UL, 1_i) == 0b001010010101010111UL);
  static_assert(set_bit(0b001010010101010101UL, 2_i) == 0b001010010101010101UL);
  static_assert(set_bit(0b001010010101010101UL, 3_i) == 0b001010010101011101UL);
  static_assert(set_bit(0b001010010101010101UL, 4_i) == 0b001010010101010101UL);

  static_assert(is_bit_set(0b001010010101010101UL, 0_i));
  static_assert(!is_bit_set(0b001010010101010101UL, 1_i));
  static_assert(is_bit_set(0b001010010101010101UL, 2_i));
  static_assert(!is_bit_set(0b001010010101010101UL, 3_i));
  static_assert(is_bit_set(0b001010010101010101UL, 4_i));
}
