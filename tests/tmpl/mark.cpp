#include "numa/tmpl/mark.hpp"

#include "catch2/catch.hpp"

using namespace numa;

DEFINE_MARK(tester);

TEST_CASE("mark", "[mark]")
{
  static_assert(m_tester.name == "tester"_s);

  CHECK(static_cast<std::string>(m_tester.name) == "tester");

  static_assert(is_mark(c_type_t<decltype(m_tester)>{}));
  static_assert(is_mark(c_type<m_rows_t>));
  static_assert(is_mark(c_type<m_cols_t>));
  static_assert(is_mark(c_type<m_lays_t>));
  static_assert(is_mark(c_type<m_a_t>));
  static_assert(is_mark(c_type<m_z_t>));
  static_assert(!is_mark(c_type<float>));
  static_assert(!is_mark(c_type<c_true_t>));

  static_assert(is_mark<m_z_t>());
  static_assert(!is_mark<int>());

  static_assert(m_z != m_a);
  static_assert(m_a == m_a);

  static_assert(m_a_t{} == m_a);
}
