#include "numa/tmpl/axes_info.hpp"

#include "numa/tmpl/mark.hpp"
#include "numa/tmpl/type_traits.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("test axes-info", "[axes_info]")
{
  static_assert(is_valid_axes_info(type_list_t{1_i, 0_i, 0_i}));

  static_assert(is_valid_axes_info(type_list_t{3_i, 1_i, 2_i, 0_i, 2_i}));

  static_assert(!is_valid_axes_info(type_list_t{3_i, 2_i, 0_i, 2_i}));

  static_assert(is_valid_axes_info(type_list_t{0_i, 1_i, 2_i, 0_i, 1_i, 2_i}));

  static_assert(!is_valid_axes_info(type_list_t{1_i, 2_i, 1_i, 2_i}));

  static_assert(axes_info<index_list_t<3, 2, 1, 0>>::num_dims == 4);
  static_assert(axes_info<index_list_t<2, 0, 1, 2, 0>>::num_dims == 5);

  static_assert(axes_info<index_list_t<3, 2, 1, 0>>::num_axes == 4);
  static_assert(axes_info<index_list_t<2, 0, 1, 2, 0>>::num_axes == 3);

  {
    constexpr axes_info<index_list_t<0, 1, 2, 0, 1, 2>> ai;

    static_assert(ai.dims_for(0_i) == type_list_t{0_i, 3_i});
    static_assert(ai.dims_for(1_i) == type_list_t{1_i, 4_i});
    static_assert(ai.dims_for(2_i) == type_list_t{2_i, 5_i});
  }

  {
    constexpr axes_info<index_list_t<0, 1, 2, 3>> ai;

    static_assert(ai.dims_for(0_i) == type_list_t{0_i});
    static_assert(ai.dims_for(1_i) == type_list_t{1_i});
    static_assert(ai.dims_for(2_i) == type_list_t{2_i});
    static_assert(ai.dims_for(3_i) == type_list_t{3_i});
  }

  {
    constexpr auto ai = index_list_iota(2_i);

    static_assert(type_list_t{0_i, 1_i} == ai);
    static_assert(index_list_iota<2>() == ai);
  }
}

TEST_CASE(
    "axes-info form size conversion", "[axes_info][size_to_form][from_to_size]")
{
  {
    constexpr axes_info<index_list_t<0, 1, 2>> ai;

    static_assert(ai.form_to_size(index(9, 8, 7)) == index(9, 8, 7));

    static_assert(ai.size_to_form(index(9, 8, 7)) == index(9, 8, 7));
  }

  {
    constexpr axes_info<index_list_t<0, 1, 2, 0, 1, 2>> ai;

    static_assert(ai.form_to_size(index(9, 8, 7, 9, 8, 7)) == index(9, 8, 7));

    static_assert(ai.size_to_form(index(9, 8, 7)) == index(9, 8, 7, 9, 8, 7));

    CHECK_NOTHROW(ai.form_to_size(index(9, 8, 7, 9, 8, 7)) == index(9, 8, 7));

    CHECK_THROWS(ai.form_to_size(index(9, 8, 8, 9, 8, 7)));
    CHECK_THROWS(ai.form_to_size(index(9, 8, 7, 9, 9, 7)));
    CHECK_THROWS(ai.form_to_size(index(9, 8, 7, 3, 8, 7)));
  }

  {
    constexpr axes_info<index_list_t<0, 1, 2, 2, 1, 0>> ai;

    static_assert(ai.form_to_size(index(9, 8, 7, 7, 8, 9)) == index(9, 8, 7));

    static_assert(ai.size_to_form(index(9, 8, 7)) == index(9, 8, 7, 7, 8, 9));
  }

  {
    constexpr auto ai = axes_info_mirror_tensor(2_i);

    static_assert(ai.list == type_list_t{0_i, 1_i, 0_i, 1_i});

    static_assert(ai.form_to_size(index(9, 3, 9, 3)) == index(9, 3));

    static_assert(ai.size_to_form(index(2, 6)) == index(2, 6, 2, 6));
  }
}

TEST_CASE(
    "axes-info selected form size conversion", "[axes_info][size_to_form]")
{
  {
    constexpr auto ai  = axes_info_mirror_tensor(3_i);
    constexpr auto sel = type_list_t{1_i, 0_i};

    static_assert(ai.size_to_form(sel, index(10, 50)) == index(50, 10, 50, 10));
  }

  {
    constexpr axes_info<index_list_t<0, 1, 2, 0, 0, 1, 2>> ai;
    constexpr auto sel = type_list_t{1_i, 0_i};

    static_assert(
        ai.size_to_form(sel, index(10, 50)) == index(50, 10, 50, 50, 10));
  }

  {
    constexpr axes_info<index_list_t<0, 1, 2, 0, 0, 1, 2>> ai;
    constexpr auto sel = type_list_t{0_i, 1_i};

    static_assert(
        ai.size_to_form(sel, index(50, 10)) == index(50, 10, 50, 50, 10));
  }
}

TEST_CASE("axes-info select-dims", "[axes_info][select_dims]")
{
  {
    constexpr auto ai = axes_info_mirror_tensor(3_i);

    static_assert(
        ai.select_dims(type_list_t{1_i, 0_i}) ==
        type_list_t{0_i, 1_i, 3_i, 4_i});
  }

  {
    constexpr axes_info<index_list_t<0, 1, 2, 0, 0, 1, 2>> ai;

    static_assert(
        ai.select_dims(type_list_t{0_i, 1_i}) ==
        type_list_t{0_i, 1_i, 3_i, 4_i, 5_i});
  }

  {
    constexpr axes_info<index_list_t<0, 1, 2, 0, 0, 1, 2>> ai;

    static_assert(
        ai.select_dims(type_list_t{1_i, 0_i}) ==
        type_list_t{0_i, 1_i, 3_i, 4_i, 5_i});
  }
}

TEST_CASE("expand-tuple", "[expand_tuple]")
{
  {
    constexpr axes_info<index_list_t<0, 1, 2, 0, 2, 1>> ai;
    constexpr index_list_t<2, 0> selected;

    constexpr std::array<char, 2> arr = {'z', 'w'};

    constexpr char fill = 'o';

    constexpr auto result = ai.expand_tuple(selected, arr, fill);

    static_assert(result == std::tuple('w', 'o', 'z', 'w', 'z', 'o'));
  }

  {
    constexpr axes_info<index_list_t<0, 1, 2, 2, 2, 1, 0, 1>> ai;
    constexpr index_list_t<2, 1> selected;

    constexpr std::array<char, 2> arr = {'a', 'b'};

    constexpr char fill = 'c';

    constexpr auto result = ai.expand_tuple(selected, arr, fill);

    static_assert(result == std::tuple('c', 'b', 'a', 'a', 'a', 'b', 'c', 'b'));
  }
}

TEST_CASE(
    "for-each on type-lists to tuple",
    "[type_list_t][for_each_indexed_to_tuple]")
{
  constexpr auto result = detail::for_each_indexed_to_tuple(
      type_list<m_a_t, m_c_t, m_z_t>, [](const auto ind, auto val) {
        if constexpr (std::is_same_v<decltype(val), m_a_t>) {
          return 7;
        }
        else if constexpr (std::is_same_v<decltype(val), m_c_t>) {
          return 3.14;
        }
        else if constexpr (std::is_same_v<decltype(val), m_z_t>) {
          return 'a';
        }
        else {
          THROW("Broken!");
        }
      });

  static_assert(result == std::tuple(7, 3.14, 'a'));
}

TEST_CASE("test axes-info class", "[axes_info]")
{
  constexpr auto ai = axes_info_tensor(3_i);

  static_assert(is_axes_info<decay_t<decltype(ai)>>());
}
