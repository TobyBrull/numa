#include "numa/tmpl/integer_list.hpp"

#include "numa/tmpl/mark.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("integer_list type-traits", "[integer_list_t][type_traits][concepts]")
{
  static_assert(!is_integer_list<char>());
  static_assert(!is_integer_list<type_list_t<c_index_t<3>, c_bool_t<true>>>());
  static_assert(is_integer_list<type_list_t<c_index_t<2>, c_index_t<5>>>());

  static_assert(
      is_integer_list_of<type_list_t<c_index_t<2>, c_index_t<5>>, index_t>());
  static_assert(
      !is_integer_list_of<type_list_t<c_index_t<2>, c_index_t<5>>, bool>());

  static_assert(is_index_list<type_list_t<c_index_t<2>, c_index_t<5>>>());
  static_assert(!is_index_list<type_list_t<c_bool_t<true>, c_bool_t<false>>>());
  static_assert(!is_index_list<type_list_t<c_index_t<2>, c_bool_t<false>>>());

  static_assert(!is_bool_list<type_list_t<c_index_t<2>, c_index_t<5>>>());
  static_assert(is_bool_list<type_list_t<c_bool_t<true>, c_bool_t<false>>>());
  static_assert(!is_bool_list<type_list_t<c_index_t<2>, c_bool_t<false>>>());

  static_assert(is_unique(index_list<0>));
  static_assert(is_unique(index_list<3>));
  static_assert(is_unique<index_list_t<0>>());
}

TEST_CASE("integer-list plumb", "[integer_list_t][plumb]")
{
  static_assert(plumb(5_i, type_list_t{3_i, 1_i}) == index_list<0, 2, 4>);

  static_assert(
      plumb(6_i, type_list_t{3_i, 1_i}) == type_list_t{0_i, 2_i, 4_i, 5_i});

  static_assert(
      plumb(6_i, type_list_t{0_i, 1_i}) == type_list_t{2_i, 3_i, 4_i, 5_i});

  static_assert(
      plumb(6_i, type_list_t{5_i, 4_i}) == type_list_t{0_i, 1_i, 2_i, 3_i});

  static_assert(
      plumb(6_i, type_list_t{1_i, 3_i}) == type_list_t{0_i, 2_i, 4_i, 5_i});

  static_assert(
      plumb<index_t>(0_i, integer_list<index_t>) == integer_list<index_t>);

  static_assert(
      plumb(5_i, type_list_t{2_i}) == type_list_t{0_i, 1_i, 3_i, 4_i});

  static_assert(
      plumb(5_i, type_list_t{2_i, 4_i}) == type_list_t{0_i, 1_i, 3_i});
  static_assert(
      plumb(5_i, type_list_t{4_i, 2_i}) == type_list_t{0_i, 1_i, 3_i});
  static_assert(
      plumb(5_i, type_list_t{4_i, 3_i, 2_i, 1_i}) == type_list_t{0_i});
  static_assert(
      plumb(5_i, type_list_t{0_i, 3_i, 2_i, 4_i}) == type_list_t{1_i});
  static_assert(
      plumb(5_i, type_list_t{0_i, 1_i, 2_i}) == type_list_t{3_i, 4_i});
  static_assert(
      plumb(5_i, type_list_t{2_i, 3_i, 4_i}) == type_list_t{0_i, 1_i});
  static_assert(
      plumb(5_i, type_list_t{0_i, 4_i}) == type_list_t{1_i, 2_i, 3_i});
}

TEST_CASE("integer-list as-index", "[index_list_t][as_index]")
{
  static_assert(as_index(type_list_t{}) == index<0>());

  static_assert(as_index(type_list_t{3_i, 1_i, 3_i, 2_i}) == index(3, 1, 3, 2));

  static_assert(as_index(index_list<9, 2, 5>) == index(9, 2, 5));
}

TEST_CASE("size of integer-list", "[integer_list_t][size]")
{
  static_assert(size(type_list_t{3_i, 0_i}) == c_index<2>);
  static_assert(size(index_list<3, 0>) == c_index<2>);
}

TEST_CASE(
    "integer-list is_(strictly_)sorted",
    "[integer_list_t][is_sorted][is_sorted_strictly]")
{
  static_assert(is_sorted(type_list_t{4_i, 5_i, 5_i}));
  static_assert(!is_sorted_strictly(type_list_t{4_i, 5_i, 5_i}));

  static_assert(is_sorted(type_list_t{0_i, 3_i, 5_i}));
  static_assert(is_sorted_strictly(type_list_t{0_i, 3_i, 5_i}));

  static_assert(!is_sorted(type_list_t{0_i, 3_i, 2_i}));
  static_assert(!is_sorted_strictly(type_list_t{0_i, 3_i, 2_i}));
}

TEST_CASE(
    "is-permutation and is-variation on integer lists",
    "[integer_list_t][is_permutation][is_variation][constexpr]")
{
  static_assert(is_permutation(integer_list_t<long>{}));
  static_assert(is_permutation(index_list<>));
  static_assert(is_permutation(type_list_t{0_i}));
  static_assert(!is_permutation(type_list_t{1_i}));
  static_assert(is_permutation(type_list_t{0_i, 1_i, 2_i}));
  static_assert(is_permutation(type_list_t{2_i, 0_i, 1_i}));
  static_assert(!is_permutation(type_list_t{2_i, 1_i, 1_i}));
  static_assert(!is_permutation(type_list_t{3_i, 0_i, 1_i}));

  static_assert(is_variation<0, long>(type_list_t{}, 0_i));
  static_assert(!is_variation(type_list_t{0_i}, 0_i));
  static_assert(is_variation<3, long>(type_list_t{}, 3_i));
  static_assert(is_variation(type_list_t{0_i}, 5_i));
  static_assert(is_variation(type_list_t{1_i}, 4_i));
  static_assert(!is_variation(type_list_t{4_i}, 4_i));
  static_assert(!is_variation(type_list_t{6_i}, 4_i));
  static_assert(is_variation(type_list_t{0_i, 1_i, 2_i}, 5_i));
  static_assert(!is_variation(type_list_t{2_i, 1_i, 1_i}, 5_i));
  static_assert(is_variation(type_list_t{3_i, 0_i, 1_i}, 5_i));

  static_assert(is_variation(type_list_t{}, 5_i));
  static_assert(is_variation(index_list<>, 6_i));
  static_assert(is_variation<6>(index_list<>));

  static_assert(is_variation(type_list_t{2_i, 0_i, 1_i}, 5_i));
  static_assert(is_variation<5, index_t, 2, 0, 1>());

  static_assert(!is_variation(type_list_t{2_i, 1_i, 1_i}, 5_i));
  static_assert(is_variation(type_list_t{3_i, 0_i, 1_i}, 5_i));
}

TEST_CASE("make-integer-list-iota", "[integer_list_iota][type_list_t]")
{
  static_assert(integer_list_uniform(2_i, 0_i) == type_list_t{0_i, 0_i});

  static_assert(
      integer_list_uniform(2_i, 0_i) ==
      integer_list_uniform_t<index_t, 2, 0>{});

  static_assert(integer_list_uniform(2_i, 11_i) == type_list_t{11_i, 11_i});

  static_assert(integer_list_iota<index_t>(3_i) == type_list_t{0_i, 1_i, 2_i});

  static_assert(integer_list_riota<index_t>(3_i) == type_list_t{2_i, 1_i, 0_i});

  static_assert(integer_list_iota<index_t>(1_i) == type_list_t{0_i});

  static_assert(index_list_uniform(3_i, 0_i) == type_list_t{0_i, 0_i, 0_i});

  static_assert(index_list_uniform(3_i, 0_i) == index_list_uniform_t<3, 0>{});

  static_assert(index_list_iota(3_i) == type_list_t{0_i, 1_i, 2_i});

  static_assert(
      index_list_uniform(5_i, 0_i) == type_list_t{0_i, 0_i, 0_i, 0_i, 0_i});

  static_assert(index_list_uniform(3_i, 7_i) == type_list_t{7_i, 7_i, 7_i});

  static_assert(index_list_uniform(3_i, 7_i) == index_list_uniform_t<3, 7>{});

  static_assert(index_list_iota(5_i) == type_list_t{0_i, 1_i, 2_i, 3_i, 4_i});

  static_assert(
      index_list_mirror(3_i) == type_list_t{0_i, 1_i, 2_i, 0_i, 1_i, 2_i});

  static_assert(
      index_list_transposition(3_i) ==
      type_list_t{3_i, 4_i, 5_i, 0_i, 1_i, 2_i});

  static_assert(index_list_transposition(1_i) == type_list_t{1_i, 0_i});

  static_assert(index_list_riota(2_i) == type_list_t{1_i, 0_i});

  static_assert(index_list_riota(1_i) == type_list_t{0_i});

  static_assert(index_list_riota(4_i) == type_list_t{3_i, 2_i, 1_i, 0_i});

  static_assert(
      bool_list_uniform(3_i, 0_b) == type_list_t{0_b, "false"_b, "f"_b});

  static_assert(
      bool_list_uniform(3_i, 1_b) == type_list_t{1_b, "true"_b, "t"_b});

  static_assert(bool_list_uniform(3_i, 1_b) == bool_list_uniform_t<3, true>{});
}

TEST_CASE("concat-list-of-lists", "[type_list_t][concat_list_of_lists]")
{
  constexpr auto to_concat = type_list_t{
      type_list_t{m_a, m_b, m_c},
      type_list_t{m_d, m_e},
      type_list_t{m_f},
      type_list_t{m_g, m_h}};

  static_assert(
      concat_list_of_lists(to_concat) ==
      type_list_t{m_a, m_b, m_c, m_d, m_e, m_f, m_g, m_h});
}

TEST_CASE("index selection", "[index][select_at][constexpr]")
{
  /*
   * index
   */
  static_assert(
      select_at(index<4>(10, 20, 30, 40), type_list_t{2_i, 0_i, 1_i}) ==
      index<3>(30, 10, 20));

  static_assert(
      select_at(
          index<4>(10, 20, 30, 40), type_list_t{2_i, 0_i, 1_i, 1_i, 0_i}) ==
      index<5>(30, 10, 20, 20, 10));

  static_assert(
      select_at(index<4>(10, 20, 30, 40), type_list_t{2_i, 0_i, 1_i}) ==
      index<3>(30, 10, 20));

  static_assert(
      select_at(
          index<4>(10, 20, 30, 40), type_list_t{2_i, 0_i, 1_i, 1_i, 0_i}) ==
      index<5>(30, 10, 20, 20, 10));

  /*
   * type-list
   */
  static_assert(
      select_at(index_list<10, 20, 30, 40>, type_list_t{2_i, 0_i, 1_i}) ==
      index_list<30, 10, 20>);

  static_assert(
      select_at(
          type_list_t{m_a, m_r, m_g, m_d_t{}}, type_list_t{2_i, 0_i, 0_i}) ==
      type_list_t{m_g, m_a_t{}, m_a});

  /*
   * std::tuple
   */
  static_assert(
      select_at(std::tuple(3, 1.23, 4321.9), type_list_t{2_i, 0_i}) ==
      std::tuple(4321.9, 3));

  static_assert(
      select_at(std::tuple(3, 7, 8, 1), type_list_t{2_i, 1_i, 1_i, 0_i}) ==
      std::tuple(8, 7, 7, 3));
}

TEST_CASE("type-list select-if", "[type_list_t][select_if][constexpr]")
{
  /*
   * index
   */
  static_assert(
      select_if(index<4>(10, 20, 30, 40), type_list_t{0_b, 1_b, 1_b, 0_b}) ==
      index<2>(20, 30));

  static_assert(
      select_if(
          index<4>(10, 20, 30, 40), bool_list<true, false, true, false>) ==
      index<2>(10, 30));

  static_assert(
      select_if(index<4>(10, 20, 30, 40), type_list_t{0_b, 0_b, 0_b, 1_b}) ==
      index<1>(40));

  static_assert(
      select_if(
          index<4>(10, 20, 30, 40), bool_list<true, false, false, false>) ==
      index<1>(10));

  /*
   * type-list
   */
  static_assert(
      select_if(index_list<10, 20, 30, 40>, type_list_t{0_b, 1_b, 1_b, 0_b}) ==
      index_list<20, 30>);

  static_assert(
      select_if(
          type_list_t{m_a, m_r, m_g, m_d},
          bool_list<true, false, true, false>) == type_list_t{m_a, m_g});

  /*
   * std::tuple
   */
  static_assert(
      select_if(std::tuple(3, 1.23, 4321.9), type_list_t{1_b, 0_b, 1_b}) ==
      std::tuple(3, 4321.9));

  static_assert(
      select_if(std::tuple(3, 7, 8, 1), type_list_t{0_b, 1_b, 0_b, 0_b}) ==
      std::tuple(7));
}

TEST_CASE("type-list count-if", "[type_list_t][count_if][constexpr]")
{
  static_assert(count_if(type_list_t{0_b, 1_b, 1_b, 0_b}) == 2_i);

  static_assert(count_if(bool_list<true, false, true, true, false>) == 3_i);
}

TEST_CASE("bool functions", "[bool_list_t][constexpr]")
{
  static_assert(all_of(type_list_t{1_b, 1_b, 1_b}));
  static_assert(!all_of(type_list_t{1_b, 0_b, 1_b}));
  static_assert(!all_of(type_list_t{0_b, 0_b, 0_b}));

  static_assert(any_of(type_list_t{1_b, 1_b, 1_b}));
  static_assert(any_of(type_list_t{1_b, 0_b, 1_b}));
  static_assert(!any_of(type_list_t{0_b, 0_b, 0_b}));

  static_assert(!none_of(type_list_t{1_b, 1_b, 1_b}));
  static_assert(!none_of(type_list_t{1_b, 0_b, 1_b}));
  static_assert(none_of(type_list_t{0_b, 0_b, 0_b}));

  static_assert(
      logical_not(type_list_t{1_b, 1_b, 1_b}) == type_list_t{0_b, 0_b, 0_b});
  static_assert(
      logical_not(type_list_t{1_b, 0_b, 1_b}) == type_list_t{0_b, 1_b, 0_b});
  static_assert(
      logical_not(type_list_t{0_b, 0_b, 0_b}) == type_list_t{1_b, 1_b, 1_b});

  static_assert(
      logical_and(type_list_t{1_b, 0_b, 0_b}, type_list_t{1_b, 0_b, 1_b}) ==
      type_list_t{1_b, 0_b, 0_b});

  static_assert(
      logical_or(type_list_t{1_b, 0_b, 0_b}, type_list_t{1_b, 0_b, 1_b}) ==
      type_list_t{1_b, 0_b, 1_b});
}

TEST_CASE(
    "index overwriting and joining", "[index][overwrite][join][constexpr]")
{
  {
    index<4> ind(zero);

    overwrite(ind, type_list_t{3_i, 1_i}, index<2>(16, 32));

    CHECK(ind == index<4>(0, 32, 0, 16));

    overwrite(ind, type_list_t{2_i, 3_i}, index<2>(-1, -2));

    CHECK(ind == index<4>(0, 32, -1, -2));
  }

  static_assert(
      join<5>(index<2>(10, 20), type_list_t{2_i, 1_i}, index<3>(-1, -2, -3)) ==
      index<5>(-1, 20, 10, -2, -3));

  static_assert(
      join<5>(index<2>(10, 20), type_list_t{1_i, 2_i}, index<3>(-1, -2, -3)) ==
      index<5>(-1, 10, 20, -2, -3));

  static_assert(
      join<5>(
          index<3>(10, 20, 30), type_list_t{4_i, 0_i, 1_i}, index<2>(3, 2)) ==
      index<5>(20, 30, 3, 2, 10));

  static_assert(
      join<5>(
          index<3>(10, 20, 30), type_list_t{0_i, 1_i, 4_i}, index<2>(3, 2)) ==
      index<5>(10, 20, 3, 2, 30));

  {
    index<4> ind(zero);

    overwrite(ind, type_list_t{3_i, 1_i}, index<2>(16, 32));

    CHECK(ind == index<4>(0, 32, 0, 16));

    overwrite(ind, type_list_t{2_i, 3_i}, index<2>(-1, -2));

    CHECK(ind == index<4>(0, 32, -1, -2));
  }

  static_assert(
      join<5>(index<2>(10, 20), type_list_t{2_i, 1_i}, index<3>(-1, -2, -3)) ==
      index<5>(-1, 20, 10, -2, -3));

  static_assert(
      join<5>(index<2>(10, 20), type_list_t{1_i, 2_i}, index<3>(-1, -2, -3)) ==
      index<5>(-1, 10, 20, -2, -3));

  static_assert(
      join<5>(
          index<3>(10, 20, 30), type_list_t{4_i, 0_i, 1_i}, index<2>(3, 2)) ==
      index<5>(20, 30, 3, 2, 10));

  static_assert(
      join<5>(
          index<3>(10, 20, 30), type_list_t{0_i, 1_i, 4_i}, index<2>(3, 2)) ==
      index<5>(10, 20, 3, 2, 30));
}
