#include "numa/tmpl/vary_axes_info.hpp"

#include "catch2/catch.hpp"

using namespace numa;

constexpr auto vam_dyn  = vary_axis_mode::dynamic;
constexpr auto vam_capa = vary_axis_mode::fixed_capacity;
constexpr auto vam_size = vary_axis_mode::fixed_size;

TEST_CASE("test vary-axes combine", "[vary_axes_mode][combine]")
{
  static_assert(
      combine(type_list_t{
          vary_axis_tuple<vam_dyn, 0>{},
          vary_axis_tuple<vam_capa, 5>{},
          vary_axis_tuple<vam_capa, 2>{}}) == vary_axis_tuple<vam_capa, 2>{});

  static_assert(
      combine(type_list_t{
          vary_axis_tuple<vam_dyn, 0>{},
          vary_axis_tuple<vam_size, 5>{},
          vary_axis_tuple<vam_capa, 7>{}}) == vary_axis_tuple<vam_size, 5>{});

  static_assert(
      combine(type_list_t{
          vary_axis_tuple<vam_dyn, 0>{},
          vary_axis_tuple<vam_size, 8>{},
          vary_axis_tuple<vam_size, 8>{}}) == vary_axis_tuple<vam_size, 8>{});

  static_assert(
      combine(type_list_t{
          vary_axis_tuple<vam_dyn, 0>{},
          vary_axis_tuple<vam_dyn, 0>{},
          vary_axis_tuple<vam_dyn, 0>{}}) == vary_axis_tuple<vam_dyn, 0>{});

  static_assert(
      combine(type_list_t{
          vary_axis_tuple<vam_dyn, 0>{}, vary_axis_tuple<vam_dyn, 0>{}}) ==
      vary_axis_tuple<vam_dyn, 0>{});

  static_assert(
      combine(type_list_t{
          vary_axis_tuple<vam_dyn, 0>{}, vary_axis_tuple<vam_capa, 4>{}}) ==
      vary_axis_tuple<vam_capa, 4>{});

  static_assert(
      combine(type_list_t{
          vary_axis_tuple<vam_dyn, 0>{}, vary_axis_tuple<vam_size, 4>{}}) ==
      vary_axis_tuple<vam_size, 4>{});

  static_assert(
      combine(type_list_t{
          vary_axis_tuple<vam_capa, 3>{}, vary_axis_tuple<vam_dyn, 0>{}}) ==
      vary_axis_tuple<vam_capa, 3>{});

  static_assert(
      combine(type_list_t{
          vary_axis_tuple<vam_capa, 6>{}, vary_axis_tuple<vam_capa, 8>{}}) ==
      vary_axis_tuple<vam_capa, 6>{});

  static_assert(
      combine(type_list_t{
          vary_axis_tuple<vam_capa, 6>{}, vary_axis_tuple<vam_size, 4>{}}) ==
      vary_axis_tuple<vam_size, 4>{});

  static_assert(
      combine(type_list_t{
          vary_axis_tuple<vam_size, 9>{}, vary_axis_tuple<vam_dyn, 0>{}}) ==
      vary_axis_tuple<vam_size, 9>{});

  static_assert(
      combine(type_list_t{
          vary_axis_tuple<vam_size, 4>{}, vary_axis_tuple<vam_capa, 7>{}}) ==
      vary_axis_tuple<vam_size, 4>{});

  static_assert(
      combine(type_list_t{
          vary_axis_tuple<vam_size, 13>{}, vary_axis_tuple<vam_size, 13>{}}) ==
      vary_axis_tuple<vam_size, 13>{});
}

TEST_CASE("test vary-axes-info-dynamic", "[vary_axes_info_dynamic]")
{
  constexpr auto ai = vary_axes_info_dynamic(3_i);

  static_assert(ai.is_dynamic() == bool_list<true, true, true>);

  static_assert(ai.is_fixed_capacity() == bool_list<false, false, false>);

  static_assert(ai.is_fixed_size() == bool_list<false, false, false>);

  static_assert(
      select_if(index_list_iota(3_i), ai.is_dynamic()) == index_list<0, 1, 2>);

  static_assert(
      select_if(index_list_iota(3_i), ai.is_fixed_capacity()) == index_list<>);

  static_assert(
      select_if(index_list_iota(3_i), ai.is_fixed_size()) == index_list<>);
}

TEST_CASE(
    "test vary-axes-info-fixed-capacity", "[vary_axes_info_fixed_capacity]")
{
  constexpr auto ai = vary_axes_info_fixed_capacity(type_list_t{2_i, 5_i, 9_i});

  static_assert(ai.is_dynamic() == bool_list<false, false, false>);

  static_assert(ai.is_fixed_capacity() == bool_list<true, true, true>);

  static_assert(ai.is_fixed_size() == bool_list<false, false, false>);
}

TEST_CASE("test vary-axes-info-fixed-size", "[vary_axes_info_fixed_size]")
{
  constexpr auto ai = vary_axes_info_fixed_size(type_list_t{2_i, 5_i, 9_i});

  static_assert(ai.is_dynamic() == bool_list<false, false, false>);

  static_assert(ai.is_fixed_capacity() == bool_list<false, false, false>);

  static_assert(ai.is_fixed_size() == bool_list<true, true, true>);
}

TEST_CASE("test diverse vary-axes-info", "[vary_axes_info]")
{
  constexpr vary_axes_info<type_list_t<
      vary_axis_tuple<vam_dyn, 0>,
      vary_axis_tuple<vam_size, 4>,
      vary_axis_tuple<vam_capa, 5>,
      vary_axis_tuple<vam_size, 3>,
      vary_axis_tuple<vam_dyn, 0>,
      vary_axis_tuple<vam_capa, 2>>>
      ai;

  static_assert(
      ai.is_dynamic() == bool_list<true, false, false, false, true, false>);

  static_assert(
      ai.is_fixed_capacity() ==
      bool_list<false, false, true, false, false, true>);

  static_assert(
      ai.is_fixed_size() == bool_list<false, true, false, true, false, false>);

  static_assert(
      select_if(index_list_iota(6_i), ai.is_dynamic()) == index_list<0, 4>);

  static_assert(
      select_if(index_list_iota(6_i), ai.is_fixed_capacity()) ==
      index_list<2, 5>);

  static_assert(
      select_if(index_list_iota(6_i), ai.is_fixed_size()) == index_list<1, 3>);
}
