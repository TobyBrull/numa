#include "numa/tmpl/dynamicizer.hpp"

#include "catch2/catch.hpp"

#include <sstream>

using namespace numa;

TEST_CASE("test dynamicizer", "[dynamicizer]")
{
  std::ostringstream buffer;

  for (index_t k = 0; k < 4; ++k) {
    dynamicizer<4>(
        [&buffer]<index_t K>() { buffer << static_cast<int>(K) << '\n'; }, k);
  }

  CHECK(buffer.str() == "0\n1\n2\n3\n");
}

struct Functor2 {
 public:
  Functor2(std::ostringstream& buffer) : buffer_(buffer) {}

  template<typename IndexList>
  void operator()(const char sep_1, const char sep_2)
  {
    static_assert(size(IndexList{}) == 2);

    (*this).operator()<get<0>(IndexList{}).value, get<1>(IndexList{}).value>(
        sep_1, sep_2);
  }

  template<index_t I, index_t J>
  void operator()(const char sep_1, const char sep_2)
  {
    buffer_ << static_cast<int>(I) << sep_1 << static_cast<int>(J) << sep_2;
  }

 private:
  std::ostringstream& buffer_;
};

TEST_CASE("test dynamicizer_list", "[dynamicizer_list]")
{
  std::ostringstream buffer;

  for (index_t i = 0; i < 4; ++i) {
    for (index_t j = 2; j <= 3; ++j) {
      dynamicizer_list<4>(
          Functor2(buffer), std::array<index_t, 2>{i, j}, ',', ';');
    }
  }

  CHECK(buffer.str() == "0,2;0,3;1,2;1,3;2,2;2,3;3,2;3,3;");
}
