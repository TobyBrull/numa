#include "numa/tmpl/type_list.hpp"

#include "numa/tmpl/mark.hpp"
#include "numa/tmpl/type_traits.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("basic type-list functions", "[type_list][basic]")
{
  static_assert(is_type_list(c_type_t(type_list_t(m_a, m_z))));
  static_assert(is_type_list(c_type_t(type_list<m_a_t, m_z_t>)));
  static_assert(is_type_list(c_type<type_list_t<m_a_t, m_z_t>>));
  static_assert(is_type_list<type_list_t<m_a_t, m_z_t>>());
  static_assert(!is_type_list(c_type<m_a_t>));

  static_assert(type_list_t<>{} != type_list_t<m_a_t>{});
  static_assert(type_list_t<>{} == type_list_t<>{});
  static_assert(type_list_t<m_a_t, m_b_t>{} == type_list<m_a_t, m_b_t>);
  static_assert(type_list_t<m_a_t, m_b_t>{} != type_list<m_b_t, m_a_t>);

  static_assert(type_list_t<m_a_t, m_b_t>{} == type_list_t(m_a, m_b));
  static_assert(type_list_t<int, double>{} == type_list_t(3, 3.0));
  static_assert(type_list<int, double> == type_list_t(3, 3.0));

  static_assert(size(type_list_t<>{}) == 0);
  static_assert(size(type_list_t<m_a_t, m_b_t, m_e_t>{}) == 3);

  static_assert(is_empty(type_list_t<>{}));
  static_assert(!is_empty(type_list<m_a_t, m_b_t, m_e_t>));

  static_assert(
      append(type_list_t<m_e_t, m_c_t, m_a_t>{}, c_type<m_f_t>) ==
      type_list_t<m_e_t, m_c_t, m_a_t, m_f_t>{});
  static_assert(append(type_list_t<>{}, c_type_t(m_c)) == type_list_t<m_c_t>{});

  static_assert(concat<int, double>() == type_list<int, double>);

  static_assert(concat(type_list_t<>{}, type_list_t<>{}) == type_list_t<>{});
  static_assert(
      concat(type_list_t<m_a_t, m_f_t>{}, type_list_t<>{}) ==
      type_list_t<m_a_t, m_f_t>{});
  static_assert(
      concat(type_list_t<>{}, type_list_t<m_a_t, m_f_t>{}) ==
      type_list_t<m_a_t, m_f_t>{});
  static_assert(
      concat(type_list<m_a_t, m_b_t, m_c_t>, type_list<m_a_t, m_f_t>) ==
      type_list_t<m_a_t, m_b_t, m_c_t, m_a_t, m_f_t>{});

  static_assert(
      erase(type_list<m_a_t, m_b_t, m_a_t, m_a_t, m_c_t, m_b_t, m_a_t>, m_a) ==
      type_list<m_b_t, m_c_t, m_b_t>);

  static_assert(
      erase(type_list_t{m_a, m_b, m_a, m_a, m_c, m_b, m_a}, m_a) ==
      type_list_t{m_b, m_c, m_b});

  static_assert(
      erase(type_list_t<m_b_t, m_c_t, m_b_t>{}, c_type<m_a_t>) ==
      type_list<m_b_t, m_c_t, m_b_t>);

  static_assert(erase(type_list_t<>{}, c_type<m_a_t>) == type_list_t<>{});

  static_assert(
      unique(type_list_t<
             m_a_t,
             m_f_t,
             m_a_t,
             m_d_t,
             m_f_t,
             m_a_t,
             m_d_t,
             m_e_t>{}) == type_list_t<m_a_t, m_f_t, m_d_t, m_e_t>{});
  static_assert(
      unique(type_list_t<m_a_t, m_a_t, m_a_t, m_d_t, m_d_t, m_d_t>{}) ==
      type_list_t<m_a_t, m_d_t>{});
  static_assert(
      unique(type_list_t<
             m_a_t,
             m_b_t,
             m_c_t,
             m_d_t,
             m_a_t,
             m_b_t,
             m_c_t,
             m_d_t,
             m_a_t,
             m_b_t,
             m_c_t,
             m_d_t>{}) == type_list_t<m_a_t, m_b_t, m_c_t, m_d_t>{});

  static_assert(!is_unique(
      type_list_t<m_a_t, m_f_t, m_a_t, m_d_t, m_f_t, m_a_t, m_d_t, m_e_t>{}));
  static_assert(is_unique(type_list_t<m_a_t, m_f_t, m_d_t, m_e_t>{}));
  static_assert(!is_unique(type_list_t<m_z_t, m_z_t>{}));
  static_assert(is_unique(type_list_t<m_z_t>{}));
  static_assert(is_unique(type_list_t<>{}));

  static_assert(
      find(type_list_t<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>{}, m_a) == 0_i);

  static_assert(
      find(type_list<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>, c_type<m_b_t>) == 1_i);

  static_assert(
      find(type_list_t<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>{}, m_d) == 3_i);

  static_assert(
      find(type_list<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>, m_z) == c_null);

  static_assert(get<0>(type_list<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>) == m_a);
  static_assert(get<1>(type_list<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>) == m_b);
  static_assert(get<3>(type_list<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>) == m_d);

  static_assert(get(type_list<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>, 0_i) == m_a);
  static_assert(get(type_list<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>, 1_i) == m_b);
  static_assert(get(type_list<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>, 3_i) == m_d);

  static_assert(
      get(type_list_t<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>{}, 3_i) == m_d);

  static_assert(front(type_list_t<m_a_t, m_b_t, m_c_t, m_d_t>{}) == m_a);
  static_assert(front(type_list_t(3_i, m_b, 1_i)) == 3_i);

  static_assert(back(type_list_t<m_a_t, m_b_t, m_c_t, m_d_t>{}) == m_d);
  static_assert(back(type_list_t(3_i, m_b, 1_i)) == 1_i);

  static_assert(
      sub(type_list_t<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>{},
          as_size,
          1_i,
          3_i) == type_list_t<m_b_t, m_c_t, m_d_t>{});

  static_assert(
      sub(type_list_t<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>{},
          as_till,
          2_i,
          5_i) == type_list_t<m_c_t, m_d_t, m_a_t>{});

  static_assert(
      sub(type_list_t<m_a_t, m_b_t, m_c_t, m_d_t, m_a_t>{},
          as_till,
          0_i,
          3_i) == type_list_t<m_a_t, m_b_t, m_c_t>{});

  static_assert(
      stride(type_list_t{m_a, m_b, m_c, m_d, m_a, m_z}, 0_i, 2_i) ==
      type_list_t{m_a, m_c, m_a});

  static_assert(
      stride(
          type_list_t{m_a, m_b, m_c, m_d, m_a, m_z, m_y, m_x, m_w}, 1_i, 3_i) ==
      type_list_t{m_b, m_a, m_x});

  static_assert(
      stride(
          type_list_t{m_a, m_b, m_c, m_d, m_a, m_z, m_y, m_x, m_w}, 5_i, 3_i) ==
      type_list_t{m_z, m_w});

  static_assert(find(type_list_t(m_d, m_g, m_a, m_w), c_type_t(m_g)) == 1);
  static_assert(find(type_list_t(m_d, m_g, m_a, m_w), c_type_t(m_d)) == 0);
  static_assert(find(type_list_t(m_d, m_g, m_a, m_w), c_type_t(m_a)) == 2);
  static_assert(find(type_list_t(m_d, m_g, m_a, m_w), c_type_t(m_w)) == 3);
}

TEST_CASE("filter-for and filter-out", "[type_list][filter_for][filter_out]")
{
  static_assert(filter_for(type_list_t(m_a, m_b, m_x, m_z), [](const auto t) {
                  return (t == m_z) || (t == m_b);
                }) == type_list_t(m_b, m_z));

  const auto p = [](const auto i) {
    return (i % 2_i == 0_i);
  };

  static_assert(
      filter_out(type_list_t(4_i, 3_i, 8_i, 10_i, 5_i), p) ==
      type_list_t(3_i, 5_i));
}

TEST_CASE("count-if", "[type_list][count_if]")
{
  static_assert(count_if(type_list_t(m_a, m_b, m_x, m_z), [](const auto t) {
                  return c_bool < std::is_same_v<decay_t<decltype(t)>, m_z_t> ||
                      std::is_same_v<decay_t<decltype(t)>, m_b_t> > ;
                }) == 2_i);

  const auto p = [](const auto i) {
    return c_bool<i.value % 2 == 0>;
  };

  static_assert(count_if(type_list_t(4_i, 3_i, 8_i, 10_i, 5_i), p) == 3_i);

  static_assert(any_of(type_list_t{4_i, 3_i, 2_i}, p));
  static_assert(!all_of(type_list_t{4_i, 3_i, 2_i}, p));
  static_assert(!none_of(type_list_t{4_i, 3_i, 2_i}, p));
}

TEST_CASE("contains", "[type_list][contains]")
{
  static_assert(
      contains(type_list_t(m_a, m_b, m_x, m_z), type_list_t(m_c, m_b)));

  static_assert(
      !contains(type_list_t(m_a, m_b, m_x, m_z), type_list_t(m_c, m_f)));

  static_assert(contains(type_list_t(m_a, m_b, m_x, m_z), type_list_t(m_x)));

  static_assert(!contains(type_list_t(m_a, m_b, m_x, m_z), type_list_t(m_y)));

  static_assert(contains(type_list_t(m_a, m_b, m_x, m_z), c_type<m_x_t>));

  static_assert(!contains(type_list_t(m_a, m_b, m_x, m_z), c_type_t{m_y}));

  static_assert(contains(type_list_t(m_a, m_b, m_x, m_z), m_x));

  static_assert(!contains(type_list_t(m_a, m_b, m_x, m_z), m_y));

  static_assert(contains(type_list<double, float>, type_list<double>));

  static_assert(contains(type_list<double, float>, c_type<double>));
}

struct type_1 {
  static constexpr int id = 1;
};
struct type_2 {
  static constexpr int id = 2;
};
struct type_3 {
  static constexpr int id = 3;
};

TEST_CASE("for-each on type-lists", "[type_list][for_each]")
{
  {
    std::ostringstream oss;

    for_each(type_list<type_1, type_3, type_1, type_2>, [&oss](const auto t) {
      oss << decltype(t)::type::id << "|";
    });

    CHECK(oss.str() == "1|3|1|2|");
  }

  {
    std::ostringstream oss;

    for_each_indexed(
        type_list<type_1, type_3, type_1, type_2>,
        [&oss](const auto e, const auto t) {
          oss << decltype(t)::type::id << "," << static_cast<int>(e.value)
              << "|";
        });

    CHECK(oss.str() == "1,0|3,1|1,2|2,3|");
  }

  {
    constexpr auto result = for_each(
        type_list_t(10_i, 2_i, 3_i, 4_i, 5_i, 8_i), [](const auto ind) {
          if constexpr (ind % 2_i == 0_i) {
            return (ind * ind);
          }
          else {
            return;
          }
        });

    static_assert(result == type_list_t(100_i, 4_i, 16_i, 64_i));
  }

  {
    constexpr auto result = for_each_indexed(
        type_list_t(10_i, 2_i, 3_i, 4_i, 5_i, 8_i),
        [](const auto ind, const auto ext) {
          if constexpr (ind >= 3_i) {
            return (ext + 5_i);
          }
          else {
            return;
          }
        });

    static_assert(result == type_list_t(9_i, 10_i, 13_i));
  }

  {
    for_each(type_list_t<>{}, [](const auto) {});
    for_each_indexed(type_list<>, [](const auto, const auto) {});
  }
}

TEST_CASE("tuple-for-each-indexed", "[tuple_for_each_indexed]")
{
  {
    std::array<std::string, 3> arr = {"Hello", "Old", "World"};

    std::ostringstream oss;

    tuple_for_each_indexed(
        [&oss](const auto ind, const auto& val) {
          oss << std::to_string(ind.value) << ':' << val << '\n';
        },
        arr);

    CHECK(
        oss.str() ==
        "0:Hello\n"
        "1:Old\n"
        "2:World\n");
  }

  {
    std::tuple<std::string, int, char> tup = {"Hello", 7, 'g'};

    std::ostringstream oss;

    tuple_for_each_indexed(
        [&oss](const auto ind, const auto& val) {
          oss << std::to_string(ind.value) << ':' << val << '\n';
        },
        tup);

    CHECK(
        oss.str() ==
        "0:Hello\n"
        "1:7\n"
        "2:g\n");
  }
}
