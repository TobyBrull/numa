#include "numa/tmpl/literals.hpp"

#include "numa/tmpl/type_traits.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("index literals", "[literals][integer]")
{
  static_assert(is_literal<c_integer_t<int, -1>>());
  static_assert(is_literal<c_index_t<8>>());
  static_assert(is_literal(c_type_t{8_i}));
  static_assert(is_literal<c_index_t<8>>());
  static_assert(is_literal<c_bool_t<true>>());
  static_assert(is_literal<c_string_t<'U', 'K'>>());
  static_assert(is_literal<c_type_t<void>>());
  static_assert(is_literal<c_type_t<int>>());

  static_assert(!is_literal(c_type<int>));
  static_assert(!is_literal<int>());

  static_assert(!is_literal<std::pair<int, c_true_t>>());
  static_assert(is_literal<std::pair<c_index_t<3>, c_true_t>>());

  static_assert(!is_literal<std::tuple<c_true_t, int>>());
  static_assert(!is_literal<std::tuple<int, c_index_t<9>>>());
  static_assert(is_literal<std::tuple<c_index_t<3>, c_true_t>>());
  static_assert(is_literal<std::pair<c_true_t, c_index_t<3>>>());

  static_assert(to_literal<int>() == c_type<int>);
  static_assert(to_literal<c_type_t<int>>() == c_type<int>);
  static_assert(to_literal<c_index_t<8>>() == c_index<8>);
  static_assert(to_literal<c_index_t<10>>() == c_index<10>);
  static_assert(to_literal<c_string_t<'g', 'o'>>() == "go"_s);
  static_assert(
      to_literal<std::pair<c_index_t<10>, c_index_t<2>>>() ==
      std::pair<c_index_t<10>, c_index_t<2>>());
  static_assert(
      to_literal<std::pair<int, c_index_t<2>>>() ==
      c_type<std::pair<int, c_index_t<2>>>);
  static_assert(
      to_literal<std::tuple<c_index_t<10>, c_index_t<2>>>() ==
      std::tuple<c_index_t<10>, c_index_t<2>>());
  static_assert(
      to_literal<std::tuple<int, c_index_t<2>>>() ==
      c_type<std::tuple<int, c_index_t<2>>>);

  static_assert(from_literal<c_type_t<int>>() == c_type<int>);
  static_assert(from_literal<c_index_t<8>>() == c_type<c_index_t<8>>);
  static_assert(from_literal<c_index_t<8>>() == c_type<c_index_t<8>>);
  static_assert(from_literal<c_string_t<'l', 's'>>() == c_type_t{"ls"_s});
  static_assert(
      from_literal<std::pair<c_true_t, c_index_t<3>>>() ==
      c_type<std::pair<c_true_t, c_index_t<3>>>);
  static_assert(
      from_literal<std::tuple<c_true_t, c_index_t<3>>>() ==
      c_type<std::tuple<c_true_t, c_index_t<3>>>);

  static_assert(is_c_null<c_null_t>());
  static_assert(is_c_null(c_type_t{c_null}));

  static_assert(c_type<decltype(10_i)> == c_type<c_index_t<10>>);
  static_assert(c_type<decltype(0_i)> == c_type<c_index_t<0>>);
  static_assert(c_type<decltype(0050_i)> == c_type<c_index_t<50>>);
  static_assert(c_type<decltype(-012_i)> == c_type<c_index_t<-12>>);
  static_assert(c_type<decltype(-7_i)> == c_type<c_index_t<-7>>);

  static_assert(10_i + 20_i == 30_i);
  static_assert(10_i - 20_i == -10_i);
  static_assert(10_i / 3_i == 3_i);
  static_assert(10_i * 3_i == 30_i);
  static_assert(10_i % 3_i == 1_i);

  static_assert(10_i == c_index<10>);
  static_assert(10_i != 11_i);
  static_assert(!(10_i != 10_i));

  static_assert(10_i < 11_i);
  static_assert(!(10_i < 10_i));

  static_assert(10_i <= 10_i);
  static_assert(!(10_i <= 9_i));

  static_assert(11_i > 10_i);
  static_assert(!(10_i > 10_i));

  static_assert(10_i >= 10_i);
  static_assert(!(9_i >= 10_i));

  static_assert(4_i + 6_i == 10_i);

  static_assert(!(0_b));

  static_assert(0_b == c_bool<false>);
  static_assert(0_b != c_bool<true>);
  static_assert(1_b == c_bool<true>);
  static_assert(1_b != c_bool<false>);

  static_assert((52_i).value == 52);
  static_assert((-7_i).value == -7);
}

TEST_CASE("bool literals", "[literals][bool]")
{
  static_assert(std::is_same_v<decltype(1_b), c_bool_t<true>>);
  static_assert(std::is_same_v<decltype(0_b), c_bool_t<false>>);

  static_assert(0_b + 0_b == 0_b);

  static_assert((0_b && 0_b) == 0_b);
  static_assert((1_b && 0_b) == 0_b);
  static_assert((0_b && 1_b) == 0_b);
  static_assert((1_b && 1_b) == 1_b);

  static_assert((0_b || 0_b) == 0_b);
  static_assert((1_b || 0_b) == 1_b);
  static_assert((0_b || 1_b) == 1_b);
  static_assert((1_b || 1_b) == 1_b);

  static_assert((!1_b) == 0_b);
  static_assert((!0_b) == 1_b);

  static_assert(1_b == "true"_b);
  static_assert("true"_b != "false"_b);
  static_assert("false"_b == "false"_b);
  static_assert("false"_b == "f"_b);
}

TEST_CASE("string literals", "[literals][string]")
{
  static_assert(
      std::is_same_v<decltype("21401"_s), c_string_t<'2', '1', '4', '0', '1'>>);
  static_assert(
      std::is_same_v<decltype("asdf"_s), c_string_t<'a', 's', 'd', 'f'>>);
  static_assert(
      std::is_same_v<decltype("-s&f"_s), c_string_t<'-', 's', '&', 'f'>>);

  static_assert(c_string_t<'O', 'M', 'G'>{} == "OMG"_s);
  static_assert(c_string<'O', 'M', 'G'> == "OMG"_s);
  static_assert(c_string_t<'O', 'M', 'P'>{} != "OMG"_s);

  static_assert("Hello"_s + " "_s + "World"_s == "Hello World"_s);

  static_assert(!("Hello"_s == "World"_s).value);
  static_assert(("Hello"_s != "World"_s).value);

  CHECK(static_cast<std::string>("Hello"_s) == "Hello");
  CHECK(static_cast<std::string>("*e!"_s) == "*e!");
}

TEST_CASE("type literals", "[literals][type]")
{
  static_assert(c_type_t<int>{} == c_type<int>);
  static_assert((c_type_t<int>{} == c_type<int>).value);
  static_assert(c_type_t<const int>{} != c_type<int>);
  static_assert(c_type_t<const int>{} != c_type<int>);
}
