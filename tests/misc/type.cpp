#include "numa/misc/type.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

using t_op = type_operation;

TEST_CASE("test traced_int", "[traced_int]")
{
  context<type_monitor> ctx;

  {
    traced_int vt;
    CHECK(vt.value() == default_value);

    {
      traced_int vt2(12);
      CHECK(vt2.value() == 12);

      vt = std::move(vt2);
      CHECK(vt2.value() == moved_from_value);
      CHECK(vt.value() == 12);
    }

    traced_int vt3(vt);
    CHECK(vt.value() == 12);
    CHECK(vt3.value() == 12);

    traced_int vt4(std::move(vt));
    CHECK(vt.value() == moved_from_value);
    CHECK(vt4.value() == 12);

    vt = vt3;
    CHECK(vt.value() == 12);
    CHECK(vt3.value() == 12);

    const auto& log    = ctx.log();
    const auto& active = ctx.active();

    REQUIRE(log.size() == 7);
    CHECK(log[0].satisfies(t_op::default_construct, hash(default_value)));
    CHECK(log[1].satisfies(t_op::construct, hash(12)));
    CHECK(log[2].satisfies(t_op::move_assign, hash(12)));
    CHECK(log[2].other == log[1].address);
    CHECK(log[3].satisfies(t_op::destruct, hash(moved_from_value)));
    CHECK(log[3].address == log[1].address);
    CHECK(log[4].satisfies(t_op::copy_construct, hash(12)));
    CHECK(log[4].other == log[0].address);
    CHECK(log[5].satisfies(t_op::move_construct, hash(12)));
    CHECK(log[5].other == log[0].address);
    CHECK(log[6].satisfies(t_op::copy_assign, hash(12)));
    CHECK(log[6].other == log[4].address);

    REQUIRE(active.size() == 3);
    CHECK(active.at(log[0].address).satisfies(t_op::copy_assign, hash(12)));
    CHECK(active.at(log[4].address).satisfies(t_op::copy_construct, hash(12)));
    CHECK(active.at(log[4].address).other == log[0].address);
    CHECK(active.at(log[5].address).satisfies(t_op::move_construct, hash(12)));
    CHECK(active.at(log[5].address).other == log[0].address);
  }

  const auto& log    = ctx.log();
  const auto& active = ctx.active();

  REQUIRE(log.size() == 10);
  CHECK(log[7].satisfies(t_op::destruct, hash(12)));
  CHECK(log[8].satisfies(t_op::destruct, hash(12)));
  CHECK(log[9].satisfies(t_op::destruct, hash(12)));

  CHECK(active.size() == 0);
}

TEST_CASE("test traced_non_copy", "[traced_non_copy]")
{
  context<type_monitor> ctx;

  {
    traced_non_copy vt;
    CHECK(vt.value() == default_value);

    {
      traced_non_copy vt2(12);
      CHECK(vt2.value() == 12);

      vt = std::move(vt2);
      CHECK(vt2.value() == moved_from_value);
      CHECK(vt.value() == 12);
    }

    traced_non_copy vt4(std::move(vt));
    CHECK(vt.value() == moved_from_value);
    CHECK(vt4.value() == 12);

    const auto& log    = ctx.log();
    const auto& active = ctx.active();

    REQUIRE(log.size() == 5);
    CHECK(log[0].satisfies(t_op::default_construct, hash(default_value)));
    CHECK(log[1].satisfies(t_op::construct, hash(12)));
    CHECK(log[2].satisfies(t_op::move_assign, hash(12)));
    CHECK(log[2].other == log[1].address);
    CHECK(log[2].address == log[0].address);
    CHECK(log[3].satisfies(t_op::destruct, hash(moved_from_value)));
    CHECK(log[3].address == log[1].address);
    CHECK(log[4].satisfies(t_op::move_construct, hash(12)));
    CHECK(log[4].other == log[0].address);

    REQUIRE(active.size() == 2);
    CHECK(active.at(log[0].address).satisfies(t_op::move_assign, hash(12)));
    CHECK(active.at(log[4].address).satisfies(t_op::move_construct, hash(12)));
    CHECK(active.at(log[4].address).other == log[0].address);
  }

  const auto& log    = ctx.log();
  const auto& active = ctx.active();

  REQUIRE(log.size() == 7);
  CHECK(log[5].satisfies(t_op::destruct, hash(12)));
  CHECK(log[5].address == log[4].address);
  CHECK(log[5].other == nullptr);
  CHECK(log[6].satisfies(t_op::destruct, hash(moved_from_value)));
  CHECK(log[6].address == log[0].address);
  CHECK(log[6].other == nullptr);

  CHECK(active.size() == 0);
}

TEST_CASE(
    "non-copy-type in vector", "[std_vector][traced_int][traced_non_copy]")
{
  context<type_monitor> ctx;

  static_assert(std::is_nothrow_move_constructible_v<non_copy_int>);
  static_assert(std::is_move_constructible_v<non_copy_int>);
  static_assert(std::is_nothrow_move_assignable_v<non_copy_int>);
  static_assert(std::is_move_assignable_v<non_copy_int>);

  static_assert(!std::is_nothrow_copy_constructible_v<non_copy_int>);
  static_assert(!std::is_copy_constructible_v<non_copy_int>);
  static_assert(!std::is_nothrow_copy_assignable_v<non_copy_int>);
  static_assert(!std::is_copy_assignable_v<non_copy_int>);

  static_assert(
      std::is_nothrow_move_constructible_v<class_wrapper<non_copy_int>>);
  static_assert(std::is_move_constructible_v<class_wrapper<non_copy_int>>);
  static_assert(std::is_nothrow_move_assignable_v<class_wrapper<non_copy_int>>);
  static_assert(std::is_move_assignable_v<class_wrapper<non_copy_int>>);

  static_assert(
      !std::is_nothrow_copy_constructible_v<class_wrapper<non_copy_int>>);
  static_assert(!std::is_copy_constructible_v<class_wrapper<non_copy_int>>);
  static_assert(
      !std::is_nothrow_copy_assignable_v<class_wrapper<non_copy_int>>);
  static_assert(!std::is_copy_assignable_v<class_wrapper<non_copy_int>>);

  static_assert(!std::is_nothrow_move_constructible_v<traced_non_copy>);
  static_assert(std::is_move_constructible_v<traced_non_copy>);
  static_assert(!std::is_nothrow_move_assignable_v<traced_non_copy>);
  static_assert(std::is_move_assignable_v<traced_non_copy>);

  static_assert(!std::is_nothrow_copy_constructible_v<traced_non_copy>);
  static_assert(!std::is_copy_constructible_v<traced_non_copy>);
  static_assert(!std::is_nothrow_copy_assignable_v<traced_non_copy>);
  static_assert(!std::is_copy_assignable_v<traced_non_copy>);

  {
    std::vector<traced_non_copy> data_1;
    data_1.emplace_back(1);
    data_1.emplace_back(2);
    data_1.emplace_back(3);
  }

  static_assert(std::is_nothrow_move_constructible_v<value_int>);
  static_assert(std::is_move_constructible_v<value_int>);
  static_assert(std::is_nothrow_move_assignable_v<value_int>);
  static_assert(std::is_move_assignable_v<value_int>);

  static_assert(std::is_nothrow_copy_constructible_v<value_int>);
  static_assert(std::is_copy_constructible_v<value_int>);
  static_assert(std::is_nothrow_copy_assignable_v<value_int>);
  static_assert(std::is_copy_assignable_v<value_int>);

  static_assert(std::is_nothrow_move_constructible_v<class_wrapper<value_int>>);
  static_assert(std::is_move_constructible_v<class_wrapper<value_int>>);
  static_assert(std::is_nothrow_move_assignable_v<class_wrapper<value_int>>);
  static_assert(std::is_move_assignable_v<class_wrapper<value_int>>);

  static_assert(std::is_nothrow_copy_constructible_v<class_wrapper<value_int>>);
  static_assert(std::is_copy_constructible_v<class_wrapper<value_int>>);
  static_assert(std::is_nothrow_copy_assignable_v<class_wrapper<value_int>>);
  static_assert(std::is_copy_assignable_v<class_wrapper<value_int>>);

  static_assert(!std::is_nothrow_move_constructible_v<traced_int>);
  static_assert(std::is_move_constructible_v<traced_int>);
  static_assert(!std::is_nothrow_move_assignable_v<traced_int>);
  static_assert(std::is_move_assignable_v<traced_int>);

  static_assert(!std::is_nothrow_copy_constructible_v<traced_int>);
  static_assert(std::is_copy_constructible_v<traced_int>);
  static_assert(!std::is_nothrow_copy_assignable_v<traced_int>);
  static_assert(std::is_copy_assignable_v<traced_int>);

  {
    std::vector<traced_int> data_1;
    data_1.emplace_back(1);
    data_1.emplace_back(2);
    data_1.emplace_back(3);
  }
}

TEST_CASE("test throwing", "[traced_int][throw]")
{
  context<type_monitor> ctx;

  {
    traced_int vt;
    traced_int vt2(12);
    traced_int vt3(vt);
    traced_int vt4(std::move(vt));
    vt = vt4;
    vt = std::move(vt4);

    {
      context<type_monitor> inner_ctx(uniform_type_throw_config(1.0));

      CHECK_THROWS(traced_int());
      CHECK_THROWS(traced_int(12));
      CHECK_THROWS(traced_int(vt));
      CHECK_THROWS(traced_int(std::move(vt)));
      CHECK_THROWS(vt = vt3);
      CHECK_THROWS(vt = std::move(vt3));

      CHECK(inner_ctx.log().size() == 0);
      CHECK(inner_ctx.active().size() == 0);
    }

    CHECK(ctx.log().size() == 6);
    CHECK(ctx.active().size() == 4);
  }

  CHECK(ctx.log().size() == 10);
  CHECK(ctx.active().size() == 0);
}
