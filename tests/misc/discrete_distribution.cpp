#include "numa/misc/discrete_distribution.hpp"

#include "numa/misc/random_sample.hpp"

#include "catch2/catch.hpp"

#include <iostream>

using namespace numa;
using namespace numa::misc;

TEST_CASE("test discrete_distribution", "[discrete_distribution]")
{
  discrete_distribution<char> dd;
  dd.add('a', 1.0);
  dd.add('c', 0.01);
  dd.add('e', 0.1);
  dd.add('b', 1.0);
  dd.add('d', 0.01);
  dd.add('f', 0.1);

  CHECK(dd.total_weight() == Approx(2.22));
  CHECK(dd.size() == 6);

  CHECK(dd.weight('a') == Approx(1.0));
  CHECK(dd.weight('b') == Approx(1.0));
  CHECK(dd.weight('c') == Approx(0.01));
  CHECK(dd.weight('d') == Approx(0.01));
  CHECK(dd.weight('e') == Approx(0.1));
  CHECK(dd.weight('f') == Approx(0.1));
  CHECK(dd.weight('g') == 0.0);

  const auto weights        = dd.all_weights();
  const auto weights_finder = [&weights](const char c) {
    const auto it =
        find_if(weights, [&](const auto& p) { return p.first == c; });
    REQUIRE(it != weights.end());
    CHECK(it->first == c);
    return it->second;
  };

  CHECK(weights_finder('a') == Approx(1.0));
  CHECK(weights_finder('b') == Approx(1.0));
  CHECK(weights_finder('c') == Approx(0.01));
  CHECK(weights_finder('d') == Approx(0.01));
  CHECK(weights_finder('e') == Approx(0.1));
  CHECK(weights_finder('f') == Approx(0.1));

  const auto probs        = dd.all_probabilities();
  const auto probs_finder = [&probs](const char c) {
    const auto it = find_if(probs, [&](const auto& p) { return p.first == c; });
    REQUIRE(it != probs.end());
    CHECK(it->first == c);
    return it->second;
  };

  CHECK(probs_finder('a') == Approx(1.0 / 2.22));
  CHECK(probs_finder('b') == Approx(1.0 / 2.22));
  CHECK(probs_finder('c') == Approx(0.01 / 2.22));
  CHECK(probs_finder('d') == Approx(0.01 / 2.22));
  CHECK(probs_finder('e') == Approx(0.1 / 2.22));
  CHECK(probs_finder('f') == Approx(0.1 / 2.22));

  std::default_random_engine eng(0);

  for (int i = 0; i < 10; ++i) {
    const auto val = dd(eng);

    CHECK('a' <= val);
    CHECK(val <= 'f');
  }

  dd.clear();

  CHECK(dd.total_weight() == 0.0);
  CHECK(dd.size() == 0);
  CHECK(dd.weight('a') == 0.0);
  CHECK(dd.all_weights().empty());
  CHECK(dd.all_probabilities().empty());
}

namespace {
  enum class test_enum { value_1, value_2 };
}

TEST_CASE(
    "test distance between discrete-distributions",
    "[discrete_distribution][dist]")
{
  discrete_distribution<int> dd_1;
  discrete_distribution<int> dd_2;

  dd_1.add(2, 0.1 * 11.1);
  dd_1.add(7, 0.1 * 11.1);
  dd_1.add(8, 0.4 * 11.1);
  dd_1.add(10, 0.4 * 11.1);

  dd_2.add(7, 0.1 * 2.2);
  dd_2.add(8, 0.3 * 2.2);
  dd_2.add(10, 0.5 * 2.2);
  dd_2.add(11, 0.1 * 2.2);

  CHECK(dist(dd_1, dd_2) == Approx(0.4));
}

TEST_CASE(
    "test discrete-distribution with enum", "[discrete_distribution][enum]")
{
  discrete_distribution<test_enum> dd;

  dd.add(test_enum::value_1, 11.0);
  dd.add(test_enum::value_2, 4.0);

  CHECK(dd.weight(test_enum::value_1) == Approx(11.0));
  CHECK(dd.weight(test_enum::value_2) == Approx(4.0));

  std::default_random_engine eng(0);

  for (int i = 0; i < 10; ++i) {
    const auto val = dd(eng);

    const bool cond =
        (val == test_enum::value_1) || (val == test_enum::value_2);
    CHECK(cond);
  }
}

TEST_CASE("test", "[.][random][extensive][discrete_distribution]")
{
  std::cout << "Random-test: convergence of discrete-distribution."
            << std::endl;

  discrete_distribution<char> dd;
  dd.add('a', 1.0);
  dd.add('b', 0.5);
  dd.add('c', 0.3);
  dd.add('d', 0.2);

  discrete_random_sample<char> rs;

  std::default_random_engine eng(0);

  index_t count = 0;
  while (true) {
    rs(dd(eng));

    if ((count % 100) == 0) {
      const auto obs_dd = rs.to_distribution();

      if (dist(dd, obs_dd) < 0.001) {
        break;
      }
    }

    ++count;
  }

  CHECK(true);
}
