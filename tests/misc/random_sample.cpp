#include "numa/misc/random_sample.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

namespace {
  enum class test_enum { value_1, value_2, value_3 };
}

TEST_CASE("test random_sample with enum", "[discrete_distribution][enum]")
{
  random_sample<test_enum> rs;

  rs(test_enum::value_1);
  rs(test_enum::value_2);
  rs(test_enum::value_2);
  rs(test_enum::value_1);
  rs(test_enum::value_2);

  CHECK(rs.total_count() == 5);
  CHECK(rs.count(test_enum::value_1) == 2);
  CHECK(rs.count(test_enum::value_2) == 3);
  CHECK(rs.count(test_enum::value_3) == 0);

  CHECK(rs.all_counts().size() == 2);
  CHECK(rs.all_counts().at(test_enum::value_1) == 2);
  CHECK(rs.all_counts().at(test_enum::value_2) == 3);

  rs.clear();

  CHECK(rs.total_count() == 0);
  CHECK(rs.count(test_enum::value_1) == 0);
  CHECK(rs.count(test_enum::value_2) == 0);
  CHECK(rs.count(test_enum::value_3) == 0);
  CHECK(rs.all_counts().empty());
}
