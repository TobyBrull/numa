#include "numa/misc/benchmark.hpp"

#include "numa/misc/candidates.hpp"

#include "numa/lina/cpu_target.hpp"

#include "numa/cell/reform.hpp"
#include "numa/cell/serialize_json.hpp"

#include "numa/core/functors.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

TEST_CASE("benchmark_context", "[benchmark_context][benchmark]")
{
  {
    benchmark_context bc;

    {
      auto t = benchmark_context::timer();
    }

    CHECK(bc.duration().count() >= 0.0);
  }

  {
    benchmark_context bc;

    CHECK(std::isnan(bc.duration().count()));

    benchmark_duration dur = benchmark_duration::zero();

    {
      auto t0 = benchmark_context::timer();

      {
        auto t1 = benchmark_context::timer();
      }

      dur = bc.duration();
    }

    CHECK(bc.duration() == dur);
  }

  {
    benchmark_context bc;

    {
      benchmark_context::notify(benchmark_duration{3.14});

      auto t0 = benchmark_context::timer();

      {
        auto t1 = benchmark_context::timer();
      }
    }

    CHECK(bc.duration() == benchmark_duration{3.14});
  }
}

TEST_CASE("basic benchmark", "[benchmark]")
{
  benchmark_setup<int, int, std::string> setup;

  emplace_back(
      setup.vary_axes(),
      init<m_trial_t>(1, 2, "Moep"),
      init<m_effort_t>(3.5),
      init<m_mandatory_t>(false));

  emplace_back(
      setup.vary_axes(),
      init<m_trial_t>(4, 3, "Blub"),
      init<m_effort_t>(1.5),
      init<m_mandatory_t>(true));

  emplace_back(
      setup.vary_axes(),
      init<m_trial_t>(5, 5, "Boom"),
      init<m_effort_t>(1.1),
      init<m_mandatory_t>(false));

  int i_1 = 0;
  std::string str_1;
  auto const candidate_1 = candidate(
      "candi_1",
      [&i_1, &str_1](matrix<int> const& mat, std::string const& str) {
        i_1 += mat.at(0, 0) + 10'000;
        str_1 += "c1: " + str + "; ";

        return mat.at(0, 0);
      });

  int i_2 = 0;
  std::string str_2;
  auto const candidate_2 = candidate(
      "candi_2",
      [&i_2, &str_2](matrix<int> const& mat, std::string const& str) {
        i_2 += mat.at(0, 0) + 10'000;
        str_2 += "c2: " + str + "; ";

        return mat.at(0, 0);
      });

  SECTION("both candidates")
  {
    benchmark_axis<identifier> candidates_axis({"candi_1", "candi_2"});

    auto const result = benchmark(
        std::move(setup),
        [](int const n, int const m, std::string const& s) {
          return std::tuple{make_cell<matrix<int>>(n, m)(f_const_t(n)), s};
        },
        std::move(candidates_axis),
        f_is_equal)(candidate_1, candidate_2);

    CHECK(i_1 == 60'020);
    CHECK(
        str_1 ==
        "c1: Blub; c1: Blub; c1: Boom; c1: Boom; c1: Moep; c1: Moep; ");
    CHECK(i_2 == 60'020);
    CHECK(
        str_2 ==
        "c2: Blub; c2: Blub; c2: Boom; c2: Boom; c2: Moep; c2: Moep; ");

    CHECK(all_of(
        f_is_greater_equal_to_t{benchmark_duration::zero()},
        result.get(m_run_time)));

    CHECK(result.get(m_candidate) == make_from_json<vector<std::string>>(R"({
          "size": [2],
          "values":
            ["candi_1", "candi_2"]
        })"));

    CHECK(
        result.get(m_trial) ==
        make_from_json<vector<std::tuple<int, int, std::string>>>(R"({
          "size": [3],
          "values":
            [[ 4, 3, "Blub"], [ 5, 5, "Boom"], [ 1, 2, "Moep"]]
        })"));
  }
  SECTION("both candidates")
  {
    benchmark_axis<identifier> candidates_axis(
        {"candi_2", "candi_1", "candi_2"});

    auto const result = benchmark(
        std::move(setup),
        [](int const n, int const m, std::string const& s) {
          return std::tuple{make_cell<matrix<int>>(n, m)(f_const_t(n)), s};
        },
        std::move(candidates_axis),
        f_is_equal)(candidate_1, candidate_2);

    CHECK(i_1 == 60'020);
    CHECK(
        str_1 ==
        "c1: Blub; c1: Blub; c1: Boom; c1: Boom; c1: Moep; c1: Moep; ");
    CHECK(i_2 == 120'040);
    CHECK(
        str_2 ==
        "c2: Blub; c2: Blub; c2: Blub; c2: Blub; "
        "c2: Boom; c2: Boom; c2: Boom; c2: Boom; "
        "c2: Moep; c2: Moep; c2: Moep; c2: Moep; ");

    CHECK(all_of(
        f_is_greater_equal_to_t{benchmark_duration::zero()},
        result.get(m_run_time)));

    CHECK(result.get(m_candidate) == make_from_json<vector<std::string>>(R"({
          "size": [3],
          "values":
            ["candi_2", "candi_1", "candi_2"]
        })"));

    CHECK(
        result.get(m_trial) ==
        make_from_json<vector<std::tuple<int, int, std::string>>>(R"({
          "size": [3],
          "values":
            [[ 4, 3, "Blub"], [ 5, 5, "Boom"], [ 1, 2, "Moep"]]
        })"));
  }
}

TEST_CASE("benchmark_axis", "[benchmark_axis]")
{
  {
    auto const ba = benchmark_axis<int>(
        {1, 2, 4, 10}, {1, 4, 16, 100}, {true, true, false, true});

    CHECK(ba == make_from_json<benchmark_setup<int>>(R"({
        "size": [4],
        "trial": { "size": [4], "values": [[1], [2], [4], [10]] },
        "effort": { "size": [4], "values": [1, 4, 16, 100] },
        "mandatory": { "size": [4], "values": [1, 1, 0, 1] }
    })"));
  }

  {
    auto const ba = benchmark_axis<int>({1, 2, 4, 10}, {1, 4, 16, 20}, 1);

    CHECK(ba == make_from_json<benchmark_setup<int>>(R"({
        "size": [4],
        "trial": { "size": [4], "values": [[1], [2], [4], [10]] },
        "effort": { "size": [4], "values": [1, 4, 16, 20] },
        "mandatory": { "size": [4], "values": [1, 0, 0, 0] }
    })"));
  }

  {
    auto const ba = benchmark_axis<int>({1, 2, 4, 10}, f_pow(x, 2), 3);

    CHECK(ba == make_from_json<benchmark_setup<int>>(R"({
        "size": [4],
        "trial": { "size": [4], "values": [[1], [2], [4], [10]] },
        "effort": { "size": [4], "values": [1, 4, 16, 100] },
        "mandatory": { "size": [4], "values": [1, 1, 1, 0] }
    })"));
  }

  {
    auto const ba = benchmark_axis<int>({1, 2, 4, 10});

    CHECK(ba == make_from_json<benchmark_setup<int>>(R"({
        "size": [4],
        "trial": { "size": [4], "values": [[1], [2], [4], [10]] },
        "effort": { "size": [4], "values": [1, 1, 1, 1] },
        "mandatory": { "size": [4], "values": [1, 1, 1, 1] }
    })"));
  }

  {
    auto const ba =
        benchmark_axis<int>::parse("[1, 2, 4][10, 30]", f_pow(x, 2));

    CHECK(ba == make_from_json<benchmark_setup<int>>(R"({
        "size": [5],
        "trial": { "size": [5], "values": [[1], [2], [4], [10], [30]] },
        "effort": { "size": [5], "values": [1, 4, 16, 100, 900] },
        "mandatory": { "size": [5], "values": [1, 1, 1, 0, 0] }
    })"));
  }

  {
    auto const ba =
        benchmark_axis<int>::parse("[1, 2, 4, 10, 30]", f_pow(x, 2));

    CHECK(ba == make_from_json<benchmark_setup<int>>(R"({
        "size": [5],
        "trial": { "size": [5], "values": [[1], [2], [4], [10], [30]] },
        "effort": { "size": [5], "values": [1, 4, 16, 100, 900] },
        "mandatory": { "size": [5], "values": [1, 1, 1, 1, 1] }
    })"));
  }

  {
    auto const ba = benchmark_axis<int>::parse("100", f_pow(x, 2));

    CHECK(ba == make_from_json<benchmark_setup<int>>(R"({
        "size": [1],
        "trial": { "size": [1], "values": [[100]] },
        "effort": { "size": [1], "values": [10000] },
        "mandatory": { "size": [1], "values": [1] }
    })"));
  }

  {
    auto const ba =
        benchmark_axis<cpu_target>::parse("avx", f_effort_cpu_target());

    CHECK(ba == make_from_json<benchmark_setup<cpu_target>>(R"({
        "size": [1],
        "trial": { "size": [1], "values": [["avx"]] },
        "effort": { "size": [1], "values": [0.5] },
        "mandatory": { "size": [1], "values": [1] }
    })"));
  }

  {
    auto const ba = benchmark_axis<cpu_target>::parse(
        "[avx, sse4_2]", f_effort_cpu_target());

    CHECK(ba == make_from_json<benchmark_setup<cpu_target>>(R"({
        "size": [2],
        "trial": { "size": [2], "values": [["avx"], ["sse4_2"]] },
        "effort": { "size": [2], "values": [0.5, 1.0] },
        "mandatory": { "size": [2], "values": [1, 1] }
    })"));
  }

  {
    auto const ba = benchmark_axis<cpu_target>::parse(
        "[avx, sse4_2][avx2]", f_effort_cpu_target());

    CHECK(ba == make_from_json<benchmark_setup<cpu_target>>(R"({
        "size": [3],
        "trial": { "size": [3], "values": [["avx"], ["sse4_2"], ["avx2"]] },
        "effort": { "size": [3], "values": [0.5, 1.0, 0.25] },
        "mandatory": { "size": [3], "values": [1, 1, 0] }
    })"));
  }
}

TEST_CASE("make_benchmark_setup", "[make_benchmark_setup][benchmark_setup]")
{
  {
    auto const setup = make_benchmark_setup(
        benchmark_axis<index_t>({1, 2, 4, 8}, f_pow(x, 2), 3),
        benchmark_axis<cpu_target>(
            {c_sse4_2, c_avx}, {1.0, 0.5}, {false, true}),
        benchmark_axis<index_t>(
            {1, 2, 4}, [](auto const nt) { return 1.0 / nt; }, 2));

    CHECK(
        setup ==
        make_from_json<benchmark_setup<index_t, cpu_target, index_t>>(R"({
        "size": [24],
        "trial": { "size": [24], "values": [
            [1, "sse4_2", 1],
            [1, "sse4_2", 2],
            [1, "sse4_2", 4],
            [1, "avx", 1],
            [1, "avx", 2],
            [1, "avx", 4],
            [2, "sse4_2", 1],
            [2, "sse4_2", 2],
            [2, "sse4_2", 4],
            [2, "avx", 1],
            [2, "avx", 2],
            [2, "avx", 4],
            [4, "sse4_2", 1],
            [4, "sse4_2", 2],
            [4, "sse4_2", 4],
            [4, "avx", 1],
            [4, "avx", 2],
            [4, "avx", 4],
            [8, "sse4_2", 1],
            [8, "sse4_2", 2],
            [8, "sse4_2", 4],
            [8, "avx", 1],
            [8, "avx", 2],
            [8, "avx", 4]
        ] },
        "effort": { "size": [24], "values": [
            1, 0.5, 0.25, 0.5, 0.25, 0.125,
            4, 2, 1, 2, 1, 0.5,
            16, 8, 4, 8, 4, 2,
            64, 32, 16, 32, 16, 8
        ] },
        "mandatory": { "size": [24], "values": [
            0, 0, 0, 1, 1, 0,
            0, 0, 0, 1, 1, 0,
            0, 0, 0, 1, 1, 0,
            0, 0, 0, 0, 0, 0
        ] }
    })"));
  }
}
