#include "numa/cell/tensor_fixed_size.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/reform.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/t_span.hpp"
#include "numa/cell/tensor.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("fixed-size-tensor concepts", "[tensor_fixed_size][concepts]")
{
  static_assert(Cell<matrix_fixed_size<double, 2, 3>>);

  static_assert(is_same_template<
                tensor_fixed_size<double, 2, 3>,
                tensor_fixed_size<double, 2, 3>>());
}

TEST_CASE("fixed-size-tensor basic", "[tensor_fixed_size][basic]")
{
  matrix_fixed_size<double, 2, 3> mf;

  mf(0, 1) = 1;
  mf(1, 0) = 1;

  CHECK(
      mf ==
      make_from_json<matrix_fixed_size<double, 2, 3>>("{ \"size\": [2, 3],"
                                                      "  \"values\":"
                                                      "[[0, 1],"
                                                      " [1, 0],"
                                                      " [0, 0]"
                                                      "] }"));

  static_assert(mf.size() == index(2, 3));
  static_assert(mf.capacity() == index(2, 3));
  static_assert(mf.required_elements({2, 3}) == 0);

  SECTION("copy c-tor")
  {
    const auto cp = mf;

    CHECK(cp == mf);
  }
  SECTION("move c-tor")
  {
    const auto cp = std::move(mf);

    CHECK(cp == mf);
  }
  SECTION("copy assignment")
  {
    matrix_fixed_size<double, 2, 3> cp;

    cp = mf;

    CHECK(cp == mf);
  }
  SECTION("move assignment")
  {
    matrix_fixed_size<double, 2, 3> cp;

    cp = std::move(mf);

    CHECK(cp == mf);
  }
}

TEST_CASE("fixed-size-tensor make-cell", "[tensor_fixed_size][make_cell]")
{
  const auto mat = make_cell<matrix_fixed_size<int, 2, 3>>(2, 3)();

  CHECK(
      mat.span() ==
      make_from_json<matrix<int>>(
          R"({"size": [2, 3], "values": [[0, 0], [0, 0], [0, 0]]})")
          .span());
}

TEST_CASE(
    "fixed-size-tensor serialize-json", "[tensor_fixed_size][serialize_json]")
{
  vector_fixed_size<int, 3> vec(2);

  CHECK(
      to_json(vec) ==
      "{\n"
      "  \"size\": [3],\n"
      "  \"values\":\n"
      "    [2, 2, 2]\n"
      "}");

  {
    from_json(R"({ "size": [3], "values": [10, 15, 6] })", vec);

    CHECK(vec.at(0) == 10);
    CHECK(vec.at(1) == 15);
    CHECK(vec.at(2) == 6);
  }

  {
    from_json(R"({ "size": [3], "values": [10, 15, 6] })", vec.unvaried());

    CHECK(vec.at(0) == 10);
    CHECK(vec.at(1) == 15);
    CHECK(vec.at(2) == 6);
  }

  const auto mat = make_from_json<matrix_fixed_size<int, 2, 3>>(
      R"({ "size": [2, 3],
           "values": [
             [6, 7],
             [10, 11],
             [1, 1]
           ] } )");

  CHECK(mat.at(0, 0) == 6);
  CHECK(mat.at(1, 0) == 7);
  CHECK(mat.at(0, 1) == 10);
  CHECK(mat.at(1, 1) == 11);
  CHECK(mat.at(0, 2) == 1);
  CHECK(mat.at(1, 2) == 1);
}
