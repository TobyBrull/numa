#include "numa/cell/tensor_fixed_capacity.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/reform.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/t_span.hpp"
#include "numa/cell/tensor.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("fixed-capacity-tensor concepts", "[tensor_fixed_capacity][concepts]")
{
  static_assert(Cell<matrix_fixed_capacity<double, 2, 3>>);

  static_assert(is_same_template<
                tensor_fixed_capacity<double, 2, 3>,
                tensor_fixed_capacity<double, 2, 3>>());
}

TEST_CASE("fixed-capacity-tensor", "[tensor_fixed_capacity][basic]")
{
  matrix_fixed_capacity<double, 2, 3> mf;

  CHECK(mf.size() == index(0, 0));

  resize(mf.vary_axes(), {1, 2}, 1.1);

  CHECK(mf.size() == index(1, 2));
  static_assert(mf.capacity() == index(2, 3));
  static_assert(mf.required_elements({1, 2}) == 0);

  mf.at(0, 1) = 2.2;

  CHECK_THROWS(resize_hard(mf.vary_axes(), {2, 2}));
  CHECK_THROWS(reserve_hard(mf.vary_axes(), {7, 4}));

  reserve_hard(mf.vary_axes(), {2, 3});

  CHECK(mf.size() == index(1, 2));
  CHECK(mf.capacity() == index(2, 3));

  CHECK_THROWS(insert_size(mf.vary_axes(1_i, 0_i), {1, 0}, {2, 0}, 0.5));

  insert_size(mf.vary_axes(1_i, 0_i), {1, 0}, {1, 1}, 0.5);

  CHECK(
      mf ==
      make_from_json<matrix_fixed_capacity<double, 2, 3>>("{ \"size\": [2, 3],"
                                                          "  \"values\":"
                                                          "[[0.5, 1.1],"
                                                          " [0.5, 0.5],"
                                                          " [0.5, 2.2]"
                                                          "] }"));

  erase_size(mf.vary_axes(), zero, one);

  CHECK(
      mf.span() ==
      make_from_json<matrix<double>>("{ \"size\": [1, 2],"
                                     "  \"values\":"
                                     "[[0.5],"
                                     " [2.2]"
                                     "] }")
          .span());

  SECTION("copy c-tor")
  {
    const auto cp = mf;

    CHECK(cp == mf);
  }
  SECTION("move c-tor")
  {
    const auto cp = std::move(mf);

    CHECK(cp == mf);
  }
  SECTION("copy assignment")
  {
    matrix_fixed_capacity<double, 2, 3> cp;

    cp = mf;

    CHECK(cp == mf);
  }
  SECTION("move assignment")
  {
    matrix_fixed_capacity<double, 2, 3> cp;

    cp = std::move(mf);

    CHECK(cp == mf);
  }
}

TEST_CASE(
    "fixed-capacity-tensor make-cell", "[tensor_fixed_capacity][make_cell]")
{
  const auto mat = make_cell<matrix_fixed_capacity<int, 3, 5>>(2, 3)();

  CHECK(
      mat ==
      make_from_json<matrix_fixed_capacity<int, 3, 5>>(
          R"({"size": [2, 3], "values": [[0, 0], [0, 0], [0, 0]]})"));
}

TEST_CASE(
    "fixed-capacity-tensor serialize-json",
    "[tensor_fixed_capacity][serialize_json]")
{
  vector_fixed_capacity<int, 3> vec;

  resize(vec.vary_axis(), 3, 2);

  CHECK(
      to_json(vec) ==
      "{\n"
      "  \"size\": [3],\n"
      "  \"values\":\n"
      "    [2, 2, 2]\n"
      "}");

  {
    from_json(R"({ "size": [3], "values": [10, 15, 6] })", vec);

    CHECK(vec.at(0) == 10);
    CHECK(vec.at(1) == 15);
    CHECK(vec.at(2) == 6);
  }

  {
    from_json(R"({ "size": [3], "values": [10, 15, 6] })", vec.unvaried());

    CHECK(vec.at(0) == 10);
    CHECK(vec.at(1) == 15);
    CHECK(vec.at(2) == 6);
  }

  const auto mat = make_from_json<matrix_fixed_capacity<int, 2, 5>>(
      R"({ "size": [2, 3],
           "values": [
             [6, 7],
             [10, 11],
             [1, 1]
           ] } )");

  CHECK(mat.at(0, 0) == 6);
  CHECK(mat.at(1, 0) == 7);
  CHECK(mat.at(0, 1) == 10);
  CHECK(mat.at(1, 1) == 11);
  CHECK(mat.at(0, 2) == 1);
  CHECK(mat.at(1, 2) == 1);
}
