#include "numa/cell/t_span_adaptor_band.hpp"

#include "numa/cell/reform.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/t_span_adaptor_triangular.hpp"
#include "numa/cell/tensor.hpp"
#include "numa/cell/tensor_staged.hpp"

#include "numa/core/functors.hpp"
#include "numa/core/generators.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("t_span_adaptor_band concepts", "[t_span_adaptor][concepts]")
{
  static_assert(Span<t_span_adaptor_band<
                    matrix_span<double>,
                    index_list_t<2>,
                    index_list_t<1>>>);
}

TEST_CASE("basic t-span-adaptor-band", "[t_span_adaptor_band]")
{
  auto A = make_cell<matrix<double>>(4, 3)(f_const_t{3.0});
  auto B = make_cell<matrix<double>>(4, 3)(g_iota(0.5));

  SECTION("case 1")
  {
    for_each(diagonal(A), B)(f_plus);

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [4, 3],"
                                       "  \"values\":"
                                       "[[3.5, 3.0, 3.0, 3.0],"
                                       " [3.0, 8.5, 3.0, 3.0],"
                                       " [3.0, 3.0, 13.5, 3.0]"
                                       "] }"));
  }
  SECTION("case 2")
  {
    diagonal(A) += band<1>(B);

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [4, 3],"
                                       "  \"values\":"
                                       "[[3.5, 3.0, 3.0, 3.0],"
                                       " [3.0, 8.5, 3.0, 3.0],"
                                       " [3.0, 3.0, 13.5, 3.0]"
                                       "] }"));
  }
  SECTION("case 3")
  {
    band(A, 1_i) += B;

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [4, 3],"
                                       "  \"values\":"
                                       "[[3.5, 4.5, 3.0, 3.0],"
                                       " [7.5, 8.5, 9.5, 3.0],"
                                       " [3.0, 12.5, 13.5, 14.5]"
                                       "] }"));
  }
  SECTION("case 4")
  {
    band(A, index_list<2>, index_list<1>) += band(B, 2_i);

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [4, 3],"
                                       "  \"values\":"
                                       "[[3.5, 4.5, 5.5, 3.0],"
                                       " [7.5, 8.5, 9.5, 10.5],"
                                       " [3.0, 12.5, 13.5, 14.5]"
                                       "] }"));
  }
  SECTION("single")
  {
    resize(A.vary_axes(), {4, 4}, 4.0);

    for_each(band_lower_strictly(A, 2_i))(f_const_t{7.0});

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [4, 4],"
                                       "  \"values\":"
                                       "[[ 3, 7, 7, 3],"
                                       " [ 3, 3, 7, 7],"
                                       " [ 3, 3, 3, 7],"
                                       " [ 4, 4, 4, 4]"
                                       "] }"));
  }
  SECTION("single again")
  {
    resize(A.vary_axes(), {4, 4}, 4.0);

    for_each(band_lower(A, 2_i))(f_const_t{7.0});

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [4, 4],"
                                       "  \"values\":"
                                       "[[ 7, 7, 7, 3],"
                                       " [ 3, 7, 7, 7],"
                                       " [ 3, 3, 7, 7],"
                                       " [ 4, 4, 4, 7]"
                                       "] }"));
  }
}

TEST_CASE(
    "t_span_adaptor_band on staged",
    "[t_span_adaptor_band][staged]"
    "[upper][lower]")
{
  auto A = make_cell<matrix_staged<double, 1>>(4)(g_iota(1.5));
  auto B = make_cell<matrix_staged<double, 2>>(4)(g_iota(100.0));

  SECTION("both lhs and rhs")
  {
    for_each(diagonal(A), upper(B))(f_plus);

    CHECK(
        A ==
        make_from_json<matrix_staged<double, 1>>("{ \"size\": [4],"
                                                 "  \"values\":"
                                                 "[[1.5, 104.5, 3.5],"
                                                 " [4.5, 112.5, 6.5],"
                                                 " [7.5, 120.5, 9.5],"
                                                 " [10.5, 128.5, 12.5]"
                                                 "] }"));
  }
  SECTION("only lhs")
  {
    for_each(band(A, 1_i), B)([](double const x, double& y) { y += x; });

    CHECK(
        B ==
        make_from_json<matrix_staged<double, 2>>(
            "{ \"size\": [4],"
            "  \"values\":"
            "[[100, 101, 104.5, 106.5, 104],"
            " [105, 110.5, 112.5, 114.5, 109],"
            " [110, 118.5, 120.5, 122.5, 114],"
            " [115, 126.5, 128.5, 118, 119]"
            "] }"));
  }
  SECTION("only lhs")
  {
    for_each(band_upper(B, 1_i), A)(f_plus);

    CHECK(
        B ==
        make_from_json<matrix_staged<double, 2>>(
            "{ \"size\": [4],"
            "  \"values\":"
            "[[100, 101, 104.5, 103, 104],"
            " [105, 110.5, 112.5, 108, 109],"
            " [110, 118.5, 120.5, 113, 114],"
            " [115, 126.5, 128.5, 118, 119]"
            "] }"));
  }
}

TEST_CASE(
    "t-span-adaptor-band members",
    "[t_span_adaptor_band][members]"
    "[upper][lower_strictly]")
{
  auto A = make_cell<matrix_staged<double, 2>>(4)(g_iota(0.5));

  SECTION("diagonal")
  {
    auto aA = diagonal(A);

    CHECK(aA.size() == index(4, 4));

    CHECK(aA(2, 2) == 12.5);

    CHECK(!aA.is_bounded(1, 2));
    CHECK(aA.is_bounded(2, 2));
    CHECK(!aA.is_bounded(3, 2));

    auto it = aA.iterator(1, 1);

    CHECK(it.pos() == index(1, 1));
    CHECK(it == aA.iterator(1, 1));
    CHECK(*it == 7.5);
    ++it;
    CHECK(it.pos() == index(2, 2));
    CHECK(it == aA.iterator(2, 2));
    CHECK(*it == 12.5);
    ++it;
    CHECK(it.pos() == index(3, 3));
    CHECK(it == aA.iterator(3, 3));
    CHECK(*it == 17.5);
    ++it;
    CHECK(it == aA.end());
    --it;
    CHECK(it.pos() == index(3, 3));
    CHECK(it == aA.iterator(3, 3));
    CHECK(*it == 17.5);
    --it;
    CHECK(it.pos() == index(2, 2));
    CHECK(it == aA.iterator(2, 2));
    CHECK(*it == 12.5);
    --it;
    CHECK(it.pos() == index(1, 1));
    CHECK(it == aA.iterator(1, 1));
    CHECK(*it == 7.5);
    --it;

    CHECK(it == aA.begin());
  }
  SECTION("band")
  {
    auto aA = band_upper_strictly(A, 2_i);

    CHECK(aA.size() == index(4, 4));

    CHECK(aA(1, 2) == 11.5);

    CHECK(!aA.is_bounded(-2, 1));
    CHECK(!aA.is_bounded(-1, 1));
    CHECK(aA.is_bounded(0, 1));
    CHECK(!aA.is_bounded(1, 1));
    CHECK(!aA.is_bounded(2, 1));

    auto it = aA.iterator(1, 2);

    CHECK(it.pos() == index(1, 2));
    CHECK(it == aA.iterator(1, 2));
    CHECK(*it == 11.5);
    ++it;
    CHECK(it.pos() == index(1, 3));
    CHECK(it == aA.iterator(1, 3));
    CHECK(*it == 15.5);
    ++it;
    CHECK(it.pos() == index(2, 3));
    CHECK(it == aA.iterator(2, 3));
    CHECK(*it == 16.5);
    ++it;
    CHECK(it == aA.end());
    --it;
    CHECK(it.pos() == index(2, 3));
    CHECK(it == aA.iterator(2, 3));
    CHECK(*it == 16.5);
    --it;
    CHECK(it.pos() == index(1, 3));
    CHECK(it == aA.iterator(1, 3));
    CHECK(*it == 15.5);
    --it;
    CHECK(it.pos() == index(1, 2));
    CHECK(it == aA.iterator(1, 2));
    CHECK(*it == 11.5);
    --it;
    CHECK(it.pos() == index(0, 2));
    CHECK(it == aA.iterator(0, 2));
    CHECK(*it == 10.5);
    --it;
    CHECK(it.pos() == index(0, 1));
    CHECK(it == aA.iterator(0, 1));
    CHECK(*it == 6.5);

    CHECK(it == aA.begin());
  }
}
