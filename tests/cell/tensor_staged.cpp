#include "numa/cell/tensor_staged.hpp"
#include "numa/cell/algorithms.hpp"
#include "numa/cell/make_cell.hpp"
#include "numa/cell/serialize_json.hpp"

#include "numa/core/functors.hpp"
#include "numa/core/generators.hpp"

#include "numa/misc/data.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

TEST_CASE("band-tensor concepts", "[tensor_staged][concepts]")
{
  static_assert(Cell<matrix_diagonal<double>>);
  static_assert(Span<matrix_span_diagonal<double>>);
  static_assert(
      is_same_template<matrix_diagonal<double>, matrix_diagonal<int>>());
}

TEST_CASE("basic band-matrix", "[tensor_staged][basic]")
{
  const auto A = make_cell<matrix_staged<int, 1>>(5)(g_iota<int>());

  CHECK(
      A ==
      make_from_json<matrix_staged<int, 1>>("{ \"size\": [5],"
                                            "  \"values\":"
                                            "[[0, 1, 2],"
                                            " [3, 4, 5],"
                                            " [6, 7, 8],"
                                            " [9, 10, 11],"
                                            " [12, 13, 14]"
                                            "] }"));
}
