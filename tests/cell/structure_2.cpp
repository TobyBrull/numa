#include "numa/cell/structure.hpp"

#include "numa/cell/reform.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/tensor.hpp"
#include "numa/cell/tensor_fixed_capacity.hpp"
#include "numa/cell/tensor_fixed_size.hpp"
#include "numa/cell/tensor_linear.hpp"
#include "numa/cell/tensor_uniform.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("fixed-size basic structure", "[structure][tensor_fixed_size]")
{
  using test_struct = structure<
      label<m_rows_t, vector<int>>,
      label<m_cols_t, vector_fixed_size<int, 3>>,
      label<m_lays_t, vector_fixed_capacity<int, 4>>,
      field<m_vals_t, cuboid<double>, m_rows_t, m_cols_t, m_lays_t>>;

  static_assert(
      test_struct::structure_info.axis_marks ==
      type_list<m_rows_t, m_cols_t, m_lays_t>);
  static_assert(test_struct::structure_info.num_axes == 3);

  static_assert(std::is_same_v<test_struct::value_type, void>);

  static_assert(
      test_struct::required_bytes({5, 3, 3}) ==
      ((5 * 3 * 3) * sizeof(double) + (5) * sizeof(int)));

  test_struct ts;

  CHECK(ts.const_cell(m_rows).size() == index(0));
  CHECK(ts.const_cell(m_cols).size() == index(3));
  CHECK(ts.const_cell(m_lays).size() == index(0));
  CHECK(ts.const_cell(m_vals).size() == index(0, 3, 0));

  CHECK(ts.const_cell(m_rows).capacity() == index(0));
  CHECK(ts.const_cell(m_cols).capacity() == index(3));
  CHECK(ts.const_cell(m_lays).capacity() == index(4));
  CHECK(ts.const_cell(m_vals).capacity() == index(0, 3, 4));

  resize(ts.vary_axes(m_lays), 2);

  CHECK(ts.const_cell(m_rows).size() == index(0));
  CHECK(ts.const_cell(m_cols).size() == index(3));
  CHECK(ts.const_cell(m_lays).size() == index(2));
  CHECK(ts.const_cell(m_vals).size() == index(0, 3, 2));

  CHECK(ts.const_cell(m_rows).capacity() == index(0));
  CHECK(ts.const_cell(m_cols).capacity() == index(3));
  CHECK(ts.const_cell(m_lays).capacity() == index(4));
  CHECK(ts.const_cell(m_vals).capacity() == index(0, 3, 4));

  resize(ts.vary_axes(m_rows), 5);

  CHECK(ts.const_cell(m_rows).size() == index(5));
  CHECK(ts.const_cell(m_cols).size() == index(3));
  CHECK(ts.const_cell(m_lays).size() == index(2));
  CHECK(ts.const_cell(m_vals).size() == index(5, 3, 2));

  CHECK(ts.const_cell(m_rows).capacity() == index(5));
  CHECK(ts.const_cell(m_cols).capacity() == index(3));
  CHECK(ts.const_cell(m_lays).capacity() == index(4));
  CHECK(ts.const_cell(m_vals).capacity() == index(5, 3, 4));

  {
    using test_struct_2 = structure<
        field<m_main_t, test_struct, m_rows_t, m_cols_t, m_lays_t>,
        field<m_a_t, matrix<std::string>, m_rows_t, m_cols_t>>;

    constexpr auto s_info_2 = test_struct_2::structure_info;

    static_assert(
        s_info_2.axis_marks == type_list<m_rows_t, m_cols_t, m_lays_t>);
    static_assert(s_info_2.num_axes == 3);

    constexpr auto v_info_2 = s_info_2.vary_axes_info();

    static_assert(v_info_2.is_dynamic() == bool_list<true, false, false>);
    static_assert(
        v_info_2.is_fixed_capacity() == bool_list<false, false, true>);
    static_assert(v_info_2.is_fixed_size() == bool_list<false, true, false>);

    test_struct_2 ts2;

    resize(
        ts2.vary_axes(m_rows_t{}, m_lays_t{}),
        {2, 1},
        init<m_a_t>("BV"),
        init<m_rows_t>(7),
        init<m_lays_t>(8),
        init<m_vals_t>(0.123));

    CHECK(
        ts2 ==
        make_from_json<test_struct_2>("{\n"
                                      "  \"size\": [2, 3, 1],\n"
                                      "  \"main\": {\n"
                                      "    \"size\": [2, 3, 1],\n"
                                      "    \"rows\": {\n"
                                      "      \"size\": [2],\n"
                                      "      \"values\":\n"
                                      "        [7, 7]\n"
                                      "    },\n"
                                      "    \"cols\": {\n"
                                      "      \"size\": [3],\n"
                                      "      \"values\":\n"
                                      "        [0, 0, 0]\n"
                                      "    },\n"
                                      "    \"lays\": {\n"
                                      "      \"size\": [1],\n"
                                      "      \"values\":\n"
                                      "        [8]\n"
                                      "    },\n"
                                      "    \"vals\": {\n"
                                      "      \"size\": [2, 3, 1],\n"
                                      "      \"values\":\n"
                                      "        [[[0.123, 0.123],\n"
                                      "          [0.123, 0.123],\n"
                                      "          [0.123, 0.123]\n"
                                      "         ]\n"
                                      "        ]\n"
                                      "    }\n"
                                      "  },\n"
                                      "  \"a\": {\n"
                                      "    \"size\": [2, 3],\n"
                                      "    \"values\":\n"
                                      "      [[\"BV\", \"BV\"],\n"
                                      "       [\"BV\", \"BV\"],\n"
                                      "       [\"BV\", \"BV\"]\n"
                                      "      ]\n"
                                      "  }\n"
                                      "}"));

    CHECK(ts2.get(m_main_t{}).get(m_cols_t{}).size() == index(3));
  }

  {
    using test_struct_2 = structure<
        field<m_main_t, test_struct, m_rows_t, m_cols_t, m_lays_t>,
        field<m_a_t, matrix_fixed_size<std::string, 5, 2>, m_rows_t, m_lays_t>>;

    constexpr auto s_info_2 = test_struct_2::structure_info;

    static_assert(
        s_info_2.axis_marks == type_list<m_rows_t, m_cols_t, m_lays_t>);
    static_assert(s_info_2.num_axes == 3);

    constexpr auto v_info_2 = s_info_2.vary_axes_info();

    static_assert(v_info_2.is_dynamic() == bool_list<false, false, false>);
    static_assert(
        v_info_2.is_fixed_capacity() == bool_list<false, false, false>);
    static_assert(v_info_2.is_fixed_size() == bool_list<true, true, true>);

    test_struct_2 ts2(
        init<m_a_t>("BV"),
        init<m_rows_t>(1),
        init<m_cols_t>(2),
        init<m_lays_t>(3),
        init<m_vals_t>(2.1));

    CHECK(
        ts2 ==
        make_from_json<test_struct_2>(
            "{\n"
            "  \"size\": [5, 3, 2],\n"
            "  \"main\": {\n"
            "    \"size\": [5, 3, 2],\n"
            "    \"rows\": {\n"
            "      \"size\": [5],\n"
            "      \"values\":\n"
            "        [1, 1, 1, 1, 1]\n"
            "    },\n"
            "    \"cols\": {\n"
            "      \"size\": [3],\n"
            "      \"values\":\n"
            "        [2, 2, 2]\n"
            "    },\n"
            "    \"lays\": {\n"
            "      \"size\": [2],\n"
            "      \"values\":\n"
            "        [3, 3]\n"
            "    },\n"
            "    \"vals\": {\n"
            "      \"size\": [5, 3, 2],\n"
            "      \"values\":\n"
            "        [[[2.1, 2.1, 2.1, 2.1, 2.1],\n"
            "          [2.1, 2.1, 2.1, 2.1, 2.1],\n"
            "          [2.1, 2.1, 2.1, 2.1, 2.1]\n"
            "         ],\n"
            "         [[2.1, 2.1, 2.1, 2.1, 2.1],\n"
            "          [2.1, 2.1, 2.1, 2.1, 2.1],\n"
            "          [2.1, 2.1, 2.1, 2.1, 2.1]\n"
            "         ]\n"
            "        ]\n"
            "    }\n"
            "  },\n"
            "  \"a\": {\n"
            "    \"size\": [5, 2],\n"
            "    \"values\":\n"
            "      [[\"BV\", \"BV\", \"BV\", \"BV\", \"BV\"],\n"
            "       [\"BV\", \"BV\", \"BV\", \"BV\", \"BV\"]\n"
            "      ]\n"
            "  }\n"
            "}"));
  }
}

TEST_CASE(
    "structure with fixed-size and fixed-capacity-tensor",
    "[structure][tensor_fixed_size][tensor_fixed_capacity]")
{
  using test_struct = structure<
      label<m_main_t, vector_fixed_size<int, 3>>,
      field<m_data_t, vector_fixed_capacity<int, 4>, m_main_t>>;

  static_assert(test_struct::structure_info.axis_marks == type_list<m_main_t>);
  static_assert(test_struct::structure_info.num_axes == 1);

  static_assert(std::is_same_v<test_struct::value_type, int>);

  static_assert(test_struct::required_bytes({3}) == 0);

  static_assert(
      test_struct::structure_info.vary_axes_info().value_list == index_list<3>);

  test_struct ts(init<m_data_t>(3), init<m_main_t>(2));

  CHECK(
      ts ==
      make_from_json<test_struct>("{\n"
                                  "  \"size\": [3],\n"
                                  "  \"main\": {\n"
                                  "    \"size\": [3],\n"
                                  "    \"values\":\n"
                                  "      [2, 2, 2]\n"
                                  "  },\n"
                                  "  \"data\": {\n"
                                  "    \"size\": [3],\n"
                                  "    \"values\":\n"
                                  "      [3, 3, 3]\n"
                                  "  }\n"
                                  "}"));
}

TEST_CASE("structure with uniform-tensor", "[structure][tensor_uniform]")
{
  using test_struct = structure<
      label<m_cols_t, vector_fixed_size<int, 3>>,
      label<m_lays_t, vector_fixed_capacity<int, 4>>,
      label<m_rows_t, vector<int>>,
      field<m_vals_t, cuboid_uniform<double>, m_rows_t, m_cols_t, m_lays_t>>;

  static_assert(
      test_struct::structure_info.axis_marks ==
      type_list<m_cols_t, m_lays_t, m_rows_t>);
  static_assert(test_struct::structure_info.num_axes == 3);

  static_assert(std::is_same_v<test_struct::value_type, void>);

  static_assert(test_struct::required_bytes({3, 2, 4}) == ((4) * sizeof(int)));

  test_struct ts;

  resize(ts.vary_axes(m_rows_t{}, m_lays_t{}), {2, 3});

  ts.get(m_vals_t{}).value() = 3.14;

  CHECK(ts.get(m_rows).size() == index(2));
  CHECK(ts.get(m_cols).size() == index(3));
  CHECK(ts.get(m_lays).size() == index(3));
  CHECK(ts.get(m_vals).size() == index(2, 3, 3));

  CHECK(ts.get(m_vals).at(0, 0, 0) == 3.14);
}

TEST_CASE("structure with lin-range", "[structure][lin_range]")
{
  using test_struct = structure<
      label<m_rows_t, vector_linear_incr<float>>,
      field<m_data_t, matrix_fixed_size<char, 4, 2>, m_rows_t, m_cols_t>>;

  static_assert(
      test_struct::structure_info.axis_marks == type_list<m_rows_t, m_cols_t>);
  static_assert(test_struct::structure_info.num_axes == 2);

  static_assert(std::is_same_v<test_struct::value_type, void>);

  static_assert(test_struct::required_bytes({4, 2}) == 0);

  test_struct ts(init(m_data, 'e'));

  ts.get(m_data).at(1, 0) = 'f';
  ts.get(m_rows).front()  = 2.0;
  ts.get(m_rows).incr()   = 0.25;

  CHECK(ts == make_from_json<test_struct>(R"( {
      "size": [4, 2],
      "rows": {
        "size": [4],
        "front": 2,
        "incr": 0.25
      },
      "data": {
        "size": [4, 2],
        "values":
          [["e", "f", "e", "e"],
           ["e", "e", "e", "e"]
          ]
      } })"));
}
