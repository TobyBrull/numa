#include "numa/cell/serialize_binary.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/make_cell.hpp"
#include "numa/cell/structure.hpp"
#include "numa/cell/tensor.hpp"

#include "numa/core/generators.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE(
    "serialize-binary for span",
    "[tensor][serialize_binary][from_binary][to_binary]")
{
  const auto A = make_cell<tensor<int, 2>>(3, 2)(g_iota(2));

  std::stringstream ss;

  to_binary(ss, A);

  SECTION("make_cell & from_binary")
  {
    auto B = make_cell<tensor<int, 2>>(A.size())();

    const auto Bs = B.span();

    from_binary(ss, Bs);

    CHECK(A == B);
  }
  SECTION("make_from_binary")
  {
    auto const B = make_from_binary<matrix<int>>(ss);

    CHECK(A == B);
  }
}
