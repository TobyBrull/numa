#include "numa/cell/make_cell.hpp"
#include "numa/cell/tensor.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/t_span_adaptor_mirror.hpp"
#include "numa/cell/t_span_adaptor_triangular.hpp"
#include "numa/cell/tensor.hpp"

#include "numa/core/functors.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("basic make-cell", "[make_cell][basic]")
{
  const auto A = make_cell<matrix<double>>(4, 2)(f_const_t(3.5));

  CHECK(
      A ==
      make_from_json<matrix<double>>("{ \"size\": [4, 2],"
                                     "  \"values\":"
                                     "[[3.5, 3.5, 3.5, 3.5],"
                                     " [3.5, 3.5, 3.5, 3.5]"
                                     "] }"));

  const auto B = make_copy(A);

  CHECK(A == B);

  CHECK(
      B ==
      make_from_json<matrix<double>>("{ \"size\": [4, 2],"
                                     "  \"values\":"
                                     "[[3.5, 3.5, 3.5, 3.5],"
                                     " [3.5, 3.5, 3.5, 3.5]"
                                     "] }"));

  const auto C = make_cell(A.span(), B.span())(f_plus);

  CHECK(
      C ==
      make_from_json<matrix<double>>("{ \"size\": [4, 2],"
                                     "  \"values\":"
                                     "[[7, 7, 7, 7],"
                                     " [7, 7, 7, 7]"
                                     "] }"));

  auto M         = make_cell<matrix<int>>(3, 3)(f_const_t{1});
  M.span()(0, 1) = 2;
  M.span()(0, 2) = 3;
  M.span()(1, 2) = 4;

  auto MM = make_copy<matrix<int>>(upper_symmetric(M));

  CHECK(
      MM ==
      make_from_json<matrix<int>>("{ \"size\": [3, 3],"
                                  "  \"values\":"
                                  "[[1, 2, 3],"
                                  " [2, 1, 4],"
                                  " [3, 4, 1]"
                                  "] }"));

  MM(0, 1) = 10;
  MM(1, 0) = 50;

  CHECK(
      MM ==
      make_from_json<matrix<int>>("{ \"size\": [3, 3],"
                                  "  \"values\":"
                                  "[[1, 50, 3],"
                                  " [10, 1, 4],"
                                  " [3, 4, 1]"
                                  "] }"));

  auto N = make_cell<matrix<int>>(3, 3)();
  copy_from(upper(MM), N);

  CHECK(
      N ==
      make_from_json<matrix<int>>("{ \"size\": [3, 3],"
                                  "  \"values\":"
                                  "[[1, 0, 0],"
                                  " [10, 1, 0],"
                                  " [3, 4, 1]"
                                  "] }"));

  auto D = make_cell(N)(f_square);

  CHECK(
      D ==
      make_from_json<matrix<int>>("{ \"size\": [3, 3],"
                                  "  \"values\":"
                                  "[[1, 0, 0],"
                                  " [100, 1, 0],"
                                  " [9, 16, 1]"
                                  "] }"));
}
