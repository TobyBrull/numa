#include "numa/cell/misc.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("band", "[misc][band]")
{
  /*
   * is_valid_band_parameters
   */
  static_assert(
      is_valid_band_parameters<4>(index_list<2, 2>, index_list<1, 3>));
  static_assert(
      is_valid_band_parameters<4>(index_list<1, 2>, index_list<1, 3>));
  static_assert(
      is_valid_band_parameters<4>(index_list<0, 2>, index_list<1, 3>));
  static_assert(
      is_valid_band_parameters<4>(index_list<-1, 2>, index_list<1, 3>));
  static_assert(
      is_valid_band_parameters<4>(index_list<2, 0>, index_list<1, 3>));
  static_assert(
      is_valid_band_parameters<4>(index_list<2, -1>, index_list<1, 3>));
  static_assert(
      is_valid_band_parameters<4>(index_list<2, -2>, index_list<1, 3>));
  static_assert(
      is_valid_band_parameters<4>(index_list<2, -3>, index_list<1, 3>));
  static_assert(
      !is_valid_band_parameters<4>(index_list<2, -5>, index_list<1, 3>));
  static_assert(
      !is_valid_band_parameters<4>(index_list<-4, -5>, index_list<-2, 0>));

  static_assert(
      is_valid_band_parameters<6>(index_list<4, 5, 6>, index_list<-2, -3, -5>));
  static_assert(
      is_valid_band_parameters<6>(index_list<4, 3, 6>, index_list<-2, -3, -5>));
  static_assert(!is_valid_band_parameters<6>(
      index_list<4, 5, 6>, index_list<-2, -3, -8>));
  static_assert(!is_valid_band_parameters<6>(
      index_list<-1, 5, 6>, index_list<-2, -3, -5>));

  static_assert(!is_valid_band_parameters<1>(index_list<1>, index_list<2>));
  static_assert(is_valid_band_parameters<2>(index_list<1>, index_list<2>));
  static_assert(!is_valid_band_parameters<3>(index_list<1>, index_list<2>));
  static_assert(!is_valid_band_parameters<4>(index_list<1>, index_list<2>));

  /*
   * is_bounded_staged
   */
  {
    constexpr index_t N                = 4;
    constexpr auto Side                = side::right;
    [[maybe_unused]] constexpr auto ls = index_list<1, 2>;
    [[maybe_unused]] constexpr auto rs = index_list<1, 2>;

    constexpr index<2> size(4, 2);

    constexpr auto is_inside = [=](index<N> const& ind) constexpr
    {
      return is_bounded_staged<N, Side>(ls, rs, ind, size);
    };

    static_assert(is_inside({3, 1, 3, 1}));
    static_assert(is_inside({2, 1, 3, 1}));
    static_assert(is_inside({2, 0, 3, 1}));
    static_assert(is_inside({4, 2, 3, 1}));
    static_assert(!is_inside({1, 0, 3, 1}));
    static_assert(is_inside({2, -1, 3, 1}));
    static_assert(!is_inside({2, -2, 3, 1}));
    static_assert(is_inside({2, 3, 3, 1}));
    static_assert(!is_inside({2, 4, 3, 1}));
  }

  {
    constexpr index_t N                = 2;
    constexpr auto Side                = side::right;
    [[maybe_unused]] constexpr auto ls = index_list<3>;
    [[maybe_unused]] constexpr auto rs = index_list<-2>;

    constexpr index<1> size(4);

    constexpr auto is_inside = [=](index<N> const& ind) constexpr
    {
      return is_bounded_staged<N, Side>(ls, rs, ind, size);
    };

    static_assert(!is_inside({3, -1}));

    static_assert(!is_inside({0, -1}));
    static_assert(!is_inside({1, -1}));
    static_assert(!is_inside({2, -1}));
    static_assert(!is_inside({3, -1}));

    static_assert(!is_inside({1, 0}));
    static_assert(is_inside({2, 0}));
    static_assert(is_inside({3, 0}));
    static_assert(!is_inside({4, 0}));

    static_assert(!is_inside({2, 1}));
    static_assert(is_inside({3, 1}));
    static_assert(is_inside({4, 1}));
    static_assert(!is_inside({5, 1}));

    static_assert(!is_inside({4, 3}));
    static_assert(is_inside({5, 3}));
    static_assert(is_inside({6, 3}));
    static_assert(!is_inside({7, 3}));

    static_assert(!is_inside({5, 4}));
    static_assert(!is_inside({6, 4}));
    static_assert(!is_inside({7, 4}));
    static_assert(!is_inside({8, 4}));
  }

  {
    constexpr index_t N                = 2;
    constexpr auto Side                = side::left;
    [[maybe_unused]] constexpr auto ls = index_list<1>;
    [[maybe_unused]] constexpr auto rs = index_list<2>;

    constexpr index<1> size(3);

    constexpr auto is_inside = [=](index<N> const& ind) constexpr
    {
      return is_bounded_staged<N, Side>(ls, rs, ind, size);
    };

    static_assert(!is_inside({-1, -2}));
    static_assert(!is_inside({0, -2}));
    static_assert(!is_inside({1, -2}));

    static_assert(!is_inside({-1, -1}));
    static_assert(is_inside({0, -1}));
    static_assert(!is_inside({1, -1}));

    static_assert(!is_inside({-1, 0}));
    static_assert(is_inside({0, 0}));
    static_assert(is_inside({1, 0}));
    static_assert(!is_inside({2, 0}));

    static_assert(!is_inside({-1, 1}));
    static_assert(is_inside({0, 1}));
    static_assert(is_inside({1, 1}));
    static_assert(is_inside({2, 1}));
    static_assert(!is_inside({3, 1}));

    static_assert(!is_inside({-1, 2}));
    static_assert(is_inside({0, 2}));
    static_assert(is_inside({1, 2}));
    static_assert(is_inside({2, 2}));
    static_assert(!is_inside({3, 2}));

    static_assert(!is_inside({-1, 3}));
    static_assert(!is_inside({0, 3}));
    static_assert(is_inside({1, 3}));
    static_assert(is_inside({2, 3}));
    static_assert(!is_inside({3, 3}));

    static_assert(!is_inside({-1, 4}));
    static_assert(!is_inside({0, 4}));
    static_assert(!is_inside({1, 4}));
    static_assert(is_inside({2, 4}));
    static_assert(!is_inside({3, 4}));

    static_assert(!is_inside({-1, 5}));
    static_assert(!is_inside({0, 5}));
    static_assert(!is_inside({1, 5}));
    static_assert(!is_inside({2, 5}));
    static_assert(!is_inside({3, 5}));
  }

  /*
   * is_bounded_band
   */
  {
    constexpr index_t N                = 4;
    [[maybe_unused]] constexpr auto ls = index_list<1, 2>;
    [[maybe_unused]] constexpr auto rs = index_list<1, 2>;

    constexpr index<4> size(3, 2, 4, 2);

    constexpr auto is_inside = [=](index<N> const& ind) constexpr
    {
      return is_bounded_band<N>(ls, rs, ind, size);
    };

    static_assert(!is_inside({3, 1, 3, 1}));
    static_assert(is_inside({2, 1, 3, 1}));
    static_assert(is_inside({2, 0, 3, 1}));
    static_assert(!is_inside({4, 2, 3, 1}));
    static_assert(!is_inside({1, 0, 3, 1}));
    static_assert(!is_inside({2, -1, 3, 1}));
    static_assert(!is_inside({2, -2, 3, 1}));
    static_assert(!is_inside({2, 3, 3, 1}));
    static_assert(!is_inside({2, 4, 3, 1}));
  }

  {
    constexpr index_t N                = 2;
    [[maybe_unused]] constexpr auto ls = index_list<1>;
    [[maybe_unused]] constexpr auto rs = index_list<-1>;

    constexpr index<2> size(5, 4);

    constexpr auto is_inside = [=](index<N> const& ind) constexpr
    {
      return is_bounded_band<N>(ls, rs, ind, size);
    };

    static_assert(!is_inside({3, -1}));

    static_assert(!is_inside({3, -1}));
    static_assert(!is_inside({4, -1}));
    static_assert(!is_inside({5, -1}));
    static_assert(!is_inside({6, -1}));

    static_assert(!is_inside({-1, 0}));
    static_assert(!is_inside({0, 0}));
    static_assert(is_inside({1, 0}));
    static_assert(!is_inside({2, 0}));

    static_assert(!is_inside({0, 1}));
    static_assert(!is_inside({1, 1}));
    static_assert(is_inside({2, 1}));
    static_assert(!is_inside({3, 1}));

    static_assert(!is_inside({1, 2}));
    static_assert(!is_inside({2, 2}));
    static_assert(is_inside({3, 2}));
    static_assert(!is_inside({4, 2}));

    static_assert(!is_inside({2, 3}));
    static_assert(!is_inside({3, 3}));
    static_assert(is_inside({4, 3}));
    static_assert(!is_inside({5, 3}));

    static_assert(!is_inside({3, 4}));
    static_assert(!is_inside({4, 4}));
    static_assert(!is_inside({5, 4}));
  }
}
