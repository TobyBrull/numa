#include "numa/cell/reform_unified.hpp"

#include "reform_unified_helper.hpp"

#include "numa/core/serialize_json.hpp"

#include "numa/boot/bits.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::unified;
using namespace numa::misc;

template<index_t N>
class LoggingCellTarget {
 public:
  LoggingCellTarget(
      index<N> const& size,
      index<N> const& capacity,
      const bool must_reallocate)
    : size_(size), capacity_(capacity), must_reallocate_(must_reallocate)
  {
  }

  class target {
   public:
    target(LoggingCellTarget* const parent) : parent(parent) {}

    target& operator=(target const&) = delete;
    target(target const&)            = delete;

    target& operator=(target&&) = default;
    target(target&&)            = delete;

    constexpr static index_t sel_num_axes = N;
    constexpr static index_t sel_num_dims = N;

    index<N> const& size() const
    {
      return parent->size_;
    }
    index<N> const& capacity() const
    {
      return parent->capacity_;
    }
    bool must_reallocate() const
    {
      return parent->must_reallocate_;
    }

    template<typename TupleLike, typename... Ts>
    requires(std::tuple_size_v<TupleLike> == N) void translate(
        index<N> const& new_size,
        index<N> const& new_capacity,
        const TupleLike& translate_fs,
        const Ts... values)
    {
      parent->ops_log_ += "new-size: " + to_json(new_size) + "\n";
      parent->ops_log_ += "new-capacity: " + to_json(new_capacity) + "\n";

      tuple_for_each_indexed(
          [&](const auto ind, const auto& f) {
            LoggingAxisTarget loc_tgt;

            f(loc_tgt);

            parent->ops_log_ += "-> translate (" + std::to_string(ind.value) +
                ((", " + std::to_string(values)) + ... + "") + ")\n";
            parent->ops_log_ += loc_tgt.log();
          },
          translate_fs);

      parent->size_     = new_size;
      parent->capacity_ = new_capacity;
    }

    template<typename TupleLike, typename... Ts>
    requires(std::tuple_size_v<TupleLike> == N) void inplace(
        index<N> const& new_size,
        TupleLike const& inplace_fs,
        Ts const... values)
    {
      parent->ops_log_ += "new-size: " + to_json(new_size) + "\n";

      tuple_for_each_indexed(
          [&](const auto ind, const auto& f) {
            LoggingAxisTarget loc_tgt;

            f(loc_tgt);

            parent->ops_log_ += "-> inplace (" + std::to_string(ind.value) +
                ((", " + std::to_string(values)) + ... + "") + ")\n";
            parent->ops_log_ += loc_tgt.log();
          },
          inplace_fs);

      parent->size_ = new_size;
    }

   private:
    LoggingCellTarget* parent;
  };

  auto axes()
  {
    return target{this};
  }

  const std::string& log() const
  {
    return ops_log_;
  }

 private:
  index<N> size_;
  index<N> capacity_;
  bool must_reallocate_;

  std::string ops_log_;
};

TEST_CASE("cell-resize(-hard)", "[resize][resize_hard]")
{
  {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, false);

    resize(tgt.axes(), {0, 0});

    CHECK(
        tgt.log() ==
        "new-size: [0, 0]\n"
        "-> inplace (0)\n"
        "apply (destruct, 0, 0)\n"
        "apply (destruct, 1, 1)\n"
        "apply (destruct, 2, 2)\n"
        "-> inplace (1)\n"
        "apply (destruct, 0, 0)\n"
        "apply (destruct, 1, 1)\n"
        "apply (destruct, 2, 2)\n"
        "apply (destruct, 3, 3)\n");
  }

  {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, true);

    resize(tgt.axes(), {0, 0});

    CHECK(
        tgt.log() ==
        "new-size: [0, 0]\n"
        "new-capacity: [4, 5]\n"
        "-> translate (0)\n"
        "-> translate (1)\n");
  }

  {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, false);

    resize_hard(tgt.axes(), {0, 0});

    CHECK(
        tgt.log() ==
        "new-size: [0, 0]\n"
        "new-capacity: [0, 0]\n"
        "-> translate (0)\n"
        "-> translate (1)\n");
  }

  {
    LoggingCellTarget<2> tgt({0, 0}, {0, 0}, false);

    resize_hard(tgt.axes(), {0, 0});

    CHECK(tgt.log() == "");
  }

  {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, false);

    resize(tgt.axes(), {4, 5}, 13);

    CHECK(
        tgt.log() ==
        "new-size: [4, 5]\n"
        "-> inplace (0, 13)\n"
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (none, 2, 2)\n"
        "apply (fill_construct, 3, 3)\n"
        "-> inplace (1, 13)\n"
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (none, 2, 2)\n"
        "apply (none, 3, 3)\n"
        "apply (fill_construct, 4, 4)\n");
  }

  {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, false);

    resize(tgt.axes(), {2, 4}, 14);

    CHECK(
        tgt.log() ==
        "new-size: [2, 4]\n"
        "-> inplace (0, 14)\n"
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (destruct, 2, 2)\n"
        "-> inplace (1, 14)\n"
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (none, 2, 2)\n"
        "apply (none, 3, 3)\n");
  }

  {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, false);

    resize_hard(tgt.axes(), {4, 5}, 7);

    CHECK(
        tgt.log() ==
        "new-size: [4, 5]\n"
        "-> inplace (0, 7)\n"
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (none, 2, 2)\n"
        "apply (fill_construct, 3, 3)\n"
        "-> inplace (1, 7)\n"
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (none, 2, 2)\n"
        "apply (none, 3, 3)\n"
        "apply (fill_construct, 4, 4)\n");
  }

  {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, false);

    resize_hard(tgt.axes(), {5, 5}, 8);

    CHECK(
        tgt.log() ==
        "new-size: [5, 5]\n"
        "new-capacity: [5, 5]\n"
        "-> translate (0, 8)\n"
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n"
        "apply (move_construct, 2, 2)\n"
        "apply (fill_construct, 3, 3)\n"
        "apply (fill_construct, 4, 4)\n"
        "-> translate (1, 8)\n"
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n"
        "apply (move_construct, 2, 2)\n"
        "apply (move_construct, 3, 3)\n"
        "apply (fill_construct, 4, 4)\n");
  }
}

TEST_CASE("cell-reserve(-hard)", "[reserve][reserve_hard]")
{
  for (const bool must_reallocate: {true, false}) {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, must_reallocate);

    reserve_hard(tgt.axes(), {3, 4});

    CHECK(
        tgt.log() ==
        "new-size: [3, 4]\n"
        "new-capacity: [3, 4]\n"
        "-> translate (0)\n"
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n"
        "apply (move_construct, 2, 2)\n"
        "-> translate (1)\n"
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n"
        "apply (move_construct, 2, 2)\n"
        "apply (move_construct, 3, 3)\n");
  }

  {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, false);

    reserve_hard(tgt.axes(), {4, 5});

    CHECK(tgt.log() == "");
  }

  {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, false);

    reserve_hard(tgt.axes(), {6, 5});

    CHECK(
        tgt.log() ==
        "new-size: [3, 4]\n"
        "new-capacity: [6, 5]\n"
        "-> translate (0)\n"
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n"
        "apply (move_construct, 2, 2)\n"
        "-> translate (1)\n"
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n"
        "apply (move_construct, 2, 2)\n"
        "apply (move_construct, 3, 3)\n");
  }
}

TEST_CASE("cell-insert-size", "[insert_size]")
{
  {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, false);

    insert_size(tgt.axes(), {1, 3}, {1, 1}, 4);

    CHECK(
        tgt.log() ==
        "new-size: [4, 5]\n"
        "-> inplace (0, 4)\n"
        "apply (none, 0, 0)\n"
        "apply (move_construct, 2, 3)\n"
        "apply (move_assign, 1, 2)\n"
        "apply (fill_assign, 1, 1)\n"
        "-> inplace (1, 4)\n"
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (none, 2, 2)\n"
        "apply (move_construct, 3, 4)\n"
        "apply (fill_assign, 3, 3)\n");
  }

  {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, false);

    insert_size(tgt.axes(), {1, 3}, {1, 4}, 4);

    CHECK(
        tgt.log() ==
        "new-size: [4, 8]\n"
        "new-capacity: [4, 10]\n"
        "-> translate (0, 4)\n"
        "apply (move_construct, 0, 0)\n"
        "apply (fill_construct, 1, 1)\n"
        "apply (move_construct, 1, 2)\n"
        "apply (move_construct, 2, 3)\n"
        "-> translate (1, 4)\n"
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n"
        "apply (move_construct, 2, 2)\n"
        "apply (fill_construct, 3, 3)\n"
        "apply (fill_construct, 4, 4)\n"
        "apply (fill_construct, 5, 5)\n"
        "apply (fill_construct, 6, 6)\n"
        "apply (move_construct, 3, 7)\n");
  }
}

TEST_CASE("cell-erase-size", "[erase_size]")
{
  {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, false);

    erase_size(tgt.axes(), {1, 3}, {1, 1});

    CHECK(
        tgt.log() ==
        "new-size: [2, 3]\n"
        "-> inplace (0)\n"
        "apply (none, 0, 0)\n"
        "apply (move_assign, 2, 1)\n"
        "apply (destruct, 2, 2)\n"
        "-> inplace (1)\n"
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (none, 2, 2)\n"
        "apply (destruct, 3, 3)\n");
  }

  {
    LoggingCellTarget<2> tgt({3, 4}, {4, 5}, true);

    erase_size(tgt.axes(), {1, 2}, {1, 2});

    CHECK(
        tgt.log() ==
        "new-size: [2, 2]\n"
        "new-capacity: [4, 5]\n"
        "-> translate (0)\n"
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 2, 1)\n"
        "-> translate (1)\n"
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n");
  }
}
