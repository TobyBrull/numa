#include "numa/cell/tensor.hpp"

#include "numa/cell/tensor_staged.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/make_cell.hpp"
#include "numa/cell/serialize_json.hpp"

#include "numa/core/functors.hpp"
#include "numa/core/generators.hpp"

#include "numa/misc/memory.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

constexpr auto alloc   = memory_operation::allocate;
constexpr auto dealloc = memory_operation::deallocate;

TEST_CASE(TO_STRING(NAMESPACE) " simple reforms on tensor", "[reform][tensor]")
{
  context<allocator_tracer_monitor> ats;

  {
    cuboid<int, allocator_tracer<default_allocator>> cc;

    CHECK(ats.log().empty());

    CHECK(cc.size() == index<3>(zero));
    CHECK(cc.capacity() == index<3>(zero));

    /*
     * Reserve some memory
     */
    NAMESPACE::reserve_hard(cc.vary_axes(), index(4, 2, 3));

    CHECK(ats.num_active() == 1);
    REQUIRE(ats.log().size() == 1);
    CHECK(ats.log()[0].satisfies(alloc, 24 * sizeof(int)));

    CHECK(cc.size() == index<3>(zero));
    CHECK(cc.capacity() == index<3>(4, 2, 3));

    /*
     * Resize the cuboid.
     */
    NAMESPACE::resize(cc.vary_axes(), index(3, 1, 3), 7);

    CHECK(ats.num_active() == 1);
    CHECK(ats.log().size() == 1);
    CHECK(cc.size() == index(3, 1, 3));
    CHECK(cc.capacity() == index<3>(4, 2, 3));

    {
      const auto aai = ats.active_as<int>();

      REQUIRE(aai.size() == 1);
      REQUIRE(aai[0].size() == index(24));
      CHECK(
          aai[0].dims(0_i).sub(as_size, 0, 3) ==
          make_from_json<vector<int>>("{ \"size\": [3],"
                                      "  \"values\":"
                                      "[7, 7, 7] }")
              .span_const());
      CHECK(
          aai[0].dims(0_i).sub(as_size, 8, 3) ==
          make_from_json<vector<int>>("{ \"size\": [3],"
                                      "  \"values\":"
                                      "[7, 7, 7] }")
              .span());
      CHECK(
          aai[0].dims(0_i).sub(as_size, 16, 3) ==
          make_from_json<vector<int>>("{ \"size\": [3],"
                                      "  \"values\":"
                                      "[7, 7, 7] }")
              .span());
    }

    /*
     * Resize again.
     */
    NAMESPACE::resize(cc.vary_axes(), index(4, 2, 2), 9);

    CHECK(ats.num_active() == 1);
    CHECK(ats.log().size() == 1);
    CHECK(cc.size() == index(4, 2, 2));
    CHECK(cc.capacity() == index<3>(4, 2, 3));

    {
      const auto aai = ats.active_as<int>();

      REQUIRE(aai.size() == 1);
      REQUIRE(aai[0].size() == index(24));
      CHECK(
          aai[0].dims(0_i).sub(as_till, 0, 8) ==
          make_from_json<vector<int>>("{ \"size\": [8],"
                                      "  \"values\":"
                                      "[7, 7, 7, 9, 9, 9, 9, 9] }")
              .span());
      CHECK(
          aai[0].dims(0_i).sub(as_till, 8, 16) ==
          make_from_json<vector<int>>("{ \"size\": [8],"
                                      "  \"values\":"
                                      "[7, 7, 7, 9, 9, 9, 9, 9] }")
              .span());
    }

    /*
     * Shrink-to-fit.
     */
    NAMESPACE::reserve_hard(cc.vary_axes(), cc.size());

    CHECK(ats.num_active() == 1);
    REQUIRE(ats.log().size() == 3);
    CHECK(ats.log()[1].satisfies(alloc, 16 * sizeof(int)));
    CHECK(ats.log()[2].satisfies(dealloc, 24 * sizeof(int)));

    CHECK(cc.size() == index(4, 2, 2));
    CHECK(cc.capacity() == index<3>(4, 2, 2));

    {
      const auto aai = ats.active_as<int>();

      REQUIRE(aai.size() == 1);
      REQUIRE(aai[0].size() == index(16));
      CHECK(
          aai[0].dims(0_i).sub(as_till, 0, 8) ==
          make_from_json<vector<int>>("{ \"size\": [8],"
                                      "  \"values\":"
                                      "[7, 7, 7, 9, 9, 9, 9, 9] }")
              .span());
      CHECK(
          aai[0].dims(0_i).sub(as_till, 8, 16) ==
          make_from_json<vector<int>>("{ \"size\": [8],"
                                      "  \"values\":"
                                      "[7, 7, 7, 9, 9, 9, 9, 9] }")
              .span());
    }

    /*
     * Clear.
     */
    NAMESPACE::resize(cc.vary_axes(), index<3>(zero));

    CHECK(ats.num_active() == 1);
    CHECK(ats.log().size() == 3);
    CHECK(cc.size() == index<3>(zero));
    CHECK(cc.capacity() == index<3>(4, 2, 2));

    /*
     * Clear hard.
     */
    NAMESPACE::resize_hard(cc.vary_axes(), index<3>(zero));

    CHECK(ats.num_active() == 1);
    REQUIRE(ats.log().size() == 5);
    CHECK(ats.log()[3].satisfies(alloc, 0));
    CHECK(ats.log()[4].satisfies(dealloc, 16 * sizeof(int)));

    CHECK(cc.size() == index<3>(zero));
    CHECK(cc.capacity() == index<3>(zero));

    /*
     * One more normal resize.
     */
    NAMESPACE::resize(cc.vary_axes(), index(3, 3, 3), 4);

    CHECK(ats.num_active() == 1);
    REQUIRE(ats.log().size() == 7);
    CHECK(ats.log()[5].satisfies(alloc, 27 * sizeof(int)));
    CHECK(ats.log()[6].satisfies(dealloc, 0));
    CHECK(cc.size() == index(3, 3, 3));
    CHECK(cc.capacity() == index<3>(3, 3, 3));

    {
      const auto aai = ats.active_as<int>();

      REQUIRE(aai.size() == 1);
      REQUIRE(aai[0].size() == index(27));
      CHECK(std::all_of(aai[0].begin(), aai[0].end(), [](const auto val) {
        return (val == 4);
      }));
    }

    /*
     * Resize hard.
     */
    NAMESPACE::resize_hard(cc.vary_axes(), index(2, 2, 2), 666);

    CHECK(ats.num_active() == 1);
    REQUIRE(ats.log().size() == 9);
    CHECK(ats.log()[7].satisfies(alloc, 8 * sizeof(int)));
    CHECK(ats.log()[8].satisfies(dealloc, 27 * sizeof(int)));
    CHECK(cc.size() == index(2, 2, 2));
    CHECK(cc.capacity() == index<3>(2, 2, 2));

    {
      const auto aai = ats.active_as<int>();

      REQUIRE(aai.size() == 1);
      REQUIRE(aai[0].size() == index(8));
      CHECK(std::all_of(aai[0].begin(), aai[0].end(), [](const auto val) {
        return (val == 4);
      }));
    }

    /*
     * Reserve hard.
     */
    NAMESPACE::reserve_hard(cc.vary_axes(), index(3, 1, 3));

    CHECK(ats.num_active() == 1);
    REQUIRE(ats.log().size() == 11);
    CHECK(ats.log()[9].satisfies(alloc, 18 * sizeof(int)));
    CHECK(ats.log()[10].satisfies(dealloc, 8 * sizeof(int)));
    CHECK(cc.size() == index(2, 2, 2));
    CHECK(cc.capacity() == index<3>(3, 2, 3));

    {
      const auto aai = ats.active_as<int>();

      REQUIRE(aai.size() == 1);
      REQUIRE(aai[0].size() == index(18));
      const auto axes = aai[0].dims(0_i);
      CHECK(
          axes.sub(as_till, 0, 2) ==
          make_from_json<vector<int>>("{ \"size\": [2],"
                                      "  \"values\":"
                                      "[4, 4] }")
              .span());
      CHECK(
          axes.sub(as_till, 3, 5) ==
          make_from_json<vector<int>>("{ \"size\": [2],"
                                      "  \"values\":"
                                      "[4, 4] }")
              .span());
      CHECK(
          axes.sub(as_till, 6, 8) ==
          make_from_json<vector<int>>("{ \"size\": [2],"
                                      "  \"values\":"
                                      "[4, 4] }")
              .span());
      CHECK(
          axes.sub(as_till, 9, 11) ==
          make_from_json<vector<int>>("{ \"size\": [2],"
                                      "  \"values\":"
                                      "[4, 4] }")
              .span());
    }

    /*
     * Do a resize hard that doesn't allocate.
     */
    NAMESPACE::resize_hard(cc.vary_axes(), index(3, 2, 3), 5);

    CHECK(ats.num_active() == 1);
    REQUIRE(ats.log().size() == 11);
    CHECK(cc.size() == index(3, 2, 3));
    CHECK(cc.capacity() == index<3>(3, 2, 3));

    {
      const auto aai = ats.active_as<int>();

      REQUIRE(aai.size() == 1);
      REQUIRE(aai[0].size() == index(18));
      CHECK(
          aai[0] ==
          make_from_json<vector<int>>("{ \"size\": [18],"
                                      "  \"values\":"
                                      "[4, 4, 5, 4, 4, 5, 4, 4, 5, "
                                      "4, 4, 5, 5, 5, 5, 5, 5, 5] }")
              .span());
    }
  }

  CHECK(ats.num_active() == 0);
  REQUIRE(ats.log().size() == 12);
  CHECK(ats.log()[11].satisfies(dealloc, 18 * sizeof(int)));
}

TEST_CASE(
    TO_STRING(NAMESPACE) " big in capacity on resize",
    "[reform][tensor][resize][capacity][bug]")
{
  tensor<int, 5> A;

  NAMESPACE::resize(A.vary_axes(), index<5>(one), 4);
  CHECK(A.capacity() == index<5>(one));

  NAMESPACE::resize(A.vary_axes(), index(2, 1, 1, 1, 1), 6);
  CHECK(A.capacity() == index<5>(2, 1, 1, 1, 1));

  NAMESPACE::resize(A.vary_axes(), index(1, 3, 1, 1, 1), 6);
  CHECK(A.capacity() == index<5>(2, 3, 1, 1, 1));

  NAMESPACE::resize(A.vary_axes(), index(1, 1, 5, 1, 1), 6);
  CHECK(A.capacity() == index<5>(2, 3, 5, 1, 1));

  NAMESPACE::resize(A.vary_axes(), index(1, 1, 1, 9, 1), 6);
  CHECK(A.capacity() == index<5>(2, 3, 5, 9, 1));

  NAMESPACE::resize(A.vary_axes(), index(1, 1, 1, 1, 17), 6);
  CHECK(A.capacity() == index<5>(2, 3, 5, 9, 17));

  NAMESPACE::resize_hard(A.vary_axes(), index<5>(zero));
  CHECK(A.capacity() == index<5>(zero));

  NAMESPACE::resize(A.vary_axes(), index<5>(one), 4);
  CHECK(A.capacity() == index<5>(one));

  NAMESPACE::resize(A.vary_axes(0_i), index(2), 6);
  CHECK(A.capacity() == index<5>(2, 1, 1, 1, 1));

  NAMESPACE::resize(A.vary_axes<1>(), index(3), 6);
  CHECK(A.capacity() == index<5>(2, 3, 1, 1, 1));

  NAMESPACE::resize(A.vary_axes(type_list_t{2_i}), index(5), 6);
  CHECK(A.capacity() == index<5>(2, 3, 5, 1, 1));

  NAMESPACE::resize(A.vary_axes(index_list<3>), index(9), 6);
  CHECK(A.capacity() == index<5>(2, 3, 5, 9, 1));

  NAMESPACE::resize(A.vary_axes(4_i), index(17), 6);
  CHECK(A.capacity() == index<5>(2, 3, 5, 9, 17));
}

TEST_CASE(
    TO_STRING(NAMESPACE) " erase_size on plain matrix", "[erase_size][tensor]")
{
  auto A = make_cell<matrix<int>>(5, 5)(g_iota(1));

  CHECK(
      A ==
      make_from_json<matrix<int>>("{"
                                  "  \"size\": [5, 5],"
                                  "  \"values\":"
                                  "  [ [  1,  2,  3,  4,  5],"
                                  "    [  6,  7,  8,  9, 10],"
                                  "    [ 11, 12, 13, 14, 15],"
                                  "    [ 16, 17, 18, 19, 20],"
                                  "    [ 21, 22, 23, 24, 25]"
                                  "  ]"
                                  "}"));

  NAMESPACE::erase_size(A.vary_axes().with(force_reallocate), {1, 2}, {1, 1});

  CHECK(
      A ==
      make_from_json<matrix<int>>("{"
                                  "  \"size\": [4, 4],"
                                  "  \"values\":"
                                  "  [ [  1,  3,  4,  5],"
                                  "    [  6,  8,  9, 10],"
                                  "    [ 16, 18, 19, 20],"
                                  "    [ 21, 23, 24, 25]"
                                  "  ]"
                                  "}"));

  NAMESPACE::erase_size(A.vary_axes(), {1, 2}, {1, 1});

  CHECK(
      A ==
      make_from_json<matrix<int>>("{"
                                  "  \"size\": [3, 3],"
                                  "  \"values\":"
                                  "  [ [  1,  4,  5],"
                                  "    [  6,  9, 10],"
                                  "    [ 21, 24, 25]"
                                  "  ]"
                                  "}"));
}

TEST_CASE(
    TO_STRING(NAMESPACE) " insert_size on plain matrix",
    "[insert_size][tensor]")
{
  auto A = make_cell<matrix<int>>(3, 3)(g_iota(1));

  CHECK(
      A ==
      make_from_json<matrix<int>>("{"
                                  "  \"size\": [3, 3],"
                                  "  \"values\":"
                                  "  [ [ 1, 2, 3],"
                                  "    [ 4, 5, 6],"
                                  "    [ 7, 8, 9]"
                                  "  ]"
                                  "}"));

  CHECK(A.capacity() == index(3, 3));

  NAMESPACE::insert_size(A.vary_axes(), {1, 2}, {1, 1}, 0);

  CHECK(A.capacity() == index(6, 6));

  CHECK(
      A ==
      make_from_json<matrix<int>>("{"
                                  "  \"size\": [4, 4],"
                                  "  \"values\":"
                                  "  [ [ 1, 0, 2, 3],"
                                  "    [ 4, 0, 5, 6],"
                                  "    [ 0, 0, 0, 0],"
                                  "    [ 7, 0, 8, 9]"
                                  "  ]"
                                  "}"));

  NAMESPACE::insert_size(A.vary_axes(), {3, 1}, {1, 1}, 10);

  CHECK(A.capacity() == index(6, 6));

  CHECK(
      A ==
      make_from_json<matrix<int>>("{"
                                  "  \"size\": [5, 5],"
                                  "  \"values\":"
                                  "  [ [  1,  0,  2, 10,  3],"
                                  "    [ 10, 10, 10, 10, 10],"
                                  "    [  4,  0,  5, 10,  6],"
                                  "    [  0,  0,  0, 10,  0],"
                                  "    [  7,  0,  8, 10,  9]"
                                  "  ]"
                                  "}"));
}

TEST_CASE(
    TO_STRING(NAMESPACE) " cell-reform on plain matrix",
    "[reform_cell][tensor]")
{
  matrix<int> A;

  NAMESPACE::resize(A.vary_axes(), {3, 4}, 1);

  CHECK(A.capacity() == index(3, 4));

  NAMESPACE::resize(A.vary_axes(), {4, 5}, 2);

  CHECK(A.capacity() == index(6, 8));

  NAMESPACE::insert_size(A.vary_axes(1_i), {2}, {2}, 3);

  CHECK(A.capacity() == index(6, 8));

  NAMESPACE::erase_size(A.vary_axes(), {1, 3}, {1, 2});

  CHECK(A.capacity() == index(6, 8));
  CHECK(A.size() == index(3, 5));

  NAMESPACE::insert_size(A.vary_axes(1_i, 0_i), {4, 1}, {1, 4}, 4);

  CHECK(A.capacity() == index(12, 8));

  CHECK(
      A ==
      make_from_json<matrix<int>>("{"
                                  "  \"size\": [7, 6],"
                                  "  \"values\":"
                                  "  [ [ 1, 4, 4, 4, 4, 1, 2],"
                                  "    [ 1, 4, 4, 4, 4, 1, 2],"
                                  "    [ 3, 4, 4, 4, 4, 3, 3],"
                                  "    [ 1, 4, 4, 4, 4, 1, 2],"
                                  "    [ 4, 4, 4, 4, 4, 4, 4],"
                                  "    [ 2, 4, 4, 4, 4, 2, 2]"
                                  "  ]"
                                  "}"));
}

TEST_CASE(
    TO_STRING(NAMESPACE) " cell-reform on staged-matrix",
    "[reform_cell][tensor_staged]")
{
  matrix_staged<int, 1> A;

  NAMESPACE::resize(A.vary_axes(), {5}, 1);

  CHECK(A.capacity() == index(5));

  NAMESPACE::insert_size(A.vary_axes(), {4}, {2}, 2);

  CHECK(A.capacity() == index(10));

  CHECK(
      A ==
      make_from_json<matrix_staged<int, 1>>("{"
                                            "  \"size\": [7],"
                                            "  \"values\":"
                                            "  [ [1, 1, 1],"
                                            "    [1, 1, 1],"
                                            "    [1, 1, 1],"
                                            "    [1, 1, 1],"
                                            "    [2, 2, 2],"
                                            "    [2, 2, 2],"
                                            "    [1, 1, 1]"
                                            "  ]"
                                            "}"));

  for_each(A)([f = g_iota(0)](auto& val) mutable { val = f(); });

  NAMESPACE::erase_size(A.vary_axes(), {3}, {2});

  CHECK(A.capacity() == index(10));

  NAMESPACE::insert_size(A.vary_axes(), {5}, {1}, 90);

  CHECK(A.capacity() == index(10));

  NAMESPACE::insert_size(A.vary_axes(), {1}, {2}, 91);

  CHECK(A.capacity() == index(10));

  CHECK(
      A ==
      make_from_json<matrix_staged<int, 1>>("{"
                                            "  \"size\": [8],"
                                            "  \"values\":"
                                            "  [ [0, 1, 2],"
                                            "    [91, 91, 91],"
                                            "    [91, 91, 91],"
                                            "    [3, 4, 5],"
                                            "    [6, 7, 8],"
                                            "    [15, 16, 17],"
                                            "    [18, 19, 20],"
                                            "    [90, 90, 90]"
                                            "  ]"
                                            "}"));
}
