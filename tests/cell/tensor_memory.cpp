#include "numa/cell/tensor.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/make_cell.hpp"
#include "numa/cell/reform.hpp"
#include "numa/cell/serialize_json.hpp"

#include "numa/core/range.hpp"

#include "numa/misc/memory.hpp"
#include "numa/misc/type.hpp"

#include "catch2/catch.hpp"

#include <numeric>
#include <sstream>
#include <vector>

using namespace numa;
using namespace numa::misc;

TEST_CASE("tensor with memory and null-allocator", "[tensor][memory]")
{
  std::vector<int> workspace(20, 7);

  memory<null_allocator, alignment_of<int>> mem(
      {}, workspace.data(), workspace.size() * sizeof(int));

  tensor<int, 2, null_allocator> A;

  CHECK(workspace == std::vector<int>{7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
                                      7, 7, 7, 7, 7, 7, 7, 7, 7, 7});

  resize(A.vary_axes().with(std::move(mem)), {4, 4}, 3);

  CHECK(workspace == std::vector<int>{3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                                      3, 3, 3, 3, 3, 3, 7, 7, 7, 7});

  resize(A.vary_axes().with(default_reform_mode), {2, 2}, 1);

  CHECK(workspace == std::vector<int>{3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                                      3, 3, 3, 3, 3, 3, 7, 7, 7, 7});

  for (auto& val: A.span()) {
    val = 1;
  }

  CHECK(workspace == std::vector<int>{1, 1, 3, 3, 1, 1, 3, 3, 3, 3,
                                      3, 3, 3, 3, 3, 3, 7, 7, 7, 7});

#ifndef NDEBUG
  CHECK_THROWS(resize(A.vary_axes(0_i), 5, 333));
  CHECK_THROWS(resize(A.vary_axes(0_i).with(force_reallocate), 5, 333));
#endif
}

TEST_CASE("move memory into tensor", "[tensor][memory]")
{
  default_allocator alloc;

  auto mem = allocate_memory_for<int>(alloc, 12);

  auto data = [&] {
    int* ptr = static_cast<int*>(mem.ptr());
    return range(ptr, ptr + 12);
  }();

  for (int& val: data) {
    val = 7;
  }

  {
    matrix<int> A;

    reserve(A.vary_axes().with(std::move(mem)), {3, 4});

    resize(A.vary_axes().with(no_allocate), {2, 3}, 8);

    CHECK(
        std::vector<int>(data.begin(), data.end()) ==
        std::vector<int>({8, 8, 7, 8, 8, 7, 8, 8, 7, 7, 7, 7}));
  }
}

TEST_CASE("get memory out of tensor", "[tensor][memory]")
{
  default_allocator alloc;

  matrix<int> A;

  resize(A.vary_axes(), {2, 3}, 9);

  CHECK(A.size() == index(2, 3));
  CHECK(A.capacity() == index(2, 3));

  auto mem = A.release();

  CHECK(A.size() == index<2>(zero));
  CHECK(A.capacity() == index<2>(zero));

  SECTION("release")
  {
    int* ptr = static_cast<int*>(mem.release());

    CHECK(
        std::vector<int>(ptr, ptr + 6) == std::vector<int>({9, 9, 9, 9, 9, 9}));

    deallocate_for<int>(alloc, ptr, 6);
  }
  SECTION("dtor")
  {
    const int* ptr = static_cast<int*>(mem.ptr());

    CHECK(
        std::vector<int>(ptr, ptr + 6) == std::vector<int>({9, 9, 9, 9, 9, 9}));
  }
}

struct my_type {
  double d alignas(128);
};

TEST_CASE(
    "use same memory in tensors with different alignment", "[tensor][memory]")
{
  using T1 = short;
  using T2 = my_type;

  static_assert(alignof(T1) < alignof(T2));

  constexpr index_t fact = alignof(T2) / alignof(T1);

  static_assert(alignof(T1) * fact == alignof(T2));

  constexpr auto Al = alignment_of<T1, T2>;

  context<allocator_tracer_monitor> ats;

  using Alloc = allocator_tracer<over_aligned_allocator<Al>>;

  constexpr auto UsedAl = Alloc::alignment(Al);

  matrix<T1, Alloc> A;

  CHECK(ats.num_active() == 0);
  CHECK(ats.log().size() == 0);

  resize(A.vary_axes(), {3, fact * 2}, T1{});

  CHECK(ats.num_active() == 1);
  REQUIRE(ats.log().size() == 1);
  CHECK(ats.log().at(0).satisfies(
      memory_operation::allocate, 3 * fact * 2 * sizeof(T1), UsedAl));

  auto mem = A.release();

  CHECK(ats.num_active() == 1);
  CHECK(ats.log().size() == 1);

  {
    matrix<T2, Alloc> B;

    CHECK(ats.num_active() == 1);
    CHECK(ats.log().size() == 1);

    resize(B.vary_axes().with(std::move(mem)), {3, 2}, T2{});

    CHECK(ats.num_active() == 1);
    CHECK(ats.log().size() == 1);
  }

  CHECK(ats.num_active() == 0);
  CHECK(ats.log().size() == 2);

  CHECK(ats.log().at(1).satisfies(
      memory_operation::deallocate, 3 * 2 * sizeof(T2), UsedAl));
}
