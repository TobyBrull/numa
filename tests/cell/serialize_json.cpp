#include "numa/cell/serialize_json.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/make_cell.hpp"
#include "numa/cell/structure.hpp"
#include "numa/cell/t_span_adaptor_mirror.hpp"
#include "numa/cell/t_span_adaptor_triangular.hpp"
#include "numa/cell/tensor.hpp"
#include "numa/cell/tensor_fixed_capacity.hpp"
#include "numa/cell/tensor_fixed_size.hpp"
#include "numa/cell/tensor_linear.hpp"
#include "numa/cell/tensor_sparse.hpp"
#include "numa/cell/tensor_staged.hpp"

#include "numa/core/functors.hpp"
#include "numa/core/generators.hpp"

#include "numa/misc/data.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

TEST_CASE(
    "serialize-json for span", "[tensor][serialize_json][from_json][to_json]")
{
  auto A = make_cell<tensor<int, 2>>(3, 2)(g_iota(2));

  std::stringstream ss;

  to_json(ss, A);

  CHECK(
      ss.str() ==
      "{\n"
      "  \"size\": [3, 2],\n"
      "  \"values\":\n"
      "    [[2, 3, 4],\n"
      "     [5, 6, 7]\n"
      "    ]\n"
      "}");

  auto B = make_cell<tensor<int, 2>>(A.size())();

  const auto Bs = B.span();

  from_json(ss, Bs);

  CHECK(A == B);

  from_json(
      "   {\n\n\n"
      "  \"size\" \n   : [   3, \n 2],\n"
      "  \"values\":\n"
      "    [\n[20, 30, 40],\n"
      "     [50, \n\n 60, 70]\n"
      "    ]\n"
      "}  \n   \n",
      Bs);

  A *= 10;

  CHECK(A == B);
}

TEST_CASE("serialize-json on tensor", "[tensor][serialize_json]")
{
  const auto data = std_iota_vector<int>(300, 0);

  {
    t_span<const int, 0> ts(data.data() + 3, {}, {});

    CHECK(
        to_json(ts) ==
        "{\n"
        "  \"size\": [],\n"
        "  \"values\":\n"
        "    3\n"
        "}");
  }

  {
    t_span<const int, 1> ts(data.data(), 0, 1);

    CHECK(
        to_json(ts) ==
        "{\n"
        "  \"size\": [0],\n"
        "  \"values\":\n"
        "    []\n"
        "}");
  }

  {
    t_span<const int, 1> ts(data.data(), 4, 3);

    CHECK(
        to_json(ts) ==
        "{\n"
        "  \"size\": [4],\n"
        "  \"values\":\n"
        "    [0, 3, 6, 9]\n"
        "}");
  }

  {
    t_span<const int, 3> ts(data.data(), {2, 3, 2}, {3, 20, 100});

    CHECK(
        to_json(ts) ==
        "{\n"
        "  \"size\": [2, 3, 2],\n"
        "  \"values\":\n"
        "    [[[0, 3],\n"
        "      [20, 23],\n"
        "      [40, 43]\n"
        "     ],\n"
        "     [[100, 103],\n"
        "      [120, 123],\n"
        "      [140, 143]\n"
        "     ]\n"
        "    ]\n"
        "}");
  }

  {
    t_span<const int, 5> ts(data.data() + 7, {3, 2, 2, 3, 2}, {0, 0, 0, 0, 0});

    CHECK(
        to_json(ts) ==
        "{\n"
        "  \"size\": [3, 2, 2, 3, 2],\n"
        "  \"values\":\n"
        "    [[[[[7, 7, 7],\n"
        "        [7, 7, 7]\n"
        "       ],\n"
        "       [[7, 7, 7],\n"
        "        [7, 7, 7]\n"
        "       ]\n"
        "      ],\n"
        "      [[[7, 7, 7],\n"
        "        [7, 7, 7]\n"
        "       ],\n"
        "       [[7, 7, 7],\n"
        "        [7, 7, 7]\n"
        "       ]\n"
        "      ],\n"
        "      [[[7, 7, 7],\n"
        "        [7, 7, 7]\n"
        "       ],\n"
        "       [[7, 7, 7],\n"
        "        [7, 7, 7]\n"
        "       ]\n"
        "      ]\n"
        "     ],\n"
        "     [[[[7, 7, 7],\n"
        "        [7, 7, 7]\n"
        "       ],\n"
        "       [[7, 7, 7],\n"
        "        [7, 7, 7]\n"
        "       ]\n"
        "      ],\n"
        "      [[[7, 7, 7],\n"
        "        [7, 7, 7]\n"
        "       ],\n"
        "       [[7, 7, 7],\n"
        "        [7, 7, 7]\n"
        "       ]\n"
        "      ],\n"
        "      [[[7, 7, 7],\n"
        "        [7, 7, 7]\n"
        "       ],\n"
        "       [[7, 7, 7],\n"
        "        [7, 7, 7]\n"
        "       ]\n"
        "      ]\n"
        "     ]\n"
        "    ]\n"
        "}");
  }

  {
    const auto A = make_cell<tensor<int, 2>>(4, 2)(g_iota(6));

    const auto B = make_from_json<matrix<int>>(
        R"({ "size": [4, 2],
             "values": [
               [6, 7, 8, 9],
               [10, 11, 12, 13]
             ] } )");

    CHECK(A == B);
  }
}

TEST_CASE(
    "serialize-json on tensor of strings", "[tensor][serialize_json][string]")
{
  const auto data = std_iota_vector_string(8, 10);

  t_span<const std::string, 2> ts(data.data(), {2, 3}, {1, 2});

  CHECK(
      to_json(ts) ==
      "{\n"
      "  \"size\": [2, 3],\n"
      "  \"values\":\n"
      "    [[\"10\", \"11\"],\n"
      "     [\"12\", \"13\"],\n"
      "     [\"14\", \"15\"]\n"
      "    ]\n"
      "}");

  CHECK(
      ts ==
      make_from_json<matrix<std::string>>(
          R"({
           "size": [2, 3],
           "values":
             [["10", "11"],
              ["12", "13"],
              ["14", "15"]
             ]
         })")
          .span());
}

TEST_CASE("serialize-json on band-tensor", "[tensor_staged][serialize_json]")
{
  {
    const auto data = std_iota_vector<int>(60, 0);

    matrix_span_staged<const int, 2> ms(data.data() + 3, 6, {1, 4});

    CHECK(
        to_json(ms, 1) ==
        "{\n"
        "   \"size\": [6],\n"
        "   \"values\":\n"
        "     [[1, 2, 3, 4, 5],\n"
        "      [6, 7, 8, 9, 10],\n"
        "      [11, 12, 13, 14, 15],\n"
        "      [16, 17, 18, 19, 20],\n"
        "      [21, 22, 23, 24, 25],\n"
        "      [26, 27, 28, 29, 30]\n"
        "     ]\n"
        " }");
  }

  {
    const auto A = make_cell<matrix_staged<int, 1>>(5)(g_iota(2));

    const auto B = make_from_json<matrix_staged<int, 1>>(
        R"({
          "size": [5],
          "values":
            [[2, 3, 4],
             [5, 6, 7],
             [8, 9, 10],
             [11, 12, 13],
             [14, 15, 16]
            ]"
        })");

    CHECK(A == B);
  }
}

TEST_CASE(
    "index-based and iterator-based serialize-json on band-tensor",
    "[tensor_staged][serialize_json]"
    "[iterator_based_to_json]")
{
  const auto vec = std_iota_vector<int>(30, 0);

  matrix_span_staged<const int, 2> bms(vec.data() + 2, 6, {1, 4});

  CHECK(
      to_json(bms) ==
      "{\n"
      "  \"size\": [6],\n"
      "  \"values\":\n"
      "    [[0, 1, 2, 3, 4],\n"
      "     [5, 6, 7, 8, 9],\n"
      "     [10, 11, 12, 13, 14],\n"
      "     [15, 16, 17, 18, 19],\n"
      "     [20, 21, 22, 23, 24],\n"
      "     [25, 26, 27, 28, 29]\n"
      "    ]\n"
      "}");
}

TEST_CASE(
    "serialize-json on band-tensor-form-span",
    "[tensor_staged][serialize_json][tensor_staged_form_span]")
{
  const auto vec = std_iota_vector<int>(30, 0);

  matrix_span_staged<const int, 2> bms(vec.data() + 2, 6, {1, 4});

  {
    const auto fs = bms.axes().sub(as_size, 4, 1);

    CHECK(
        to_json(fs) ==
        "{\n"
        "  \"size\": [1],\n"
        "  \"values\":\n"
        "    [[20, 21, 22, 23, 24]\n"
        "    ]\n"
        "}");
  }

  {
    const auto fs = bms.axes().sub(as_till, 2, 4);

    CHECK(
        to_json(fs) ==
        "{\n"
        "  \"size\": [2],\n"
        "  \"values\":\n"
        "    [[10, 11, 12, 13, 14],\n"
        "     [15, 16, 17, 18, 19]\n"
        "    ]\n"
        "}");
  }
}

TEST_CASE(
    "serialize-json on t_span_adaptor_mirror",
    "[t_span_adaptor_mirror][serialize_json]")
{
  {
    auto A = make_cell<tensor<int, 4>>(2, 2, 2, 2)();
    for_each(upper(A))(g_iota(0));

    CHECK(
        to_json(upper(A)) ==
        "{\n"
        "  \"size\": [2, 2, 2, 2],\n"
        "  \"values\":\n"
        "    [[[[0]\n"
        "      ],\n"
        "      [[1, 2]\n"
        "      ]\n"
        "     ],\n"
        "     [[[3, 4],\n"
        "       [5]\n"
        "      ],\n"
        "      [[6, 7],\n"
        "       [8, 9]\n"
        "      ]\n"
        "     ]\n"
        "    ]\n"
        "}");
  }

  {
    auto A = make_cell<matrix<int>>(4, 4)();
    for_each(upper(A))(g_iota(3));

    const auto B = make_from_json<matrix<int>>(
        R"({ "size": [4, 4],
             "values": [[3, 0, 0, 0],"
                       "[4, 5, 0, 0],"
                       "[6, 7, 8, 0],"
                       "[9, 10, 11, 12]] })");

    CHECK(A == B);
  }
}

TEST_CASE("serialize-json on sparse-tensor", "[tensor_sparse][serialize_json]")
{
  const auto ms = [] {
    sparse_matrix<int> retval;

    resize(retval.vary_axes(), index(4, 3));

    retval.push_back(index(2, 0), 3);
    retval.push_back(index(3, 0), 1);
    retval.push_back(index(1, 2), 4);
    retval.push_back(index(3, 0), 2);
    retval.push_back(index(3, 2), 7);

    return retval;
  }();

  {
    CHECK(
        to_json(ms, 2) ==
        "{\n"
        "    \"size\": [4, 3],\n"
        "    \"storage_size\": 5,\n"
        "    \"indices\": [[2, 0], [3, 0], [1, 2], [3, 0], [3, 2]],\n"
        "    \"values\": [3, 1, 4, 2, 7]\n"
        "  }");
  }

  {
    const char* str = R"( {
        "size": [4, 3],
        "storage_size": 5,
        "indices": [[2, 0], [3, 0], [1, 2], [3, 0], [3, 2]],
        "values": [3, 1, 4, 2, 7] } )";

    auto B = make_from_json<sparse_matrix<int>>(str);

    CHECK(ms == B);

    const auto [ind_begin, ind_end] = B.indices().as_tuple();
    std::fill(ind_begin, ind_end, index(1, 1));

    const auto [val_begin, val_end] = B.values().as_tuple();
    std::fill(val_begin, val_end, 3);

    CHECK(ms != B);

    from_json(std::string_view(str), B);

    CHECK(ms == B);
  }
}

TEST_CASE("serialize-json on lin-ranges", "[lin_range][serialize_json]")
{
  vector_linear_incr<double> lri(11, 5.0, 0.5);

  CHECK(
      to_json(lri.unvaried()) ==
      "{\n"
      "  \"size\": [11],\n"
      "  \"front\": 5,\n"
      "  \"incr\": 0.5\n"
      "}");

  const char* lin_range_str =
      R"({
        "size": [11],
        "front": 10.0,
        "incr": 1.5
      })";

  const auto check_lri = [](const vector_linear_incr<double>& lri) {
    CHECK(lri.size() == index(11));
    CHECK(lri.front() == 10.0);
    CHECK(lri.incr() == 1.5);
  };

  from_json(lin_range_str, lri);
  check_lri(lri);

  check_lri(make_from_json<vector_linear_incr<double>>(lin_range_str));
}

TEST_CASE("nested cells", "[serialize_json][nested]")
{
  vector<vector<int>> vv;

  resize(vv.vary_axes(0_i), 4);

  resize(vv.at(0).vary_axes(0_i), 3);
  resize(vv.at(1).vary_axes(0_i), 4);
  resize(vv.at(2).vary_axes(0_i), 2);
  resize(vv.span().at(3).vary_axes(0_i), 1);

  (vv.at(0)).at(1) = 7;

  CHECK(
      to_json(vv) ==
      "{\n"
      "  \"size\": [4],\n"
      "  \"values\":\n"
      "    [{\n"
      "      \"size\": [3],\n"
      "      \"values\":\n"
      "        [0, 7, 0]\n"
      "    }, {\n"
      "      \"size\": [4],\n"
      "      \"values\":\n"
      "        [0, 0, 0, 0]\n"
      "    }, {\n"
      "      \"size\": [2],\n"
      "      \"values\":\n"
      "        [0, 0]\n"
      "    }, {\n"
      "      \"size\": [1],\n"
      "      \"values\":\n"
      "        [0]\n"
      "    }]\n"
      "}");

  from_json(
      "{"
      "  \"size\": [4],"
      "  \"values\":"
      "    [{"
      "      \"size\": [3],"
      "      \"values\":"
      "        [2, 3, 4]"
      "    }, {"
      "      \"size\": [4],"
      "      \"values\":"
      "        [10, 11, 12, 13]"
      "    }, {"
      "      \"size\": [2],"
      "      \"values\":"
      "        [20, 21]"
      "    }, {"
      "      \"size\": [1],"
      "      \"values\":"
      "        [100]"
      "    }]"
      "}",
      vv);

  {
    const auto& s = vv.at(0);
    CHECK(s.at(0) == 2);
    CHECK(s.at(1) == 3);
    CHECK(s.at(2) == 4);
  }

  {
    const auto& s = vv.at(1);
    CHECK(s.at(0) == 10);
    CHECK(s.at(1) == 11);
    CHECK(s.at(2) == 12);
    CHECK(s.at(3) == 13);
  }

  {
    const auto& s = vv.at(2);
    CHECK(s.at(0) == 20);
    CHECK(s.at(1) == 21);
  }

  {
    const auto& s = vv.at(3);
    CHECK(s.at(0) == 100);
  }
}

TEST_CASE("serialize-json for structure", "[serialize_json][structure]")
{
  using sub_struct = structure<
      field<m_main_t, vector<std::string>, m_rows_t>,
      field<m_data_t, vector<char>, m_rows_t>>;

  using test_struct = structure<
      field<m_a_t, vector<int>, m_rows_t>,
      field<m_b_t, matrix<double>, m_rows_t, m_cols_t>,
      field<m_c_t, sub_struct, m_cols_t>>;

  test_struct ts;

  resize(
      ts.vary_axes(),
      {2, 3},
      init<m_main_t>("numa"),
      init<m_data_t>('x'),
      init<m_a_t>(7),
      init<m_b_t>(2.75));

  ts.get(m_c_t{}).get(m_data_t{}).at(1) = 'y';
  ts.get(m_b_t{}).at(0, 2)              = 3.25;

  CHECK(
      to_json(ts) ==
      "{\n"
      "  \"size\": [2, 3],\n"
      "  \"a\": {\n"
      "    \"size\": [2],\n"
      "    \"values\":\n"
      "      [7, 7]\n"
      "  },\n"
      "  \"b\": {\n"
      "    \"size\": [2, 3],\n"
      "    \"values\":\n"
      "      [[2.75, 2.75],\n"
      "       [2.75, 2.75],\n"
      "       [3.25, 2.75]\n"
      "      ]\n"
      "  },\n"
      "  \"c\": {\n"
      "    \"size\": [3],\n"
      "    \"main\": {\n"
      "      \"size\": [3],\n"
      "      \"values\":\n"
      "        [\"numa\", \"numa\", \"numa\"]\n"
      "    },\n"
      "    \"data\": {\n"
      "      \"size\": [3],\n"
      "      \"values\":\n"
      "        [\"x\", \"y\", \"x\"]\n"
      "    }\n"
      "  }\n"
      "}");

  const char* structure_str = "{"
                              "  \"size\": [2,3],\"a\"   :       {"
                              "    \"size\": [2   ],"
                              "    \"values\":"
                              "      [2, 1]"
                              "  },"
                              "  \"b\": {"
                              "    \"size\": [2, 3],"
                              "    \"values\":"
                              "      [[8.000, 1.5],\n"
                              "       [4.25, 4],\n"
                              "       [3.25, -3]\n"
                              "      ]\n"
                              "  },"
                              "  \"c\": {"
                              "\"size\":[3],"
                              "    \"main\": {"
                              "      \"size\": [3],"
                              "      \"values\":"
                              "        [\"puma\", \"tuna\", \"fubar\"]"
                              "    },"
                              "    \"data\": {"
                              "      \"size\": [3],"
                              "      \"values\":"
                              "        [\"c\", \"i\", \"a\"]"
                              "    }"
                              "  }"
                              "}";

  const auto check_all_test_struct = [](const auto& strct) {
    {
      const auto& cc = strct.get(m_a_t{});

      CHECK(cc.at(0) == 2);
      CHECK(cc.at(1) == 1);
    }

    {
      const auto& cc = strct.get(m_b_t{});

      CHECK(cc.at(0, 0) == 8.00);
      CHECK(cc.at(1, 0) == 1.50);
      CHECK(cc.at(0, 1) == 4.25);
      CHECK(cc.at(1, 1) == 4.00);
      CHECK(cc.at(0, 2) == 3.25);
      CHECK(cc.at(1, 2) == -3.00);
    }

    {
      const auto& cc = strct.get(m_c_t{}).get(m_main_t{});

      CHECK(cc.at(0) == "puma");
      CHECK(cc.at(1) == "tuna");
      CHECK(cc.at(2) == "fubar");
    }

    {
      const auto& cc = strct.get(m_c_t{}).get(m_data_t{});

      CHECK(cc.at(0) == 'c');
      CHECK(cc.at(1) == 'i');
      CHECK(cc.at(2) == 'a');
    }
  };

  from_json(structure_str, ts);
  check_all_test_struct(ts);

  check_all_test_struct(make_from_json<test_struct>(structure_str));
}
