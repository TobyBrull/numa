#include "numa/cell/serialize_json.hpp"
#include "numa/cell/tensor_sparse.hpp"

#include "numa/cell/reform.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("sparse-tensor", "[tensor_sparse]")
{
  sparse_cuboid<int> u_cube;

  u_cube.storage_reserve(10);

  resize(u_cube.vary_axes(), index(10, 20, 30));

  u_cube.push_back(index(3, 2, 0), 3);
  u_cube.push_back(index(3, 1, 2), 4);
  u_cube.push_back(index(5, 10, 1), 5);

  CHECK(
      u_cube ==
      make_from_json<sparse_cuboid<int>>(
          "{"
          "  \"size\": [10, 20, 30],"
          "  \"storage_size\": 3,"
          "  \"indices\": [[3, 2, 0], [3, 1, 2], [5, 10, 1]],"
          "  \"values\": [3, 4, 5]"
          "}"));

  SECTION("resize")
  {
    SECTION("resize all")
    {
      resize(u_cube.vary_axes(), index(10, 20, 2));
    }
    SECTION("resize single")
    {
      resize(u_cube.vary_axes<2>(), index(2));
    }

    CHECK(
        u_cube ==
        make_from_json<sparse_cuboid<int>>(
            "{"
            "  \"size\": [10, 20, 2],"
            "  \"storage_size\": 2,"
            "  \"indices\": [[3, 2, 0], [5, 10, 1]],"
            "  \"values\": [3, 5]"
            "}"));
  }
  SECTION("conversion to ordered and back")
  {
    sparse_cuboid<int> cube(std::move(u_cube));
    cube.sort_strictly();

    CHECK(
        cube ==
        make_from_json<sparse_cuboid<int>>(
            "{"
            "  \"size\": [10, 20, 30],"
            "  \"storage_size\": 3,"
            "  \"indices\": [[3, 2, 0], [5, 10, 1], [3, 1, 2]],"
            "  \"values\": [3, 5, 4]"
            "}"));

    CHECK(u_cube.storage_size() == 0);
    CHECK(cube.storage_size() > 0);

    SECTION("iterator")
    {
      const auto it = cube.lower_bound(index(2, 8, 1));

      REQUIRE(it != cube.end());

      CHECK((*it) == 5);
    }
  }
  SECTION("makeing a copy")
  {
    const auto cpy = make_copy(u_cube);

    CHECK(
        cpy ==
        make_from_json<sparse_cuboid<int>>(
            "{"
            "  \"size\": [10, 20, 30],"
            "  \"storage_size\": 3,"
            "  \"indices\": [[3, 2, 0], [3, 1, 2], [5, 10, 1]],"
            "  \"values\": [3, 4, 5]"
            "}"));

    CHECK(cpy == u_cube);
  }
  SECTION("add and subtract")
  {
    sparse_cuboid<int> rhs;

    resize(rhs.vary_axes(), index(10, 20, 30));

    rhs.push_back(index(9, 9, 1), 30);
    rhs.push_back(index(5, 3, 2), 40);
    rhs.push_back(index(3, 2, 0), 50);

    u_cube.insert(rhs);

    CHECK(
        u_cube ==
        make_from_json<sparse_cuboid<int>>(
            "{"
            "  \"size\": [10, 20, 30],"
            "  \"storage_size\": 6,"
            "  \"indices\": [[3, 2, 0], [3, 1, 2], [5, 10, 1], "
            "[9, 9, 1], [5, 3, 2], [3, 2, 0]],"
            "  \"values\": [3, 4, 5, 30, 40, 50]"
            "}"));

    u_cube.insert(rhs, f_negate);

    CHECK(
        u_cube ==
        make_from_json<sparse_cuboid<int>>(
            "{"
            "  \"size\": [10, 20, 30],"
            "  \"storage_size\": 9,"
            "  \"indices\": [[3, 2, 0], [3, 1, 2], [5, 10, 1], "
            "[9, 9, 1], [5, 3, 2], [3, 2, 0], "
            "[9, 9, 1], [5, 3, 2], [3, 2, 0]],"
            "  \"values\": [3, 4, 5, 30, 40, 50, -30, -40, -50]"
            "}"));

    u_cube.sort_strictly();

    CHECK(
        u_cube ==
        make_from_json<sparse_cuboid<int>>(
            "{"
            "  \"size\": [10, 20, 30],"
            "  \"storage_size\": 5,"
            "  \"indices\": [[3, 2, 0], [9, 9, 1], [5, 10, 1], "
            "[3, 1, 2], [5, 3, 2]],"
            "  \"values\": [3, 0, 5, 4, 0]"
            "}"));
  }
}

TEST_CASE("sparse-tensor 2", "[tensor_sparse]")
{
  auto cube = [] {
    sparse_cuboid<int> retval;

    resize(retval.vary_axes(), index(10, 20, 30));

    retval.push_back(index(3, 2, 0), 3);
    retval.push_back(index(5, 10, 1), 5);
    retval.push_back(index(3, 1, 2), 4);

    return retval;
  }();

  SECTION("add and subtract")
  {
    sparse_cuboid<int> other;

    resize(other.vary_axes(), index(10, 20, 30));

    other.push_back(index(3, 4, 0), 3);
    other.push_back(index(4, 10, 1), 8);
    other.push_back(index(5, 10, 1), 7);
    other.push_back(index(6, 10, 1), 6);
    other.push_back(index(3, 1, 2), 2);

    {
      const auto result = simple_add_sorted_strictly(1, cube, 1, other);

      CHECK(
          result ==
          make_from_json<sparse_cuboid<int>>(
              "{"
              "  \"size\": [10, 20, 30],"
              "  \"storage_size\": 6,"
              "  \"indices\": [[3, 2, 0], [3, 4, 0], [4, 10, 1], "
              "[5, 10, 1], [6, 10, 1], [3, 1, 2]],"
              "  \"values\": [3, 3, 8, 12, 6, 6]"
              "}"));
    }

    {
      const sparse_cuboid<int> result =
          simple_add_sorted_strictly(1, cube, -1, other);

      CHECK(
          result ==
          make_from_json<sparse_cuboid<int>>(
              "{"
              "  \"size\": [10, 20, 30],"
              "  \"storage_size\": 6,"
              "  \"indices\": [[3, 2, 0], [3, 4, 0], [4, 10, 1], "
              "[5, 10, 1], [6, 10, 1], [3, 1, 2]],"
              "  \"values\": [3, -3, -8, -2, -6, 2]"
              "}"));
    }
  }
}

TEST_CASE(
    "transform(_indexed) and for_each(_indexed) "
    "for sparse-tensors",
    "[tensor_sparse][for_each][for_each_indexed]")
{
  const sparse_matrix<int> sm_int = [] {
    sparse_matrix<int> retval;

    resize(retval.vary_axes(), index(5, 8));

    retval.push_back(index(3, 7), 5);
    retval.push_back(index(4, 4), 4);
    retval.push_back(index(1, 2), 3);

    return retval;
  }();

  {
    int result = 0;
    for_each(sm_int)([&result](const int i) { result += i; });

    CHECK(result == 12);
  }

  {
    int result = 0;
    for_each(sm_int).indexed(
        [&result](auto& ind, const int i) { result += i + ind[1]; });

    CHECK(result == 25);
  }

  //  {
  //    const auto result =
  //    make_cell_sorted_strictly<sparse_matrix<std::string>> (sm_int) (
  //        [] (const int i) { return std::to_string (2 * i); });
  //
  //    static_assert (std::is_same_v<
  //      typename decay_t<decltype (result)>::value_type,
  //      std::string>);
  //
  //    CHECK (result == make_from_json<sparse_matrix<std::string>> (
  //        "{"
  //        "  \"size\": [5, 8],"
  //        "  \"storage_size\": 3,"
  //        "  \"indices\": [[3, 7], [4, 4], [1, 2]],"
  //        "  \"values\": [\"10\", \"8\", \"6\"]"
  //        "}"));
  //  }
  //
  //  {
  //    const auto result = transform_indexed (
  //      [] (const auto ind, const int i)
  //      {
  //        return static_cast<double> (i + ind [0] + ind [1]);
  //      }, sm_int.span ());
  //
  //    static_assert (std::is_same_v<
  //      typename decltype (result)::value_type, double>);
  //
  //    CHECK (result.span () ==
  //        make_from_json<unordered_sparse_matrix<double>> (
  //          "{"
  //          "  \"size\": [5, 8],"
  //          "  \"storage_size\": 3,"
  //          "  \"indices\": [[3, 7], [4, 4], [1, 2]],"
  //          "  \"values\": [15, 12, 6]"
  //          "}"));
  //  }
}

TEST_CASE(
    "for_each(_indexed) for tensor_sparses",
    "[tensor_sparse][for_each][for_each_indexed]")
{
  const sparse_matrix<int> sm_int = [] {
    sparse_matrix<int> retval;

    resize(retval.vary_axes(), index(5, 8));

    retval.push_back(index(1, 2), 3);
    retval.push_back(index(4, 4), 4);
    retval.push_back(index(3, 7), 5);

    return retval;
  }();

  const sparse_matrix<std::string> sm_str = [] {
    sparse_matrix<std::string> retval;

    retval.vary_axes().resize(index(5, 8));

    retval.push_back(index(0, 2), "10");
    retval.push_back(index(1, 2), "20");
    retval.push_back(index(2, 2), "50");
    retval.push_back(index(2, 4), "30");
    retval.push_back(index(2, 6), "40");
    retval.push_back(index(3, 7), "40");

    return retval;
  }();

  const sparse_matrix<double> sm_dbl = [] {
    sparse_matrix<double> retval;

    resize(retval.vary_axes(), index(5, 8));

    retval.push_back(index(1, 2), 8.0);
    retval.push_back(index(2, 2), 2.0);
    retval.push_back(index(2, 6), 5.0);
    retval.push_back(index(4, 7), 2.0);

    return retval;
  }();

  {
    int result = 0;

    for_each(sm_int, sm_str)([&result](const int* i, const auto* s) {
      if (i != nullptr) {
        result += (*i);
      }

      if (s != nullptr) {
        result += std::stoi(*s);
      }
    });

    CHECK(result == 202);
  }

  {
    int result = 0;

    for_each(sm_int, sm_str)
        .indexed([&result](const auto ind, const int* i, const auto* s) {
          if (i != nullptr) {
            result += (*i);
          }

          if (s != nullptr) {
            result += std::stoi(*s);
          }

          result += ind[0] + ind[1];
        });

    CHECK(result == 243);
  }

  //  {
  //    const auto result =
  //      transform ([] (const int *i, const std::string *s, const double *d)
  //                {
  //                  int retval = 0;
  //
  //                  if (i != nullptr)
  //                  {
  //                    retval += (*i);
  //                  }
  //
  //                  if (s != nullptr)
  //                  {
  //                    retval += std::stoi (*s);
  //                  }
  //
  //                  if (d != nullptr)
  //                  {
  //                    retval += static_cast<int> (*d);
  //                  }
  //
  //                  return std::to_string (retval);
  //                }, sm_int.span (), sm_str.span (), sm_dbl.span ());
  //
  //    static_assert (std::is_same_v<
  //      typename decltype (result)::value_type, std::string>);
  //
  //    CHECK (result.span () == make_from_json<sparse_matrix<std::string>> (
  //      "{"
  //      "  \"size\": [5, 8],"
  //      "  \"storage_size\": 8,"
  //      "  \"indices\": [[0, 2], [1, 2], [2, 2], [2, 4], "
  //      "                [4, 4], [2, 6], [3, 7], [4, 7]],"
  //      "  \"values\": [\"10\", \"31\", \"52\", \"30\", "
  //      "               \"4\", \"45\", \"45\", \"2\"]"
  //      "}"));
  //  }
  //
  //  {
  //    const auto result =
  //      transform_indexed (
  //        [] (const index<2>& ind, const int *i,
  //            const std::string *s, const double *d)
  //        {
  //          int retval = 0;
  //
  //          if (i != nullptr)
  //          {
  //            retval += (*i);
  //          }
  //
  //          if (s != nullptr)
  //          {
  //            retval += std::stoi (*s);
  //          }
  //
  //          if (d != nullptr)
  //          {
  //            retval += static_cast<int> (*d);
  //          }
  //
  //          return static_cast<double> (retval + ind [0]);
  //        }, sm_int.span (), sm_str.span (), sm_dbl.span ());
  //
  //    static_assert (std::is_same_v<
  //      typename decltype (result)::value_type, double>);
  //
  //    CHECK (result.span () == make_from_json<sparse_matrix<double>> (
  //      "{"
  //      "  \"size\": [5, 8],"
  //      "  \"storage_size\": 8,"
  //      "  \"indices\": [[0, 2], [1, 2], [2, 2], [2, 4], "
  //                      "[4, 4], [2, 6], [3, 7], [4, 7]],"
  //      "  \"values\": [10, 32, 54, 32, 8, 47, 48, 6]"
  //      "}"));
  //  }
}

TEST_CASE("sparse-tensor reforms", "[tensor_sparse][reform]")
{
  {
    sparse_matrix<int> sm;
    sm.vary_axes().resize_hard(index(4, 4));
    sm.push_back(index(2, 1), 4);
    sm.push_back(index(1, 2), 11);
    sm.push_back(index(2, 2), 6);
    sm.push_back(index(3, 2), 19);
    sm.push_back(index(0, 3), 1);

    SECTION("no-ops")
    {
      reserve(sm.vary_axes(), index(10, 10));

      reserve_hard(sm.vary_axes(), index(10, 10));

      shrink_to_fit(sm.vary_axes());

      CHECK(sm.size() == index<2>(4, 4));
      CHECK(sm.storage_size() > 0);
      CHECK(sm.storage_capacity() > 0);
    }
    SECTION("clear")
    {
      clear(sm.vary_axes());

      CHECK(sm.size() == index<2>(zero));
      CHECK(sm.storage_size() == 0);
      CHECK(sm.storage_capacity() > 0);
    }
    SECTION("clear_hard")
    {
      clear_hard(sm.vary_axes());

      CHECK(sm.size() == index<2>(zero));
      CHECK(sm.storage_size() == 0);
      CHECK(sm.storage_capacity() == 0);
    }
    SECTION("insert-size")
    {
      CHECK(
          sm ==
          make_from_json<sparse_matrix<int>>(
              "{"
              "  \"size\": [4, 4],"
              "  \"storage_size\": 5,"
              "  \"indices\": [[2, 1], [1, 2], [2, 2], [3, 2], [0, 3]],"
              "  \"values\": [4, 11, 6, 19, 1]"
              "}"));

      insert_size(sm.vary_axes(1_i, 0_i), {3, 1}, {2, 1});

      CHECK(
          sm ==
          make_from_json<sparse_matrix<int>>(
              "{"
              "  \"size\": [5, 6],"
              "  \"storage_size\": 5,"
              "  \"indices\": [[3, 1], [2, 2], [3, 2], [4, 2], [0, 5]],"
              "  \"values\": [4, 11, 6, 19, 1]"
              "}"));

      insert_size(sm.vary_axes(1_i), 2, 2);

      CHECK(
          sm ==
          make_from_json<sparse_matrix<int>>(
              "{"
              "  \"size\": [5, 8],"
              "  \"storage_size\": 5,"
              "  \"indices\": [[3, 1], [2, 4], [3, 4], [4, 4], [0, 7]],"
              "  \"values\": [4, 11, 6, 19, 1]"
              "}"));
    }
    SECTION("erase-size")
    {
      erase_size(sm.vary_axes(1_i, 0_i), {2, 1}, {1, 1});

      CHECK(
          sm ==
          make_from_json<sparse_matrix<int>>("{"
                                             "  \"size\": [3, 3],"
                                             "  \"storage_size\": 2,"
                                             "  \"indices\": [[1, 1], [0, 2]],"
                                             "  \"values\": [4, 1]"
                                             "}"));

      erase_size(sm.vary_axes(0_i), {1}, {1});

      CHECK(
          sm ==
          make_from_json<sparse_matrix<int>>("{"
                                             "  \"size\": [2, 3],"
                                             "  \"storage_size\": 1,"
                                             "  \"indices\": [[0, 2]],"
                                             "  \"values\": [1]"
                                             "}"));
    }
    SECTION("erase-if")
    {
      sm.erase_if(
          [](const auto&, const auto& val) { return ((val % 2) == 0); });

      CHECK(
          sm ==
          make_from_json<sparse_matrix<int>>(
              "{"
              "  \"size\": [4, 4],"
              "  \"storage_size\": 3,"
              "  \"indices\": [[1, 2], [3, 2], [0, 3]],"
              "  \"values\": [11, 19, 1]"
              "}"));
    }
  }
}
