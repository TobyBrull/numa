#include "numa/cell/serialize_json.hpp"
#include "numa/cell/tensor_sparse.hpp"

#include "numa/cell/reform.hpp"
#include "numa/core/algorithms.hpp"

#include "catch2/catch.hpp"

using namespace numa;

auto
make_test_sparse_cuboid()
{
  sparse_cuboid<int> retval;

  resize(retval.vary_axes(), index(4, 2, 3));

  retval.push_back(index(2, 0, 0), 8);
  retval.push_back(index(3, 0, 0), 3);
  retval.push_back(index(1, 0, 2), 5);
  retval.push_back(index(3, 1, 2), -2);

  return retval;
}

TEST_CASE(
    "sparse-tensor basic operators", "[tensor_sparse][operators][for_each]")
{
  static_assert(sparse_cuboid<int>::axes_info.num_dims == 3);
  static_assert(sparse_matrix<int>::axes_info.num_dims == 2);
  static_assert(sparse_vector<int>::axes_info.num_dims == 1);

  auto cs = make_test_sparse_cuboid();

  cs *= 3;

  CHECK(equal(cs.values(), std::vector<int>({24, 9, 15, -6})));

  cs /= 2;

  CHECK(equal(cs.values(), std::vector<int>({12, 4, 7, -3})));

  {
    std::vector<index<3>> vec;

    for_each(cs).indexed([&vec](const index<3>& ind, int& i) {
      vec.push_back(ind);
      i = 1;
    });

    CHECK(equal(vec, cs.indices()));
  }

  CHECK(equal(cs.values(), std::vector<int>({1, 1, 1, 1})));
}

TEST_CASE("sparse-tensor comparison", "[tensor_sparse][equal][compare]")
{
  auto cs_a = make_test_sparse_cuboid();
  auto cs_b = make_copy(cs_a);

  CHECK(cs_a == cs_b);
  CHECK(equal(cs_a.indices(), cs_b.indices()));
  CHECK(equal(cs_a.values(), cs_b.values()));

  CHECK(cs_a.size() == cs_b.size());
  CHECK(cs_a.size() == index(4, 2, 3));

  cs_b.indices().begin()[1][1] = 1;
  CHECK(cs_a != cs_b);
  cs_b.indices().begin()[1][1] = 0;
  CHECK(cs_a == cs_b);

  cs_b.values().begin()[3] = -3;
  CHECK(cs_a != cs_b);
  cs_b.values().begin()[3] = -2;
  CHECK(cs_a == cs_b);

  cs_b.storage_resize(3);
  CHECK(cs_a != cs_b);
  cs_b.storage_resize(4, index(3, 1, 2), -2);
  CHECK(cs_a == cs_b);

  for_each(cs_a).indexed([](auto& ind, auto& val) {
    --(ind[0]);
    val *= 2;
  });

  CHECK(equal(
      cs_a.indices(),
      std::vector<index<3>>(
          {index(1, 0, 0), index(2, 0, 0), index(0, 0, 2), index(2, 1, 2)})));
  CHECK(equal(cs_a.values(), std::vector<int>({16, 6, 10, -4})));
}

TEST_CASE("sparse-tensor iterators", "[tensor_sparse][iterator]")
{
  auto cs_a = make_test_sparse_cuboid();

  REQUIRE(cs_a.is_sorted_strictly());

  SECTION("section one")
  {
    const auto it = cs_a.lower_bound(index(2, 0, 0));

    REQUIRE(it == cs_a.begin());

    const auto& ref = (*it);

    CHECK(it.pos() == index(2, 0, 0));
    CHECK((*it) == 8);
    CHECK(ref == 8);
  }
  SECTION("section two")
  {
    auto it = cs_a.lower_bound(index(-2, 1, 1));

    REQUIRE(it == cs_a.begin() + 2);

    CHECK(it.pos() == index(1, 0, 2));
    CHECK((*it) == 5);

    (*it) = 2;

    CHECK((*it) == 2);

    CHECK(equal(cs_a.values(), std::vector<int>({8, 3, 2, -2})));
  }
  SECTION("section three")
  {
    const auto it = cs_a.lower_bound(index(3, 4, 2));

    CHECK(it == cs_a.end());
  }
}

TEST_CASE(
    "sparse-tensor for-each with non-void-functor",
    "[tensor_sparse][for_each][non_void_functor]")
{
  auto cs = make_test_sparse_cuboid();

  for_each(cs)(f_square);

  CHECK(equal(cs.values(), std::vector<int>({64, 9, 25, 4})));

  int result = 0;

  for_each(cs).indexed([&](const auto& ind, int i) {
    result += i;

    return for_each_continue{(ind[2] < 1)};
  });

  CHECK(result == 98);
}

TEST_CASE(
    "sparse-tensor for-each with advanced non-void-functor",
    "[tensor_sparse][for_each][non_void_functor]")
{
  auto cs_a = make_test_sparse_cuboid();

  sparse_cuboid<int> cs_b;

  resize(cs_b.vary_axes(), index(4, 2, 3));

  cs_b.push_back(index(1, 0, 0), 9);
  cs_b.push_back(index(2, 0, 0), -4);
  cs_b.push_back(index(1, 0, 2), -2);
  cs_b.push_back(index(3, 0, 2), 5);
  cs_b.push_back(index(3, 1, 2), 1);

  {
    std::string result;

    for_each(cs_a, cs_b)([&result](int* p_i, int* p_j) {
      if (p_i != nullptr) {
        result += std::to_string(*p_i);
      }

      result += ',';

      if (p_j != nullptr) {
        result += std::to_string(*p_j);
      }

      result += ';';

      return (p_i == nullptr) || (p_j == nullptr);
    });

    CHECK(result == ",9;8,-4;");
  }
}

template<typename T, index_t N, typename X>
constexpr index_t
sparse_required_bytes(const index_t storage_size)
{
  return tensor_sparse<T, N, X>::required_bytes(storage_size);
}

TEST_CASE("sparse-tensor required-bytes", "[tensor_sparse][required_bytes]")
{
  static_assert(sparse_required_bytes<double, 2, int32_t>(10) == 160);
  static_assert(sparse_required_bytes<double, 3, int64_t>(10) == 320);
  static_assert(sparse_required_bytes<float, 2, int64_t>(20) == 400);
  static_assert(sparse_required_bytes<double, 2, int64_t>(10) == 240);
  static_assert(sparse_required_bytes<float, 2, int32_t>(10) == 120);
}
