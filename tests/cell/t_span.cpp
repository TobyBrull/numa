#include "numa/cell/t_span.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/serialize_json.hpp"

#include "numa/misc/data.hpp"

#include "catch2/catch.hpp"

#include <numeric>

using namespace numa;
using namespace numa::misc;

TEST_CASE("tensor-span concepts", "[t_span][concepts]")
{
  static_assert(Span<matrix_span<double>>);
  static_assert(Span<t_span<double, 1>>);

  t_span<int const, 2> const_span;
  t_span<int, 2> span;

  const_span = span;
}

TEST_CASE("tensor-span input/output", "[t_span][io]")
{
  auto const data = std_iota_vector<int>(300, 0);

  {
    t_span<const int, 0> ts(data.data() + 3, {}, {});

    CHECK(ts.at() == 3);
  }

  {
    t_span<const int, 1> ts(data.data(), 0, 1);

    CHECK(ts.size() == index(0));
  }

  {
    t_span<const int, 1> ts(data.data(), 4, 3);

    CHECK(ts.size() == index(4));

    CHECK(ts.at(0) == 0);
    CHECK(ts.at(1) == 3);
    CHECK(ts.at(2) == 6);
    CHECK(ts.at(3) == 9);
  }

  {
    t_span<const int, 2> ts(data.data(), {2, 3}, {2, 10});

    CHECK(ts.size() == index(2, 3));

    CHECK(ts.at(0, 0) == 0);
    CHECK(ts.at(1, 0) == 2);
    CHECK(ts.at(0, 1) == 10);
    CHECK(ts.at(1, 1) == 12);
    CHECK(ts.at(0, 2) == 20);
    CHECK(ts.at(1, 2) == 22);
  }

  {
    t_span<const int, 2> ts(data.data(), {2, 3}, {10, 2});

    CHECK(ts.at(zero) == 0);
    CHECK(ts.at(1, 0) == 10);
    CHECK(ts.at(0, 1) == 2);
    CHECK(ts.at(index(1, 1)) == 12);
    CHECK(ts.at(index<2>(0, 2)) == 4);
    CHECK(ts.at(1, 2) == 14);

    CHECK(ts.is_bounded(1, 1));
    CHECK(ts.is_bounded(1, 2));
    CHECK(!ts.is_bounded(2, 2));
    CHECK(!ts.is_bounded(-1, 0));
    CHECK(ts(1, 1) == 12);
    CHECK(ts[index(1, 1)] == 12);
    CHECK(ts.at(1, 1) == 12);
    CHECK(*ts.get_ptr(1, 1) == 12);
    CHECK(*ts.get_ptr_checked(1, 1) == 12);
  }

  {
    t_span<const int, 3> ts(data.data(), {2, 3, 2}, {3, 20, 100});

    CHECK(ts.at(0, 0, 0) == 0);
    CHECK(ts.at(1, 0, 0) == 3);
    CHECK(ts.at(0, 1, 0) == 20);
    CHECK(ts.at(1, 1, 0) == 23);
    CHECK(ts.at(0, 2, 0) == 40);
    CHECK(ts.at(1, 2, 0) == 43);
    CHECK(ts.at(0, 0, 1) == 100);
    CHECK(ts.at(1, 0, 1) == 103);
    CHECK(ts.at(0, 1, 1) == 120);
    CHECK(ts.at(one) == 123);
    CHECK(ts.at(0, 2, 1) == 140);
    CHECK(ts.at(1, 2, 1) == 143);
  }
}

TEST_CASE("t_span indexing", "[t_span][subscript][indexing]")
{
  auto const data = std_iota_vector<int>(100, 0);

  {
    scalar_span<const int> ss(data.data() + 3, {}, {});

    CHECK((ss[{}]) == 3);
    CHECK(ss() == 3);
    CHECK(ss.at() == 3);
    CHECK(ss.at(zero) == 3);
  }

  {
    vector_span<const int> vs(data.data(), 4, 3);

    CHECK((vs[{2}]) == 6);
    CHECK((vs[2]) == 6);
    CHECK(vs(2) == 6);
    CHECK(vs.at(2) == 6);
    CHECK(vs.at(2) == 6);

    CHECK(vs[zero] == 0);
    CHECK(vs[iota] == 0);
    CHECK(vs[one] == 3);
    CHECK(vs(unit, 0) == 3);

    CHECK(vs.at(zero) == 0);
    CHECK(vs.at(iota) == 0);
    CHECK(vs.at(one) == 3);
    CHECK(vs.at(unit, 0) == 3);
  }

  {
    matrix_span<const int> ms(data.data(), {2, 3}, {2, 10});

    CHECK((ms[{1, 2}]) == 22);
    CHECK(ms(1, 2) == 22);
    CHECK(ms.at(index{1, 2}) == 22);
    CHECK(ms.at(1, 2) == 22);

    CHECK(ms[zero] == 0);
    CHECK(ms[iota] == 10);
    CHECK(ms[one] == 12);
    CHECK((ms[{unit, 1}]) == 10);

    CHECK(ms.at(zero) == 0);
    CHECK(ms.at(iota) == 10);
    CHECK(ms.at(one) == 12);
    CHECK(ms.at(unit, 1) == 10);
  }
}

TEST_CASE("t_span constness", "[t_span][const]")
{
  /*
   * In the following several lines are commented out; these should
   * all break the build when uncommented.
   */

  {
    auto const const_data = std_iota_vector<int>(3, 0);

    t_span<const int, 1> ts_1(const_data.data(), {3}, {1});
    //        t_span<      int, 1> ts_2 (const_data.data (), {3}, {1});
    const t_span<const int, 1> ts_3(const_data.data(), {3}, {1});
    //  const t_span<      int, 1> ts_4 (const_data.data (), {3}, {1});

    //  ts_1.at (2) = 30;
    //  ts_3.at (2) = 30;

    CHECK(ts_1.at(2) == 2);
    CHECK(ts_3.at(2) == 2);
  }

  {
    auto data = std_iota_vector<int>(3, 0);

    t_span<const int, 1> ts_1(data.data(), {3}, {1});
    t_span<int, 1> ts_2(data.data(), {3}, {1});
    const t_span<const int, 1> ts_3(data.data(), {3}, {1});
    const t_span<int, 1> ts_4(data.data(), {3}, {1});

    //  ts_1.at (2) = 30;
    ts_2.at(2) = 30;
    //  ts_3.at (2) = 30;
    ts_4.at(2) = 60;

    CHECK(ts_1.at(2) == 60);
    CHECK(ts_2.at(2) == 60);
    CHECK(ts_3.at(2) == 60);
    CHECK(ts_4.at(2) == 60);
  }
}

TEST_CASE("as_t_span", "[t_span][as_t_span]")
{
  auto const data = std_iota_vector<int>(12, 0);

  const t_span<const int, 2> ts(data.data(), {3, 4}, {1, 3});

  {
    auto const new_ts = ts.as_t_span<4>(type_list_t{3_i, 0_i});

    CHECK(new_ts.origin_ == ts.origin_);
    CHECK(new_ts.size() == index<4>({4, 1, 1, 3}));
    CHECK(new_ts.stride_ == index<4>({3, 0, 0, 1}));
  }

  {
    auto const new_ts = ts.as_t_span<4>(type_list_t{1_i, 2_i});

    CHECK(new_ts.origin_ == ts.origin_);
    CHECK(new_ts.size() == index<4>({1, 3, 4, 1}));
    CHECK(new_ts.stride_ == index<4>({0, 1, 3, 0}));
  }
}

TEST_CASE("rows/cols/lays helper functions", "[t_span][helper]")
{
  auto const data = std_iota_vector<int>(30, 0);

  const cuboid_span<const int> cs(data.data(), {2, 4, 3}, {1, 2, 8});

  auto const rsa = cs.dims(0_i).span_along(2, 1);
  CHECK(rsa.size() == index<1>(2));
  CHECK(rsa.stride_ == index<1>(1));
  CHECK(rsa(0) == 12);
  CHECK(rsa(1) == 13);

  auto const csa = cs.dims(1_i).span_along(0, 1);
  CHECK(csa.size() == index<1>(4));
  CHECK(csa.stride_ == index<1>(2));
  CHECK(csa(0) == 8);
  CHECK(csa(1) == 10);
  CHECK(csa(2) == 12);
  CHECK(csa(3) == 14);

  auto const lsa = cs.dims(2_i).span_along(1, 1);
  CHECK(lsa.size() == index<1>(3));
  CHECK(lsa.stride_ == index<1>(8));
  CHECK(lsa(0) == 3);
  CHECK(lsa(1) == 11);
  CHECK(lsa(2) == 19);

  CHECK(cs.size()[0_i] == 2);
  CHECK(cs.size()[1_i] == 4);
  CHECK(cs.size()[2_i] == 3);

  auto const rr = cs.dims(0_i).span_plumb(1);
  CHECK(rr.origin_ == data.data() + 1);
  CHECK(rr.size() == index<2>(4, 3));
  CHECK(rr.stride_ == index<2>(2, 8));
  CHECK(rr.size()[0_i] == 4);
  CHECK(rr.size()[1_i] == 3);

  auto const cc = cs.dims(1_i).span_plumb(2);
  CHECK(cc.origin_ == data.data() + 4);
  CHECK(cc.size() == index<2>(2, 3));
  CHECK(cc.stride_ == index<2>(1, 8));

  auto const ll = cs.dims(2_i).span_plumb(2);
  CHECK(ll.origin_ == data.data() + 16);
  CHECK(ll.size() == index<2>(2, 4));
  CHECK(ll.stride_ == index<2>(1, 2));
}

TEST_CASE("as_(vector|matrix|cuboid)_span helper functions", "[t_span][helper]")
{
  {
    auto const data = std_iota_vector<int>(1, 33);

    const scalar_span<const int> ss(data.data(), {}, {});

    auto const vs = ss.as_vector_span();
    CHECK(vs.origin_ == ss.origin_);
    CHECK(vs.size() == index<1>(1));
    CHECK(vs.stride_ == index<1>(0));
    CHECK(vs.at(0) == 33);

    auto const ms = ss.as_matrix_span();

    CHECK(ms.origin_ == ss.origin_);
    CHECK(ms.size() == index<2>(1, 1));
    CHECK(ms.stride_ == index<2>(0, 0));

    CHECK(ms.at(0, 0) == 33);

    auto const cs = ss.as_cuboid_span();

    CHECK(cs.origin_ == ss.origin_);
    CHECK(cs.size() == index<3>(1, 1, 1));
    CHECK(cs.stride_ == index<3>(0, 0, 0));

    CHECK(cs.at(0, 0, 0) == 33);
  }

  {
    auto const data = std_iota_vector<int>(10, 10);

    const vector_span<const int> vs(data.data(), 2, 4);

    {
      auto const ms = vs.as_matrix_span(type_list_t{0_i});

      CHECK(ms.origin_ == vs.origin_);
      CHECK(ms.size() == index<2>(2, 1));
      CHECK(ms.stride_ == index<2>(4, 0));

      CHECK(ms.at(0, 0) == 10);
      CHECK(ms.at(1, 0) == 14);
    }

    {
      auto const cs = vs.as_cuboid_span(type_list_t{2_i});

      CHECK(cs.origin_ == vs.origin_);
      CHECK(cs.size() == index<3>(1, 1, 2));
      CHECK(cs.stride_ == index<3>(0, 0, 4));

      CHECK(cs.at(0, 0, 0) == 10);
      CHECK(cs.at(0, 0, 1) == 14);
    }
  }

  {
    auto const data = std_iota_vector<int>(10, 0);

    const matrix_span<const int> ms(data.data(), {2, 3}, {1, 2});

    {
      auto const ms_2 = ms.as_matrix_span(type_list_t{1_i, 0_i});
      auto const ms_3 = ms.transposed();

      CHECK(ms_2.origin_ == ms_3.origin_);
      CHECK(ms_2.size() == ms_3.size());
      CHECK(ms_2.stride_ == ms_3.stride_);

      CHECK(ms_2.origin_ == ms.origin_);
      CHECK(ms_2.size() == index<2>(3, 2));
      CHECK(ms_2.stride_ == index<2>(2, 1));
    }

    {
      auto const cs   = ms.as_cuboid_span(index_list_iota<2>());
      auto const cs_2 = ms.as_cuboid_span(type_list_t{0_i, 1_i});

      CHECK(cs.origin_ == ms.origin_);
      CHECK(cs.size() == index<3>(2, 3, 1));
      CHECK(cs.stride_ == index<3>(1, 2, 0));

      CHECK(cs.origin_ == cs_2.origin_);
      CHECK(cs.size() == cs_2.size());
      CHECK(cs.stride_ == cs_2.stride_);

      CHECK(cs.at(0, 0, 0) == 0);
      CHECK(cs.at(1, 0, 0) == 1);
      CHECK(cs.at(0, 1, 0) == 2);
      CHECK(cs.at(1, 1, 0) == 3);
      CHECK(cs.at(1, 2, 0) == 5);
    }
  }
}

TEST_CASE("t_span arithmetic operators", "[t_span][operator][arithmetic]")
{
  auto data        = std_iota_vector<int>(200, 0);
  auto const cdata = std_iota_vector<int>(200, 100);

  SECTION("add on vector_span")
  {
    const vector_span<int> vs(data.data(), 10, 2);
    const vector_span<const int> rhs(cdata.data(), 10, 1);

    vs += rhs;

    CHECK(
        vs ==
        make_from_json<vector<int>>(
            "{"
            "  \"size\": [10],"
            "  \"values\":"
            "    [100, 103, 106, 109, 112, 115, 118, 121, 124, 127]"
            "}")
            .span());

    const vector_span<const int> lhs_1(data.data() + 100, 10, 1);
    const vector_span<const int> lhs_2(data.data() + 100, 8, 1);

    CHECK(lhs_1 == rhs);
    CHECK(lhs_2 != rhs);
  }
  SECTION("add on cuboid_span")
  {
    const cuboid_span<int> cs(data.data(), {2, 2, 2}, {1, 10, 40});
    const cuboid_span<const int> rhs(cdata.data(), {2, 2, 2}, {1, 2, 4});

    CHECK(cs != rhs);

    cs += rhs;

    CHECK(
        cs ==
        make_from_json<cuboid<int>>("{ \"size\": [2, 2, 2],"
                                    "  \"values\":"
                                    "[[[100, 102],"
                                    "  [112, 114]"
                                    " ],"
                                    " [[144, 146],"
                                    "  [156, 158]"
                                    " ]"
                                    "] }")
            .span());

    cs *= 2.8;

    CHECK(
        cs ==
        make_from_json<cuboid<int>>("{ \"size\": [2, 2, 2],"
                                    "  \"values\":"
                                    "[[[280, 285],"
                                    "  [313, 319]"
                                    " ],"
                                    " [[403, 408],"
                                    "  [436, 442]"
                                    " ]"
                                    "] }")
            .span());

    cs /= 2;

    CHECK(
        cs ==
        make_from_json<cuboid<int>>("{ \"size\": [2, 2, 2],"
                                    "  \"values\":"
                                    "[[[140, 142],"
                                    "  [156, 159]"
                                    " ],"
                                    " [[201, 204],"
                                    "  [218, 221]"
                                    " ]"
                                    "] }")
            .span());

    cs -= rhs;

    CHECK(
        cs ==
        make_from_json<cuboid<int>>("{ \"size\": [2, 2, 2],"
                                    "  \"values\":"
                                    "[[[40, 41],"
                                    "  [54, 56]"
                                    " ],"
                                    " [[97, 99],"
                                    "  [112, 114]"
                                    " ]"
                                    "] }")
            .span());
  }
  SECTION("add vector_span to matrix_span")
  {
    const matrix_span<int> ms(data.data(), {3, 5}, {2, 6});
    const vector_span<const int> rr(cdata.data(), 3, 1);

    auto temp       = rr.as_matrix_span(type_list_t{0_i});
    temp.size_[1_i] = 5;

    ms += temp;

    CHECK(
        ms ==
        make_from_json<matrix<int>>("{ \"size\": [3, 5],"
                                    "  \"values\":"
                                    "[[100, 103, 106],"
                                    " [106, 109, 112],"
                                    " [112, 115, 118],"
                                    " [118, 121, 124],"
                                    " [124, 127, 130]"
                                    "] }")
            .span());

    std::fill(ms.begin(), ms.end(), 0);

    ms += temp;

    CHECK(
        ms ==
        make_from_json<matrix<int>>("{ \"size\": [3, 5],"
                                    "  \"values\":"
                                    "[[100, 101, 102],"
                                    " [100, 101, 102],"
                                    " [100, 101, 102],"
                                    " [100, 101, 102],"
                                    " [100, 101, 102]"
                                    "] }")
            .span());
  }
}

TEST_CASE("t_span is_aliasing", "[t_span][is_aliasing]")
{
  constexpr int id[200] = {0};

  using vs = vector_span<const int>;

  static_assert(!is_aliasing(vs(id, 0, 0)));
  static_assert(!is_aliasing(vs(id, 1, 0)));
  static_assert(!is_aliasing(vs(id, 0, 1)));
  static_assert(!is_aliasing(vs(id, 1, 1)));
  static_assert(!is_aliasing(vs(id, 3, 1)));
  static_assert(is_aliasing(vs(id, 3, 0)));
  static_assert(!is_aliasing(vs(id, 3, 1)));

  using ms = matrix_span<const int>;

  static_assert(!is_aliasing(ms(id, {0, 0}, {0, 0})));
  static_assert(!is_aliasing(ms(id, {1, 1}, {0, 0})));
  static_assert(is_aliasing(ms(id, {2, 1}, {0, 0})));
  static_assert(!is_aliasing(ms(id, {7, 3}, {1, 7})));
  static_assert(is_aliasing(ms(id, {7, 3}, {1, 6})));
  static_assert(!is_aliasing(ms(id, {5, 3}, {3, 5})));
  static_assert(is_aliasing(ms(id, {6, 4}, {3, 5})));
}
