#include "numa/cell/t_span_adaptor_mirror.hpp"

#include "numa/cell/serialize_json.hpp"
#include "numa/cell/tensor.hpp"
#include "numa/cell/tensor_staged.hpp"

#include "numa/core/functors.hpp"
#include "numa/core/generators.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("t_span_adaptor_mirror concepts", "[t_span][concepts]")
{
  static_assert(
      Span<t_span_adaptor_mirror<matrix_span<double>, side::right, f_conj_t>>);
}

TEST_CASE("basic t-span-adaptor-mirror", "[t_span_adaptor_mirror]")
{
  auto A = make_cell<matrix<double>>(3, 3)(f_const_t{3.0});
  auto B = make_cell<matrix<double>>(3, 3)(g_iota(0.5));

  SECTION("case 1")
  {
    for_each(A, upper_symmetric(B))(f_plus);

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [3, 3],"
                                       "  \"values\":"
                                       "[[3.5, 6.5, 9.5],"
                                       " [6.5, 7.5, 10.5],"
                                       " [9.5, 10.5, 11.5]"
                                       "] }"));
  }
  SECTION("case 2")
  {
    A += lower_symmetric(B);

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [3, 3],"
                                       "  \"values\":"
                                       "[[3.5, 4.5, 5.5],"
                                       " [4.5, 7.5, 8.5],"
                                       " [5.5, 8.5, 11.5]"
                                       "] }"));
  }
  SECTION("case 3")
  {
    A -= upper_hermitian(B);

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [3, 3],"
                                       "  \"values\":"
                                       "[[2.5, -0.5, -3.5],"
                                       " [-0.5, -1.5, -4.5],"
                                       " [-3.5, -4.5, -5.5]"
                                       "] }"));
  }
  SECTION("case 4")
  {
    A -= lower_hermitian(B);

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [3, 3],"
                                       "  \"values\":"
                                       "[[2.5, 1.5, 0.5],"
                                       " [1.5, -1.5, -2.5],"
                                       " [0.5, -2.5, -5.5]"
                                       "] }"));
  }
  SECTION("case 5")
  {
    for_each(lower_hermitian(B), A)([](const auto b, auto& a) { a -= b; });

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [3, 3],"
                                       "  \"values\":"
                                       "[[2.5, 1.5, 0.5],"
                                       " [1.5, -1.5, -2.5],"
                                       " [0.5, -2.5, -5.5]"
                                       "] }"));
  }
}

TEST_CASE(
    "t-span-adaptor-mirror members",
    "[t_span_adaptor_mirror][members]"
    "[upper][lower_strictly]")
{
  auto A = make_cell<matrix_staged<double, 1>>(3)(g_iota(0.5));

  SECTION("upper")
  {
    auto uA = upper_symmetric(A);

    CHECK(uA.size() == index(3));

    CHECK(uA(1, 2) == 6.5);

    CHECK(uA.is_bounded(1, 2));
    CHECK(uA.is_bounded(2, 2));
    CHECK(!uA.is_bounded(3, 2));
    CHECK(A.is_bounded(3, 2));

    auto it = uA.iterator(1, 1);

    CHECK(it.pos() == index(1, 1));
    CHECK(it == uA.iterator(1, 1));
    CHECK(*it == 4.5);
    ++it;
    CHECK(it.pos() == index(1, 2));
    CHECK(it == uA.iterator(1, 2));
    CHECK(*it == 6.5);
    ++it;
    CHECK(it.pos() == index(2, 1));
    CHECK(it == uA.iterator(2, 1));
    CHECK(*it == 6.5);
    ++it;
    CHECK(it.pos() == index(2, 2));
    CHECK(it == uA.iterator(2, 2));
    CHECK(*it == 7.5);
    ++it;
    CHECK(it == uA.end());
    --it;
    CHECK(it.pos() == index(2, 2));
    CHECK(it == uA.iterator(2, 2));
    CHECK(*it == 7.5);
    --it;
    CHECK(it.pos() == index(2, 1));
    CHECK(it == uA.iterator(2, 1));
    CHECK(*it == 6.5);
    --it;
    CHECK(it.pos() == index(1, 2));
    CHECK(it == uA.iterator(1, 2));
    CHECK(*it == 6.5);
    --it;
    CHECK(it.pos() == index(1, 1));
    CHECK(it == uA.iterator(1, 1));
    CHECK(*it == 4.5);
    --it;
    CHECK(it.pos() == index(1, 0));
    CHECK(it == uA.iterator(1, 0));
    CHECK(*it == 3.5);
    --it;
    CHECK(it.pos() == index(0, 1));
    CHECK(it == uA.iterator(0, 1));
    CHECK(*it == 3.5);
    --it;
    CHECK(it.pos() == index(0, 0));
    CHECK(it == uA.iterator(0, 0));
    CHECK(*it == 1.5);
    --it;
    CHECK(it.pos() == index(0, -1));
    CHECK(it == uA.iterator(0, -1));
    CHECK(*it == 0.5);
    --it;
    CHECK(it.pos() == index(-1, 0));
    CHECK(it == uA.iterator(-1, 0));
    CHECK(*it == 0.5);

    CHECK(it == uA.begin());
  }
}
