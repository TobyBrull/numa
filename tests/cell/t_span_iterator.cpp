#include "numa/cell/t_span.hpp"

#include "numa/misc/data.hpp"

#include "catch2/catch.hpp"

#include <numeric>

using namespace numa;
using namespace numa::misc;

TEST_CASE("single-step tensor-span-iterator", "[t_span_iterator][single]")
{
  const auto data = std_iota_vector<int>(10, 0);

  cuboid_span<const int> cs(data.data(), {2, 2, 2}, {1, 2, 4});

  auto iter             = cs.begin();
  const auto iter_begin = cs.begin();
  const auto iter_end   = cs.end();

  CHECK(iter < iter_end);
  CHECK(iter <= iter_end);

  CHECK(iter_end > iter);
  CHECK(iter_end >= iter);

  CHECK(iter_end >= iter_end);
  CHECK(iter_end <= iter_end);

  CHECK(!(iter_end > iter_end));
  CHECK(!(iter_end < iter_end));

  CHECK(iter != iter_end);
  CHECK(*iter == 0);
  CHECK(iter.pos() == index(0, 0, 0));

  ++iter;
  CHECK(iter != iter_end);
  CHECK(*iter == 1);
  CHECK(iter.pos() == index(1, 0, 0));

  ++iter;
  CHECK(iter != iter_end);
  CHECK(*iter == 2);
  CHECK(iter.pos() == index(0, 1, 0));

  ++iter;
  CHECK(iter != iter_end);
  CHECK(*iter == 3);
  CHECK(iter.pos() == index(1, 1, 0));

  ++iter;
  CHECK(iter != iter_end);
  CHECK(*iter == 4);
  CHECK(iter.pos() == index(0, 0, 1));

  ++iter;
  CHECK(iter != iter_end);
  CHECK(*iter == 5);
  CHECK(iter.pos() == index(1, 0, 1));

  ++iter;
  CHECK(iter != iter_end);
  CHECK(*iter == 6);
  CHECK(iter.pos() == index(0, 1, 1));

  ++iter;
  CHECK(iter != iter_end);
  CHECK(*iter == 7);
  CHECK(iter.pos() == index(1, 1, 1));

  ++iter;
  CHECK(iter.pos() == index(0, 0, 2));
  CHECK(iter == iter_end);

  --iter;
  CHECK(iter != iter_end);
  CHECK(iter != iter_begin);
  CHECK(*iter == 7);
  CHECK(iter.pos() == index(1, 1, 1));

  --iter;
  CHECK(iter != iter_end);
  CHECK(iter != iter_begin);
  CHECK(*iter == 6);
  CHECK(iter.pos() == index(0, 1, 1));

  --iter;
  CHECK(iter != iter_begin);
  CHECK(*iter == 5);
  CHECK(iter.pos() == index(1, 0, 1));

  --iter;
  CHECK(iter != iter_begin);
  CHECK(*iter == 4);
  CHECK(iter.pos() == index(0, 0, 1));

  --iter;
  CHECK(iter != iter_begin);
  CHECK(*iter == 3);
  CHECK(iter.pos() == index(1, 1, 0));

  --iter;
  CHECK(iter != iter_begin);
  CHECK(*iter == 2);
  CHECK(iter.pos() == index(0, 1, 0));

  --iter;
  CHECK(iter != iter_begin);
  CHECK(*iter == 1);
  CHECK(iter.pos() == index(1, 0, 0));

  --iter;
  CHECK(iter == iter_begin);
  CHECK(*iter == 0);
  CHECK(iter.pos() == index(0, 0, 0));
}

TEST_CASE("tensor-span-iterator with empty span", "[t_span_iterator]")
{
  const cuboid_span<const int> cs(nullptr, {0, 0, 3}, {1, 2, 0});

  const auto beg = cs.begin();
  const auto end = cs.end();

  CHECK(beg == end);

  for (const int val: cs) {
    (void)val;
    FAIL("Range should be empty");
  }
}

template<typename Iterator>
auto
single_stepped(Iterator iter, index_t offset)
{
  if (offset > 0) {
    for (index_t i = 0; i < offset; ++i) {
      ++iter;
    }
  }
  else if (offset < 0) {
    for (index_t i = 0; i < -offset; ++i) {
      --iter;
    }
  }

  return iter;
}

TEST_CASE(
    "multi-step tensor-span-iterator & difference",
    "[t_span_iterator][multi_step][difference]")
{
  const auto data = std_iota_vector<int>(1000, 0);

  cuboid_span<const int> cs(data.data(), {4, 6, 8}, {2, 10, 100});

  auto iter = cs.begin();

  const auto do_test = [&](auto& iter, const index_t offset) {
    const auto expected = single_stepped(iter, offset);

    const auto orig_iter = iter;

    iter += offset;
    CHECK(iter == expected);

    auto copy = orig_iter;
    copy -= (-offset);
    CHECK(copy == expected);

    CHECK((iter - orig_iter) == offset);
    CHECK((orig_iter - iter) == -offset);

    if (iter != cs.end()) {
      CHECK(iter == cs.iterator(iter.pos()));
    }
  };

  do_test(iter, 0);
  CHECK(*iter == 0);
  CHECK(iter.pos() == index(0, 0, 0));
  CHECK(cs.iterator(zero) == iter);

  do_test(iter, 6);
  CHECK(*iter == 14);
  CHECK(iter.pos() == index(2, 1, 0));

  do_test(iter, -3);
  CHECK(*iter == 6);
  CHECK(iter.pos() == index(3, 0, 0));

  do_test(iter, 0);
  CHECK(*iter == 6);
  CHECK(iter.pos() == index(3, 0, 0));
  CHECK(cs.iterator(3, 0, 0) == iter);

  do_test(iter, 24);
  CHECK(*iter == 106);
  CHECK(iter.pos() == index(3, 0, 1));
  CHECK(cs.iterator(3, 0, 1) == iter);

  do_test(iter, 90);
  CHECK(*iter == 452);
  CHECK(iter.pos() == index(1, 5, 4));
  CHECK(cs.iterator(1, 5, 4) == iter);

  do_test(iter, 75);
  CHECK(iter == cs.end());
  CHECK(iter.pos() == index(0, 0, 8));

  do_test(iter, -(8 * 6 * 4));
  CHECK(iter == cs.begin());
  CHECK(iter.pos() == index(0, 0, 0));
  CHECK(*iter == 0);

  do_test(iter, 8 * 6 * 4);
  CHECK(iter == cs.end());
  CHECK(iter.pos() == index(0, 0, 8));

  do_test(iter, 0);
  CHECK(iter == cs.end());
  CHECK(iter.pos() == index(0, 0, 8));

  do_test(iter, -2);
  CHECK(*iter == 754);
  CHECK(iter.pos() == index(2, 5, 7));

  do_test(iter, -131);
  CHECK(*iter == 226);
  CHECK(iter.pos() == index(3, 2, 2));
}

TEST_CASE(
    "exhaustive multi-step tensor-span-iterator & difference",
    "[t_span_iterator][extensive][multi_step][difference]")
{
  const auto data = std_iota_vector<int>(100, 0);

  cuboid_span<const int> cs(data.data(), {2, 3, 2}, {3, 10, 50});

  const index_t tot_size = product(cs.size());

  auto it = cs.begin();

  for (index_t count = 0; count <= tot_size; ++count) {
    const index cur_pos_it((count % 2) / 1, (count % 6) / 2, (count) / 6);

    CHECK((it - cs.begin()) == count);
    CHECK((cs.begin() - it) == (-count));
    CHECK((it - cs.end()) == (count - tot_size));
    CHECK((cs.end() - it) == (tot_size - count));

    CHECK(it.pos() == cur_pos_it);

    if (it != cs.end()) {
      CHECK((*it) == dot(cur_pos_it, cs.stride_));
    }

    for (index_t target = 0; target <= tot_size; ++target) {
      auto copy = it;

      const index cur_pos_copy(
          (target % 2) / 1, (target % 6) / 2, (target) / 6);

      const index_t jump = (target - count);

      copy += jump;

      CHECK(copy.pos() == cur_pos_copy);

      if (copy != cs.end()) {
        CHECK((*copy) == dot(cur_pos_copy, cs.stride_));
      }

      CHECK((copy - it) == jump);
      CHECK((it - copy) == (-jump));
    }

    ++it;
  }
}

TEST_CASE("iterator on scalar-span", "[t_span_iterator][scalar]")
{
  const int i = 12;

  scalar_span<const int> ss(&i, {}, {});

  auto it           = ss.begin();
  const auto end_it = ss.end();

  CHECK(it != end_it);

  CHECK(it.ptr() == &i);
  CHECK(it.pos() == index<0>());
  CHECK((*it) == 12);

  CHECK((end_it - it) == 1);
  CHECK((it - end_it) == -1);

  auto copy = it;

  ++it;
  copy += 1;

  CHECK(it == end_it);
  CHECK(copy == end_it);
  CHECK(it == copy);

  CHECK((end_it - it) == 0);

  --it;
  copy -= 1;

  CHECK(it != end_it);

  CHECK(it.ptr() == &i);
  CHECK(it.pos() == index<0>());
  CHECK((*it) == 12);

  CHECK((end_it - it) == 1);
  CHECK((it - end_it) == -1);
}
