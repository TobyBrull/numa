#include "numa/cell/algorithms.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/tensor_staged.hpp"

#include "numa/misc/data.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

TEST_CASE("basic band-matrix-span", "[t_span_staged][basic]")
{
  const auto vec = std_iota_vector<int>(30, 0);

  {
    matrix_span_staged<const int, 2> bms(vec.data() + 2, 6, {1, 4});

    CHECK_THROWS(bms.at(-3, 0));
    CHECK(bms.at(-2, 0) == 0);
    CHECK(bms.at(-1, 0) == 1);
    CHECK(bms.at(0, 0) == 2);
    CHECK(bms.at(1, 0) == 3);
    CHECK(bms.at(2, 0) == 4);
    CHECK(!bms.is_bounded(3, 0));
    CHECK(!bms.is_bounded(4, 0));
    CHECK(!bms.is_bounded(5, 0));

    CHECK(!bms.is_bounded(-2, 1));
    CHECK(bms.at(-1, 1) == 5);
    CHECK(bms.at(0, 1) == 6);
    CHECK(bms.at(1, 1) == 7);
    CHECK(bms.at(2, 1) == 8);
    CHECK(bms.at(3, 1) == 9);
    CHECK(!bms.is_bounded(4, 1));
    CHECK(!bms.is_bounded(5, 1));

    CHECK(bms.at(0, 2) == 10);
    CHECK(bms.at(1, 2) == 11);
    CHECK(bms.at(2, 2) == 12);
    CHECK(bms.at(3, 2) == 13);
    CHECK(bms.at(4, 2) == 14);
    CHECK(!bms.is_bounded(5, 2));

    CHECK(!bms.is_bounded(0, 3));
    CHECK(bms.at(1, 3) == 15);
    CHECK(bms.at(2, 3) == 16);
    CHECK(bms.at(3, 3) == 17);
    CHECK(bms.at(4, 3) == 18);
    CHECK(bms.at(5, 3) == 19);

    CHECK(!bms.is_bounded(index(0, 4)));
    CHECK(!bms.is_bounded(index(1, 4)));
    CHECK(bms.at(index(2, 4)) == 20);
    CHECK(bms.at(3, 4) == 21);
    CHECK(bms.at(4, 4) == 22);
    CHECK(bms.at(5, 4) == 23);

    CHECK(!bms.is_bounded(0, 5));
    CHECK(!bms.is_bounded(1, 5));
    CHECK(!bms.is_bounded(2, 5));
    CHECK(bms.at(3, 5) == 25);
    CHECK(bms.at(4, 5) == 26);
    CHECK(bms.at(5, 5) == 27);
    CHECK(bms.at(6, 5) == 28);
    CHECK(bms.at(7, 5) == 29);
    CHECK_THROWS(bms.at(8, 5));
  }

  {
    matrix_span_staged<const int, 1> bms(vec.data() + 2, 6, {1, 4});

    auto it = bms.begin();

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index<2>(-1, 0));
    CHECK(*it == 1);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index<2>(zero));
    CHECK(*it == 2);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(1, 0));
    CHECK(*it == 3);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(0, 1));
    CHECK(*it == 6);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(1, 1));
    CHECK(*it == 7);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(2, 1));
    CHECK(*it == 8);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(1, 2));
    CHECK(*it == 11);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(2, 2));
    CHECK(*it == 12);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(3, 2));
    CHECK(*it == 13);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(2, 3));
    CHECK(*it == 16);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(3, 3));
    CHECK(*it == 17);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(4, 3));
    CHECK(*it == 18);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(3, 4));
    CHECK(*it == 21);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(4, 4));
    CHECK(*it == 22);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(5, 4));
    CHECK(*it == 23);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(4, 5));
    CHECK(*it == 26);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(5, 5));
    CHECK(*it == 27);

    ++it;

    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(6, 5));
    CHECK(*it == 28);

    ++it;

    REQUIRE(it == bms.end());
    CHECK(it.pos() == index(5, 6));

    --it;
    REQUIRE(it != bms.end());
    CHECK(it.pos() == index(6, 5));
    CHECK(*it == 28);

    --it;

    REQUIRE(it != bms.begin());
    CHECK(it.pos() == index(5, 5));
    CHECK(*it == 27);

    --it;

    REQUIRE(it != bms.begin());
    CHECK(it.pos() == index(4, 5));
    CHECK(*it == 26);

    --it;

    REQUIRE(it != bms.begin());
    CHECK(it.pos() == index(5, 4));
    CHECK(*it == 23);

    --it;

    REQUIRE(it != bms.begin());
    CHECK(it.pos() == index(4, 4));
    CHECK(*it == 22);

    --it;

    REQUIRE(it != bms.begin());
    CHECK(it.pos() == index(3, 4));
    CHECK(*it == 21);

    --it;

    REQUIRE(it != bms.begin());
    CHECK(it.pos() == index(4, 3));
    CHECK(*it == 18);

    --it;
    --it;
    --it;
    --it;
    --it;

    REQUIRE(it != bms.begin());
    CHECK(it.pos() == index(1, 2));
    CHECK(*it == 11);

    --it;

    REQUIRE(it != bms.begin());
    CHECK(it.pos() == index(2, 1));
    CHECK(*it == 8);

    --it;

    REQUIRE(it != bms.begin());
    CHECK(it.pos() == index(1, 1));
    CHECK(*it == 7);

    --it;

    REQUIRE(it != bms.begin());
    CHECK(it.pos() == index(0, 1));
    CHECK(*it == 6);

    --it;

    REQUIRE(it != bms.begin());
    CHECK(it.pos() == index(1, 0));
    CHECK(*it == 3);

    --it;

    REQUIRE(it != bms.begin());
    CHECK(it.pos() == index<2>(zero));
    CHECK(*it == 2);

    --it;

    REQUIRE(it == bms.begin());
    CHECK(it.pos() == index(-1, 0));
    CHECK(*it == 1);
  }
}

TEST_CASE("basic band-tensor-span", "[t_span_staged][iterator]")
{
  const auto vec = std_iota_vector<int>(400, 0);

  t_span_staged<const int, 4, index_list_t<2, 1>, index_list_t<1, 1>> bts(
      vec.data() + 5, {6, 5}, {1, 4, 11, 68});

  auto it = bts.begin();

  CHECK(it.pos() == index<4>(-1, -1, 0, 0));
  CHECK(*it == 0);

  it += 5;

  CHECK(it.pos() == index<4>(zero));
  CHECK(*it == 5);

  it += 16;

  CHECK(it.pos() == index(1, 1, 1, 0));
  CHECK(*it == 21);

  it += 233;

  CHECK(it.pos() == index(4, 2, 3, 3));
  CHECK(*it == 254);

  it += 106;

  CHECK(it.pos() == index(-1, 4, 0, 5));

  it -= 1;

  CHECK(it.pos() == index(7, 5, 5, 4));
  CHECK(*it == 359);

  it += 1;

  CHECK(it.pos() == index(-1, 4, 0, 5));

  it -= 72;

  CHECK(it.pos() == index(-1, 3, 0, 4));
  CHECK(*it == 288);

  it -= 62;

  CHECK(it.pos() == index(1, 4, 0, 3));
  CHECK(*it == 226);

  it -= 226;

  CHECK(it.pos() == index(-1, -1, 0, 0));
  CHECK(*it == 0);

  it += 84;

  CHECK(it.pos() == index(0, 0, 1, 1));
  CHECK(*it == 84);

  it += 71;

  CHECK(it.pos() == index(2, 3, 0, 2));
  CHECK(*it == 155);

  it += 1;

  CHECK(it.pos() == index(0, 1, 1, 2));
  CHECK(*it == 156);

  it += 23;

  CHECK(it.pos() == index(4, 3, 2, 2));
  CHECK(*it == 179);

  it -= 23;

  CHECK(it.pos() == index(0, 1, 1, 2));
  CHECK(*it == 156);

  it -= 1;

  CHECK(it.pos() == index(2, 3, 0, 2));
  CHECK(*it == 155);

  it -= 71;

  CHECK(it.pos() == index(0, 0, 1, 1));
  CHECK(*it == 84);

  it -= 84;

  CHECK(it.pos() == index(-1, -1, 0, 0));
  CHECK(*it == 0);
}

TEST_CASE("band-tensor-span iterator", "[t_span_staged][iterator]")
{
  const auto vec = std_iota_vector<int>(400, 0);

  t_span_staged<const int, 4, index_list_t<2, 1>, index_list_t<1, 1>> bts(
      vec.data() + 5, {6, 5}, {1, 4, 11, 68});

  CHECK((bts.end() - bts.begin()) == 360);
  CHECK((bts.begin() - bts.end()) == -360);

  {
    const auto a = bts.iterator(2, 0, 2, 1);
    const auto b = bts.begin();

    CHECK(a.pos() == index(2, 0, 2, 1));
    CHECK(b.pos() == index(-1, -1, 0, 0));
    CHECK(*a == 97);
    CHECK(*b == 0);
    CHECK((a - b) == 97);
    CHECK((b - a) == -97);
  }

  {
    const auto a = bts.iterator(2, 4, 3, 3);
    const auto b = bts.iterator(3, 3, 4, 2);

    CHECK(a.pos() == index(2, 4, 3, 3));
    CHECK(b.pos() == index(3, 3, 4, 2));
    CHECK(*a == 260);
    CHECK(*b == 200);
    CHECK((a - b) == 60);
    CHECK((b - a) == -60);
  }

  {
    const auto a = bts.iterator(7, 5, 5, 4);
    const auto b = bts.end();

    CHECK(a.pos() == index(7, 5, 5, 4));
    CHECK(b.pos() == index(-1, 4, 0, 5));
    CHECK(*a == 359);
    CHECK((a - b) == -1);
    CHECK((b - a) == 1);
  }

  {
    const auto a = bts.iterator(5, 3, 4, 3);
    const auto b = bts.end();

    CHECK(a.pos() == index(5, 3, 4, 3));
    CHECK(b.pos() == index(-1, 4, 0, 5));
    CHECK(*a == 270);
    CHECK((a - b) == -90);
    CHECK((b - a) == 90);
  }

  {
    const auto a = bts.iterator(3, 1, 3, 1);
    const auto b = bts.iterator(3, 2, 1, 2);

    CHECK(a.pos() == index(3, 1, 3, 1));
    CHECK(b.pos() == index(3, 2, 1, 2));
    CHECK(*a == 113);
    CHECK(*b == 163);
    CHECK((a - b) == -50);
    CHECK((b - a) == 50);
  }

  {
    const auto a = bts.iterator(3, 0, 2, 1);
    const auto b = bts.iterator(2, 1, 2, 1);

    CHECK(a.pos() == index(3, 0, 2, 1));
    CHECK(b.pos() == index(2, 1, 2, 1));
    CHECK(*a == 98);
    CHECK(*b == 101);
    CHECK((a - b) == -3);
    CHECK((b - a) == 3);
  }
}

TEST_CASE("diagonal-matrix-span", "[t_span_staged]")
{
  const auto vec = std_iota_vector<int>(10, 0);

  const matrix_span_diagonal<const int> dms(vec.data(), 4, {1, 1});

  CHECK(
      dms ==
      make_from_json<matrix_diagonal<int>>("{ \"size\": [4],"
                                           "  \"values\":"
                                           "[[0],"
                                           " [2],"
                                           " [4],"
                                           " [6]"
                                           "] }")
          .span());

  CHECK(!dms.is_bounded(index(-2, 0)));
  CHECK(!dms.is_bounded(index(-1, 0)));
  CHECK(dms.at(0, 0) == 0);
  CHECK(!dms.is_bounded(index(1, 0)));
  CHECK(!dms.is_bounded(index(2, 0)));
  CHECK(!dms.is_bounded(index(3, 0)));

  CHECK(dms.at(1, 1) == 2);
  CHECK(dms.at(2, 2) == 4);
  CHECK(dms.at(3, 3) == 6);
}

TEST_CASE("band-tensor-span-axes", "[t_span_staged][axes]")
{
  const auto vec = std_iota_vector<int>(400, 0);

  t_span_staged<const int, 4, index_list_t<2, 1>, index_list_t<1, 1>> bts(
      vec.data() + 5, {6, 5}, {1, 4, 11, 68});

  const auto sub = bts.axes().sub(as_size, {1, 2}, {3, 2});

  CHECK(
      sub ==
      make_from_json<
          tensor_staged<int, 4, index_list_t<2, 1>, index_list_t<1, 1>>>(
          "{ \"size\": [3, 2],"
          "  \"values\":"
          "[[[[156, 157, 158, 159],"
          "   [160, 161, 162, 163],"
          "   [164, 165, 166, 167]"
          "  ],"
          "  [[168, 169, 170, 171],"
          "   [172, 173, 174, 175],"
          "   [176, 177, 178, 179]"
          "  ],"
          "  [[180, 181, 182, 183],"
          "   [184, 185, 186, 187],"
          "   [188, 189, 190, 191]"
          "  ]"
          " ],"
          " [[[228, 229, 230, 231],"
          "   [232, 233, 234, 235],"
          "   [236, 237, 238, 239]"
          "  ],"
          "  [[240, 241, 242, 243],"
          "   [244, 245, 246, 247],"
          "   [248, 249, 250, 251]"
          "  ],"
          "  [[252, 253, 254, 255],"
          "   [256, 257, 258, 259],"
          "   [260, 261, 262, 263]"
          "  ]"
          " ]"
          "] }")
          .span());
}
