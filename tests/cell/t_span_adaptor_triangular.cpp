#include "numa/cell/t_span_adaptor_triangular.hpp"

#include "numa/cell/serialize_json.hpp"
#include "numa/cell/tensor.hpp"
#include "numa/cell/tensor_staged.hpp"

#include "numa/core/functors.hpp"
#include "numa/core/generators.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("t_span_adaptor_triangular concepts", "[t_span_adaptor][concepts]")
{
  static_assert(Span<t_span_adaptor_triangular<
                    matrix_span<double>,
                    side::right,
                    strictly::no>>);
}

TEST_CASE(
    "basic t_span_adaptor_triangular",
    "[t_span_adaptor_triangular]"
    "[upper][lower][upper_strictly][lower_strictly]")
{
  auto A = make_cell<matrix<double>>(4, 3)(g_iota(1.1));
  auto B = make_cell<matrix<double>>(4, 3)(f_const_t{3.0});

  SECTION("both lhs and rhs")
  {
    for_each(upper(A), upper(B))(f_plus);

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [4, 3],"
                                       "  \"values\":"
                                       "[[4.1, 2.1, 3.1, 4.1],"
                                       " [8.1, 9.1, 7.1, 8.1],"
                                       " [12.1, 13.1, 14.1, 12.1]"
                                       "] }"));
  }
  SECTION("only lhs")
  {
    for_each(upper(A), B)(f_plus);

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [4, 3],"
                                       "  \"values\":"
                                       "[[4.1, 2.1, 3.1, 4.1],"
                                       " [8.1, 9.1, 7.1, 8.1],"
                                       " [12.1, 13.1, 14.1, 12.1]"
                                       "] }"));

    lower_strictly(A) += B;

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [4, 3],"
                                       "  \"values\":"
                                       "[[4.1, 5.1, 6.1, 7.1],"
                                       " [8.1, 9.1, 10.1, 11.1],"
                                       " [12.1, 13.1, 14.1, 15.1]"
                                       "] }"));

    upper_strictly(B) *= 2.0;

    CHECK(
        B ==
        make_from_json<matrix<double>>("{ \"size\": [4, 3],"
                                       "  \"values\":"
                                       "[[3.0, 3.0, 3.0, 3.0],"
                                       " [6.0, 3.0, 3.0, 3.0],"
                                       " [6.0, 6.0, 3.0, 3.0]"
                                       "] }"));

    lower(B) *= 10.0;

    CHECK(
        B ==
        make_from_json<matrix<double>>("{ \"size\": [4, 3],"
                                       "  \"values\":"
                                       "[[30.0, 30.0, 30.0, 30.0],"
                                       " [6.0, 30.0, 30.0, 30.0],"
                                       " [6.0, 6.0, 30.0, 30.0]"
                                       "] }"));
  }
}

TEST_CASE(
    "t_span_adaptor_triangular on staged",
    "[t_span_adaptor_triangular][staged]"
    "[upper][lower]")
{
  auto A = make_cell<matrix_staged<double, 1>>(4)(g_iota(1.5));
  auto B = make_cell<matrix_staged<double, 2>>(4)(g_iota(100.0));

  SECTION("both lhs and rhs")
  {
    for_each(upper(A), upper(B))(f_plus);

    CHECK(
        A ==
        make_from_json<matrix_staged<double, 1>>("{ \"size\": [4],"
                                                 "  \"values\":"
                                                 "[[102.5, 104.5, 3.5],"
                                                 " [110.5, 112.5, 6.5],"
                                                 " [118.5, 120.5, 9.5],"
                                                 " [126.5, 128.5, 12.5]"
                                                 "] }"));
  }
  SECTION("only lhs")
  {
    for_each(lower(A), B)([](const double x, double& y) { y += x; });

    CHECK(
        B ==
        make_from_json<matrix_staged<double, 2>>(
            "{ \"size\": [4],"
            "  \"values\":"
            "[[100, 101, 104.5, 106.5, 104],"
            " [105, 106, 112.5, 114.5, 109],"
            " [110, 111, 120.5, 122.5, 114],"
            " [115, 116, 128.5, 130.5, 119]"
            "] }"));
  }
  SECTION("only lhs")
  {
    for_each(lower_strictly(A), B)([](const double x, double& y) { y += x; });

    CHECK(
        B ==
        make_from_json<matrix_staged<double, 2>>("{ \"size\": [4],"
                                                 "  \"values\":"
                                                 "[[100, 101, 102, 106.5, 104],"
                                                 " [105, 106, 107, 114.5, 109],"
                                                 " [110, 111, 112, 122.5, 114],"
                                                 " [115, 116, 117, 130.5, 119]"
                                                 "] }"));
  }
}

TEST_CASE(
    "t_span_adaptor_triangular to_json",
    "[t_span_adaptor_triangular][to_json][lower]")
{
  auto B = make_cell<matrix_staged<double, 2>>(4)(g_iota(100.0));

  CHECK(
      to_json(lower(B)) ==
      "{\n"
      "  \"size\": [4, 4],\n"
      "  \"values\":\n"
      "    [[102, 103, 104],\n"
      "     [107, 108, 109],\n"
      "     [112, 113, 114],\n"
      "     [117, 118, 119]\n"
      "    ]\n"
      "}");
}

TEST_CASE(
    "t_span_adaptor_triangular members",
    "[t_span_adaptor_triangular][members]"
    "[upper][lower_strictly]")
{
  auto A = make_cell<matrix<double>>(4, 3)(g_iota(0.5));

  SECTION("upper")
  {
    auto uA = upper(A);

    CHECK(uA.size() == index(4, 3));

    CHECK(*uA.get_ptr(1, 2) == 9.5);

    CHECK(uA.is_bounded(1, 2));
    CHECK(uA.is_bounded(2, 2));
    CHECK(!uA.is_bounded(3, 2));

    auto it = uA.iterator(1, 1);

    CHECK(it.pos() == index(1, 1));
    CHECK(it == uA.iterator(1, 1));
    CHECK(*it == 5.5);
    ++it;
    CHECK(it.pos() == index(0, 2));
    CHECK(it == uA.iterator(0, 2));
    CHECK(*it == 8.5);
    ++it;
    CHECK(it.pos() == index(1, 2));
    CHECK(it == uA.iterator(1, 2));
    CHECK(*it == 9.5);
    ++it;
    CHECK(it.pos() == index(2, 2));
    CHECK(it == uA.iterator(2, 2));
    CHECK(*it == 10.5);
    ++it;

    CHECK(it == uA.end());

    --it;
    CHECK(it.pos() == index(2, 2));
    CHECK(it == uA.iterator(2, 2));
    CHECK(*it == 10.5);
    --it;
    CHECK(it.pos() == index(1, 2));
    CHECK(it == uA.iterator(1, 2));
    CHECK(*it == 9.5);
    --it;
    CHECK(it.pos() == index(0, 2));
    CHECK(it == uA.iterator(0, 2));
    CHECK(*it == 8.5);
    --it;
    CHECK(it.pos() == index(1, 1));
    CHECK(it == uA.iterator(1, 1));
    CHECK(*it == 5.5);
    --it;
    CHECK(it.pos() == index(0, 1));
    CHECK(it == uA.iterator(0, 1));
    CHECK(*it == 4.5);
    --it;
    CHECK(it.pos() == index(0, 0));
    CHECK(it == uA.iterator(0, 0));
    CHECK(*it == 0.5);

    CHECK(it == uA.begin());
  }
  SECTION("lower_strictly")
  {
    auto uA = lower_strictly(A);

    CHECK(uA.size() == index(4, 3));

    CHECK(*uA.get_ptr(2, 1) == 6.5);

    CHECK(!uA.is_bounded(1, 2));
    CHECK(!uA.is_bounded(2, 2));
    CHECK(uA.is_bounded(3, 2));

    auto it = uA.iterator(2, 1);

    CHECK(it.pos() == index(2, 1));
    CHECK(it == uA.iterator(2, 1));
    CHECK(*it == 6.5);
    ++it;
    CHECK(it.pos() == index(3, 1));
    CHECK(it == uA.iterator(3, 1));
    CHECK(*it == 7.5);
    ++it;
    CHECK(it.pos() == index(3, 2));
    CHECK(it == uA.iterator(3, 2));
    CHECK(*it == 11.5);
    ++it;

    CHECK(it == uA.end());

    --it;
    CHECK(it.pos() == index(3, 2));
    CHECK(it == uA.iterator(3, 2));
    CHECK(*it == 11.5);
    --it;
    CHECK(it.pos() == index(3, 1));
    CHECK(it == uA.iterator(3, 1));
    CHECK(*it == 7.5);
    --it;
    CHECK(it.pos() == index(2, 1));
    CHECK(it == uA.iterator(2, 1));
    CHECK(*it == 6.5);
    --it;
    CHECK(it.pos() == index(3, 0));
    CHECK(it == uA.iterator(3, 0));
    CHECK(*it == 3.5);
    --it;
    CHECK(it.pos() == index(2, 0));
    CHECK(it == uA.iterator(2, 0));
    CHECK(*it == 2.5);
    --it;
    CHECK(it.pos() == index(1, 0));
    CHECK(it == uA.iterator(1, 0));
    CHECK(*it == 1.5);

    CHECK(it == uA.begin());
  }
}
