#include "numa/cell/t_span.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/serialize_json.hpp"

#include "numa/core/accumulators.hpp"
#include "numa/core/functors.hpp"

#include "numa/misc/data.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

TEST_CASE("for-each edge-cases", "[t_span][for_each]")
{
  const auto idata = std_iota_vector<int>(200, 0);

  {
    scalar_span<const int> ss_1(idata.data() + 10, {}, {});
    scalar_span<const int> ss_2(idata.data() + 16, {}, {});

    for_each(ss_1)([](const int& i_1) { CHECK(i_1 == 10); });

    for_each(ss_1, ss_2)([](const int i_1, int i_2) {
      CHECK(i_1 == 10);
      CHECK(i_2 == 16);
    });

    for_each(ss_1).indexed(
        [](const index<0>, const int& i_1) { CHECK(i_1 == 10); });

    for_each(ss_1, ss_2).indexed([](const index<0>&, const int i_1, int i_2) {
      CHECK(i_1 == 10);
      CHECK(i_2 == 16);
    });
  }

  {
    vector_span<const int> vs_1(idata.data() + 10, 3, 1);
    vector_span<const int> vs_2(idata.data() + 16, 3, 1);

    {
      int result = 0;

      for_each(vs_1, vs_2)(
          [&result](auto i_1, const auto& i_2) { result += i_1 + i_2; });

      CHECK(result == 33 + 51);
    }

    {
      int result = 0;

      for_each(vs_1, vs_2)
          .indexed([&result](index<1> ind, auto i_1, const auto& i_2) {
            result += i_1 + i_2 + ind[0];
          });

      CHECK(result == 33 + 51 + 3);
    }
  }

  {
    vector_span<const int> vs_1(idata.data() + 10, 3, 2);
    vector_span<const int> vs_2(idata.data() + 16, 3, 2);
    vector_span<const int> vs_3(idata.data() + 20, 3, 3);

    {
      int result = 0;

      for_each(vs_1)([&result](const int& i_1) { result += i_1; });

      CHECK(result == 36);
    }

    {
      int result = 0;

      for_each(vs_1, vs_2)(
          [&result](const int& i_1, const auto i_2) { result += i_1 + i_2; });

      CHECK(result == 36 + 54);
    }

    {
      int result = 0;

      for_each(vs_1, vs_3)(
          [&result](const int& i_1, const auto i_2) { result += i_1 + i_2; });

      CHECK(result == 36 + 69);
    }
  }
}

TEST_CASE("for-each with single cuboid-span", "[t_span][for_each]")
{
  const auto idata = std_iota_vector<int>(200, 0);

  SECTION("default layout spans")
  {
    const cuboid_span<const int> ic{idata.data(), {2, 3, 2}, {6, 2, 1}};

    int result = 0;

    for_each(ic)([&result](const int& i) { result += i; });

    CHECK(result == 66);
  }
  SECTION("unusual layout spans")
  {
    const cuboid_span<const int> ic{idata.data(), {2, 3, 2}, {8, 16, 4}};

    int result = 0;

    for_each(ic)([&result](const int& i) { result += i; });

    CHECK(result == 66 * 4);
  }
}

TEST_CASE("for-each with two cuboid-spans", "[t_span][for_each]")
{
  std::vector cdata(200, 'a');
  const auto idata = std_iota_vector<int>(200, 0);

  SECTION("default layout spans")
  {
    const cuboid_span<char> cc{cdata.data(), {2, 3, 2}, {1, 2, 6}};
    const cuboid_span<const int> ic{idata.data(), {2, 3, 2}, {1, 2, 6}};

    for_each(cc, ic)([](char& c, const int i) { c += i; });

    CHECK(
        cc ==
        make_from_json<cuboid<char>>("{ \"size\": [2, 3, 2],"
                                     "  \"values\":"
                                     "[[[\"a\", \"b\"],"
                                     "  [\"c\", \"d\"],"
                                     "  [\"e\", \"f\"]"
                                     " ],"
                                     " [[\"g\", \"h\"],"
                                     "  [\"i\", \"j\"],"
                                     "  [\"k\", \"l\"]"
                                     " ]"
                                     "] }")
            .span());
  }
  SECTION("permuted layout spans")
  {
    const cuboid_span<char> cc{cdata.data(), {2, 3, 2}, {3, 1, 6}};
    const cuboid_span<const int> ic{idata.data(), {2, 3, 2}, {3, 1, 6}};

    for_each(cc, ic)([](char& c, int i) { c += i; });

    CHECK(
        cc ==
        make_from_json<cuboid<char>>("{ \"size\": [2, 3, 2],"
                                     "  \"values\":"
                                     "[[[\"a\", \"d\"],"
                                     "  [\"b\", \"e\"],"
                                     "  [\"c\", \"f\"]"
                                     " ],"
                                     " [[\"g\", \"j\"],"
                                     "  [\"h\", \"k\"],"
                                     "  [\"i\", \"l\"]"
                                     " ]"
                                     "] }")
            .span());
  }
  SECTION("differing layout spans")
  {
    const cuboid_span<char> cc{cdata.data(), {2, 3, 2}, {6, 2, 1}};
    const cuboid_span<const int> ic{idata.data(), {2, 3, 2}, {3, 1, 6}};

    for_each(cc, ic)([](char& c, const int& i) { c += i; });

    CHECK(
        cc ==
        make_from_json<cuboid<char>>("{ \"size\": [2, 3, 2],"
                                     "  \"values\":"
                                     "[[[\"a\", \"d\"],"
                                     "  [\"b\", \"e\"],"
                                     "  [\"c\", \"f\"]"
                                     " ],"
                                     " [[\"g\", \"j\"],"
                                     "  [\"h\", \"k\"],"
                                     "  [\"i\", \"l\"]"
                                     " ]"
                                     "] }")
            .span());
  }
  SECTION("big lowest stride")
  {
    const cuboid_span<char> cc{cdata.data(), {2, 3, 2}, {18, 6, 36}};
    const cuboid_span<const int> ic{idata.data(), {2, 3, 2}, {18, 6, 36}};

    for_each(cc, ic)([](char& c, const int i) { c += i / 6; });

    CHECK(
        cc ==
        make_from_json<cuboid<char>>("{ \"size\": [2, 3, 2],"
                                     "  \"values\":"
                                     "[[[\"a\", \"d\"],"
                                     "  [\"b\", \"e\"],"
                                     "  [\"c\", \"f\"]"
                                     " ],"
                                     " [[\"g\", \"j\"],"
                                     "  [\"h\", \"k\"],"
                                     "  [\"i\", \"l\"]"
                                     " ]"
                                     "] }")
            .span());
  }
  SECTION("with for-each-indexed")
  {
    const cuboid_span<char> cc{cdata.data(), {2, 3, 2}, {1, 2, 6}};
    const cuboid_span<const int> ic{idata.data(), {2, 3, 2}, {1, 2, 6}};

    for_each(cc, ic).indexed(
        [](const index<3>& ind, char& c, const int i) { c += (i + ind[1]); });

    CHECK(
        cc ==
        make_from_json<cuboid<char>>("{ \"size\": [2, 3, 2],"
                                     "  \"values\":"
                                     "[[[\"a\", \"b\"],"
                                     "  [\"d\", \"e\"],"
                                     "  [\"g\", \"h\"]"
                                     " ],"
                                     " [[\"g\", \"h\"],"
                                     "  [\"j\", \"k\"],"
                                     "  [\"m\", \"n\"]"
                                     " ]"
                                     "] }")
            .span());
  }
}

TEST_CASE("for-each with bool functor", "[t_span][for_each][non_void_functor]")
{
  auto idata = std_iota_vector<int>(6, 0);

  const matrix_span<int> im{idata.data(), {2, 3}, {1, 2}};

  {
    std::string result;

    for_each(im)([&result](const int i) {
      result += std::to_string(i);
      result += ",";

      return for_each_continue{(i <= 2)};
    });

    CHECK(result == "0,1,2,3,");
  }

  {
    std::string result;

    for_each(im).indexed([&result](const auto ind, const int i) {
      if (ind[1_i] >= 1)
        return;

      result += std::to_string(i);
      result += ",";
    });

    CHECK(result == "0,1,");
  }
}

TEST_CASE(
    "for-each with floating-point function",
    "[t_span][for_each][non_void_functor]")
{
  auto idata = std_iota_vector<double>(6, 0.0);
  const matrix_span<double> im{idata.data(), {2, 2}, {1, 2}};

  {
    for_each(im)(f_square);

    CHECK(
        im ==
        make_from_json<matrix<double>>("{ \"size\": [2, 2],"
                                       "  \"values\":"
                                       "[[0, 1],"
                                       " [4, 9]"
                                       "] }")
            .span());

    for_each(im)(f_sqrt);

    CHECK(
        im ==
        make_from_json<matrix<double>>("{ \"size\": [2, 2],"
                                       "  \"values\":"
                                       "[[0, 1],"
                                       " [2, 3]"
                                       "] }")
            .span());
  }

  auto idata_2 = std_iota_vector<double>(6, 10.0);
  const matrix_span<double> im_2{idata_2.data(), {2, 2}, {1, 2}};

  {
    for_each(im, im_2).indexed(
        [](const auto& ind, const int i, const int j) { return (i + j); });

    CHECK(
        im ==
        make_from_json<matrix<double>>("{ \"size\": [2, 2],"
                                       "  \"values\":"
                                       "[[10, 12],"
                                       " [14, 16]"
                                       "] }")
            .span());

    CHECK(
        im_2 ==
        make_from_json<matrix<double>>("{ \"size\": [2, 2],"
                                       "  \"values\":"
                                       "[[10, 11],"
                                       " [12, 13]"
                                       "] }")
            .span());
  }
}

TEST_CASE("for-each with accumulator", "[t_span][for_each][accumulator]")
{
  auto const vec = make_from_json<vector<double>>(R"(
      { "size": [3], "values": [1.1, 2.2, 3.3] }
  )");

  auto acc = a_plus<double>();

  for_each(vec)(acc);

  CHECK(acc.get() == 6.6);
}
