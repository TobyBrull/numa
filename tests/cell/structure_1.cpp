#include "numa/cell/reform.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/structure.hpp"
#include "numa/cell/tensor.hpp"

#include "catch2/catch.hpp"

using namespace numa;

using test_struct = structure<
    label<m_rows_t, vector<int>>,
    field<m_vals_t, matrix<double>, m_rows_t, m_cols_t>>;

TEST_CASE("basic structure", "[structure][basic]")
{
  static_assert(
      test_struct::structure_info.axis_marks == type_list<m_rows_t, m_cols_t>);
  static_assert(test_struct::structure_info.num_axes == 2);

  static_assert(std::is_same_v<test_struct::value_type, void>);

  static_assert(
      test_struct::required_bytes({3, 2}) ==
      (3 * sizeof(int) + 3 * 2 * sizeof(double)));

  static_assert(
      test_struct::required_bytes({2, 5}) ==
      (2 * sizeof(int) + 2 * 5 * sizeof(double)));

  static_assert(
      test_struct::required_bytes({6, 4}) ==
      (6 * sizeof(int) + 6 * 4 * sizeof(double)));

  test_struct ts;

  CHECK(ts.vary_axes().selected_axes == type_list_t{0_i, 1_i});

  CHECK(ts.vary_axes(m_rows_t{}).selected_axes == type_list_t{0_i});

  CHECK(ts.vary_axes<m_cols_t>().selected_axes == type_list_t{1_i});

  CHECK(
      ts.vary_axes(m_cols_t{}, m_rows_t{}).selected_axes ==
      type_list_t{1_i, 0_i});

  CHECK(
      ts.vary_axes<m_rows_t, m_cols_t>().selected_axes ==
      type_list_t{0_i, 1_i});

  CHECK(ts.vary_axes(1_i, 0_i).selected_axes == type_list_t{1_i, 0_i});

  CHECK(ts.vary_axes<1>().selected_axes == type_list_t{1_i});

  CHECK(ts.vary_axes(index_list<1, 0>).selected_axes == type_list_t{1_i, 0_i});

  CHECK(ts.size() == index(0, 0));
  CHECK(ts.capacity() == index(0, 0));

  reserve(ts.vary_axes(), {5, 3});
  resize(ts.vary_axes(), {3, 1});

  CHECK(ts.size() == index(3, 1));
  CHECK(ts.size<0>() == 3);
  CHECK(ts.size<m_cols_t>() == 1);
  CHECK(ts.size(m_cols_t{}) == 1);

  CHECK(ts.capacity() == index(5, 3));
  CHECK(ts.capacity<0>() == 5);
  CHECK(ts.capacity<m_rows_t>() == 5);
  CHECK(ts.capacity(m_rows_t{}) == 5);
  CHECK(ts.capacity<1>() == 3);

  CHECK(ts.get<m_rows_t>().size() == index(3));
  CHECK(ts.get<m_vals_t>().span().size() == index(3, 1));

  CHECK(ts.get(m_rows_t{}).size() == index(3));
  CHECK(ts.get<m_rows_t>().size() == index(3));
  CHECK(ts.get(0_i).size() == index(3));
  CHECK(ts.get<0>().size() == index(3));

  static_assert(
      std::is_same_v<decltype(ts.get<m_rows_t>()), tensor_unvaried<int, 1>&>);

  CHECK(ts.get(m_vals_t{}).size() == index(3, 1));
  CHECK(ts.get<m_vals_t>().size() == index(3, 1));
  CHECK(ts.get(1_i).size() == index(3, 1));
  CHECK(ts.get<1>().size() == index(3, 1));

  static_assert(
      std::is_same_v<decltype(ts.get<1>()), tensor_unvaried<double, 2>&>);

  const auto& const_ts = ts;

  static_assert(std::is_same_v<
                decltype(const_ts.get<m_rows_t>()),
                const tensor_unvaried<int, 1>&>);

  static_assert(std::is_same_v<
                decltype(const_ts.get<1>()),
                const tensor_unvaried<double, 2>&>);

  CHECK(ts.get(m_vals_t{}).span().size() == index(3, 1));
  CHECK(ts.get<m_vals_t>().span().size() == index(3, 1));
  CHECK(ts.get(1_i).span().size() == index(3, 1));
  CHECK(ts.get<1>().span().size() == index(3, 1));

  CHECK(ts.get(m_vals_t{}).size() == index(3, 1));
  CHECK(ts.get<m_vals_t>().size() == index(3, 1));
  CHECK(ts.get(1_i).size() == index(3, 1));
  CHECK(ts.get<1>().size() == index(3, 1));

  CHECK(ts.size() == index(3, 1));
  CHECK(ts.capacity() == index(5, 3));

  matrix<double> rep;

  reserve(rep.vary_axes(), {7, 9});

  CHECK(rep.capacity() == index(7, 9));

  CHECK_THROWS(ts.replace<1>(std::move(rep)));

  CHECK(rep.capacity() == index(7, 9));

  CHECK(ts.size() == index(3, 1));
  CHECK(ts.capacity() == index(5, 3));

  resize(rep.vary_axes(), {3, 1}, 3.1);

  CHECK(ts.const_cell<1>().capacity() == index(5, 3));

  ts.replace(1_i, std::move(rep));

  CHECK(ts.const_cell<1>().capacity() == index(7, 9));
  CHECK(ts.size() == index(3, 1));
  CHECK(ts.capacity() == index(5, 9));

  vector<int> vec;

  reserve(vec.vary_axes(), 8);

  CHECK_THROWS(ts.replace<m_rows_t>(std::move(vec)));

  CHECK(ts.size() == index(3, 1));
  CHECK(ts.capacity() == index(5, 9));

  resize(vec.vary_axes(), 3, 9);

  ts.replace(m_rows_t{}, std::move(vec));

  CHECK(ts.size() == index(3, 1));
  CHECK(ts.capacity() == index(7, 9));
}

DEFINE_MARK(input);
DEFINE_MARK(state);
DEFINE_MARK(output);

template<typename T>
using linear_system = structure<
    field<m_A_t, matrix<T>, m_state_t, m_state_t>,
    field<m_B_t, matrix<T>, m_state_t, m_input_t>,
    field<m_C_t, matrix<T>, m_output_t, m_state_t>,
    field<m_D_t, matrix<T>, m_output_t, m_input_t>>;

TEST_CASE("linear system structure", "[structure][lsys]")
{
  static_assert(
      linear_system<double>::structure_info.axis_marks ==
      type_list<m_state_t, m_input_t, m_output_t>);

  static_assert(linear_system<double>::structure_info.num_axes == 3);

  static_assert(std::is_same_v<linear_system<double>::value_type, double>);

  static_assert(linear_system<double>::required_elements({3, 2, 1}) == 20);
  static_assert(
      linear_system<double>::required_bytes({3, 2, 1}) == 20 * sizeof(double));

  linear_system<double> ls;

  resize(ls.vary_axes(m_output_t{}, m_state_t{}), {1, 4});

  ls.get<m_A_t>().span().at(1, 2) = 4;
  ls.get(m_C_t{}).at(0, 1)        = 2;

  CHECK(
      ls ==
      make_from_json<linear_system<double>>("{\n"
                                            "  \"size\": [4, 0, 1],\n"
                                            "  \"A\": {\n"
                                            "    \"size\": [4, 4],\n"
                                            "    \"values\":\n"
                                            "      [[0, 0, 0, 0],\n"
                                            "       [0, 0, 0, 0],\n"
                                            "       [0, 4, 0, 0],\n"
                                            "       [0, 0, 0, 0]\n"
                                            "      ]\n"
                                            "  },\n"
                                            "  \"B\": {\n"
                                            "    \"size\": [4, 0],\n"
                                            "    \"values\":\n"
                                            "      [[]\n"
                                            "      ]\n"
                                            "  },\n"
                                            "  \"C\": {\n"
                                            "    \"size\": [1, 4],\n"
                                            "    \"values\":\n"
                                            "      [[0],\n"
                                            "       [2],\n"
                                            "       [0],\n"
                                            "       [0]\n"
                                            "      ]\n"
                                            "  },\n"
                                            "  \"D\": {\n"
                                            "    \"size\": [1, 0],\n"
                                            "    \"values\":\n"
                                            "      [[]\n"
                                            "      ]\n"
                                            "  }\n"
                                            "}"));

  resize(ls.vary_axes<m_input_t>(), 2);

  CHECK(ls.vary_axes(m_state_t{}).selected_axes == type_list_t{0_i});
  CHECK(ls.vary_axes<m_input_t>().selected_axes == type_list_t{1_i});
  CHECK(
      ls.vary_axes(m_input_t{}, m_state_t{}).selected_axes ==
      type_list_t{1_i, 0_i});

  CHECK(ls.size() == index(4, 2, 1));
  CHECK(ls.capacity() == index(4, 2, 1));

  CHECK(
      ls ==
      make_from_json<linear_system<double>>("{\n"
                                            "  \"size\": [4, 2, 1],\n"
                                            "  \"A\": {\n"
                                            "    \"size\": [4, 4],\n"
                                            "    \"values\":\n"
                                            "      [[0, 0, 0, 0],\n"
                                            "       [0, 0, 0, 0],\n"
                                            "       [0, 4, 0, 0],\n"
                                            "       [0, 0, 0, 0]\n"
                                            "      ]\n"
                                            "  },\n"
                                            "  \"B\": {\n"
                                            "    \"size\": [4, 2],\n"
                                            "    \"values\":\n"
                                            "      [[0, 0, 0, 0],\n"
                                            "       [0, 0, 0, 0]\n"
                                            "      ]\n"
                                            "  },\n"
                                            "  \"C\": {\n"
                                            "    \"size\": [1, 4],\n"
                                            "    \"values\":\n"
                                            "      [[0],\n"
                                            "       [2],\n"
                                            "       [0],\n"
                                            "       [0]\n"
                                            "      ]\n"
                                            "  },\n"
                                            "  \"D\": {\n"
                                            "    \"size\": [1, 2],\n"
                                            "    \"values\":\n"
                                            "      [[0],\n"
                                            "       [0]\n"
                                            "      ]\n"
                                            "  }\n"
                                            "}"));

  clear(ls.vary_axes());

  resize(ls.vary_axes(), {4, 2, 2}, 1.0);
  insert_size(ls.vary_axes(), {2, 1, 1}, {2, 1, 1}, 2.0);
  insert_size(ls.vary_axes(m_output_t{}), {3}, {2}, 3.0);
  erase_size(
      ls.vary_axes(m_output_t{}, m_input_t{}, m_state_t{}),
      {2, 0, 1},
      {2, 1, 2});

  CHECK(
      ls ==
      make_from_json<linear_system<double>>("{\n"
                                            "  \"size\": [4, 2, 3],\n"
                                            "  \"A\": {\n"
                                            "    \"size\": [4, 4],\n"
                                            "    \"values\":\n"
                                            "      [[1, 2, 1, 1],\n"
                                            "       [2, 2, 2, 2],\n"
                                            "       [1, 2, 1, 1],\n"
                                            "       [1, 2, 1, 1]\n"
                                            "      ]\n"
                                            "  },\n"
                                            "  \"B\": {\n"
                                            "    \"size\": [4, 2],\n"
                                            "    \"values\":\n"
                                            "      [[2, 2, 2, 2],\n"
                                            "       [1, 2, 1, 1]\n"
                                            "      ]\n"
                                            "  },\n"
                                            "  \"C\": {\n"
                                            "    \"size\": [3, 4],\n"
                                            "    \"values\":\n"
                                            "      [[1, 2, 3],\n"
                                            "       [2, 2, 3],\n"
                                            "       [1, 2, 3],\n"
                                            "       [1, 2, 3]\n"
                                            "      ]\n"
                                            "  },\n"
                                            "  \"D\": {\n"
                                            "    \"size\": [3, 2],\n"
                                            "    \"values\":\n"
                                            "      [[2, 2, 3],\n"
                                            "       [1, 2, 3]\n"
                                            "      ]\n"
                                            "  }\n"
                                            "}"));
}

using test_struct_2 = structure<
    field<m_w_t, cuboid<std::string>, m_a_t, m_b_t, m_a_t>,
    field<m_x_t, cuboid<double>, m_b_t, m_c_t, m_b_t>,
    field<m_y_t, matrix<double>, m_c_t, m_a_t>,
    field<m_z_t, sparse_cuboid<int>, m_d_t, m_c_t, m_b_t>>;

TEST_CASE("advanced structure", "[structure][advanced]")
{
  test_struct_2 ts;

  {
    cuboid<std::string> cs;
    reserve(cs.vary_axes(), {19, 17, 13});

    cuboid<double> cd;
    reserve(cd.vary_axes(), {11, 7, 5});

    matrix<double> md;
    reserve(md.vary_axes(), {9, 23});

    sparse_cuboid<int> sci;
    sci.storage_reserve(23);

    ts.replace(m_w_t{}, std::move(cs));
    ts.replace(m_x_t{}, std::move(cd));
    ts.replace(m_y_t{}, std::move(md));
    ts.replace(m_z_t{}, std::move(sci));
  }

  CHECK(ts.size() == index(0, 0, 0, 0));
  CHECK(ts.capacity() == index(13, 5, 7, std::numeric_limits<index_t>::max()));

  resize(
      ts.vary_axes(),
      {1, 1, 1, 1},
      init<std::string>("XOX"),
      init<double>(3.14),
      init<int>());

  resize(
      ts.vary_axes(),
      {1, 2, 2, 1},
      init<m_x_t>(1.1),
      init<m_y_t>(uninitialized),
      init<std::string>(4, 'g'),
      init<int>());

  ts.get<m_y_t>()(1, 0) = 7.0;

  ts.get(m_z).push_back(index(0, 1, 1), 42);

  CHECK(
      ts ==
      make_from_json<test_struct_2>("{\n"
                                    "  \"size\": [1, 2, 2, 1],\n"
                                    "  \"w\": {\n"
                                    "    \"size\": [1, 2, 1],\n"
                                    "    \"values\":\n"
                                    "      [[[\"XOX\"],\n"
                                    "        [\"gggg\"]\n"
                                    "       ]\n"
                                    "      ]\n"
                                    "  },\n"
                                    "  \"x\": {\n"
                                    "    \"size\": [2, 2, 2],\n"
                                    "    \"values\":\n"
                                    "      [[[3.14, 1.1],\n"
                                    "        [1.1, 1.1]\n"
                                    "       ],\n"
                                    "       [[1.1, 1.1],\n"
                                    "        [1.1, 1.1]\n"
                                    "       ]\n"
                                    "      ]\n"
                                    "  },\n"
                                    "  \"y\": {\n"
                                    "    \"size\": [2, 1],\n"
                                    "    \"values\":\n"
                                    "      [[3.14, 7]\n"
                                    "      ]\n"
                                    "  },\n"
                                    "  \"z\": {\n"
                                    "    \"size\": [1, 2, 2],\n"
                                    "    \"storage_size\": 1,\n"
                                    "    \"indices\": [[0, 1, 1]],\n"
                                    "    \"values\": [42]\n"
                                    "  }\n"
                                    "}"));

  {
    sparse_cuboid<int> sci;
    sci.storage_reserve(4);
    resize(sci.vary_axes(), index(1, 2, 2));
    sci.push_back(index(0, 0, 0), 7);
    sci.push_back(index(0, 1, 1), 8);

    ts.replace(m_z_t{}, std::move(sci));
  }

  insert_size(
      ts.vary_axes<m_b_t>(),
      1,
      1,
      init<std::string>("--"),
      init<double>(0.1),
      init<int>());

  erase_till(ts.vary_axes<m_c_t>(), 0, 1);

  CHECK(
      ts ==
      make_from_json<test_struct_2>("{\n"
                                    "  \"size\": [1, 3, 1, 1],\n"
                                    "  \"w\": {\n"
                                    "    \"size\": [1, 3, 1],\n"
                                    "    \"values\":\n"
                                    "      [[[\"XOX\"],\n"
                                    "        [\"--\"],\n"
                                    "        [\"gggg\"]\n"
                                    "       ]\n"
                                    "      ]\n"
                                    "  },\n"
                                    "  \"x\": {\n"
                                    "    \"size\": [3, 1, 3],\n"
                                    "    \"values\":\n"
                                    "      [[[1.1, 0.1, 1.1]\n"
                                    "       ],\n"
                                    "       [[0.1, 0.1, 0.1]\n"
                                    "       ],\n"
                                    "       [[1.1, 0.1, 1.1]\n"
                                    "       ]\n"
                                    "      ]\n"
                                    "  },\n"
                                    "  \"y\": {\n"
                                    "    \"size\": [1, 1],\n"
                                    "    \"values\":\n"
                                    "      [[7]\n"
                                    "      ]\n"
                                    "  },\n"
                                    "  \"z\": {\n"
                                    "    \"size\": [1, 1, 3],\n"
                                    "    \"storage_size\": 1,\n"
                                    "    \"indices\": [[0, 0, 2]],\n"
                                    "    \"values\": [8]\n"
                                    "  }\n"
                                    "}"));
}
