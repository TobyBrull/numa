#include "numa/cell/tensor.hpp"
#include "numa/cell/tensor_staged.hpp"

#include "numa/misc/fuzz_cell.hpp"

#include "numa/ctxt/phase_monitors.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

constexpr index_t reps = 500000;

/*
 * tensor
 */
template<typename T, Allocator Alloc>
using fuzz_tensor = tensor<T, 2, Alloc>;

TEST_CASE("extensive fuzz test for tensor", "[.][fuzz][extensive][tensor]")
{
  phase_monitor_observe pm(std::cout, c_note);

  fuzz_cell<fuzz_tensor>(reps, 3e-3, "tensor");
}

/*
 * band-tensor
 */
template<typename T, Allocator Alloc>
using fuzz_tensor_staged =
    tensor_staged<T, 2, index_list_t<2>, index_list_t<1>, Alloc>;

TEST_CASE(
    "extensive fuzz test for band-tensor",
    "[.][fuzz][extensive][tensor_staged]")
{
  phase_monitor_observe pm(std::cout, c_note);

  fuzz_cell<fuzz_tensor_staged>(reps, 3e-3, "band-tensor");
}

/*
 * Total test
 */
TEST_CASE("all fuzz tests", "[.][fuzz][total]")
{
  phase_monitor_observe pm(std::cout, c_note);

  phase p{c_volume, "fuzz_cell"_s, 2, NUMA_LOCATION};

  {
    p.start(1);
    p.status("tensor");
    fuzz_cell<fuzz_tensor>(reps, 3e-3, "tensor");
  }

  {
    p.start(1);
    p.status("band-tensor");
    fuzz_cell<fuzz_tensor_staged>(reps, 3e-3, "band-tensor");
  }
}
