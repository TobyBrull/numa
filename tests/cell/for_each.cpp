#include "numa/cell/for_each.hpp"

#include "numa/cell/algorithms.hpp"

#include "numa/cell/serialize_json.hpp"
#include "numa/cell/t_span_adaptor_mirror.hpp"
#include "numa/cell/t_span_adaptor_triangular.hpp"
#include "numa/cell/tensor.hpp"
#include "numa/cell/tensor_sparse.hpp"

#include "numa/core/functors.hpp"
#include "numa/core/generators.hpp"

#include "numa/misc/data.hpp"
#include "numa/misc/type.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

TEST_CASE(
    "large tensor-span with mirror-tensor-span",
    "[t_span][t_span_adaptor_mirror]")
{
  auto data_ts = std_iota_vector<int>(100, 1000);

  t_span<int, 4> ts(data_ts.data(), index(2, 3, 2, 3), index(1, 2, 6, 12));

  auto data_sts = std_iota_vector<int>(100, 0);

  t_span<int, 4> sts(data_sts.data(), index(2, 3, 2, 3), index(1, 2, 6, 12));

  SECTION("using assignment operators")
  {
    CHECK(
        ts ==
        make_from_json<tensor<int, 4>>("{ \"size\": [2, 3, 2, 3],"
                                       "  \"values\":"
                                       "[[[[1000, 1001],"
                                       "   [1002, 1003],"
                                       "   [1004, 1005]"
                                       "  ],"
                                       "  [[1006, 1007],"
                                       "   [1008, 1009],"
                                       "   [1010, 1011]"
                                       "  ]"
                                       " ],"
                                       " [[[1012, 1013],"
                                       "   [1014, 1015],"
                                       "   [1016, 1017]"
                                       "  ],"
                                       "  [[1018, 1019],"
                                       "   [1020, 1021],"
                                       "   [1022, 1023]"
                                       "  ]"
                                       " ],"
                                       " [[[1024, 1025],"
                                       "   [1026, 1027],"
                                       "   [1028, 1029]"
                                       "  ],"
                                       "  [[1030, 1031],"
                                       "   [1032, 1033],"
                                       "   [1034, 1035]"
                                       "  ]"
                                       " ]"
                                       "] }")
            .span());

    {
      tensor<int, 4> temp;
      resize(temp.vary_axes(), {2, 3, 2, 3}, -1);

      copy_from(upper_symmetric(sts), temp);

      CHECK(
          temp ==
          make_from_json<tensor<int, 4>>("{ \"size\": [2, 3, 2, 3],"
                                         "  \"values\":"
                                         "[[[[ 0,  6],"
                                         "   [12, 18],"
                                         "   [24, 30]"
                                         "  ],"
                                         "  [[ 6,  7],"
                                         "   [13, 19],"
                                         "   [25, 31]"
                                         "  ]"
                                         " ],"
                                         " [[[12, 13],"
                                         "   [14, 20],"
                                         "   [26, 32]"
                                         "  ],"
                                         "  [[18, 19],"
                                         "   [20, 21],"
                                         "   [27, 33]"
                                         "  ]"
                                         " ],"
                                         " [[[24, 25],"
                                         "   [26, 27],"
                                         "   [28, 34]"
                                         "  ],"
                                         "  [[30, 31],"
                                         "   [32, 33],"
                                         "   [34, 35]"
                                         "  ]"
                                         " ]"
                                         "] }"));
    }

    ts += upper_symmetric(sts);

    CHECK(
        ts ==
        make_from_json<tensor<int, 4>>("{ \"size\": [2, 3, 2, 3],"
                                       "  \"values\":"
                                       "[[[[1000, 1007],"
                                       "   [1014, 1021],"
                                       "   [1028, 1035]"
                                       "  ],"
                                       "  [[1012, 1014],"
                                       "   [1021, 1028],"
                                       "   [1035, 1042]"
                                       "  ]"
                                       " ],"
                                       " [[[1024, 1026],"
                                       "   [1028, 1035],"
                                       "   [1042, 1049]"
                                       "  ],"
                                       "  [[1036, 1038],"
                                       "   [1040, 1042],"
                                       "   [1049, 1056]"
                                       "  ]"
                                       " ],"
                                       " [[[1048, 1050],"
                                       "   [1052, 1054],"
                                       "   [1056, 1063]"
                                       "  ],"
                                       "  [[1060, 1062],"
                                       "   [1064, 1066],"
                                       "   [1068, 1070]"
                                       "  ]"
                                       " ]"
                                       "] }")
            .span());
  }
  SECTION("using multiple mirror-tensors-spans")
  {
    struct MyF {
      int operator()(const int i) const
      {
        return 1000 * i;
      }
    };

    for_each(ts, upper_symmetric(sts), upper_mirror<MyF>(sts))
        .indexed([](const auto ind, auto& val, const int x, const int y) {
          val = (x + y + ind[0]);
        });

    CHECK(
        ts ==
        make_from_json<tensor<int, 4>>("{ \"size\": [2, 3, 2, 3],"
                                       "  \"values\":"
                                       "[[[[0, 6007],"
                                       "   [12012, 18019],"
                                       "   [24024, 30031]"
                                       "  ],"
                                       "  [[12, 15],"
                                       "   [13013, 19020],"
                                       "   [25025, 31032]"
                                       "  ]"
                                       " ],"
                                       " [[[24, 27],"
                                       "   [28, 20021],"
                                       "   [26026, 32033]"
                                       "  ],"
                                       "  [[36, 39],"
                                       "   [40, 43],"
                                       "   [27027, 33034]"
                                       "  ]"
                                       " ],"
                                       " [[[48, 51],"
                                       "   [52, 55],"
                                       "   [56, 34035]"
                                       "  ],"
                                       "  [[60, 63],"
                                       "   [64, 67],"
                                       "   [68, 71]"
                                       "  ]"
                                       " ]"
                                       "] }")
            .span());
  }
}

TEST_CASE(
    "exhaustive tensor-span with mirror-tensor-span",
    "[t_span][t_span_adaptor_mirror][operators]")
{
  auto data_ts = std_iota_vector<int>(10, 11);
  matrix_span<int> ms(data_ts.data(), index(2, 2), index(1, 3));

  auto data_sts = std_iota_vector<int>(10, 1);
  matrix_span<int> sms(data_sts.data(), index(2, 2), index(1, 2));

  for_each(upper(sms))(g_iota(5));

  SECTION("add & subtract one")
  {
    ms += upper_symmetric(sms);

    CHECK(
        ms ==
        make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                    "  \"values\":[[16, 18], [20, 22]] }")
            .span());

    CHECK(
        sms ==
        make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                    "  \"values\":[[5, 2], [6, 7]] }")
            .span());

    sms.origin_ += 2;

    CHECK(
        sms ==
        make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                    "  \"values\":[[6, 7], [5, 6]] }")
            .span());

    ms -= upper_symmetric(sms);

    CHECK(
        ms ==
        make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                    "  \"values\":[[10, 13], [15, 16]] }")
            .span());
  }
  SECTION("add & subtract two")
  {
    upper(sms) += ms;

    CHECK(
        sms ==
        make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                    "  \"values\":[[16, 2], [20, 22]] }")
            .span());

    upper(sms) -= ms.transposed();

    CHECK(
        sms ==
        make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                    "  \"values\":[[5, 2], [8, 7]] }")
            .span());
  }
  SECTION("copy one")
  {
    copy_from(upper_symmetric(sms), ms);

    CHECK(
        ms ==
        make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                    "  \"values\":[[5, 6], [6, 7]] }")
            .span());
  }
  SECTION("copy two")
  {
    ms(0, 0) = 5;
    ms(1, 0) = 9;
    ms(0, 1) = 2;
    ms(1, 1) = 4;

    copy_into(upper(sms), ms);

    CHECK(
        sms ==
        make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                    "  \"values\":[[5, 2], [2, 4]] }")
            .span());

    copy_into(upper(sms), ms.transposed());

    CHECK(
        sms ==
        make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                    "  \"values\":[[5, 2], [9, 4]] }")
            .span());
  }
  SECTION("all_of & any_of")
  {
    ms(0, 0) = 5;
    ms(1, 0) = 9;
    ms(0, 1) = 2;
    ms(1, 1) = 4;

    sms(0, 0) = 5;
    sms(1, 0) = -1;
    sms(0, 1) = 2;
    sms(1, 1) = 4;

    CHECK(all_of(f_is_equal, upper(sms), ms));
    CHECK(!any_of(f_is_not_equal, upper(sms), ms));

    CHECK(!all_of(f_is_equal, ms, sms));
    CHECK(any_of(f_is_not_equal, ms, sms));

    sms(1, 0) = 9;

    CHECK(all_of(f_is_equal, sms, ms));
    CHECK(all_of(f_is_equal, ms, sms));

    CHECK(!any_of(f_is_not_equal, sms, ms));
    CHECK(!any_of(f_is_not_equal, ms, sms));
  }
}

TEST_CASE(
    "move between t_span_adaptor and t_span",
    "[t_span_adaptor_triangular][t_span][move]")
{
  context<type_monitor> ctx;

  {
    std::vector<traced_non_copy> data_1;
    data_1.emplace_back(1);
    data_1.emplace_back(2);
    data_1.emplace_back(3);
    data_1.emplace_back(4);

    std::vector<traced_non_copy> data_2;
    data_2.emplace_back(10);
    data_2.emplace_back(20);
    data_2.emplace_back(30);

    matrix_span<traced_non_copy> ms(data_1.data(), index(2, 2), index(1, 2));

    matrix_span<traced_non_copy> sms(data_2.data(), index(2, 2), index(1, 1));

    SECTION("default")
    {
      move_into(upper(sms), ms);

      CHECK(data_1.at(0).value() == moved_from_value);
      CHECK(data_1.at(1).value() == 2);
      CHECK(data_1.at(2).value() == moved_from_value);
      CHECK(data_1.at(3).value() == moved_from_value);

      CHECK(data_2.at(0).value() == 1);
      CHECK(data_2.at(1).value() == 3);
      CHECK(data_2.at(2).value() == 4);
    }
    SECTION("transposed")
    {
      move_from(upper(ms.transposed()), upper(sms));

      CHECK(data_1.at(0).value() == moved_from_value);
      CHECK(data_1.at(1).value() == moved_from_value);
      CHECK(data_1.at(2).value() == 3);
      CHECK(data_1.at(3).value() == moved_from_value);

      CHECK(data_2.at(0).value() == 1);
      CHECK(data_2.at(1).value() == 2);
      CHECK(data_2.at(2).value() == 4);
    }
  }

  REQUIRE(ctx.active().size() == 0);
}

TEST_CASE("several algorithms", "[t_span_adaptor_mirror][t_span][count_if]")
{
  auto ms = make_cell<matrix<int>>(2, 2)(g_iota(1));

  ms(1, 0) = 2;
  ms(0, 1) = 2;
  ms(1, 1) = 3;

  auto sms = make_cell<matrix<int>>(2, 2)();
  for_each(upper(sms))(g_iota(1));

  CHECK(count_if(f_is_equal, ms, upper_symmetric(sms)) == 4);
  CHECK(count_if(f_is_equal, upper(sms), ms) == 3);

  ms(1, 0) = 10;
  ms(1, 1) = 10;

  CHECK(count_if(f_is_equal, ms, upper_symmetric(sms)) == 2);
  CHECK(count_if(f_is_equal, upper(sms), ms) == 2);

  CHECK(
      ms ==
      make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                  "  \"values\":"
                                  "[[1, 10],"
                                  " [2, 10]"
                                  "] }"));

  CHECK(
      sms ==
      make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                  "  \"values\":"
                                  "[[1, 0],"
                                  " [2, 3]"
                                  "] }"));

  for_each(upper(sms), ms)(f_swap);

  CHECK(
      ms ==
      make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                  "  \"values\":"
                                  "[[1, 10],"
                                  " [2, 3]"
                                  "] }"));

  CHECK(
      sms ==
      make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                  "  \"values\":"
                                  "[[1, 0],"
                                  " [2, 10]"
                                  "] }"));

  for_each(upper(sms), ms)(f_const_t{4});

  CHECK(
      ms ==
      make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                  "  \"values\":"
                                  "[[1, 10],"
                                  " [2, 3]"
                                  "] }"));

  CHECK(
      sms ==
      make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                  "  \"values\":"
                                  "[[4, 0],"
                                  " [4, 4]"
                                  "] }"));

  for_each(ms)(f_const_t{5});

  CHECK(
      ms ==
      make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                  "  \"values\":"
                                  "[[5, 5],"
                                  " [5, 5]"
                                  "] }"));
}

TEST_CASE(
    "non-void functors",
    "[t_span_adaptor_triangular][t_span][non_void_functor]")
{
  auto ms  = make_cell<matrix<int>>(2, 2)(g_iota(1));
  auto sms = make_cell<matrix<int>>(2, 2)(f_const_t{12});

  for_each(upper(sms))(g_iota(1));

  {
    std::string result;

    for_each(upper(sms), ms)([&](const int i, const int j) {
      result += std::to_string(i);
      result += ',';
      result += std::to_string(j);
      result += ';';

      return for_each_continue{(i < 2)};
    });

    CHECK(result == "1,1;2,3;");
  }

  {
    std::string result;

    for_each(ms, upper(sms))
        .indexed([&](const auto ind, const int i, const int j) {
          if ((ind[0] >= 1) && (ind[1] >= 1)) {
            return for_each_continue{false};
          }

          result += std::to_string(i);
          result += ',';
          result += std::to_string(j);
          result += ';';

          return for_each_continue{true};
        });

    CHECK(result == "1,1;2,12;3,2;");
  }

  {
    for_each(upper(sms))(g_riota(3));
    for_each(upper(sms), ms)(f_max);

    CHECK(
        sms ==
        make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                    "  \"values\":"
                                    "[[3, 12],"
                                    " [3, 4]"
                                    "] }"));

    CHECK(
        ms ==
        make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                    "  \"values\":"
                                    "[[1, 2],"
                                    " [3, 4]"
                                    "] }"));
  }

  {
    for_each(ms, sms).indexed([](const auto& ind, const int i, const int j) {
      return (ind[0] + ind[1] + i + j);
    });

    CHECK(
        sms ==
        make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                    "  \"values\":"
                                    "[[3, 12],"
                                    " [3, 4]"
                                    "] }"));

    CHECK(
        ms ==
        make_from_json<matrix<int>>("{ \"size\": [2, 2],"
                                    "  \"values\":"
                                    "[[4, 15],"
                                    " [7, 10]"
                                    "] }"));
  }
}

TEST_CASE(
    "non-void functors with for-each on "
    "sparse-tensor-span and tensor-span",
    "[tensor_sparse_span][t_span][non_void_functor]")
{
  sparse_cuboid<int> scs;

  resize(scs.vary_axes(), index(4, 2, 3));

  scs.push_back(index(2, 0, 0), 8);
  scs.push_back(index(3, 0, 0), 3);
  scs.push_back(index(1, 0, 2), 5);
  scs.push_back(index(3, 1, 2), -2);

  auto data_ts = std_iota_vector<int>(24, 0);
  cuboid_span<int> cs(data_ts.data(), index(4, 2, 3), index(1, 4, 8));

  for_each(scs, cs)(f_max);

  CHECK(
      scs ==
      make_from_json<sparse_cuboid<int>>(
          "{"
          "  \"size\": [4, 2, 3],"
          "  \"storage_size\": 4,"
          "  \"indices\": [[2, 0, 0], [3, 0, 0], [1, 0, 2], [3, 1, 2]],"
          "  \"values\": [8, 3, 17, 23]"
          "}"));

  int result = 0;

  for_each(scs, cs).indexed([&](const auto& ind, int i, int j) {
    if (ind[2] > 1) {
      return for_each_continue{false};
    }

    result += i * j;

    return for_each_continue{true};
  });

  CHECK(result == 25);
}

TEST_CASE(
    "add sparse-tensor-span and tensor-span", "[tensor_sparse_span][t_span]")
{
  sparse_matrix<int> sms;

  resize(sms.vary_axes(), index(4, 3));

  sms.push_back(index(2, 0), 8);
  sms.push_back(index(3, 0), 3);
  sms.push_back(index(1, 2), 5);
  sms.push_back(index(3, 2), -2);

  auto data = std_iota_vector<int>(12, 0);

  const matrix_span<int> ms(data.data(), index(4, 3), index(1, 4));

  SECTION("sparse to dense")
  {
    for_each(sms, ms)([](const auto y, auto& x) { x += y; });

    CHECK(
        ms ==
        make_from_json<matrix<int>>("{ \"size\": [4, 3],"
                                    "  \"values\":"
                                    "[[0, 1, 10, 6],"
                                    " [4, 5, 6, 7],"
                                    " [8, 14, 10, 9]"
                                    "] }")
            .span());
  }
  SECTION("dense to sparse")
  {
    sms += ms;

    CHECK(
        sms ==
        make_from_json<sparse_matrix<int>>(
            "{"
            "  \"size\": [4, 3],"
            "  \"storage_size\": 4,"
            "  \"indices\": [[2, 0], [3, 0], [1, 2], [3, 2]],"
            "  \"values\": [10, 6, 14, 9]"
            "}"));
  }
}
