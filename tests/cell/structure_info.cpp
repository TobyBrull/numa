#include "numa/cell/structure_info.hpp"
#include "numa/cell/tensor.hpp"
#include "numa/cell/tensor_fixed_capacity.hpp"
#include "numa/cell/tensor_fixed_size.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("basic structure-info", "[basic][structure_info_type][constexpr]")
{
  using info_type = structure_info_type<type_list_t<
      label<m_rows_t, vector<int>>,
      field<m_vals_t, matrix<double>, m_rows_t, m_cols_t>>>;

  const info_type info;

  static_assert(std::is_same_v<info_type::value_type, void>);

  static_assert(info.axis_marks == type_list<m_rows_t, m_cols_t>);
  static_assert(info.field_marks == type_list<m_rows_t, m_vals_t>);

  static_assert(info.num_axes == 2);
  static_assert(info.num_fields == 2);

  static_assert(
      info.find_axes(m_rows) ==
      type_list_t{std::tuple{0_i, 0_i}, std::tuple{1_i, 0_i}});

  static_assert(info.find_axes(m_cols) == type_list_t{std::tuple{1_i, 1_i}});

  static_assert(info.vary_axes_info().is_dynamic() == bool_list<true, true>);
  static_assert(
      info.vary_axes_info().is_fixed_capacity() == bool_list<false, false>);
  static_assert(
      info.vary_axes_info().is_fixed_size() == bool_list<false, false>);
}

TEST_CASE("structure-info with fixed sizes", "[structure_info_type][constexpr]")
{
  using info_type = structure_info_type<type_list_t<
      field<m_vals_t, matrix<double>, m_rows_t, m_cols_t>,
      label<m_cols_t, vector_fixed_size<int, 3>>>>;

  const info_type info;

  static_assert(std::is_same_v<info_type::value_type, void>);

  static_assert(info.axis_marks == type_list<m_rows_t, m_cols_t>);
  static_assert(info.field_marks == type_list<m_vals_t, m_cols_t>);

  static_assert(info.num_axes == 2);
  static_assert(info.num_fields == 2);

  static_assert(info.vary_axes_info().is_dynamic() == bool_list<true, false>);
  static_assert(
      info.vary_axes_info().is_fixed_capacity() == bool_list<false, false>);
  static_assert(
      info.vary_axes_info().is_fixed_size() == bool_list<false, true>);
}

TEST_CASE(
    "complex structure-info with fixed sizes",
    "[structure_info_type][constexpr]")
{
  using info_type = structure_info_type<type_list_t<
      field<m_vals_t, matrix_fixed_size<double, 5, 3>, m_rows_t, m_cols_t>,
      label<m_cols_t, vector_fixed_size<int, 3>>,
      label<m_rows_t, vector_fixed_size<int, 5>>>>;

  const info_type info;

  static_assert(std::is_same_v<info_type::value_type, void>);

  static_assert(info.axis_marks == type_list<m_rows_t, m_cols_t>);
  static_assert(info.field_marks == type_list<m_vals_t, m_cols_t, m_rows_t>);

  static_assert(info.num_axes == 2);
  static_assert(info.num_fields == 3);

  static_assert(
      info.vary_axes_info().tuple_list ==
      type_list_t(
          vary_axis_tuple<vary_axis_mode::fixed_size, 5>{},
          vary_axis_tuple<vary_axis_mode::fixed_size, 3>{}));

  static_assert(
      info.vary_axes_info().mode_list ==
      integer_list<
          vary_axis_mode,
          vary_axis_mode::fixed_size,
          vary_axis_mode::fixed_size>);

  static_assert(info.vary_axes_info().value_list == index_list<5, 3>);

  static_assert(info.vary_axes_info().is_dynamic() == bool_list<false, false>);
  static_assert(
      info.vary_axes_info().is_fixed_capacity() == bool_list<false, false>);
  static_assert(info.vary_axes_info().is_fixed_size() == bool_list<true, true>);
}

TEST_CASE(
    "complex structure-info with fixed sizes and capacities",
    "[structure_info_type][constexpr]")
{
  using info_type = structure_info_type<type_list_t<
      field<m_vals_t, matrix_fixed_capacity<int, 8, 9>, m_rows_t, m_cols_t>,
      label<m_cols_t, vector_fixed_capacity<int, 7>>,
      label<m_rows_t, vector_fixed_size<int, 5>>>>;

  const info_type info;

  static_assert(std::is_same_v<info_type::value_type, int>);

  static_assert(info.axis_marks == type_list<m_rows_t, m_cols_t>);
  static_assert(info.field_marks == type_list<m_vals_t, m_cols_t, m_rows_t>);

  static_assert(info.num_axes == 2);
  static_assert(info.num_fields == 3);

  static_assert(
      info.vary_axes_info().tuple_list ==
      type_list_t(
          vary_axis_tuple<vary_axis_mode::fixed_size, 5>{},
          vary_axis_tuple<vary_axis_mode::fixed_capacity, 7>{}));

  static_assert(info.vary_axes_info().is_dynamic() == bool_list<false, false>);
  static_assert(
      info.vary_axes_info().is_fixed_capacity() == bool_list<false, true>);
  static_assert(
      info.vary_axes_info().is_fixed_size() == bool_list<true, false>);
}

struct ForEachFieldFunctor {
 public:
  ForEachFieldFunctor() : oss_(std::make_shared<std::ostringstream>()) {}

  template<
      typename FieldInd,
      typename SelIndices,
      typename LocalAxes,
      typename GlobalAxes>
  void operator()(
      const FieldInd field_ind,
      const SelIndices sel_inds,
      const LocalAxes local_axes,
      const GlobalAxes global_axes) const
  {
    (*oss_) << static_cast<int>(field_ind.value) << ':';

    const auto print = [&](const auto i) {
      (*oss_) << static_cast<int>(i.value) << ',';
    };

    for_each(sel_inds, print);
    (*oss_) << '|';
    for_each(local_axes, print);
    (*oss_) << '|';
    for_each(global_axes, print);
    (*oss_) << '\n';
  }

  std::string str() const
  {
    return oss_->str();
  }

 private:
  std::shared_ptr<std::ostringstream> oss_;
};

TEST_CASE(
    "test structure-info with one fixed size and capacity",
    "[structure_info_type][constexpr]")
{
  using info_type = structure_info_type<type_list_t<
      label<m_rows_t, vector<int>>,
      label<m_cols_t, vector_fixed_size<int, 3>>,
      label<m_lays_t, vector_fixed_capacity<int, 3>>,
      field<m_vals_t, cuboid<double>, m_rows_t, m_cols_t, m_lays_t>>>;

  const info_type info;

  {
    ForEachFieldFunctor feff;

    info.for_each_field(feff, index_list<1>);

    CHECK(
        feff.str() ==
        "1:0,|0,|1,\n"
        "3:0,|1,|1,\n");
  }

  {
    ForEachFieldFunctor feff;

    info.for_each_field(feff, index_list<1, 0>);

    CHECK(
        feff.str() ==
        "0:1,|0,|0,\n"
        "1:0,|0,|1,\n"
        "3:1,0,|0,1,|0,1,\n");
  }

  {
    ForEachFieldFunctor feff;

    info.for_each_field(feff, index_list<0, 2>);

    CHECK(
        feff.str() ==
        "0:0,|0,|0,\n"
        "2:1,|0,|2,\n"
        "3:0,1,|0,2,|0,2,\n");
  }
}
