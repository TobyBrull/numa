#ifndef INCLUDE_GUARD_TEST_CORE_REFORM_UNIFIED_HELPER_H_
#define INCLUDE_GUARD_TEST_CORE_REFORM_UNIFIED_HELPER_H_

#include "numa/boot/index.hpp"
#include "numa/cell/reform_axis.hpp"

namespace numa::detail {
  std::string to_string(op_mode mode);
}

namespace numa::misc {
  class LoggingAxisTarget {
   public:
    LoggingAxisTarget();

    void apply(
        const detail::op_mode mode, const index_t source, const index_t target);

    const std::string& log() const;

   private:
    std::string ops_log_;
  };
}

#endif
