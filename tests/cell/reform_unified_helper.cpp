#include "reform_unified_helper.hpp"

namespace numa::detail {
  std::string to_string(const op_mode mode)
  {
    switch (mode) {
      case op_mode::none:
        return "none";
      case op_mode::move_assign:
        return "move_assign";
      case op_mode::move_construct:
        return "move_construct";
      case op_mode::fill_assign:
        return "fill_assign";
      case op_mode::fill_construct:
        return "fill_construct";
      case op_mode::destruct:
        return "destruct";
      case op_mode::never:
        return "never";
    }

    UNREACHABLE();
  }
}

namespace numa::misc {
  /*
   * LoggingAxisTarget
   */
  LoggingAxisTarget::LoggingAxisTarget() {}

  void LoggingAxisTarget::apply(
      const detail::op_mode mode, const index_t source, const index_t target)
  {
    ops_log_ += "apply (" + to_string(mode) + ", " + std::to_string(source) +
        ", " + std::to_string(target) + ")\n";
  }

  const std::string& LoggingAxisTarget::log() const
  {
    return ops_log_;
  }
}
