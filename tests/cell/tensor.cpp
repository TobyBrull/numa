#include "numa/cell/tensor.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/make_cell.hpp"
#include "numa/cell/reform.hpp"
#include "numa/cell/serialize_json.hpp"

#include "numa/core/range.hpp"

#include "numa/misc/type.hpp"

#include "catch2/catch.hpp"

#include <numeric>
#include <sstream>
#include <vector>

using namespace numa;
using namespace numa::misc;

TEST_CASE("tensor concepts", "[tensor][concepts]")
{
  static_assert(Cell<matrix<double>>);
  static_assert(is_same_template<matrix<double>, matrix<int>>());
}

TEST_CASE("basic tensor", "[tensor][basic]")
{
  {
    tensor<int, 3> A;

    resize(A.vary_axes(), {3, 2, 2}, 7);

    CHECK(
        A.unvaried() ==
        make_from_json<tensor<int, 3>>("{ \"size\": [3, 2, 2],"
                                       "  \"values\":"
                                       "[[[7, 7, 7],"
                                       "  [7, 7, 7]"
                                       " ],"
                                       " [[7, 7, 7],"
                                       "  [7, 7, 7]"
                                       " ]"
                                       "] }")
            .unvaried());

    resize(A.vary_axes(0_i, 2_i), {4, 3}, 9);

    auto As = A.span();

    As(2, 0, 1) = 11;

    CHECK(
        A ==
        make_from_json<tensor<int, 3>>("{ \"size\": [4, 2, 3],"
                                       "  \"values\":"
                                       "[[[7, 7, 7, 9],"
                                       "  [7, 7, 7, 9]"
                                       " ],"
                                       " [[7, 7, 11, 9],"
                                       "  [7, 7, 7, 9]"
                                       " ],"
                                       " [[9, 9, 9, 9],"
                                       "  [9, 9, 9, 9]"
                                       " ]"
                                       "] }"));
  }

  {
    matrix<int> A;

    resize(A.vary_axes(), {3, 5}, 0);

    const auto As = A.span();

    std::iota(As.begin(), As.end(), 0);

    CHECK(As.at(0, 0) == 0);
    CHECK(As.at(1, 0) == 1);
    CHECK(As.at(2, 0) == 2);

    CHECK(As.at(0, 1) == 3);
    CHECK(As.at(1, 1) == 4);
    CHECK(As.at(2, 1) == 5);

    CHECK(As.at(0, 2) == 6);
    CHECK(As.at(1, 2) == 7);
    CHECK(As.at(2, 2) == 8);

    CHECK(As.at(0, 3) == 9);
    CHECK(As.at(1, 3) == 10);
    CHECK(As.at(2, 3) == 11);

    CHECK(As.at(0, 4) == 12);
    CHECK(As.at(1, 4) == 13);
    CHECK(As.at(2, 4) == 14);

    CHECK(A.size() == index<2>(3, 5));
  }
}

TEST_CASE("tensor as cell-span in for-each", "[tensor][unvaried][for_each]")
{
  vector<int> A;

  resize(A.vary_axes(), 3, -1);

  {
    int val = 0;

    for_each(A)([&val](const int x) { val += x; });

    CHECK(val == -3);
  }
}

TEST_CASE("tensor destruction", "[tensor][destruct][copy][move_only]")
{
  context<type_monitor> ctx;

  {
    matrix<traced_int> A;

    resize(A.vary_axes(), index(3, 2), 2);
  }

  CHECK(ctx.active().size() == 0);

  {
    matrix<traced_int> A;
    resize(A.vary_axes(), index(3, 4), 3);

    const auto copy_A = make_copy(A.span());

    CHECK(copy_A.span()(2, 2).value() == 3);
  }

  CHECK(ctx.active().size() == 0);

  {
    matrix<traced_non_copy> A;

    resize(A.vary_axes(), index(3, 2), 2);
  }

  CHECK(ctx.active().size() == 0);
}

TEST_CASE("tensor transform", "[tensor][transform][transform_indexed]")
{
  matrix<int> A;
  resize(A.vary_axes(), index(3, 4), 3);

  matrix<double> B;
  resize(B.vary_axes(), index(3, 4), 5.5);

  {
    const auto result = make_cell<matrix<std::string>>(
        A.span_const(), B.span_const())([](const int i, const double d) {
      std::ostringstream oss;
      oss << i << ',' << static_cast<int>(d * 2);
      return oss.str();
    });

    matrix<std::string> expected;
    resize(expected.vary_axes(), index(3, 4), "3,11");

    CHECK(result.span() == expected.span());
  }

  {
    const auto result =
        make_cell<matrix<std::string>>(A.span_const(), B.span_const())
            .indexed([](const index<2>& ind, const int i, const double d) {
              std::ostringstream oss;
              oss << i << ',' << static_cast<int>(d * 2) << ',' << ind;
              return oss.str();
            });

    matrix<std::string> expected;
    resize(expected.vary_axes(), index(3, 4), "3,11");

    const auto exp_span = expected.span();
    exp_span(0, 0) += ",[0, 0]";
    exp_span(0, 1) += ",[0, 1]";
    exp_span(0, 2) += ",[0, 2]";
    exp_span(0, 3) += ",[0, 3]";

    exp_span(1, 0) += ",[1, 0]";
    exp_span(1, 1) += ",[1, 1]";
    exp_span(1, 2) += ",[1, 2]";
    exp_span(1, 3) += ",[1, 3]";

    exp_span(2, 0) += ",[2, 0]";
    exp_span(2, 1) += ",[2, 1]";
    exp_span(2, 2) += ",[2, 2]";
    exp_span(2, 3) += ",[2, 3]";

    CHECK(result.span() == expected.span());
  }
}

TEST_CASE("bug in tensor resize", "[tensor][bug]")
{
  context<type_monitor> ctx;

  {
    auto A = make_cell<matrix<traced_int>>(4, 4)(f_const_t(traced_int(0)));

    CHECK(ctx.active().size() == 16);

    resize(A.vary_axes(), {0, 2});

    CHECK(ctx.active().size() == 0);

    resize(A.vary_axes<0>(), 2);

    CHECK(ctx.active().size() == 4);
  }

  CHECK(ctx.active().size() == 0);
}

TEST_CASE("zip sort numa::vector", "[zip][vector]")
{
  auto a =
      make_from_json<vector<int>>(R"({ "size": [4], "values": [4, 3, 1, 3] })");

  auto b = make_from_json<vector<int>>(
      R"({ "size": [4], "values": [10, 4, 100, 3] })");

  auto c =
      make_from_json<vector<int>>(R"({ "size": [4], "values": [1, 2, 3, 4] })");

  zip_sort(a, b, c);

  CHECK(
      a ==
      make_from_json<vector<int>>(
          R"({ "size": [4], "values": [1, 3, 3, 4] })"));

  CHECK(
      b ==
      make_from_json<vector<int>>(
          R"({ "size": [4], "values": [100, 3, 4, 10] })"));

  CHECK(
      c ==
      make_from_json<vector<int>>(
          R"({ "size": [4], "values": [3, 4, 2, 1] })"));
}
