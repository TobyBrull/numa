#include "numa/cell/tensor_linear.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/reform.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/t_span.hpp"
#include "numa/cell/tensor.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("basic lin-range", "[lin_range][basic]")
{
  vector_linear_incr<double> lri(6, 5.0, 0.2);

  CHECK(lri.size() == index(6));

  {
    std::ostringstream oss;

    for_each(lri)([&](const auto& val) { oss << val << '\n'; });

    CHECK(oss.str() == "5\n5.2\n5.4\n5.6\n5.8\n6\n");
  }

  //  CHECK (lri == make_from_json<vector_linear_incr<double>> (
  //      "{ \"size\": 11, \"front\": 5.0, \"incr\": 0.2 }"));
}
