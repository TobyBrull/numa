#include "numa/cell/t_span.hpp"

#include "numa/misc/data.hpp"

#include "numa/cell/serialize_json.hpp"
#include "numa/cell/t_span.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

TEST_CASE("reordered t_span_axes", "[t_span_axes][reordered][transposed]")
{
  const auto data = std_iota_vector<int>(100, 0);

  {
    const cuboid_span<const int> cs(data.data(), {2, 4, 3}, {1, 2, 8});

    const auto res_1 = cs.axes(1_i, 2_i).transposed();
    CHECK(res_1.origin_ == cs.origin_);
    CHECK(res_1.size() == index<3>(2, 3, 4));
    CHECK(res_1.stride_ == index<3>(1, 8, 2));

    const auto res_3 = cs.axes().reordered(type_list_t{1_i, 2_i, 0_i});

    CHECK(res_3.origin_ == cs.origin_);
    CHECK(res_3.size() == index<3>(4, 3, 2));
    CHECK(res_3.stride_ == index<3>(2, 8, 1));
  }

  {
    const t_span<const int, 4> ts(data.data(), {2, 4, 3, 2}, {1, 2, 8, 24});

    const auto res_1 =
        ts.axes(0_i, 1_i, 3_i).reordered(type_list_t{2_i, 0_i, 1_i});

    CHECK(res_1.origin_ == ts.origin_);
    CHECK(res_1.size() == index<4>(2, 2, 3, 4));
    CHECK(res_1.stride_ == index<4>(24, 1, 8, 2));

    const auto res_2 = ts.axes(0_i, 1_i, 3_i).reordered(index_list_iota<3>());

    CHECK(res_2.origin_ == ts.origin_);
    CHECK(res_2.size() == ts.size());
    CHECK(res_2.stride_ == ts.stride_);

    const auto res_3 = ts.axes(0_i, 1_i, 3_i).reordered(index_list_riota<3>());

    CHECK(res_3.origin_ == ts.origin_);
    CHECK(res_3.size() == index<4>(2, 4, 3, 2));
    CHECK(res_3.stride_ == index<4>(24, 2, 8, 1));
  }
}

TEST_CASE("reversed t_span_axes", "[t_span_axes][reversed]")
{
  const auto data = std_iota_vector<int>(100, 0);

  {
    const vector_span<const int> vs(data.data(), {3}, {2});

    const auto res = vs.dims(0_i).reversed();
    CHECK(res.origin_ == vs.origin_ + 4);
    CHECK(res.size() == index(3));
    CHECK(res.stride_ == index(-2));
  }

  {
    const cuboid_span<const int> cs(data.data(), {2, 4, 3}, {1, 2, 8});

    const auto res_1 = cs.axes(1_i, 2_i).reversed();
    CHECK(res_1.origin_ == cs.origin_ + 22);
    CHECK(res_1.size() == cs.size());
    CHECK(res_1.stride_ == index<3>(1, -2, -8));

    const auto res_2 = cs.axes().reversed();
    CHECK(res_2.origin_ == cs.origin_ + 23);
    CHECK(res_2.size() == cs.size());
    CHECK(res_2.stride_ == index<3>(-1, -2, -8));
  }

  {
    const t_span<const int, 4> ts(data.data(), {2, 4, 0, 2}, {1, 2, 8, 24});

    const auto res_1 = ts.axes(0_i, 1_i).reversed();
    CHECK(res_1.origin_ == ts.origin_);
    CHECK(res_1.size() == ts.size());
    CHECK(res_1.stride_ == index(-1, -2, 8, 24));

    const auto res_2 = ts.axes(1_i, 3_i, 0_i).reversed();
    CHECK(res_2.origin_ == ts.origin_);
    CHECK(res_2.size() == ts.size());
    CHECK(res_2.stride_ == index(-1, -2, 8, -24));
  }
}

TEST_CASE("taking sub-tensors via t_span_axes", "[t_span_axes][sub]")
{
  const auto data = std_iota_vector<int>(500, 0);

  {
    const vector_span<const int> vs(data.data(), {5}, {3});

    const auto res_1 = vs.dims(0_i).sub(as_size, 2, 3);
    CHECK(res_1.origin_ == data.data() + 6);
    CHECK(res_1.size() == index(3));
    CHECK(res_1.stride_ == index(3));

    const auto res_2 = vs.dims<0>().sub(as_till, 1, 3);
    CHECK(res_2.origin_ == data.data() + 3);
    CHECK(res_2.size() == index(2));
    CHECK(res_2.stride_ == index(3));
  }

  {
    const matrix_span<const int> ms(data.data(), {5, 3}, {3, 20});

    const auto res_1 = ms.dims(0_i).sub(as_size, 2, 3);
    CHECK(res_1.origin_ == data.data() + 6);
    CHECK(res_1.size() == index(3, 3));
    CHECK(res_1.stride_ == index(3, 20));

    const auto res_2 = ms.dims<1>().sub(as_till, 1, 3);
    CHECK(res_2.origin_ == data.data() + 20);
    CHECK(res_2.size() == index(5, 2));
    CHECK(res_2.stride_ == index(3, 20));

    const auto res_3 = ms.axes().sub(as_size, {3, 2}, {2, 1});
    CHECK(res_3.origin_ == data.data() + 49);
    CHECK(res_3.size() == index(2, 1));
    CHECK(res_3.stride_ == index(3, 20));
  }

  {
    const t_span<const int, 4> ts(data.data(), {5, 3, 2, 4}, {2, 10, 30, 60});

    const auto res_1 = ts.dims(2_i).sub(as_size, 1, 1);
    CHECK(res_1.origin_ == data.data() + 30);
    CHECK(res_1.size() == index<4>(5, 3, 1, 4));
    CHECK(res_1.stride_ == index(2, 10, 30, 60));

    const auto res_2 =
        ts.axes(3_i, 2_i, 1_i, 0_i).sub(as_till, {2, 1, 2, 2}, {3, 2, 3, 4});
    CHECK(res_2.origin_ == data.data() + 174);
    CHECK(res_2.size() == index(2, 1, 1, 1));
    CHECK(res_2.stride_ == index(2, 10, 30, 60));

    const auto res_3 = res_2.axes(0_i).span_along(zero);
    CHECK(res_3.origin_ == data.data() + 174);
    CHECK(res_3.size() == index(2));
    CHECK(res_3.stride_ == index(2));
  }
}

TEST_CASE("sub-striding t_span_axes", "[t_span_axes][stride]")
{
  const auto data = std_iota_vector<int>(500, 0);

  {
    const vector_span<const int> vs(data.data(), {5}, {3});

    const auto res_1 = vs.dims(0_i).strided(2);
    CHECK(res_1.origin_ == data.data());
    CHECK(res_1.size() == index(3));
    CHECK(res_1.stride_ == index(6));

    const auto res_2 = vs.dims(0_i).strided(4);
    CHECK(res_2.origin_ == data.data());
    CHECK(res_2.size() == index(2));
    CHECK(res_2.stride_ == index(12));
  }

  {
    const matrix_span<const int> ms(data.data(), {9, 10}, {3, 30});

    const auto res_1 = ms.dims(0_i).strided(3);
    CHECK(res_1.origin_ == data.data());
    CHECK(res_1.size() == index(3, 10));
    CHECK(res_1.stride_ == index(9, 30));

    const auto res_2 = ms.dims(1_i).strided(3);
    CHECK(res_2.origin_ == data.data());
    CHECK(res_2.size() == index(9, 4));
    CHECK(res_2.stride_ == index(3, 90));

    const auto res_3 = ms.axes().strided(4, 3);
    CHECK(res_3.origin_ == data.data());
    CHECK(res_3.size() == index(3, 4));
    CHECK(res_3.stride_ == index(12, 90));
  }

  {
    const t_span<const int, 4> ts(data.data(), {5, 3, 2, 4}, {2, 10, 30, 60});

    const auto res_1 = ts.axes(3_i).strided(2);
    CHECK(res_1.origin_ == data.data());
    CHECK(res_1.size() == index<4>(5, 3, 2, 2));
    CHECK(res_1.stride_ == index(2, 10, 30, 120));

    const auto res_2 = ts.axes(3_i, 2_i, 1_i, 0_i).strided(2, 1, 2, 2);
    CHECK(res_2.origin_ == data.data());
    CHECK(res_2.size() == index(3, 2, 2, 2));
    CHECK(res_2.stride_ == index(4, 20, 30, 120));
  }
}
TEST_CASE(
    "(span|for_each)_(along|plumb)",
    "[t_span_axes][span_along][span_plumb]"
    "[for_each_along][for_each_plumb]")
{
  const auto data = std_iota_vector<int>(100, 0);

  {
    const cuboid_span<const int> cs(data.data(), {2, 4, 3}, {1, 2, 8});

    const auto res_1 = cs.axes(1_i).span_along(1, 2);
    CHECK(res_1.origin_ == cs.origin_ + 17);
    CHECK(res_1.size() == index<1>(4));
    CHECK(res_1.stride_ == index<1>(2));

    const auto res_alt = cs.as_t_span<8>(type_list_t{6_i, 2_i, 5_i})
                             .axes(2_i)
                             .span_along(0, 0, 0, 0, 2, 1, 0);

    CHECK(res_alt.origin_ == cs.origin_ + 17);
    CHECK(res_alt.size() == index<1>(4));
    CHECK(res_alt.stride_ == index<1>(2));

    index<2> ind(1, 2);

    const auto res_2 = cs.dims(2_i).span_along(ind);
    CHECK(res_2.origin_ == cs.origin_ + 5);
    CHECK(res_2.size() == index<1>(3));
    CHECK(res_2.stride_ == index<1>(8));

    const auto res_3 = cs.axes(0_i).span_plumb(0);
    CHECK(res_3.origin_ == cs.origin_);
    CHECK(res_3.size() == index<2>(4, 3));
    CHECK(res_3.stride_ == index<2>(2, 8));

    const auto res_4 = cs.axes(0_i, 1_i).span_plumb(index<2>(1, 2));
    CHECK(res_4.origin_ == cs.origin_ + 5);
    CHECK(res_4.size() == index<1>(3));
    CHECK(res_4.stride_ == index<1>(8));
  }

  {
    const cuboid_span<const int> cs(data.data(), {2, 2, 2}, {1, 2, 4});

    const auto str_1 = [&] {
      std::string retval;
      cs.dims(0_i).for_each_along([&retval](const vector_span<const int> vs) {
        retval += to_json(vs.size());
        retval += ";";

        for (const auto x: vs) {
          retval += to_json(x);
          retval += ',';
        }

        retval += "|";
      });
      return retval;
    }();

    CHECK(str_1 == "[2];0,1,|[2];2,3,|[2];4,5,|[2];6,7,|");

    const auto str_2 = [&] {
      std::string retval;
      cs.axes(2_i, 1_i).for_each_along(
          [&retval](const matrix_span<const int> ms) {
            retval += to_json(ms.size());
            retval += ";";

            for (const auto x: ms) {
              retval += to_json(x);
              retval += ',';
            }

            retval += "|";
          });
      return retval;
    }();

    CHECK(str_2 == "[2, 2];0,4,2,6,|[2, 2];1,5,3,7,|");

    const auto str_3 = [&] {
      std::string retval;
      cs.axes(0_i, 2_i).for_each_plumb(
          [&retval](const vector_span<const int> vs) {
            retval += to_json(vs.size());
            retval += ";";

            for (const auto x: vs) {
              retval += to_json(x);
              retval += ',';
            }

            retval += "|";
          });
      return retval;
    }();

    CHECK(str_3 == "[2];0,2,|[2];1,3,|[2];4,6,|[2];5,7,|");

    const auto str_4 = [&] {
      std::string retval;
      cs.axes(2_i, 0_i).for_each_plumb(
          [&retval](const vector_span<const int> vs) {
            retval += to_json(vs.size());
            retval += ";";

            for (const auto x: vs) {
              retval += to_json(x);
              retval += ',';
            }

            retval += "|";
          });
      return retval;
    }();

    CHECK(str_4 == "[2];0,2,|[2];4,6,|[2];1,3,|[2];5,7,|");
  }
}
