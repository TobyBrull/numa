#include "numa/cell/tensor_uniform.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/reform.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/t_span.hpp"
#include "numa/cell/tensor.hpp"

#include "catch2/catch.hpp"

#include <string>

using namespace numa;

TEST_CASE("uniform-tensor", "[tensor_uniform][basic]")
{
  matrix_uniform<std::string> mf("r2d2");

  CHECK(mf.size() == index(0, 0));

  resize(mf.vary_axes(), {2, 3});

  CHECK(mf.size() == index(2, 3));
  CHECK(mf.capacity() == index<2>(one) * std::numeric_limits<index_t>::max());
  CHECK(mf.required_elements({4, 3}) == 0);

  CHECK(
      mf.span() ==
      make_from_json<matrix<std::string>>(
          R"({ "size": [2, 3],
        "values":
          [["r2d2", "r2d2"],
           ["r2d2", "r2d2"],
           ["r2d2", "r2d2"]
          ] })")
          .span());

  mf.at(0, 2) = "c3po";

  CHECK_THROWS(resize_hard(mf.vary_axes(), {2, 2}));
  CHECK_THROWS(reserve_hard(mf.vary_axes(), {4, 3}));

  CHECK(mf.size() == index(2, 3));

  insert_size(mf.vary_axes(1_i, 0_i), {3, 1}, {2, 1});

  CHECK(mf.size() == index(3, 5));

  CHECK(
      mf.span() ==
      make_from_json<matrix<std::string>>(
          R"({ "size": [3, 5],
        "values":
          [["c3po", "c3po", "c3po"],
           ["c3po", "c3po", "c3po"],
           ["c3po", "c3po", "c3po"],
           ["c3po", "c3po", "c3po"],
           ["c3po", "c3po", "c3po"]
          ] })")
          .span());

  erase_size(mf.vary_axes(), zero, index<2>(one) * 2);

  CHECK(mf.size() == index(1, 3));

  CHECK(
      mf.span() ==
      make_from_json<matrix<std::string>>(
          R"({ "size": [1, 3],
        "values":
          [["c3po"],
           ["c3po"],
           ["c3po"]
          ] })")
          .span());
}
