#include "numa/cell/t_span_adaptor_sub.hpp"

#include "numa/cell/reform.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/tensor.hpp"
#include "numa/cell/tensor_staged.hpp"

#include "numa/core/functors.hpp"
#include "numa/core/generators.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("t_span_adaptor_sub concepts", "[t_span_adaptor_sub][concepts]")
{
  static_assert(Span<t_span_adaptor_sub<matrix_span<double>>>);
}

TEST_CASE("basic t_span_adaptor_sub", "[t_span_adaptor_sub]")
{
  auto A = make_cell<matrix<double>>(4, 3)(f_const_t{3.0});
  auto B = make_cell<matrix<double>>(4, 3)(g_iota(0.5));

  SECTION("case 1")
  {
    auto sub_A = adaptor_sub(A, as_size, {1, 0}, {2, 1});
    auto sub_B = adaptor_sub(B, as_size, {2, 1}, {2, 1});

    CHECK(sub_B.size() == index{2, 1});

    CHECK(sub_B.at(0, 0) == 6.5);
    CHECK(sub_B.at(1, 0) == 7.5);
    CHECK(!sub_A.is_bounded(-1, 0));
    CHECK(!sub_A.is_bounded(2, 0));

    for_each(sub_A, sub_B)(f_plus);

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [4, 3],"
                                       "  \"values\":"
                                       "[[3.0, 9.5, 10.5, 3.0],"
                                       " [3.0, 3.0, 3.0, 3.0],"
                                       " [3.0, 3.0, 3.0, 3.0]"
                                       "] }"));
  }
  SECTION("case 2")
  {
    auto sub_A = adaptor_sub(A, as_size, {0, 0}, {2, 3});
    auto sub_B = adaptor_sub(B, as_size, {2, 0}, {2, 3});

    CHECK(sub_B.size() == index{2, 3});

    sub_A += sub_B;

    CHECK(
        A ==
        make_from_json<matrix<double>>("{ \"size\": [4, 3],"
                                       "  \"values\":"
                                       "[[ 5.5,  6.5, 3.0, 3.0],"
                                       " [ 9.5, 10.5, 3.0, 3.0],"
                                       " [13.5, 14.5, 3.0, 3.0]"
                                       "] }"));
  }
}
