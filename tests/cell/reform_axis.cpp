#include "numa/cell/reform_axis.hpp"

#include "numa/cell/reform.hpp"
#include "numa/cell/tensor.hpp"

#include "numa/core/serialize_json.hpp"

#include "numa/misc/type.hpp"

#include "reform_unified_helper.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::detail;
using namespace numa::misc;

TEST_CASE("inplace-trivial", "[inplace_trivial]")
{
  LoggingAxisTarget tgt;

  inplace_trivial{5}(tgt);

  CHECK(
      tgt.log() ==
      "apply (none, 0, 0)\n"
      "apply (none, 1, 1)\n"
      "apply (none, 2, 2)\n"
      "apply (none, 3, 3)\n"
      "apply (none, 4, 4)\n");
}

TEST_CASE("inplace-resize", "[inplace_resize]")
{
  {
    LoggingAxisTarget tgt;

    inplace_resize{5, 0}(tgt);

    CHECK(
        tgt.log() ==
        "apply (destruct, 0, 0)\n"
        "apply (destruct, 1, 1)\n"
        "apply (destruct, 2, 2)\n"
        "apply (destruct, 3, 3)\n"
        "apply (destruct, 4, 4)\n");
  }

  {
    LoggingAxisTarget tgt;

    inplace_resize{5, 2}(tgt);

    CHECK(
        tgt.log() ==
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (destruct, 2, 2)\n"
        "apply (destruct, 3, 3)\n"
        "apply (destruct, 4, 4)\n");
  }

  {
    LoggingAxisTarget tgt;

    inplace_resize{5, 7}(tgt);

    CHECK(
        tgt.log() ==
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (none, 2, 2)\n"
        "apply (none, 3, 3)\n"
        "apply (none, 4, 4)\n"
        "apply (fill_construct, 5, 5)\n"
        "apply (fill_construct, 6, 6)\n");
  }

  {
    LoggingAxisTarget tgt;

    inplace_resize{5, 5}(tgt);

    CHECK(
        tgt.log() ==
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (none, 2, 2)\n"
        "apply (none, 3, 3)\n"
        "apply (none, 4, 4)\n");
  }
}

TEST_CASE("inplace-insert-size", "[inplace_insert_size]")
{
  {
    LoggingAxisTarget tgt;

    inplace_insert_size{10, 3, 5}(tgt);

    CHECK(
        tgt.log() ==
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (none, 2, 2)\n"
        "apply (move_construct, 9, 14)\n"
        "apply (move_construct, 8, 13)\n"
        "apply (move_construct, 7, 12)\n"
        "apply (move_construct, 6, 11)\n"
        "apply (move_construct, 5, 10)\n"
        "apply (move_assign, 4, 9)\n"
        "apply (move_assign, 3, 8)\n"
        "apply (fill_assign, 3, 3)\n"
        "apply (fill_assign, 4, 4)\n"
        "apply (fill_assign, 5, 5)\n"
        "apply (fill_assign, 6, 6)\n"
        "apply (fill_assign, 7, 7)\n");
  }

  {
    LoggingAxisTarget tgt;

    inplace_insert_size{5, 4, 5}(tgt);

    CHECK(
        tgt.log() ==
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (none, 2, 2)\n"
        "apply (none, 3, 3)\n"
        "apply (move_construct, 4, 9)\n"
        "apply (fill_assign, 4, 4)\n"
        "apply (fill_construct, 5, 5)\n"
        "apply (fill_construct, 6, 6)\n"
        "apply (fill_construct, 7, 7)\n"
        "apply (fill_construct, 8, 8)\n");
  }
}

TEST_CASE("inplace-erase-size", "[inplace_erase_size]")
{
  {
    LoggingAxisTarget tgt;

    inplace_erase_size{10, 3, 5}(tgt);

    CHECK(
        tgt.log() ==
        "apply (none, 0, 0)\n"
        "apply (none, 1, 1)\n"
        "apply (none, 2, 2)\n"
        "apply (move_assign, 8, 3)\n"
        "apply (move_assign, 9, 4)\n"
        "apply (destruct, 5, 5)\n"
        "apply (destruct, 6, 6)\n"
        "apply (destruct, 7, 7)\n"
        "apply (destruct, 8, 8)\n"
        "apply (destruct, 9, 9)\n");
  }
}

TEST_CASE("translate-copy", "[translate_trivial]")
{
  LoggingAxisTarget tgt;

  translate_trivial{5}(tgt);

  CHECK(
      tgt.log() ==
      "apply (move_construct, 0, 0)\n"
      "apply (move_construct, 1, 1)\n"
      "apply (move_construct, 2, 2)\n"
      "apply (move_construct, 3, 3)\n"
      "apply (move_construct, 4, 4)\n");
}

TEST_CASE("translate-resize", "[translate_resize]")
{
  {
    LoggingAxisTarget tgt;

    translate_resize{5, 0}(tgt);

    CHECK(tgt.log() == "");
  }

  {
    LoggingAxisTarget tgt;

    translate_resize{5, 2}(tgt);

    CHECK(
        tgt.log() ==
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n");
  }

  {
    LoggingAxisTarget tgt;

    translate_resize{5, 7}(tgt);

    CHECK(
        tgt.log() ==
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n"
        "apply (move_construct, 2, 2)\n"
        "apply (move_construct, 3, 3)\n"
        "apply (move_construct, 4, 4)\n"
        "apply (fill_construct, 5, 5)\n"
        "apply (fill_construct, 6, 6)\n");
  }

  {
    LoggingAxisTarget tgt;

    translate_resize{5, 5}(tgt);

    CHECK(
        tgt.log() ==
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n"
        "apply (move_construct, 2, 2)\n"
        "apply (move_construct, 3, 3)\n"
        "apply (move_construct, 4, 4)\n");
  }
}

TEST_CASE("translate-insert-size", "[translate_insert_size]")
{
  {
    LoggingAxisTarget tgt;

    translate_insert_size{10, 3, 5}(tgt);

    CHECK(
        tgt.log() ==
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n"
        "apply (move_construct, 2, 2)\n"
        "apply (fill_construct, 3, 3)\n"
        "apply (fill_construct, 4, 4)\n"
        "apply (fill_construct, 5, 5)\n"
        "apply (fill_construct, 6, 6)\n"
        "apply (fill_construct, 7, 7)\n"
        "apply (move_construct, 3, 8)\n"
        "apply (move_construct, 4, 9)\n"
        "apply (move_construct, 5, 10)\n"
        "apply (move_construct, 6, 11)\n"
        "apply (move_construct, 7, 12)\n"
        "apply (move_construct, 8, 13)\n"
        "apply (move_construct, 9, 14)\n");
  }

  {
    LoggingAxisTarget tgt;

    translate_insert_size{5, 4, 5}(tgt);

    CHECK(
        tgt.log() ==
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n"
        "apply (move_construct, 2, 2)\n"
        "apply (move_construct, 3, 3)\n"
        "apply (fill_construct, 4, 4)\n"
        "apply (fill_construct, 5, 5)\n"
        "apply (fill_construct, 6, 6)\n"
        "apply (fill_construct, 7, 7)\n"
        "apply (fill_construct, 8, 8)\n"
        "apply (move_construct, 4, 9)\n");
  }
}

TEST_CASE("translate-erase-size", "[translate_erase_size]")
{
  {
    LoggingAxisTarget tgt;

    translate_erase_size{10, 3, 5}(tgt);

    CHECK(
        tgt.log() ==
        "apply (move_construct, 0, 0)\n"
        "apply (move_construct, 1, 1)\n"
        "apply (move_construct, 2, 2)\n"
        "apply (move_construct, 8, 3)\n"
        "apply (move_construct, 9, 4)\n");
  }
}

TEST_CASE("combine translate-mode", "[combine][op_mode]")
{
  static_assert(
      combine(op_mode::fill_construct, op_mode::fill_assign) ==
      op_mode::fill_construct);

  static_assert(
      combine(op_mode::fill_construct, op_mode::move_assign) ==
      op_mode::fill_construct);

  static_assert(
      combine(op_mode::move_construct, op_mode::move_assign) ==
      op_mode::move_construct);

  static_assert(
      combine(op_mode::move_assign, op_mode::move_assign) ==
      op_mode::move_assign);

  static_assert(
      combine(op_mode::move_assign, op_mode::destruct) == op_mode::destruct);

  static_assert(
      combine(op_mode::move_construct, op_mode::fill_construct) ==
      op_mode::fill_construct);
}

TEST_CASE("execute-op-mode", "[execute_op_mode][op_mode]")
{
  context<type_monitor> ctx;

  vector<traced_int> vec_1;
  vector<traced_int> vec_2;

  resize(vec_1.vary_axes(), 2, 0);
  resize(vec_2.vary_axes(), 2, 2);

  auto span_1 = vec_1.span();
  auto span_2 = vec_2.span();

  vec_1[1]  = traced_int(1);
  span_2[1] = traced_int(3);

  CHECK(ctx.sorted_hash_values() == std::vector<std::size_t>({0, 1, 2, 3}));

  execute_op_mode(op_mode::none, index(0), index(1), vec_1, vec_2, 2);

  CHECK(ctx.sorted_hash_values() == std::vector<std::size_t>({0, 1, 2, 3}));

  execute_op_mode(op_mode::never, index(0), index(1), span_1, span_2, 22);

  CHECK(ctx.sorted_hash_values() == std::vector<std::size_t>({0, 1, 2, 3}));

  execute_op_mode(op_mode::move_assign, index(0), index(1), vec_1, vec_2, 3);

  CHECK(ctx.sorted_hash_values() == std::vector<std::size_t>({0, 0, 1, 2}));

  execute_op_mode(op_mode::destruct, index(-1), index(0), span_1, span_2, 4);

  CHECK(ctx.sorted_hash_values() == std::vector<std::size_t>({0, 0, 1}));

  execute_op_mode(
      op_mode::move_construct, index(1), index(0), span_1, span_2, 5);

  CHECK(ctx.sorted_hash_values() == std::vector<std::size_t>({0, 0, 1, 1}));

  execute_op_mode(op_mode::fill_assign, index(1), index(1), span_1, span_2, 6);

  CHECK(ctx.sorted_hash_values() == std::vector<std::size_t>({0, 1, 1, 6}));

  execute_op_mode(op_mode::destruct, index(-1), index(1), span_1, span_2, 7);

  CHECK(ctx.sorted_hash_values() == std::vector<std::size_t>({0, 1, 1}));

  execute_op_mode(
      op_mode::fill_construct, index(-1), index(1), span_1, span_2, 8);

  CHECK(ctx.sorted_hash_values() == std::vector<std::size_t>({0, 1, 1, 8}));
}

TEST_CASE("axis-target-applier", "[axis_target_applier]")
{
  const std::tuple tuple_fs(
      inplace_insert_size{3, 1, 1}, inplace_insert_size{4, 3, 1});

  std::ostringstream oss;

  apply_axis_target(
      [&](const auto om, const auto& form, const auto& to) {
        oss << to_string(om) << form << to << '\n';
      },
      tuple_fs);

  CHECK(
      oss.str() ==
      // columns 1
      "none[0, 0][0, 0]\n"
      "move_construct[2, 0][3, 0]\n"
      "move_assign[1, 0][2, 0]\n"
      "fill_assign[1, 0][1, 0]\n"
      // columns 2
      "none[0, 1][0, 1]\n"
      "move_construct[2, 1][3, 1]\n"
      "move_assign[1, 1][2, 1]\n"
      "fill_assign[1, 1][1, 1]\n"
      // columns 3
      "none[0, 2][0, 2]\n"
      "move_construct[2, 2][3, 2]\n"
      "move_assign[1, 2][2, 2]\n"
      "fill_assign[1, 2][1, 2]\n"
      // columns 4 to 5
      "move_construct[0, 3][0, 4]\n"
      "move_construct[2, 3][3, 4]\n"
      "move_construct[1, 3][2, 4]\n"
      "fill_construct[1, 3][1, 4]\n"
      // columns 4
      "fill_assign[0, 3][0, 3]\n"
      "fill_construct[2, 3][3, 3]\n"
      "fill_assign[1, 3][2, 3]\n"
      "fill_assign[1, 3][1, 3]\n");
}
