#include "numa/cell/concepts.hpp"

#include "numa/cell/tensor.hpp"
#include "numa/cell/tensor_fixed_capacity.hpp"
#include "numa/cell/tensor_fixed_size.hpp"
#include "numa/cell/tensor_linear.hpp"
#include "numa/cell/tensor_sparse.hpp"
#include "numa/cell/tensor_staged.hpp"
#include "numa/cell/tensor_uniform.hpp"

#include "numa/misc/type.hpp"

#include "catch2/catch.hpp"

#include <complex>
#include <string>

using namespace numa;

TEST_CASE("allocator concept (cell)", "[allocator][concept]")
{
  static_assert(HasAllocator<matrix<int>>);
}

TEST_CASE("span concept", "[span][concept]")
{
  static_assert(Span<matrix_span_staged<double, 2>>);
  static_assert(Span<t_span_diagonal<double, 6>>);

  using Alloc = default_allocator;

  static_assert(!Span<tensor_sparse_unvaried<int, 2, index_t, Alloc>>);
  static_assert(WeakSpan<tensor_sparse_unvaried<int, 2, index_t, Alloc>>);

  static_assert(Span<t_span<int, 2>>);
  static_assert(Span<tensor_unvaried<int, 2>>);
  static_assert(Span<t_span<vector<int>, 2>>);

  static_assert(Span<vector_linear_incr_unvaried<float>>);
}

TEST_CASE("cell concept", "[cell][concept]")
{
  static_assert(Cell<matrix_staged<double, 2>>);
  static_assert(Cell<tensor_diagonal<double, 6>>);

  static_assert(Cell<tensor_fixed_capacity<char, 2, 3>>);
  static_assert(Cell<tensor_fixed_size<std::string, 2, 1, 3>>);

  static_assert(!Cell<tensor_sparse<int, 2>>);
  static_assert(WeakCell<tensor_sparse<int, 2>>);

  static_assert(Cell<tensor<int, 2>>);
  static_assert(Cell<tensor<tensor<int, 1>, 2>>);

  static_assert(Cell<tensor_uniform<float, 1>>);

  static_assert(Cell<vector_linear_incr<float>>);
}
