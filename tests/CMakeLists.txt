# Only needed because executables aren't allowed to no/zero source files.
file (WRITE "${CMAKE_CURRENT_BINARY_DIR}/null.cpp" "// Empty C++ file\n")
add_library (null_lib OBJECT "${CMAKE_CURRENT_BINARY_DIR}/null.cpp")

# Define a single unit-test as a shell for a shared-library that contains a
# bunch of Catch2 TEST_CASEs. Returns the name of the corresponding
# shared-library.
function (AddTestSingle LIB_NAME_VAR)
  cmake_parse_arguments(PARAM "" "DIRNAME;BASENAME" "EXTRA_DEPENDENCIES" ${ARGN})

  set (EXEC_NAME "test.${PARAM_DIRNAME}.${PARAM_BASENAME}")
  set (LIB_NAME "lib.${EXEC_NAME}")

  add_library (${LIB_NAME} SHARED "${PARAM_DIRNAME}/${PARAM_BASENAME}.cpp")
  target_link_libraries (${LIB_NAME} numa_shared catch_prebuilt ${PARAM_EXTRA_DEPENDENCIES})

  add_executable (${EXEC_NAME} "$<TARGET_OBJECTS:null_lib>")
  target_link_libraries(${EXEC_NAME} "${LIB_NAME}")

  # Return
  set (${LIB_NAME_VAR} "${LIB_NAME}" PARENT_SCOPE)
endfunction ()

# Define a module unit-test as a shell for a bunch of shared-libraries, each of
# which contain a bunch of Catch2 TEST_CASEs.
function (AddTestModule)
  cmake_parse_arguments(PARAM "" "DIRNAME" "DIR_LIB_NAMES" ${ARGN})

  set (EXEC_NAME "test.${PARAM_DIRNAME}")

  add_executable (${EXEC_NAME} "$<TARGET_OBJECTS:null_lib>")
  target_link_libraries (${EXEC_NAME} ${PARAM_DIR_LIB_NAMES})

  add_test (NAME ${EXEC_NAME} COMMAND "${EXEC_NAME}")
endfunction ()

# Call 'AddTestSingle' for each cpp-file in the directory given by 'DIRNAME'
# and collect the return value of each of those calls in a list.
# Then call 'AddTestModule' with that list and append that list to the
# 'ALL_LIBS' variable in the PARENT_SCOPE.
function (AddTestDir DIRNAME)
  cmake_parse_arguments(PARAM "" "" "EXTRA_DEPENDENCIES" ${ARGN})

  file (GLOB test_files CONFIGURE_DEPENDS "${DIRNAME}/*.cpp")
  foreach (TEST_FILENAME ${test_files})
    get_filename_component (BASENAME "${TEST_FILENAME}" NAME_WE)
    AddTestSingle (CURRENT_LIB_NAME
      DIRNAME "${DIRNAME}"
      BASENAME "${BASENAME}"
      EXTRA_DEPENDENCIES ${PARAM_EXTRA_DEPENDENCIES}
      )
    list (APPEND DIR_LIB_NAMES "${CURRENT_LIB_NAME}")
  endforeach ()

  AddTestModule (
    DIRNAME "${DIRNAME}"
    DIR_LIB_NAMES ${DIR_LIB_NAMES}
    )

  list (APPEND ALL_LIBS ${DIR_LIB_NAMES})
  set (ALL_LIBS "${ALL_LIBS}" PARENT_SCOPE)
endfunction ()

# Mini library for helping to test the 'cell' module.
add_library (cell_support
    SHARED
    "cell/reform_unified_helper.hpp"
    "cell/reform_unified_helper.cpp"
    "cell/reform_general.inl")
target_link_libraries (cell_support numa_shared)
set_target_properties (cell_support PROPERTIES LINKER_LANGUAGE CXX)

# One directory per module.
AddTestDir (boot)
AddTestDir (ctxt)
AddTestDir (tmpl)
AddTestDir (core)
AddTestDir (cell EXTRA_DEPENDENCIES cell_support)
AddTestDir (lina)
AddTestDir (misc)

# Without this there were problems in the tensor-reform fuzz-test
# that were possibly due to a bug in ASAN.
target_compile_options (lib.test.cell.fuzz_cell
  PRIVATE "-fno-sanitize-address-use-after-scope")

# One test that subsumes all other tests.
add_executable (test.all "$<TARGET_OBJECTS:null_lib>")
target_link_libraries (test.all ${ALL_LIBS})
