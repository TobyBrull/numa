#include "numa/ctxt/thalanx.hpp"

#include "numa/ctxt/context.hpp"

#include "numa/misc/fibonacci.hpp"

#include "catch2/catch.hpp"

#include <iostream>

using namespace numa;

namespace {
  index_t fib(index_t n)
  {
    if (n <= 1) {
      return 1;
    }
    else {
      return fib(n - 1) + fib(n - 2);
    }
  }

  index_t fib_thalanx_1(index_t const n)
  {
    if ((n <= 3) || thalanx::size() == 1) {
      return fib(n) + 1;
    }
    else {
      auto const [f_1, f_2] = thalanx_fork(
          chunking_equal{},
          [n] { return fib_thalanx_1(n - 1); },
          [n] { return fib_thalanx_1(n - 2); });

      return (f_1 + f_2);
    }
  }
}

TEST_CASE("thalanx", "[thalanx]")
{
  index_t const num_threads = 4;
  thalanx ti(num_threads);

  index_t const n = 20;

  index_t const expected = fib(n);

  {
    index_t const result = fib_thalanx_1(n);
    CHECK(result == (expected + num_threads));
  }

  {
    index_t const num_threads_2 = 2;
    thalanx ti_2(num_threads_2);

    index_t const result = fib_thalanx_1(n);
    CHECK(result == (expected + num_threads_2));
  }

  {
    index_t const result = fib_thalanx_1(n);
    CHECK(result == (expected + num_threads));
  }
}

TEST_CASE("thalanx_thread id", "[thalanx][thalanx_thread][id]")
{
  const auto this_id = [] {
    return thalanx_thread::this_thread_id();
  };

  CHECK(this_id() == 0);
  CHECK(thalanx::reserve().empty());

  index_t const num_threads = 4;
  thalanx ti(num_threads);

  index_t const first_new_id = thalanx::reserve().at(0)->id();
  CHECK(first_new_id > 0);

  for (index_t i = 0; (i + 1) < num_threads; ++i) {
    index_t const expected = (first_new_id + i);

    CHECK(thalanx::reserve().at(i)->id() == expected);

    auto fut = thalanx::reserve().at(i)->dispatch(
        this_id, stack_tree_invalid_id, stack_tree_invalid_id);
    CHECK(fut.get() == expected);
  }
}

namespace {
  template<int I>
  class StackCounter : public context<StackCounter<I>>::base {
   public:
    StackCounter(int const plus)
    {
      ++count_ctor_;
      sum_ += plus;
    }

    ~StackCounter()
    {
      ++count_dtor_;
    }

    static auto count()
    {
      return std::tuple<int, int, int>{count_ctor_, sum_, count_dtor_};
    }

   private:
    static std::atomic<int> count_ctor_;
    static std::atomic<int> sum_;
    static std::atomic<int> count_dtor_;
  };
}

TEST_CASE("thalanx stack-count", "[thalanx][internal]")
{
  index_t const num_threads = 4;
  thalanx ti(num_threads);

  /*
   * TODO: count that not more stacks are created than expected.
   */
}

TEST_CASE("test runtime", "[runtime][.]")
{
  int const R = 20;

  int i              = 0;
  auto const f_empty = [&i] {
    ++i;
  };

  auto const time = [R](auto const dut) {
    auto const gt = [] {
      return std::chrono::high_resolution_clock::now();
    };

    std::vector<double> retval;

    retval.reserve(R);

    using Dur = std::chrono::duration<double, std::micro>;

    for (int i = 0; i < R; ++i) {
      auto const start = gt();
      dut();
      auto const end = gt();

      retval.push_back(Dur{end - start}.count());
    }

    std::cout << "[\n";
    std::cout << std::setprecision(15);
    for (double const rt: retval) {
      std::cout << rt << ",\n";
    }
    std::cout << "]\n";
  };

  {
    std::cout << "=======> full thread cycle <=======\nftt = ";
    i = 0;
    time([&] {
      std::thread t(f_empty);
      t.join();
    });
    CHECK(i == R);
  }

  {
    std::cout << "=======> reuse <=======\nreu = ";
    std::function<void()> ff(f_empty);
    i = 0;
    thalanx_thread nt;
    time([&] {
      auto fut = nt.dispatch(ff, stack_tree_invalid_id, stack_tree_invalid_id);
      fut.get();
    });
    CHECK(i == R);
  }

  {
    std::cout << "=======> mutex <=======\nmtx = ";
    i = 0;
    std::mutex m;
    time([&] {
      std::unique_lock lk{m};
      ++i;
    });
    CHECK(i == R);
  }
}
