#include "numa/ctxt/phase.hpp"

#include "numa/misc/fibonacci.hpp"

#include "numa/ctxt/phase_monitors.hpp"
#include "numa/ctxt/thalanx_fork.hpp"

#include "catch2/catch.hpp"

#include <iostream>

using namespace numa;

namespace {
  struct phase_monitor_test : public phase_delegate_target<phase_monitor_test> {
   public:
    phase_monitor_test(
        std::ostream& os,
        phase_level_t const level_record,
        phase_level_t const level_progress)
      : os_(os), pm_(level_record, level_progress, get_delegate())
    {
    }

   private:
    friend class phase_delegate_target<phase_monitor_test>;

    void handle_status(phase_level_t, std::string_view sv) {}

    void handle_progress(progress_stack const& ps)
    {
      //        CHECK (last_status_ <= ps.status ());
      //        last_status_ = ps.status ();
      //
      //        os_ << ps.status () << '\n';

      to_pretty(os_, ps);
      os_ << '\n';
    }

    double last_status_ = 0.0;

    std::ostream& os_;

    context<phase_receptor, phase_receptor_main> pm_;
  };
}

TEST_CASE("basic progress", "[progress]")
{
  index_t const n = 5;

  disable_cursor dc(std::cout);

  phase_monitor_test pmt{std::cout, c_off, c_paragraph};

  misc::fibonacci(n);
}

TEST_CASE("progress with thalanx", "[progress][thalanx]")
{
  // index_t const n = 24;
  index_t const n = 8;

  disable_cursor dc(std::cout);

  thalanx th(4);

  {
    phase_monitor_observe pmt{std::cout, c_off, c_paragraph};

    phase p{c_section, "fib"_s, 4, NUMA_LOCATION};

    p.start(1);
    misc::fibonacci(n);

    p.start(1);
    misc::fibonacci_th(n, chunking_equal{});

    p.start(1);
    misc::fibonacci(n);

    p.start(1);
    misc::fibonacci_th(n, chunking_equal{});
  }
}

TEST_CASE("bad progress", "[progress][bad]")
{
  // index_t const n = 27;
  index_t const n = 7;

  {
    phase_monitor_test pmt{std::cout, c_paragraph, c_paragraph};

    phase p_out{c_section, "fib"_s, 5, NUMA_LOCATION};

    p_out.start(1);
    p_out.start(3);
    {
      phase p{c_section, "fib"_s, 3, NUMA_LOCATION};

      p.start(1);
      misc::fibonacci(n);

      p.start(1, 2.0);
      misc::fibonacci(n);
      misc::fibonacci(n);
      misc::fibonacci(n);

      p.start(1);
      misc::fibonacci(n);
    }
    p_out.start(1);
  }
}

TEST_CASE("bad progress 2", "[progress][bad]")
{
  // index_t const n = 27;
  index_t const n = 9;

  {
    phase_monitor_observe pmt{std::cout, c_paragraph, c_paragraph};

    thalanx th(2);

    misc::fibonacci_th(n, chunking_equal{});
    misc::fibonacci_th(n, chunking_equal{});
    misc::fibonacci_th(n, chunking_equal{});
  }
}

TEST_CASE("bad progress 3", "[progress][bad]")
{
  // index_t const n = 27;
  index_t const n = 9;

  {
    phase_monitor_test pmt{std::cout, c_paragraph, c_paragraph};

    thalanx th(2);

    phase p{c_section, "fib"_s, 2, NUMA_LOCATION};

    p.start(1);

    thalanx_fork(
        chunking_equal{},
        [n] {
          misc::fibonacci(n);
          misc::fibonacci(n);
        },
        [n] { misc::fibonacci(n); });

    p.start(1);
    misc::fibonacci(n);
  }
}
