#include "numa/ctxt/terminal.hpp"

#include "catch2/catch.hpp"

#include <iostream>

using namespace numa;

TEST_CASE("enable cursor", "[enable_cursor][tool][.]")
{
  disable_cursor dc(std::cout);
}
