#include "numa/ctxt/env_var.hpp"

#include "numa/misc/fibonacci.hpp"

#include "catch2/catch.hpp"

using namespace numa;

namespace {
  void f()
  {
    CHECK(env_var("x", c_type<std::string>) == "42");
    CHECK(env_var<double>("y") == 3.14159);
    CHECK(env_var("z") == "Hello, World!");
    CHECK_THROWS(env_var("a", c_type<int>));
    CHECK(env_var("b", c_type<int>) == 1);

    CHECK(env_var("f") == "");
    CHECK_THROWS(env_var("f", c_type<double>));

    {
      std::optional<int> f = env_var_if("f", c_type<int>);
      CHECK(!f);

      CHECK(env_var_default("f", 123) == 123);
    }

    std::optional<bool> b = env_var_if("b", c_type<bool>);
    REQUIRE(b);
    CHECK(b.value() == true);

    CHECK(env_var_default("b", false) == true);
  }

  void g()
  {
    CHECK(env_var("c") == "72");
    CHECK(env_var("x") == "50");
    CHECK(env_var("z") == "Hello, World!");
    CHECK(env_var("f") == "");
  }

  void h()
  {
    CHECK(env_var("c") == "");
    CHECK(env_var("x") == "");
    CHECK(env_var("z") == "");
    CHECK(env_var("f") == "");
  }
}

TEST_CASE("basic env_var", "[env_var]")
{
  {
    numa::env_var_scope es_a(
        {{"x", "42"},
         {"y", "3.14159"},
         {"z", "Hello, World!"},
         {"a", ""},
         {"b", "1"},
         {"command", "cat > input.log"}});

    CHECK(env_var("command") == "cat > input.log");
    CHECK(env_var_default("command", "ls -al") == "cat > input.log");
    CHECK(env_var_default("commando", "ls -al") == "ls -al");

    f();

    {
      numa::env_var_scope es_b({{"x", "50"}, {"c", "72"}});

      g();
    }
  }

  h();
}

namespace {
  index_t fib_env()
  {
    auto const n = env_var("n", c_type<index_t>);

    if (n <= 1) {
      return 1;
    }
    else {
      index_t f_1, f_2;

      {
        env_var_scope esf{{"n", std::to_string(n - 1)}};
        f_1 = fib_env();
      }

      {
        env_var_scope esf{{"n", std::to_string(n - 2)}};
        f_2 = fib_env();
      }

      return (f_1 + f_2);
    }
  }

  index_t fib_env_var_thalanx()
  {
    if ((env_var<index_t>("n") <= 3) || thalanx::size() == 1) {
      return fib_env();
    }
    else {
      auto const [f_1, f_2] = thalanx_fork(
          chunking_equal{},
          [] {
            auto const n = env_var("n", c_type<index_t>);
            env_var_scope esf{{"n", std::to_string(n - 1)}};

            return fib_env_var_thalanx();
          },
          [] {
            auto const n = env_var<index_t>("n");
            env_var_scope esf{{"n", std::to_string(n - 2)}};

            return fib_env_var_thalanx();
          });

      return (f_1 + f_2);
    }
  }
}
TEST_CASE("thalanx with env_var", "[thalanx][env_var]")
{
  index_t const n        = 10;
  index_t const expected = misc::fibonacci(n);

  {
    env_var_scope esf{{"n", std::to_string(n)}};
    thalanx ti(4);

    index_t const result = fib_env_var_thalanx();

    CHECK(result == expected);
  }

  {
    thalanx ti(4);
    env_var_scope esf{{"n", std::to_string(n)}};

    index_t const result = fib_env_var_thalanx();

    CHECK(result == expected);
  }

  {
    env_var_scope esf{{"n", std::to_string(n)}};
    thalanx ti(4);

    {
      env_var_scope temp{{"n", "4"}};
    }

    index_t const result = fib_env_var_thalanx();

    CHECK(result == expected);
  }

  {
    thalanx ti(4);
    env_var_scope esf{{"n", std::to_string(n)}};

    {
      env_var_scope temp{{"n", "4"}};
    }

    index_t const result = fib_env_var_thalanx();

    CHECK(result == expected);
  }

  {
    env_var_scope esf{{"n", std::to_string(n)}};
    thalanx ti(4);

    {
      thalanx temp(3);
    }

    index_t const result = fib_env_var_thalanx();

    CHECK(result == expected);
  }

  {
    thalanx ti(4);
    env_var_scope esf{{"n", std::to_string(n)}};

    {
      thalanx temp(3);
    }

    index_t const result = fib_env_var_thalanx();

    CHECK(result == expected);
  }
}

TEST_CASE("env_var_map_system", "[env_var][env_var_map_system]")
{
  auto const evm = env_var_map_system();

  CHECK(evm.size() >= 2);
  CHECK(evm.count("PATH") > 0);
}

TEST_CASE("env_var_map_command_line", "[env_var][env_var_map_command_line]")
{
  int const argc     = 5;
  char const* argv[] = {
      "my_executable", "n=123", "hello", "m=world", "command=cat > input.log"};

  auto const evm = env_var_map_command_line(argc, argv);

  CHECK(evm.size() == 3);
  CHECK(evm.at("n") == "123");
  CHECK(evm.at("m") == "world");
  CHECK(evm.at("command") == "cat > input.log");
}

TEST_CASE("env_var_map_test", "[env_var][env_var_map_test]")
{
  {
    int const argc     = 3;
    char const* argv[] = {"my_executable", "n=123", "nest"};

    auto const evm =
        env_var_map_test(argc, argv, {{"x", "Hello"}, {"y", "World"}});

    CHECK(evm.empty());
  }

  {
    int const argc     = 3;
    char const* argv[] = {"my_executable", "n=123", "test"};

    auto const evm =
        env_var_map_test(argc, argv, {{"x", "Hello"}, {"y", "World"}});

    CHECK(evm.size() == 2);
    CHECK(evm.at("x") == "Hello");
    CHECK(evm.at("y") == "World");
  }
}
