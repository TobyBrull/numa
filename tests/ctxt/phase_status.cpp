#include "numa/ctxt/phase.hpp"

#include "catch2/catch.hpp"

using namespace numa;

namespace {
  void gun()
  {
    {
      phase p{c_note, "gun"_s, NUMA_LOCATION};
      p.status("gun");
    }

    {
      phase p{c_profile, "gun"_s, NUMA_LOCATION};
      p.status("this should not appear");
    }
  }

  void fun()
  {
    {
      phase p{c_trace, "fun"_s, NUMA_LOCATION};
      p.status("this should not appear");
    }

    {
      phase p{c_error, "fun"_s, NUMA_LOCATION};
      p.status("fun ", 1);
    }

    gun();

    phase_status(c_warning, "fun ", 2);
    phase_status(c_profile, "this should not appear");
  }

  struct phase_monitor_test : public phase_delegate_target<phase_monitor_test> {
   public:
    phase_monitor_test(std::ostream& os, phase_level_t const lvl)
      : os_(os), pm_(lvl, lvl, get_delegate())
    {
    }

   private:
    friend class phase_delegate_target<phase_monitor_test>;

    void handle_status(phase_level_t, std::string_view sv)
    {
      os_ << sv << '\n';
    }

    void handle_progress(progress_stack const&) {}

    void handle_progress_common();

    std::ostream& os_;

    context<phase_receptor, phase_receptor_main> pm_;
  };
}

TEST_CASE("basic phase_status", "[phase_status]")
{
  std::ostringstream oss;

  phase_monitor_test pmt{oss, c_note};

  fun();

  CHECK(oss.str() == "fun 1\ngun\nfun 2\n");
}
