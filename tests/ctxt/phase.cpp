#include "numa/ctxt/phase.hpp"

#include "numa/misc/fibonacci.hpp"

#include "catch2/catch.hpp"

#include <fstream>
#include <iostream>

using namespace numa;

namespace {
  void hun(std::string& result)
  {
    phase p{c_paragraph, "hun"_s, NUMA_LOCATION};

    result += phase_receptor::meh_str();
    result += '\n';
  }

  void gun(std::string& result)
  {
    phase p{c_section, "gun"_s, NUMA_LOCATION};

    hun(result);
  }

  void fun(std::string& result)
  {
    phase p{c_chapter, "fun"_s, NUMA_LOCATION};

    gun(result);

    hun(result);
  }
}

TEST_CASE("basic phase without record", "[phase]")
{
  std::string result;

  fun(result);

  CHECK(
      result ==
      "/fun/gun/hun\n"
      "/fun/hun\n");
}

TEST_CASE("basic phase with record", "[phase]")
{
  context<phase_receptor, phase_receptor_main> scp(
      phase_level_t::all, phase_level_t::all, phase_delegate());

  std::string result;

  fun(result);

  CHECK(
      result ==
      "/fun/gun/hun\n"
      "/fun/hun\n");

  CHECK(scp.record().size() == 8);
}

TEST_CASE("basic phase with partial record", "[phase]")
{
  context<phase_receptor, phase_receptor_main> scp(
      phase_level_t::section, phase_level_t::section, phase_delegate());

  std::string result;

  fun(result);

  CHECK(
      result ==
      "/fun/gun/hun\n"
      "/fun/hun\n");

  CHECK(scp.record().size() == 4);
}
