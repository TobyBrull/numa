#include "numa/ctxt/context.hpp"

#include "catch2/catch.hpp"

using namespace numa;

/*
 * Define TestContext.
 */
class TestContext : public context_base<TestContext> {
 public:
  static std::string get_message(std::string msg)
  {
    std::string retval;

    retval += base::get().id_;
    retval += ": ";
    retval += std::move(msg);
    retval += "\n";

    return retval;
  }

 protected:
  using base = context_base<TestContext>;

  TestContext(base b, thalanx_remote_t, TestContext const&) : base(std::move(b))
  {
  }

  TestContext(base b, const char* const id) : base(std::move(b)), id_(id) {}

  TestContext(base b, root_t) : TestContext(std::move(b), "<global>") {}

 private:
  std::string id_;
};

/*
 * Test static properties.
 */
TEST_CASE("context properties", "[context][property]")
{
  using T = TestContext;

  static_assert(std::is_constructible_v<context<T>>);
  static_assert(!std::is_copy_constructible_v<context<T>>);
  static_assert(!std::is_move_constructible_v<context<T>>);
}

namespace {
  void h(std::string& result)
  {
    result += TestContext::get_message("h called");
  }

  void g(std::string& result)
  {
    result += TestContext::get_message("g called");
  }

  void f(std::string& result)
  {
    context<TestContext> ctx("f");

    g(result);
  }
}

TEST_CASE("basic context", "[context]")
{
  std::string result;

  g(result);

  {
    context<TestContext> ctx("test_case");

    g(result);

    f(result);

    g(result);

    h(result);
  }

  g(result);

  CHECK(
      result ==
      "<global>: g called\n"
      "test_case: g called\n"
      "f: g called\n"
      "test_case: g called\n"
      "test_case: h called\n"
      "<global>: g called\n");
}

struct Base : public context_base<Base> {
 public:
  static std::string func(int const i)
  {
    return base::get().func_impl(i);
  }

 protected:
  using base = context_base<Base>;

  Base(base b, thalanx_remote_t, Base const&) : base(std::move(b)) {}

  Base(base b, std::string name) : base(std::move(b)), name(std::move(name)) {}

  Base(base b, root_t) : Base(std::move(b), "<global>") {}

  std::string name;

 private:
  virtual std::string func_impl(const int i) const
  {
    std::string retval;

    retval += "Base/";
    retval += name;
    retval += ": func (";
    retval += std::to_string(i);
    retval += ")\n";

    return retval;
  };
};

struct Derived : public Base {
 public:
  Derived(base b, std::string name) : Base(std::move(b), std::move(name)) {}

 private:
  virtual std::string func_impl(const int i) const override
  {
    std::string retval;

    retval += "Derived/";
    retval += this->name;
    retval += ": func (";
    retval += std::to_string(i);
    retval += ")\n";

    return retval;
  };
};

TEST_CASE("context with derived class hierachy", "[context]")
{
  std::string result;

  result += Base::func(1);

  context<Base> ctx("test_case");

  {
    context<Base, Derived> ctx2("sub");

    result += Base::func(2);
  }

  {
    context<Base, Derived> der("sub2");

    result += Base::func(3);
  }

  result += Base::func(4);

  CHECK(
      result ==
      "Base/<global>: func (1)\n"
      "Derived/sub: func (2)\n"
      "Derived/sub2: func (3)\n"
      "Base/test_case: func (4)\n");
}

struct WithRoot : public context_base<WithRoot> {
 public:
  static std::string func(const int i)
  {
    std::string retval;

    retval += base::get().name;
    retval += ": func (";
    retval += std::to_string(i);
    retval += ")\n";

    return retval;
  };

 protected:
  using base = context_base<WithRoot>;

  WithRoot(base b, thalanx_remote_t, WithRoot const&) : base(std::move(b)) {}

  WithRoot(base b, std::string name) : base(std::move(b)), name(std::move(name))
  {
  }

  WithRoot(base b, root_t) : base(std::move(b)), name("<global>") {}

  std::string name;
};

TEST_CASE("context with context_root", "[context][context_root]")
{
  std::string result;

  result += WithRoot::func(0);

  {
    context<WithRoot> ctx2("first");

    result += WithRoot::func(1);
  }

  {
    context<WithRoot> init("sub");

    result += WithRoot::func(3);
  }

  result += WithRoot::func(4);

  CHECK(
      result ==
      "<global>: func (0)\n"
      "first: func (1)\n"
      "sub: func (3)\n"
      "<global>: func (4)\n");
}
