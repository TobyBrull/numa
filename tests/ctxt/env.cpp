#include "numa/ctxt/env.hpp"

#include "numa/misc/fibonacci.hpp"

#include "catch2/catch.hpp"

using namespace numa;

namespace {
  struct some_options {
    int num         = 0;
    std::string str = "";
  };

  std::tuple<int, std::string> fun(int x = numa::env(c_type<some_options>).num)
  {
    std::string y = numa::env<some_options>().str;

    return std::tuple{x, y};
  }
}

TEST_CASE("basic env", "[env_var][env]")
{
  numa::env_scope<some_options> esr(42, "numa");

  {
    auto const [x, y] = fun();
    CHECK(x == 42);
    CHECK(y == "numa");
  }

  {
    numa::env_scope esr(c_type<some_options>, 5, "blah");

    {
      auto const [x, y] = fun();
      CHECK(x == 5);
      CHECK(y == "blah");
    }
  }

  {
    auto const [x, y] = fun();
    CHECK(x == 42);
    CHECK(y == "numa");
  }

  {
    auto const [x, y] = fun(8);
    CHECK(x == 8);
    CHECK(y == "numa");
  }
}

namespace {
  struct FibParams {
    index_t n_ = 0;
  };

  index_t fib_env()
  {
    index_t const n = env(c_type<FibParams>).n_;

    if (n <= 1) {
      return 1;
    }
    else {
      index_t f_1, f_2;

      {
        env_scope<FibParams> esf{n - 1};
        f_1 = fib_env();
      }

      {
        env_scope<FibParams> esf{n - 2};
        f_2 = fib_env();
      }

      return (f_1 + f_2);
    }
  }

  index_t fib_env_thalanx()
  {
    if ((env(c_type<FibParams>).n_ <= 3) || thalanx::size() == 1) {
      return fib_env();
    }
    else {
      auto const [f_1, f_2] = thalanx_fork(
          chunking_equal{},
          [] {
            index_t const n = env(c_type<FibParams>).n_;
            env_scope<FibParams> esf{n - 1};

            return fib_env_thalanx();
          },
          [] {
            index_t const n = env(c_type<FibParams>).n_;
            env_scope<FibParams> esf{n - 2};

            return fib_env_thalanx();
          });

      return (f_1 + f_2);
    }
  }
}

TEST_CASE("thalanx with env", "[thalanx][env]")
{
  index_t const n        = 10;
  index_t const expected = misc::fibonacci(n);

  {
    env_scope<FibParams> esf{n};
    thalanx ti(4);

    index_t const result = fib_env_thalanx();

    CHECK(result == expected);
  }

  {
    thalanx ti(4);
    env_scope<FibParams> esf{n};

    index_t const result = fib_env_thalanx();

    CHECK(result == expected);
  }

  {
    env_scope<FibParams> esf{n};
    thalanx ti(4);

    {
      env_scope<FibParams> temp{3};
    }

    index_t const result = fib_env_thalanx();

    CHECK(result == expected);
  }

  {
    thalanx ti(4);
    env_scope<FibParams> esf{n};

    {
      env_scope<FibParams> temp{3};
    }

    index_t const result = fib_env_thalanx();

    CHECK(result == expected);
  }

  {
    env_scope<FibParams> esf{n};
    thalanx ti(4);

    {
      thalanx temp(3);
    }

    index_t const result = fib_env_thalanx();

    CHECK(result == expected);
  }

  {
    thalanx ti(4);
    env_scope<FibParams> esf{n};

    {
      thalanx temp(3);
    }

    index_t const result = fib_env_thalanx();

    CHECK(result == expected);
  }
}
