#include "numa/ctxt/thalanx_batch.hpp"

#include "numa/misc/fibonacci.hpp"

#include "numa/ctxt/phase_monitors.hpp"

#include "catch2/catch.hpp"

#include <iostream>

using namespace numa;

namespace {
  void all_cases(std::span<index_t const> ns)
  {
    disable_cursor dc(std::cout);

    phase_monitor_observe pmo{std::cout};

    {
      ASSERT(thalanx::size() > 4);

      auto const results = thalanx_batch(
          chunking_equal{},
          4,
          [&](index_t const n) {
            return misc::fibonacci_th(n, chunking_equal{});
          },
          ns);

      DUMP_VEC(results);
    }
  }

  std::vector<index_t> make_ns(index_t const num_chunks, index_t const base_n)
  {
    std::vector<index_t> retval;

    index_t j = 0;
    for (index_t i = 0; i < num_chunks; ++i) {
      retval.push_back(base_n + j);
      j = (j + 1) % 5;
    }

    return retval;
  }
}

TEST_CASE("sample: thalanx_batch", "[thalanx_batch][sample][.]")
{
  thalanx th(16);

  all_cases(make_ns(20, 16));
}

TEST_CASE("static thalanx_batch", "[thalanx_batch][static]")
{
  thalanx th(4);

  auto const [f1, f2, f3, f4] = thalanx_batch(
      chunking_equal{},
      2,
      [&](index_t const n) { return misc::fibonacci_th(n, chunking_equal{}); },
      8,
      7,
      6,
      5);

  CHECK(f1 == misc::fibonacci_fast(8));
  CHECK(f2 == misc::fibonacci_fast(7));
  CHECK(f3 == misc::fibonacci_fast(6));
  CHECK(f4 == misc::fibonacci_fast(5));
}

TEST_CASE("dynamic thalanx_batch", "[thalanx_batch][dynamic]")
{
  thalanx th(8);

  all_cases(make_ns(10, 7));
}

TEST_CASE(
    "static thalanx_batch with void return", "[thalanx_batch][static][void]")
{
  thalanx th(4);

  {
    std::atomic<int> count{0};

    thalanx_batch(
        chunking_equal{},
        2,
        [&count] { count += 7; },
        [&count] { count += 1; },
        [&count] { count += 5; },
        [&count] { count += 3; });

    CHECK(count.load() == 16);
  }

  {
    std::atomic<int> count{0};

    const auto [v1, v2] = thalanx_batch(
        chunking_equal{},
        2,
        [&count] { count += 7; },
        [&count] {
          count += 1;
          return std::string("World");
        },
        [&count] {
          count += 5;
          return 71;
        },
        [&count] { count += 3; });

    CHECK(count.load() == 16);
    CHECK(v1 == "World");
    CHECK(v2 == 71);
  }
}

TEST_CASE(
    "dynamic thalanx_batch with void return", "[thalanx_batch][dynamic][void]")
{
  thalanx th(3);

  std::atomic<int> count{0};

  std::vector const vec = {3, 1, 5, 7};

  thalanx_batch(
      chunking_equal{},
      2,
      [&count](int const i) { count += i; },
      std::span{vec});

  CHECK(count.load() == 16);
}
