#include "numa/ctxt/phase_monitors.hpp"

#include "numa/ctxt/phase.hpp"

#include "catch2/catch.hpp"

#include <iostream>

using namespace numa;

namespace {
  void f()
  {
    phase p{c_note, "f"_s, NUMA_LOCATION};

    p.status("Hello, World!");
  }

  int g(int a = 7)
  {
    phase p{c_paragraph, "g"_s, NUMA_LOCATION};

    p.status("running g (", a, ")");

    int x = 0;
    int y = 42;

    phase_status(c_trace, "x = ", x);
    phase_status(c_trace, NUMA_VAR(y));

    {
      phase p{c_trace, "debug"_s, NUMA_LOCATION};
      p.status("x = ", x);
      p.status("y = ", y);
    }

    NUMA_TRACE_VAR(x);

    return y;
  }

  int main_func()
  {
    phase p{c_section, "main"_s, 10, NUMA_LOCATION};

    p.start(3);
    p.status("preamble");
    g();

    p.start(4);
    p.status("core");
    f();

    p.start(3);
    p.status("postprocessing");
    return g();
  }
}

TEST_CASE("phase_monitor_debug", "[phase_monitor][phase_monitor_debug]")
{
  phase_monitor_debug pm(std::cout);

  main_func();
}
