#include "numa/ctxt/progress_estimator.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("basic progress_estimator", "[progress_estimator]") {}

TEST_CASE("to_stream_duration_remaining", "[to_stream_duration_remaining")
{
  {
    progress_estimator::duration_t dur{1e100};

    std::ostringstream oss;
    to_stream_duration_remaining(oss, dur);

    CHECK(oss.str() == "--h --m --s");
  }

  {
    progress_estimator::duration_t dur{-2.0};

    std::ostringstream oss;
    to_stream_duration_remaining(oss, dur);

    CHECK(oss.str() == "--h --m --s");
  }
}
