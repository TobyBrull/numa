#include "numa/ctxt/thalanx_thread.hpp"

#include "catch2/catch.hpp"

#include "numa/boot/error.hpp"
#include <iostream>

using namespace numa;

namespace {
  class SomeClass {
   public:
    SomeClass(int const plus)
    {
      count_ += plus;
    }

    ~SomeClass()
    {
      ++count_;
    }

    static int count()
    {
      return count_;
    }

   private:
    static std::atomic<int> count_;
  };

  std::atomic<int> SomeClass::count_ = 0;
}

TEST_CASE("thalanx_thread stack push and pop", "[thalanx_thread]")
{
  thalanx_thread tth;

  int flag = 0;

  {
    auto rs = tth.push_stack<SomeClass>(42);

    auto fut = tth.dispatch(
        [&] {
          CHECK(SomeClass::count() == 42);
          flag = 7;
        },
        stack_tree_invalid_id,
        stack_tree_invalid_id);

    fut.get();

    CHECK(flag == 7);
    CHECK(SomeClass::count() == 42);
  }

  CHECK(SomeClass::count() == 43);
}
