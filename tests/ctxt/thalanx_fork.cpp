#include "numa/ctxt/thalanx_fork.hpp"

#include "numa/misc/fibonacci.hpp"

#include "numa/ctxt/phase_monitors.hpp"

#include "catch2/catch.hpp"

#include <iostream>

using namespace numa;

namespace {
  void all_cases(index_t const n)
  {
    disable_cursor dc(std::cout);

    phase_monitor_observe pmo{std::cout};

    {
      phase p{c_chapter, "test thalanx_fork"_s, 5, NUMA_LOCATION};

      p.start(1);
      misc::fibonacci_th(n, chunking_equal{});

      p.start(1);
      misc::fibonacci_th(n, chunking_front{});

      p.start(1);
      misc::fibonacci_th(n, chunking_back{});

      p.start(1);
      misc::fibonacci_th(n, chunking_approx({2, 11}));

      p.start(1);
      misc::fibonacci_th_dynamic(n, chunking_equal{});
    }
  }
}

TEST_CASE("sample: all kinds of thalanx_fork", "[thalanx_fork][sample][.]")
{
  thalanx th(16);

  all_cases(24);
}

TEST_CASE(
    "all kinds of thalanx_fork (static & dynamic)",
    "[thalanx_fork][static][dynamic]")
{
  thalanx th(4);

  all_cases(8);
}

TEST_CASE(
    "static thalanx_fork with void return", "[thalanx_fork][static][void]")
{
  thalanx th(3);

  {
    std::atomic<int> count{0};

    thalanx_fork(
        chunking_equal{}, [&count] { ++count; }, [&count] { ++count; });

    CHECK(count.load() == 2);
  }

  {
    std::atomic<int> count{0};

    auto const [v1, v2] = thalanx_fork(
        chunking_equal{},
        [&count] {
          ++count;
          return std::string("Hello");
        },
        [&count] { ++count; },
        [&count] {
          ++count;
          return 7;
        });

    CHECK(count.load() == 3);
    CHECK(v1 == "Hello");
    CHECK(v2 == 7);
  }

  {
    std::atomic<int> count{0};

    auto const [v] = thalanx_fork(
        chunking_equal{},
        [&count] { ++count; },
        [&count] {
          ++count;
          return 11.1;
        },
        [&count] { ++count; });

    CHECK(count.load() == 3);
    CHECK(v == 11.1);

    static_assert(std::Same<decltype(v), double const>);
  }
}

TEST_CASE(
    "dynamic thalanx_fork with void return", "[thalanx_fork][dynamic][void]")
{
  thalanx th(3);

  {
    std::atomic<int> count{0};

    std::vector const vec = {3, 1};

    thalanx_fork(
        chunking_equal{},
        [&count](int const i) { count += i; },
        std::span{vec});

    CHECK(count.load() == 4);
  }
}
