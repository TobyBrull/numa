#include "numa/core/index_detail.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE(
    "index detail (capacity-to-stride)",
    "[index][detail][capacity_to_stride][constexpr]")
{
  static_assert(detail::capacity_to_stride(index<0>()) == index<0>());

  static_assert(detail::capacity_to_stride(index<1>(0)) == index<1>(1));
  static_assert(detail::capacity_to_stride(index<1>(10)) == index<1>(1));

  static_assert(detail::capacity_to_stride(index<2>(0, 5)) == index<2>(1, 0));
  static_assert(detail::capacity_to_stride(index<2>(10, 4)) == index<2>(1, 10));

  static_assert(
      detail::capacity_to_stride(index<4>(10, 0, 6, 8)) ==
      index<4>(1, 10, 0, 0));
  static_assert(
      detail::capacity_to_stride(index<4>(10, 4, 6, 8)) ==
      index<4>(1, 10, 40, 240));
}

TEST_CASE(
    "index detail (head-capacity-dot)",
    "[index][detail][capacity_dot][constexpr]")
{
  static_assert(detail::capacity_dot(index<1>(7), index<1>(3)) == 7);

  static_assert(detail::capacity_dot(index<2>(4, 2), index<2>(3, 8)) == 10);

  static_assert(
      detail::capacity_dot(index<3>(2, 5, 2), index<3>(3, 2, 1)) == 29);
}
