#include "numa/core/random.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("test scalar random functions", "[random]")
{
  std::default_random_engine eng(0);

  for (int i = 1; i < 10; ++i) {
    auto const iu = static_cast<std::size_t>(i);
    {
      const auto i1 = random<int>(i, eng);
      const auto i2 = random<std::size_t>(i, eng);
      const auto i3 = random<index_t>(i, eng);
      const auto i4 = random<index_t>(i, eng);

      CHECK(0 <= i1);
      CHECK(i1 < i);
      CHECK(0u <= i2);
      CHECK(i2 < iu);
      CHECK(0 <= i3);
      CHECK(i3 < i);
      CHECK(0 <= i4);
      CHECK(i4 < i);
    }

    {
      const auto i1 = random_size<int>(i, i, eng);
      const auto i2 = random_size<std::size_t>(i, i, eng);
      const auto i3 = random_size<index_t>(i, i, eng);
      const auto i4 = random_size<index_t>(i, i, eng);

      CHECK(i <= i1);
      CHECK(i1 < (2 * i));
      CHECK(iu <= i2);
      CHECK(i2 < (2 * iu));
      CHECK(i <= i3);
      CHECK(i3 < (2 * i));
      CHECK(i <= i4);
      CHECK(i4 < (2 * i));
    }

    {
      const auto i1 = random_till<int>(i, 2 * i, eng);
      const auto i2 = random_till<std::size_t>(i, 2 * i, eng);
      const auto i3 = random_till<index_t>(i, 2 * i, eng);
      const auto i4 = random_till<index_t>(i, 2 * i, eng);

      CHECK(i <= i1);
      CHECK(i1 < (2 * i));
      CHECK(iu <= i2);
      CHECK(i2 < (2 * iu));
      CHECK(i <= i3);
      CHECK(i3 < (2 * i));
      CHECK(i <= i4);
      CHECK(i4 < (2 * i));
    }
  }
}

TEST_CASE("test index-based random functions", "[random][index]")
{
  std::default_random_engine eng(0);

  const index size(5, 2, 10, 5);
  const index from(2, 4, -30, -12);
  const index till(7, 10, -12, -10);

  const auto result_1 = random(size, eng);
  const auto result_2 = random_size(from, size, eng);
  const auto result_3 = random_till(from, till, eng);

  CHECK(is_bounded(index<4>(zero), result_1));
  CHECK(is_bounded_strictly(result_1, size));

  CHECK(is_bounded(from, result_2));
  CHECK(is_bounded_strictly(result_2, (from + size)));

  CHECK(is_bounded(from, result_3));
  CHECK(is_bounded_strictly(result_3, till));
}

TEST_CASE("random axes", "[random][axes]")
{
  std::default_random_engine eng(0);

  const auto result = random_variation<4, 10>(eng);

  static_assert(result.size() == 4);

  for (const auto val: result) {
    CHECK(val < 10);
  }
}
