#include "numa/core/zip.hpp"

#include "numa/core/functors.hpp"

#include "numa/misc/type.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;
using namespace numa::misc::literals;

TEST_CASE("test zip-iterator", "[zip][zip_range]")
{
  context<type_monitor> ctx;

  SECTION("manual tests")
  {
    std::vector<traced_int> vec_1 = {2_ti, 5_ti, 8_ti, 1_ti};
    std::vector<traced_int> vec_2 = {20_ti, 50_ti, 80_ti, 10_ti};

    const auto zr = make_zip_range(vec_1, vec_2);

    auto it        = zr.begin();
    const auto end = zr.end();

    using value_type = typename std::iterator_traits<decltype(it)>::value_type;

    REQUIRE(it != end);
    value_type val = *it;

    CHECK(std::get<0>(val.get_ref()).value() == 2);
    CHECK(std::get<1>(val.get_ref()).value() == 20);

    ++it;

    REQUIRE(it != end);
    *it = val;

    CHECK(val.get<0>().value() == 2);
    CHECK(val.get<1>().value() == 20);
    CHECK(std::get<0>(val.get_ref()).value() == 2);
    CHECK(std::get<1>(val.get_ref()).value() == 20);

    CHECK(vec_1[0].value() == 2);
    CHECK(vec_1[1].value() == 2);
    CHECK(vec_1[2].value() == 8);
    CHECK(vec_1[3].value() == 1);

    CHECK(vec_2[0].value() == 20);
    CHECK(vec_2[1].value() == 20);
    CHECK(vec_2[2].value() == 80);
    CHECK(vec_2[3].value() == 10);

    val = std::move(*it);

    CHECK(std::get<0>(val.get_ref()).value() == 2);
    CHECK(std::get<1>(val.get_ref()).value() == 20);

    CHECK(vec_1[0].value() == 2);
    CHECK(vec_1[1].value() == moved_from_value);
    CHECK(vec_1[2].value() == 8);
    CHECK(vec_1[3].value() == 1);

    CHECK(vec_2[0].value() == 20);
    CHECK(vec_2[1].value() == moved_from_value);
    CHECK(vec_2[2].value() == 80);
    CHECK(vec_2[3].value() == 10);

    it += 2;
    REQUIRE(it != end);

    *it = std::move(val);

    CHECK(std::get<0>(val.get_ref()).value() == moved_from_value);
    CHECK(std::get<1>(val.get_ref()).value() == moved_from_value);

    CHECK(vec_1[0].value() == 2);
    CHECK(vec_1[1].value() == moved_from_value);
    CHECK(vec_1[2].value() == 8);
    CHECK(vec_1[3].value() == 2);

    CHECK(vec_2[0].value() == 20);
    CHECK(vec_2[1].value() == moved_from_value);
    CHECK(vec_2[2].value() == 80);
    CHECK(vec_2[3].value() == 20);

    it -= 1;

    {
      const auto& [c_val_1, c_val_2] = (*it);

      CHECK(c_val_1.value() == 8);
      CHECK(c_val_2.value() == 80);
    }

    {
      const auto [c_val_1, c_val_2] = (*it);

      CHECK(c_val_1.value() == 8);
      CHECK(c_val_2.value() == 80);
    }

    {
      auto [c_val_1, c_val_2] = (*it);

      CHECK(c_val_1.value() == 8);
      CHECK(c_val_2.value() == 80);
    }

    auto& [val_1, val_2] = (*it);

    val_1 = 1000_ti;
    val_2 = 2000_ti;

    CHECK(vec_1[0].value() == 2);
    CHECK(vec_1[1].value() == moved_from_value);
    CHECK(vec_1[2].value() == 1000);
    CHECK(vec_1[3].value() == 2);

    CHECK(vec_2[0].value() == 20);
    CHECK(vec_2[1].value() == moved_from_value);
    CHECK(vec_2[2].value() == 2000);
    CHECK(vec_2[3].value() == 20);
  }
  SECTION("sort tests")
  {
    std::vector<traced_int> vec_1 = {2_ti, 5_ti, 8_ti, 1_ti};
    std::vector<traced_int> vec_2 = {20_ti, 50_ti, 80_ti, 10_ti};

    zip_sort_unpack(
        [](const auto& lhs_1,
           const auto& lhs_2,
           const auto& rhs_1,
           const auto& rhs_2) {
          return std::tuple(lhs_1.value(), lhs_2.value()) <
              std::tuple(rhs_1.value(), rhs_2.value());
        },
        vec_1,
        vec_2);
  }

  CHECK(ctx.active().empty());
}

TEST_CASE(
    "test zip-sort with unpacked parameter",
    "[zip][make_zip_range][zip_sort_unpack]")
{
  std::vector<int> i_vec = {5, 1, 4, 2};

  const std::string nn           = "nnnnnnnnnnnnnnnnnnnn";
  const std::string uu           = "uuuuuuuuuuuuuu";
  const std::string mm           = "mmmmmmmmmmmmmmmmmmmmmmmm";
  const std::string aa           = "aaaaaaaaaaaaaaaaa";
  std::vector<std::string> s_vec = {aa, nn, mm, uu};

  SECTION("by first ascending")
  {
    zip_sort_unpack(
        [](const int& lhs_i,
           const std::string& lhs_s,
           const int& rhs_i,
           const std::string& rhs_s) { return (lhs_i < rhs_i); },
        i_vec,
        s_vec);

    CHECK(i_vec == std::vector<int>{1, 2, 4, 5});
    CHECK(s_vec == std::vector<std::string>{nn, uu, mm, aa});
  }
  SECTION("by first descending")
  {
    zip_sort_unpack(
        [](const int& lhs_i,
           const std::string& lhs_s,
           const int& rhs_i,
           const std::string& rhs_s) { return (lhs_i > rhs_i); },
        i_vec,
        s_vec);

    CHECK(i_vec == std::vector<int>{5, 4, 2, 1});
    CHECK(s_vec == std::vector<std::string>{aa, mm, uu, nn});
  }
  SECTION("by second ascending")
  {
    zip_sort_unpack(
        [](const int& lhs_i,
           const std::string& lhs_s,
           const int& rhs_i,
           const std::string& rhs_s) { return (lhs_s < rhs_s); },
        i_vec,
        s_vec);

    CHECK(i_vec == std::vector<int>{5, 4, 1, 2});
    CHECK(s_vec == std::vector<std::string>{aa, mm, nn, uu});
  }
  SECTION("by second descending")
  {
    zip_sort_unpack(
        [](const int& lhs_i,
           const std::string& lhs_s,
           const int& rhs_i,
           const std::string& rhs_s) { return (lhs_s > rhs_s); },
        i_vec,
        s_vec);

    CHECK(i_vec == std::vector<int>{2, 1, 4, 5});
    CHECK(s_vec == std::vector<std::string>{uu, nn, mm, aa});
  }
}

TEST_CASE(
    "test zip-sort lexicographically and with tuple parameter",
    "[zip][make_zip_range][zip_sort][zip_sort_tuple]")
{
  std::vector<int> i_vec    = {1, 2, 1, 2, 1, 2};
  std::vector<double> d_vec = {2.2, 5.5, 3.3, 1.1, 0.3, 7.7};

  SECTION("lexicographically")
  {
    zip_sort(i_vec, d_vec);

    CHECK(i_vec == std::vector<int>{1, 1, 1, 2, 2, 2});
    CHECK(d_vec == std::vector<double>{0.3, 2.2, 3.3, 1.1, 5.5, 7.7});
  }
  SECTION("with tuple-parameter")
  {
    zip_sort_tuple(element_compare<1>(f_is_less), i_vec, d_vec);

    CHECK(i_vec == std::vector<int>{1, 2, 1, 1, 2, 2});
    CHECK(d_vec == std::vector<double>{0.3, 1.1, 2.2, 3.3, 5.5, 7.7});
  }
}

TEST_CASE(
    "test zip-iterator with structured bindings",
    "[zip][zip_range][structured_binding]")
{
  std::vector<int> i_vec    = {1, 2, 1};
  std::vector<double> d_vec = {2.2, 5.5, 3.3};

  const std::vector<int> c_i_vec    = {10, 20, 10};
  const std::vector<double> c_d_vec = {22, 55, 33};

  {
    const auto zr = make_zip_range(i_vec, d_vec);
    const auto it = zr.begin();

    auto [v1, v2]        = (*it);
    auto& [w1, w2]       = (*it);
    const auto [x1, x2]  = (*it);
    const auto& [y1, y2] = (*it);

    CHECK(w1 == 1);
    CHECK(w2 == 2.2);
    CHECK(v1 == 1);
    CHECK(v2 == 2.2);
    CHECK(x1 == 1);
    CHECK(x2 == 2.2);
    CHECK(y1 == 1);
    CHECK(y2 == 2.2);
  }

  {
    const auto zr = make_zip_range(i_vec, c_d_vec);
    const auto it = zr.begin();

    auto [v1, v2]        = (*it);
    auto& [w1, w2]       = (*it);
    const auto [x1, x2]  = (*it);
    const auto& [y1, y2] = (*it);

    CHECK(w1 == 1);
    CHECK(w2 == 22);
    CHECK(v1 == 1);
    CHECK(v2 == 22);
    CHECK(x1 == 1);
    CHECK(x2 == 22);
    CHECK(y1 == 1);
    CHECK(y2 == 22);
  }

  {
    const auto zr = make_zip_range(c_i_vec, c_d_vec);
    const auto it = zr.begin();

    auto [v1, v2]        = (*it);
    auto& [w1, w2]       = (*it);
    const auto [x1, x2]  = (*it);
    const auto& [y1, y2] = (*it);

    CHECK(w1 == 10);
    CHECK(w2 == 22);
    CHECK(v1 == 10);
    CHECK(v2 == 22);
    CHECK(x1 == 10);
    CHECK(x2 == 22);
    CHECK(y1 == 10);
    CHECK(y2 == 22);
  }
}

TEST_CASE("test zip-unique", "[zip][zip_unique]")
{
  std::vector<int> i_vec    = {1, 1, 1, 2, 4, 4, 5};
  std::vector<double> d_vec = {2.2, 2.2, 3.3, 4.4, 7.7, 9.9, 8.8};

  SECTION("zip-unique-unpack")
  {
    const auto num_uniq = zip_unique_unpack(
        [](int& i1, double& d1, const int i2, const double d2) {
          if (i1 == i2) {
            d1 += d2;
            return true;
          }
          else {
            return false;
          }
        },
        i_vec,
        d_vec);

    i_vec.resize(num_uniq);
    d_vec.resize(num_uniq);

    CHECK(i_vec == std::vector<int>({1, 2, 4, 5}));
    CHECK(d_vec == std::vector<double>({7.7, 4.4, 17.6, 8.8}));
  }
  SECTION("zip-unique-tuple (1)")
  {
    const auto num_uniq = zip_unique_tuple(
        [](const auto& t1, const auto& t2) {
          if (std::get<0>(t1) == std::get<0>(t2)) {
            std::get<1>(t1) += std::get<1>(t2);
            return true;
          }
          else {
            return false;
          }
        },
        i_vec,
        d_vec);

    i_vec.resize(num_uniq);
    d_vec.resize(num_uniq);

    CHECK(i_vec == std::vector<int>({1, 2, 4, 5}));
    CHECK(d_vec == std::vector<double>({7.7, 4.4, 17.6, 8.8}));
  }
  SECTION("zip-unique-tuple (2)")
  {
    const auto num_uniq =
        zip_unique_tuple(element_compare<0>(f_is_equal), i_vec, d_vec);

    i_vec.resize(num_uniq);
    d_vec.resize(num_uniq);

    CHECK(i_vec == std::vector<int>({1, 2, 4, 5}));
    CHECK(d_vec == std::vector<double>({2.2, 4.4, 7.7, 8.8}));
  }
}
