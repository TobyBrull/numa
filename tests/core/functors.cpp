#include "numa/core/functors.hpp"

#include "numa/tmpl/integer_list.hpp"
#include "numa/tmpl/literals.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("fundamental functors", "[functors]")
{
  /*
   * Binary functor with literals should return bool-literal.
   */
  {
    constexpr auto result = f_is_less(1_i, 2_i);
    static_assert(std::Same<decltype(result), const c_true_t>);
    static_assert(result.value);
  }

  {
    static_assert(f_is_less(1_i, 2_i, 3_i).value);
  }

  /*
   * binary (and higher) predicates
   */
  static_assert(f_is_less(1, 2.0));
  static_assert(f_is_less_t{}(1, 2, 3));
  static_assert(!f_is_less(1, 3, 2.0));
  static_assert(!f_is_less(1, 4, 4));

  static_assert(f_is_less_equal(1, 4, 4));
  static_assert(!f_is_less_equal_t{}(1, 4.0, 3));

  static_assert(f_is_greater(2, 1));
  static_assert(f_is_greater(7, 4.0, 1));
  static_assert(!f_is_greater(6.0, 6, 1));
  static_assert(!f_is_greater_t{}(6, 7, 1));

  static_assert(f_is_greater_equal(4, 1, 1));
  static_assert(!f_is_greater_equal_t{}(5, 8.0, 1));

  static_assert(f_is_equal(1, 1));
  static_assert(!f_is_equal(7, 4.0));
  static_assert(f_is_equal(1, 1, 1.0, 1, 1));
  static_assert(!f_is_equal_t{}(7, 4));

  static_assert(f_is_not_equal(7, 4));
  static_assert(!f_is_not_equal_t{}(7, 7.0));

  /*
   * Stateful, unary predicates
   */
  static_assert(f_is_less_than_t{2}(1));
  static_assert(!f_is_less_than_t{2}(2));
  static_assert(!f_is_less_than_t{2}(3));

  static_assert(f_is_less_equal_to_t{2}(1));
  static_assert(f_is_less_equal_to_t{2}(2));
  static_assert(!f_is_less_equal_to_t{2}(3));

  static_assert(!f_is_greater_than_t{2}(1));
  static_assert(!f_is_greater_than_t{2}(2));
  static_assert(f_is_greater_than_t{2}(3));

  static_assert(!f_is_greater_equal_to_t{2}(1));
  static_assert(f_is_greater_equal_to_t{2}(2));
  static_assert(f_is_greater_equal_to_t{2}(3));

  static_assert(!f_is_equal_to_t{2}(1));
  static_assert(f_is_equal_to_t{2}(2));
  static_assert(!f_is_equal_to_t{2}(3));

  static_assert(f_is_not_equal_to_t{2}(1));
  static_assert(!f_is_not_equal_to_t{2}(2));
  static_assert(f_is_not_equal_to_t{2}(3));

  static_assert(f_id(2) == 2);
  static_assert(f_id_t{}(4.1) == 4.1);

  static_assert(f_plus(2) == 2);
  static_assert(f_plus(2, 3.5) == 5.5);
  static_assert(f_plus(1, 2.5, 8) == 11.5);

  static_assert(f_times(2) == 2);
  static_assert(f_times(2, 3.5) == 7);
  static_assert(f_times(3, 4.5, 2) == 27);

  static_assert(f_minus(5, 2) == 3);
  static_assert(f_divided(5, 2) == 2);
  static_assert(f_modulus(5, 4) == 1);

  static_assert(f_negate(5) == -5);

  static_assert(f_div(x, 2)(4) == 2);
  static_assert(f_div(2, x)(4) == 0);

  static_assert(f_pow(x, 2)(3) == 9);
  static_assert(f_pow(2, x)(3) == 8);

  static_assert(f_min(8) == 8);
  static_assert(f_min(8, 2.0, 4.0) == 2);

  static_assert(f_max(2.0) == 2);
  static_assert(f_max(2.0, 1, -1) == 2);

  static_assert(f_sin(2.0) == std::sin(2.0));
  static_assert(f_cos(2.0) == std::cos(2.0));

  static_assert(f_abs(-2.0) == std::abs(-2.0));

  CHECK(f_norm(std::complex<double>(1.0, 0.0)) == 1.0);
  CHECK(
      f_conj(std::complex<double>(1.0, 1.0)) ==
      std::complex<double>(1.0, -1.0));

  CHECK(f_conj(1.0) == 1.0);
  static_assert(c_type<decltype(f_conj(1.0))> == c_type<double>);
  CHECK(f_conj(1.0f) == 1.0f);
  static_assert(c_type<decltype(f_conj(1.0f))> == c_type<float>);

  static_assert(f_exp(-2.0) == std::exp(-2.0));
  static_assert(f_exp2(-2.0) == 0.25);
  static_assert(f_expm1(-2.0) == std::expm1(-2.0));

  static_assert(f_log(2.0) == std::log(2.0));
  static_assert(f_log2(2.0) == 1.0);
  static_assert(f_log1p(2.0) == std::log1p(2.0));

  static_assert(f_sqrt(4.0) == 2.0);

  static_assert(f_square(2.0) == 4.0);

  static_assert(f_const<int>() == 0);
  static_assert(f_const<int>(1, 'c', 3.1) == 0);
  static_assert(f_const_t{3.1}() == 3.1);
  static_assert(f_const_t{3.1}(1, 'a', 5.2) == 3.1);

  {
    int i = 0;
    int j = 0;
    f_assign_t{f_const_t{10}}(i, j);

    CHECK(i == 10);
    CHECK(j == 0);
  }

  {
    int i = 3;
    int j = 8;
    int k = 5;

    f_assign_t{f_plus}(i, j, k);

    CHECK(i == 13);
    CHECK(j == 8);
    CHECK(k == 5);
  }

  {
    int i = 3;
    int j = 8;
    int k = 5;

    f_assign_plus_t{f_times}(i, j, k);

    CHECK(i == 43);
    CHECK(j == 8);
    CHECK(k == 5);
  }

  {
    int i = 3;
    int j = 8;

    f_assign_minus_t{f_id}(i, j);

    CHECK(i == -5);
    CHECK(j == 8);
  }

  {
    int i = 3;
    int j = 8;

    f_assign_times_t{f_negate}(i, j);

    CHECK(i == -24);
    CHECK(j == 8);
  }

  {
    int i = 7;
    int j = 2;

    f_assign_divided_t{f_id}(i, j);

    CHECK(i == 3);
    CHECK(j == 2);
  }

  {
    int i = 7;
    int j = 2;

    f_assign_modulus_t{f_id}(i, j);

    CHECK(i == 1);
    CHECK(j == 2);
  }
}

TEST_CASE(
    "functor interaction with type_list and index_list",
    "[functors][integer_list][type_list][all_of]")
{
  static_assert(!all_of(index_list<3, 7, 9>, f_is_less_than_t{c_index<8>}));
  static_assert(all_of(index_list<3, 7, 9>, f_is_less_than_t{c_index<10>}));
  static_assert(
      any_of(type_list<char, int, std::string>, f_is_equal_to_t{c_type<int>})
          .value);
  static_assert(
      !all_of(type_list<char, int, std::string>, f_is_equal_to_t{c_type<int>})
           .value);
  static_assert(
      !none_of(type_list<char, int, std::string>, f_is_equal_to_t{c_type<int>})
           .value);
}
