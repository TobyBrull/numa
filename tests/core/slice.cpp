#include "numa/core/slice.hpp"
#include "numa/core/serialize_json.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("slice basic", "[slice][basic]")
{
  {
    const auto sa = make_slice_size_array(
        index(4, 2, 1), index(1, 2, 4), index(10, 20, 30));

    CHECK(to_json(std::span(sa)) == "[[4:1:10], [2:2:20], [1:4:30]]");
  }

  {
    const auto sa = make_slice_till_array(
        index(4, 2, 1), index(6, 6, 7), index(10, 20, 30));

    CHECK(to_json(std::span(sa)) == "[[4:2:10], [2:4:20], [1:6:30]]");
  }
}
