#include "numa/core/algorithms.hpp"

#include "numa/core/range.hpp"

#include "numa/boot/bits.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("test equal", "[algorithms][equal]")
{
  std::vector<int> vec_1 = {3, 1, 2};
  std::vector<int> vec_2 = {3, 1, 2};
  std::vector<int> vec_3 = {3, 1, 2};

  CHECK(numa::equal(vec_1, vec_2, vec_3));

  vec_2.push_back(4);

  CHECK(!numa::equal(vec_1, vec_2, vec_3));

  vec_3.push_back(4);

  CHECK(!numa::equal(vec_1, vec_2, vec_3));

  vec_1.push_back(4);

  CHECK(numa::equal(vec_1, vec_2, vec_3));

  vec_2[1] = 10;

  CHECK(!numa::equal(vec_1, vec_2, vec_3));

  vec_1[1] = 10;

  CHECK(!numa::equal(vec_1, vec_2, vec_3));

  vec_3[1] = 10;

  CHECK(numa::equal(vec_1, vec_2, vec_3));
}

TEST_CASE("test for_each", "[algorithms][for_each_tuple][for_each_combination]")
{
  const std::vector<int> vec_1  = {3, 1, 2};
  const std::vector<char> vec_2 = {'a', 'a', 'b'};

  /*
   * for_each_tuple
   */
  {
    int sum = 0;

    for_each_tuple(vec_1)([&sum](const int value) { sum += value; });

    CHECK(sum == 6);
  }

  {
    std::string str;

    for_each_tuple(vec_1, vec_2)(
        [&str](const int i, const char c) { str.push_back(c + i); });

    CHECK(str == "dbd");
  }

  /*
   * for_each_combination
   */
  {
    int sum = 0;

    for_each_combination(vec_1)([&sum](const int value) { sum += value; });

    CHECK(sum == 6);
  }

  {
    std::string str;

    for_each_combination(vec_1, vec_2)(
        [&str](const int i, const char c) { str.push_back(c + i); });

    CHECK(str == "ddebbcccd");
  }
}

TEST_CASE(
    "test for_each_combination",
    "[algorithms][for_each_tuple][for_each_combination]")
{
  const std::vector<uint32_t> vec_1 = {1, 2, 3, 4};
  const std::vector<double> vec_2   = {1.1, 2.2};
  const std::vector<int32_t> vec_3  = {-1, -2, -3};

  const std::vector<std::tuple<uint32_t, double, int32_t>> expected = {
      {1, 1.1, -1}, {1, 1.1, -2}, {1, 1.1, -3}, {1, 2.2, -1}, {1, 2.2, -2},
      {1, 2.2, -3}, {2, 1.1, -1}, {2, 1.1, -2}, {2, 1.1, -3}, {2, 2.2, -1},
      {2, 2.2, -2}, {2, 2.2, -3}, {3, 1.1, -1}, {3, 1.1, -2}, {3, 1.1, -3},
      {3, 2.2, -1}, {3, 2.2, -2}, {3, 2.2, -3}, {4, 1.1, -1}, {4, 1.1, -2},
      {4, 1.1, -3}, {4, 2.2, -1}, {4, 2.2, -2}, {4, 2.2, -3},
  };

  std::vector<std::tuple<uint32_t, double, int32_t>> result;

  for_each_combination(vec_1, vec_2, vec_3)(
      [&result](uint32_t const x, double const y, int32_t const z) {
        result.emplace_back(x, y, z);
      });

  CHECK(result == expected);
}

TEST_CASE("the finding wrappers", "[algorithms][find][find_if]")
{
  const std::vector<int> vec = {3, 1, 2};

  {
    const auto it = find(vec, 1);
    REQUIRE(it != vec.end());
    CHECK(*it == 1);

    CHECK(find(vec, 7) == vec.end());
  }

  {
    const auto is_even = [](const auto value) {
      return ((value % 2) == 0);
    };

    const auto it = find_if(vec, is_even);
    REQUIRE(it != vec.end());
    CHECK(*it == 2);

    const auto greater_10 = [](const auto value) {
      return (value > 10);
    };

    CHECK(find_if(vec, greater_10) == vec.end());
  }
}

TEST_CASE("the sorting wrappers", "[algorithms][sort]")
{
  std::vector<int> vec = {3, 1, 2};

  sort(vec);

  CHECK(vec == std::vector<int>({1, 2, 3}));

  sort(vec, std::greater<>());

  CHECK(vec == std::vector<int>({3, 2, 1}));
}

TEST_CASE("check if strictly sorted", "[algorithms][is_sorted_strictly]")
{
  std::vector<int> vec = {3, 1, 2};

  CHECK(!is_sorted_strictly(vec));

  vec[2] = 100;
  vec[0] = -3;

  CHECK(is_sorted_strictly(vec));

  vec[0] = 1;

  CHECK(!is_sorted_strictly(vec));

  vec[1] = 11;

  CHECK(is_sorted_strictly(vec));

  vec[2] = 11;

  CHECK(!is_sorted_strictly(vec));
}

TEST_CASE(
    "test combine_sorted_strictly", "[algorithms][combine_sorted_strictly]")
{
  {
    const std::vector<int> vec_1 = {2, 5, 6, 10, 14};
    const std::vector<int> vec_2 = {1, 2, 3, 4, 5, 10, 14, 20, 21};

    std::vector<int> result;

    combine_sorted_strictly(
        vec_1,
        vec_2,
        std::less<>(),
        [&result](const int value) { result.push_back(value); },
        [&result](const int lhs, const int rhs) {
          CHECK(lhs == rhs);
          result.push_back(lhs);
        });

    CHECK(result == std::vector<int>({1, 2, 3, 4, 5, 6, 10, 14, 20, 21}));
  }
}
