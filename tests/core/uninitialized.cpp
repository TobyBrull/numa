#include "numa/core/uninitialized.hpp"

#include "numa/boot/index.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("basic uninitialized syntax", "[uninitialized][init][syntax]")
{
  {
    const auto initer = init<int>(3);

    static_assert(std::is_same_v<decltype(initer.args_tuple), std::tuple<int>>);

    CHECK(initer.args_tuple == std::tuple<int>(3));
  }

  {
    const auto initer = init<std::string>(3, 'c');

    static_assert(
        std::is_same_v<decltype(initer.args_tuple), std::tuple<int, char>>);

    CHECK(initer.args_tuple == std::tuple<int, char>(3, 'c'));
  }

  {
    const int i       = 3;
    const auto initer = init<m_a_t>(i);

    static_assert(std::is_same_v<decltype(initer.args_tuple), std::tuple<int>>);

    CHECK(initer.args_tuple == std::tuple<int>(3));
  }

  {
    int i             = 3;
    const auto initer = init(m_a, i);

    static_assert(std::is_same_v<decltype(initer.args_tuple), std::tuple<int>>);

    CHECK(initer.args_tuple == std::tuple<int>(3));
  }

  {
    const auto initer = init<double>(uninitialized);

    static_assert(std::is_same_v<
                  decltype(initer.args_tuple),
                  std::tuple<uninitialized_t>>);
  }

  {
    const auto initer = init<double>(uninitialized_t{});

    static_assert(std::is_same_v<
                  decltype(initer.args_tuple),
                  std::tuple<uninitialized_t>>);
  }

  {
    const auto initer = init<m_a_t>();

    static_assert(std::is_same_v<decltype(initer.args_tuple), std::tuple<>>);
  }

  {
    const auto initer = init<void>(3, 5);

    static_assert(
        std::is_same_v<decltype(initer.args_tuple), std::tuple<int, int>>);

    CHECK(initer.args_tuple == std::tuple<int, int>(3, 5));
  }

  {
    const auto initer = init_all(1, 3);

    static_assert(
        std::is_same_v<decltype(initer.args_tuple), std::tuple<int, int>>);

    CHECK(initer.args_tuple == std::tuple<int, int>(1, 3));
  }
}

TEST_CASE("basic initializer-for", "[uninitialized][initializer_for]")
{
  static_assert(is_initializer_v<decltype(init_all(3.14))>);

  CHECK(initializer_for<m_g_t, double>(3.14) == std::tuple<double>(3.14));

  CHECK(
      initializer_for<m_g_t, double>(
          init<m_a_t>(3.14), init<int>("Hello", 2), init<double>(2.7)) ==
      std::tuple<double>(2.7));

  CHECK(
      initializer_for<m_g_t, double>(
          init<m_a_t>(3.14), init<int>("Hello", 2), init<float>(2.7)) ==
      std::tuple<>());

  CHECK(
      initializer_for<m_a_t, double>(
          init(m_a, 3.14), init<int>("Hello", 2), init<double>(2.7)) ==
      std::tuple<double>(3.14));

  CHECK(
      initializer_for<m_g_t, double>(
          init(m_a, 3.14), init<int>("Hello", 2), init<double>(2.7)) ==
      std::tuple<double>(2.7));

  CHECK(
      initializer_for<m_g_t, double>(
          init<m_a_t>(3.14),
          init<void>(std::string("Hello"), 2),
          init<double>(2.7)) == std::tuple<std::string, int>("Hello", 2));
}

TEST_CASE("test uninitialized detail", "[uninitialized][detail]")
{
  static_assert(!detail::has_uninitialized_constructor<double>);
  static_assert(detail::has_uninitialized_constructor<index<1>>);
  static_assert(detail::has_uninitialized_constructor<index<2>>);
  static_assert(detail::has_uninitialized_constructor<index<3>>);
  static_assert(!detail::has_uninitialized_constructor<std::string>);
}
