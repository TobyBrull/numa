#include "numa/core/accumulators.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("plus accumulator", "[accumulators]")
{
  auto acc = a_plus<int>();

  CHECK(acc.get() == 0.0);

  acc(2);
  CHECK(acc.get() == 2);
  acc(4);
  CHECK(acc.get() == 6);
  acc(-1);
  CHECK(acc.get() == 5);
}

TEST_CASE("times accumulator", "[accumulators]")
{
  auto acc = a_times<double>();

  CHECK(acc.get() == 1.0);

  acc(2);
  CHECK(acc.get() == 2.0);
  acc(4.0);
  CHECK(acc.get() == 8.0);
  acc(-0.1);
  CHECK(acc.get() == -0.8);
}

TEST_CASE("min accumulator", "[accumulators]")
{
  auto acc = a_min<double>();

  CHECK(acc.get() == std::numeric_limits<double>::infinity());

  acc(2.0);
  CHECK(acc.get() == 2.0);
  acc(4.0);
  CHECK(acc.get() == 2.0);
  acc(-0.1);
  CHECK(acc.get() == -0.1);
}

TEST_CASE("max accumulator", "[accumulators]")
{
  auto acc = a_max<double>();

  CHECK(acc.get() == -std::numeric_limits<double>::infinity());

  acc(2.0);
  CHECK(acc.get() == 2.0);
  acc(4.0);
  CHECK(acc.get() == 4.0);
  acc(-0.1);
  CHECK(acc.get() == 4.0);
}

TEST_CASE("abs-max accumulator", "[accumulators]")
{
  auto acc = a_abs_max<double>();

  CHECK(acc.get() == 0.0);

  acc(2.0);
  CHECK(acc.get() == 2.0);
  acc(-4.0);
  CHECK(acc.get() == 4.0);
  acc(-1.1);
  CHECK(acc.get() == 4.0);

  acc(10.0, 5.0);
  CHECK(acc.get() == 5.0);
}

TEST_CASE("frobenius accumulator", "[accumulators]")
{
  auto acc = a_frobenius<double>();

  CHECK(acc.get() == 0.0);

  acc(3.0);
  CHECK(acc.get() == 3.0);
  acc(-2.0, -6.0);
  CHECK(acc.get() == 5.0);

  acc(3.0);
  acc(-1.0);
  acc(1.0);

  CHECK(acc.get() == 6.0);
}
