#include "numa/core/serialize_json.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("peek_till_non_whitespace", "[peek_till_non_whitespace]")
{
  {
    std::istringstream iss("    Hello, World");
    peek_till_non_whitespace(iss);
    CHECK(iss.get() == 'H');
  }

  {
    std::istringstream iss("    ");
    peek_till_non_whitespace(iss);
    CHECK(iss.get() == std::istringstream::traits_type::eof());
    CHECK(!iss);
  }
}

TEST_CASE("read_while", "[read_while]")
{
  {
    std::istringstream iss("Hello, World");
    auto const result = read_while(iss, [](char const c) {
      return (c == 'H') || (c == 'e') || (c == 'l');
    });
    CHECK(result == "Hell");
  }

  {
    std::istringstream iss("    Hello, World");
    peek_till_non_whitespace(iss);
    auto const result = read_while(iss, [](char const c) {
      return std::isalnum(c) || (c == ' ') || (c == ',');
    });
    CHECK(result == "Hello, World");
  }
}

TEST_CASE(
    "serialize-json for strings and chars", "[serialize_json][string][char]")
{
  using namespace std::string_literals;

  CHECK(to_json(std::string("Hello")) == "\"Hello\"");
  CHECK(to_json('c') == "\"c\"");
  CHECK(to_json('\0') == "\"\\0\"");
  CHECK(
      to_json("H\0e\nl\blo W\r\torl\"d"s) ==
      "\"H\\0e\\nl\\blo W\\r\\torl\\\"d\"");

  CHECK(make_from_json<std::string>("\"Hello\"") == "Hello");
  CHECK(make_from_json<char>("\"c\"") == 'c');
  CHECK(make_from_json<char>("\"\\0\"") == '\0');
  CHECK(
      make_from_json<std::string>("\"H\\0e\\nl\\blo W\\r\\torl\\\"d\"") ==
      "H\0e\nl\blo W\r\torl\"d"s);
}

TEST_CASE("serialize-json for tuples", "[serialize_json][tuple]")
{
  std::tuple<int, std::string, char> tt{42, "WAT", 'g'};

  CHECK(to_json(tt) == "[42, \"WAT\", \"g\"]");

  CHECK(
      make_from_json<decltype(tt)>("[11, \"World\", \"x\"]") ==
      std::tuple<int, std::string, char>{11, "World", 'x'});
}

TEST_CASE("serialize-json for variant", "[serialize_json][variant]")
{
  using T = std::variant<int, std::string, char>;

  CHECK(to_json(T(123)) == "{\n  \"index\": 0,\n  \"value\": 123\n}");
  CHECK(to_json(T("Hello")) == "{\n  \"index\": 1,\n  \"value\": \"Hello\"\n}");
  CHECK(to_json(T('c')) == "{\n  \"index\": 2,\n  \"value\": \"c\"\n}");

  CHECK(make_from_json<T>("{\n  \"index\": 0,\n  \"value\": 123\n}") == T(123));
  CHECK(
      make_from_json<T>("{\n  \"index\": 1,\n  \"value\": \"Hello\"\n}") ==
      T("Hello"));
  CHECK(
      make_from_json<T>("{\n  \"index\": 2,\n  \"value\": \"c\"\n}") == T('c'));
}

TEST_CASE(
    "serialize-json for std::chrono::duration",
    "[serialize_json][duration][chrono]")
{
  CHECK(to_json(std::chrono::duration<int>(1234)) == "1234");
}

TEST_CASE("serialize-json index", "[serialize_json][index]")
{
  const index ind(3, 4, 5);

  std::stringstream ss;

  to_json(ss, ind);

  CHECK(ss.str() == "[3, 4, 5]");

  index<3> read_ind = zero;

  from_json(ss, read_ind);

  CHECK(read_ind == ind);
}

TEST_CASE("serialize-json vector", "[serialize_json][vector]")
{
  std::vector<index_t> const base_vec = {8, 4, 2, 1};
  std::vector<index_t> vec            = base_vec;

  std::stringstream ss;
  to_json(ss, vec);

  CHECK(ss.str() == "[8, 4, 2, 1]");

  vec.clear();
  from_json(ss, vec);

  CHECK(vec == base_vec);
}

TEST_CASE("serialize-from-json index", "[serialize_json][from_json][index]")
{
  CHECK(make_from_json<index<3>>("   [5,2     , 1    ]   ") == index(5, 2, 1));
  CHECK(make_from_json<index<3>>(".[5,2,1]") == index(5, 2, 1));
}

TEST_CASE(
    "serialize-from-json array-span", "[serialize_json][from_json][std::span]")
{
  std::array<int, 4> arr = {2, 3, 5, 7};

  CHECK(to_json(std::span(arr)) == "[2, 3, 5, 7]");

  from_json("[1, 2, 4, 8]", std::span(arr));

  CHECK(arr.at(0) == 1);
  CHECK(arr.at(1) == 2);
  CHECK(arr.at(2) == 4);
  CHECK(arr.at(3) == 8);
}

TEST_CASE("serialize-from-json slice", "[serialize_json][from_json][slice]")
{
  slice sl(5, 2, 1);

  CHECK(to_json(sl) == "[5:2:1]");

  from_json("[8:5:4]", sl);

  CHECK(sl.base == 8);
  CHECK(sl.size == 5);
  CHECK(sl.stride == 4);
}

TEST_CASE("generic serialize-json functions", "[serialize_json][generic]")
{
  const index ind(1, 3, 2);

  const auto str = to_json(ind);

  CHECK(str == "[1, 3, 2]");

  {
    index<3> jnd;

    from_json(str, jnd);

    CHECK(jnd == ind);
  }

  CHECK(make_from_json<index<3>>(str) == ind);

  {
    std::istringstream iss(str);

    CHECK(make_from_json<index<3>>(iss) == ind);
  }

  {
    std::ostringstream oss;

    oss << ind;

    CHECK(oss.str() == str);
  }

  {
    std::istringstream iss(str);

    index<3> jnd;

    iss >> jnd;

    CHECK(jnd == ind);
  }
}

TEST_CASE("index to string conversion", "[index][serialize_json]")
{
  CHECK(to_json(index<1>(3)) == "[3]");
  CHECK(to_json(index<2>(3, 4)) == "[3, 4]");
  CHECK(to_json(index<3>(iota)) == "[0, 1, 2]");
}

TEST_CASE("index io", "[index][io][serialize_json]")
{
  std::ostringstream oss;
  oss << index(4, 1, 9);

  CHECK(oss.str() == "[4, 1, 9]");
}
