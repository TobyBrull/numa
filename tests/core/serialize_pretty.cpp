#include "numa/core/serialize_pretty.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE(
    "to_pretty for std::chrono::duration", "[to_pretty][chrono][duration]")
{
  using Catch::Matchers::StartsWith;

  CHECK_THAT(
      to_pretty(std::chrono::duration<double>(3.14)), StartsWith("3.14"));

  CHECK(to_pretty(std::chrono::duration<int>(1234)) == "1234");
  CHECK(
      make_from_pretty<std::chrono::duration<int>>("234") ==
      std::chrono::duration<int>(234));

  {
    std::ostringstream oss;
    oss << std::chrono::duration<int>(678);
    CHECK(oss.str() == "678");
  }

  {
    std::istringstream iss("456");
    CHECK(make_from_pretty<index_t>(iss) == 456);
  }

  {
    index_t i;
    from_pretty("123", i);
    CHECK(i == 123);
  }
}

TEST_CASE("to_pretty for identifier", "[to_pretty][identifier]")
{
  CHECK(to_pretty(identifier("_gr8")) == "_gr8");
  CHECK(make_from_pretty<identifier>("_gr8") == identifier("_gr8"));

  using namespace std::literals;

  CHECK(
      make_from_pretty<std::vector<identifier>>("[abc,  def   ,xyz]") ==
      std::vector<identifier>{"abc"s, "def"s, "xyz"s});
}
