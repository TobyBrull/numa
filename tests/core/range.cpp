#include "numa/core/range.hpp"

#include "numa/core/algorithms.hpp"
#include "numa/core/functors.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE(
    "test element_finder and element_comparer",
    "[range][element_finder][element_comparer]")
{
  std::vector<std::pair<int, std::string>> vec = {
      {10, "!"}, {2, "Hello"}, {7, "World"}, {5, ", "}};

  const auto it = find_if(vec, element_find<1>("World"));
  REQUIRE(it != vec.end());
  CHECK(it->first == 7);
  CHECK(it->second == "World");

  sort(vec, element_compare<0>(f_is_less));

  CHECK(
      vec ==
      std::vector<std::pair<int, std::string>>{
          {2, "Hello"}, {5, ", "}, {7, "World"}, {10, "!"}});
}
