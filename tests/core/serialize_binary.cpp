#include "numa/core/serialize_binary.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE(
    "to_binary for std::chrono::duration", "[to_pretty][chrono][duration]")
{
  std::stringstream oss;

  uint32_t const val = 0x41424344;
  to_binary(oss, val);

  /*
   * Fails on systems with different endianess?
   */
  CHECK(oss.str() == "DCBA");

  auto const result = make_from_binary<uint32_t>(oss.str());

  CHECK(val == result);
}
