#include "numa/core/concepts.hpp"

#include "numa/core/memory.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("allocator concept (core)", "[allocator][concept]")
{
  static_assert(Allocator<default_allocator>);
}
