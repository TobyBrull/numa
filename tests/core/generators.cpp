#include "numa/core/generators.hpp"

#include "catch2/catch.hpp"

using namespace numa;

TEST_CASE("fundamental generators", "[generators]")
{
  {
    auto g = g_iota(5);

    static_assert(c_type<decltype(g())> == c_type<int>);

    CHECK(g() == 5);
    CHECK(g(123) == 6);
    CHECK(g('b') == 7);
  }

  {
    auto g = g_iota<double>();

    static_assert(c_type<decltype(g())> == c_type<double>);

    CHECK(g() == 0.0);
    CHECK(g(123) == 1.0);
    CHECK(g('b') == 2.0);
  }
}

TEST_CASE("random generators", "[random][generators]")
{
  std::default_random_engine eng;

  {
    auto g = g_random_normal(eng);
    static_assert(c_type<decltype(g())> == c_type<double>);
    CHECK(std::abs(g()) <= 1000.0);
  }

  {
    auto g = g_random_normal<float>(eng);
    static_assert(c_type<decltype(g())> == c_type<float>);
    CHECK(std::abs(g()) <= 1000.0f);
  }

  {
    auto g = g_random_uniform_int(eng);
    static_assert(c_type<decltype(g())> == c_type<index_t>);
    CHECK(g() >= 0);
  }

  {
    auto g = g_random_uniform_int(eng, -6, 6);
    static_assert(c_type<decltype(g())> == c_type<int>);
    CHECK(std::abs(g()) <= 6);
  }

  {
    auto g = g_random_uniform_real(eng);
    static_assert(c_type<decltype(g())> == c_type<double>);
    CHECK(std::abs(g()) <= 1000.0);
  }

  {
    auto g = g_random_uniform_real(eng, 4.0f, 6.0f);
    static_assert(c_type<decltype(g())> == c_type<float>);
    CHECK(4.0f <= g());
    CHECK(g() <= 6.0f);
  }

  {
    auto g = g_random_test(eng);
    static_assert(c_type<decltype(g())> == c_type<int>);
    CHECK(-1000 <= g());
    CHECK(g() <= 1000);
  }
}
