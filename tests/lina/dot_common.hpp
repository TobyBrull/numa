#ifndef INCLUDE_GUARD_TESTS_LINA_DOT_COMMON_H_
#define INCLUDE_GUARD_TESTS_LINA_DOT_COMMON_H_

#include "numa/lina/dot.hpp"
#include "numa/lina/dot_kernels.hpp"

namespace numa::test {
  template<typename T>
  auto dot_reference(matrix<T> const& A, matrix<T> const& B)
  {
    auto const n = A.size()[0], m = A.size()[1], p = B.size()[1];
    auto retval = make_cell<matrix<T>>(n, p)(f_const_t{T{0}});

    for (index_t i = 0; i < n; ++i) {
      for (index_t k = 0; k < p; ++k) {
        for (index_t j = 0; j < m; ++j) {
          retval(i, k) += A(i, j) * B(j, k);
        }
      }
    }

    return retval;
  }
}

#endif
