#include "numa/lina/cpu_target.hpp"
#include "numa/lina/cpu_target_registry.hpp"
#include "numa/lina/cpu_target_scope.hpp"

#include "catch2/catch.hpp"

#include <sstream>

using namespace numa;

TEST_CASE("cpu_target", "[cpu_target]")
{
  auto const ct = cpu_target_default();
  CHECK(ct != cpu_target::generic);

  {
    std::ostringstream oss;
    to_pretty(oss, cpu_target::avx2);
    CHECK(oss.str() == "avx2");
  }

  {
    std::istringstream iss("avx512");
    cpu_target ct = cpu_target::generic;
    from_pretty(iss, ct);
    CHECK(ct == cpu_target::avx512);
  }

  {
    std::istringstream iss("   avx512");
    cpu_target ct = cpu_target::generic;
    from_pretty(iss, ct);
    CHECK(ct == cpu_target::avx512);
  }

  CHECK(cpu_target_is_compatible(cpu_target::generic, cpu_target::generic));
  CHECK(cpu_target_is_compatible(cpu_target::generic, cpu_target::sse4_2));
  CHECK(cpu_target_is_compatible(cpu_target::generic, cpu_target::avx));
  CHECK(cpu_target_is_compatible(cpu_target::generic, cpu_target::avx2));
  CHECK(cpu_target_is_compatible(cpu_target::generic, cpu_target::avx512));

  CHECK(!cpu_target_is_compatible(cpu_target::avx, cpu_target::generic));
  CHECK(!cpu_target_is_compatible(cpu_target::avx, cpu_target::sse4_2));
  CHECK(cpu_target_is_compatible(cpu_target::avx, cpu_target::avx));
  CHECK(cpu_target_is_compatible(cpu_target::avx, cpu_target::avx2));
  CHECK(cpu_target_is_compatible(cpu_target::avx, cpu_target::avx512));
}

namespace {
  thread_local std::string (*test_func_cpu_target)(int);

  std::string test_func_generic(int const n)
  {
    std::string retval;
    for (int i = 0; i < n; ++i) {
      retval += "generic, ";
    }
    return retval;
  }

  std::string test_func_sse4_2(int const n)
  {
    std::string retval;
    for (int i = 0; i < n; ++i) {
      retval += "sse4.2, ";
    }
    return retval;
  }

  std::string test_func_avx(int const n)
  {
    std::string retval;
    for (int i = 0; i < n; ++i) {
      retval += "avx, ";
    }
    return retval;
  }

  std::string test_func_avx512(int const n)
  {
    std::string retval;
    for (int i = 0; i < n; ++i) {
      retval += "avx512, ";
    }
    return retval;
  }

  bool ATTRIBUTE_USED registered_ = [] {
    return cpu_target_registry::register_function(
        &test_func_cpu_target,
        {
            {cpu_target::generic, &test_func_generic},
            {cpu_target::sse4_2, &test_func_sse4_2},
            {cpu_target::avx, &test_func_avx},
            {cpu_target::avx512, &test_func_avx512},
        });
  }();
}

TEST_CASE(
    "cpu_target_registry, cpu_target_scope",
    "[cpu_target_registry][cpu_target_scope]")
{
  std::string const native_string = (*test_func_cpu_target)(2);
  CHECK(!native_string.empty());

  {
    cpu_target_scope s1(cpu_target::generic);

    CHECK((*test_func_cpu_target)(1) == "generic, ");

    {
      cpu_target_scope s2(cpu_target::avx512);

      CHECK((*test_func_cpu_target)(3) == "avx512, avx512, avx512, ");

      {
        cpu_target_scope s3(cpu_target::avx);

        CHECK((*test_func_cpu_target)(2) == "avx, avx, ");

        {
          cpu_target_scope s4(cpu_target::avx2);

          CHECK((*test_func_cpu_target)(4) == "avx, avx, avx, avx, ");

          {
            cpu_target_scope s5(cpu_target::generic);

            CHECK((*test_func_cpu_target)(1) == "generic, ");
          }

          CHECK((*test_func_cpu_target)(1) == "avx, ");
        }

        CHECK((*test_func_cpu_target)(3) == "avx, avx, avx, ");
      }

      CHECK((*test_func_cpu_target)(2) == "avx512, avx512, ");
    }

    CHECK((*test_func_cpu_target)(4) == "generic, generic, generic, generic, ");
  }

  CHECK((*test_func_cpu_target)(2) == native_string);
}
