#include "numa/lina/matrix_aligned_span.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/tensor.hpp"

#include "numa/misc/data.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

TEST_CASE("basic matrix_aligned_span", "[matrix_aligned_span]")
{
  auto const data = std_iota_vector<int>(40, 0);

  {
    matrix_aligned_span<int const, major::left, 4> const mas{
        data.data(), {6, 3}, 5};

    CHECK(!mas.is_bounded(-1, 0));
    CHECK(mas.at(0, 0) == 0);
    CHECK(mas.at(1, 0) == 1);
    CHECK(mas.at(2, 0) == 2);
    CHECK(mas.at(3, 0) == 3);
    CHECK(mas.at(4, 0) == 20);
    CHECK(mas.at(5, 0) == 21);
    CHECK(!mas.is_bounded(6, 0));

    CHECK(!mas.is_bounded(-1, 1));
    CHECK(mas.at(0, 1) == 4);
    CHECK(mas.at(1, 1) == 5);
    CHECK(mas.at(2, 1) == 6);
    CHECK(mas.at(3, 1) == 7);
    CHECK(mas.at(4, 1) == 24);
    CHECK(mas.at(5, 1) == 25);
    CHECK(!mas.is_bounded(6, 1));

    CHECK(!mas.is_bounded(-1, 2));
    CHECK(mas.at(0, 2) == 8);
    CHECK(mas.at(1, 2) == 9);
    CHECK(mas.at(2, 2) == 10);
    CHECK(mas.at(3, 2) == 11);
    CHECK(mas.at(4, 2) == 28);
    CHECK(mas.at(5, 2) == 29);
    CHECK(!mas.is_bounded(6, 2));

    {
      matrix<int> copy;
      resize(copy.vary_axes(), mas.size());

      copy_into(copy.span(), mas);

      CHECK(
          copy ==
          make_from_json<matrix<int>>("{ \"size\": [6, 3],"
                                      "  \"values\":"
                                      "[[0, 1, 2, 3, 20, 21],"
                                      " [4, 5, 6, 7, 24, 25],"
                                      " [8, 9, 10, 11, 28, 29]"
                                      "] }"));
    }
  }

  {
    matrix_aligned_span<int const, major::right, 4> const mas{
        data.data(), {3, 6}, 5};

    CHECK(!mas.is_bounded(0, -1));
    CHECK(mas.at(0, 0) == 0);
    CHECK(mas.at(0, 1) == 1);
    CHECK(mas.at(0, 2) == 2);
    CHECK(mas.at(0, 3) == 3);
    CHECK(mas.at(0, 4) == 20);
    CHECK(mas.at(0, 5) == 21);
    CHECK(!mas.is_bounded(0, 6));

    CHECK(!mas.is_bounded(1, -1));
    CHECK(mas.at(1, 0) == 4);
    CHECK(mas.at(1, 1) == 5);
    CHECK(mas.at(1, 2) == 6);
    CHECK(mas.at(1, 3) == 7);
    CHECK(mas.at(1, 4) == 24);
    CHECK(mas.at(1, 5) == 25);
    CHECK(!mas.is_bounded(1, 6));

    CHECK(!mas.is_bounded(2, -1));
    CHECK(mas.at(2, 0) == 8);
    CHECK(mas.at(2, 1) == 9);
    CHECK(mas.at(2, 2) == 10);
    CHECK(mas.at(2, 3) == 11);
    CHECK(mas.at(2, 4) == 28);
    CHECK(mas.at(2, 5) == 29);
    CHECK(!mas.is_bounded(2, 6));

    {
      matrix<int> copy;
      resize(copy.vary_axes(), mas.size());

      copy_into(copy.span(), mas);

      CHECK(
          copy ==
          make_from_json<matrix<int>>("{ \"size\": [3, 6],"
                                      "  \"values\":"
                                      "[[0, 4, 8],"
                                      " [1, 5, 9],"
                                      " [2, 6, 10],"
                                      " [3, 7, 11],"
                                      " [20, 24, 28],"
                                      " [21, 25, 29]"
                                      "] }"));
    }
  }
}

TEST_CASE("matrix_aligned_span for_each", "[matrix_aligned_span][for_each]")
{
  auto data = std_iota_vector<int>(52, 0);

  SECTION("major::left")
  {
    matrix_aligned_span<int, major::left, 4> const mas{data.data(), {6, 3}, 5};

    mas *= 10;

    CHECK(data == std::vector<int>({0,  10,  20,  30,  40, 50, 60,  70,  80,
                                    90, 100, 110, 12,  13, 14, 15,  16,  17,
                                    18, 19,  200, 210, 22, 23, 240, 250, 26,
                                    27, 280, 290, 30,  31, 32, 33,  34,  35,
                                    36, 37,  38,  39,  40, 41, 42,  43,  44,
                                    45, 46,  47,  48,  49, 50, 51}));

    const auto check_iter =
        [&mas](index<2> const& ind, auto const it, int const expected) {
          CHECK(*it == expected);
          CHECK(it.pos() == ind);

          auto const ref_it = mas.iterator(ind);

          CHECK(ref_it == it);

          CHECK(*ref_it == expected);
          CHECK(ref_it.pos() == ind);
        };

    auto it = mas.begin();
    check_iter(index(0, 0), it, 0);

    ++it;
    check_iter(index(1, 0), it, 10);
    ++it;
    check_iter(index(2, 0), it, 20);
    ++it;
    check_iter(index(3, 0), it, 30);
    ++it;
    check_iter(index(0, 1), it, 40);
    ++it;
    check_iter(index(1, 1), it, 50);
    ++it;
    check_iter(index(2, 1), it, 60);
    ++it;
    check_iter(index(3, 1), it, 70);
    ++it;
    check_iter(index(0, 2), it, 80);
    ++it;
    check_iter(index(1, 2), it, 90);
    ++it;
    check_iter(index(2, 2), it, 100);
    ++it;
    check_iter(index(3, 2), it, 110);
    ++it;
    check_iter(index(4, 0), it, 200);
    ++it;
    check_iter(index(5, 0), it, 210);
    ++it;
    check_iter(index(4, 1), it, 240);
    ++it;
    check_iter(index(5, 1), it, 250);
    ++it;
    check_iter(index(4, 2), it, 280);
    ++it;
    check_iter(index(5, 2), it, 290);

    ++it;

    CHECK(it.ptr() == mas.end().ptr());
    CHECK(it.pos() == mas.end().pos());
    CHECK(it == mas.end());
  }
  SECTION("major::right")
  {
    matrix_aligned_span<int, major::right, 4> const mas{data.data(), {3, 6}, 5};

    mas *= 10;

    CHECK(data == std::vector<int>({0,  10,  20,  30,  40, 50, 60,  70,  80,
                                    90, 100, 110, 12,  13, 14, 15,  16,  17,
                                    18, 19,  200, 210, 22, 23, 240, 250, 26,
                                    27, 280, 290, 30,  31, 32, 33,  34,  35,
                                    36, 37,  38,  39,  40, 41, 42,  43,  44,
                                    45, 46,  47,  48,  49, 50, 51}));

    const auto check_iter =
        [&mas](index<2> const& ind, auto const it, int const expected) {
          CHECK(*it == expected);
          CHECK(it.pos() == ind);

          auto const ref_it = mas.iterator(ind);

          CHECK(ref_it == it);

          CHECK(*ref_it == expected);
          CHECK(ref_it.pos() == ind);
        };

    auto it = mas.begin();
    check_iter(index(0, 0), it, 0);

    ++it;
    check_iter(index(0, 1), it, 10);
    ++it;
    check_iter(index(0, 2), it, 20);
    ++it;
    check_iter(index(0, 3), it, 30);
    ++it;
    check_iter(index(1, 0), it, 40);
    ++it;
    check_iter(index(1, 1), it, 50);
    ++it;
    check_iter(index(1, 2), it, 60);
    ++it;
    check_iter(index(1, 3), it, 70);
    ++it;
    check_iter(index(2, 0), it, 80);
    ++it;
    check_iter(index(2, 1), it, 90);
    ++it;
    check_iter(index(2, 2), it, 100);
    ++it;
    check_iter(index(2, 3), it, 110);
    ++it;
    check_iter(index(0, 4), it, 200);
    ++it;
    check_iter(index(0, 5), it, 210);
    ++it;
    check_iter(index(1, 4), it, 240);
    ++it;
    check_iter(index(1, 5), it, 250);
    ++it;
    check_iter(index(2, 4), it, 280);
    ++it;
    check_iter(index(2, 5), it, 290);

    ++it;

    CHECK(it.ptr() == mas.end().ptr());
    CHECK(it.pos() == mas.end().pos());
    CHECK(it == mas.end());
  }
  SECTION("empty")
  {
    {
      matrix_aligned_span<int, major::left, 4> const mas{
          data.data(), {0, 0}, 5};

      int i = 0;

      const auto f = [&i](auto const) {
        ++i;
      };

      for_each(mas)(f);

      CHECK(i == 0);

      CHECK(mas.begin() == mas.end());
    }

    {
      matrix_aligned_span<int, major::right, 4> const mas{
          data.data(), {0, 0}, 5};

      int i = 0;

      const auto f = [&i](auto const) {
        ++i;
      };

      for_each(mas)(f);

      CHECK(i == 0);

      CHECK(mas.begin() == mas.end());
    }
  }
}

TEST_CASE("matrix_aligned_span for_each bug", "[matrix_aligned_span][for_each]")
{
  auto data = std_iota_vector<int>(24, 0);

  SECTION("block fit")
  {
    matrix_aligned_span<int, major::left, 4> const mas{data.data(), {8, 2}, 3};

    mas *= 10;

    CHECK(data == std::vector<int>({0,   10,  20,  30,  40,  50,  60,  70,
                                    8,   9,   10,  11,  120, 130, 140, 150,
                                    160, 170, 180, 190, 20,  21,  22,  23}));
  }
}
