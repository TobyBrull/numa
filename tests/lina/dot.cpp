#include "dot_common.hpp"

#include "numa/cell/make_cell.hpp"
#include "numa/cell/reform.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/tensor.hpp"

#include "numa/core/generators.hpp"

#include "catch2/catch.hpp"

#include <random>

using namespace numa;

TEST_CASE("basic dense matrix-product", "[dot][t_span]")
{
  {
    auto const A = make_from_json<matrix<double>>("{ \"size\": [4, 2],"
                                                  "  \"values\":"
                                                  "[[1, 2, 3, 4],"
                                                  " [4, 3, 2, 1]"
                                                  "] }");

    auto const B = make_from_json<matrix<double>>("{ \"size\": [2, 3],"
                                                  "  \"values\":"
                                                  "[[0, 2],"
                                                  " [1, 1],"
                                                  " [1, 3]"
                                                  "] }");

    matrix<double> C;
    resize(C.vary_axes(), {4, 3});

    dot(C.span(), A.span(), B.span());

    CHECK(
        C ==
        make_from_json<matrix<double>>("{ \"size\": [4, 3],"
                                       "  \"values\":"
                                       "[[8, 6, 4, 2],"
                                       " [5, 5, 5, 5],"
                                       " [13, 11, 9, 7]"
                                       "] }"));
  }

  {
    std::default_random_engine eng;

    auto const A = make_cell<matrix<float>>(20, 20)(g_random_test(eng));
    auto const B = make_cell<matrix<float>>(20, 20)(g_random_test(eng));
    auto C       = make_cell<matrix<float>>(20, 20)();

    static_assert(numa::contains(dot_kernel_cpu_target_types, c_type<float>));

    dot(C.span(), A.span(), B.span());

    auto const ref = numa::test::dot_reference(A, B);
    CHECK(C == ref);
  }
}

TEST_CASE("dense matrix-product for ints", "[dot][t_span][int]")
{
  {
    auto const A = make_from_json<matrix<int>>("{ \"size\": [4, 2],"
                                               "  \"values\":"
                                               "[[1, 2, 3, 4],"
                                               " [4, 3, 2, 1]"
                                               "] }");

    auto const B = make_from_json<matrix<int>>("{ \"size\": [2, 3],"
                                               "  \"values\":"
                                               "[[0, 2],"
                                               " [1, 1],"
                                               " [1, 3]"
                                               "] }");

    matrix<int> C;
    resize(C.vary_axes(), {4, 3});

    dot(C.span(), A.span(), B.span());

    CHECK(
        C ==
        make_from_json<matrix<int>>("{ \"size\": [4, 3],"
                                    "  \"values\":"
                                    "[[8, 6, 4, 2],"
                                    " [5, 5, 5, 5],"
                                    " [13, 11, 9, 7]"
                                    "] }"));
  }
}
