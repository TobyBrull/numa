#include "dot_common.hpp"

#include "numa/lina/cpu_target_scope.hpp"

#include "numa/cell/serialize_json.hpp"

#include "numa/core/algorithms.hpp"
#include "numa/core/generators.hpp"

#include "numa/ctxt/phase.hpp"
#include "numa/ctxt/phase_monitors.hpp"
#include "numa/ctxt/thalanx.hpp"

#include "catch2/catch.hpp"

#include <iostream>
#include <random>

using namespace numa;

TEST_CASE("dot_kernels check unusual result-span", "[dot_kernels]")
{
  // ...
}

namespace {
  template<typename T>
  auto test_dot_kernels_run(matrix<T> const& A, matrix<T> const& B)
  {
    auto const n = A.size()[0];
    auto const p = B.size()[1];
    auto retval  = make_cell<matrix<T>>(n, p)(f_const_t{T{0}});

    (*dot_kernel_cpu_target<T>::run)(retval.span(), A.span(), B.span());

    return retval;
  }

  template<typename T>
  auto test_dot_kernels_run_raw(matrix<T> const& A, matrix<T> const& B)
  {
    auto const n = A.size()[0];
    auto const m = A.size()[1];
    auto const p = B.size()[1];
    auto retval  = make_cell<matrix<T>>(n, p)(f_const_t{T{0}});

    auto ma_A = (*dot_kernel_cpu_target<T>::allocate_left)({n, m});
    auto ma_B = (*dot_kernel_cpu_target<T>::allocate_right)({m, p});

    (*dot_kernel_cpu_target<T>::copy_left)(span(ma_A), A.span());
    (*dot_kernel_cpu_target<T>::copy_right)(span(ma_B), B.span());

    (*dot_kernel_cpu_target<T>::run_raw)(retval.span(), span(ma_A), span(ma_B));

    return retval;
  }

  template<typename T>
  void test_case_dot_kernels()
  {
    phase_status(c_section, "type = ", typeid(T).name());

    std::vector<index_t> const ns = {1, 5, 13};
    std::vector<index_t> const ms = ns;
    std::vector<index_t> const ps = ns;

    std::vector<cpu_target> const cts = {
        cpu_target::generic,
        cpu_target::sse4_2,
        cpu_target::avx,
        cpu_target::avx2};

    std::vector<index_t> const nts = {1, 4};

    std::default_random_engine eng;

    for (index_t const nt: nts) {
      phase_status(c_section, "num_threads = ", nt);
      thalanx th{nt};

      for_each_combination(ns, ms, ps)(
          [&](index_t const n, index_t const m, index_t const p) {
            phase_status(c_section, "n = ", n, "; m = ", m, "; p = ", p);

            auto const A = make_cell<matrix<T>>(n, m)(g_random_test(eng));
            auto const B = make_cell<matrix<T>>(m, p)(g_random_test(eng));

            auto const expected = test::dot_reference(A, B);

            for (cpu_target const ct: cts) {
              phase_status(c_paragraph, "cpu_target = ", ct);
              cpu_target_scope scope_1{ct};

              {
                phase_status(c_note, "run");
                auto const result = test_dot_kernels_run<T>(A, B);
                CHECK(result == expected);
              }

              {
                phase_status(c_note, "run_raw");
                auto const result = test_dot_kernels_run_raw<T>(A, B);
                CHECK(result == expected);
              }
            }
          });
    }
  }
}

TEST_CASE("dot_kernels combination test", "[dot_kernels][combinations]")
{
  // phase_monitor_debug dbp {std::cout};

  phase_status(c_chapter, "dot_kernels");

  test_case_dot_kernels<double>();
  test_case_dot_kernels<float>();
}
