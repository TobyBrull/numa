#include "numa/lina/matrix_aligned.hpp"

#include "numa/cell/algorithms.hpp"
#include "numa/cell/serialize_json.hpp"
#include "numa/cell/tensor.hpp"

#include "numa/misc/data.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

TEST_CASE("matrix_aligned concepts", "[matrix_aligned][concepts]")
{
  static_assert(Cell<matrix_aligned<double, major::left, 4>>);
}

TEST_CASE("basic matrix_aligned", "[matrix_aligned]")
{
  matrix_aligned<int32_t, major::left, 4> ma;

  resize(ma.vary_axes(), {8, 3}, 7);

  resize(ma.vary_axes(), {12, 4}, 8);

  auto cc = make_copy<matrix<int>>(ma);

  CHECK(
      cc ==
      make_from_json<matrix<int>>("{ \"size\": [12, 4],"
                                  "  \"values\":"
                                  "[[7, 7, 7, 7,   7, 7, 7, 7,   8, 8, 8, 8],"
                                  " [7, 7, 7, 7,   7, 7, 7, 7,   8, 8, 8, 8],"
                                  " [7, 7, 7, 7,   7, 7, 7, 7,   8, 8, 8, 8],"
                                  " [8, 8, 8, 8,   8, 8, 8, 8,   8, 8, 8, 8],"
                                  "] }"));

  matrix_aligned_variant<int32_t, major::left> ccv(std::move(ma));

  auto ccvs = span(ccv);

  std::visit([](auto const& ccs) { CHECK(ccs.at(1, 1) == 7); }, ccvs);
}
