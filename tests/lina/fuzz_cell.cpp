#include "numa/lina/matrix_aligned.hpp"

#include "numa/misc/fuzz_cell.hpp"

#include "numa/ctxt/phase_monitors.hpp"

#include "catch2/catch.hpp"

using namespace numa;
using namespace numa::misc;

constexpr index_t reps = 500000;

/*
 * matrix_aligned<major::left>
 */
template<typename T, Allocator Alloc>
using fuzz_matrix_aligned_left = matrix_aligned<T, major::left, 4, Alloc>;

TEST_CASE(
    "extensive fuzz test for matrix_aligned (left)",
    "[.][fuzz][extensive][matrix_aligned_left]")
{
  phase_monitor_observe pm(std::cout, c_note);

  fuzz_cell<fuzz_matrix_aligned_left>(reps, 3e-3, "matrix_aligned (left)");
}

/*
 * matrix_aligned<major::right>
 */
template<typename T, Allocator Alloc>
using fuzz_matrix_aligned_right = matrix_aligned<T, major::right, 8, Alloc>;

TEST_CASE(
    "extensive fuzz test for matrix_aligned (right)",
    "[.][fuzz][extensive][matrix_aligned_right]")
{
  phase_monitor_observe pm(std::cout, c_note);

  fuzz_cell<fuzz_matrix_aligned_right>(reps, 3e-3, "matrix_aligned (right)");
}
