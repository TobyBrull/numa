namespace strassen_search::detail {
  template<typename RangeLhs, typename RangeRhs>
  std::strong_ordering
  three_way_lexicographical_compare(RangeLhs const& lhs, RangeRhs const& rhs)
  {
    bool const is_equal =
        std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    if (is_equal) {
      return std::strong_ordering::equal;
    }
    else {
      bool const is_less = std::lexicographical_compare(
          lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
      if (is_less) {
        return std::strong_ordering::less;
      }
      else {
        return std::strong_ordering::greater;
      }
    }
  }
}

namespace strassen_search {
  template<typename T>
  blkmat_sum<T>::blkmat_sum(
      T const c_11, T const c_12, T const c_21, T const c_22)
  {
    (*this)(0, 0) = c_11;
    (*this)(0, 1) = c_12;
    (*this)(1, 0) = c_21;
    (*this)(1, 1) = c_22;
  }

  template<typename T>
  blkmat_sum<T> blkmat_sum<T>::operator+(blkmat_sum<T> const& other) const
  {
    auto retval = *this;
    retval += other;
    return retval;
  }

  template<typename T>
  blkmat_prod_sum<T> blkmat_sum<T>::operator*(blkmat_sum<T> const& other) const
  {
    auto const& self = (*this);
    blkmat_prod_sum<T> retval;
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) {
        for (int k = 0; k < 2; ++k) {
          for (int l = 0; l < 2; ++l) {
            retval(i, j, k, l) = self(i, j) * other(k, l);
          }
        }
      }
    }
    return retval;
  }

  template<typename T>
  blkmat_sum<T> blkmat_sum<T>::operator*(T const alpha) const
  {
    auto retval = *this;
    retval *= alpha;
    return retval;
  }

  template<typename T>
  std::strong_ordering
  blkmat_sum<T>::operator<=>(blkmat_sum<T> const& other) const noexcept
  {
    return detail::three_way_lexicographical_compare(*this, other);
  }

  template<typename T>
  std::vector<blkmat_sum<T>> blkmat_sum<T>::search_space_complete(T eta)
  {
    ASSERT(eta >= T{1});
    std::vector<blkmat_sum<T>> retval;
    size_t const n = [eta] {
      size_t const fact = 2 * static_cast<size_t>(eta) + 1;
      return fact * fact * fact * fact - 1;
    }();
    retval.reserve(n);
    for (T a = -eta; a <= eta; a += T{1}) {
      for (T b = -eta; b <= eta; b += T{1}) {
        for (T c = -eta; c <= eta; c += T{1}) {
          for (T d = -eta; d <= eta; d += T{1}) {
            if (a != T{0} || b != T{0} || c != T{0} || d != T{0}) {
              retval.emplace_back(a, b, c, d);
            }
          }
        }
      }
    }
    ASSERT(n == retval.size());
    return retval;
  }

  template<typename T>
  std::vector<blkmat_sum<T>> blkmat_sum<T>::search_space_canonical()
  {
    std::vector<blkmat_sum<T>> retval;
    retval.reserve(4);
    retval.emplace_back(1, 0, 0, 0);
    retval.emplace_back(0, 1, 0, 0);
    retval.emplace_back(0, 0, 1, 0);
    retval.emplace_back(0, 0, 0, 1);
    return retval;
  }

  template<typename T>
  std::vector<blkmat_sum<T>> blkmat_sum<T>::search_space_strassen()
  {
    std::vector<blkmat_sum<T>> retval;
    retval.reserve(7);
    retval.emplace_back(1, 0, 0, 1);
    retval.emplace_back(0, 0, 1, 1);
    retval.emplace_back(1, 0, 0, 0);
    retval.emplace_back(0, 0, 0, 1);
    retval.emplace_back(1, 1, 0, 0);
    retval.emplace_back(-1, 0, 1, 0);
    retval.emplace_back(0, 1, 0, -1);
    return retval;
  }

  template<typename T>
  blkmat_prod_sum<T> blkmat_prod_sum<T>::from_solution(int const i, int const j)
  {
    ASSERT(0 <= i && i < 2);
    ASSERT(0 <= j && j < 2);
    blkmat_prod_sum<T> retval;
    retval(i, 0, 0, j) = 1;
    retval(i, 1, 1, j) = 1;
    return retval;
  }

  template<typename T>
  bool blkmat_prod_sum<T>::is_solution(int const i, int const j) const
  {
    ASSERT(0 <= i && i < 2);
    ASSERT(0 <= j && j < 2);
    auto const& self = (*this);
    if ((self(i, 0, 0, j) != self(i, 1, 1, j)) || (self(i, 0, 0, j) == 0)) {
      return false;
    }

    auto copy = self;

    copy(i, 0, 0, j) = 0;
    copy(i, 1, 1, j) = 0;

    return numa::all_of(numa::f_is_equal_to_t{T{0}}, copy);
  }

  template<typename T>
  bool blkmat_prod_sum<T>::is_solution_any() const
  {
    return which_solution().has_value();
  }

  template<typename T>
  std::optional<std::tuple<int, int>> blkmat_prod_sum<T>::which_solution() const
  {
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) {
        if (is_solution(i, j)) {
          return std::tuple{i, j};
        }
      }
    }
    return std::nullopt;
  }

  template<typename T>
  blkmat_prod_sum<T>
  blkmat_prod_sum<T>::operator+(blkmat_prod_sum<T> const& other) const
  {
    auto retval = *this;
    retval += other;
    return retval;
  }

  template<typename T>
  blkmat_prod_sum<T> blkmat_prod_sum<T>::operator*(T const alpha) const
  {
    auto retval = *this;
    retval *= alpha;
    return retval;
  }

  template<typename T>
  std::strong_ordering blkmat_prod_sum<T>::operator<=>(
      blkmat_prod_sum<T> const& other) const noexcept
  {
    return detail::three_way_lexicographical_compare(*this, other);
  }
}

namespace strassen_search::detail {
  template<typename T, typename FormatIter>
  auto format_blkmat_sum(
      FormatIter out, numa::matrix_span<T> const& span, char const name)
  {
    ASSERT((span.size() == numa::index{2, 2}));

    out = fmt::format_to(out, "(");

    bool first_term = true;
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) {
        auto const x = span(i, j);
        if (x == 0) {
          continue;
        }

        if (x == 1) {
          if (!first_term) {
            out = fmt::format_to(out, " +");
          }
        }
        else if (x == -1) {
          out = fmt::format_to(out, " -");
        }
        else {
          out = fmt::format_to(out, " {:+}", span(i, j));
        }
        out = fmt::format_to(out, " {}_{}{} ", name, i + 1, j + 1);

        first_term = false;
      }
    }
    if (first_term) {
      out = fmt::format_to(out, " 0 ");
    }
    return fmt::format_to(out, ")");
  }
}

template<typename T>
struct fmt::formatter<strassen_search::blkmat_sum<T>> {
  char name = 'A';

  constexpr auto parse(format_parse_context& ctx)
  {
    auto it        = ctx.begin();
    auto const end = ctx.end();

    if (it != end && *it != '}') {
      name = *it++;
    }

    while (it != end && *it != '}') {
      ++it;
    }

    return it;
  }

  template<typename FormatContext>
  auto format(strassen_search::blkmat_sum<T> const& bms, FormatContext& ctx)
  {
    return strassen_search::detail::format_blkmat_sum(
        ctx.out(), bms.span(), name);
  }
};

template<typename T>
struct fmt::formatter<strassen_search::blkmat_prod_sum<T>> {
  char nameA = 'A';
  char nameB = 'B';

  constexpr auto parse(format_parse_context& ctx)
  {
    auto it        = ctx.begin();
    auto const end = ctx.end();

    if (it != end && *it != '}') {
      nameA = *it++;
      nameB = (nameA + 1);
    }

    if (it != end && *it != '}') {
      nameB = *it++;
    }

    THROW_IF(nameA == nameB, "Same name for both matrices: ", nameA);

    while (it != end && *it != '}') {
      ++it;
    }

    return it;
  }

  template<typename FormatContext>
  auto
  format(strassen_search::blkmat_prod_sum<T> const& bmps, FormatContext& ctx)
  {
    using namespace numa;
    using strassen_search::detail::format_blkmat_sum;
    auto const span = bmps.span();

    auto out = ctx.out();

    bool first_term = true;
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) {
        auto const sub_span = span.axes(0_i, 1_i).span_along(i, j);
        if (numa::all_of(numa::f_is_equal_to_t{T{0}}, sub_span)) {
          continue;
        }
        else {
          if (!first_term) {
            out = fmt::format_to(out, "\n+ ");
          }
          out = format_blkmat_sum(out, sub_span, nameA);
          out = fmt::format_to(out, " * {}_{}{}", nameB, i + 1, j + 1);

          first_term = false;
        }
      }
    }
    if (first_term) {
      out = fmt::format_to(out, "( 0 )");
    }
    return out;
  }
};
