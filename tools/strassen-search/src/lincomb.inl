namespace strassen_search {
  namespace detail {
    template<int I, int D, typename T>
    void lincomb_complete_impl(
        std::vector<std::array<T, D>>& retval,
        std::array<T, D>& cur,
        T const eta)
    {
      if constexpr (I == D) {
        if (!std::all_of(cur.begin(), cur.end(), numa::f_is_equal_to_t{T{0}})) {
          retval.push_back(cur);
        }
      }
      else {
        for (T a = -eta; a <= eta; a += T{1}) {
          cur[I] = a;
          lincomb_complete_impl<I + 1, D, T>(retval, cur, eta);
        }
      }
    }
  }

  template<int D, typename T>
  std::vector<std::array<T, D>> lincomb_complete(T const eta)
  {
    std::vector<std::array<T, D>> retval;
    size_t const n = [eta] {
      size_t retval     = 1;
      size_t const fact = 2 * static_cast<size_t>(eta) + 1;
      for (int i = 0; i < D; ++i) {
        retval *= fact;
      }
      return retval - 1;
    }();
    retval.reserve(n);
    std::array<T, D> cur;
    detail::lincomb_complete_impl<0, D, T>(retval, cur, eta);
    ASSERT(retval.size() == n);
    return retval;
  }
}
