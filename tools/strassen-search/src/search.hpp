#ifndef INCLUDE_GUARD_STRASSEN_SEARCH_SEARCH_HPP_
#define INCLUDE_GUARD_STRASSEN_SEARCH_SEARCH_HPP_

#include "algospec.hpp"
#include "lincomb.hpp"

#include <numa/ctxt/phase.hpp>

namespace strassen_search {
  template<int D, typename T, typename F>
  void search_symmetric_AB(std::span<blkmat_sum<T> const> search_space_AB, F f);

  template<int D, typename T>
  void search_symmetric(
      std::span<blkmat_sum<T> const> search_space_AB,
      std::span<std::array<T, D> const> search_space_M,
      std::function<void(algospec<D, T> const&)> solution_callback);

  template<int D, typename T>
  void search_asymmetric(
      std::span<blkmat_sum<T> const> search_space_A,
      std::span<blkmat_sum<T> const> search_space_B,
      std::span<std::array<T, D> const> search_space_M,
      std::function<void(algospec<D, T> const&)> solution_callback);
}

#include "search.inl"

#endif
