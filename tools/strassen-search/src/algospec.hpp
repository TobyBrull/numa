#ifndef INCLUDE_GUARD_STRASSEN_SEARCH_ALGOSPEC_HPP_
#define INCLUDE_GUARD_STRASSEN_SEARCH_ALGOSPEC_HPP_

#include "blkmat.hpp"

namespace strassen_search {
  template<int D, typename T>
  struct algospec {
    numa::vector_fixed_size<blkmat_sum<T>, D> A;
    numa::vector_fixed_size<blkmat_sum<T>, D> B;
    numa::matrix_fixed_size<T, D, 4> M;

    /// Computes the total blkmat_prod_sum for the column "col".
    blkmat_prod_sum<T> sum_column(int col) const;

    /// Checks if all four columns of M correspond to a solution and each of
    /// those solutions are different.
    bool is_solution() const;

    /// Checks if the first "num_cols" columns of M are compatible with a full
    /// solution.
    bool is_partial_solution(int num_cols) const;
  };
}

#include "algospec.inl"

#endif
