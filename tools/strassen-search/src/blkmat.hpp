#ifndef INCLUDE_GUARD_STRASSEN_SEARCH_BLKMAT_HPP_
#define INCLUDE_GUARD_STRASSEN_SEARCH_BLKMAT_HPP_

#include "fwd.hpp"

#include <numa/cell/serialize_json.hpp>
#include <numa/cell/tensor_fixed_size.hpp>
#include <numa/misc/main.hpp>

#include <fmt/format.h>
#include <fmt/ostream.h>

namespace strassen_search {
  /// Represents a sum of submatrices of a 2-by-2 block matrix. More precisely,
  /// if we have a block matrix with equally-sized sblock square blocks
  ///     [ A_11, A_12 ]
  ///     [ A_21, A_22 ]
  /// and an object of this class has the values c_ij, then this class
  /// represents the sum
  ///     ( c_11 A_11 + c_12 A_12 + c_21 A_21 + c_22 A_22 )
  ///
  /// This class supports formatting.
  template<typename T>
  struct blkmat_sum : public numa::matrix_fixed_size<T, 2, 2> {
    blkmat_sum() = default;
    blkmat_sum(T c_11, T c_12, T c_21, T c_22);

    blkmat_sum<T> operator+(blkmat_sum<T> const&) const;
    blkmat_prod_sum<T> operator*(blkmat_sum<T> const&) const;
    blkmat_sum<T> operator*(T alpha) const;

    /// Returns an array of length ((2 * eta + 1)**4 - 1) containing all
    /// distinct, non-zero blkmat_sum objects with integer coefficients in the
    /// interval [-eta, eta].
    static std::vector<blkmat_sum<T>> search_space_complete(T eta);

    /// Returns an array containing the four canonical blkmat_sums.
    static std::vector<blkmat_sum<T>> search_space_canonical();

    /// Returns an array containing the seven differen blkmat_sums used in
    /// Strassen's algorithm.
    static std::vector<blkmat_sum<T>> search_space_strassen();

    std::strong_ordering operator<=>(blkmat_sum<T> const&) const noexcept;
  };

  /// Represents the sum of submatrix products of two 2-by-2 block matrices.
  /// More precisely, if we have the two block matrcies with equally-sized
  /// square blocks
  ///     [ A_11, A_12 ]    [ B_11, B_12 ]
  ///     [ A_21, A_22 ]    [ B_21, B_22 ]
  /// and an object of this class has values denoted by c_ijkl, then this class
  /// represents the sum
  ///     (c_1111 A_11 + c_1211 A_12 + c_2111 A_21 + c_2211 A_22) * B_11
  ///   + (c_1112 A_11 + c_1212 A_12 + c_2112 A_21 + c_2212 A_22) * B_12
  ///   + (c_1121 A_11 + c_1221 A_12 + c_2121 A_21 + c_2221 A_22) * B_21
  ///   + (c_1122 A_11 + c_1222 A_12 + c_2122 A_21 + c_2222 A_22) * B_22
  ///
  /// This class supports formatting.
  template<typename T>
  struct blkmat_prod_sum : public numa::tensor_fixed_size<T, 2, 2, 2, 2> {
    blkmat_prod_sum() = default;

    /// Constructs the desired solution for the selected 2-by-2 submatrix. This
    /// is:
    ///       (A_i1 * B_1j) + (A_i2 * B_2j)
    ///
    /// The parameters i and j must each be either 0 or 1.
    static blkmat_prod_sum<T> from_solution(int i, int j);

    /// Check if this object is a non-zero multiple of
    /// strassen_search::blkmat_prod_sum<T>::from_solution(i, j).
    bool is_solution(int i, int j) const;

    /// Check if this object is a non-zero multiple of any of the four different
    /// objects that can be returned by
    /// strassen_search::blkmat_prod_sum<T>::from_solution(i, j).
    bool is_solution_any() const;

    /// Check if this object is a non-zero multiple of any of the four different
    /// objects that can be returned by
    /// strassen_search::blkmat_prod_sum<T>::from_solution(i, j) and if so,
    /// which.
    std::optional<std::tuple<int, int>> which_solution() const;

    blkmat_prod_sum<T> operator+(blkmat_prod_sum<T> const&) const;
    blkmat_prod_sum<T> operator*(T alpha) const;

    std::strong_ordering operator<=>(blkmat_prod_sum<T> const&) const noexcept;
  };
}

#include "blkmat.inl"

#endif
