namespace strassen_search {
  template<int D, typename T>
  blkmat_prod_sum<T> algospec<D, T>::sum_column(int const col) const
  {
    ASSERT(0 <= col && col < 4);
    blkmat_prod_sum<T> retval;
    for (int row = 0; row < D; ++row) {
      if (M(row, col) != 0) {
        retval += (A(row) * B(row)) * M(row, col);
      }
    }
    return retval;
  }

  template<int D, typename T>
  bool algospec<D, T>::is_partial_solution(int const num_cols) const
  {
    numa::matrix_fixed_size<bool, 2, 2> solution;
    for (int col = 0; col < num_cols; ++col) {
      auto result = sum_column(col).which_solution();
      if (!result.has_value()) {
        return false;
      }
      else {
        auto const [i, j] = result.value();
        if (solution(i, j)) {
          return false;
        }
        solution(i, j) = true;
      }
    }
    return true;
  }

  template<int D, typename T>
  bool algospec<D, T>::is_solution() const
  {
    return is_partial_solution(4);
  }
}

template<int D, typename T>
struct fmt::formatter<strassen_search::algospec<D, T>>
  : public fmt::formatter<std::string_view> {
  template<typename FormatContext>
  auto format(strassen_search::algospec<D, T> const& aspec, FormatContext& ctx)
  {
    fmt::memory_buffer out;
    for (int row = 0; row < D; ++row) {
      fmt::format_to(
          out, "M_{} := {:A} * {:B}\n", row + 1, aspec.A(row), aspec.B(row));
    }
    fmt::format_to(out, "\n");
    for (int col = 0; col < 4; ++col) {
      fmt::format_to(out, "C_{} := ", col);
      for (int row = 0; row < D; ++row) {
        if (aspec.M(row, col) != 0) {
          fmt::format_to(out, " {:+} M_{}", aspec.M(row, col), row + 1);
        }
      }
      auto const sol = aspec.sum_column(col).which_solution();
      if (sol.has_value()) {
        auto const [i, j] = sol.value();
        fmt::format_to(out, " (solution for C_{}{})", i + 1, j + 1);
      }
      fmt::format_to(out, "\n");
    }
    return fmt::formatter<std::string_view>::format(
        std::string_view(out.data(), out.size()), ctx);
  }
};
