#ifndef INCLUDE_GUARD_STRASSEN_SEARCH_LINCOMB_HPP_
#define INCLUDE_GUARD_STRASSEN_SEARCH_LINCOMB_HPP_

#include "fwd.hpp"

#include <numa/boot/error.hpp>
#include <numa/core/functors.hpp>

#include <array>
#include <vector>

namespace strassen_search {
  /// Returns an array of length ((2 * eta + 1)**D - 1) containing all non-zero
  /// combinations of D integers from the interval [-eta, eta].
  template<int D, typename T>
  std::vector<std::array<T, D>> lincomb_complete(T eta);
}

#include "lincomb.inl"

#endif
