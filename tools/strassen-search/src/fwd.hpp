#ifndef INCLUDE_GUARD_STRASSEN_SEARCH_FWD_HPP_
#define INCLUDE_GUARD_STRASSEN_SEARCH_FWD_HPP_

#include <cstdint>

namespace strassen_search {
  using int_t = int8_t;

  template<typename T = int_t>
  struct blkmat_sum;

  template<typename T = int_t>
  struct blkmat_prod_sum;
}

#endif
