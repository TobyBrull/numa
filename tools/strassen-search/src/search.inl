namespace strassen_search {
  namespace detail {
    template<int j, int k, typename F>
    void for_each_n_choose_k_impl(
        std::array<size_t, k>& cur,
        size_t const from_index,
        size_t const n,
        F const& f)
    {
      static_assert(j <= k);
      constexpr int remaining_k = k - j;
      if (n < remaining_k) {
        return;
      }
      size_t const last_index = n - remaining_k;
      for (size_t i = from_index; i <= last_index; ++i) {
        cur[j] = i;
        if constexpr (j + 1 == k) {
          f(cur);
        }
        else {
          for_each_n_choose_k_impl<j + 1, k, F>(cur, i + 1, n, f);
        }
      }
    }

    template<int k, typename F>
    void for_each_n_choose_k(size_t const n, F const& f)
    {
      if constexpr (k >= 1) {
        std::array<size_t, k> cur{};
        for_each_n_choose_k_impl<0, k, F>(cur, 0, n, f);
      }
    }

    inline numa::index_t
    n_choose_k(numa::index_t const n, numa::index_t const k)
    {
      if (k == 0) {
        return 1;
      }
      else {
        return (n * n_choose_k(n - 1, k - 1)) / k;
      }
    }

    inline numa::index_t factorial(numa::index_t const n)
    {
      if (n <= 1) {
        return 1;
      }
      else {
        return n * factorial(n - 1);
      }
    }
  }

  template<int D, typename T, typename F>
  void search_symmetric_AB(std::span<blkmat_sum<T> const> search_space_AB, F f)
  {
    size_t const n = search_space_AB.size();
    using namespace numa;
    phase p_outer{c_section, "A"_s, detail::n_choose_k(n, D), NUMA_LOCATION};

    algospec<D, T> aspec;

    detail::for_each_n_choose_k<D>(
        n, [&](std::array<size_t, D> const& choice_A) {
          p_outer.start(1);
          for (int i = 0; i < D; ++i) {
            aspec.A[i] = search_space_AB[choice_A[i]];
          }

          std::array<numa::index_t, D> choice_B_rel_A;
          std::iota(
              choice_B_rel_A.begin(),
              choice_B_rel_A.end(),
              static_cast<numa::index_t>(0));

          phase p_inner{c_section, "B"_s, detail::factorial(D), NUMA_LOCATION};
          do {
            p_inner.start(1);
            for (int i = 0; i < D; ++i) {
              aspec.B[i] = aspec.A[choice_B_rel_A[i]];
            }
            f(aspec);
          } while (std::next_permutation(
              choice_B_rel_A.begin(), choice_B_rel_A.end()));
        });
  }

  template<int D, typename T>
  void search_symmetric(
      std::span<blkmat_sum<T> const> search_space_AB,
      std::span<std::array<T, D> const> search_space_M,
      std::function<void(algospec<D, T> const&)> solution_callback)
  {
    search_symmetric_AB<D, T>(search_space_AB, [&](algospec<D, T>& aspec) {
      int col = 0;

      size_t const m = search_space_M.size();

      using namespace numa;
      phase p{c_section, "M"_s, m, NUMA_LOCATION};
      constexpr int skip = 1000;

      for (size_t k = 0; k < m; ++k) {
        if (k % skip == 0) {
          p.start(std::min<index_t>(skip, m - k));
        }

        auto const& ms = search_space_M[k];

        for (int i = 0; i < D; ++i) {
          aspec.M(i, col) = ms[i];
        }

        if (aspec.is_partial_solution(col + 1)) {
          col += 1;

          if (col == 4) {
            solution_callback(aspec);
            return;
          }
        }
      }
    });
  }
}
