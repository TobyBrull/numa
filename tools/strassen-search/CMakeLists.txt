file(GLOB STRASSEN_SEARCH_FILES CONFIGURE_DEPENDS src/*.hpp src/*.inl src/*.cpp)
add_library(strassen-search-lib INTERFACE)
target_link_libraries(strassen-search-lib INTERFACE numa_shared fmt::fmt)
target_include_directories (strassen-search-lib INTERFACE "${CMAKE_CURRENT_SOURCE_DIR}/src")

file(GLOB STRASSEN_SEARCH_TEST_FILES CONFIGURE_DEPENDS tests/*.hpp tests/*.cpp)
add_executable(strassen-search-test ${STRASSEN_SEARCH_TEST_FILES})
target_link_libraries(strassen-search-test PRIVATE strassen-search-lib catch_prebuilt)

add_test(NAME strassen-search-test COMMAND strassen-search-test)

add_executable(strassen-search main.cpp)
target_link_libraries(strassen-search PUBLIC strassen-search-lib)
