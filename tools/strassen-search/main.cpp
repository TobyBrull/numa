#include "search.hpp"

#include <fmt/format.h>

int
main(int const argc, char const** const argv)
{
  fmt::print("Brute-Force Search For Strassen-Like Algorithms\n");

  return numa::misc::main(argc, argv, [] {
    using namespace strassen_search;

    using T = int_t;

    constexpr int D = 4;

    auto const param_blkmat  = numa::env_var_default("blkmat", 1);
    auto const param_lincomb = numa::env_var_default("lincomb", 1);

    auto const search_AB = blkmat_sum<T>::search_space_complete(param_blkmat);
    auto const search_M  = lincomb_complete<D, T>(param_lincomb);

    std::optional<algospec<D, T>> result;
    try {
      search_symmetric<D, T>(search_AB, search_M, [&result](auto const& aspec) {
        result = aspec;
        throw std::runtime_error("solution found");
      });
    }
    catch (std::runtime_error const&) {
    }

    if (result.has_value()) {
      fmt::print("{}\n", result.value());
    }
  });
}
