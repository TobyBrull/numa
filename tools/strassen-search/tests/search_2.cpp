#include "search.hpp"

#include <numa/cell/tensor.hpp>

#include <catch2/catch.hpp>

using namespace strassen_search;

TEST_CASE("search strassen algorithm", "[search]")
{
  using T = int_t;

  constexpr int D = 7;

  auto const search_space_AB = blkmat_sum<T>::search_space_strassen();
  auto const search_space_M  = lincomb_complete<D, T>(1);

  algospec<D, T> result;
  CHECK(!result.is_solution());

  CHECK_THROWS_WITH(
      (search_symmetric<D, T>(
          search_space_AB,
          search_space_M,
          [&result](auto const& aspec) {
            CHECK(aspec.is_solution());
            result = aspec;
            throw std::runtime_error("STOP TEST");
          })),
      "STOP TEST");

  CHECK(result.is_solution());
  fmt::print("{}\n\n\n", result);
}
