#include "search.hpp"

#include <numa/cell/tensor.hpp>

#include <catch2/catch.hpp>

using namespace strassen_search;

namespace {
  template<int k>
  struct n_choose_k_iterator {
    std::vector<std::array<size_t, k>> operator()(size_t const n) const
    {
      std::vector<std::array<size_t, k>> result;
      strassen_search::detail::for_each_n_choose_k<k>(
          n, [&](std::array<size_t, k> const& choice) {
            result.push_back(choice);
          });
      return result;
    }
  };
}

TEST_CASE("n choose k generator", "[search]")
{
  {
    auto const result = n_choose_k_iterator<0>{}(4);
    std::vector<std::array<size_t, 0>> const expected = {};
    CHECK(result == expected);
  }
  {
    auto const result = n_choose_k_iterator<1>{}(4);
    std::vector<std::array<size_t, 1>> const expected = {
        {0},
        {1},
        {2},
        {3},
    };
    CHECK(result == expected);
  }
  {
    auto const result = n_choose_k_iterator<2>{}(4);
    std::vector<std::array<size_t, 2>> const expected = {
        {0, 1},
        {0, 2},
        {0, 3},
        {1, 2},
        {1, 3},
        {2, 3},
    };
    CHECK(result == expected);
  }
  {
    auto const result = n_choose_k_iterator<3>{}(4);
    std::vector<std::array<size_t, 3>> const expected = {
        {0, 1, 2},
        {0, 1, 3},
        {0, 2, 3},
        {1, 2, 3},
    };
    CHECK(result == expected);
  }
  {
    auto const result = n_choose_k_iterator<4>{}(4);
    std::vector<std::array<size_t, 4>> const expected = {
        {0, 1, 2, 3},
    };
    CHECK(result == expected);
  }
  {
    auto const result = n_choose_k_iterator<0>{}(5);
    std::vector<std::array<size_t, 0>> const expected = {};
    CHECK(result == expected);
  }
  {
    auto const result = n_choose_k_iterator<1>{}(5);
    std::vector<std::array<size_t, 1>> const expected = {
        {0},
        {1},
        {2},
        {3},
        {4},
    };
    CHECK(result == expected);
  }
  {
    auto const result = n_choose_k_iterator<2>{}(5);
    std::vector<std::array<size_t, 2>> const expected = {
        {0, 1},
        {0, 2},
        {0, 3},
        {0, 4},
        {1, 2},
        {1, 3},
        {1, 4},
        {2, 3},
        {2, 4},
        {3, 4},
    };
    CHECK(result == expected);
  }
  {
    auto const result = n_choose_k_iterator<3>{}(5);
    std::vector<std::array<size_t, 3>> const expected = {
        {0, 1, 2},
        {0, 1, 3},
        {0, 1, 4},
        {0, 2, 3},
        {0, 2, 4},
        {0, 3, 4},
        {1, 2, 3},
        {1, 2, 4},
        {1, 3, 4},
        {2, 3, 4},
    };
    CHECK(result == expected);
  }
  {
    auto const result = n_choose_k_iterator<4>{}(5);
    std::vector<std::array<size_t, 4>> const expected = {
        {0, 1, 2, 3},
        {0, 1, 2, 4},
        {0, 1, 3, 4},
        {0, 2, 3, 4},
        {1, 2, 3, 4},
    };
    CHECK(result == expected);
  }
  {
    auto const result = n_choose_k_iterator<5>{}(5);
    std::vector<std::array<size_t, 5>> const expected = {
        {0, 1, 2, 3, 4},
    };
    CHECK(result == expected);
  }
}

TEST_CASE("n choose k", "[search]")
{
  CHECK(detail::n_choose_k(1, 0) == 1);
  CHECK(detail::n_choose_k(1, 1) == 1);

  CHECK(detail::n_choose_k(2, 0) == 1);
  CHECK(detail::n_choose_k(2, 1) == 2);
  CHECK(detail::n_choose_k(2, 2) == 1);

  CHECK(detail::n_choose_k(3, 0) == 1);
  CHECK(detail::n_choose_k(3, 1) == 3);
  CHECK(detail::n_choose_k(3, 2) == 3);
  CHECK(detail::n_choose_k(3, 3) == 1);

  CHECK(detail::n_choose_k(4, 0) == 1);
  CHECK(detail::n_choose_k(4, 1) == 4);
  CHECK(detail::n_choose_k(4, 2) == 6);
  CHECK(detail::n_choose_k(4, 3) == 4);
  CHECK(detail::n_choose_k(4, 4) == 1);
}

TEST_CASE("n factorial", "[search]")
{
  CHECK(detail::factorial(0) == 1);
  CHECK(detail::factorial(1) == 1);
  CHECK(detail::factorial(2) == 2);
  CHECK(detail::factorial(3) == 6);
  CHECK(detail::factorial(4) == 24);
}

TEST_CASE("search_symmetric_AB", "[search]")
{
  using T = int_t;

  constexpr int D = 3;

  blkmat_sum<T> A1(1, 0, 0, 0);
  blkmat_sum<T> A2(2, 1, 0, 0);
  blkmat_sum<T> A3(3, 2, 1, 0);
  blkmat_sum<T> A4(4, 3, 2, 1);
  blkmat_sum<T> A5(5, 4, 3, 2);

  std::vector<blkmat_sum<T>> const search_space_AB{A1, A2, A3, A4, A5};

  std::vector<algospec<D, T>> results;
  search_symmetric_AB<D, T>(search_space_AB, [&results](auto const& aspec) {
    results.push_back(aspec);
    return false;
  });

  REQUIRE(results.size() == 10 * 6);

  auto const make_result = [](auto A, auto B, auto C) {
    numa::vector_fixed_size<blkmat_sum<T>, D> retval;
    retval[0] = A;
    retval[1] = B;
    retval[2] = C;
    return retval;
  };

  // A1, A2, A3
  CHECK(results[0].A == make_result(A1, A2, A3));
  CHECK(results[0].B == make_result(A1, A2, A3));

  CHECK(results[1].A == make_result(A1, A2, A3));
  CHECK(results[1].B == make_result(A1, A3, A2));

  CHECK(results[2].A == make_result(A1, A2, A3));
  CHECK(results[2].B == make_result(A2, A1, A3));

  CHECK(results[3].A == make_result(A1, A2, A3));
  CHECK(results[3].B == make_result(A2, A3, A1));

  CHECK(results[4].A == make_result(A1, A2, A3));
  CHECK(results[4].B == make_result(A3, A1, A2));

  CHECK(results[5].A == make_result(A1, A2, A3));
  CHECK(results[5].B == make_result(A3, A2, A1));

  // A1, A2, A4
  CHECK(results[6].A == make_result(A1, A2, A4));
  CHECK(results[6].B == make_result(A1, A2, A4));

  CHECK(results[7].A == make_result(A1, A2, A4));
  CHECK(results[7].B == make_result(A1, A4, A2));

  CHECK(results[8].A == make_result(A1, A2, A4));
  CHECK(results[8].B == make_result(A2, A1, A4));

  CHECK(results[9].A == make_result(A1, A2, A4));
  CHECK(results[9].B == make_result(A2, A4, A1));

  CHECK(results[10].A == make_result(A1, A2, A4));
  CHECK(results[10].B == make_result(A4, A1, A2));

  CHECK(results[11].A == make_result(A1, A2, A4));
  CHECK(results[11].B == make_result(A4, A2, A1));

  // A1, A2, A5
  CHECK(results[12].A == make_result(A1, A2, A5));
  CHECK(results[12].B == make_result(A1, A2, A5));

  CHECK(results[13].A == make_result(A1, A2, A5));
  CHECK(results[13].B == make_result(A1, A5, A2));

  CHECK(results[14].A == make_result(A1, A2, A5));
  CHECK(results[14].B == make_result(A2, A1, A5));

  CHECK(results[15].A == make_result(A1, A2, A5));
  CHECK(results[15].B == make_result(A2, A5, A1));

  CHECK(results[16].A == make_result(A1, A2, A5));
  CHECK(results[16].B == make_result(A5, A1, A2));

  CHECK(results[17].A == make_result(A1, A2, A5));
  CHECK(results[17].B == make_result(A5, A2, A1));

  // A1, A3, A4
  CHECK(results[18].A == make_result(A1, A3, A4));
  CHECK(results[18].B == make_result(A1, A3, A4));

  CHECK(results[19].A == make_result(A1, A3, A4));
  CHECK(results[19].B == make_result(A1, A4, A3));

  CHECK(results[20].A == make_result(A1, A3, A4));
  CHECK(results[20].B == make_result(A3, A1, A4));

  CHECK(results[21].A == make_result(A1, A3, A4));
  CHECK(results[21].B == make_result(A3, A4, A1));

  CHECK(results[22].A == make_result(A1, A3, A4));
  CHECK(results[22].B == make_result(A4, A1, A3));

  CHECK(results[23].A == make_result(A1, A3, A4));
  CHECK(results[23].B == make_result(A4, A3, A1));

  // A1, A3, A5
  CHECK(results[24].A == make_result(A1, A3, A5));
  CHECK(results[24].B == make_result(A1, A3, A5));

  CHECK(results[25].A == make_result(A1, A3, A5));
  CHECK(results[25].B == make_result(A1, A5, A3));

  CHECK(results[26].A == make_result(A1, A3, A5));
  CHECK(results[26].B == make_result(A3, A1, A5));

  CHECK(results[27].A == make_result(A1, A3, A5));
  CHECK(results[27].B == make_result(A3, A5, A1));

  CHECK(results[28].A == make_result(A1, A3, A5));
  CHECK(results[28].B == make_result(A5, A1, A3));

  CHECK(results[29].A == make_result(A1, A3, A5));
  CHECK(results[29].B == make_result(A5, A3, A1));

  // A1, A4, A5
  CHECK(results[30].A == make_result(A1, A4, A5));
  CHECK(results[30].B == make_result(A1, A4, A5));

  CHECK(results[31].A == make_result(A1, A4, A5));
  CHECK(results[31].B == make_result(A1, A5, A4));

  CHECK(results[32].A == make_result(A1, A4, A5));
  CHECK(results[32].B == make_result(A4, A1, A5));

  CHECK(results[33].A == make_result(A1, A4, A5));
  CHECK(results[33].B == make_result(A4, A5, A1));

  CHECK(results[34].A == make_result(A1, A4, A5));
  CHECK(results[34].B == make_result(A5, A1, A4));

  CHECK(results[35].A == make_result(A1, A4, A5));
  CHECK(results[35].B == make_result(A5, A4, A1));

  // A2, A3, A4
  CHECK(results[36].A == make_result(A2, A3, A4));
  CHECK(results[36].B == make_result(A2, A3, A4));

  CHECK(results[37].A == make_result(A2, A3, A4));
  CHECK(results[37].B == make_result(A2, A4, A3));

  CHECK(results[38].A == make_result(A2, A3, A4));
  CHECK(results[38].B == make_result(A3, A2, A4));

  CHECK(results[39].A == make_result(A2, A3, A4));
  CHECK(results[39].B == make_result(A3, A4, A2));

  CHECK(results[40].A == make_result(A2, A3, A4));
  CHECK(results[40].B == make_result(A4, A2, A3));

  CHECK(results[41].A == make_result(A2, A3, A4));
  CHECK(results[41].B == make_result(A4, A3, A2));

  // A2, A3, A5
  CHECK(results[42].A == make_result(A2, A3, A5));
  CHECK(results[42].B == make_result(A2, A3, A5));

  CHECK(results[43].A == make_result(A2, A3, A5));
  CHECK(results[43].B == make_result(A2, A5, A3));

  CHECK(results[44].A == make_result(A2, A3, A5));
  CHECK(results[44].B == make_result(A3, A2, A5));

  CHECK(results[45].A == make_result(A2, A3, A5));
  CHECK(results[45].B == make_result(A3, A5, A2));

  CHECK(results[46].A == make_result(A2, A3, A5));
  CHECK(results[46].B == make_result(A5, A2, A3));

  CHECK(results[47].A == make_result(A2, A3, A5));
  CHECK(results[47].B == make_result(A5, A3, A2));

  // A2, A4, A5
  CHECK(results[48].A == make_result(A2, A4, A5));
  CHECK(results[48].B == make_result(A2, A4, A5));

  CHECK(results[49].A == make_result(A2, A4, A5));
  CHECK(results[49].B == make_result(A2, A5, A4));

  CHECK(results[50].A == make_result(A2, A4, A5));
  CHECK(results[50].B == make_result(A4, A2, A5));

  CHECK(results[51].A == make_result(A2, A4, A5));
  CHECK(results[51].B == make_result(A4, A5, A2));

  CHECK(results[52].A == make_result(A2, A4, A5));
  CHECK(results[52].B == make_result(A5, A2, A4));

  CHECK(results[53].A == make_result(A2, A4, A5));
  CHECK(results[53].B == make_result(A5, A4, A2));

  // A3, A4, A5
  CHECK(results[54].A == make_result(A3, A4, A5));
  CHECK(results[54].B == make_result(A3, A4, A5));

  CHECK(results[55].A == make_result(A3, A4, A5));
  CHECK(results[55].B == make_result(A3, A5, A4));

  CHECK(results[56].A == make_result(A3, A4, A5));
  CHECK(results[56].B == make_result(A4, A3, A5));

  CHECK(results[57].A == make_result(A3, A4, A5));
  CHECK(results[57].B == make_result(A4, A5, A3));

  CHECK(results[58].A == make_result(A3, A4, A5));
  CHECK(results[58].B == make_result(A5, A3, A4));

  CHECK(results[59].A == make_result(A3, A4, A5));
  CHECK(results[59].B == make_result(A5, A4, A3));
}
