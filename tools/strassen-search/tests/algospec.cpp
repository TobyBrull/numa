#include "algospec.hpp"

#include <catch2/catch.hpp>

using namespace strassen_search;

TEST_CASE("algospec canonical algorithm", "[algospec]")
{
  using T = int_t;

  algospec<8, T> aspec;

  // clang-format off
  aspec.B(0) =       blkmat_sum<T>(1, 0,
                                   0, 0);
  aspec.A(0) = blkmat_sum<T>(1, 0,
                             0, 0);
  ////
  aspec.B(1) =       blkmat_sum<T>(0, 0,
                                   1, 0);
  aspec.A(1) = blkmat_sum<T>(0, 1,
                             0, 0);
  aspec.M(0, 0) = 1;
  aspec.M(1, 0) = 1;
  CHECK(aspec.sum_column(0) == blkmat_prod_sum<T>::from_solution(0, 0));
  CHECK(aspec.is_partial_solution(1));
  CHECK(!aspec.is_partial_solution(2));
  //////////////////
  aspec.B(2) =       blkmat_sum<T>(0, 1,
                                   0, 0);
  aspec.A(2) = blkmat_sum<T>(1, 0,
                             0, 0);
  ////
  aspec.B(3) =       blkmat_sum<T>(0, 0,
                                   0, 1);
  aspec.A(3) = blkmat_sum<T>(0, 1,
                             0, 0);
  aspec.M(2, 1) = 1;
  aspec.M(3, 1) = 1;
  CHECK(aspec.sum_column(1) == blkmat_prod_sum<T>::from_solution(0, 1));
  CHECK(aspec.is_partial_solution(2));
  CHECK(!aspec.is_partial_solution(3));
  //////////////////
  aspec.B(4) =       blkmat_sum<T>(1, 0,
                                   0, 0);
  aspec.A(4) = blkmat_sum<T>(0, 0,
                             1, 0);
  ////
  aspec.B(5) =       blkmat_sum<T>(0, 0,
                                   1, 0);
  aspec.A(5) = blkmat_sum<T>(0, 0,
                             0, 1);
  aspec.M(4, 2) = 1;
  aspec.M(5, 2) = 1;
  CHECK(aspec.sum_column(2) == blkmat_prod_sum<T>::from_solution(1, 0));
  CHECK(aspec.is_partial_solution(3));
  CHECK(!aspec.is_partial_solution(4));
  //////////////////
  aspec.B(6) =       blkmat_sum<T>(0, 1,
                                   0, 0);
  aspec.A(6) = blkmat_sum<T>(0, 0,
                             1, 0);
  ////
  aspec.B(7) =       blkmat_sum<T>(0, 0,
                                   0, 1);
  aspec.A(7) = blkmat_sum<T>(0, 0,
                             0, 1);
  aspec.M(6, 3) = 1;
  aspec.M(7, 3) = 1;
  // clang-format on

  CHECK(aspec.sum_column(3) == blkmat_prod_sum<T>::from_solution(1, 1));
  CHECK(aspec.is_partial_solution(4));
  CHECK(aspec.is_solution());

  aspec.A(7)(1, 1) = 0;
  aspec.A(7)(0, 1) = 1;
  CHECK(!aspec.is_solution());
  CHECK(
      fmt::to_string(aspec.sum_column(3)) ==
      "( A_21 ) * B_12\n+ ( A_12 ) * B_22");

  CHECK(
      fmt::to_string(aspec) ==
      "M_1 := ( A_11 ) * ( B_11 )\n"
      "M_2 := ( A_12 ) * ( B_21 )\n"
      "M_3 := ( A_11 ) * ( B_12 )\n"
      "M_4 := ( A_12 ) * ( B_22 )\n"
      "M_5 := ( A_21 ) * ( B_11 )\n"
      "M_6 := ( A_22 ) * ( B_21 )\n"
      "M_7 := ( A_21 ) * ( B_12 )\n"
      "M_8 := ( A_12 ) * ( B_22 )\n"
      "\n"
      "C_0 :=  +1 M_1 +1 M_2 (solution for C_11)\n"
      "C_1 :=  +1 M_3 +1 M_4 (solution for C_12)\n"
      "C_2 :=  +1 M_5 +1 M_6 (solution for C_21)\n"
      "C_3 :=  +1 M_7 +1 M_8\n");
}

TEST_CASE("algospec strassen algorithm", "[algospec]")
{
  using T = int_t;

  algospec<7, T> aspec;

  // clang-format off
  aspec.B(0) =       blkmat_sum<T>(1, 0,
                                   0, 1);
  aspec.A(0) = blkmat_sum<T>(1, 0,
                             0, 1);
  ////
  aspec.B(1) =       blkmat_sum<T>(1, 0,
                                   0, 0);
  aspec.A(1) = blkmat_sum<T>(0, 0,
                             1, 1);
  ////
  aspec.B(2) =       blkmat_sum<T>(0, 1,
                                   0, -1);
  aspec.A(2) = blkmat_sum<T>(1, 0,
                             0, 0);
  ////
  aspec.B(3) =       blkmat_sum<T>(-1, 0,
                                   1, 0);
  aspec.A(3) = blkmat_sum<T>(0, 0,
                             0, 1);
  ////
  aspec.B(4) =       blkmat_sum<T>(0, 0,
                                   0, 1);
  aspec.A(4) = blkmat_sum<T>(1, 1,
                             0, 0);
  ////
  aspec.B(5) =       blkmat_sum<T>(1, 1,
                                   0, 0);
  aspec.A(5) = blkmat_sum<T>(-1, 0,
                             1, 0);
  ////
  aspec.B(6) =       blkmat_sum<T>(0, 0,
                                   1, 1);
  aspec.A(6) = blkmat_sum<T>(0, 1,
                             0, -1);
  // clang-format on

  // Check consistency with blkmat_sum<T>::search_space_strassen()
  auto const st = blkmat_sum<T>::search_space_strassen();
  for (int i = 0; i < 7; ++i) {
    CHECK(aspec.A(i) == st.at(i));
  }
  CHECK(aspec.B(0) == st.at(0));
  CHECK(aspec.B(1) == st.at(2));
  CHECK(aspec.B(2) == st.at(6));
  CHECK(aspec.B(3) == st.at(5));
  CHECK(aspec.B(4) == st.at(3));
  CHECK(aspec.B(5) == st.at(4));
  CHECK(aspec.B(6) == st.at(1));

  aspec.M(0, 0) = 1;
  aspec.M(3, 0) = 1;
  aspec.M(4, 0) = -1;
  aspec.M(6, 0) = 1;

  aspec.M(2, 1) = 1;
  aspec.M(4, 1) = 1;

  aspec.M(1, 2) = 1;
  aspec.M(3, 2) = 1;

  aspec.M(0, 3) = 1;
  aspec.M(1, 3) = -1;
  aspec.M(2, 3) = 1;
  aspec.M(5, 3) = 1;

  CHECK(aspec.is_solution());
  CHECK(
      fmt::to_string(aspec) ==
      "M_1 := ( A_11  + A_22 ) * ( B_11  + B_22 )\n"
      "M_2 := ( A_21  + A_22 ) * ( B_11 )\n"
      "M_3 := ( A_11 ) * ( B_12  - B_22 )\n"
      "M_4 := ( A_22 ) * ( - B_11  + B_21 )\n"
      "M_5 := ( A_11  + A_12 ) * ( B_22 )\n"
      "M_6 := ( - A_11  + A_21 ) * ( B_11  + B_12 )\n"
      "M_7 := ( A_12  - A_22 ) * ( B_21  + B_22 )\n"
      "\n"
      "C_0 :=  +1 M_1 +1 M_4 -1 M_5 +1 M_7 (solution for C_11)\n"
      "C_1 :=  +1 M_3 +1 M_5 (solution for C_12)\n"
      "C_2 :=  +1 M_2 +1 M_4 (solution for C_21)\n"
      "C_3 :=  +1 M_1 -1 M_2 +1 M_3 +1 M_6 (solution for C_22)\n");
}
