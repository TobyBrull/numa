#include "blkmat.hpp"

#include <numa/cell/tensor.hpp>

#include <catch2/catch.hpp>

using namespace strassen_search;

TEST_CASE("blkmat_sum", "[blkmat]")
{
  auto const with = [&](auto... xs) {
    return fmt::format("{}", blkmat_sum<>(xs...));
  };
  CHECK(with(0, 0, 0, 0) == "( 0 )");
  CHECK(with(1, 0, 0, -1) == "( A_11  - A_22 )");
  CHECK(with(0, 0, 0, -1) == "( - A_22 )");
  CHECK(with(0, 0, 0, 1) == "( A_22 )");
  CHECK(with(1, 1, 1, 1) == "( A_11  + A_12  + A_21  + A_22 )");
  CHECK(with(-1, -1, -1, -1) == "( - A_11  - A_12  - A_21  - A_22 )");
  CHECK(with(3, 3, 0, 1) == "( +3 A_11  +3 A_12  + A_22 )");
  CHECK(with(-1, 1, -1, 1) == "( - A_11  + A_12  - A_21  + A_22 )");
  CHECK(with(1, -1, 1, -1) == "( A_11  - A_12  + A_21  - A_22 )");

  {
    blkmat_sum<int_t> const B(2, 3, -2, -3);
    CHECK(fmt::format("{:B}", B) == "( +2 B_11  +3 B_12  -2 B_21  -3 B_22 )");
  }
}

TEST_CASE("blkmat_sum operators", "[blkmat]")
{
  blkmat_sum<> const A(1, 0, 0, 1);
  blkmat_sum<> const B(2, 3, 4, 5);
  auto const result = A + B;
  CHECK(result == blkmat_sum<>(3, 3, 4, 6));

  CHECK(A == A);
  CHECK(B != A);
  CHECK(A < B);
  CHECK(B < result);

  {
    std::vector v = {B, A};
    numa::sort(v);
    CHECK(v.at(0) == A);
    CHECK(v.at(1) == B);
  }

  CHECK(A * 3 == blkmat_sum<>(3, 0, 0, 3));
  CHECK(B * 3 == blkmat_sum<>(6, 9, 12, 15));
  CHECK(result * -1 == blkmat_sum<>(-3, -3, -4, -6));
}

TEST_CASE("blkmat_sum sets", "[blkmat]")
{
  {
    auto const v = blkmat_sum<>::search_space_complete(1);
    REQUIRE(v.size() == 80);
    CHECK(fmt::to_string(v.at(0)) == "( - A_11  - A_12  - A_21  - A_22 )");
    CHECK(fmt::to_string(v.at(1)) == "( - A_11  - A_12  - A_21 )");
    CHECK(fmt::to_string(v.at(2)) == "( - A_11  - A_12  - A_21  + A_22 )");
    CHECK(fmt::to_string(v.at(3)) == "( - A_11  - A_12  - A_22 )");
    CHECK(fmt::to_string(v.at(4)) == "( - A_11  - A_12 )");
    CHECK(fmt::to_string(v.at(5)) == "( - A_11  - A_12  + A_22 )");
    CHECK(fmt::to_string(v.at(6)) == "( - A_11  - A_12  + A_21  - A_22 )");
    CHECK(fmt::to_string(v.at(7)) == "( - A_11  - A_12  + A_21 )");
    CHECK(fmt::to_string(v.at(8)) == "( - A_11  - A_12  + A_21  + A_22 )");
  }

  {
    auto const v = blkmat_sum<>::search_space_canonical();
    REQUIRE(v.size() == 4);
    CHECK(fmt::to_string(v.at(0)) == "( A_11 )");
    CHECK(fmt::to_string(v.at(1)) == "( A_12 )");
    CHECK(fmt::to_string(v.at(2)) == "( A_21 )");
    CHECK(fmt::to_string(v.at(3)) == "( A_22 )");
  }

  {
    auto const v = blkmat_sum<>::search_space_strassen();
    REQUIRE(v.size() == 7);
    CHECK(fmt::to_string(v.at(0)) == "( A_11  + A_22 )");
    CHECK(fmt::to_string(v.at(1)) == "( A_21  + A_22 )");
    CHECK(fmt::to_string(v.at(2)) == "( A_11 )");
    CHECK(fmt::to_string(v.at(3)) == "( A_22 )");
    CHECK(fmt::to_string(v.at(4)) == "( A_11  + A_12 )");
    CHECK(fmt::to_string(v.at(5)) == "( - A_11  + A_21 )");
    CHECK(fmt::to_string(v.at(6)) == "( A_12  - A_22 )");
  }
}

TEST_CASE("blkmat_prod_sum", "[blkmat]")
{
  {
    blkmat_prod_sum<int_t> bmps;
    bmps(0, 0, 0, 0) = 1;
    CHECK(fmt::format("{}", bmps) == "( A_11 ) * B_11");
    CHECK(fmt::format("{:AB}", bmps) == "( A_11 ) * B_11");
    CHECK(fmt::format("{:C}", bmps) == "( C_11 ) * D_11");
    CHECK(fmt::format("{:DF}", bmps) == "( D_11 ) * F_11");
  }

  {
    blkmat_prod_sum<int_t> bmps;
    CHECK(fmt::to_string(bmps) == "( 0 )");
    bmps(0, 0, 0, 0) = 1;
    CHECK(fmt::to_string(bmps) == "( A_11 ) * B_11");
    bmps(1, 1, 0, 0) = -1;
    CHECK(fmt::to_string(bmps) == "( A_11  - A_22 ) * B_11");
    bmps(1, 1, 0, 1) = -1;
    bmps(1, 1, 1, 0) = -1;
    CHECK(
        fmt::to_string(bmps) ==
        "( A_11  - A_22 ) * B_11\n"
        "+ ( - A_22 ) * B_12\n"
        "+ ( - A_22 ) * B_21");
  }

  {
    blkmat_prod_sum<int_t> bmps;
    int_t count = 0;
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) {
        bmps(j / 2, j % 2, i / 2, i % 2) = ++count;
      }
    }
    CHECK(
        fmt::to_string(bmps) ==
        "( A_11  +2 A_12  +3 A_21  +4 A_22 ) * B_11\n"
        "+ ( +5 A_11  +6 A_12  +7 A_21  +8 A_22 ) * B_12\n"
        "+ ( +9 A_11  +10 A_12  +11 A_21  +12 A_22 ) * B_21\n"
        "+ ( +13 A_11  +14 A_12  +15 A_21  +16 A_22 ) * B_22");
  }
}

TEST_CASE("blkmat_prod_sum operators", "[blkmat]")
{
  blkmat_prod_sum<> A;
  A(1, 0, 1, 0) = 7;
  A(1, 1, 1, 0) = 4;
  blkmat_prod_sum<> B;
  B(0, 1, 0, 1) = 2;
  B(1, 1, 1, 0) = 4;

  auto const result = A + B;
  blkmat_prod_sum<> expected;
  expected(1, 0, 1, 0) = 7;
  expected(0, 1, 0, 1) = 2;
  expected(1, 1, 1, 0) = 8;
  CHECK(result == expected);

  CHECK(A == A);
  CHECK(B != A);
  CHECK(B < A);
  CHECK(A < result);

  B(1, 0, 1, 0) = 7;
  CHECK(A < B);
  B(0, 1, 0, 1) = 0;
  CHECK(A == B);

  {
    blkmat_prod_sum<> expected;
    expected(1, 0, 1, 0) = 14;
    expected(1, 1, 1, 0) = 8;
    CHECK(A * 2 == expected);
  }

  {
    blkmat_prod_sum<> expected;
    expected(1, 0, 1, 0) = 21;
    expected(1, 1, 1, 0) = 12;
    CHECK(B * 3 == expected);
  }
}

TEST_CASE("blkmat product", "[blkmat]")
{
  {
    blkmat_sum<> const A(1, 0, 0, 1);
    blkmat_sum<> const B(2, 3, 4, 5);

    auto const result = A * B;
    blkmat_prod_sum<> expected;
    expected(0, 0, 0, 0) = 2;
    expected(1, 1, 0, 0) = 2;
    expected(0, 0, 0, 1) = 3;
    expected(1, 1, 0, 1) = 3;
    expected(0, 0, 1, 0) = 4;
    expected(1, 1, 1, 0) = 4;
    expected(0, 0, 1, 1) = 5;
    expected(1, 1, 1, 1) = 5;

    CHECK(result == expected);
  }

  {
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) {
        for (int k = 0; k < 2; ++k) {
          for (int l = 0; l < 2; ++l) {
            blkmat_sum<> A;
            blkmat_sum<> B;
            A(i, j) = 3;
            B(k, l) = 4;

            auto const result = A * B;

            blkmat_prod_sum<> expected;
            expected(i, j, k, l) = 12;

            CHECK(result == expected);
          }
        }
      }
    }
  }
}

TEST_CASE("blkmat_prod_sum solution", "[blkmat]")
{
  auto const test = [](int const i, int const j) {
    return fmt::to_string(blkmat_prod_sum<>::from_solution(i, j));
  };
  CHECK(test(0, 0) == "( A_11 ) * B_11\n+ ( A_12 ) * B_21");
  CHECK(test(1, 0) == "( A_21 ) * B_11\n+ ( A_22 ) * B_21");
  CHECK(test(0, 1) == "( A_11 ) * B_12\n+ ( A_12 ) * B_22");
  CHECK(test(1, 1) == "( A_21 ) * B_12\n+ ( A_22 ) * B_22");

  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      auto const check_sol =
          [i, j](blkmat_prod_sum<> const& mat, bool const is_sol = true) {
            auto const which = mat.which_solution();
            if (is_sol) {
              REQUIRE(which.has_value());
              CHECK(which.value() == std::tuple{i, j});
            }
            else {
              CHECK(!which.has_value());
            }
            CHECK(mat.is_solution_any() == is_sol);
            CHECK(mat.is_solution(i, j) == is_sol);
            CHECK(!mat.is_solution((i + 1) % 2, j));
            CHECK(!mat.is_solution(i, (j + 1) % 2));
            CHECK(!mat.is_solution((i + 1) % 2, (j + 1) % 2));
          };
      check_sol(blkmat_prod_sum<>::from_solution(i, j));
      check_sol(blkmat_prod_sum<>::from_solution(i, j) * -2);
      check_sol(blkmat_prod_sum<>::from_solution(i, j) * 3);
      check_sol(blkmat_prod_sum<>::from_solution(i, j) * 0, false);
      {
        auto mat = blkmat_prod_sum<>::from_solution(i, j);
        check_sol(mat);

        mat(i, 0, 0, j) *= 2;
        check_sol(mat, false);
        mat(i, 0, 0, j) /= 2;
        check_sol(mat);

        mat(i, 1, 1, j) = 0;
        check_sol(mat, false);
        mat(i, 1, 1, j) = 2;
        check_sol(mat, false);
        mat(i, 1, 1, j) = 1;
        check_sol(mat);

        mat(i, 1, 0, j) = 1;
        check_sol(mat, false);
        mat(i, 1, 0, j) = 0;
        check_sol(mat);

        mat((i + 1) % 2, 0, 0, (j + 1) % 2) = 2;
        mat((i + 1) % 2, 0, 0, (j + 1) % 2) = 2;
        check_sol(mat, false);
        mat((i + 1) % 2, 0, 0, (j + 1) % 2) = 0;
        mat((i + 1) % 2, 0, 0, (j + 1) % 2) = 0;
        check_sol(mat);
      }
    }
  }
}
