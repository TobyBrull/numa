#include "lincomb.hpp"

#include <catch2/catch.hpp>

using namespace strassen_search;

TEST_CASE("lincomb_complete", "[lincomb]")
{
  auto const result = lincomb_complete<4, int_t>(1);
  REQUIRE(result.size() == (3 * 3 * 3 * 3 - 1));
  auto const check_result_at = [&](size_t const index,
                                   std::initializer_list<int_t> const ilist) {
    std::array<int_t, 4> expected;
    REQUIRE(ilist.size() == 4);
    std::copy(ilist.begin(), ilist.end(), expected.begin());
    CHECK(result.at(index) == expected);
  };

  check_result_at(0, {-1, -1, -1, -1});
  check_result_at(1, {-1, -1, -1, 0});
  check_result_at(2, {-1, -1, -1, 1});
  check_result_at(3, {-1, -1, 0, -1});
  check_result_at(4, {-1, -1, 0, 0});
  check_result_at(5, {-1, -1, 0, 1});
  check_result_at(6, {-1, -1, 1, -1});
  check_result_at(7, {-1, -1, 1, 0});
  check_result_at(8, {-1, -1, 1, 1});

  check_result_at(9, {-1, 0, -1, -1});
  check_result_at(10, {-1, 0, -1, 0});
  check_result_at(11, {-1, 0, -1, 1});
  check_result_at(12, {-1, 0, 0, -1});
  check_result_at(13, {-1, 0, 0, 0});
  check_result_at(14, {-1, 0, 0, 1});
  check_result_at(15, {-1, 0, 1, -1});
  check_result_at(16, {-1, 0, 1, 0});
  check_result_at(17, {-1, 0, 1, 1});

  check_result_at(18, {-1, 1, -1, -1});
  check_result_at(19, {-1, 1, -1, 0});
  check_result_at(20, {-1, 1, -1, 1});
  check_result_at(21, {-1, 1, 0, -1});
  check_result_at(22, {-1, 1, 0, 0});
  check_result_at(23, {-1, 1, 0, 1});
  check_result_at(24, {-1, 1, 1, -1});
  check_result_at(25, {-1, 1, 1, 0});
  check_result_at(26, {-1, 1, 1, 1});

  check_result_at(79, {1, 1, 1, 1});
}
