set -Eeuxo pipefail

if [[ $# -ne 1 ]] ; then
    echo 'Usage: bash version.sh <commit>'
    exit 1
fi

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

commit=$1

git_version_tag=$(git --git-dir=$script_dir/.git describe --match="v[0-9]*" ${commit})
echo "${git_version_tag:1}"
