import os, subprocess
from conans import ConanFile, CMake, tools

class NumaConan(ConanFile):
    build_requires = [
        "catch2/2.13.0",
        "fmt/7.1.3",
        ]
    generators = "cmake_find_package"

    name = "numa"
    version = "0.0.1"
    license = "<Put the package license here>"
    author = "<Put your name here> <And your email here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of Numa here>"
    topics = ("<Put some tag here>", "<here>", "<and here>")
    settings = ("os", "compiler", "build_type", "arch")
    options = {}
    default_options = {}
    exports_sources = ("*", "!build*")

    def init(self):
        p = subprocess.Popen(['cat', 'VERSION'], stdout=subprocess.PIPE)
        self.version = p.communicate()[0].decode('utf-8')
        self.output.info(f'Git-version: {self.version}')

    def build(self):
        cmake = CMake(self, generator="Ninja")
        cmake.configure()
        cmake.build()
        cmake.test()

    def package(self):
        self.copy("*.hpp", dst="include", src="src")
        self.copy("*.inl", dst="include", src="src")
        for lib_glob in [
                "*numa_s*.lib", "*numa_s*.dll",
                "*numa_s*.a", "*numa_s*.so",
                "*numa_s*.dylib"]:
            self.copy(lib_glob, dst="lib", keep_path=False)

        self.copy("numa.benchmark.*", dst="bin", src="bin", keep_path=False)

        self.copy("*.cmake", dst="lib/cmake/numa", src="cmake")

    def package_info(self):
        self.cpp_info.libs = ["numa_shared"]
