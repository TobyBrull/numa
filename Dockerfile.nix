FROM nixos/nix as build

RUN nix-env -i bash coreutils gnutar

COPY . /repo

RUN mkdir /build \
 && tar --exclude-vcs --transform s/repo/numa/ -czf /build/numa.tar.gz /repo \
 && cp /repo/*.nix /build \
 && cp /repo/VERSION /build

# Numa library
RUN cd /build \
 && nix-build . -A numa \
 && nix-env -f default.nix -iA numa \
 && numa.benchmark.ctxt.phase

# Numa docker image
RUN cd /build \
 && container_file=$(nix-build . -A numa-container) \
 && mkdir /output \
 && cp $container_file /output
