# TODO list

## general
* `set(CMAKE\_CXX\_CLANG\_TIDY clang-tidy -checks=-*,readability-*)`
* use class types in non-type template parameters

## ctxt
* exception safety in thalanx
  * add thalanx\_check, which throws an exception if another
    fork/batch was terminated due to an exception.
  * should every 'phase call' include such a thalanx\_check
  * Run batch of jobs in such order that longest running go first
  * Run graph of jobs (long-term), like TBB/dask?
* unit-tests
  * get rid of custom fib implementations in test/ctxt
* performance
* logging: allow logging, e.g., floats which are then stringified in a
  different thread
* environment-variables:
   * add flag that logs whether or not a variable was read. A warning may be
     logged or an error raised if certain variables were not read in the
     context<env> destructor.
   * add contextual return values: a context<return_env> rc {"foo", "bar"} can
     be created. Then any function can return a hidden return value by saying
     something like context<return_env>::set_return_value("foo", 42). If no
     context exists, that value is simply lost. If a context exists, that value
     is stored in that context. After some functions are run, one has to
     explicitly end the context: const auto retval = rc.close(). Then one can
     access the values via retval.get("foo").

* (Context)Policy = std::tuple<policy\_phase\_t, ...>
  * shall make progress?
  * shall make phases?
  * shall make status?
  * shall show info messages?
  * shall use thalanx?

## seri

* redo the string formating
  * io with phase\_status
  * string-to-type and type-to-string conversion
* downstream hook: template<typename T, typename Style> struct serializer; (??)
* quickly dump std::vector and other ranges for debugging
* be able to control indent in json output
* make\_from\_json for std::variant of numa::structures
* seri::ostream & seri::istream
  * allow writing objects (via a function-pointer and using reinterpret\_cast,
    cf. Overload 152, A Low-Latency Logging Framework)?

## core
* functors and accumulators
  * cpu\_target aware functors (plus interaction for cell-for-each)
* allocators
  * rename
      default\_allocator -> allocator\_default,
      over\_aligned\_allocator -> allocator\_aligned
  * pmr
  * allocator propagation
  * null-allocator
* multi-threading

## cell
* cells & spans:
  * fixed-capacity-tensor should have index-type templatized
  * most tensors/cells should have side::left/right parameter
    controlling memory layout (row/col-major);
    especially tensor_staged, should also change raster there.
  * make tensor-span-iterator work for uniform-tensor (when all strides are
    zero?); maybe uniform-tensor shouldn't have a "T" but a "T [1]" as member.
  * json-serialization for uniform-tensor
  * clean-up sparse-tensor:
    * replace sparse-tensor transform by make-cell paradigm
  * Cell::num\_axes from index\_t -> c\_index\_t?
  * add make\_vector, make\_matrix, make\_cuboid, make\_matrix\_staged, ...

* structure:
  * there should be a way to prescribe the order of the axes
    (currently the order of the axes is always in which they are seen)
  * give joint-structure another shot
  * have dynamic joint-structure, e.g., a vector of tensor\_staged should
    be possible with just a single allocation. 
  * compiler-error when passing initer that is never used
  * make-cell/make-copy/make-move needs way to inject custom allocator
    * span\_to\_cell needs to respect allocator and potentially other value-type.
  * sorting along a single axis?

* generalise "sum of all rows in a matrix into a vector"
  (for-each-along & for-each-plumb)???
  * or get rid of all along/plumb stuff?

* for-each:
  * in-order/sequential
  * multi-threaded
  * with usage of logging/performance/progress context (see (Context)Policy
    above)
  * pointer based (?)
  * iterators can be sequential or not: that means whether they iterate the
    indices/positions in order or not. Iterator-based to-json serialisation
    only should compile for sequential iterators.

## lina
* arbitrary dimensional tensor multiply
  * make\_dot?
  * with progress
* arbitrary dimensional tensor LU decomposition
* arbitrary dimensional tensor QR factorisation and least squares
* arbitrary dimensional Cholesky
* eigenvalues
* all for complex numbers

## Upcoming

* benchmark
  * use repeat (make configurable), use mandatory data
    * repeat\_functor = at least 3, up to a second
  * run the non-mandatory combinations in order of effort, starting with those
    taking the least amount of effort until some criterion is satisfied


* performance
  * implement fast copy\_left & copy\_right (may be via the cpu\_target aware functors?)
