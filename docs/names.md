# Possible library names

* ```nu``` - numerics library
* ```mu``` - math-utitlity lib
* ```nx``` - numerics library
* ```num``` - ???
* ```npi``` - numeric programming interface
* ```lila``` - library for linear algebra
* ```lama``` - linear algebra and math
* ```lapa``` - lapack without 'ck'
* ```numa``` - numerical mathematics
* ```numat``` - numerical mathematics
* ```numi``` - numerical math interface
* ```nupi``` - new numpi
* ```numx``` - numerix
* ```sumo``` - ???
* ```numo``` - numerical math ????
* ```numc``` - numerics for c
* ```numr``` - ???
* ```numt``` - ???
* ```numu``` - ???
* ```numv``` - ???
* ```numy``` - ???
* ```numz``` - ???
* ```lima``` - library for math
* ```mali``` - math library

# Possible type-names

* b int8
* s int16
* i int32
* l int64
* s float32
* d float64
* c complex64
* z complex128
