[[up](../numa.md)]

# numa/ctxt

Fundamental assumption:
The contexted class (= the class that interacts with the stack)
is used infrequently.

## Applications:

Broadly speaking, there a three areas of application:
1. threading
  * Interacts with the other two areas of application.
  * See below, thalanx.
2. ghost parameters
  * See below, env
  * As if every function has a set of additional parameters, which are not used
    by most functions.
3. execution monitoring
  * Allocation monitoring.
  * Monitoring construction and destruction of certain types.
  * See below, phase.

In particular, ctxt implements the following:

1. [env](ctxt/env.md) (Environment variables)
  * Linux bash environment variables
  * command-line parameters
  * std::locale and stream flags (like std::setprecision (16))
2. [phase](ctxt/phase.md) (Program execution monitoring)
  * logger
  * phase
  * progress
  * maybe allow user to give feedback?
3. [thalanx](ctxt/thalanx.md) (Multi-threading)
  * fixed number of threads
  * stack-bound
  * compatible with 1. and 2.

Maybe also do:
* random-engine?
* allocator?
* error-handling?

## Use-cases

* thalanx-inited, then env-scope constructed, before fork.
* env-scope constructed, then thalanx-inited, then forked.

## Dependencies

<img src='https://g.gravizo.com/svg?digraph G {
  thalanx_thread -> thalanx [style=dashed, constraint=false];
  thalanx -> thalanx_registry -> thalanx_remote_stacks -> thalanx_thread -> thalanx_misc;
  thalanx_detail -> thalanx;
  thalanx_fork -> thalanx_detail;
  thalanx_batch -> thalanx_detail;
  context -> thalanx;
  env -> env_detail -> context;
  env_var -> env_var_detail -> context;
  phase_receptor -> context;
  phase -> phase_receptor -> phase_delegate;
  phase_delegate -> phase_record -> phase_record_parser -> phase_record_strand -> phase_registry -> phase_level;
  phase_delegate -> progress_stack -> progress_stack_strand;
  phase_monitors -> phase_receptor;
  phase_monitors -> progress_estimator;
  phase_monitors -> terminal;
  thalanx [fillcolor=lightblue, style=filled];
  thalanx_fork [fillcolor=lightblue, style=filled];
  thalanx_batch [fillcolor=lightblue, style=filled];
  env [fillcolor=lightblue, style=filled];
  env_var [fillcolor=lightblue, style=filled];
  phase [fillcolor=lightblue, style=filled];
  phase_monitors [fillcolor=lightblue, style=filled];
}'/>

## References

* [Blog entry by Giuseppe](
  http://www.italiancpp.org/2017/03/19/singleton-revisited-eng/)
* [ACCU article by Bob Schmidt](
  http://accu.org/index.php/journals/2085)
* [news.ycombinator](
  https://news.ycombinator.com/item?id=24544716)
