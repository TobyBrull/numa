[[up](../numa.md)]

# numa/core

* Functor: Function call operator could be const.
* Accumulator: Function call operator cannot be const. This also includes
  generators.
