[[up](../ctxt.md)]

# numa/ctxt/phase.hpp

Program execution monitoring

Features:
  * logger
  * phase
  * progress
  * maybe allow user to give feedback?

```cpp
namespace numa
{
  c_profile
  c_trace

  // default Debug/Release boundary

  c_note

  // default phase_monitor::stack boundary

  c_paragraph
  c_section
  c_chapter
  c_volume

  c_warning
  c_error
  c_critical
}
```

## New Features?

* For progress, ability to fix the names of the parts
  at the very beginning, for better progress bar.

## Code annotation (monitored side)

```cpp
void f ()
{
  /*
   * numa::phase is a RAII type that designates a the code
   * between its constructor and destructor.
   * The first parameter is the phase-level, the second the
   * (compile-time) name of the phase, and the third a
   * (compile-time) string that uniquely identifies the
   * phase.
   */
  numa::phase p {numa::c_info, "f"_s, NUMA_LOCATION};

  /*
   * It can also be used to log status messages.
   */
  p.status ("Hello, World!");
}

int g (int a = 7)
{
  /*
   * Various phase-levels are available. They form a hierarchy
   * with, e.g., numa::c_paragraph being higher up (and thus
   * more important) then numa::c_info.
   */
  numa::phase p {numa::c_paragraph, "g"_s, NUMA_LOCATION};

  /*
   * Status messages can also contain variables.
   */
  p.status ("running g (", a, ")");

  p.args ("a, b, c", a, b, c);
  p.NUMA_ARGS (a, b, c)

  int x = 0;
  int y = 42;

  /*
   * Status messages may also be issued independent of a
   * concrete phase.
   */
  numa::phase_status (numa::c_debug, "x = ", x);
  numa::phase_status (numa::c_debug, NUMA_VAR (y));

  /*
   * This macro expands to...
   */
  NUMA_DEBUG_VAR (x, y);
  /*
   * ...this block.
   */
  {
    numa::phase p {numa::c_debug, "debug"_s, NUMA_LOCATION};
    p.status ("x = ", x);
    p.status ("y = ", y);
  }

  /*
   * Analogous to NUMA_DEBUG_VAR. Only available for the
   * two phase-levels numa::c_debug and numa::c_trace
   * (e.g., there is no NUMA_INFO_VAR).
   */
  NUMA_TRACE_VAR (x, y);

  return y;
}

int numa::main ()
{
  /*
   * Giving a numeric value as the third constructor
   * argument to numa::phase creates a phase that
   * also has progress-bar capabilities. The numeric
   * value should correspond to an estimate of the
   * total amount of time that is taken up by this phase.
   */
  numa::phase p {numa::c_section, "main"_s, numa::progress {10},
                 NUMA_LOCATION};

  p.start (3);
  p.status ("preamble");
  g ();

  p.start (4);
  p.status ("core");
  f ();

  p.start (3);
  p.status ("postprocessing");
  return g ();
}

int numa::main ()
{
  numa::phase p {
      numa::c_section, "main"_s, 10,
      std::vector<std::pair<int, std::string>> {
          { 3, "preamble" },
          { 4, "core" },
          { 3, "postprocessing" }
      },
      NUMA_LOCATION};

  p.start ();
  g ();

  p.start ();
  f ();

  p.start ();
  return g ();
}
```

## Use-cases (monitoring side)

* Retracing:(logger, phase)
  * Debugging
  * log variables
  * active stack frames determine error messages
  * get log of program execution after program has finished
* Profiling: (phase, logger?, progress
  * minimal overhead logging of program execution
* Progress: (progress, phase, logger??)
  * Live user feedback
  * progress and estimated time till finished.
  * what the program is currently doing
  * information and warnings

```cpp
/*
 * Monitoring
 */
int main (int argc, char *argv [])
{
  numa::environment_scope es (numa::environment_system);
  numa::environment_scope ec (argc, argv);

  auto const mode = numa::environment::get ("phase_monitor"_s);

  if (mode == "debug")
  {
    numa::phase_monitor_debug em (std::cout);
    return numa::main ();
  }
  else if (mode == "profile")
  {
    numa::phase_monitor_profile em;
    int const retval = numa::main ();
    em.to_json (std::cout);
    return retval;
  }
  else if (mode == "observe")
  {
    numa::phase_monitor_observe em (std::cout);
    return numa::main ();
  }
}
```

## References

* [Member Function Pointers and the Fastest Possible C++ Delegates](
  https://www.codeproject.com/Articles/7150/%2FArticles%2F7150%2FMember-Function-Pointers-and-the-Fastest-Possible)
* [The Impossibly Fast C++ Delegates](
  https://www.codeproject.com/articles/11015/the-impossibly-fast-c-delegates)
