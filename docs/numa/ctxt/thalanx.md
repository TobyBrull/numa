[[up](../ctxt.md)]

# numa/ctxt/thalanx.hpp

Multi-threading

Features:
  * fixed number of threads
  * stack-bound

Possible other names:
```cpp
thread\_row
stread (stacked thread)
thrank (=thread rank)
```

## Mmmm???

```cpp

int fib (int n)
{
  if (n <= 1)
  {
    return 1;
  }
  else
  {
    thalanx_throw_if_interrupted ();

    return fib (n - 1) + fib (n - 2);
  }
}

int fib_thalanx_1 (int const n)
{
  ASSERT (n >= 10);

  auto& p = numa::thalanx ();

  if (p.num_threads () == 1)
  {
    return fib (n);
  }
  else
  {
    auto const [f_1, f_2] = p.fork_equal (
        [n]
        {
          return fib_thalanx (n - 1);
        },
        [n]
        {
          return fib_thalanx (n - 2);
        });

    return (f_1 + f_2);
  }
}

int fib_thalanx_2 (int const n)
{
  ASSERT (n >= 10);

  auto& p = numa::thalanx ();

  numa::index_t const nt = p.num_threads ();

  if (nt == 1)
  {
    return fib (n);
  }
  else
  {
    ASSERT (nt >= 2);
    numa::future<int> f_1 = p.fork_of (nt / 2,
        [n]
        {
          return fib_thalanx (n - 1);
        });

    ASSERT (p.num_threads () >= 1);

    int const f_2 = fib_thalanx (n - 2);

    return f_1.get () + f_2;
  }
}

int main ()
{
  /*
   * Build thalanx with 4 additional threads.
   */
  numa::thalanx_init ti (4);

  fib_thalanx_1 (40);
  fib_thalanx_2 (40);
  fib_thalanx_3 (40);
}
```
