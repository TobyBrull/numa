[[up](../ctxt.md)]

# [numa/ctxt/env.hpp](../../src/numa/ctxt/env.hpp)

## List of files

* [numa/ctxt/env.hpp](../../src/numa/ctxt/env.hpp)
* [numa/ctxt/env.inl](../../src/numa/ctxt/env.inl)
* numa/ctxt/env\_static.hpp
* numa/ctxt/env\_static.inl
* numa/ctxt/env\_dynamic.hpp
* numa/ctxt/env\_dynamic.cpp

## Description

Environment variables

Features:
  * Run-time:
    * Linux bash environment variables
    * command-line parameters
  * Compile-time:
    * random-engine?
    * allocator?
    * machanism for stream flags (std::setprecision (16))

## Interface

```cpp
/*
 *
 */
std::string_view numa::env (std::string_view);
```

## Example

```cpp
struct some_options
{
  int num;
  std::string str;
}

void f (int x = numa::env (c_type<some_options>).num)
{
  std::string y = numa::env<some_options> ().str;
}

void g ()
{
  index_t const x = numa::env_var ("x", c_type<index_t>);
  double const y = numa::env_var<double> ("y");

  std::string const a = numa::env_var ("a");

  std::optional<bool> x = numa::env_var_if ("x", c_type<bool>);
}

int main (int argc, char *argv [])
{
  numa::env_var_scope evs1 {env_var_map_from_opsys ()};
  numa::env_var_scope_opsys evs2;

  numa::env_var_scope evs3 {env_var_map_from_cmdline (argc, argv)};
  numa::env_var_scope_cmdline evs4 {argc, argv};

  numa::env_var_scope es ({
      {"x", "42"},
      {"y", "3.14159"},
      {"z", "Hello, World!"},
      {"a", ""}
  });

  numa::env_var_scope esi {env_var_map_from_ini_file ("config.ini")};

  /*
   *
   */
  numa::env_scope<some_options> esr (42, "Hello, World!");
  numa::env_scope esr {c_type<some_options>, 42, "Hello, World!"};

  f ();
  g ();
}
```
