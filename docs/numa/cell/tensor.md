# Tensor

Defined in header [numa/cell/tensor.hpp](../../src/numa/cell/tensor.hpp)

## Class declarations

```
namespace numa
{
  template<typename T, index_t N>
  class tensor_unvaried;

  template<typename T, index_t N, Allocator Alloc = default_allocator>
  class tensor;

  template<typename T, Allocator Alloc = default_allocator>
  using cuboid = tensor<T, 3, Alloc>;

  template<typename T, Allocator Alloc = default_allocator>
  using matrix = tensor<T, 2, Alloc>;

  template<typename T, Allocator Alloc = default_allocator>
  using vector = tensor<T, 1, Alloc>;
}
```

## Unvaried

???

## Cell
