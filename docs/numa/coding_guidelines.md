[[Up](../numa.md)]

# Coding Guidelines

* For collections of things, if the plural makes sense, use the plural. Example:
  A header that contains a set of algorithms, should be called algorithms.hpp,
  not algorithm.hpp. The folder for the unit-tests is called tests not test.
* Prefer names of the same length for things that are similar. Example:
  all sub-libraries have four letters; 'tensor', 'vector', 'matrix', 'cuboid'
  all have six letters.
