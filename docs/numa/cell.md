[[up](../numa.md)]

# numa::cell
# numa::tensor

# Concepts

## Cells and Spans

* A _Span_ is a class that **refers** to memory for an
  N-dimensional tensor; it is like a fat pointer.
* A _Cell_ is a class that contains or **manages** memory for an
  N-dimensional tensor.
* Every _Cell_ is a _Span_.
* Furthermore, a _Cell_ is **just** a container for values; it does
  not confer any meaning onto its values. A _Span_, on the other hand,
  may confer meaning onto its values. What this means, for example, is
  that the upper triangular part of a matrix (which is a _Cell_) can
  represent a symmetric matrix, but there is no way to express that
  semantically at the _Cell_ level. However, one can get a special
  kind of _Span_ to that exact data that represents a symmetric matrix.
* A _Span_ can have a value-type that is const or non-const.
  This const-ness controls if the values that the _Span_ refers
  to can be modified through the _Span_ or not.
* A _Cell_ must always have a value-type that is non-const.
  It must always offer three layers of access:
  * [const]: This is similar to the access provided by a
    const span. It allows to read all values but does not
    allow to make any modifications to the _Cell_.
  * [unvaried]: This is similar to the access provided by a
    non-const span. It allows to read and write all values
    but does not allow to change the size of the _Cell_.
  * [mutable]: As [unvaried], but also allows to make changes
    to the size of the _Cell_.
* Each _Cell_ and each _Span_ also has a _layout_ associated with it.
  The layout describes how the values are layed out in memory. A
  certain _Span_ can only be used to refer to the contents of a
  certain _Cell_, if the two have the same layout.
* Each _Span_ (and by implicated also each _Cell_) also has a _raster_
  associated with it. The raster is a subset of indices, for which
  values may be requested. Asking a _Span_ for values to indices not
  in its raster is undefined behaviour.
* The type-name of every _Cell_ starts with `tensor`, `cuboid`, `matrix`, or
  `vector`.
* The type-name of every _Span_ starts with `t_span`, `cuboid_span`,
  `matrix_span`, or `vector_span`.

### _Span_

* Usually implemented as a struct with only public member variables. These
  member variables may be changed to make the tensor refer to other part of
  memory.
* Usually all member functions are const-qualified and const-ness of the
  referred-to memory is expressed via a const template parameter. This is
  similar to the const-ness of pointers.

```c++
/*
 * Partial, exemplary implementation:
 */

  template<typename T>
  struct t_span_doc
  {
    static_assert (!std::is_reference_v<T>);

    T& operator [] (index_t i) const { return origin_ [i]; }

    T *origin_;
    index_t size_;
  };
```

### _Unvaried_

* Usually implemented as a class with private members.
* Const member functions only allow read access to tensor values;
* Non-const member functions also allow write access to tensor values;
* Size of the tensor can never be modified.
* The ability to obtain an _Unvaried_ is mandatory for a _Cell_.
* Every _Unvaried_ is a _Span_.
* Usually the constructor is protected so that it can only be
  used by the derived cell.
* Sometimes has "span" member functions that return pure spans
  (i.e., spans which are not also _Unvaried_ or _Cell_). The idea here
  is that various unvarieds or cells can have the same span-class
  (see, e.g., `tensor`, `tensor_fixed_size`, `tensor_fixed_capacity`)

```c++
/*
 * Partial, exemplary implementation:
 */

  template<typename T>
  class tensor_doc_unvaried
  {
    public:
      static_assert (!std::is_reference_v<T> && !std::is_const_v<T>);

      T& operator [] (index_t i) { return origin_ [i]; }
      const T& operator [] (index_t i) const { return origin_ [i]; }

      auto span () { return span_doc<T> {origin_, size_}; }
      auto span () const { return span_doc<const T> {origin_, size_}; }
      auto const_span () const { return span (); }

    protected:
      tensor_doc_unvaried () = default;

      tensor_doc_unvaried (tensor_doc_unvaried&&) = delete;
      tensor_doc_unvaried& operator = (tensor_doc_unvaried&&) = delete;

      tensor_doc_unvaried (const tensor_doc_unvaried&) = delete;
      tensor_doc_unvaried& operator = (const tensor_doc_unvaried&) = delete;

      T *origin_ = nullptr;
      index_t size_ = 0;
  };
```

### _Cell_
```c++
/*
 * Partial, exemplary implementation:
 */

  template<typename T, Allocator Alloc = numa::default_allocator>
  class tensor_doc
    : public tensor_doc_unvaried<T>
  {
    public:
      tensor_doc (Alloc alloc = {}) : alloc_ (std::move (alloc)) { }

      auto vary_axes ();

    private:
      Alloc alloc_;
  }
```

### Function Signatures

The following declarations show how functions can be declared that
need the various levels of access described above.

```c++
/*
 * For functions that need only [const] access
 */
template<typename T> void func (t_span_doc<const T>        span);
template<typename T> void func (t_span_doc<const T> const& span);

template<typename T> void func (tensor_doc_unvaried<T> const& span);

/*
 * For functions that need [unvaried] access
 */
template<typename T> void func (t_span_doc<T>        span);
template<typename T> void func (t_span_doc<T> const& span);

template<typename T> void func (tensor_doc_unvaried<T>& span);

/*
 * For functions that need full [mutable] access
 */
template<typename T> void func (tensor_doc<T>& span);
```

# List of cells and spans

The following tables give an overview of the cells and spans that
come with the library.

## Cells

|         cell                  |  layout       |  raster               | TODO |
|:------------------------------|---------------|-----------------------|------|
| [`tensor`](cell/tensor.md)    |  `std`        | `full`                |      |
| `tensor_fixed_capacity`       |  `std`        | `full`                |      |
| `tensor_fixed_size`           |  `std`        | `full`                |      |
| `tensor_uniform`              |  `std`        | `full`                |      |
| `tensor_staged`               | `staged`      | `staged`              |      |
| `tensor_staged_uniform`       | `staged`      | `staged`              |  X   |
| `tensor_mapped`               | `mapped`      | `full`                |  O   |
| `tensor_permutation`          | `permutation` | permutation structure |  X   |

|       weak-cells      | layout         |  raster            | TODO |
|:----------------------|----------------|--------------------|------|
| `tensor_sparse`       | `coordinate`   | sparsity structure |      |
| `tensor_sparse_comp`  | `compressed`   | sparsity structure |  X   |
| `tensor_hierarchical` | `hierarchical` | `full`             |  X   |
| `tensor_blocked`      | `blocked`      | sparsity structure |  X   |

## Spans

|  span                           |  layout  |  raster                  | TODO |
|:--------------------------------|:---------|--------------------------|------|
| `t_span`                        |  `std`   | `full`                   |      |
| `t_span_native`                 |  `std`   | `full`                   |  X   |
| `t_span_triangular`             |  `std`   | `triangular`             |  X   |
| `t_span_mirror`                 |  `std`   | `full`                   |  X   |
| `t_span_band`                   |  `std`   | `band`                   |  X   |
| `t_span_band_periodic`          |  `std`   | `band_periodic`          |  X   |
| `t_span_band_mirror`            |  `std`   | `band_mirror`            |  X   |
| `t_span_band_cut`               |  `std`   | `cut`                    |  X   |
| `t_span_staged`                 | `staged` | `staged`                 |      |
| `t_span_staged_native`          | `staged` | `staged`                 |  X   |
| `t_span_staged_triangular`      | `staged` | `staged_triangular`      |  X   |
| `t_span_staged_mirror`          | `staged` | `staged_mirror`          |  X   |
| `t_span_staged_band`            | `staged` | `staged_band`            |  X   |
| `t_span_staged_band_periodic`   | `staged` | `staged_band_periodic`   |  X   |

## Span-Adoptors

|  span-adaptor                    | layout  |  raster                                  | TODO |
|:---------------------------------|:--------|------------------------------------------|------|
| `t_span_adaptor_triangular`      | adapted | as adapted, but also `triangular`        |      |
| `t_span_adaptor_mirror`          | adapted | mirror of adapted                        |      |
| `t_span_adaptor_band`            | adapted | as adapted, but also `band`              |      |
| `t_span_adaptor_band_periodic`   | adapted | as adapted, but also `band_periodic`     |  X   |
| `t_span_adaptor_band_mirror`     | adapted | mirror of adapted, but also `band`       |  X   |
| `t_span_adaptor_sub`             | adapted | sub-size or sub-till of adapted          |      |

## Aliases

|  alias           |   aliased     |
|:-----------------|:--------------|
| `vector`         |  1D `tensor`  |
| `matrix`         |  2D `tensor`  |
| `cuboid`         |  3D `tensor`  |
| `vector_span`    |  1D `t_span`  |
| `matrix_span`    |  2D `t_span`  |
| `cuboid_span`    |  3D `t_span`  |
