# numa

## Coding Guidelines

[Here](coding_guidelines.md)

## Submodules

* [boot](numa/boot.md)
* [tmpl](numa/tmpl.md)
* [ctxt](numa/ctxt.md)
* [core](numa/core.md)
* [cell](numa/cell.md)
* [lina](numa/lina.md)
* [misc](numa/misc.md)
* [type](numa/type.md)

The dependencies between the submodules are given in the following
graph, where an arrow from A to B means that A depends on B.

<img src='https://g.gravizo.com/svg?digraph G {
  type -> core;
  misc -> lina -> cell -> core -> ctxt -> tmpl -> boot;
}'/>

## ??? Parameter passing convention ???
By default, all parameters are passed by values. In particular, the elements of
cells are assumed to be cheap to copy; they may be moved where appropriate, by
they are never passed by reference.

For reference semantics, spans are used. If a function does not take one of its
cell parameters by value it should have the suffix `_inplace'.

