#include "numa/lina/dot.hpp"

#include "numa/ctxt/env_var.hpp"

#include "numa/misc/benchmark.hpp"
#include "numa/misc/candidates.hpp"
#include "numa/misc/main.hpp"

int
main(int const argc, char const** const argv)
{
  using namespace numa;
  using namespace numa::misc;

  env_var_scope sc(
      {{"ns", "[1, 2, 5, 8, 14, 17, 24, 30, 100][300, 1000]"},
       {"ps", "[1, 10, 100][1000]"},
       {"types", "[float, double]"},
       {"num_threads", "[1, 2, 4]"},
       {"cpu_targets", "[sse4_2, avx, avx2]"}});

  env_var_scope sct(env_var_map_test(
      argc,
      argv,
      {{"ns", "[2][4]"},
       {"ps", "[3][5]"},
       {"num_threads", "[1]"},
       {"cpu_targets", "[sse4_2]"}}));

  return numa::misc::main(argc, argv, [] {
    auto setup = [&] {
      auto ba_ns = benchmark_axis<index_t>::parse(env_var("ns"), f_pow(x, 2));

      auto ba_ps = benchmark_axis<index_t>::parse(env_var("ps"), f_pow(x, 1));

      auto ba_st = benchmark_axis<supported_type>::parse(
          env_var("types"), f_effort_supported_type());

      auto ba_num_threads =
          benchmark_axis<index_t>::parse(env_var("num_threads"), f_div(1.0, x));

      auto ba_cpu_target = benchmark_axis<cpu_target>::parse(
          env_var("cpu_targets"), f_effort_cpu_target());

      return make_benchmark_setup(
          std::move(ba_ns),
          std::move(ba_ps),
          std::move(ba_st),
          std::move(ba_num_threads),
          std::move(ba_cpu_target));
    }();

    auto candidates = benchmark_axis<identifier>::parse(
        env_var_default("candidates", "[dot, dot_kernel_cpu_target]"),
        f_const_t{1.0});

    return benchmark(
        std::move(setup),
        benchmark_map_dot{},
        std::move(candidates),
        f_is_close_supported_type())(
        candidate_dot{
            "dot",
            [](auto const& retval, auto const& A, auto const& B) {
              dot(retval, A, B);
            }},
        candidate_dot{
            "dot_kernel_cpu_target",
            []<typename T>(
                matrix_span<T> const& retval,
                matrix_span<T const> const& A,
                matrix_span<T const> const& B) {
              auto ma_A = (*dot_kernel_cpu_target<T>::allocate_left)(A.size());
              auto ma_B = (*dot_kernel_cpu_target<T>::allocate_right)(B.size());

              (*dot_kernel_cpu_target<T>::copy_left)(span(ma_A), A);
              (*dot_kernel_cpu_target<T>::copy_right)(span(ma_B), B);

              (*dot_kernel_cpu_target<T>::run_raw)(
                  retval, span(ma_A), span(ma_B));
            }});
  });
}
