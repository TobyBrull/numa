#include "numa/core/serialize_json.hpp"

#include "numa/misc/benchmark.hpp"

#include "numa/cell/reform.hpp"
#include "numa/cell/tensor.hpp"

#include "numa/ctxt/env_var.hpp"

#include <iostream>

using namespace numa;
using namespace numa::misc;

template<typename T, index_t N>
void
test_benchmark(const T& def)
{
  using numa::index;

  auto const ns = make_from_pretty<std::vector<index_t>>(env_var("ns"));

  std::cout << "n,unified,recursive,nat\n";

  for (index_t const n: ns) {
    if (product(index<N>(one) * n) > 10'000'000) {
      break;
    }

    const auto unified_rs = benchmark_quick(
        [&def, n]() {
          tensor<T, N> A;

          unified::resize(A.vary_axes(), index<N>(one) * n, def);

          unified::reserve_hard(A.vary_axes(), index<N>(one) * 2 * n);

          unified::resize(A.vary_axes(), index<N>(one) * 3 * n, def);

          return std::tuple(A.size(), A.capacity());
        },
        10);

    const auto recursive_rs = benchmark_quick(
        [&def, n]() {
          tensor<T, N> A;

          recursive::resize(A.vary_axes(), index<N>(one) * n, def);

          recursive::reserve_hard(A.vary_axes(), index<N>(one) * 2 * n);

          recursive::resize(A.vary_axes(), index<N>(one) * 3 * n, def);

          return std::tuple(A.size(), A.capacity());
        },
        10);

    std::cout << n << ',' << unified_rs.to_runtime_distribution().mode().count()
              << ',' << recursive_rs.to_runtime_distribution().mode().count();

    if constexpr (N == 1) {
      const auto nat_rs = benchmark_quick(
          [&def, n]() {
            std::vector<T> A;

            A.resize(n, def);

            A.reserve(2 * n);

            A.resize(3 * n, def);

            return std::tuple(A.size(), A.capacity());
          },
          10);

      std::cout << ',' << nat_rs.to_runtime_distribution().mode().count();
    }

    std::cout << '\n';
  }
}

int
main(int const argc, char const** const argv)
{
  env_var_scope sc(
      {{"ns", "[10, 100, 1000, 10000, 100000, 1000000, 10000000]"}});

  env_var_scope sct(env_var_map_test(argc, argv, {{"ns", "[3, 5]"}}));

  test_benchmark<double, 1>(3.14);
  test_benchmark<double, 2>(3.14);
  test_benchmark<double, 3>(3.14);
}
