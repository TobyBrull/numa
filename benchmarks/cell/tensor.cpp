#include "numa/misc/benchmark.hpp"
#include "numa/misc/data.hpp"

#include "numa/cell/t_span.hpp"

#include "numa/ctxt/env_var.hpp"

#include <chrono>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <random>

using namespace numa;
using namespace numa::misc;

int
main(int const argc, char const** const argv)
{
  env_var_scope sc({{"ns", "[10000, 100000, 1000000]"}});

  env_var_scope sct(env_var_map_test(argc, argv, {{"ns", "[10, 100]"}}));

  std::default_random_engine eng(0);

  auto const ns = make_from_pretty<std::vector<index_t>>(env_var("ns"));

  std::cout << "n,std_vector,iter_1,iter_2,"
               "iter_4,iter_8,iter_16\n";

  for (const index_t n: ns) {
    std::cout << n << ", ";

    for (const index_t stride: {1, 2, 4, 8, 16}) {
      const auto vec = std_random_vector<int>(n * stride, 100, eng);

      if (stride == 1) {
        const auto rs = benchmark_quick(
            [&vec] {
              int result = 0;

              for (const int val: vec) {
                result += val;
              }

              return result;
            },
            1);

        std::cout << rs.data().front().count() << ", ";
      }

      const auto rs = benchmark_quick(
          [&vec, n, stride] {
            int result = 0;

            const vector_span<const int> vs(vec.data(), n, stride);

            for (const int val: vs) {
              result += val;
            }

            return result;
          },
          1);

      std::cout << rs.data().front().count() << ", ";
    }

    std::cout << '\n';
  }
}
