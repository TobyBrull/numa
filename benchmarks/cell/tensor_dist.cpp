#include "numa/misc/benchmark.hpp"
#include "numa/misc/data.hpp"

#include "numa/cell/t_span.hpp"

#include "numa/ctxt/env_var.hpp"

#include <chrono>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <random>

using namespace numa;
using namespace numa::misc;

int
main(int const argc, char const** const argv)
{
  env_var_scope sc({{"ns", "[1000]"}});

  env_var_scope sct(env_var_map_test(argc, argv, {{"ns", "[10]"}}));

  std::default_random_engine eng(0);

  auto const ns = make_from_pretty<std::vector<index_t>>(env_var("ns"));

  for (index_t const n: ns) {
    {
      const auto vec = std_random_vector<int>(n * n, 100, eng);

      const auto rs = benchmark_quick(
          [&vec] {
            int result = 0;

            for (const int val: vec) {
              result += val;
            }

            return result;
          },
          100);

      std::cout << std::setw(23)
                << "vector: " << rs.to_runtime_distribution().mode().count()
                << '\n';
    }

    for (const index_t stride: {1, 2, 3, 4, 8}) {
      double base_time = -1.0;

      {
        const auto vec = std_random_vector<int>(n * n * stride, 100, eng);

        const auto rs = benchmark_quick(
            [&vec, n, stride] {
              int result = 0;

              const vector_span<const int> vs(vec.data(), n * n, stride);

              for (const int val: vs) {
                result += val;
              }

              return result;
            },
            100);

        base_time = rs.to_runtime_distribution().mode().count();

        std::cout << std::setw(20) << "vector_span_" << stride << ": "
                  << base_time << '\n';
      }

      {
        const auto vec = std_random_vector<int>(n * n * stride, 100, eng);

        const auto rs = benchmark_quick(
            [&vec, n, stride] {
              int result = 0;

              const vector_span<const int> vs(vec.data(), n * n, stride);

              for_each(vs)([&result](const int val) { result += val; });

              return result;
            },
            100);

        double fast_time = rs.to_runtime_distribution().mode().count();

        std::cout << std::setw(20) << "for_each_" << stride << ": " << fast_time
                  << "; fact = " << (base_time / fast_time)
                  << "; fact = " << (fast_time / base_time) << '\n';
      }

      {
        const auto vec = std_random_vector<int>(n * n * stride, 100, eng);

        const auto rs = benchmark_quick(
            [&vec, n, stride] {
              int result = 0;

              const matrix_span<const int> ms(
                  vec.data(), {n, n}, {stride, n * stride});

              for (const int val: ms) {
                result += val;
              }

              return result;
            },
            100);

        std::cout << std::setw(20) << "matrix_span_" << stride << ": "
                  << base_time << '\n';
      }

      {
        const auto vec = std_random_vector<int>(n * n * stride, 100, eng);

        const auto rs = benchmark_quick(
            [&vec, n, stride] {
              int result = 0;

              const matrix_span<const int> ms(
                  vec.data(), {n, n}, {stride, n * stride});

              for_each(ms)([&result](const int val) { result += val; });

              return result;
            },
            100);

        double fast_time = rs.to_runtime_distribution().mode().count();

        std::cout << std::setw(20) << "matrix_for_each_" << stride << ": "
                  << fast_time << '\n';
      }

      std::cout << "\n\n";
    }
  }
}
