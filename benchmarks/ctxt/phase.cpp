#include "numa/ctxt/phase.hpp"

#include "numa/misc/fibonacci.hpp"

#include <fstream>
#include <iostream>

using namespace numa;

namespace {
  index_t fib_pure(index_t const n)
  {
    if (n == 0) {
      return 1;
    }
    else if (n == 1) {
      return 1;
    }
    else {
      return fib_pure(n - 1) + fib_pure(n - 2);
    }
  }

  index_t fib_paragraph(index_t const n)
  {
    phase p{c_paragraph, "fib_paragraph"_s, NUMA_LOCATION};

    p.NUMA_ARGS(n, 2 * n);

    if (n == 0) {
      return 1;
    }
    else if (n == 1) {
      return 1;
    }
    else {
      return fib_paragraph(n - 1) + fib_paragraph(n - 2);
    }
  }

  index_t fib_paragraph_dynamic(index_t const n)
  {
    phase p{c_paragraph, "fib_paragraph_dynamic"_s, NUMA_LOCATION};
    phase_status(c_trace, NUMA_VAR(n));

    if ((n == 0) || (n == 1)) {
      auto retval = 1;
      phase_status(c_trace, "retval = ", retval);
      return retval;
    }
    else {
      const auto fib_n =
          fib_paragraph_dynamic(n - 1) + fib_paragraph_dynamic(n - 2);

      NUMA_TRACE_VAR(fib_n);
      // NUMA_TRACE_VAR (fib_n, n - 1);
      return fib_n;
    }
  }
}

int
main()
{
  // index_t const n = 32;
  // index_t const n = 16;
  index_t const n = 6;

  {
    double duration_baseline = 0.0;
    double duration_full     = 0.0;
    double duration_disabled = 0.0;

    double const count = (2.0 * fib_pure(n) - 1.0);

    {
      context<phase_receptor, phase_receptor_main> scp(
          c_off, c_off, phase_delegate());

      {
        phase p{c_section, "test"_s, NUMA_LOCATION};
        fib_pure(n);
      }

      duration_baseline =
          static_cast<double>(scp.record().strand().duration_total().count());
    }

    //    {
    //      context<phase_receptor, phase_receptor_main> scp;
    //
    //      {
    //        phase p {c_section, "test"_s, NUMA_LOCATION};
    //        fib_paragraph (n);
    //      }
    //
    //      {
    //        std::ofstream ofs ("out_complete.json");
    //        scp.record ().to_json (ofs, trace_event_format::complete);
    //      }
    //      {
    //        std::ofstream ofs ("out_duration.json");
    //        scp.record ().to_json (ofs, trace_event_format::duration);
    //      }
    //
    //      duration_full = static_cast<double> (
    //          scp.record ().strand ().duration_total ().count ());
    //    }
    //
    //    {
    //      context<phase_receptor, phase_receptor_main> scp
    //      (phase_level_t::section);
    //
    //      {
    //        phase p {c_section, "test"_s, NUMA_LOCATION};
    //        fib_paragraph (n);
    //      }
    //
    //      duration_disabled = static_cast<double> (
    //          scp.record ().strand ().duration_total ().count ());
    //    }

    {
      context<phase_receptor, phase_receptor_main> scp{
          c_paragraph, c_paragraph, phase_delegate()};

      {
        phase p{c_section, "test"_s, NUMA_LOCATION};
        fib_paragraph_dynamic(n);
      }

      {
        phase p{c_section, "test2"_s, NUMA_LOCATION};
        fib_paragraph_dynamic(n);
      }

      {
        std::ofstream ofs("json/out_complete.json");
        scp.record().remove_overhead_copy().to_json(
            ofs, trace_event_format::complete);
      }
      {
        std::ofstream ofs("json/out_duration.json");
        scp.record().remove_overhead_copy().to_json(
            ofs, trace_event_format::duration);
      }

      duration_full =
          static_cast<double>(scp.record().strand().duration_total().count());
    }

    {
      context<phase_receptor, phase_receptor_main> scp(
          c_section, c_section, phase_delegate());

      {
        phase p{c_section, "test"_s, NUMA_LOCATION};
        fib_paragraph_dynamic(n);
      }

      duration_disabled =
          static_cast<double>(scp.record().strand().duration_total().count());
    }

    std::cout << "Duration baseline: " << duration_baseline << '\n';
    std::cout << "Duration full:     " << duration_full << '\n';
    std::cout << "Duration disabled: " << duration_disabled << "\n\n";

    std::cout << "#scopes = " << count << "\n\n";

    std::cout << "Total overhead full:     "
              << (duration_full - duration_baseline) << '\n';
    std::cout << "Total overhead disabled: "
              << (duration_disabled - duration_baseline) << '\n';

    std::cout << "Per-scope overhead full:     "
              << (duration_full - duration_baseline) / count << '\n';
    std::cout << "Per-scope overhead disabled: "
              << (duration_disabled - duration_baseline) / count << '\n';
  }
}
