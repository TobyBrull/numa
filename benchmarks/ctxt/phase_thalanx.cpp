#include "numa/ctxt/phase.hpp"

#include "numa/misc/fibonacci.hpp"

#include "numa/core/serialize_pretty.hpp"

#include "numa/ctxt/env_var.hpp"

#include <fstream>
#include <iostream>

using namespace numa;

namespace {
  index_t fib_paragraph(index_t const n)
  {
    phase p{c_paragraph, "fib_paragraph"_s, NUMA_LOCATION};

    p.NUMA_ARGS(n, 2 * n);

    if (n == 0) {
      return 1;
    }
    else if (n == 1) {
      return 1;
    }
    else {
      return fib_paragraph(n - 1) + fib_paragraph(n - 2);
    }
  }

  index_t fib_thalanx(index_t const n)
  {
    phase p{c_chapter, "fib_thalanx"_s, NUMA_LOCATION};

    p.NUMA_ARGS(n);

    if ((n <= 3) || thalanx::size() == 1) {
      return fib_paragraph(n) + 1;
    }
    else {
      auto const [f_1, f_2] = thalanx_fork(
          chunking_equal{},
          [n] { return fib_thalanx(n - 1); },
          [n] { return fib_thalanx(n - 2); });

      return (f_1 + f_2);
    }
  }
}

int
main(int const argc, char const** const argv)
{
  env_var_scope sc({{"n", "20"}});

  env_var_scope sct(env_var_map_test(argc, argv, {{"n", "4"}}));

  index_t const n = make_from_pretty<index_t>(env_var("n"));

  index_t const num_threads = 4;

  thalanx th(num_threads);

  {
    context<phase_receptor, phase_receptor_main> scp{
        c_paragraph, c_off, phase_delegate()};

    {
      phase p{c_volume, "test"_s, NUMA_LOCATION};
      fib_thalanx(n);
    }

    {
      std::ofstream ofs("json/out_complete.json");
      scp.record().to_json(ofs, trace_event_format::complete);
    }
    {
      std::ofstream ofs("json/out_duration.json");
      scp.record().to_json(ofs, trace_event_format::duration);
    }
  }
}
